/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   void                 (*COM1_TxFunc)( void);
static   void                 (*COM1_RxFunc)( void);
static   void                 (*COM2_TxFunc)( void);
static   void                 (*COM2_RxFunc)( void);
#ifdef   _IO2560_H
static   void                 (*COM3_TxFunc)( void);
static   void                 (*COM3_RxFunc)( void);
static   void                 (*COM4_TxFunc)( void);
static   void                 (*COM4_RxFunc)( void);
#endif   // ]

#if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)
static   void                 (*COM5_TxFunc)( void);
static   void                 (*COM5_RxFunc)( void);
static   void                 (*COM6_TxFunc)( void);
static   void                 (*COM6_RxFunc)( void);
#endif

#if   defined(__USES_EXAR_4_PORTS__)
static   void                 (*COM7_TxFunc)( void);
static   void                 (*COM7_RxFunc)( void);
static   void                 (*COM8_TxFunc)( void);
static   void                 (*COM8_RxFunc)( void);
#endif

static   usint                drv_ModeBit;
static   usint                drv_StateDTR;
static   usint                drv_StateRTS;
/*------------------------ Local Function Prototypes -------------------------*/
static   void                 UartDRV_Exar_SetBaudRate( tUart  *pUart);
static   void                 UartDRV_Exar_SetBitsAndParity( tUart  *pUart);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		UartDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
   COM1_TxFunc   =  NULL;
   COM1_RxFunc   =  NULL;
   COM2_TxFunc   =  NULL;
   COM2_RxFunc   =  NULL;

   // Extra Built-in 2-UART
   #ifdef   __IO2560_H
   COM3_TxFunc   =  NULL;
   COM3_RxFunc   =  NULL;
   COM4_TxFunc   =  NULL;
   COM4_RxFunc   =  NULL;
   #endif

   // Exar 2-UART or 4-UART
   #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
   COM5_TxFunc   =  NULL;
   COM5_RxFunc   =  NULL;
   COM6_TxFunc   =  NULL;
   COM6_RxFunc   =  NULL;
   #endif   // ]

   // Exar 4-UART
   #if   defined(__USES_EXAR_4_PORTS__)   // [
   COM7_TxFunc   =  NULL;
   COM7_RxFunc   =  NULL;
   COM8_TxFunc   =  NULL;
   COM8_RxFunc   =  NULL;
   #endif   // ]

   drv_ModeBit    =  0;
   drv_StateDTR   =  0;
   drv_StateRTS   =  0;

   HW_DEF_COM_1_RS485_DIS;
   HW_DEF_COM_2_RS485_DIS;
   #ifdef   __IO2560_H
   HW_DEF_COM_3_RS485_DIS;
   HW_DEF_COM_4_RS485_DIS;
   #endif
}



/*----------------------------------------------------------------------------*/
void                    		UartDRV_SetPort( tUart  *pUart)
/*----------------------------------------------------------------------------*/
{
   if( pUart != NULL)
   {
      if(   (Util_IsBufferFullOf( (byte *)pUart, sizeof( tUart), 0x00) == TRUE)  ||
            (Util_IsBufferFullOf( (byte *)pUart, sizeof( tUart), 0xFF) == TRUE))
      {
         return;
      }

      UartDRV_Exar_SetBaudRate( pUart);
      UartDRV_Exar_SetBitsAndParity( pUart);
      switch( pUart->Com.Port)
      {
         case  COM_1:   //  (RS485)
               HW_DEF_COM_1_RS485_DIS;
               UCSR0A                     =  0x00;
               UCSR0B                     =  HW_DEF_COM_1_RX_ENA;
               COM1_TxFunc                =  pUart->TxFunc;
               COM1_RxFunc                =  pUart->RxFunc;
               pUart->Uart_TxByte         =  UartDRV_COM1_TxByte;
               pUart->Uart_TxEnd          =  UartDRV_COM1_TxEnd;
               pUart->Uart_RxByte         =  UartDRV_COM1_RxByte;
               pUart->Uart_RS485_Enable   =  UartDRV_COM1_RS485_Enable;
               pUart->Uart_RS485_Disable  =  UartDRV_COM1_RS485_Disable;
               break;

         case  COM_2:   //  (RS232)
               HW_DEF_COM_2_RS485_DIS;
               UCSR1A                     =  0x00;
               UCSR1B                     =  HW_DEF_COM_2_RX_ENA;
               COM2_TxFunc                =  pUart->TxFunc;
               COM2_RxFunc                =  pUart->RxFunc;
               pUart->Uart_TxByte         =  UartDRV_COM2_TxByte;
               pUart->Uart_TxEnd          =  UartDRV_COM2_TxEnd;
               pUart->Uart_RxByte         =  UartDRV_COM2_RxByte;
               pUart->Uart_RS485_Enable   =  UartDRV_COM2_RS485_Enable;
               pUart->Uart_RS485_Disable  =  UartDRV_COM2_RS485_Disable;
               break;

         #ifdef   __IO2560_H
         case  COM_3:
               HW_DEF_COM_3_RS485_DIS;
               UCSR2A                     =  0x00;
               UCSR2B                     =  HW_DEF_COM_3_RX_ENA;
               COM3_TxFunc                =  pUart->TxFunc;
               COM3_RxFunc                =  pUart->RxFunc;
               pUart->Uart_TxByte         =  UartDRV_COM3_TxByte;
               pUart->Uart_TxEnd          =  UartDRV_COM3_TxEnd;
               pUart->Uart_RxByte         =  UartDRV_COM3_RxByte;
               pUart->Uart_RS485_Enable   =  UartDRV_COM3_RS485_Enable;
               pUart->Uart_RS485_Disable  =  UartDRV_COM3_RS485_Disable;
               break;

         case  COM_4:
               HW_DEF_COM_4_RS485_DIS;
               UCSR3A                     =  0x00;
               UCSR3B                     =  HW_DEF_COM_4_RX_ENA;
               COM4_TxFunc                =  pUart->TxFunc;
               COM4_RxFunc                =  pUart->RxFunc;
               pUart->Uart_TxByte         =  UartDRV_COM4_TxByte;
               pUart->Uart_TxEnd          =  UartDRV_COM4_TxEnd;
               pUart->Uart_RxByte         =  UartDRV_COM4_RxByte;
               pUart->Uart_RS485_Enable   =  UartDRV_COM4_RS485_Enable;
               pUart->Uart_RS485_Disable  =  UartDRV_COM4_RS485_Disable;
               break;
         #endif

         #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
         case  COM_5:  // Exar UART A (RS232)
               HW_DEF_EXAR_MCRA           =  0x08; // Control DCD in Loopback mode
               HW_DEF_COM_5_RX_ENA;
               COM5_TxFunc                =  pUart->TxFunc;
               COM5_RxFunc                =  pUart->RxFunc;
               pUart->Uart_TxByte         =  UartDRV_COM5_TxByte;
               pUart->Uart_TxEnd          =  UartDRV_COM5_TxEnd;
               pUart->Uart_RxByte         =  UartDRV_COM5_RxByte;
               pUart->Uart_RS485_Enable   =  UartDRV_COM5_RS485_Enable;
               pUart->Uart_RS485_Disable  =  UartDRV_COM5_RS485_Disable;
               break;

         case  COM_6:  // Exar UART B (RS232)
               HW_DEF_EXAR_MCRB           =  0x08; // Control DCD in Loopback mode
               HW_DEF_COM_6_RX_ENA;
               COM6_TxFunc                =  pUart->TxFunc;
               COM6_RxFunc                =  pUart->RxFunc;
               pUart->Uart_TxByte         =  UartDRV_COM6_TxByte;
               pUart->Uart_TxEnd          =  UartDRV_COM6_TxEnd;
               pUart->Uart_RxByte         =  UartDRV_COM6_RxByte;
               pUart->Uart_RS485_Enable   =  UartDRV_COM6_RS485_Enable;
               pUart->Uart_RS485_Disable  =  UartDRV_COM6_RS485_Disable;
               break;
         #endif   // ]

         #if   defined(__USES_EXAR_4_PORTS__)  // [
         case  COM_7:  // Exar UART C (RS232)
               HW_DEF_EXAR_MCRC           =  0x08; // Control DCD in Loopback mode
               HW_DEF_COM_7_RX_ENA;
               COM7_TxFunc                =  pUart->TxFunc;
               COM7_RxFunc                =  pUart->RxFunc;
               pUart->Uart_TxByte         =  UartDRV_COM7_TxByte;
               pUart->Uart_TxEnd          =  UartDRV_COM7_TxEnd;
               pUart->Uart_RxByte         =  UartDRV_COM7_RxByte;
               pUart->Uart_RS485_Enable   =  UartDRV_COM7_RS485_Enable;
               pUart->Uart_RS485_Disable  =  UartDRV_COM7_RS485_Disable;
               break;

         case  COM_8:  // Exar UART D (RS232)
               HW_DEF_EXAR_MCRD           =  0x08; // Control DCD in Loopback mode
               HW_DEF_COM_8_RX_ENA;
               COM8_TxFunc                =  pUart->TxFunc;
               COM8_RxFunc                =  pUart->RxFunc;
               pUart->Uart_TxByte         =  UartDRV_COM8_TxByte;
               pUart->Uart_TxEnd          =  UartDRV_COM8_TxEnd;
               pUart->Uart_RxByte         =  UartDRV_COM8_RxByte;
               pUart->Uart_RS485_Enable   =  UartDRV_COM8_RS485_Enable;
               pUart->Uart_RS485_Disable  =  UartDRV_COM8_RS485_Disable;
               break;
         #endif   // ]
      }
   }
}



#if 0
"///////////////////////////////////   UART 1 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif

#ifdef   __USES_RS485_COM_1__
   #ifdef   __IO2560_H
      #pragma vector = USART0_TX_vect     // Tx Complete Interrupt
   #else
      #pragma vector = USART0_TXC_vect    // Tx Complete Interrupt
   #endif
#else
   #ifdef   __IO2560_H
      #pragma vector = USART0_UDRE_vect   // UDR Interrupt
   #else
      #pragma vector = USART0_UDRE_vect   // UDR Interrupt
   #endif
#endif
/*----------------------------------------------------------------------------*/
static __interrupt void       UartDRV_COM1_TxISR( void)
/*----------------------------------------------------------------------------*/
{
   if( COM1_TxFunc != NULL)   COM1_TxFunc();
   else                       UartDRV_COM1_TxEnd();
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM1_RS485_Enable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_1_RS485_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM1_RS485_Disable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_1_RS485_DIS;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM1_TxByte( byte Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_1_RS485_ENA;

   if( BIT_IS_SET( drv_ModeBit, BIT_0))   SET_BIT  ( UCSR0B, BIT_0);
   else                                   CLEAR_BIT( UCSR0B, BIT_0);

   UDR0     = Data;
   UCSR0B   |= HW_DEF_COM_1_TX_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM1_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
   CLEAR_BIT( drv_ModeBit, BIT_0);
   UCSR0B   = HW_DEF_COM_1_RX_ENA;

   HW_DEF_COM_1_RS485_DIS;
}



#ifdef   __IO2560_H
   #pragma vector = USART0_RX_vect
#else
   #pragma vector = USART0_RXC_vect
#endif
/*----------------------------------------------------------------------------*/
static __interrupt void       UartDRV_COM1_RxISR( void)
/*----------------------------------------------------------------------------*/
{
   if( COM1_RxFunc != NULL)   COM1_RxFunc();
   else                       UartDRV_COM1_RxByte();
}



/*----------------------------------------------------------------------------*/
byte                          UartDRV_COM1_RxByte( void)
/*----------------------------------------------------------------------------*/
{
   return( UDR0);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM1_SetModeBit( bool Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode == ON)   SET_BIT  ( drv_ModeBit, BIT_0);
   else              CLEAR_BIT( drv_ModeBit, BIT_0);
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_COM1_GetModeBit( void)
/*----------------------------------------------------------------------------*/
{
   return( BIT_IS_SET( UCSR0B, BIT_1) ? TRUE : FALSE);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM1_DisableTxInt( void)
/*----------------------------------------------------------------------------*/
{
   UCSR0B   = HW_DEF_COM_1_RX_ENA;
}



#if 0
"///////////////////////////////////   UART 2 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif

#ifdef   __USES_RS485_COM_2__
   #ifdef   __IO2560_H
      #pragma vector = USART1_TX_vect     // Tx Complete Interrupt
   #else
      #pragma vector = USART1_TXC_vect    // Tx Complete Interrupt
   #endif
#else
   #ifdef   __IO2560_H
      #pragma vector = USART1_UDRE_vect   // UDR Interrupt
   #else
      #pragma vector = USART1_UDRE_vect   // UDR Interrupt
   #endif
#endif
/*----------------------------------------------------------------------------*/
static __interrupt void       UartDRV_COM2_TxISR( void)
/*----------------------------------------------------------------------------*/
{
   if( COM2_TxFunc != NULL)   COM2_TxFunc();
   else                       UartDRV_COM2_TxEnd();
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM2_RS485_Enable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_2_RS485_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM2_RS485_Disable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_2_RS485_DIS;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM2_TxByte( byte Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_2_RS485_ENA;

   if( BIT_IS_SET( drv_ModeBit, BIT_1))   SET_BIT  ( UCSR1B, BIT_0);
   else                                   CLEAR_BIT( UCSR1B, BIT_0);

   UDR1     = Data;
   UCSR1B   |= HW_DEF_COM_2_TX_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM2_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
   CLEAR_BIT( drv_ModeBit, BIT_1);
   UCSR1B   = HW_DEF_COM_2_RX_ENA;

   HW_DEF_COM_2_RS485_DIS;
}



#ifdef   __IO2560_H
   #pragma vector = USART1_RX_vect
#else
   #pragma vector = USART1_RXC_vect
#endif
/*----------------------------------------------------------------------------*/
static __interrupt void       UartDRV_COM2_RxISR( void)
/*----------------------------------------------------------------------------*/
{
   if( COM2_RxFunc != NULL)   COM2_RxFunc();
   else                       UartDRV_COM2_RxByte();
}



/*----------------------------------------------------------------------------*/
byte                          UartDRV_COM2_RxByte( void)
/*----------------------------------------------------------------------------*/
{
   return( UDR1);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM2_SetModeBit( bool Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode == ON)   SET_BIT  ( drv_ModeBit, BIT_1);
   else              CLEAR_BIT( drv_ModeBit, BIT_1);
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_COM2_GetModeBit( void)
/*----------------------------------------------------------------------------*/
{
   return( BIT_IS_SET( UCSR1B, BIT_1) ? TRUE : FALSE);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM2_DisableTxInt( void)
/*----------------------------------------------------------------------------*/
{
   UCSR1B   = HW_DEF_COM_2_RX_ENA;
}



#ifdef   __IO2560_H  // [
#if 0
"///////////////////////////////////   UART 3 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif

#ifdef   __USES_RS485_COM_3__
   #ifdef   __IO2560_H
      #pragma vector = USART2_TX_vect     // Tx Complete Interrupt
   #else
      #pragma vector = USART2_TXC_vect    // Tx Complete Interrupt
   #endif
#else
   #ifdef   __IO2560_H
      #pragma vector = USART2_UDRE_vect   // UDR Interrupt
   #else
      #pragma vector = USART2_UDRE_vect   // UDR Interrupt
   #endif
#endif
/*----------------------------------------------------------------------------*/
static __interrupt void       UartDRV_COM3_TxISR( void)
/*----------------------------------------------------------------------------*/
{
   if( COM3_TxFunc != NULL)   COM3_TxFunc();
   else                       UartDRV_COM3_TxEnd();
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM3_RS485_Enable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_3_RS485_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM3_RS485_Disable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_3_RS485_DIS;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM3_TxByte( byte Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_3_RS485_ENA;

   if( BIT_IS_SET( drv_ModeBit, BIT_2))   SET_BIT  ( UCSR2B, BIT_0);
   else                                   CLEAR_BIT( UCSR2B, BIT_0);

   UDR2     = Data;
   SET_BIT( UCSR2B, HW_DEF_COM_3_TX_ENA);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM3_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
   CLEAR_BIT( drv_ModeBit, BIT_2);
   UCSR2B   = HW_DEF_COM_3_RX_ENA;

   HW_DEF_COM_3_RS485_DIS;
}



#pragma vector = USART2_RX_vect
/*----------------------------------------------------------------------------*/
static __interrupt void       UartDRV_COM3_RxISR( void)
/*----------------------------------------------------------------------------*/
{
   if( COM3_RxFunc != NULL)   COM3_RxFunc();
   else                       UartDRV_COM3_RxByte();
}



/*----------------------------------------------------------------------------*/
byte                          UartDRV_COM3_RxByte( void)
/*----------------------------------------------------------------------------*/
{
   return( UDR2);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM3_SetModeBit( bool Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode == ON)   SET_BIT  ( drv_ModeBit, BIT_2);
   else              CLEAR_BIT( drv_ModeBit, BIT_2);
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_COM3_GetModeBit( void)
/*----------------------------------------------------------------------------*/
{
   return( BIT_IS_SET( UCSR2B, BIT_1) ? TRUE : FALSE);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM3_DisableTxInt( void)
/*----------------------------------------------------------------------------*/
{
   UCSR2B   = HW_DEF_COM_3_RX_ENA;
}
#endif   // ]



#ifdef   __IO2560_H  // [
#if 0
"///////////////////////////////////   UART 4 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif

#ifdef   __USES_RS485_COM_4__
   #ifdef   __IO2560_H
      #pragma vector = USART3_TX_vect     // Tx Complete Interrupt
   #else
      #pragma vector = USART3_TXC_vect    // Tx Complete Interrupt
   #endif
#else
   #ifdef   __IO2560_H
      #pragma vector = USART3_UDRE_vect   // UDR Interrupt
   #else
      #pragma vector = USART3_UDRE_vect   // UDR Interrupt
   #endif
#endif
/*----------------------------------------------------------------------------*/
static __interrupt void       UartDRV_COM4_TxISR( void)
/*----------------------------------------------------------------------------*/
{
   if( COM4_TxFunc != NULL)   COM4_TxFunc();
   else                       UartDRV_COM4_TxEnd();
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM4_RS485_Enable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_4_RS485_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM4_RS485_Disable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_4_RS485_DIS;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM4_TxByte( byte Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_4_RS485_ENA;

   if( BIT_IS_SET( drv_ModeBit, BIT_3))   SET_BIT  ( UCSR3B, BIT_0);
   else                                   CLEAR_BIT( UCSR3B, BIT_0);

   UDR3     = Data;
   SET_BIT( UCSR3B, HW_DEF_COM_4_TX_ENA);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM4_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
   CLEAR_BIT( drv_ModeBit, BIT_3);
   UCSR3B   = HW_DEF_COM_4_RX_ENA;

   HW_DEF_COM_4_RS485_DIS;
}



#pragma vector = USART3_RX_vect
static __interrupt void       UartDRV_COM4_RxISR( void)
/*----------------------------------------------------------------------------*/
{
   if( COM4_RxFunc != NULL)   COM4_RxFunc();
   else                       UartDRV_COM4_RxByte();
}



/*----------------------------------------------------------------------------*/
byte                          UartDRV_COM4_RxByte( void)
/*----------------------------------------------------------------------------*/
{
   return( UDR3);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM4_SetModeBit( bool Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode == ON)   SET_BIT  ( drv_ModeBit, BIT_3);
   else              CLEAR_BIT( drv_ModeBit, BIT_3);
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_COM4_GetModeBit( void)
/*----------------------------------------------------------------------------*/
{
   return( BIT_IS_SET( UCSR3B, BIT_1) ? TRUE : FALSE);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM4_DisableTxInt( void)
/*----------------------------------------------------------------------------*/
{
   UCSR3B   = HW_DEF_COM_4_RX_ENA;
}
#endif   // ]



#if 0
"///////////////////////////////////   EXAR-A  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
"///////////////////////////////////   EXAR-B  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
"///////////////////////////////////   EXAR-C  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
"///////////////////////////////////   EXAR-D  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif
#pragma vector =  INT1_vect   // Common INT for Exar UART
/*----------------------------------------------------------------------------*/
static __interrupt void       UartDRV_Exar_CommonISR( void)
/*----------------------------------------------------------------------------*/
{
   #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
   #if 0
   "Exar A"
   #endif
   if( BIT_IS_SET( HW_DEF_EXAR_LSRA, BIT_5) && BIT_IS_SET( HW_DEF_EXAR_IERA, BIT_1))
   {
      if( COM5_TxFunc != NULL)   COM5_TxFunc();
      else                       UartDRV_COM5_TxEnd();
   }

   if( HW_DEF_EXAR_LSRA & 0x0F)
   {
      if( COM5_RxFunc != NULL)   COM5_RxFunc();
      else                       UartDRV_COM5_RxByte();
   }

   #if 0
   "Exar B"
   #endif
   if( BIT_IS_SET( HW_DEF_EXAR_LSRB, BIT_5) && BIT_IS_SET( HW_DEF_EXAR_IERB, BIT_1))
   {
      if( COM6_TxFunc != NULL)   COM6_TxFunc();
      else                       UartDRV_COM6_TxEnd();
   }

   if( HW_DEF_EXAR_LSRB & 0x0F)
   {
      if( COM6_RxFunc != NULL)   COM6_RxFunc();
      else                       UartDRV_COM6_RxByte();
   }
   #endif   // ]

   #if   defined(__USES_EXAR_4_PORTS__)  // [
   #if 0
   "Exar C"
   #endif
   if( BIT_IS_SET( HW_DEF_EXAR_LSRC, BIT_5) && BIT_IS_SET( HW_DEF_EXAR_IERC, BIT_1))
   {
      if( COM7_TxFunc != NULL)   COM7_TxFunc();
      else                       UartDRV_COM7_TxEnd();
   }

   if( HW_DEF_EXAR_LSRC & 0x0F)
   {
      if( COM7_RxFunc != NULL)   COM7_RxFunc();
      else                       UartDRV_COM7_RxByte();
   }

   #if 0
   "Exar D"
   #endif
   if( BIT_IS_SET( HW_DEF_EXAR_LSRD, BIT_5) && BIT_IS_SET( HW_DEF_EXAR_IERD, BIT_1))
   {
      if( COM8_TxFunc != NULL)   COM8_TxFunc();
      else                       UartDRV_COM8_TxEnd();
   }

   if( HW_DEF_EXAR_LSRD & 0x0F)
   {
      if( COM8_RxFunc != NULL)   COM8_RxFunc();
      else                       UartDRV_COM8_RxByte();
   }
   #endif   // ]
}



#if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
#if 0
"///////////////////////////////////   EXAR-A \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif
/*----------------------------------------------------------------------------*/
void                          UartDRV_COM5_RS485_Enable( void)
/*----------------------------------------------------------------------------*/
{
   // Thus function must be present. DON'T DELETE IT !!!
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM5_RS485_Disable( void)
/*----------------------------------------------------------------------------*/
{
   // Thus function must be present. DON'T DELETE IT !!!
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM5_TxByte( byte Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_EXAR_THRA  =  Data;
   HW_DEF_COM_5_TX_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM5_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_5_RX_ENA;
}



/*----------------------------------------------------------------------------*/
byte                          UartDRV_COM5_RxByte( void)
/*----------------------------------------------------------------------------*/
{
   return( HW_DEF_EXAR_RHRA);
}



#if 0
"///////////////////////////////////   EXAR-B \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif
/*----------------------------------------------------------------------------*/
void                          UartDRV_COM6_RS485_Enable( void)
/*----------------------------------------------------------------------------*/
{
   // Thus function must be present. DON'T DELETE IT !!!
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM6_RS485_Disable( void)
/*----------------------------------------------------------------------------*/
{
   // Thus function must be present. DON'T DELETE IT !!!
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM6_TxByte( byte Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_EXAR_THRB  =  Data;
   HW_DEF_COM_6_TX_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM6_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_6_RX_ENA;
}



/*----------------------------------------------------------------------------*/
byte                          UartDRV_COM6_RxByte( void)
/*----------------------------------------------------------------------------*/
{
   return( HW_DEF_EXAR_RHRB);
}
#endif // ]


#if   defined(__USES_EXAR_4_PORTS__)  // [
#if 0
"///////////////////////////////////   EXAR-C \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif
/*----------------------------------------------------------------------------*/
void                          UartDRV_COM7_RS485_Enable( void)
/*----------------------------------------------------------------------------*/
{
   // Thus function must be present. DON'T DELETE IT !!!
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM7_RS485_Disable( void)
/*----------------------------------------------------------------------------*/
{
   // Thus function must be present. DON'T DELETE IT !!!
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM7_TxByte( byte Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_EXAR_THRC  =  Data;
   HW_DEF_COM_7_TX_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM7_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_7_RX_ENA;
}



/*----------------------------------------------------------------------------*/
byte                          UartDRV_COM7_RxByte( void)
/*----------------------------------------------------------------------------*/
{
   return( HW_DEF_EXAR_RHRC);
}



#if 0
"///////////////////////////////////   EXAR-D \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
#endif
/*----------------------------------------------------------------------------*/
void                          UartDRV_COM8_RS485_Enable( void)
/*----------------------------------------------------------------------------*/
{
   // Thus function must be present. DON'T DELETE IT !!!
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM8_RS485_Disable( void)
/*----------------------------------------------------------------------------*/
{
   // Thus function must be present. DON'T DELETE IT !!!
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM8_TxByte( byte Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_EXAR_THRD  =  Data;
   HW_DEF_COM_8_TX_ENA;
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_COM8_TxEnd( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_COM_8_RX_ENA;
}



/*----------------------------------------------------------------------------*/
byte                          UartDRV_COM8_RxByte( void)
/*----------------------------------------------------------------------------*/
{
   return( HW_DEF_EXAR_RHRD);
}
#endif   // [



#if 0
"///////////////////////////   Control UART I/O lines  \\\\\\\\\\\\\\\\\\\\\\\\\"
#endif
/*----------------------------------------------------------------------------*/
void                          UartDRV_SetDTR( byte Port, bool Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode != OFF)
      Mode = ON;

   switch( Port)
   {
      case  COM_1:   if( Mode == OFF)  HW_DEF_COM_1_DTR_OFF;   else  HW_DEF_COM_1_DTR_ON; break;
      case  COM_2:   if( Mode == OFF)  HW_DEF_COM_2_DTR_OFF;   else  HW_DEF_COM_2_DTR_ON; break;
      #ifdef   __IO2560_H
      case  COM_3:   if( Mode == OFF)  HW_DEF_COM_3_DTR_OFF;   else  HW_DEF_COM_3_DTR_ON; break;
      case  COM_4:   if( Mode == OFF)  HW_DEF_COM_4_DTR_OFF;   else  HW_DEF_COM_4_DTR_ON; break;
      #endif

      #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_5:   if( Mode == OFF)  HW_DEF_COM_5_DTR_OFF;   else  HW_DEF_COM_5_DTR_ON; break;
      case  COM_6:   if( Mode == OFF)  HW_DEF_COM_6_DTR_OFF;   else  HW_DEF_COM_6_DTR_ON; break;
      #endif   // ]

      #if   defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_7:   if( Mode == OFF)  HW_DEF_COM_7_DTR_OFF;   else  HW_DEF_COM_7_DTR_ON; break;
      case  COM_8:   if( Mode == OFF)  HW_DEF_COM_8_DTR_OFF;   else  HW_DEF_COM_8_DTR_ON; break;
      #endif   // ]

      default:       return;
   }

   SET_BIT( drv_StateDTR, (Mode << Port));
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_GetDTR( byte Port)
/*----------------------------------------------------------------------------*/
{
   if( Port == COM_VOID)
      return( FALSE);

   return( BIT_IS_SET( drv_StateDTR, (1 << Port)) ? TRUE : FALSE);
}



/*----------------------------------------------------------------------------*/
void                          UartDRV_SetRTS( byte Port, bool Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode != OFF)
      Mode = ON;

   switch( Port)
   {
      case  COM_1:   if( Mode == OFF)  HW_DEF_COM_1_RTS_OFF;   else  HW_DEF_COM_1_RTS_ON; break;
      case  COM_2:   if( Mode == OFF)  HW_DEF_COM_2_RTS_OFF;   else  HW_DEF_COM_2_RTS_ON; break;
      #ifdef   __IO2560_H
      case  COM_3:   if( Mode == OFF)  HW_DEF_COM_3_RTS_OFF;   else  HW_DEF_COM_3_RTS_ON; break;
      case  COM_4:   if( Mode == OFF)  HW_DEF_COM_4_RTS_OFF;   else  HW_DEF_COM_4_RTS_ON; break;
      #endif

      #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_5:   if( Mode == OFF)  HW_DEF_COM_5_RTS_OFF;   else  HW_DEF_COM_5_RTS_ON; break;
      case  COM_6:   if( Mode == OFF)  HW_DEF_COM_6_RTS_OFF;   else  HW_DEF_COM_6_RTS_ON; break;
      #endif   // ]

      #if   defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_7:   if( Mode == OFF)  HW_DEF_COM_7_RTS_OFF;   else  HW_DEF_COM_7_RTS_ON; break;
      case  COM_8:   if( Mode == OFF)  HW_DEF_COM_8_RTS_OFF;   else  HW_DEF_COM_8_RTS_ON; break;
      #endif   // ]

      default:       return;
   }

   SET_BIT( drv_StateRTS, (Mode << Port));
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_GetRTS( byte Port)
/*----------------------------------------------------------------------------*/
{
   return( BIT_IS_SET( drv_StateRTS, (1 << Port)) ? TRUE : FALSE);
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_GetDSR( byte Port)
/*----------------------------------------------------------------------------*/
{
static   bool     fResult;


   fResult  =  FALSE;

   switch( Port)
   {
      case  COM_1:   fResult  =  HW_DEF_COM_1_DSR; break;
      case  COM_2:   fResult  =  HW_DEF_COM_2_DSR; break;
      #ifdef   __IO2560_H
      case  COM_3:   fResult  =  HW_DEF_COM_3_DSR; break;
      case  COM_4:   fResult  =  HW_DEF_COM_4_DSR; break;
      #endif

      #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_5:   fResult  =  HW_DEF_COM_5_DSR; break;
      case  COM_6:   fResult  =  HW_DEF_COM_6_DSR; break;
      #endif   // ]

      #if   defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_7:   fResult  =  HW_DEF_COM_7_DSR; break;
      case  COM_8:   fResult  =  HW_DEF_COM_8_DSR; break;
      #endif   // ]
   }

   return( fResult);
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_GetCTS( byte Port)
/*----------------------------------------------------------------------------*/
{
static   bool     fResult;


   if( Port == COM_VOID)
      return( FALSE);

   fResult  =  FALSE;

   switch( Port)
   {
      case  COM_1:   fResult  =  HW_DEF_COM_1_CTS; break;
      case  COM_2:   fResult  =  HW_DEF_COM_2_CTS; break;
      #ifdef   __IO2560_H
      case  COM_3:   fResult  =  HW_DEF_COM_3_CTS; break;
      case  COM_4:   fResult  =  HW_DEF_COM_4_CTS; break;
      #endif

      #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_5:   fResult  =  HW_DEF_COM_5_CTS; break;
      case  COM_6:   fResult  =  HW_DEF_COM_6_CTS; break;
      #endif   // ]

      #if   defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_7:   fResult  =  HW_DEF_COM_7_CTS; break;
      case  COM_8:   fResult  =  HW_DEF_COM_8_CTS; break;
      #endif   // ]
   }

   return( fResult);
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_GetDCD( byte Port)
/*----------------------------------------------------------------------------*/
{
static   bool     fResult;


   fResult  =  FALSE;

   switch( Port)
   {
      case  COM_1:   fResult  =  HW_DEF_COM_1_DCD; break;
      case  COM_2:   fResult  =  HW_DEF_COM_2_DCD; break;
      #ifdef   __IO2560_H
      case  COM_3:   fResult  =  HW_DEF_COM_3_DCD; break;
      case  COM_4:   fResult  =  HW_DEF_COM_4_DCD; break;
      #endif

      #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_5:   fResult  =  HW_DEF_COM_5_DCD; break;
      case  COM_6:   fResult  =  HW_DEF_COM_6_DCD; break;
      #endif   // ]

      #if   defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_7:   fResult  =  HW_DEF_COM_7_DCD; break;
      case  COM_8:   fResult  =  HW_DEF_COM_8_DCD; break;
      #endif   // ]
   }

   return( fResult);
}



/*----------------------------------------------------------------------------*/
bool                          UartDRV_GetRI( byte Port)
/*----------------------------------------------------------------------------*/
{
static   bool     fResult;


   fResult  =  FALSE;

   switch( Port)
   {
      case  COM_1:   fResult  =  HW_DEF_COM_1_RI;  break;
      case  COM_2:   fResult  =  HW_DEF_COM_2_RI;  break;
      #ifdef   __IO2560_H
      case  COM_3:   fResult  =  HW_DEF_COM_3_RI;  break;
      case  COM_4:   fResult  =  HW_DEF_COM_4_RI;  break;
      #endif

      #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_5:   fResult  =  HW_DEF_COM_5_RI;  break;
      case  COM_6:   fResult  =  HW_DEF_COM_6_RI;  break;
      #endif   // ]

      #if   defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_7:   fResult  =  HW_DEF_COM_7_RI;  break;
      case  COM_8:   fResult  =  HW_DEF_COM_8_RI;  break;
      #endif   // ]
   }

   return( fResult);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static   void                 UartDRV_Exar_SetBaudRate( tUart  *pUart)
/*----------------------------------------------------------------------------*/
{
byte     UBRRxL;
byte     UBRRxH;
byte     IER_Value;  // DLM
byte     THR_Value;  // DLL


   switch( pUart->Com.BaudRate)
   {
      case  BAUD_RATE_1200    :
            UBRRxL      =  0xBF  ;  // 191
            UBRRxH      =  0x00  ;
            IER_Value   =  0x00  ;
            THR_Value   =  0xC0  ;
            break;

      case  BAUD_RATE_2400    :
            UBRRxL      =  0x5F  ;  // 95
            UBRRxH      =  0x00  ;
            IER_Value   =  0x00  ;
            THR_Value   =  0x60  ;
            break;

      case  BAUD_RATE_19200   :
            UBRRxL      =  0x0B  ;  // 11
            UBRRxH      =  0x00  ;
            IER_Value   =  0x00  ;
            THR_Value   =  0x0C  ;
            break;

      case  BAUD_RATE_38400   :
            UBRRxL      =  0x05  ;  // 5
            UBRRxH      =  0x00  ;
            IER_Value   =  0x00  ;
            THR_Value   =  0x06  ;
            break;

      case  BAUD_RATE_57600   :
            UBRRxL      =  0x03  ;  // 3
            UBRRxH      =  0x00  ;
            IER_Value   =  0x00  ;
            THR_Value   =  0x04  ;
            break;

      case  BAUD_RATE_115200  :
            UBRRxL      =  0x01  ;  // 1
            UBRRxH      =  0x00  ;
            IER_Value   =  0x00  ;
            THR_Value   =  0x02  ;
            break;

      case  BAUD_RATE_9600    :
      default                 :
            UBRRxL      =  0x17  ;  // 23
            UBRRxH      =  0x00  ;
            IER_Value   =  0x00  ;
            THR_Value   =  0x18  ;
            break;
   }

   switch( pUart->Com.Port)
   {
      case  COM_1:   // Internal COM 1
            UBRR0L            =  UBRRxL;
            UBRR0H            =  UBRRxH;
            break;

      case  COM_2:   // Internal COM 2
            UBRR1L            =  UBRRxL;
            UBRR1H            =  UBRRxH;
            break;

      #ifdef   _IO2560_H
      case  COM_3:   // Internal COM 3
            UBRR2L            =  UBRRxL;
            UBRR2H            =  UBRRxH;
            break;

      case  COM_4:   // Internal COM 4
            UBRR3L            =  UBRRxL;
            UBRR3H            =  UBRRxH;
            break;
      #endif

      #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_5:   // Exar A
            SET_BIT( HW_DEF_EXAR_LCRA, BIT_7);  // LCRx:7 must be high to set baud-rate
            HW_DEF_EXAR_IERA  =  IER_Value;
            HW_DEF_EXAR_THRA  =  THR_Value;
            CLEAR_BIT( HW_DEF_EXAR_LCRA, BIT_7);
            break;

      case  COM_6:   // Exar B
            SET_BIT( HW_DEF_EXAR_LCRB, BIT_7);  // LCRx:7 must be high to set baud-rate
            HW_DEF_EXAR_IERB  =  IER_Value;
            HW_DEF_EXAR_THRB  =  THR_Value;
            CLEAR_BIT( HW_DEF_EXAR_LCRB, BIT_7);
            break;
      #endif   // ]

      #if   defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_7:   // Exar C
            SET_BIT( HW_DEF_EXAR_LCRC, BIT_7);  // LCRx:7 must be high to set baud-rate
            HW_DEF_EXAR_IERC  =  IER_Value;
            HW_DEF_EXAR_THRC  =  THR_Value;
            CLEAR_BIT( HW_DEF_EXAR_LCRC, BIT_7);
            break;

      case  COM_8:   // Exar D
            SET_BIT( HW_DEF_EXAR_LCRD, BIT_7);  // LCRx:7 must be high to set baud-rate
            HW_DEF_EXAR_IERD  =  IER_Value;
            HW_DEF_EXAR_THRD  =  THR_Value;
            CLEAR_BIT( HW_DEF_EXAR_LCRD, BIT_7);
            break;
      #endif   // ]
  }
}



/*----------------------------------------------------------------------------*/
static   void                 UartDRV_Exar_SetBitsAndParity( tUart  *pUart)
/*----------------------------------------------------------------------------*/
{
byte     UCSRxC;     // CPU (ATmega)
byte     LCR_Value;  // Exar


   UCSRxC      =  0;
   LCR_Value   =  0;

   switch( pUart->Com.DataBits)
   {
      case  DATA_BITS_8 :  SET_BIT( UCSRxC, (BIT_1 | BIT_2));  SET_BIT( LCR_Value, (BIT_0 | BIT_1));  break;
      case  DATA_BITS_7 :  SET_BIT( UCSRxC, BIT_2);            SET_BIT( LCR_Value, BIT_1);            break;
      case  DATA_BITS_6 :  SET_BIT( UCSRxC, BIT_1);            SET_BIT( LCR_Value, BIT_0);            break;
      case  DATA_BITS_5 :                                                                             break;
   }

   switch( pUart->Com.Parity)
   {
      case  PARITY_NONE :                                                                             break;
      case  PARITY_ODD  :  SET_BIT( UCSRxC, (BIT_4 | BIT_5));  SET_BIT( LCR_Value, BIT_3);            break;
      case  PARITY_EVEN :  SET_BIT( UCSRxC, BIT_5);            SET_BIT( LCR_Value, (BIT_3 | BIT_4));  break;
   }

   switch( pUart->Com.StopBits)
   {
      case  STOP_BITS_1 :                                                                             break;
      case  STOP_BITS_2 :  SET_BIT( UCSRxC, BIT_3);            SET_BIT( LCR_Value, BIT_0);            break;
   }

   switch( pUart->Com.Port)
   {
      case  COM_1:   UCSR0C            =  UCSRxC;     break;   // Internal COM 1
      case  COM_2:   UCSR1C            =  UCSRxC;     break;   // Internal COM 2
      #ifdef   _IO2560_H
      case  COM_3:   UCSR2C            =  UCSRxC;     break;   // Internal COM 3
      case  COM_4:   UCSR3C            =  UCSRxC;     break;   // Internal COM 4
      #endif

      #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_5:   HW_DEF_EXAR_LCRA  =  LCR_Value;  break;   // Exar A
      case  COM_6:   HW_DEF_EXAR_LCRB  =  LCR_Value;  break;   // Exar B
      #endif   // ]

      #if   defined(__USES_EXAR_4_PORTS__)  // [
      case  COM_7:   HW_DEF_EXAR_LCRC  =  LCR_Value;  break;   // Exar C
      case  COM_8:   HW_DEF_EXAR_LCRD  =  LCR_Value;  break;   // Exar D
      #endif   // ]
  }
}





