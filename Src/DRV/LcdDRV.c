/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   bool                 LcdDRV_fLcdIsConnected;
static   bool                 LcdDRV_fReInit;
/*------------------------ Local Function Prototypes -------------------------*/
static   void             		LcdDRV_WriteState( byte State);
static   void             		LcdDRV_WriteData( char Data);
static   byte             		LcdDRV_Wait( void);
static   void             		LcdDRV_LongDelay( void);
static   void             		LcdDRV_Delay( void);
static   void             		LcdDRV_WriteToPort( char Data);
//static   void                 LcdDRV_SetCGRAM( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		LcdDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
   LcdDRV_fLcdIsConnected  =  TRUE;
   LcdDRV_fReInit          =  FALSE;

	LcdDRV_Delay();
	LcdDRV_ClearScreen();
	LcdDRV_LongDelay();
}



/*----------------------------------------------------------------------------*/
void                    		LcdDRV_SetBackLight( bool	Mode)
/*----------------------------------------------------------------------------*/
{
	switch( Mode)
	{
		case  ON:   HW_DEF_LCD_BACKLIGHT_ON;   break;
		case  OFF:  HW_DEF_LCD_BACKLIGHT_OFF;  break;
	}
}



/*----------------------------------------------------------------------------*/
void                    		LcdDRV_ClearScreen( void)
/*----------------------------------------------------------------------------*/
{
	//LcdDRV_WriteState( 0x28);  // 4 bits data (BIT_4)
	LcdDRV_WriteState( 0x38);  // 8 bits data (BIT_4)

	LcdDRV_WriteState( 0x0C);
	LcdDRV_WriteState( 0x01);
}



/*----------------------------------------------------------------------------*/
void                    		LcdDRV_WriteString( byte	Line, char	*pStr)
/*----------------------------------------------------------------------------*/
{
byte     Counter;


   if( LcdDRV_fReInit == TRUE)
      LcdDRV_Init();

   LcdDRV_SetCursorPosition( Line, 0);

   for( Counter = 0; (Counter < LCD_LINE_WIDTH) && (*pStr != 0); Counter ++, pStr ++)
      LcdDRV_WriteData( *pStr);
}



/*----------------------------------------------------------------------------*/
void                    		LcdDRV_WriteChar( byte	Line, byte	Offset, char	Char)
/*----------------------------------------------------------------------------*/
{
   if( LcdDRV_fReInit == TRUE)
      LcdDRV_Init();

   if( Offset >= LCD_LINE_WIDTH)
      Offset = (LCD_LINE_WIDTH - 1);

   switch( Line)
   {
      case  LINE1:   LcdDRV_WriteState( 0x80 + Offset);  break;
      case  LINE2:   LcdDRV_WriteState( 0xC0 + Offset);  break;
   }

   LcdDRV_WriteData( Char);
}



/*----------------------------------------------------------------------------*/
void                          LcdDRV_SetCursorPosition( byte Line, byte Offset)
/*----------------------------------------------------------------------------*/
{
   if( Offset >= LCD_LINE_WIDTH)
      Offset = (LCD_LINE_WIDTH - 1);

   switch( Line)
   {
      case  LINE1:   LcdDRV_WriteState( 0x80 + Offset);  break;
      case  LINE2:   LcdDRV_WriteState( 0xC0 + Offset);  break;
   }
}



/*----------------------------------------------------------------------------*/
void                          LcdDRV_SetCursorMode( byte Mode)
/*----------------------------------------------------------------------------*/
{
   switch( Mode)
   {
      case  CURSOR_MODE_BLINK:   LcdDRV_WriteState( 0x0F);  break;
      case  CURSOR_MODE_HIDE:
      default:                   LcdDRV_WriteState( 0x0C);  break;
   }
}



/*----------------------------------------------------------------------------*/
void                          LcdDRV_HideText( byte Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode == ON)   LcdDRV_WriteState( 0x08);
   else              LcdDRV_WriteState( 0x0C);
}



/*----------------------------------------------------------------------------*/
void                          LcdDRV_SetContrast( byte   Value)
/*----------------------------------------------------------------------------*/
{
   if( Value > 0x1F)
      Value =  0x1F;

   HW_DEF_LCD_SET_CONTRAST( Value);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static   void             		LcdDRV_WriteState( byte State)
/*----------------------------------------------------------------------------*/
{
	LcdDRV_Wait();

	HW_DEF_LCD_STATE;          // Line A0
	HW_DEF_LCD_WRITE;          // Line RW
	HW_DEF_LCD_FULL_OUTPUT;    //

	LcdDRV_WriteToPort( State);
	LcdDRV_Delay();
	HW_DEF_LCD_DISABLE;        // Line E
	LcdDRV_Delay();
	HW_DEF_LCD_ENABLE;         // Line E
	LcdDRV_Delay();

   /*
	LcdDRV_WriteToPort( State << 4);
	LcdDRV_Delay();
	HW_DEF_LCD_DISABLE;        // Line E
	LcdDRV_Delay();
	HW_DEF_LCD_ENABLE;         // Line E
	LcdDRV_Delay();
   */

	HW_DEF_LCD_LOW_OUTPUT;     //
}



/*----------------------------------------------------------------------------*/
static   void             		LcdDRV_WriteData( char Data)
/*----------------------------------------------------------------------------*/
{
	LcdDRV_Wait();

	// if( Data == '?')     Data = '!';
	// if( Data == '@')     Data = 'o';
	// if( Data == '_')     Data = '-';
	// if( Data == '[')     Data = '<';
	// if( Data == ']')     Data = '>';

	HW_DEF_LCD_DATA;           // Line A0
	HW_DEF_LCD_WRITE;          // Line RW
	HW_DEF_LCD_FULL_OUTPUT;    //

	LcdDRV_WriteToPort( Data);
	LcdDRV_Delay();
	HW_DEF_LCD_DISABLE;        // Line E
	LcdDRV_Delay();
	HW_DEF_LCD_ENABLE;         // Line E
	LcdDRV_Delay();

   /*
	LcdDRV_WriteToPort( Data << 4);
	LcdDRV_Delay();
	HW_DEF_LCD_DISABLE;        // Line E
	LcdDRV_Delay();
	HW_DEF_LCD_ENABLE;         // Line E
	LcdDRV_Delay();
   */

	HW_DEF_LCD_LOW_OUTPUT;     //
}



/*----------------------------------------------------------------------------*/
static   byte             		LcdDRV_Wait( void)
/*----------------------------------------------------------------------------*/
{
byte  Counter;
char  LcdAddr1;


   if( LcdDRV_fLcdIsConnected == FALSE)
      return( 0x80); // Top line, position 0

	Counter = 0;
	do
	{
		Counter ++;

		if( Counter >= 50)
		{
			LcdDRV_fLcdIsConnected = FALSE;
			return( 0x80); // Top line, position 0
		}

		HW_DEF_LCD_LOW_OUTPUT;     LcdDRV_Delay();   //
		HW_DEF_LCD_STATE;          LcdDRV_Delay();   // Line A0
		HW_DEF_LCD_READ;           LcdDRV_Delay();   // Line RW
		HW_DEF_LCD_DISABLE;        LcdDRV_Delay();   // Line E
		LcdAddr1 = HW_DEF_LCD_PORT_VALUE;            // Read  PINF
      HW_DEF_LCD_ENABLE;         LcdDRV_Delay();   // Line E

	}  while( LcdAddr1 & HW_DEF_LCD_LINE_D7);

   HW_DEF_LCD_ENABLE;            LcdDRV_Delay();   // Line E

	return( LcdAddr1);
}



/*----------------------------------------------------------------------------*/
static   void             		LcdDRV_LongDelay( void)
/*----------------------------------------------------------------------------*/
{
byte  i;


   for( i = 0; i < 5; i ++)
   {
      LcdDRV_Delay();
      _WDR();
   }
}



/*----------------------------------------------------------------------------*/
static   void             		LcdDRV_Delay( void)
/*----------------------------------------------------------------------------*/
{
  _NOP();  _NOP();  _NOP();  _NOP();  _NOP();  _NOP();  _NOP();  _NOP();
  _NOP();  _NOP();  _NOP();  _NOP();  _NOP();  _NOP();  _NOP();  _NOP();
}



/*----------------------------------------------------------------------------*/
static   void             		LcdDRV_WriteToPort( char Data)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_LCD_WRITE_VALUE( Data);
}


/*
#if 0
Program the LCD CGRAM (first 8 characters in characters table)
--------------------------------------------------------------

                      �����
                      �����
               �����  �����
               �����  �����
        �����  �����  �����
        �����  �����  �����
 �����  �����  �����  �����
 _____  _____  _____  _____ (cursor line)
#endif

#define  MAX_PATTERNS      4
CONST byte  Patterns[MAX_PATTERNS][8] =
            {
               {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x00},
               {0x00, 0x00, 0x00, 0x00, 0x1F, 0x1F, 0x1F, 0x00},
               {0x00, 0x00, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x00},
               {0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x00}
            };
*/
#if 0
/*----------------------------------------------------------------------------*/
static   void                 LcdDRV_SetCGRAM( void)
/*----------------------------------------------------------------------------*/
{
byte  PixelsLine;
byte  Char;


   for( Char = 0; Char < MAX_PATTERNS; Char ++)
   {
      for( PixelsLine = 0; PixelsLine < 8; PixelsLine ++)
      {
         LcdDRV_WriteState( 0x40 + (Char * 8) + PixelsLine);
         LcdDRV_WriteData( Patterns[Char][PixelsLine]);
         _WDR();
      }
   }
}
#endif

