/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
#ifdef   __USES_RTC_DS1302__  // RTC Dallas DS1302 [
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
/*------------------------ Local Function Prototypes -------------------------*/
static   void                 ClockDRV_Delay( byte Count);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		ClockDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
void                    		ClockDRV_WriteRTC( byte Address, byte Data)
/*----------------------------------------------------------------------------*/
{
byte     Idx;


   // pre-engage
   HW_DEF_RTC_RST_LO;
   HW_DEF_RTC_RST_OUT;

   HW_DEF_RTC_CLOCK_LO;
   HW_DEF_RTC_CLOCK_OUT;

   HW_DEF_RTC_DATA_LO;
   HW_DEF_RTC_DATA_OUT;

   ClockDRV_Delay( 4);

   SET_BIT( Address, BIT_7);  // bit 7 must be '1' always

   // engage device
   HW_DEF_RTC_RST_HI;

   ClockDRV_Delay( 1);

   for( Idx = 0; Idx < 8; Idx ++)
   {
      if( BIT_IS_SET( Address, BIT_0))
         HW_DEF_RTC_DATA_HI;
      else
         HW_DEF_RTC_DATA_LO;

      Address >>= 1;

      ClockDRV_Delay( 1);

      HW_DEF_RTC_CLOCK_HI;
      ClockDRV_Delay( 1);

      HW_DEF_RTC_CLOCK_LO;
      ClockDRV_Delay( 1);
   }

   for( Idx = 0; Idx < 8; Idx ++)
   {
      if( BIT_IS_SET( Data, BIT_0))
         HW_DEF_RTC_DATA_HI;
      else
         HW_DEF_RTC_DATA_LO;

      Data >>= 1;

      ClockDRV_Delay( 1);

      HW_DEF_RTC_CLOCK_HI;
      ClockDRV_Delay( 1);

      HW_DEF_RTC_CLOCK_LO;
      ClockDRV_Delay( 1);
   }

   HW_DEF_RTC_RST_LO;
   ClockDRV_Delay( 4);
}



/*----------------------------------------------------------------------------*/
byte                    		ClockDRV_ReadRTC( byte Address)
/*----------------------------------------------------------------------------*/
{
byte     Data;
byte     Mask;
byte     Idx;


   // pre-engage
   HW_DEF_RTC_RST_LO;
   HW_DEF_RTC_RST_OUT;

   HW_DEF_RTC_CLOCK_LO;
   HW_DEF_RTC_CLOCK_OUT;

   HW_DEF_RTC_DATA_LO;
   HW_DEF_RTC_DATA_OUT;

   ClockDRV_Delay( 4);

   SET_BIT( Address, BIT_7);  // bit 7 must be '1' always
   SET_BIT( Address, BIT_0);  // bit 0 = Read

   // engage device
   HW_DEF_RTC_RST_HI;

   ClockDRV_Delay( 1);

   for( Idx = 0; Idx < 8; Idx ++)
   {
      if( BIT_IS_SET( Address, BIT_0))
         HW_DEF_RTC_DATA_HI;
      else
         HW_DEF_RTC_DATA_LO;

      Address >>= 1;

      ClockDRV_Delay( 1);

      HW_DEF_RTC_CLOCK_HI;
      ClockDRV_Delay( 1);

      HW_DEF_RTC_CLOCK_LO;
      ClockDRV_Delay( 1);
   }

   Data  =  0;
   Mask  =  0x01;

   HW_DEF_RTC_DATA_IN;
   ClockDRV_Delay( 1);

   for( Idx = 0; Idx < 8; Idx ++)
   {
      if( HW_DEF_RTC_DATA_STATE != 0)
         SET_BIT( Data, Mask);

      Mask <<= 1;

      ClockDRV_Delay( 1);

      HW_DEF_RTC_CLOCK_HI;
      ClockDRV_Delay( 1);

      HW_DEF_RTC_CLOCK_LO;
      ClockDRV_Delay( 1);
   }

   HW_DEF_RTC_RST_LO;
   ClockDRV_Delay( 4);

   return( Data);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Delay( byte Count)
/*----------------------------------------------------------------------------*/
{
byte  i;


   for( i = 0; i < Count; i ++)
      _NOP();
}
#endif   // ]  __USES_RTC_DS1302__



#ifdef   __USES_RTC_DS1307__  // RTC Dallas DS1307 [
/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
/*------------------------ Local Function Prototypes -------------------------*/
static   bool                 ClockDRV_ACK( void);
static   void                 ClockDRV_NACK( void);
static   void                 ClockDRV_Send( byte  Data);
static   byte                 ClockDRV_Get( void);
static   byte                 ClockDRV_ReadPort( void);
static   void                 ClockDRV_Start( void);
static   void                 ClockDRV_Stop( void);
static   void                 ClockDRV_ClockIn( void);
static   void                 ClockDRV_ClockOut( void);
static   void                 ClockDRV_DataIn( void);
static   void                 ClockDRV_DataOut( void);
static   void                 ClockDRV_ClockEnable( void);
static   void                 ClockDRV_ClockDisable( void);
static   void                 ClockDRV_DataEnable( void);
static   void                 ClockDRV_DataDisable( void);
static   void                 ClockDRV_Delay( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		ClockDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
void                    		ClockDRV_WriteRTC( byte Address, byte Data)
/*----------------------------------------------------------------------------*/
{
byte     Ack;


   do
   {
      ClockDRV_Start();
      ClockDRV_Send( HW_DEF_RTC_ADDRESS);
      Ack   =  ClockDRV_ACK();
      _WDR();
   }  while( Ack != FALSE);

   ClockDRV_Send( Address);
   Ack   =  ClockDRV_ACK();
   ClockDRV_Send( Data);
   Ack   =  ClockDRV_ACK();
   ClockDRV_Stop();
}



/*----------------------------------------------------------------------------*/
byte                    		ClockDRV_ReadRTC( byte Address)
/*----------------------------------------------------------------------------*/
{
byte     Ack;
byte     Data;


   do
   {
      ClockDRV_Start();
      ClockDRV_Send( HW_DEF_RTC_ADDRESS);
      Ack   =  ClockDRV_ACK();
      _WDR();
   }  while( Ack != FALSE);

   ClockDRV_Send( Address);
   Ack   =  ClockDRV_ACK();
   ClockDRV_Start();
   ClockDRV_Send( HW_DEF_RTC_ADDRESS + 1);
   Ack   =  ClockDRV_ACK();
   Data  =  ClockDRV_Get();
   ClockDRV_NACK();
   ClockDRV_Stop();

   return( Data);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static   bool                 ClockDRV_ACK( void)
/*----------------------------------------------------------------------------*/
{
usint    i;


   ClockDRV_DataIn();
   ClockDRV_ClockEnable();

   for( i = 0; i < 100; i ++)
   {
      if( ClockDRV_ReadPort() == 0)
      {
         ClockDRV_ClockDisable();
         ClockDRV_DataDisable();
         ClockDRV_DataOut();
         return( FALSE);
      }

      _WDR();
   }

   ClockDRV_ClockDisable();
   ClockDRV_DataDisable();
   ClockDRV_DataOut();
   return( TRUE);
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_NACK( void)
/*----------------------------------------------------------------------------*/
{
   ClockDRV_DataEnable();
   ClockDRV_ClockEnable();
   ClockDRV_ClockDisable();
   ClockDRV_DataDisable();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Send( byte  Data)
/*----------------------------------------------------------------------------*/
{
byte     i;


   for( i = 0; i < 8; i ++)
   {
      if( Data & 0x80)
         ClockDRV_DataEnable();
      else
         ClockDRV_DataDisable();

      ClockDRV_ClockEnable();
      ClockDRV_ClockDisable();
      Data <<= 1;
      _WDR();
   }
}



/*----------------------------------------------------------------------------*/
static   byte                 ClockDRV_Get( void)
/*----------------------------------------------------------------------------*/
{
byte     i;
byte     Data;


   Data  =  0;
   ClockDRV_DataIn();

   for( i = 0; i < 8; i ++)
   {
      ClockDRV_ClockEnable();
      Data <<= 1;

      if( ClockDRV_ReadPort() != 0)
         SET_BIT( Data, BIT_0);

      ClockDRV_ClockDisable();
      _WDR();
   }

   ClockDRV_DataOut();

   return( Data);
}



/*----------------------------------------------------------------------------*/
static   byte                 ClockDRV_ReadPort( void)
/*----------------------------------------------------------------------------*/
{
   ClockDRV_DataIn();
   return( HW_DEF_RTC_CLOCK_STATE); // See Inc\DRV\HW.def
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Start( void)
/*----------------------------------------------------------------------------*/
{
   ClockDRV_DataEnable();
   ClockDRV_ClockEnable();
   ClockDRV_DataOut();
   ClockDRV_ClockOut();

   ClockDRV_Delay();

   ClockDRV_DataEnable();
   ClockDRV_ClockEnable();
   ClockDRV_DataDisable();
   ClockDRV_ClockDisable();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Stop( void)
/*----------------------------------------------------------------------------*/
{
   ClockDRV_ClockEnable();
   ClockDRV_DataEnable();
   ClockDRV_ClockIn();
   ClockDRV_DataIn();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_ClockIn( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_CLOCK_IN; // See Inc\DRV\HW.def
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_ClockOut( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_CLOCK_OUT;// See Inc\DRV\HW.def
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_DataIn( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_DATA_IN;  // See Inc\DRV\HW.def
   ClockDRV_DataEnable();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_DataOut( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_DATA_OUT; // See Inc\DRV\HW.def
   ClockDRV_DataDisable();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_ClockEnable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_CLOCK_ENA;// See Inc\DRV\HW.def
   ClockDRV_Delay();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_ClockDisable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_CLOCK_DIS;// See Inc\DRV\HW.def
   ClockDRV_Delay();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_DataEnable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_DATA_ENA; // See Inc\DRV\HW.def
   ClockDRV_Delay();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_DataDisable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_DATA_DIS; // See Inc\DRV\HW.def
   ClockDRV_Delay();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Delay( void)
/*----------------------------------------------------------------------------*/
{
byte  i;


   //for( i = 0; i < 8; i ++)
   for( i = 0; i < 80; i ++)
      _NOP();
}
#endif   // ]  __USES_RTC_DS1307__



#ifdef   __USES_RTC_PCF8583__ // Phillips PCF8583  [
/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
/*------------------------ Local Function Prototypes -------------------------*/
static   bool                 ClockDRV_ACK( void);
static   void                 ClockDRV_NACK( void);
static   void                 ClockDRV_Send( byte  Data);
static   byte                 ClockDRV_Get( void);
static   byte                 ClockDRV_ReadPort( void);
static   void                 ClockDRV_Start( void);
static   void                 ClockDRV_Stop( void);
static   void                 ClockDRV_ClockIn( void);
static   void                 ClockDRV_ClockOut( void);
static   void                 ClockDRV_DataIn( void);
static   void                 ClockDRV_DataOut( void);
static   void                 ClockDRV_ClockEnable( void);
static   void                 ClockDRV_ClockDisable( void);
static   void                 ClockDRV_DataEnable( void);
static   void                 ClockDRV_DataDisable( void);
static   void                 ClockDRV_Delay( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		ClockDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
void                    		ClockDRV_WriteRTC( byte Address, byte Data)
/*----------------------------------------------------------------------------*/
{
byte     Ack;


   do
   {
      ClockDRV_Start();
      ClockDRV_Send( 0xA0);
      Ack   =  ClockDRV_ACK();
      _WDR();
   }  while( Ack != FALSE);

   ClockDRV_Send( Address);
   Ack   =  ClockDRV_ACK();
   ClockDRV_Send( Data);
   Ack   =  ClockDRV_ACK();
   ClockDRV_Stop();
}



/*----------------------------------------------------------------------------*/
byte                    		ClockDRV_ReadRTC( byte Address)
/*----------------------------------------------------------------------------*/
{
byte     Ack;
byte     Data;


   do
   {
      ClockDRV_Start();
      ClockDRV_Send( 0xA0);
      Ack   =  ClockDRV_ACK();
      _WDR();
   }  while( Ack != FALSE);

   ClockDRV_Send( Address);
   Ack   =  ClockDRV_ACK();
   ClockDRV_Start();
   ClockDRV_Send( 0xA1);
   Ack   =  ClockDRV_ACK();
   Data  =  ClockDRV_Get();
   ClockDRV_NACK();
   ClockDRV_Stop();

   return( Data);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static   bool                 ClockDRV_ACK( void)
/*----------------------------------------------------------------------------*/
{
usint    i;


   ClockDRV_DataIn();
   ClockDRV_ClockEnable();

   for( i = 0; i < 100; i ++)
   {
      if( ClockDRV_ReadPort() == 0)
      {
         ClockDRV_ClockDisable();
         ClockDRV_DataDisable();
         ClockDRV_DataOut();
         return( FALSE);
      }

      _WDR();
   }

   ClockDRV_ClockDisable();
   ClockDRV_DataDisable();
   ClockDRV_DataOut();
   return( TRUE);
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_NACK( void)
/*----------------------------------------------------------------------------*/
{
   ClockDRV_DataEnable();
   ClockDRV_ClockEnable();
   ClockDRV_ClockDisable();
   ClockDRV_DataDisable();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Send( byte  Data)
/*----------------------------------------------------------------------------*/
{
byte     i;


   for( i = 0; i < 8; i ++)
   {
      if( Data & 0x80)
         ClockDRV_DataEnable();
      else
         ClockDRV_DataDisable();

      ClockDRV_ClockEnable();
      ClockDRV_ClockDisable();
      Data <<= 1;
      _WDR();
   }
}



/*----------------------------------------------------------------------------*/
static   byte                 ClockDRV_Get( void)
/*----------------------------------------------------------------------------*/
{
byte     i;
byte     Data;


   Data  =  0;
   ClockDRV_DataIn();

   for( i = 0; i < 8; i ++)
   {
      ClockDRV_ClockEnable();
      Data <<= 1;

      if( ClockDRV_ReadPort() != 0)
         SET_BIT( Data, BIT_0);

      ClockDRV_ClockDisable();
      _WDR();
   }

   ClockDRV_DataOut();

   return( Data);
}



/*----------------------------------------------------------------------------*/
static   byte                 ClockDRV_ReadPort( void)
/*----------------------------------------------------------------------------*/
{
   ClockDRV_DataIn();
   return( HW_DEF_RTC_CLOCK_STATE); // See Inc\DRV\HW.def
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Start( void)
/*----------------------------------------------------------------------------*/
{
   ClockDRV_DataEnable();
   ClockDRV_ClockEnable();
   ClockDRV_DataOut();
   ClockDRV_ClockOut();

   ClockDRV_Delay();

   ClockDRV_DataEnable();
   ClockDRV_ClockEnable();
   ClockDRV_DataDisable();
   ClockDRV_ClockDisable();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Stop( void)
/*----------------------------------------------------------------------------*/
{
   ClockDRV_ClockEnable();
   ClockDRV_DataEnable();
   ClockDRV_ClockIn();
   ClockDRV_DataIn();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_ClockIn( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_CLOCK_IN; // See Inc\DRV\HW.def
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_ClockOut( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_CLOCK_OUT;// See Inc\DRV\HW.def
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_DataIn( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_DATA_IN;  // See Inc\DRV\HW.def
   ClockDRV_DataEnable();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_DataOut( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_DATA_OUT; // See Inc\DRV\HW.def
   ClockDRV_DataDisable();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_ClockEnable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_CLOCK_ENA;// See Inc\DRV\HW.def
   ClockDRV_Delay();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_ClockDisable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_CLOCK_DIS;// See Inc\DRV\HW.def
   ClockDRV_Delay();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_DataEnable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_DATA_ENA; // See Inc\DRV\HW.def
   ClockDRV_Delay();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_DataDisable( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_RTC_DATA_DIS; // See Inc\DRV\HW.def
   ClockDRV_Delay();
}



/*----------------------------------------------------------------------------*/
static   void                 ClockDRV_Delay( void)
/*----------------------------------------------------------------------------*/
{
byte  i;


   for( i = 0; i < 8; i ++)
      _NOP();
}
#endif   // ]  __USES_RTC_PCF8583__

