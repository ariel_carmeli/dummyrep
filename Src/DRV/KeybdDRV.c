/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   byte                 drv_Key;
/*------------------------ Local Function Prototypes -------------------------*/
static   void                 KeyboardDRV_Delay( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



 /*----------------------------------------------------------------------------*/
void                    		KeyboardDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
   drv_Key  =  KEY_NO_KEY;
}



/*----------------------------------------------------------------------------*/
void                    		KeyboardDRV_Scan( void)
/*----------------------------------------------------------------------------*/
{
bool           fKeyWasPressed;
static   byte  ScanResult     =  0;
static   bool  fKeyIsReleased =  FALSE;


   HW_DEF_KBD_SELECT_ALL_ROWS;   KeyboardDRV_Delay();
   ScanResult  =  HW_DEF_KBD_READ_ALL_COLS;
   HW_DEF_KBD_CLEAR_ALL_ROWS;

   fKeyWasPressed =  FALSE;
   if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_1))  fKeyWasPressed =  TRUE;
   if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_2))  fKeyWasPressed =  TRUE;
   if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_3))  fKeyWasPressed =  TRUE;
   if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_4))  fKeyWasPressed =  TRUE;

   if( fKeyWasPressed == FALSE)
   {
      fKeyIsReleased =  TRUE;
      drv_Key        =  KEY_NO_KEY;
      return;
   }

   drv_Key = KEY_NO_KEY; // don't delete this command !!!

   if( fKeyIsReleased == TRUE)
   {
      fKeyIsReleased =  FALSE;

      HW_DEF_KBD_SELECT_ROW_1;   KeyboardDRV_Delay();
      ScanResult  =  HW_DEF_KBD_READ_ALL_COLS;
      HW_DEF_KBD_CLEAR_ROW_1;
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_1))  drv_Key  =  KEY_1;      // 1
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_2))  drv_Key  =  KEY_2;      // 2
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_3))  drv_Key  =  KEY_3;      // 3
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_4))  drv_Key  =  KEY_F1;     // A
      if( drv_Key != KEY_NO_KEY)
         return;

      HW_DEF_KBD_SELECT_ROW_2;   KeyboardDRV_Delay();
      ScanResult  =  HW_DEF_KBD_READ_ALL_COLS;
      HW_DEF_KBD_CLEAR_ROW_2;
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_1))  drv_Key  =  KEY_4;      // 4
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_2))  drv_Key  =  KEY_5;      // 5
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_3))  drv_Key  =  KEY_6;      // 6
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_4))  drv_Key  =  KEY_F2;     // B
      if( drv_Key != KEY_NO_KEY)
         return;

      HW_DEF_KBD_SELECT_ROW_3;   KeyboardDRV_Delay();
      ScanResult  =  HW_DEF_KBD_READ_ALL_COLS;
      HW_DEF_KBD_CLEAR_ROW_3;
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_1))  drv_Key  =  KEY_7;      // 7
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_2))  drv_Key  =  KEY_8;      // 8
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_3))  drv_Key  =  KEY_9;      // 9
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_4))  drv_Key  =  KEY_F3;     // C
      if( drv_Key != KEY_NO_KEY)
         return;

      HW_DEF_KBD_SELECT_ROW_4;   KeyboardDRV_Delay();
      ScanResult  =  HW_DEF_KBD_READ_ALL_COLS;
      HW_DEF_KBD_CLEAR_ROW_4;
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_1))  drv_Key  =  KEY_ESC;    // *
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_2))  drv_Key  =  KEY_0;      // 0
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_3))  drv_Key  =  KEY_ENTER;  // #
      if( BIT_IS_CLEAR( ScanResult, U14_BIT_KBD_COL_4))  drv_Key  =  KEY_F4;     // D
      if( drv_Key != KEY_NO_KEY)
         return;
   }
}



/*----------------------------------------------------------------------------*/
byte                    		KeyboardDRV_GetKey( void)
/*----------------------------------------------------------------------------*/
{
   return( drv_Key);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static   void                 KeyboardDRV_Delay( void)
/*----------------------------------------------------------------------------*/
{
   _NOP();  _NOP();  _NOP();  _NOP();
   _NOP();  _NOP();  _NOP();  _NOP();
}

