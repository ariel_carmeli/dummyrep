/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   byte                 drv_BitMap_GroupA;
static   byte                 drv_BitMap_GroupB;
static   byte                 drv_BitMap_GroupC;
static   byte                 drv_BitMap_GroupD;
static   byte                 drv_BitMap_GroupE;
/*------------------------ Local Function Prototypes -------------------------*/
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		InOutDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
void                    		InOutDRV_Main( void)
/*----------------------------------------------------------------------------*/
{
   // Group A -   Exar port A
   drv_BitMap_GroupA =  0;
   if( HW_DEF_INPUT_CTS_A)          SET_BIT  ( drv_BitMap_GroupA, BIT_0);
   if( HW_DEF_INPUT_RI_A )          SET_BIT  ( drv_BitMap_GroupA, BIT_1);
   if( HW_DEF_INPUT_DCD_A)          SET_BIT  ( drv_BitMap_GroupA, BIT_2);
   if( HW_DEF_INPUT_DSR_A)          SET_BIT  ( drv_BitMap_GroupA, BIT_3);

   // Group B -   Exar port B
   drv_BitMap_GroupB =  0;
   if( HW_DEF_INPUT_CTS_B)          SET_BIT  ( drv_BitMap_GroupB, BIT_0);
   if( HW_DEF_INPUT_RI_B )          SET_BIT  ( drv_BitMap_GroupB, BIT_1);
   if( HW_DEF_INPUT_DCD_B)          SET_BIT  ( drv_BitMap_GroupB, BIT_2);
   if( HW_DEF_INPUT_DSR_B)          SET_BIT  ( drv_BitMap_GroupB, BIT_3);

   // Group C -   Exar port C
   drv_BitMap_GroupC =  0;
   if( HW_DEF_INPUT_CTS_C)          SET_BIT  ( drv_BitMap_GroupC, BIT_0);
   if( HW_DEF_INPUT_RI_C )          SET_BIT  ( drv_BitMap_GroupC, BIT_1);
   if( HW_DEF_INPUT_DCD_C)          SET_BIT  ( drv_BitMap_GroupC, BIT_2);
   if( HW_DEF_INPUT_DSR_C)          SET_BIT  ( drv_BitMap_GroupC, BIT_3);

   // Group D -   Exar port D
   drv_BitMap_GroupD =  0;
   if( HW_DEF_INPUT_CTS_D)          SET_BIT  ( drv_BitMap_GroupD, BIT_0);
   if( HW_DEF_INPUT_RI_D )          SET_BIT  ( drv_BitMap_GroupD, BIT_1);
   if( HW_DEF_INPUT_DCD_D)          SET_BIT  ( drv_BitMap_GroupD, BIT_2);
   if( HW_DEF_INPUT_DSR_D)          SET_BIT  ( drv_BitMap_GroupD, BIT_3);

   // Group E - Latch U18 (PORTE 4-7 + PORTF 4-7)
   drv_BitMap_GroupE =  0;
   if( BIT_IS_CLEAR( PINF, BIT_7))  SET_BIT  ( drv_BitMap_GroupE, BIT_0);
   if( BIT_IS_CLEAR( PINE, BIT_7))  SET_BIT  ( drv_BitMap_GroupE, BIT_1);
   if( BIT_IS_CLEAR( PINF, BIT_6))  SET_BIT  ( drv_BitMap_GroupE, BIT_2);
   if( BIT_IS_CLEAR( PINE, BIT_6))  SET_BIT  ( drv_BitMap_GroupE, BIT_3);
   if( BIT_IS_CLEAR( PINF, BIT_4))  SET_BIT  ( drv_BitMap_GroupE, BIT_4);
   if( BIT_IS_CLEAR( PINE, BIT_4))  SET_BIT  ( drv_BitMap_GroupE, BIT_5);
   if( BIT_IS_CLEAR( PINF, BIT_5))  SET_BIT  ( drv_BitMap_GroupE, BIT_6);
   if( BIT_IS_CLEAR( PINE, BIT_5))  SET_BIT  ( drv_BitMap_GroupE, BIT_7);

   #ifdef   HW_DEF_SWAP_IO_VALUES
   drv_BitMap_GroupA =  ~drv_BitMap_GroupA;
   drv_BitMap_GroupB =  ~drv_BitMap_GroupB;
   drv_BitMap_GroupC =  ~drv_BitMap_GroupC;
   drv_BitMap_GroupD =  ~drv_BitMap_GroupD;
   drv_BitMap_GroupE =  ~drv_BitMap_GroupE;
   #endif
}



/*----------------------------------------------------------------------------*/
byte                    		InOutDRV_GetStatus( byte  Group)
/*----------------------------------------------------------------------------*/
{
   switch( Group)
   {
      case  IO_GROUP_A  :  return( drv_BitMap_GroupA);
      case  IO_GROUP_B  :  return( drv_BitMap_GroupB);
      case  IO_GROUP_C  :  return( drv_BitMap_GroupC);
      case  IO_GROUP_D  :  return( drv_BitMap_GroupD);
      case  IO_GROUP_E  :  return( drv_BitMap_GroupE);
      default           :  return( 0);
   }
}



/*----------------------------------------------------------------------------*/
void                    		InOutDRV_SetStatus( byte   Output, bool   Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode != OFF)
      Mode  =  ON;   // like 'ON - if not OFF'

   switch( Output)
   {
   }
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



