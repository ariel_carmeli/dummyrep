/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
/*------------------------ Local Function Prototypes -------------------------*/
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		HopperDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
void                    		HopperDRV_SetMode( byte Hopper, bool   Mode)
/*----------------------------------------------------------------------------*/
{
   switch( Hopper)
   {
      case  HOPPER_A:   if( Mode == ON)   HW_DEF_HOPPER_1_PWR_ON; else  HW_DEF_HOPPER_1_PWR_OFF;   break;
      case  HOPPER_B:   if( Mode == ON)   HW_DEF_HOPPER_2_PWR_ON; else  HW_DEF_HOPPER_2_PWR_OFF;   break;
      case  HOPPER_C:   if( Mode == ON)   HW_DEF_HOPPER_3_PWR_ON; else  HW_DEF_HOPPER_3_PWR_OFF;   break;
      case  HOPPER_D:   if( Mode == ON)   HW_DEF_HOPPER_4_PWR_ON; else  HW_DEF_HOPPER_4_PWR_OFF;   break;
   }
}



/*----------------------------------------------------------------------------*/
bool                    		HopperDRV_GetStatus_LowLevel( byte Hopper)
/*----------------------------------------------------------------------------*/
{
bool  ReturnValue;


   ReturnValue =  FALSE;

   switch( Hopper)
   {
      case  HOPPER_A:   if( HW_DEF_HOPPER_1_LOW_LEVEL)   ReturnValue =  TRUE; break;
      case  HOPPER_B:   if( HW_DEF_HOPPER_2_LOW_LEVEL)   ReturnValue =  TRUE; break;
      case  HOPPER_C:   if( HW_DEF_HOPPER_3_LOW_LEVEL)   ReturnValue =  TRUE; break;
      case  HOPPER_D:   if( HW_DEF_HOPPER_4_LOW_LEVEL)   ReturnValue =  TRUE; break;
   }

   return( ReturnValue);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

