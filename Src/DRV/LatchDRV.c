/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   byte                 drv_Mask_U12 ; // Outputs  (Keyboard)
static   byte                 drv_Mask_U13 ; // Outputs  (Keyboard)
static   byte                 drv_Mask_U15 ; // Outputs  (LCD)
static   byte                 drv_Mask_U17 ; // Outputs  (Buzzer, Relays / Transistors, LCD Backlight)
/*------------------------ Local Function Prototypes -------------------------*/
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		LatchDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
   HW_DEF_LATCHES_ENA;  // Set low OC line (Enable all latches)

   drv_Mask_U15      =  0;
   HW_DEF_LATCH_U15  =  drv_Mask_U15;  // Write Latch U15

   drv_Mask_U17      =  0;
   HW_DEF_LATCH_U17  =  drv_Mask_U17;  // Write Latch U17
}



/*----------------------------------------------------------------------------*/
void                    		LatchDRV_Main( void)
/*----------------------------------------------------------------------------*/
{
static   byte  Counter  =  0;


   if( (Counter ++) == 0)
   {
      Counter =  0xFF;
      HW_DEF_LATCH_U12  =  drv_Mask_U12;  // Write Latch U12
      HW_DEF_LATCH_U13  =  drv_Mask_U13;  // Write Latch U13
      HW_DEF_LATCH_U15  =  drv_Mask_U15;  // Write Latch U15
      HW_DEF_LATCH_U17  =  drv_Mask_U17;  // Write Latch U17
   }
}



/*----------------------------------------------------------------------------*/
void                          LatchDRV_SetBit( tLatch Latch, byte Bit)
/*----------------------------------------------------------------------------*/
{
   switch( Latch)
   {
      case  LATCH_U12:
            SET_BIT( drv_Mask_U12, Bit);
            HW_DEF_LATCH_U12  =  drv_Mask_U12;  // Write Latch U12
            break;

      case  LATCH_U13:
            SET_BIT( drv_Mask_U13, Bit);
            HW_DEF_LATCH_U13  =  drv_Mask_U13;  // Write Latch U13
            break;

      case  LATCH_U15:
            SET_BIT( drv_Mask_U15, Bit);
            HW_DEF_LATCH_U15  =  drv_Mask_U15;  // Write Latch U15
            break;

      case  LATCH_U17:
            SET_BIT( drv_Mask_U17, Bit);
            HW_DEF_LATCH_U17  =  drv_Mask_U17;  // Write Latch U17
            break;
   }
}



/*----------------------------------------------------------------------------*/
void                          LatchDRV_ClearBit( tLatch  Latch, byte Bit)
/*----------------------------------------------------------------------------*/
{
   switch( Latch)
   {
      case  LATCH_U12:
            CLEAR_BIT( drv_Mask_U12, Bit);
            HW_DEF_LATCH_U12  =  drv_Mask_U12;  // Write Latch U12
            break;

      case  LATCH_U13:
            CLEAR_BIT( drv_Mask_U13, Bit);
            HW_DEF_LATCH_U13  =  drv_Mask_U13;  // Write Latch U13
            break;

      case  LATCH_U15:
            CLEAR_BIT( drv_Mask_U15, Bit);
            HW_DEF_LATCH_U15  =  drv_Mask_U15;  // Write Latch U15
            break;

      case  LATCH_U17:
            CLEAR_BIT( drv_Mask_U17, Bit);
            HW_DEF_LATCH_U17  =  drv_Mask_U17;  // Write Latch U17
            break;
   }
}



/*----------------------------------------------------------------------------*/
void                          LatchDRV_SetValue( tLatch Latch, byte Value)
/*----------------------------------------------------------------------------*/
{
   switch( Latch)
   {
      case  LATCH_U16:
            HW_DEF_LATCH_U16  =  Value;
            break;
   }
}



/*----------------------------------------------------------------------------*/
byte                          LatchDRV_GetValue( tLatch Latch)
/*----------------------------------------------------------------------------*/
{
byte     ReturnValue;


   ReturnValue =  0;

   switch( Latch)
   {
      case  LATCH_U14:
            ReturnValue =  HW_DEF_LATCH_U14;
            break;

      case  LATCH_U18:
            if( BIT_IS_SET  ( PINE, BIT_7))  SET_BIT( ReturnValue, U18_BIT_HOPPER_4_LOW   )  ;  // BIT_0 (U18:1)
            if( BIT_IS_CLEAR( PINF, BIT_7))  SET_BIT( ReturnValue, U18_BIT_HOPPER_4_SENSE )  ;  // BIT_1 (U18:0)
            if( BIT_IS_SET  ( PINF, BIT_6))  SET_BIT( ReturnValue, U18_BIT_HOPPER_3_LOW   )  ;  // BIT_2 (U18:2)
            if( BIT_IS_CLEAR( PINE, BIT_6))  SET_BIT( ReturnValue, U18_BIT_HOPPER_3_SENSE )  ;  // BIT_3 (U18:3)
            if( BIT_IS_SET  ( PINF, BIT_4))  SET_BIT( ReturnValue, U18_BIT_HOPPER_1_LOW   )  ;  // BIT_4 (U18:4)
            if( BIT_IS_CLEAR( PINE, BIT_4))  SET_BIT( ReturnValue, U18_BIT_HOPPER_1_SENSE )  ;  // BIT_5 (U18:5)
            if( BIT_IS_SET  ( PINF, BIT_5))  SET_BIT( ReturnValue, U18_BIT_HOPPER_2_LOW   )  ;  // BIT_6 (U18:6)
            if( BIT_IS_CLEAR( PINE, BIT_5))  SET_BIT( ReturnValue, U18_BIT_HOPPER_2_SENSE )  ;  // BIT_7 (U18:7)
            break;
   }

   return( ReturnValue);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

