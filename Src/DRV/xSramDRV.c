/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   byte           drv_LastDevice;
static   byte           drv_LastPage;
/*------------------------ Local Function Prototypes -------------------------*/
static   void                 xSramDRV_SelectDevice( byte Device);
static   void                 xSramDRV_SelectPage( byte Page);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		xSramDRV_Init( void)
/*----------------------------------------------------------------------------*/
{
   drv_LastDevice    =  0xFF;
   drv_LastPage      =  0xFF;
}



/*----------------------------------------------------------------------------*/
void                    		xSramDRV_Write( byte Device, byte Page, usint Address, byte Data)
/*----------------------------------------------------------------------------*/
{
   if( drv_LastDevice != Device)
   {
      drv_LastDevice =  Device;
      xSramDRV_SelectDevice( Device);
   }

   if( drv_LastPage  != Page)
   {
      drv_LastPage   =  Page;
      xSramDRV_SelectPage( Page);
   }

   *(byte *)((usint)(HW_DEF_xSRAM_BASE_ADDRESS + Address)) =  Data;
}



/*----------------------------------------------------------------------------*/
byte                    		xSramDRV_Read( byte Device, byte Page, usint Address)
/*----------------------------------------------------------------------------*/
{
   if( drv_LastDevice != Device)
   {
      drv_LastDevice =  Device;
      xSramDRV_SelectDevice( Device);
   }

   if( drv_LastPage  != Page)
   {
      drv_LastPage   =  Page;
      xSramDRV_SelectPage( Page);
   }

   return( *(byte *)((usint)(HW_DEF_xSRAM_BASE_ADDRESS + Address)));
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static   void                 xSramDRV_SelectDevice( byte Device)
/*----------------------------------------------------------------------------*/
{
byte  Mask;


   if( Device >= HW_DEF_xSRAM_MAX_DEVICES)
      Device   = (HW_DEF_xSRAM_MAX_DEVICES - 1);

   Mask  =  0;

   if( BIT_IS_CLEAR( Device, BIT_0))  SET_BIT( Mask, HW_DEF_xSRAM_DEVICE_B0); // BIT_7
   if( BIT_IS_CLEAR( Device, BIT_1))  SET_BIT( Mask, HW_DEF_xSRAM_DEVICE_B1); // BIT_6

   _CLI();  // Disable Global Interrupts
   CLEAR_BIT( HW_DEF_xSRAM_DEVICE_PORT, (HW_DEF_xSRAM_DEVICE_B0 | HW_DEF_xSRAM_DEVICE_B1));
   SET_BIT  ( HW_DEF_xSRAM_DEVICE_PORT, Mask);
   _SEI();  // Enable Global Interrupts
   _NOP();  _NOP();  _NOP();  _NOP();
}



/*----------------------------------------------------------------------------*/
static   void                 xSramDRV_SelectPage( byte Page)
/*----------------------------------------------------------------------------*/
{
byte  Mask;


   if( Page >= HW_DEF_xSRAM_MAX_PAGES_IN_DEVICE)
      Page = (HW_DEF_xSRAM_MAX_PAGES_IN_DEVICE - 1);

   Mask  =  0;

   if( BIT_IS_SET( Page, BIT_0))  SET_BIT( Mask, HW_DEF_xSRAM_PAGE_B0);  // BIT_7
   if( BIT_IS_SET( Page, BIT_1))  SET_BIT( Mask, HW_DEF_xSRAM_PAGE_B1);  // BIT_6
   if( BIT_IS_SET( Page, BIT_2))  SET_BIT( Mask, HW_DEF_xSRAM_PAGE_B2);  // BIT_5
   if( BIT_IS_SET( Page, BIT_3))  SET_BIT( Mask, HW_DEF_xSRAM_PAGE_B3);  // BIT_4

   _CLI();  // Disable Global Interrupts
   CLEAR_BIT( HW_DEF_xSRAM_PAGE_PORT, (HW_DEF_xSRAM_PAGE_B0 | HW_DEF_xSRAM_PAGE_B1 | HW_DEF_xSRAM_PAGE_B2 | HW_DEF_xSRAM_PAGE_B3));
   SET_BIT  ( HW_DEF_xSRAM_PAGE_PORT, Mask);
   _SEI();  // Enable Global Interrupts
   _NOP();  _NOP();  _NOP();  _NOP();
}







