/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define     MAX_DISPENSING_CYCLES   MAX_HOPPERS_DE_FACTO        /*MAX_HOPPERS*/
/*---------------------------- Typedef Directives ----------------------------*/
typedef  struct
{  /* NOTE !!! Each member MUST be from type of ulong (size of tParams_C_Rec) */
   ulong       CoinValue;
   ulong       MaxLevel;
   ulong       MinLevel;
}  tHopperParam;  // See T_Params.h | PARAM_C_PROD_xx_?????
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tHopperInfo         *task_pHopperInfo;
static   tHoppers_Info        task_Info;
static   tHopperStatus        task_Status;
static   tHopperParam         task_HopperParams;
static   tLogEvent_Rec        task_Log_Event_Rec;
static   tTimerMsec           task_TimerMsec;
static   tParams_A_Rec        task_Param_A_Rec;
static   tParams_C_Rec        task_Param_C_Rec;
static   ulong                task_ValueToDispense;
static   ulong                task_ValueDispensed;
static   ulong                task_ValueDispensed_Session;
static   usint                task_CoinValue[MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/];
static   usint                task_CoinsToDispense[MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/];
static   byte                 task_Cycle;
static   bool                 task_fPowerup;
static   bool                 task_fDispensing;
static   bool                 task_fSaveDB;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						Task_Hopper_HandleTimer( void);
static	void						Task_Hopper_ReadDB( void);
static	void						Task_Hopper_WriteDB( void);
static	void						Task_Hopper_HandleExtEvent( void);
static	void						Task_Hopper_HandleDispense( void);
static	void                 Task_Hopper_SetCoinValue( void);
static	void						Task_Hopper_Params_Read( byte Idx);
static	void						Task_Hopper_Params_Write( byte Idx);
//static	void						Task_Hopper_Log_Event( tLog_Event EvCode, ulong   Value, bool fValueIsPrice);
/*----------------------------------------------------------------------------*/
#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Hopper_Init( void)
/*----------------------------------------------------------------------------*/
{
static tHoppers_Setup       Hopper_API_Setup;

   task_pHopperInfo     =  HopperAPI_GetInfo();
   task_Status          =  HOPPER_STATUS_IDLE;
   task_Cycle           =  0;
   task_fPowerup        =  TRUE;

   // Init Hooper API library
   Hopper_API_Setup.api_SenseOut[HOPPER_A] = INPUT_U18_5;                                       //
   Hopper_API_Setup.api_SenseOut[HOPPER_B] = INPUT_U18_7;                                       //
   Hopper_API_Setup.api_SenseOut[HOPPER_C] = INPUT_U18_3;                                       //
   Hopper_API_Setup.api_SenseOut[HOPPER_D] = INPUT_U18_1;                                       //
   Hopper_API_Setup.api_SenseOut[HOPPER_E] = 0;
   Hopper_API_Setup.api_SenseOut[HOPPER_F] = 0;
   Hopper_API_Setup.func_HopperDRV_GetStatus_LowLevel = HopperDRV_GetStatus_LowLevel;           //
   Hopper_API_Setup.func_HopperDRV_Init = HopperDRV_Init;                                       //
   Hopper_API_Setup.func_HopperDRV_SetMode = HopperDRV_SetMode;
   Hopper_API_Setup.func_InOutAPI_GetStatus = (byte(*)(byte,bool))InOutAPI_GetStatus;
   Hopper_API_Setup.func_Task_Timer_GetTimer = Task_Timer_GetTimer;
   Hopper_API_Setup.HwDef_TMR0_TickInterval = HW_DEF_TMR0_TICK_INTERVAL;
   Hopper_API_Setup.io_status_closed = IO_STATUS_CLOSED;
   Hopper_API_Setup.MaximumNumOfHoppers = MAX_HOPPERS_DE_FACTO;
   
   HopperAPI_Init(&Hopper_API_Setup);
   Lib_Version_Hopper_1_0_0 = 0;   
}



/*----------------------------------------------------------------------------*/
void                    		Task_Hopper_Main( void)
/*----------------------------------------------------------------------------*/
{
	if( App_TaskStatus[TASK_HOPPER] == DISABLED)
		return;

   if( task_fPowerup == TRUE)
   {
      Task_Hopper_ReadDB();
      Task_Hopper_SetCoinValue();
   }

   Task_Hopper_HandleTimer();
   Task_Hopper_HandleExtEvent();
   Task_Hopper_HandleDispense();

   if( (task_fSaveDB == TRUE) && (task_TimerMsec == 0))
   {
      task_fSaveDB   =  FALSE;
      Task_Hopper_WriteDB();
   }

   task_fPowerup  =  FALSE;

   HopperAPI_Main();
}



/*----------------------------------------------------------------------------*/
tHopperStatus                 Task_Hopper_GetStatus( void)
/*----------------------------------------------------------------------------*/
{
   return( task_Status);
}



/*----------------------------------------------------------------------------*/
tHoppers_Info                *Task_Hopper_GetInfo( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_Info);
}



/*----------------------------------------------------------------------------*/
usint                         Task_Hopper_GetCoinsDispensed( tHopper Hopper)
/*----------------------------------------------------------------------------*/
{
   return( task_pHopperInfo[Hopper].CoinsDispensed);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Hopper_IsLowLevel( tHopper Hopper)
/*----------------------------------------------------------------------------*/
{
   return( task_pHopperInfo[Hopper].fLowLevel);
}



/*----------------------------------------------------------------------------*/
usint                         Task_Hopper_GetCoinValue( tHopper Hopper)
/*----------------------------------------------------------------------------*/
{
   return( task_CoinValue[Hopper]);
}



/*----------------------------------------------------------------------------*/
void                          Task_Hopper_AddCoinsToHopper( tHopper Hopper, usint   Count)
/*----------------------------------------------------------------------------*/
{
ulong    Value;


   Value =  Count;
   Value *= task_CoinValue[Hopper];

   task_Info.CoinsInTube[Hopper] += Count;
   task_Info.MoneyInTubes        += Value;
   task_Info.MoneyInTube[Hopper] += Value;

   task_TimerMsec =  Task_Timer_SetTimeMsec( 200);
   task_fSaveDB   =  TRUE;
}



/*----------------------------------------------------------------------------*/
ulong                         Task_Hopper_GetValueDispensed( void)
/*----------------------------------------------------------------------------*/
{
ulong    Value;


   Value =  task_ValueDispensed_Session;
   if( task_ValueDispensed_Session > 0)
      task_ValueDispensed_Session   =  0;

   return( Value);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Hopper_Dispense_Value( ulong   Value, bool fInternalCall)
/*----------------------------------------------------------------------------*/
{
ulong    ValueToDispense;
usint    CoinValue;
byte     Hopper;


   if( Value == 0)
   {
      task_ValueDispensed_Session   =  0;
      task_Status =  HOPPER_STATUS_DONE_OK;
      return;
   }

   memset( task_CoinsToDispense, 0, sizeof( task_CoinsToDispense));
   ValueToDispense   =  Value;

   if( task_Cycle == 0)
      task_ValueDispensed_Session   =  0;

   Hopper   =  MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/;
   do
   {
      Hopper   --;

      if( task_Info.CoinsInTube[Hopper] > 0)
      {
         CoinValue   =  task_CoinValue[Hopper];

         if( CoinValue > 0)
         {
            task_CoinsToDispense[Hopper]  =  (ValueToDispense / CoinValue);
            ValueToDispense   -= (ulong)((ulong)task_CoinsToDispense[Hopper] * (ulong)CoinValue);
         }
      }

      if( ValueToDispense == 0)
         break;
   }  while( Hopper > 0);

   task_ValueToDispense =  Value;
   task_ValueDispensed  =  0;
   task_fDispensing     =  TRUE;
   task_Status          =  HOPPER_STATUS_BUSY;

   Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, PARAM_A_HOPPER_TIMEOUT_DISPENSE);
   HopperAPI_SetTimeoutValue_Dispensing( task_Param_A_Rec);

   for( Hopper = 0; Hopper < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; Hopper ++)
      HopperAPI_Activate( (tHopper)Hopper, task_CoinsToDispense[Hopper]);

   // to be the best accurate with coins dispensing (with regards to input-lines handling [InOutAPI module])
   if( fInternalCall == FALSE)
   {
      Task_Hardware_Call_B2B( TRUE);
      do
      {
         Task_Hopper_Main();
         Task_Hardware_Main( TRUE);
      }  while( Task_Hopper_GetStatus() == HOPPER_STATUS_BUSY);
   }
}



/*----------------------------------------------------------------------------*/
void                    		Task_Hopper_Dispense_Count( tHopper Hopper, usint  Count, bool fInternalCall)
/*----------------------------------------------------------------------------*/
{
byte     Idx;


   if( (Hopper >= MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/) || (Count == 0))
      return;

   memset( task_CoinsToDispense, 0, sizeof( task_CoinsToDispense));

   for( Idx = 0; Idx < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; Idx ++)
      HopperAPI_Activate( (tHopper)Idx, 0);  // Reset counting values. See HopperAPI_Activate() -> api_Info[Hopper].CoinsDispensed

   task_ValueToDispense          =  0;
   task_ValueDispensed           =  0;
   task_CoinsToDispense[Hopper]  =  Count;
   task_fDispensing              =  TRUE;
   task_Status                   =  HOPPER_STATUS_BUSY;

   Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, PARAM_A_HOPPER_TIMEOUT_DISPENSE);
   HopperAPI_SetTimeoutValue_Dispensing( task_Param_A_Rec);

   HopperAPI_Activate( Hopper, Count);

   // to be the best accurate with coins dispensing (with regards to input-lines handling [InOutAPI module])
   if( fInternalCall == FALSE)
   {
      Task_Hardware_Call_B2B( TRUE);
      do
      {
         Task_Hopper_Main();
         Task_Hardware_Main( TRUE);
      }  while( Task_Hopper_GetStatus() == HOPPER_STATUS_BUSY);
   }
}



/*----------------------------------------------------------------------------*/
void                    		Task_Hopper_ResetInfo( void)
/*----------------------------------------------------------------------------*/
{
   memset( &task_Info, 0, sizeof( task_Info));
   task_fSaveDB   =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                                    Task_Hopper_SetxRam_CoinVal_Capacity( ulong coin_value, ulong capacity, byte hopper_num)
/*----------------------------------------------------------------------------*/
{
usint    Offset;

   task_CoinValue[hopper_num] = (usint)coin_value;

   Offset   =  hopper_num;
   Offset   *= (sizeof( task_HopperParams) / sizeof( task_Param_C_Rec));
   
   Task_Params_Write( &coin_value, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_HPR_01_VALUE      + Offset));
   Task_Params_Write( &capacity,                   PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_HPR_01_MAX_LEVEL  + Offset));
   
}


#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( task_TimerMsec   <  Gap)  task_TimerMsec =  0; else  task_TimerMsec -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_ReadDB( void)
/*----------------------------------------------------------------------------*/
{
   xSramAPI_Read( &task_Info, sizeof( task_Info), xSRAM_TYPE_HOPPERS_INFO, 0, TRUE);
   if( Checksum_Compare( &task_Info, sizeof( task_Info), task_Info.Checksum) == FALSE)
   {
      LcdAPI_ClearLine( LINE2);
      memset( &task_Info, 0, sizeof( task_Info));
      xSramAPI_Write( &task_Info, sizeof( task_Info), xSRAM_TYPE_HOPPERS_INFO, 0, TRUE);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_WriteDB( void)
/*----------------------------------------------------------------------------*/
{
   task_Info.Checksum   =  Checksum_Calc( &task_Info, sizeof( task_Info), task_Info.Checksum);
   xSramAPI_Write( &task_Info, sizeof( task_Info), xSRAM_TYPE_HOPPERS_INFO, 0, TRUE);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_HandleExtEvent( void)
/*----------------------------------------------------------------------------*/
{
   App_TaskEvent[TASK_HOPPER] =  EV_APP_VOID;

   if( App_TaskEvent[TASK_SERVER] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_READ_PARAMS))
         Task_Hopper_SetCoinValue();
   }

   if( App_TaskEvent[TASK_SETUP] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_CLEAR_SALES))
      {
         task_Info.MoneyRefilled =  0;
         task_Info.MoneyEmptied  =  0;
         task_fSaveDB            =  TRUE;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_RESET_INFO))
      {
         // 3'rd of 3 RESET steps
         memset( &task_Info, 0, sizeof( task_Info));
         task_fSaveDB   =  TRUE;
         
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_INACTIVE))
      {
         Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, PARAM_A_HOPPER_TIMEOUT_EMPTY);
         HopperAPI_SetTimeoutValue( task_Param_A_Rec);

         Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, PARAM_A_HOPPER_TIMEOUT_DISPENSE);
         HopperAPI_SetTimeoutValue_Dispensing( task_Param_A_Rec);
      }

      // Refill Hoppers
      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_STOP_REFILLING))
      {
      ulong    Value;
      byte     Idx;


         task_Info.MoneyInTubes  =  0;

         for( Idx = 0; Idx < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; Idx ++)
         {
            Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_REFILLED + Idx));
            Value =  (task_Param_A_Rec * (ulong)Task_Hopper_GetCoinValue( (tHopper)Idx));
            
            task_Info.MoneyRefilled    += Value;
            task_Info.MoneyInTube[Idx] += Value;
            task_Info.CoinsInTube[Idx] += task_Param_A_Rec;
            task_Info.MoneyInTubes     += task_Info.MoneyInTube[Idx];
            
            Task_Application_SetEvent_CoinHopperRefill(Idx+1, task_Param_A_Rec);
            
            task_Param_A_Rec   =  0;
            Task_Params_Write( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_REFILLED + Idx));

            
            //if( Value > 0)
            //   Task_Hopper_Log_Event( (tLog_Event)(LOG_EV_TUBES_REFILL_1 + Idx), Value, TRUE);   // Set event in Log file
         }

         task_fSaveDB   =  TRUE;
      }

      // Empty Hoppers
      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_COINS_EMPTYING))
      {
      ulong    Value;
      byte     Idx;

         for( Idx = 0; Idx < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; Idx ++)
         {
            Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_EMPTYING + Idx));
            Value =  (task_Param_A_Rec * (ulong)Task_Hopper_GetCoinValue( (tHopper)Idx));

            /* Info parameters were updated already in Task_Hopper_HandleDispense() function */

            task_Info.MoneyEmptied  += Value;

            task_Param_A_Rec   =  0;
            Task_Params_Write( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_EMPTYING + Idx));

            //if( Value > 0)
            //   Task_Hopper_Log_Event( (tLog_Event)(LOG_EV_TUBES_EMPTY_1 + Idx), Value, TRUE);   // Set event in Log file
         }

         task_fSaveDB   =  TRUE;
      }
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_HandleDispense( void)
/*----------------------------------------------------------------------------*/
{
byte     Hopper;


   if( task_fDispensing == FALSE)
      return;

   task_Status =  HOPPER_STATUS_IDLE;
   for( Hopper = 0; Hopper < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; Hopper ++)
   {
      if( task_pHopperInfo[Hopper].fBusy == TRUE)
         task_Status =  HOPPER_STATUS_BUSY;
   }

   // End of dispensing coins
   if( task_Status == HOPPER_STATUS_IDLE)
   {
      task_fDispensing  =  FALSE;

      task_fSaveDB            =  TRUE;
      task_ValueDispensed     =  0;
      task_Info.MoneyInTubes  =  0;

      for( Hopper = 0; Hopper < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; Hopper ++)
      {
         if( (task_CoinsToDispense[Hopper] > 0) && (task_pHopperInfo[Hopper].CoinsDispensed < task_CoinsToDispense[Hopper]))
         {
            task_Info.MoneyInTube[Hopper] =  0;
            task_Info.CoinsInTube[Hopper] =  0;
         }

         if( task_Info.CoinsInTube[Hopper] >= task_pHopperInfo[Hopper].CoinsDispensed)
            task_Info.CoinsInTube[Hopper] -= task_pHopperInfo[Hopper].CoinsDispensed;

         task_Info.MoneyInTube[Hopper] =  task_Info.CoinsInTube[Hopper];
         task_Info.MoneyInTube[Hopper] *= task_CoinValue[Hopper];

         task_Info.MoneyInTubes        += task_Info.MoneyInTube[Hopper];
         task_ValueDispensed           += (ulong)((ulong)task_pHopperInfo[Hopper].CoinsDispensed * (ulong)task_CoinValue[Hopper]);
      }

      task_ValueDispensed_Session   += task_ValueDispensed;

      if( task_ValueToDispense > 0)
      {
         if( task_ValueDispensed < task_ValueToDispense)
         {
            if( task_Cycle < MAX_DISPENSING_CYCLES)
            {
               task_Cycle  ++;
               Task_Hopper_Dispense_Value( task_ValueToDispense - task_ValueDispensed, TRUE);
               return;
            }
         }

         task_Status =  (task_ValueDispensed == task_ValueToDispense) ? HOPPER_STATUS_DONE_OK : HOPPER_STATUS_DONE_ERROR;
      }
      else
         task_Status =  HOPPER_STATUS_DONE_OK;

      task_Cycle  =  0;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_SetCoinValue( void)
/*----------------------------------------------------------------------------*/
{
byte     Idx;
      
   for( Idx = 0; Idx < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; Idx ++)
   {
      Task_Hopper_Params_Read( Idx);
      task_CoinValue[Idx]  =  task_HopperParams.CoinValue;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_Params_Read( byte Idx)
/*----------------------------------------------------------------------------*/
{
usint    Offset;

   Offset   =  Idx;
   Offset   *= (sizeof( task_HopperParams) / sizeof( task_Param_C_Rec));

   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_HPR_01_VALUE      + Offset)); task_HopperParams.CoinValue   =  task_Param_C_Rec;
      
   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_HPR_01_MAX_LEVEL  + Offset)); task_HopperParams.MaxLevel    =  task_Param_C_Rec;
   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_HPR_01_MIN_LEVEL  + Offset)); task_HopperParams.MinLevel    =  task_Param_C_Rec;
   
   
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_Params_Write( byte Idx)
/*----------------------------------------------------------------------------*/
{
usint    Offset;


   Offset   =  Idx;
   Offset   *= (sizeof( task_HopperParams) / sizeof( task_Param_C_Rec));


   task_Param_C_Rec  =  task_HopperParams.CoinValue;  Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_HPR_01_VALUE      + Offset));
   task_Param_C_Rec  =  task_HopperParams.MaxLevel ;  Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_HPR_01_MAX_LEVEL  + Offset));
   task_Param_C_Rec  =  task_HopperParams.MinLevel ;  Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_HPR_01_MIN_LEVEL  + Offset));
}



#if 0
/*----------------------------------------------------------------------------*/
static	void						Task_Hopper_Log_Event( tLog_Event EvCode, ulong   Value, bool fValueIsPrice)
/*----------------------------------------------------------------------------*/
{
   memset( &task_Log_Event_Rec      ,  0, sizeof( task_Log_Event_Rec));
   task_Log_Event_Rec.EvCode        =  EvCode;
   task_Log_Event_Rec.Value         =  Value;
   task_Log_Event_Rec.fValueIsPrice =  fValueIsPrice;
   Task_Log_Event_WriteRecord( &task_Log_Event_Rec);
}
#endif




