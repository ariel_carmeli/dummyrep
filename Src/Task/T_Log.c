/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  FLAG_INFO_WRITE         BIT_0
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   STATE_IDLE,
   STATE_WRITE_INFO,
   /******************/
   STATE_LAST
}  tState;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   bool                    task_fPowerup;

// Events
static   tLogEvent_Info          task_Event_Info;                 // 32
static   tLogEvent_Rec           task_Event_Rec_Write;            // 16
static   tLogEvent_Rec           task_Event_Rec_Read;             // 16
static   usint                   task_Event_ReadIndex;
static   byte                    task_Event_InfoFlags;
static   bool                    task_Event_fReadLastRec;

// Transactions
static   tLogTrans_Info          task_Trans_Info;                 // 32
static   tLogTrans_Rec           task_Trans_Rec_Write;            // 16
static   tLogTrans_Rec           task_Trans_Rec_Read;             // 16
static   usint                   task_Trans_ReadIndex;
static   byte                    task_Trans_InfoFlags;
static   bool                    task_Trans_fReadLastRec;

// Coin Routing
static   tLogCoinRouting_Info    task_CoinRouting_Info;           // 45
static   tLogCoinRouting_Rec     task_CoinRouting_Rec_Write;      // 11
static   tLogCoinRouting_Rec     task_CoinRouting_Rec_Read;       // 11
static   usint                   task_CoinRouting_ReadIndex;
static   byte                    task_CoinRouting_InfoFlags;
static   bool                    task_CoinRouting_fReadLastRec;

// Emergencies
static   tLogEmergency_Info      task_Emergency_Info;             // 32
static   byte                    task_Emergency_InfoFlags;

#if 0
// Z Records
static   tLogZ_Info              task_Z_Info;                     // 32
static   tLogZ_Rec               task_Z_Rec_Write;                // 32
static   tLogZ_Rec               task_Z_Rec_Read;                 // 32
static   usint                   task_Z_ReadIndex;
static   byte                    task_Z_InfoFlags;
static   bool                    task_Z_fReadLastRec;
#endif
/*------------------------ Local Function Prototypes -------------------------*/
static	void						Task_Log_HandleExtEvent       ( void);
static	void						Task_Log_Trans_HandleDB       ( void);
static	void						Task_Log_Event_HandleDB       ( void);
static	void						Task_Log_Emergency_HandleDB   ( void);
static	void						Task_Log_CoinRouting_HandleDB ( void);
//static	void						Task_Log_Z_HandleDB           ( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Log_Init( void)
/*----------------------------------------------------------------------------*/
{
   task_fPowerup        =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Log_Main( void)
/*----------------------------------------------------------------------------*/
{
	if( App_TaskStatus[TASK_LOG] == DISABLED)
		return;
   Task_Log_HandleExtEvent();
   Task_Log_Event_HandleDB();
   Task_Log_Trans_HandleDB();                   // This is it !!!
   Task_Log_CoinRouting_HandleDB();
   Task_Log_Emergency_HandleDB();
   ////Task_Log_Z_HandleDB();
   task_fPowerup  =  FALSE;

}



                              /****************/
                              /*    EVENTS    */
                              /****************/

/*----------------------------------------------------------------------------*/
void                          Task_Log_Event_WriteRecord( tLogEvent_Rec   *pRec)
/*----------------------------------------------------------------------------*/
{
   
   if( (pRec == NULL) || (task_fPowerup == TRUE))
      return;
   
   task_Event_Rec_Write =  *pRec;

   if( task_Event_Info.Idx_W >= MAX_LOG_EVENT_RECORDS) task_Event_Info.Idx_W =  0;
   xSramAPI_Write( &task_Event_Rec_Write, sizeof( task_Event_Rec_Write), xSRAM_TYPE_LOG_EVENT_REC, task_Event_Info.Idx_W, TRUE);
   if( (++ task_Event_Info.Idx_W) >= MAX_LOG_EVENT_RECORDS)  task_Event_Info.Idx_W =  0;

   if( (++ task_Event_Info.RecordsInDB)   >= MAX_LOG_EVENT_RECORDS)  task_Event_Info.RecordsInDB   =  MAX_LOG_EVENT_RECORDS;
   if( task_Event_Info.Idx_W == task_Event_Info.Idx_R)
      if( (++ task_Event_Info.Idx_R)  >= MAX_LOG_EVENT_RECORDS)  task_Event_Info.Idx_R  =  0;

   if( (++ task_Event_Info.RecordsInDB_Server)  >= MAX_LOG_EVENT_RECORDS)  task_Event_Info.RecordsInDB_Server  =  MAX_LOG_EVENT_RECORDS;
   if( task_Event_Info.Idx_W == task_Event_Info.Idx_R_Server)
      if( (++ task_Event_Info.Idx_R_Server) >= MAX_LOG_EVENT_RECORDS)  task_Event_Info.Idx_R_Server =  0;

   SET_BIT( task_Event_InfoFlags, FLAG_INFO_WRITE);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Log_Event_ReadRecord( tLogEvent_Rec *pRec, bool   fReadFirstRecord)
/*----------------------------------------------------------------------------*/
{
   if( fReadFirstRecord == TRUE)
   {
      task_Event_ReadIndex    =  task_Event_Info.Idx_R;
      task_Event_fReadLastRec =  FALSE;
   }

   if( (task_Event_Info.RecordsInDB == 0) || (pRec == NULL) || (task_Event_fReadLastRec == TRUE) || (task_fPowerup == TRUE))
      return( FALSE);

   xSramAPI_Read ( &task_Event_Rec_Read, sizeof( task_Event_Rec_Read), xSRAM_TYPE_LOG_EVENT_REC, task_Event_ReadIndex, TRUE);
   if( (++ task_Event_ReadIndex) >= MAX_LOG_EVENT_RECORDS)  task_Event_ReadIndex =  0;

   if( task_Event_ReadIndex == task_Event_Info.Idx_W)
      task_Event_fReadLastRec =  TRUE;

   *pRec =  task_Event_Rec_Read;

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Log_Event_ReadRecord_Server( tLogEvent_Rec *pRec)
/*----------------------------------------------------------------------------*/
{
   if( task_Event_Info.Idx_R_Server ==   task_Event_Info.Idx_W)
      task_Event_Info.RecordsInDB_Server  =  0;

   if( (task_Event_Info.RecordsInDB_Server == 0) || (pRec == NULL) || (task_fPowerup == TRUE))
      return( FALSE);

   xSramAPI_Read( &task_Event_Rec_Read, sizeof( task_Event_Rec_Read), xSRAM_TYPE_LOG_EVENT_REC, task_Event_Info.Idx_R_Server, TRUE);
   *pRec =  task_Event_Rec_Read;

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
void                          Task_Log_Event_Step_Fwd_Server( void)
/*----------------------------------------------------------------------------*/
{
   if( task_Event_Info.RecordsInDB_Server == 0)
      return;

   if( (++ task_Event_Info.Idx_R_Server) >= MAX_LOG_EVENT_RECORDS)
      task_Event_Info.Idx_R_Server =  0;
   
   if( task_Event_Info.RecordsInDB_Server > 0)
      task_Event_Info.RecordsInDB_Server  --;

   SET_BIT( task_Event_InfoFlags, FLAG_INFO_WRITE);
}



/*----------------------------------------------------------------------------*/
tLogEvent_Info               *Task_Log_Event_GetInfo( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_Event_Info);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Log_Event_ClearDB( void)
/*----------------------------------------------------------------------------*/
{
   memset( &task_Event_Info   ,  0, sizeof( task_Event_Info));
   SET_BIT( task_Event_InfoFlags, FLAG_INFO_WRITE);
}



                              /****************/
                              /* TRANSACTIONS */
                              /****************/

/*----------------------------------------------------------------------------*/
void                          Task_Log_Trans_WriteRecord( tLogTrans_Rec   *pRec)
/*----------------------------------------------------------------------------*/
{
   
   if( (pRec == NULL) || (task_fPowerup == TRUE))
      return;
   
   task_Trans_Rec_Write =  *pRec;

   if( task_Trans_Info.Idx_W >= MAX_LOG_TRANS_RECORDS) /*task_Trans_Info.Idx_W =  0;*/ return;

   xSramAPI_Write( &task_Trans_Rec_Write, sizeof( task_Trans_Rec_Write), xSRAM_TYPE_LOG_TRANS_REC, task_Trans_Info.Idx_W, TRUE);

   task_Trans_Info.Idx_W   ++;
   if( task_Trans_Info.Idx_W >= MAX_LOG_TRANS_RECORDS)
      task_Trans_Info.Idx_W   =  (MAX_LOG_TRANS_RECORDS - 1);

   /*
   if( task_Trans_Info.Idx_W >= MAX_LOG_TRANS_RECORDS)
   {
      task_Trans_Info.Idx_W =  0;
      task_Trans_Info.fMadeCyclicRound =  TRUE;
   }
   */

   if( (++ task_Trans_Info.RecordsInDB)   >= MAX_LOG_TRANS_RECORDS)  task_Trans_Info.RecordsInDB   =  MAX_LOG_TRANS_RECORDS;
   /*
   if( task_Trans_Info.Idx_W == task_Trans_Info.Idx_R)
      if( (++ task_Trans_Info.Idx_R)  >= MAX_LOG_TRANS_RECORDS)  task_Trans_Info.Idx_R  =  0;
   */


   if( (++ task_Trans_Info.RecordsInDB_Server)  >= MAX_LOG_TRANS_RECORDS)  task_Trans_Info.RecordsInDB_Server  =  MAX_LOG_TRANS_RECORDS;        // This line cause an HEAVY LOAD on the system !!!

   /*
   if( task_Trans_Info.Idx_W == task_Trans_Info.Idx_R_Server)
      if( (++ task_Trans_Info.Idx_R_Server) >= MAX_LOG_TRANS_RECORDS)  task_Trans_Info.Idx_R_Server =  0;
   */

   SET_BIT( task_Trans_InfoFlags, FLAG_INFO_WRITE);

}



/*----------------------------------------------------------------------------*/
bool                          Task_Log_Trans_ReadRecord( tLogTrans_Rec *pRec, bool   fReadFirstRecord)
/*----------------------------------------------------------------------------*/
{
   if( fReadFirstRecord == TRUE)
   {
      task_Trans_ReadIndex    =  task_Trans_Info.Idx_R;
      task_Trans_fReadLastRec =  FALSE;
   }

   if( (task_Trans_Info.RecordsInDB == 0) || (pRec == NULL) || (task_Trans_fReadLastRec == TRUE) || (task_fPowerup == TRUE))
      return( FALSE);

   xSramAPI_Read( &task_Trans_Rec_Read, sizeof( task_Trans_Rec_Read), xSRAM_TYPE_LOG_TRANS_REC, task_Trans_ReadIndex, TRUE);
   task_Trans_ReadIndex ++;
   /*
   if( (task_Trans_ReadIndex >= MAX_LOG_TRANS_RECORDS)
      task_Trans_ReadIndex =  0;
   */

   if( task_Trans_ReadIndex == task_Trans_Info.Idx_W)
      task_Trans_fReadLastRec =  TRUE;

   *pRec =  task_Trans_Rec_Read;

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Log_Trans_ReadRecord_Server( tLogTrans_Rec *pRec)
/*----------------------------------------------------------------------------*/
{
   
   if( task_Trans_Info.Idx_R_Server ==   task_Trans_Info.Idx_W)
      task_Trans_Info.RecordsInDB_Server  =  0;

   //if( (task_Trans_Info.Idx_R_Server <= task_Trans_Info.Idx_W) && (task_Trans_Info.fMadeCyclicRound == TRUE))
   //   task_Trans_Info.fMadeCyclicRound =  FALSE;

   if( (task_Trans_Info.RecordsInDB_Server == 0) || (pRec == NULL) || (task_fPowerup == TRUE))
      return( FALSE);

   xSramAPI_Read( &task_Trans_Rec_Read, sizeof( task_Trans_Rec_Read), xSRAM_TYPE_LOG_TRANS_REC, task_Trans_Info.Idx_R_Server, TRUE);
   *pRec =  task_Trans_Rec_Read;

   SET_BIT( task_Trans_InfoFlags, FLAG_INFO_WRITE);

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
void                          Task_Log_Trans_Step_Fwd_Server( void)
/*----------------------------------------------------------------------------*/
{

   if( task_Trans_Info.RecordsInDB_Server == 0)
      return;

   if( (++ task_Trans_Info.Idx_R_Server) >= MAX_LOG_TRANS_RECORDS)
      task_Trans_Info.Idx_R_Server =  0;
   
   if( task_Trans_Info.RecordsInDB_Server > 0)
      task_Trans_Info.RecordsInDB_Server  --;
   
   SET_BIT( task_Trans_InfoFlags, FLAG_INFO_WRITE);
}



/*----------------------------------------------------------------------------*/
tLogTrans_Info               *Task_Log_Trans_GetInfo( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_Trans_Info);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Log_Trans_ClearDB( void)
/*----------------------------------------------------------------------------*/
{
   memset( &task_Trans_Info   ,  0, sizeof( task_Trans_Info));

   SET_BIT( task_Trans_InfoFlags, FLAG_INFO_WRITE);
}



                              /**********************/
                              /*    COIN ROUTING    */
                              /**********************/

/*----------------------------------------------------------------------------*/
void                          Task_Log_CoinRouting_WriteRecord( tLogCoinRouting_Rec   *pRec)
/*----------------------------------------------------------------------------*/
{
   if( (pRec == NULL) || (task_fPowerup == TRUE))
      return;

   task_CoinRouting_Rec_Write             =  *pRec;
   task_CoinRouting_Rec_Write.EvNumerator =  (++ task_CoinRouting_Info.EvNumerator_Last);
   task_CoinRouting_Rec_Write.TimeStamp   =  ClockAPI_DateAndTimeToSeconds();

   xSramAPI_Write( &task_CoinRouting_Rec_Write, sizeof( task_CoinRouting_Rec_Write), xSRAM_TYPE_LOG_COIN_ROUTING_REC, task_CoinRouting_Info.WriteIndex, TRUE);
   if( (++ task_CoinRouting_Info.WriteIndex) >= MAX_LOG_COIN_ROUTING_RECORDS)  task_CoinRouting_Info.WriteIndex =  0;

   if( (++ task_CoinRouting_Info.RecordsInDB)   >= MAX_LOG_COIN_ROUTING_RECORDS)  task_CoinRouting_Info.RecordsInDB   =  MAX_LOG_COIN_ROUTING_RECORDS;
   if( task_CoinRouting_Info.WriteIndex == task_CoinRouting_Info.ReadIndex)
      if( (++ task_CoinRouting_Info.ReadIndex) >= MAX_LOG_COIN_ROUTING_RECORDS)   task_CoinRouting_Info.ReadIndex =  0;

   if( (++ task_CoinRouting_Info.RecordsInDB_Server)  >= MAX_LOG_COIN_ROUTING_RECORDS)  task_CoinRouting_Info.RecordsInDB_Server  =  MAX_LOG_COIN_ROUTING_RECORDS;
   if( task_CoinRouting_Info.WriteIndex == task_CoinRouting_Info.ReadIndex_Server)
      if( (++ task_CoinRouting_Info.ReadIndex_Server) >= MAX_LOG_COIN_ROUTING_RECORDS)  task_CoinRouting_Info.ReadIndex_Server =  0;

   SET_BIT( task_CoinRouting_InfoFlags, FLAG_INFO_WRITE);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Log_CoinRouting_ReadRecord( tLogCoinRouting_Rec *pRec, bool   fReadFirstRecord)
/*----------------------------------------------------------------------------*/
{
   if( fReadFirstRecord == TRUE)
   {
      task_CoinRouting_ReadIndex    =  task_CoinRouting_Info.ReadIndex;
      task_CoinRouting_fReadLastRec =  FALSE;
   }

   if( (task_CoinRouting_Info.RecordsInDB == 0) || (pRec == NULL) || (task_CoinRouting_fReadLastRec == TRUE) || (task_fPowerup == TRUE))
      return( FALSE);

   xSramAPI_Read( &task_CoinRouting_Rec_Read, sizeof( task_CoinRouting_Rec_Read), xSRAM_TYPE_LOG_COIN_ROUTING_REC, task_CoinRouting_ReadIndex, TRUE);
   if( (++ task_CoinRouting_ReadIndex) >= MAX_LOG_COIN_ROUTING_RECORDS)  task_CoinRouting_ReadIndex =  0;

   if( task_CoinRouting_ReadIndex == task_CoinRouting_Info.WriteIndex)
      task_CoinRouting_fReadLastRec =  TRUE;

   *pRec =  task_CoinRouting_Rec_Read;

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Log_CoinRouting_ReadRecord_Server( tLogCoinRouting_Rec *pRec)
/*----------------------------------------------------------------------------*/
{
   if( task_CoinRouting_Info.ReadIndex_Server ==   task_CoinRouting_Info.WriteIndex)
      task_CoinRouting_Info.RecordsInDB_Server  =  0;

   if( (task_CoinRouting_Info.RecordsInDB_Server == 0) || (pRec == NULL) || (task_fPowerup == TRUE))
      return( FALSE);

   xSramAPI_Read( &task_CoinRouting_Rec_Read, sizeof( task_CoinRouting_Rec_Read), xSRAM_TYPE_LOG_COIN_ROUTING_REC, task_CoinRouting_Info.ReadIndex_Server, TRUE);

   *pRec =  task_CoinRouting_Rec_Read;

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
void                          Task_Log_CoinRouting_Step_Fwd_Server( void)       // Not used
/*----------------------------------------------------------------------------*/
{
   if( task_CoinRouting_Info.RecordsInDB_Server == 0)
      return;

   if( (++ task_CoinRouting_Info.ReadIndex_Server) >= MAX_LOG_COIN_ROUTING_RECORDS)
      task_CoinRouting_Info.ReadIndex_Server =  0;

   if( task_CoinRouting_Info.RecordsInDB_Server > 0)
      task_CoinRouting_Info.RecordsInDB_Server  --;

   SET_BIT( task_CoinRouting_InfoFlags, FLAG_INFO_WRITE);
}



/*----------------------------------------------------------------------------*/
tLogCoinRouting_Info               *Task_Log_CoinRouting_GetInfo( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_CoinRouting_Info);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Log_CoinRouting_ClearDB( void)
/*----------------------------------------------------------------------------*/
{
   memset( &task_CoinRouting_Info   ,  0, sizeof( task_CoinRouting_Info));
   SET_BIT( task_CoinRouting_InfoFlags, FLAG_INFO_WRITE);
}



                              /****************/
                              /* EMERGENCIES  */
                              /****************/

/*----------------------------------------------------------------------------*/
void                          Task_Log_Emergency_Step_Fwd_Server( void)
/*----------------------------------------------------------------------------*/
{
   task_Emergency_Info.Idx_W  ++;
   SET_BIT( task_Emergency_InfoFlags, FLAG_INFO_WRITE);
}

/*----------------------------------------------------------------------------*/
tLogEmergency_Info           *Task_Log_Emergency_GetInfo( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_Emergency_Info);
}



#if 0 // [
                              /***************/
                              /*  Z Records  */
                              /***************/

/*----------------------------------------------------------------------------*/
void                          Task_Log_Z_WriteRecord( tLogZ_Rec   *pRec)
/*----------------------------------------------------------------------------*/
{
   if( (pRec == NULL) || (task_fPowerup == TRUE))
      return;

   task_Z_Rec_Write =  *pRec;

   if( task_Z_Info.Idx_W >= MAX_LOG_Z_RECORDS) task_Z_Info.Idx_W =  0;
   xSramAPI_Write( &task_Z_Rec_Write, sizeof( task_Z_Rec_Write), xSRAM_TYPE_LOG_Z_REC, task_Z_Info.Idx_W, TRUE);
   if( (++ task_Z_Info.Idx_W) >= MAX_LOG_Z_RECORDS)  task_Z_Info.Idx_W =  0;

   if( (++ task_Z_Info.RecordsInDB)   >= MAX_LOG_Z_RECORDS)  task_Z_Info.RecordsInDB   =  MAX_LOG_Z_RECORDS;
   if( task_Z_Info.Idx_W == task_Z_Info.Idx_R)
      if( (++ task_Z_Info.Idx_R)  >= MAX_LOG_Z_RECORDS)  task_Z_Info.Idx_R  =  0;

   if( (++ task_Z_Info.RecordsInDB_Server)  >= MAX_LOG_Z_RECORDS)  task_Z_Info.RecordsInDB_Server  =  MAX_LOG_Z_RECORDS;
   if( task_Z_Info.Idx_W == task_Z_Info.Idx_R_Server)
      if( (++ task_Z_Info.Idx_R_Server) >= MAX_LOG_Z_RECORDS)  task_Z_Info.Idx_R_Server =  0;

   SET_BIT( task_Z_InfoFlags, FLAG_INFO_WRITE);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Log_Z_ReadRecord( tLogZ_Rec *pRec, bool   fReadFirstRecord)
/*----------------------------------------------------------------------------*/
{
   if( fReadFirstRecord == TRUE)
   {
      task_Z_ReadIndex    =  task_Z_Info.Idx_R;
      task_Z_fReadLastRec =  FALSE;
   }

   if( (task_Z_Info.RecordsInDB == 0) || (pRec == NULL) || (task_Z_fReadLastRec == TRUE) || (task_fPowerup == TRUE))
      return( FALSE);

   xSramAPI_Read( &task_Z_Rec_Read, sizeof( task_Z_Rec_Read), xSRAM_TYPE_LOG_Z_REC, task_Z_ReadIndex, TRUE);
   if( (++ task_Z_ReadIndex) >= MAX_LOG_Z_RECORDS)  task_Z_ReadIndex =  0;

   if( task_Z_ReadIndex == task_Z_Info.Idx_W)
      task_Z_fReadLastRec =  TRUE;

   *pRec =  task_Z_Rec_Read;

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Log_Z_ReadRecord_Server( tLogZ_Rec *pRec)
/*----------------------------------------------------------------------------*/
{
   if( task_Z_Info.Idx_R_Server ==   task_Z_Info.Idx_W)
      task_Z_Info.RecordsInDB_Server  =  0;

   if( (task_Z_Info.RecordsInDB_Server == 0) || (pRec == NULL) || (task_fPowerup == TRUE))
      return( FALSE);

   xSramAPI_Read( &task_Z_Rec_Read, sizeof( task_Z_Rec_Read), xSRAM_TYPE_LOG_Z_REC, task_Z_Info.Idx_R_Server, TRUE);
   *pRec =  task_Z_Rec_Read;

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
void                          Task_Log_Z_Step_Fwd_Server( void)         // Not used
/*----------------------------------------------------------------------------*/
{
   if( task_Z_Info.RecordsInDB_Server == 0)
      return;

   if( (++ task_Z_Info.Idx_R_Server) >= MAX_LOG_Z_RECORDS)
      task_Z_Info.Idx_R_Server =  0;

   if( task_Z_Info.RecordsInDB_Server > 0)
      task_Z_Info.RecordsInDB_Server  --;

   SET_BIT( task_Z_InfoFlags, FLAG_INFO_WRITE);
}



/*----------------------------------------------------------------------------*/
tLogZ_Info                   *Task_Log_Z_GetInfo( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_Z_Info);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Log_Z_ClearDB( void)
/*----------------------------------------------------------------------------*/
{
   memset( &task_Z_Info   ,  0, sizeof( task_Z_Info));
   SET_BIT( task_Z_InfoFlags, FLAG_INFO_WRITE);
}
#endif   // ]



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Log_HandleExtEvent( void)
/*----------------------------------------------------------------------------*/
{
   if( App_TaskEvent[TASK_SERVER] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_V_POINTERS))
         Task_Log_Event_ClearDB();

      if( BIT_IS_SET( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_L_POINTERS))
         Task_Log_Trans_ClearDB();

      if( BIT_IS_SET( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_COIN_ROUTING))
         Task_Log_CoinRouting_ClearDB();

      //if( BIT_IS_SET( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_Z_POINTERS))
      //   Task_Log_Z_ClearDB();
   }

   if( App_TaskEvent[TASK_LOG] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_V_POINTERS))
         Task_Log_Event_ClearDB();

      if( BIT_IS_SET( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_L_POINTERS))
         Task_Log_Trans_ClearDB();

      if( BIT_IS_SET( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_COIN_ROUTING))
         Task_Log_CoinRouting_ClearDB();

      //if( BIT_IS_SET( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_Z_POINTERS))
      //   Task_Log_Z_ClearDB();
   }

   App_TaskEvent[TASK_LOG] =  EV_APP_VOID;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Log_Event_HandleDB( void)
/*----------------------------------------------------------------------------*/
{
   if( task_fPowerup == TRUE)
   {
      xSramAPI_Read( &task_Event_Info, sizeof( task_Event_Info), xSRAM_TYPE_LOG_EVENT_INFO, 0, TRUE);
      if( Checksum_Compare( &task_Event_Info, sizeof( task_Event_Info), task_Event_Info.Checksum) == FALSE)
      {
         memset( &task_Event_Info, 0, sizeof( task_Event_Info));
         SET_BIT( task_Event_InfoFlags, FLAG_INFO_WRITE);
      }
   }

   if( BIT_IS_SET( task_Event_InfoFlags, FLAG_INFO_WRITE))
   {
      CLEAR_BIT( task_Event_InfoFlags, FLAG_INFO_WRITE);
      task_Event_Info.Checksum   =  Checksum_Calc( &task_Event_Info, sizeof( task_Event_Info), task_Event_Info.Checksum);
      xSramAPI_Write( &task_Event_Info, sizeof( task_Event_Info), xSRAM_TYPE_LOG_EVENT_INFO, 0, TRUE);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Log_Trans_HandleDB( void)
/*----------------------------------------------------------------------------*/
{
   if( task_fPowerup == TRUE)
   {
      xSramAPI_Read( &task_Trans_Info, sizeof( task_Trans_Info), xSRAM_TYPE_LOG_TRANS_INFO, 0, TRUE);
      if( Checksum_Compare( &task_Trans_Info, sizeof( task_Trans_Info), task_Trans_Info.Checksum) == FALSE)
      {
         memset( &task_Trans_Info, 0, sizeof( task_Trans_Info));

         SET_BIT( task_Trans_InfoFlags, FLAG_INFO_WRITE);
      }
   }
   //DEBUG_LINE_ON;
   // Heavy part ~60mS
   if( BIT_IS_SET( task_Trans_InfoFlags, FLAG_INFO_WRITE))
   {
      CLEAR_BIT( task_Trans_InfoFlags, FLAG_INFO_WRITE);
      task_Trans_Info.Checksum   =  Checksum_Calc( &task_Trans_Info, sizeof( task_Trans_Info), task_Trans_Info.Checksum);
      xSramAPI_Write( &task_Trans_Info, sizeof( task_Trans_Info), xSRAM_TYPE_LOG_TRANS_INFO, 0, TRUE);
   }
   //DEBUG_LINE_OFF;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Log_CoinRouting_HandleDB( void)
/*----------------------------------------------------------------------------*/
{
   if( task_fPowerup == TRUE)
   {
      xSramAPI_Read( &task_CoinRouting_Info, sizeof( task_CoinRouting_Info), xSRAM_TYPE_LOG_COIN_ROUTING_INFO, 0, TRUE);
      if( Checksum_Compare( &task_CoinRouting_Info, sizeof( task_CoinRouting_Info), task_CoinRouting_Info.Checksum) == FALSE)
      {
         memset( &task_CoinRouting_Info, 0, sizeof( task_CoinRouting_Info));
         SET_BIT( task_CoinRouting_InfoFlags, FLAG_INFO_WRITE);
      }
   }

   if( BIT_IS_SET( task_CoinRouting_InfoFlags, FLAG_INFO_WRITE))
   {
      CLEAR_BIT( task_CoinRouting_InfoFlags, FLAG_INFO_WRITE);
      task_CoinRouting_Info.Checksum   =  Checksum_Calc( &task_CoinRouting_Info, sizeof( task_CoinRouting_Info), task_CoinRouting_Info.Checksum);
      xSramAPI_Write( &task_CoinRouting_Info, sizeof( task_CoinRouting_Info), xSRAM_TYPE_LOG_COIN_ROUTING_INFO, 0, TRUE);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Log_Emergency_HandleDB( void)
/*----------------------------------------------------------------------------*/
{
   if( task_fPowerup == TRUE)
   {
      xSramAPI_Read( &task_Emergency_Info, sizeof( task_Emergency_Info), xSRAM_TYPE_LOG_EMERG_INFO, 0, TRUE);
      if( Checksum_Compare( &task_Emergency_Info, sizeof( task_Emergency_Info), task_Emergency_Info.Checksum) == FALSE)
      {
         memset( &task_Emergency_Info, 0, sizeof( task_Emergency_Info));
         SET_BIT( task_Emergency_InfoFlags, FLAG_INFO_WRITE);
      }
   }

   if( BIT_IS_SET( task_Emergency_InfoFlags, FLAG_INFO_WRITE))
   {
      CLEAR_BIT( task_Emergency_InfoFlags, FLAG_INFO_WRITE);
      task_Emergency_Info.Checksum  =  Checksum_Calc( &task_Emergency_Info, sizeof( task_Emergency_Info), task_Emergency_Info.Checksum);
      xSramAPI_Write( &task_Emergency_Info, sizeof( task_Emergency_Info), xSRAM_TYPE_LOG_EMERG_INFO, 0, TRUE);
   }
}



#if 0  // [
/*----------------------------------------------------------------------------*/
static	void						Task_Log_Z_HandleDB( void)
/*----------------------------------------------------------------------------*/
{
   if( task_fPowerup == TRUE)
   {
      xSramAPI_Read( &task_Z_Info, sizeof( task_Z_Info), xSRAM_TYPE_LOG_Z_INFO, 0, TRUE);
      if( Checksum_Compare( &task_Z_Info, sizeof( task_Z_Info), task_Z_Info.Checksum) == FALSE)
      {
         memset( &task_Z_Info, 0, sizeof( task_Z_Info));
         SET_BIT( task_Z_InfoFlags, FLAG_INFO_WRITE);
      }
   }

   if( BIT_IS_SET( task_Z_InfoFlags, FLAG_INFO_WRITE))
   {
      CLEAR_BIT( task_Z_InfoFlags, FLAG_INFO_WRITE);
      task_Z_Info.Checksum   =  Checksum_Calc( &task_Z_Info, sizeof( task_Z_Info), task_Z_Info.Checksum);
      xSramAPI_Write( &task_Z_Info, sizeof( task_Z_Info), xSRAM_TYPE_LOG_Z_INFO, 0, TRUE);
   }
}
#endif // ]




/*----------------------------------------------------------------------------*/
void                    Task_Log_DebugIncr_RecordsInDB_Server(void)
/*----------------------------------------------------------------------------*/
{
   task_Trans_Info.RecordsInDB_Server++;
}

/*----------------------------------------------------------------------------*/
void                    Task_Log_DebugClear_RecordsInDB_Server(void)
/*----------------------------------------------------------------------------*/
{
   task_Trans_Info.RecordsInDB_Server = 0;
}


/*----------------------------------------------------------------------------*/
void                    Task_Log_DebugShow_RecordsInDB_Server(void)
/*----------------------------------------------------------------------------*/
{
   //printf("RecordsInDB_Server %i\n", task_Trans_Info.RecordsInDB_Server);     //remarked by AC 28/11/2016
}


/*----------------------------------------------------------------------------*/
void                    Task_Log_DebugClear_Idx_W(void)
/*----------------------------------------------------------------------------*/
{
   task_Trans_Info.Idx_W = 0;
}


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/*
void  Task_Log_Event_WriteRecord( tLogEvent_Rec *pRec);
  Caller:  Task_Application_SetEvent()


bool  Task_Log_Event_ReadRecord( tLogEvent_Rec  *pRec, bool   fReadFirstRecord);
  Caller:  Task_Printer_PrintEvents



bool  Task_Log_Event_ReadRecord_Server( tLogEvent_Rec *pRec);
  Caller:  Task_Server_HandleComm_Tx() !!!  When trying to send an event ('V')



void  Task_Log_Event_Step_Fwd_Server( void);
  Caller:  Task_Server_HandleComm_Rx()   When Event was confirmed by the Server.



tLogEvent_Info  *Task_Log_Event_GetInfo( void);
  Caller:  Task_Setup_Func_LogFiles(),  
           Task_Server_HandleComm_Tx() when trying to send an Event ('V')


void  Task_Log_Event_ClearDB( void);
  Caller:  Task_Log_HandleExtEvent()
           Task_Setup_Func_PrintReport()
           Task_Setup_Func_EraseSalesInfo()
*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
