/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
	TASK_STATE_INIT_FILES         ,
   TASK_STATE_INIT_DRIVERS       ,
   TASK_STATE_INIT_LIBRERIES     ,
   TASK_STATE_INIT_API           ,
	TASK_STATE_INIT_TASKS         ,
	TASK_STATE_SHOW_VERSION       ,
	TASK_STATE_SHOW_WELCOME_NOTE  ,
	TASK_STATE_INIT_END           ,
	/********************/
	TASK_STATE_LAST
}  tTaskState;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
tTimerS_API_Setup               TimerS_API_Setup;

static	tTaskState		task_State;
static	tTimerSec		task_TimerSec;
static   tParams_C_Rec          task_Param_C_Rec;
/*------------------------ Local Function Prototypes -------------------------*/
static	void			Task_Init_HandleTimer   ( void);
static   void                   Task_Init_Ports         ( void);

static   void                   Task_Init_BuzzAPI_Init  ( void);
static   void                   Task_Init_LedAPI_Init   ( void);
static   void                   Task_Init_Checksum_Init ( void);
static   void                   Task_Init_Clock_Init    ( void);
static   void                   Task_Init_InOut_Init    ( void);
static   void                   Task_Init_Lcd_Init      ( void);
static	 void                   Task_Init_Init_Util     ( void);
static   void                   Task_Init_Keyboard_Init ( void);
/*----------------------------------------------------------------------------*/
void                            T_Init_TimerS_OVF       (byte argumentOVF);


#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Init_Init( void)
/*----------------------------------------------------------------------------*/
{
ulong    Counter;
byte		Task;


	// Let hardware to stabilze its power
   for( Counter = 0; Counter < 4000L; Counter ++)
      _WDR();

   Task_Init_Ports();

	// On power up enable just the INIT task
   memset( App_TaskStatus, DISABLED, sizeof( App_TaskStatus)); // DON'T DELETE THIS LINE
	App_TaskStatus[TASK_INIT] = ENABLED;

	task_State     = (tTaskState)0;
   task_TimerSec  =  0;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Init_Main( void)
/*----------------------------------------------------------------------------*/
{
char*                   pApp_code_string;
tParams_A_Rec           task_Rec_Type_A;

   if( App_TaskStatus[TASK_INIT] == DISABLED)
		return;

   Task_Init_HandleTimer();
   switch( task_State)
   {
      case	TASK_STATE_INIT_FILES:
	_CLI(); // Disable global interrupts

	_WDR(); // Watch Dog Reset

         Task_Hardware_Init();
         Task_Timer_Init();
         // TimerS Init
         TimerS_API_Setup.func_TimerS_OVF = T_Init_TimerS_OVF;
         Timers_Init(&TimerS_API_Setup);                                 // New Timers System
         //App_TaskStatus[TASK_HW]	   = ENABLED;
         App_TaskStatus[TASK_TIMER]	= ENABLED;

         _SEI(); // Enable global interrupts
	_WDR(); // Watch Dog Reset

	task_State ++;
        break;

      case  TASK_STATE_INIT_DRIVERS:
        LatchDRV_Init();
        UartDRV_Init();
        task_State ++;
        break;

      case  TASK_STATE_INIT_LIBRERIES:
         Debug_Start( COM_8);           //( COM_7);      
         Task_Init_BuzzAPI_Init();
         Task_Init_LedAPI_Init();
         Task_Init_Checksum_Init();
         Task_Init_Clock_Init();
         Task_Init_InOut_Init();
         Task_Init_Lcd_Init();
         Task_Init_Init_Util();
         //Debug_Start( COM_8);           //( COM_7);      
         Task_Init_Keyboard_Init(); 
         task_State ++;
         break;

      case  TASK_STATE_INIT_API:
         //App_TaskStatus[TASK_HW]	   = ENABLED;
         EsqrAPI_Init();
         xSramAPI_Init();
         //Task_Init_Keyboard_Init();

         task_State ++;
         break;
            
      case  TASK_STATE_INIT_TASKS:
      	Task_Params_Init();
         Task_Log_Init();
         Task_Display_Init();

         Task_B2B_Master_Init();
         Task_Application_Init();
         Task_Server_Init();
         Task_TibaPay_Init();           //A.C
         Task_Printer_Init();
         Task_Hopper_Init();
         Task_Setup_Init();
         Task_Lib_Init();
         
         App_TaskStatus[TASK_HW]	   = ENABLED;
         
         task_State ++;
         break;
   
      case  TASK_STATE_SHOW_VERSION:
         {
         static   char  Str[20];

         LcdAPI_WriteStringConst( LINE1, MSG_APP_VER);

         Task_Display_SetFont ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_5);
         Task_Display_SetColor( VGA_COLOR_LIGHT_GREEN);

         //
         Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, PARAM_A_SITE_NAME /*ParamCode*/);    // Get parameter (A) which points to the Site Names table
         pApp_code_string = Task_Printer_GetSiteName((byte) task_Rec_Type_A);                   // Get pointer to the matched string
         //strcpy_P((char*)pDestStr, App_Code);
         strcpy((char*)Str, pApp_code_string);
         //
         //strcpy_P ( Str, App_Code);
         Task_Display_WriteString( LINE3, 0, Str);

         strcpy_P ( Str, App_Version);
         Task_Display_WriteString( LINE4, 0, Str);

         strcpy   ( Str, App_Date);
         Task_Display_WriteString( LINE5, 0, Str);

         Task_Display_Run();
         }

         task_TimerSec  =	Task_Timer_SetTimeSec( 3);
				task_State ++;
         break;

      case  TASK_STATE_SHOW_WELCOME_NOTE:
         if( task_TimerSec > 0)
            break;

         task_Param_C_Rec  =  0;
         //Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_TIME_PWR_UP);
         if( task_Param_C_Rec == 0) task_Param_C_Rec  =  3;
         task_TimerSec  =	Task_Timer_SetTimeSec( task_Param_C_Rec);
         task_State ++;
         break;

      case	TASK_STATE_INIT_END:
         if( task_TimerSec > 0)
            break;

         LcdAPI_ClearScreen();
         memset( App_TaskStatus, ENABLED, sizeof( App_TaskStatus));

         App_TaskStatus[TASK_INIT]  =  DISABLED;
         App_fReady                 =  TRUE;
         task_State                 =  (tTaskState)0;
         break;

      default:
         task_State = (tTaskState)0;
         break;
   }
   
}



/*----------------------------------------------------------------------------*/
bool                    		Task_Init_IsInitDone( void)
/*----------------------------------------------------------------------------*/
{
   return( App_fReady);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Init_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( task_TimerSec <  Gap)  task_TimerSec  =  0; else  task_TimerSec  -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Init_Ports( void)
/*----------------------------------------------------------------------------*/
{
   // See Inc\DRV\HW.def
   HW_DEF_INIT_PORTA;
   HW_DEF_INIT_DDRA;

   HW_DEF_INIT_PORTB;
   HW_DEF_INIT_DDRB;

   HW_DEF_INIT_PORTC;
   HW_DEF_INIT_DDRC;

   HW_DEF_INIT_PORTD;
   HW_DEF_INIT_DDRD;

   HW_DEF_INIT_PORTE;
   HW_DEF_INIT_DDRE;

   HW_DEF_INIT_PORTF;
   HW_DEF_INIT_DDRF;

   HW_DEF_INIT_PORTG;
   HW_DEF_INIT_DDRG;

   // Magnetic Card Reader
   HW_DEF_INIT_EICRA;
   HW_DEF_INIT_EICRB;
   HW_DEF_INIT_EIMSK;

   // External SRAM
   HW_DEF_INIT_MCUCR;
   HW_DEF_INIT_XMCRA;

   // Timer interrupt (TIMER0_OVF_vect)
   HW_DEF_INIT_TCCR0;
   HW_DEF_INIT_TCNT0;
   HW_DEF_INIT_TIMSK;
   HW_DEF_INIT_WDTCR;
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Init_BuzzAPI_Init( void)
/*----------------------------------------------------------------------------*/
{
static   tBuzz_API_Setup    Buzz_API_Setup;


   Lib_Version_BuzzAPI__1_0_0                =  0; // dummy assignment (simply to use this global variable)

   memset( &Buzz_API_Setup, 0, sizeof( Buzz_API_Setup));

   Buzz_API_Setup.func_BuzzDRV_SetMode                =  BuzzDRV_SetMode;
   Buzz_API_Setup.func_BuzzDRV_Init                   =  BuzzDRV_Init;
   Buzz_API_Setup.func_Task_Timer_GetTimer            =  Task_Timer_GetTimer;
   Buzz_API_Setup.HwDef_TMR0_TickInterval             =  HW_DEF_TMR0_TICK_INTERVAL;

   BuzzAPI_Init( &Buzz_API_Setup);
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Init_LedAPI_Init( void)
/*----------------------------------------------------------------------------*/
{
static   tLED_API_Setup    LED_API_Setup;


   Lib_Version_LedAPI__1_0_0                       =  0; // dummy assignment (ensure you use the correct librery version)

   memset( &LED_API_Setup, 0, sizeof( LED_API_Setup));

   LED_API_Setup.func_LedDRV_SetMode               =  LedDRV_SetMode;
   LED_API_Setup.func_LedDRV_Init                  =  LedDRV_Init;
   LED_API_Setup.func_Task_Timer_GetTimer          =  Task_Timer_GetTimer;
   LED_API_Setup.HwDef_TMR0_TickInterval           =  HW_DEF_TMR0_TICK_INTERVAL;

   LedAPI_Init( &LED_API_Setup);
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Init_Checksum_Init( void)
/*----------------------------------------------------------------------------*/
{
   Lib_Version_ChkSumAPI__1_0_0                    =  0; // dummy assignment (ensure you use the correct librery version)
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Init_Clock_Init( void)
/*----------------------------------------------------------------------------*/
{
static   tClock_API_Setup     ClockAPI_Setup_str;


   Lib_Version_ClockAPI__1_0_1                     =  0; // dummy assignment (ensure you use the correct librery version)

   memset( &ClockAPI_Setup_str, 0, sizeof( ClockAPI_Setup_str));

   ClockAPI_Setup_str.HwDef_TMR0_TickInterval      =  HW_DEF_TMR0_TICK_INTERVAL;
   ClockAPI_Setup_str.func_ClockDRV_Init           =  ClockDRV_Init;
   ClockAPI_Setup_str.Lcd_LineWidth                =  T_HW_LCD_LINE_WIDTH;              //LCD_LINE_WIDTH;
   ClockAPI_Setup_str.Lcd_MaxLines                 =  T_HW_LCD_MAX_LINES;               //MAX_LINES;
   ClockAPI_Setup_str.Lcd_App_Language             =  App_Language;
   ClockAPI_Setup_str.Language_Left2Right          =  LANGUAGE_ENGLISH;
   ClockAPI_Setup_str.Language_Right2Left          =  LANGUAGE_HEBREW;
   ClockAPI_Setup_str.func_ClockDRV_ReadRTC        =  ClockDRV_ReadRTC;
   ClockAPI_Setup_str.func_ClockDRV_WriteRTC       =  ClockDRV_WriteRTC;
   ClockAPI_Setup_str.func_LcdAPI_ToggleMsg_Idle   =  LcdAPI_ToggleMsg_Idle;
   ClockAPI_Setup_str.func_LcdAPI_WriteChar        =  LcdAPI_WriteChar;
   ClockAPI_Setup_str.func_LcdAPI_WriteString      =  LcdAPI_WriteString;
   ClockAPI_Setup_str.func_Task_Timer_GetTimer     =  Task_Timer_GetTimer;

   #ifdef   __USES_RTC_DS1302__  // [  RTC Dallas DS1302
   ClockAPI_Setup_str.RTC_ChipType                 =  RTC_CHIP_DS1302;
   #endif   // ]

   #ifdef   __USES_RTC_DS1307__  // [  RTC Dallas DS1307
   ClockAPI_Setup_str.RTC_ChipType                 =  RTC_CHIP_DS1307;
   #endif   // ]

   #ifdef   __USES_RTC_PCF8583__ // [  RTC Philips PCF8583
   ClockAPI_Setup_str.RTC_ChipType                 =  RTC_CHIP_PCF8583;
   #endif   // ]
   
   ClockAPI_Init( &ClockAPI_Setup_str);
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Init_InOut_Init( void)
/*----------------------------------------------------------------------------*/
{
static   tInOut_API_Setup  InOutAPI_Setup_str;


   Lib_Version_InOutAPI__1_0_0                     =  0; // dummy assignment (ensure you use the correct librery version)

   memset( &InOutAPI_Setup_str, 0, sizeof( InOutAPI_Setup_str));

   InOutAPI_Setup_str.func_InOutDRV_Init           =  InOutDRV_Init;    
   InOutAPI_Setup_str.func_InOutDRV_Main           =  InOutDRV_Main;              
   InOutAPI_Setup_str.func_Task_Timer_GetTimer     =  Task_Timer_GetTimer;
   InOutAPI_Setup_str.func_InOutDRV_GetStatus      =  InOutDRV_GetStatus;
   InOutAPI_Setup_str.func_InOutDRV_SetStatus      =  InOutDRV_SetStatus;
   InOutAPI_Setup_str.HwDef_TMR0_TickInterval      =  HW_DEF_TMR0_TICK_INTERVAL;
   InOutAPI_Setup_str.Max_IO_Groups                =  (byte)MAX_IO_GROUPS;
   InOutAPI_Setup_str.Input_Last                   =  (byte)INPUT_LAST;   
   InOutAPI_Setup_str.Output_Last                  =  (byte)OUTPUT_LAST;

   InOutAPI_Init( &InOutAPI_Setup_str);
}



/*----------------------------------------------------------------------------*/
static   void                   Task_Init_Lcd_Init      ( void)
/*----------------------------------------------------------------------------*/
{
static	tLCD_API_Setup						LcdAPI_Setup;
	LcdAPI_Setup.func_ClockAPI_DisplayClock			= ClockAPI_DisplayClock;
	LcdAPI_Setup.func_LcdDRV_ClearScreen			= LcdDRV_ClearScreen;
	LcdAPI_Setup.func_LcdDRV_HideText			= LcdDRV_HideText;
	LcdAPI_Setup.func_LcdDRV_Init				= LcdDRV_Init;
	LcdAPI_Setup.func_LcdDRV_SetBackLight			= LcdDRV_SetBackLight;
	LcdAPI_Setup.func_LcdDRV_SetCursorMode			= LcdDRV_SetCursorMode;
	LcdAPI_Setup.func_LcdDRV_SetCursorPosition		= LcdDRV_SetCursorPosition;
	LcdAPI_Setup.func_LcdDRV_WriteString			= LcdDRV_WriteString;
        LcdAPI_Setup.func_LcdDRV_WriteChar                      = LcdDRV_WriteChar;
	LcdAPI_Setup.func_Task_Timer_GetTimer			= Task_Timer_GetTimer;
	LcdAPI_Setup.HwDef_TMR0_TickInterval 			= HW_DEF_TMR0_TICK_INTERVAL;
	LcdAPI_Setup.func_Task_Application_ExtractConstMsg	= Task_Application_ExtractConstMsg;
	LcdAPI_Setup.Lcd_MaxLines				= 2;
	LcdAPI_Setup.Valid_Alef_Compiler			= '�';
	LcdAPI_Setup.Valid_Alef_LCD				= 0xA0;		// LCD['�']
	LcdAPI_Setup.Lcd_LineWidth				= T_HW_LCD_LINE_WIDTH;
	LcdAPI_Setup.Lcd_MaxLines				= T_HW_LCD_MAX_LINES;
	LcdAPI_Setup.Lcd_App_Language				= App_Language;
	LcdAPI_Setup.Language_Left2Right			= LANGUAGE_ENGLISH;
	LcdAPI_Setup.Language_Right2Left			= LANGUAGE_HEBREW;
	LcdAPI_Setup.func_isMsgToHalt_ClockDisplay		= Task_Application_isMsgToHalt_ClockDisplay;
        LcdAPI_Setup.func_LcdDRV_SetContrast                    = LcdDRV_SetContrast;
	LcdAPI_Init(&LcdAPI_Setup);
	Lib_Version_LcdAPI__2_0_0 = 0;	                        // Validate the LcdAPI_LIB version.
        LcdAPI_SetBackLight(ON);                                // Original inits from HOFIM code
        LcdDRV_SetContrast( 0x18);                              // Original inits from HOFIM code
}


/*----------------------------------------------------------------------------*/
static	void			Task_Init_Init_Util(void)
/*----------------------------------------------------------------------------*/
{
static	tUtil_API_Setup					UtilAPI_Setup;

	UtilAPI_Setup.func_Task_Timer_GetTimer		= Task_Timer_GetTimer;
	UtilAPI_Setup.func_Task_Hardware_Main		= Task_Hardware_Main;
	UtilAPI_Init(&UtilAPI_Setup);

	Lib_Version_UtilAPI__1_0_0 = 0;					// Validate the UtilAPI_LIB version.
}



/*----------------------------------------------------------------------------*/
static void                     Task_Init_Keyboard_Init(void)
/*----------------------------------------------------------------------------*/
{
static	tKeybd_Setup	Keyboard_Setup;
  
   Keyboard_Setup.func_Task_Timer_GetTimer      = Task_Timer_GetTimer;
   Keyboard_Setup.func_KeyboardDRV_Init         = KeyboardDRV_Init;
   Keyboard_Setup.func_KeyboardDRV_Scan         = KeyboardDRV_Scan;
   Keyboard_Setup.func_KeyboardDRV_GetKey       = KeyboardDRV_GetKey;
   Keyboard_Setup.func_BuzzAPI_Beep             = BuzzAPI_Beep;
          
   KeyboardAPI_Init(&Keyboard_Setup);
   Lib_Version_Keyboard_1_0_0 = 0;
}




/*----------------------------------------------------------------------------*/
void                            T_Init_TimerS_OVF(byte argumentOVF)
/*----------------------------------------------------------------------------*/
{
  
  
}



/*----------------------------------------------------------------------------*/
/*                            TFT module initialization                       */   
/*----------------------------------------------------------------------------*/
#if 0
void                            T_Init_Init_TFT(void)
/*----------------------------------------------------------------------------*/
{
     //============= Object member functions =============
   pTFT->func_Task_Timer_GetTimer       = Task_Timer_GetTimer;          //1. v
   pTFT->func_OpenPort                  = T_Init_TFTOpenPort;           //2. v
   pTFT->func_Util_SwapArray            = Util_SwapArray;               //6. v
   pTFT->HwDef_TMR0_TickInterval        = HW_DEF_TMR0_TICK_INTERVAL;    //7. v HW_DEF_TMR0_TICK_INTERVAL (usually it equals to 10msec)
   pTFT->pNvram_TFT                     = p_NVRAM_TFT_DISPLAY;          //8. v See T_App.h NVRAM for TFT
   //============= Pbject member variables/parameters =============
   App_TaskStatus[TASK_DISPLAY] = ENABLED;
   pTFT->App_TaskStatus_TASK_DISPLAY    = App_TaskStatus[TASK_DISPLAY]; //9. Represent App_TaskStatus[TASK_DISPLAY]

  
  
  

   // tTFT_API_Setup  TFT_Setup;
   pTFT->func_Task_Timer_GetTimer                       = Task_Timer_GetTimer;
   pTFT->func_OpenPort                                  = T_Init_TFTOpenPort;
   pTFT->HwDef_TMR0_TickInterval                        = HW_DEF_TMR0_TICK_INTERVAL;            // HW_DEF_TMR0_TICK_INTERVAL (usually it equals to 10msec)
   pTFT->func_Util_SwapArray                            = Util_SwapArray;
   Task_TFT_Init(pTFT); 
}



/*----------------------------------------------------------------------------*/
void                            T_Init_TFTOpenPort(void)
/*----------------------------------------------------------------------------*/
{

   /*** Communication Setting ***/ 
   // tUart  pTFT_Uart;
   pTFT_Uart->Com.Port                 = COM_6;
   pTFT_Uart->Com.BaudRate             = BAUD_RATE_9600;
   pTFT_Uart->TxFunc                   = TFT_API_Tx_ISR;
   pTFT_Uart->RxFunc                   = NULL;                 //TFT_Rx_ISR;
   UartDRV_SetPort( pTFT_Uart);
   /*****************************/
   // UartDRV_SetPort() returns back the 3 callback function, over 'task_Hardware_BillUartParams'.
   pTFT->func_Uart_TxByte                               = pTFT_Uart->Uart_TxByte;
   pTFT->func_Uart_TxEnd                                = pTFT_Uart->Uart_TxEnd;
}
#endif



/****************************************************************************************/
/*                                    End of File                                       */
/****************************************************************************************/