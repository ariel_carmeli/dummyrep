/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  FORMAT_1_VALUE       0x55
#define  FORMAT_2_VALUE       0xAA
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tParams_A_Rec        task_Rec_Type_A;
static   tParams_B_Rec        task_Rec_Type_B;
static   tParams_C_Rec        task_Rec_Type_C;
static   tParams_D_Rec        task_Rec_Type_D;
static   tParams_E_Rec        task_Rec_Type_E;
static   ulong                task_Address;
static   byte                 task_RecSize;
static   bool                 task_fCheckParams;
static   bool                 task_fReadParamSetToZero;
static   byte                 task_Format_State;
static   byte                 task_Format_1;
static   byte                 task_Format_2;
static   bool                 task_fPowerUp;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						Task_Params_HandleExtEvent( void);
static   void                 Task_Params_GetParamProperties( byte ParamType, usint ParamCode);
static   void                 Task_Params_CheckParams( void);
static   void                 Task_Params_SetDefaultValue( byte ParamType, usint  ParamCode);
static   void                 Task_Params_ClearAll( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Params_Init( void)
/*----------------------------------------------------------------------------*/
{
   task_fPowerUp  =  TRUE;
   //Task_Params_Format();
}


/*----------------------------------------------------------------------------*/
void                    		Task_Params_Main( void)
/*----------------------------------------------------------------------------*/
{
	if( App_TaskStatus[TASK_PARAMS] == DISABLED)
		return;

   if( (task_Format_1 == FORMAT_1_VALUE) && (task_Format_2 == FORMAT_2_VALUE))
   {
      // Don't set 'task_Format_1' and 'task_Format_2' to 0 (see Task_Params_ClearAll() case 6)
      Task_Params_ClearAll();
      return;
   }

   if( task_fCheckParams == TRUE)
   {
      task_fCheckParams =  FALSE;
      Task_Params_CheckParams();
   }

   if( task_fPowerUp == TRUE)
   {
   }

   Task_Params_HandleExtEvent();

   task_fPowerUp  =  FALSE;
}



/*----------------------------------------------------------------------------*/
bool                    		Task_Params_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
	return( EsqrAPI_IsBusy() || xSramAPI_IsBusy());
}



/*----------------------------------------------------------------------------*/
void                    		Task_Params_Format( void)
/*----------------------------------------------------------------------------*/
{
   task_Format_1     =  FORMAT_1_VALUE;
   task_Format_2     =  FORMAT_2_VALUE;
   task_Format_State =  0;
}



/*----------------------------------------------------------------------------*/
bool                    		Task_Params_Read( void  *pRec, byte ParamType, usint  ParamCode)
/*----------------------------------------------------------------------------*/
{
   if( pRec == NULL)
      return( FALSE);

   Task_Params_GetParamProperties( ParamType, ParamCode);
   if( task_RecSize == 0)
      return( FALSE);

   switch( ParamType)
   {
      case  PARAM_TYPE_A:
      case  PARAM_TYPE_B:
            EsqrAPI_Read_Address( pRec, task_RecSize, task_Address, TRUE);
            break;

      case  PARAM_TYPE_C:
      case  PARAM_TYPE_D:
      case  PARAM_TYPE_E:
            xSramAPI_Read_Address( pRec, task_RecSize, task_Address, TRUE);
            break;

      default:
            return( FALSE);
   }

   task_fReadParamSetToZero   =  FALSE;
   if( Util_IsBufferFullOf( pRec, task_RecSize, 0xFF) == TRUE)
   {
      memset( pRec, 0, task_RecSize);
      task_fReadParamSetToZero   =  TRUE;
   }

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
bool                    		Task_Params_Write( void *pRec, byte ParamType, usint  ParamCode)
/*----------------------------------------------------------------------------*/
{
   if( pRec == NULL)
      return( FALSE);

   Task_Params_GetParamProperties( ParamType, ParamCode);
   if( task_RecSize == 0)
      return( FALSE);

   switch( ParamType)
   {
      case  PARAM_TYPE_A:
      case  PARAM_TYPE_B:
            EsqrAPI_Write_Address( pRec, task_RecSize, task_Address, TRUE);
            break;

      case  PARAM_TYPE_C:
      case  PARAM_TYPE_D:
      case  PARAM_TYPE_E:
            xSramAPI_Write_Address( pRec, task_RecSize, task_Address, TRUE);
            break;

      default:
            return( FALSE);
   }

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Params_Read_Var( void  *pRec, usint Size, usint  ParamCode)
/*----------------------------------------------------------------------------*/
{
ulong    Address;


   Address  =  BASE_ADDRESS_VARS;
   Address  += ParamCode;

   xSramAPI_Read_Address( pRec, Size, Address, TRUE);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Params_Write_Var( void  *pRec, usint Size, usint  ParamCode)
/*----------------------------------------------------------------------------*/
{
ulong    Address;


   Address  =  BASE_ADDRESS_VARS;
   Address  += ParamCode;

   xSramAPI_Write_Address( pRec, Size, Address, TRUE);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Params_Read_String( byte ParamType, void  *pRec, usint Size, usint  ParamCode)
/*----------------------------------------------------------------------------*/
{
ulong    Address;


   Address  =  BASE_ADDRESS_STRINGS_16;   // default value (better than using a simple 0 value...)
   switch( ParamType)
   {
      case  PARAM_TYPE_D   :  // Str 16
            Address  =  BASE_ADDRESS_STRINGS_16;
            break;

      case  PARAM_TYPE_E   :  // Str 32
            Address  =  BASE_ADDRESS_STRINGS_32;
            break;

      default:
            return;
   }

   Address  += ParamCode;

   xSramAPI_Read_Address( pRec, Size, Address, TRUE);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Params_Write_String( byte ParamType, void  *pRec, usint Size, usint  ParamCode)
/*----------------------------------------------------------------------------*/
{
ulong    Address;


   Address  =  BASE_ADDRESS_STRINGS_16;   // default value (better than using a simple 0 value...)
   switch( ParamType)
   {
      case  PARAM_TYPE_D   :  // Str 16
            Address  =  BASE_ADDRESS_STRINGS_16;
            break;

      case  PARAM_TYPE_E   :  // Str 32
            Address  =  BASE_ADDRESS_STRINGS_32;
            break;

      default:
            return;
   }

   Address  += ParamCode;

   xSramAPI_Write_Address( pRec, Size, Address, TRUE);
}



#if 0
/*----------------------------------------------------------------------------*/
void                    		Task_Params_RestoreSettings( void)
/*----------------------------------------------------------------------------*/
{
tParam_Code    ParamCode;


   for( ParamCode = (tParam_Code)0; ParamCode < PARAM_A_LAST; ParamCode ++)
   {
      xSramAPI_Read( &task_Rec_Type_A, sizeof( task_Rec_Type_A), xSRAM_TYPE_BACKUP_PARAMS, ParamCode, TRUE);
      Task_Params_Write( &task_Rec_Type_A, ParamCode);
   }
}



/*----------------------------------------------------------------------------*/
void                    		Task_Params_SaveSettings( void)
/*----------------------------------------------------------------------------*/
{
tParam_Code    ParamCode;


   for( ParamCode = (tParam_Code)0; ParamCode < PARAM_A_LAST; ParamCode ++)
   {
      Task_Params_Read( &task_Rec_Type_A, ParamCode);
      xSramAPI_Write( &task_Rec_Type_A, sizeof( task_Rec_Type_A), xSRAM_TYPE_BACKUP_PARAMS, ParamCode, TRUE);
   }
}
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Params_SetFactoryDefaults( void)
/*----------------------------------------------------------------------------*/
{
byte           Language;
byte           ParamType;
usint          ParamCode;


   // Read 'Language' setting
   Task_Params_Read( &task_Rec_Type_C, PARAM_TYPE_C, PARAM_C_HMI_LANGUAGE_01);
   Language =  task_Rec_Type_C;

   for( ParamCode = 0; ParamCode < PARAM_A_LAST; ParamCode ++)    Task_Params_SetDefaultValue( PARAM_TYPE_A    , ParamCode);
   for( ParamCode = 0; ParamCode < PARAM_B_LAST; ParamCode ++)    Task_Params_SetDefaultValue( PARAM_TYPE_B    , ParamCode);
   for( ParamCode = 0; ParamCode < PARAM_C_LAST; ParamCode ++)    Task_Params_SetDefaultValue( PARAM_TYPE_C    , ParamCode);
   //for( ParamCode = 0; ParamCode < STR_D_LAST  ; ParamCode ++) Task_Params_SetDefaultValue( PARAM_TYPE_D, ParamCode);
   for( ParamCode = 0; ParamCode < VAR_RECEIPT; ParamCode ++)     Task_Params_SetDefaultValue( PARAM_TYPE_VARS , ParamCode);

   // Restore 'Language' setting
   if( Language >= MAX_LANGUAGES)
      Language =  0;

   task_Rec_Type_C   =  Language;
   Task_Params_Write( &task_Rec_Type_C, PARAM_TYPE_C, PARAM_C_HMI_LANGUAGE_01);

   BuzzAPI_SetMode( FRAGMENTED_3, 1);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Params_HandleExtEvent( void)
/*----------------------------------------------------------------------------*/
{
   App_TaskEvent[TASK_PARAMS] =  EV_APP_VOID;

   if( App_TaskEvent[TASK_SETUP] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_RESET_INFO))
      {
         // 1'st of 3 RESET steps
         task_Format_State =  0;
         Task_Params_SetFactoryDefaults();
      }
   }

   if( App_TaskEvent[TASK_APPL] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_REQ_TO_ERASE_ALL_PARAMS))
         Task_Params_SetFactoryDefaults();         
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Params_GetParamProperties( byte ParamType, usint ParamCode)
/*----------------------------------------------------------------------------*/
{
   task_RecSize   =  0;
   task_Address   =  0;

   switch( ParamType)
   {
      case  PARAM_TYPE_A:
            task_RecSize   =  sizeof( tParams_A_Rec);
            task_Address   =  BASE_ADDRESS_PARAMS_GROUP_A;
            break;

      case  PARAM_TYPE_B:
            task_RecSize   =  sizeof( tParams_B_Rec);
            task_Address   =  BASE_ADDRESS_PARAMS_GROUP_B;
            break;

      case  PARAM_TYPE_C:
            task_RecSize   =  sizeof( tParams_C_Rec);
            task_Address   =  BASE_ADDRESS_PARAMS_GROUP_C;
            break;

      case  PARAM_TYPE_D:
            task_RecSize   =  sizeof( tParams_D_Rec);
            task_Address   =  BASE_ADDRESS_PARAMS_GROUP_D;
            break;

      case  PARAM_TYPE_E:
            task_RecSize   =  sizeof( tParams_E_Rec);
            task_Address   =  BASE_ADDRESS_PARAMS_GROUP_E;
            break;

      default:
            return;
   }

   task_Address   += (ParamCode * task_RecSize);
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Params_CheckParams( void)
/*----------------------------------------------------------------------------*/
{
usint          ParamCode;


   for( ParamCode = 0; ParamCode < PARAM_A_LAST; ParamCode ++)
   {
      Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, ParamCode);

      if( task_fReadParamSetToZero == TRUE)
         Task_Params_SetDefaultValue( PARAM_TYPE_A, ParamCode);
   }

   for( ParamCode = 0; ParamCode < PARAM_B_LAST; ParamCode ++)
   {
      Task_Params_Read( task_Rec_Type_B, PARAM_TYPE_B, ParamCode);

      if( task_fReadParamSetToZero == TRUE)
         Task_Params_SetDefaultValue( PARAM_TYPE_B, ParamCode);
   }

   for( ParamCode = 0; ParamCode < PARAM_C_LAST; ParamCode ++)
   {
      Task_Params_Read( &task_Rec_Type_C, PARAM_TYPE_C, ParamCode);

      if( task_fReadParamSetToZero == TRUE)
         Task_Params_SetDefaultValue( PARAM_TYPE_C, ParamCode);
   }

   /*
   for( ParamCode = 0; ParamCode < STR_D_LAST; ParamCode ++)
   {
      Task_Params_Read( task_Rec_Type_D, PARAM_TYPE_D, ParamCode);

      if( task_fReadParamSetToZero == TRUE)
         Task_Params_SetDefaultValue( PARAM_TYPE_D, ParamCode);
   }
   */
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Params_SetDefaultValue( byte ParamType, usint  ParamCode)
/*----------------------------------------------------------------------------*/
{
   switch( ParamType)
   {
      case  PARAM_TYPE_A:
            task_Rec_Type_A   =  0;

            switch( ParamCode)
            {
               case  PARAM_A_LANGUAGE                 :  task_Rec_Type_A   =  LANGUAGE_HEBREW;  break;
               case  PARAM_A_PASSWORD_LEVEL_1         :  task_Rec_Type_A   =  1111;             break;
               case  PARAM_A_PASSWORD_LEVEL_2         :  task_Rec_Type_A   =  2222;             break;
               case  PARAM_A_PASSWORD_LEVEL_3         :  task_Rec_Type_A   =  3333;             break;
               case  PARAM_A_HOPPER_TIMEOUT_DISPENSE  :  task_Rec_Type_A   =  50;               break;   // in ms units
               case  PARAM_A_HOPPER_TIMEOUT_EMPTY     :  task_Rec_Type_A   =  5;                break;
               case  PARAM_A_HOPPER_A_COIN_VALUE      :  task_Rec_Type_A   =  100;              break;   // Hopper A:   1.000 NIS
               case  PARAM_A_HOPPER_B_COIN_VALUE      :  task_Rec_Type_A   =  200;              break;   // Hopper B:   2.00  NIS
               case  PARAM_A_HOPPER_C_COIN_VALUE      :  task_Rec_Type_A   =  1000;             break;   // Hopper C:   10.00 NIS
               case  PARAM_A_HOPPER_D_COIN_VALUE      :  task_Rec_Type_A   =  1000;             break;   // Hopper D:   10.00 NIS
               case  PARAM_A_HOPPER_A_CAPACITY        :  task_Rec_Type_A   =  300;              break;   // Hopper A:   Max. coins in hopper A
               case  PARAM_A_HOPPER_B_CAPACITY        :  task_Rec_Type_A   =  300;              break;   // Hopper B:   Max. coins in hopper B
               case  PARAM_A_HOPPER_C_CAPACITY        :  task_Rec_Type_A   =  300;              break;   // Hopper C:   Max. coins in hopper C
               case  PARAM_A_HOPPER_D_CAPACITY        :  task_Rec_Type_A   =  300;              break;   // Hopper D:   Max. coins in hopper D
            }

            Task_Params_Write( &task_Rec_Type_A, ParamType, ParamCode);
            break;

      case  PARAM_TYPE_B:
            memset( task_Rec_Type_B, 0, sizeof( task_Rec_Type_B));

            Task_Params_Write( task_Rec_Type_B, ParamType, ParamCode);
            break;

      case  PARAM_TYPE_C:
            task_Rec_Type_C   =  0;

            switch( ParamCode)
            {
               //case  PARAM_C_COMPANY_NUMBER           :  task_Rec_Type_C   =  510266208L;       break;   // ������ ���� - ��' �.�
               case  PARAM_C_HMI_LANGUAGE_01          :  task_Rec_Type_C   =  LANGUAGE_HEBREW;  break;
               case  PARAM_C_TIME_AUTO_CANCEL         :  task_Rec_Type_C   =  90;               break;
               case  PARAM_C_COIN_MIN_VALUE           :  task_Rec_Type_C   =  100;              break;   // 1.00 NIS coin
               case  PARAM_C_COIN_MAX_VALUE           :  task_Rec_Type_C   =  1000;             break;   // 10.00 NIS coin
               case  PARAM_C_BILL_MIN_VALUE           :  task_Rec_Type_C   =  2000;             break;   // 20.00 NIS bill
               case  PARAM_C_BILL_MAX_VALUE           :  task_Rec_Type_C   =  10000;            break;   // 100.00 NIS bill
               case  PARAM_C_HPR_AUTO_EMPTY           :  task_Rec_Type_C   =  TRUE;             break;
               case  PARAM_C_HPR_TIMEOUT_PAY          :  task_Rec_Type_C   =  5;                break;   // sec
               case  PARAM_C_HPR_XTRA_TIME_ON         :  task_Rec_Type_C   =  50;               break;   // msec

               case  PARAM_C_HPR_01_VALUE             :  task_Rec_Type_C   =  100;              break;   // Hopper A: 1.00 NIS coin
               case  PARAM_C_HPR_02_VALUE             :  task_Rec_Type_C   =  200;              break;   // Hopper B: 2.00 NIS coin
               case  PARAM_C_HPR_03_VALUE             :  task_Rec_Type_C   =  1000;             break;   // Hopper C: 10.00 NIS coin
               case  PARAM_C_HPR_04_VALUE             :  task_Rec_Type_C   =  1000;             break;   // Hopper D: 10.00 NIS coin

               case  PARAM_C_HPR_01_MAX_LEVEL         :  task_Rec_Type_C   =  300;              break;
               case  PARAM_C_HPR_02_MAX_LEVEL         :  task_Rec_Type_C   =  300;              break;
               case  PARAM_C_HPR_03_MAX_LEVEL         :  task_Rec_Type_C   =  300;              break;
               case  PARAM_C_HPR_04_MAX_LEVEL         :  task_Rec_Type_C   =  300;              break;

               case  PARAM_C_BILL_f_STCKR             :  task_Rec_Type_C   =  TRUE;             break;   //
               case  PARAM_C_BILL_STCKR_SIZE          :  task_Rec_Type_C   =  400;              break;   // Max bills in stacker
               case  PARAM_C_CRDT_SRVR_TIMOUT         :  task_Rec_Type_C   =  20;               break;   // Credit-Card Server timeout (sec)
               case  PARAM_C_LVL_EXACT_CHANGE         :  task_Rec_Type_C   =  5000;             break;   // 50.00 NIS
               case  PARAM_C_MAX_MONEY_CHANGE         :  task_Rec_Type_C   =  10000;            break;   // 100.00 NIS
               case  PARAM_C_MAX_MONEY_CANCEL         :  task_Rec_Type_C   =  1500;             break;   // 15.00 NIS
               case  PARAM_C_DIGITEL_TRACK_2_LENGTH   :  task_Rec_Type_C   =  17;               break;
               case  PARAM_C_DIGITEL_CARD_ID_START    :  task_Rec_Type_C   =  0;                break;
               case  PARAM_C_DIGITEL_CARD_ID_LEN      :  task_Rec_Type_C   =  4;                break;
               case  PARAM_C_DIGITEL_CARD_ID_VALUE    :  task_Rec_Type_C   =  2145;             break;
            }

            Task_Params_Write( &task_Rec_Type_C, ParamType, ParamCode);
            break;

      case  PARAM_TYPE_D:
            /*
            memset( task_Rec_Type_D, 0, sizeof( task_Rec_Type_D));
            Task_Params_Write( task_Rec_Type_D, ParamType, ParamCode);
            */
            break;

      case  PARAM_TYPE_VARS:
            {
            tParams_Var_Rec   Var;

               Var   =  0;
               
               switch( ParamCode)
               {
                  case  VAR_CURRENT_Z_NUMBER          :  Var               =  1;                break;
               }
               
               Task_Params_Write_Var( &Var, sizeof( Var), ParamCode);
            }
            break;

      default:
            return;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Params_ClearAll( void)
/*----------------------------------------------------------------------------*/
{
static   byte     Language;
static   usint    ParamCode;


   switch( task_Format_State)
   {
      case  0:
            SET_BIT( App_TaskEvent[TASK_PARAMS], EV_APP_PARAMS_FORMAT_ACTIVE);

            // Read 'Language' setting
            Task_Params_Read( &task_Rec_Type_C, PARAM_TYPE_C, PARAM_C_HMI_LANGUAGE_01);
            Language =  task_Rec_Type_C;
            break;

      case  1:
            task_Rec_Type_A   =  0;
            for( ParamCode = 0; ParamCode < PARAM_A_LAST; ParamCode ++) Task_Params_Write( &task_Rec_Type_A, PARAM_TYPE_A, ParamCode);
            break;

      case  2:
            memset( task_Rec_Type_B, 0, sizeof( task_Rec_Type_B));
            for( ParamCode = 0; ParamCode < PARAM_B_LAST; ParamCode ++) Task_Params_Write( &task_Rec_Type_B, PARAM_TYPE_B, ParamCode);
            break;

      case  3:
            task_Rec_Type_C   =  0;
            for( ParamCode = 0; ParamCode < PARAM_C_LAST; ParamCode ++) Task_Params_Write( &task_Rec_Type_C, PARAM_TYPE_C, ParamCode);
            break;

      case  4:
            //memset( task_Rec_Type_D, 0xFF, sizeof( task_Rec_Type_D));
            //for( ParamCode = 0; ParamCode < STR_D_END; ParamCode ++)    Task_Params_Write( &task_Rec_Type_D, PARAM_TYPE_D, ParamCode);
            break;

      case  5:
            task_Rec_Type_C   =  0;
            {
            ulong    Address;

               for( Address = BASE_ADDRESS_VARS; Address < (BASE_ADDRESS_VARS + MAX_SIZE_VARS); Address += sizeof( task_Rec_Type_C))
                  xSramAPI_Write_Address( &task_Rec_Type_C, sizeof( task_Rec_Type_C), Address, TRUE);
            }
            break;

      case  6:
            // Restore 'Language' setting
            if( Language >= MAX_LANGUAGES)
               Language =  0;

            task_Rec_Type_C   =  Language;
            Task_Params_Write( &task_Rec_Type_C, PARAM_TYPE_C, PARAM_C_HMI_LANGUAGE_01);

            task_Format_State =  0;
            task_Format_1     =  0;
            task_Format_2     =  0;

            SET_BIT( App_TaskEvent[TASK_PARAMS], EV_APP_PARAMS_FORMAT_END);
            break;

      default:
            break;
   }

   task_Format_State ++;
}

