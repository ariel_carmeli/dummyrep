/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define     MEM_PAGE_SIZE           0x8000
#define     MAX_SIZE_BUF_PARAMS     32 // 8 X long
#define     TIME_VOID_RX_COMMANDS   15 // seconds
#define     TIME_EMERGENCY          5  // seconds

#define TIBA_PAY_EN     0

/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   TYPE_CHAR   ,
   TYPE_BYTE   ,
   TYPE_USINT  ,
   TYPE_ULONG  ,
   TYPE_STRING ,
}  tType;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tParams_C_Rec        task_Param_C_Rec;
//static   Cmd_Q_Data           task_StatusStruct;
static   tParams_Var_Rec      task_Param_Var;
static   tServerPacket       *task_pServerPacket_Rx;
static   tServerPacket        task_ServerPacket_Tx;
static   tServer_MasterRec    task_MasterRec;
static   tServer_SlaveRec     task_SlaveRec;
static   tLogEmergency_Info  *task_pEmergency_Info;
static   tLogTrans_Info      *task_pTrans_Info;
static   tLogEvent_Info      *task_pEvent_Info;
static   tTimerSec            task_Timer_ReadParams;
static   tTimerSec            task_Timer_EquipmentID;           //task_MachineID
static   usint                task_TxBufferIdx;
//static   char                 task_TxBuffer[SERVER_MAX_LEN_BUFFER];
static   bool                 task_fPowerup;
//static   bool                 task_fRequest_Z_Command;
static   tServerReplyCmd      task_TxCommand;
static   byte                 task_EquipmentID;
static   byte                 task_Buf_Params[MAX_SIZE_BUF_PARAMS];
static   byte                 task_Cmd_E_Type;
static   bool                 task_fReadParams;
static   bool                 task_fRequestForEmergency;
//static   bool                 task_fParkParam;
static   bool                 task_fMode_IamMaster;
static   bool                 task_fRxBroadcastMsg;

static   tTimerSec            task_Timer_SendStatus;
static   tTimerSec            task_Timer_RxActive;
static   tTimerSec            task_Timer_Emergency;
static   byte                 task_Time_SendStatus;
static   bool                 task_fSendStatus;
static   bool                 task_fReplyToAnyMsg;

// Parameters for replying to the Server
static   byte                 task_Page;
static   usint                task_AddressInPage;
static   ulong                task_Address;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						Task_Server_HandleTimer( void);
static   void                 Task_Server_ReadDB( void);
static   void                 Task_Server_ReadParams( void);
static	void						Task_Server_ReadStatusVars( void);
static	void						Task_Server_HandleExtEvent( void);
static	void						Task_Server_HandleComm( void);
static	void						Task_Server_HandleComm_Rx( void);
static   bool                 Task_Server_IsMsg4Me( void);
static	void						Task_Server_HandleComm_Tx( void);
static	void						Task_Server_AddObject( tType  Type, void  *pData, byte   Len);
static	void						Task_Server_EraseAllData( void);
/*----------------------------------------------------------------------------*/
#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Server_Init( void)
/*----------------------------------------------------------------------------*/
{
   memset( /*&task_StatusStruct*/p_NVRAM_SERVER_Q_CMD, 0, sizeof( Cmd_Q_Data));
   task_fPowerup                 =  TRUE;
   task_fReadParams              =  TRUE;
   task_TxCommand                =  SERVER_REPLY_VOID;
   //task_fRequest_Z_Command       =  TRUE;
   ServerAPI_Init( COM_1);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Server_Main( void)
/*----------------------------------------------------------------------------*/
{
	if( App_TaskStatus[TASK_SERVER] == DISABLED)
		return;

   Task_Server_HandleTimer();

   if( task_Timer_EquipmentID == 0)
   {
      task_Timer_EquipmentID  =  Task_Timer_SetTimeSec( 60);
      ClockAPI_NVRAM_Read( (byte *)&task_EquipmentID, sizeof( task_EquipmentID), RTC_NVRAM_TYPE_MACHINE_ID);
   }

   if( task_fPowerup == TRUE)
   {
      Task_Server_ReadDB();
      Task_Server_ReadParams();
      Task_Server_ReadStatusVars();
   }

   ServerAPI_Main();

   Task_Server_HandleExtEvent();
   Task_Server_HandleComm();

   task_fPowerup  =  FALSE;
}



/*----------------------------------------------------------------------------*/
Cmd_Q_Data                   *Task_Server_GetStatusStructPtr( void)
/*----------------------------------------------------------------------------*/
{
   return( p_NVRAM_SERVER_Q_CMD/*&task_StatusStruct*/);
}



/*----------------------------------------------------------------------------*/
void						         Task_Server_SaveStatusVars( void)
/*----------------------------------------------------------------------------*/
{
   //Task_Params_Write_Var( &p_NVRAM_SERVER_Q_CMD->Qtty_Coins_Hopper_A /*&task_StatusStruct.Qtty_Coins_Hopper_A*/, (VAR_STATUS_END - VAR_STATUS_START), VAR_STATUS_QTTY_COINS_HOPPER_A);
#if TIBA_PAY_EN
   Task_Params_Write_Var( task_StatusStruct.Qtty_Coins_Hopper, (VAR_STATUS_END - VAR_STATUS_START), VAR_STATUS_QTTY_COINS_HOPPER_A);
#endif
}



/*----------------------------------------------------------------------------*/
void                          Task_Server_SendStatus( void)
/*----------------------------------------------------------------------------*/
{
   task_fSendStatus  =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                          Task_Server_SetEquipmentID( byte   EquipmentID)
/*----------------------------------------------------------------------------*/
{
   task_EquipmentID  =  (EquipmentID + 210);   // 210 - Prefix of POF machine (like APS-30)
   ClockAPI_NVRAM_Write( (byte *)&task_EquipmentID, sizeof( task_EquipmentID), RTC_NVRAM_TYPE_MACHINE_ID);
   task_Timer_EquipmentID  =  0;                        //task_MachineID
}



/*----------------------------------------------------------------------------*/
byte                          Task_Server_GetEquipmentID( void)
/*----------------------------------------------------------------------------*/
{
   return( task_EquipmentID);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Server_IsServerOnline( void)
/*----------------------------------------------------------------------------*/
{
   return( task_Timer_RxActive > 0);
}



/*----------------------------------------------------------------------------*/
void                          Task_Server_SetMode_IamMaster( bool fMode)
/*----------------------------------------------------------------------------*/
{
   task_fMode_IamMaster =  fMode;   // set if I am a Master-machine (acts like CT-20)
}



/*----------------------------------------------------------------------------*/
void                          Task_Server_SetMode_ReplyToAnyMsg( bool fMode)
/*----------------------------------------------------------------------------*/
{
   task_fReplyToAnyMsg  =  fMode;
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Server_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( task_Timer_ReadParams     <  Gap)  task_Timer_ReadParams      =  0; else  task_Timer_ReadParams      -= Gap;
      if( task_Timer_EquipmentID    <  Gap)  task_Timer_EquipmentID     =  0; else  task_Timer_EquipmentID     -= Gap;          //task_MachineID
      if( task_Timer_SendStatus     <  Gap)  task_Timer_SendStatus      =  0; else  task_Timer_SendStatus      -= Gap;
      if( task_Timer_RxActive       <  Gap)  task_Timer_RxActive        =  0; else  task_Timer_RxActive        -= Gap;
      if( task_Timer_Emergency      <  Gap)  task_Timer_Emergency       =  0; else  task_Timer_Emergency       -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Server_ReadDB( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Server_ReadParams( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
static	void						Task_Server_ReadStatusVars( void)
/*----------------------------------------------------------------------------*/
{
   //Task_Params_Read_Var( &p_NVRAM_SERVER_Q_CMD->Qtty_Coins_Hopper_A /*&task_StatusStruct.Qtty_Coins_Hopper_A*/, (VAR_STATUS_END - VAR_STATUS_START), VAR_STATUS_QTTY_COINS_HOPPER_A);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Server_HandleExtEvent( void)
/*----------------------------------------------------------------------------*/
{
   if( App_TaskEvent[TASK_SERVER] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_READ_PARAMS))
         Task_Server_ReadParams();

      //if( BIT_IS_SET( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_L_POINTERS))
         //task_fRequest_Z_Command =  TRUE;
   }

   App_TaskEvent[TASK_SERVER] =  EV_APP_VOID;

   if( (task_fReadParams == TRUE) && (task_Timer_ReadParams == 0))
   {
      task_fReadParams  =  FALSE;
      SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_READ_PARAMS);

      Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_TIME_SEND_STATUS);
      task_Time_SendStatus =  task_Param_C_Rec;
   }

   if( App_TaskEvent[TASK_APPL] != EV_APP_VOID)
   {
      /*A.C 17/7/2016  T_Server.c should handle DIGITEL only
      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_EMERGENCY_CREDIT))
      {
         task_fRequestForEmergency =  TRUE;
         task_Timer_Emergency =  Task_Timer_SetTimeSec( TIME_EMERGENCY);
         task_Cmd_E_Type   =  EMERGENCY_TYPE_CREDIT_CARD;
      }*/

      // Handle EMERGENCY for Digitel
      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_EMERGENCY_DIGITEL_AUTHENTICATION))
      {
         task_fRequestForEmergency =  TRUE;
         task_Timer_Emergency =  Task_Timer_SetTimeSec( TIME_EMERGENCY);
         task_Cmd_E_Type   =  EMERGENCY_TYPE_DIGITEL_AUTHENTICATION;            // Emergency message
      }
     
      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_NEW_MACHINE_ID))
         task_Timer_EquipmentID  =  0;                                          //task_MachineID

      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_REQ_TO_ERASE_ALL_DATA))
         Task_Server_EraseAllData();
   }
#if 0   // 
   else
   {
      // App_TaskEvent[TASK_APPL] is EV_APP_VOID
      // Handle EMERGENCY for TibaPay Communication Loss
      if( BIT_IS_SET( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_COMM_LOSS))   // 
      {
         CLEAR_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_COMM_LOSS);
         task_fRequestForEmergency =  TRUE;
         task_Timer_Emergency =  Task_Timer_SetTimeSec( TIME_EMERGENCY);
         task_Cmd_E_Type   =  EMERGENCY_TYPE_TIBAPAY_COMM_LOSS;            // Emergency message
      }
      
      // Handle EMERGENCY for TibaPay Communication Loss
      if( BIT_IS_SET( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_COMM_BACK))   // 
      {
         CLEAR_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_COMM_BACK);
         task_fRequestForEmergency =  TRUE;
         task_Timer_Emergency =  Task_Timer_SetTimeSec( TIME_EMERGENCY);
         task_Cmd_E_Type   =  EMERGENCY_TYPE_TIBAPAY_COMM_BACK;            // Emergency message
         
      }
   }
#endif
}



/*----------------------------------------------------------------------------*/
static	void						Task_Server_HandleComm( void)
/*----------------------------------------------------------------------------*/
{
   if( ServerAPI_IsBusy() == TRUE)
      return;

   /* Receive */
   //if( task_Timer_RxActive == 0) // if lost connection with the server, enforce the server (once resumed) to send back 'Z' command
   //   task_fRequest_Z_Command    =  TRUE;

   if( ServerAPI_IsRxNewPacket() == TRUE)
      Task_Server_HandleComm_Rx();

   /* Transmit */
   if( task_TxCommand != SERVER_REPLY_VOID)
   {
      Task_Server_HandleComm_Tx();

      task_ServerPacket_Tx.pBuffer  =  (byte *)p_NVRAM_SERVER_TX_PACKET;/*task_TxBuffer;*/
      task_ServerPacket_Tx.Command  =  task_TxCommand;
      task_TxCommand                =  SERVER_REPLY_VOID;

      ServerAPI_SendData( &task_ServerPacket_Tx);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Server_HandleComm_Rx( void)
/*----------------------------------------------------------------------------*/
{
usint    Idx;
byte     bData;
usint    uData;
ulong    lData;

   task_TxCommand =  SERVER_REPLY_ACK;
   Idx            =  0;

   task_pServerPacket_Rx   =  ServerAPI_GetData();

   if( task_pServerPacket_Rx == NULL)
      return;

   // check if message came from ParkParam (first message after at least 5 seconds of silence)
   /*
   if( task_Timer_RxActive == 0)
   {
      task_fParkParam   =  FALSE;

      switch( task_pServerPacket_Rx->Command)
      {
         // only these commands are used by ParkParam
         case  SERVER_CMD_PARAMS_READ  :  // 'R'
         case  SERVER_CMD_PARAMS_WRITE :  // 'W'
               task_fParkParam   =  TRUE;
               break;
      }
   }
   */
   
   SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_ACTIVE);
   task_Timer_RxActive  =  Task_Timer_SetTimeSec( 5);

   task_fRxBroadcastMsg =  FALSE;
   if( Task_Server_IsMsg4Me() == FALSE)
   {
      task_TxCommand =  SERVER_REPLY_VOID;
      return;
   }

   switch( task_pServerPacket_Rx->Command)
   {
      case  SERVER_CMD_ACK             :  // '.'
            break;

      case  SERVER_CMD_NACK            :  // '*'
            break;

      case  SERVER_CMD_DO_ACTION       :  // 'D'
            memcpy( &task_MasterRec.Cmd_D, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_D));
            Task_Application_SetDoAction( task_MasterRec.Cmd_D.SubCmd, &task_MasterRec.Cmd_D.Data);
            break;

      case  SERVER_CMD_INC_EMERGENCY   :  // 'E'
            break;

      case  SERVER_CMD_FORMAT          :  // 'F'
            memcpy( &bData, (task_pServerPacket_Rx->pBuffer + Idx), sizeof( bData));   Idx +=   sizeof( bData);
            switch( bData)
            {
               case  1:
                     SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_L_POINTERS);
                     break;

               case  2:
                     SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_V_POINTERS);
                     break;

               case  3:
                     SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_Z_POINTERS);
                     break;

               //case  4:
               //      SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_DEL_CRNT_Z_RECORD);
                     break;

               //case  5:
               //      SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_VITAL_PARAMS);
                     break;

               case  0xFF:
                     Task_Server_EraseAllData();
                     break;
            }
            break;

      case  SERVER_CMD_GET_STATUS      :  // 'L'
            memcpy( &task_MasterRec.Cmd_L, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_L));
            Util_SwapArray( &task_MasterRec.Cmd_L.Idx_R_Trans, sizeof( task_MasterRec.Cmd_L.Idx_R_Trans));
            task_TxCommand =  SERVER_REPLY_TANSACTION;
            break;

      case  SERVER_CMD_READ_TRANS      :  // 'l'
            break;

      case  SERVER_CMD_PARAMS_READ     :  // 'R'
            memcpy( &task_MasterRec.Cmd_R, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_R));
            // Memory page
            task_Page            =  task_MasterRec.Cmd_R.Page;

            // Address within memory page
            Util_SwapArray( &task_MasterRec.Cmd_R.Offset, sizeof( task_MasterRec.Cmd_R.Offset));
            task_AddressInPage   =  task_MasterRec.Cmd_R.Offset;
            task_AddressInPage   -= MEM_PAGE_SIZE;

            // Actual address = (Page * Address within a page)
            task_Address         =  task_Page;
            task_Address         *= MEM_PAGE_SIZE;
            task_Address         += task_AddressInPage;

            task_TxCommand =  SERVER_REPLY_PARAMS_READ;
            break;

      case  SERVER_CMD_SET_TIME_DATE   :  // 'T'
            {
            tClock   Clock;

               memcpy( &task_MasterRec.Cmd_T, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_T));
               // ddmmyyhhmmss
               Clock.Day      =  (Util_BCDInt2Int( task_MasterRec.Cmd_T.Day_BCD     ) / 100);
               Clock.Month    =  (Util_BCDInt2Int( task_MasterRec.Cmd_T.Month_BCD   ) / 100);
               Clock.Year     =  (Util_BCDInt2Int( task_MasterRec.Cmd_T.Year_BCD    ) / 100);
               Clock.Hour     =  (Util_BCDInt2Int( task_MasterRec.Cmd_T.Hour_BCD    ) / 100);
               Clock.Minutes  =  (Util_BCDInt2Int( task_MasterRec.Cmd_T.Minutes_BCD ) / 100);
               Clock.Seconds  =  (Util_BCDInt2Int( task_MasterRec.Cmd_T.Seconds_BCD ) / 100);
               ClockAPI_SetClock( &Clock);
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_DATE_TIME_UPDATED);
            }
            break;

      case  SERVER_CMD_EVENT_CONFIRM   :  // 'V'               Rx
            Task_Log_Event_Step_Fwd_Server();
            break;

      case  SERVER_CMD_PARAMS_WRITE    :  // 'W'
            memcpy( &task_MasterRec.Cmd_W, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_W));
            // Memory page
            task_Page            =  task_MasterRec.Cmd_W.Page;

            // Address within memory page
            Util_SwapArray( &task_MasterRec.Cmd_W.Offset, sizeof( task_MasterRec.Cmd_W.Offset));
            task_AddressInPage   =  task_MasterRec.Cmd_W.Offset;
            task_AddressInPage   -= MEM_PAGE_SIZE;

            // Actual address = (Page * Address within a page)
            task_Address         =  task_Page;
            task_Address         *= MEM_PAGE_SIZE;
            task_Address         += task_AddressInPage;

            Idx   =  sizeof( task_MasterRec.Cmd_W.Page) + sizeof( task_MasterRec.Cmd_W.Offset);

            if( task_pServerPacket_Rx->BufSize > Idx)
               xSramAPI_Write_Address( task_MasterRec.Cmd_W.Data, (task_pServerPacket_Rx->BufSize - Idx), task_Address, TRUE);

            task_TxCommand =  SERVER_REPLY_PARAMS_WRITE;

            task_Timer_ReadParams   =  Task_Timer_SetTimeSec( 10);
            task_fReadParams        =  TRUE;
            break;

      case  SERVER_CMD_END_Z_SESSION   :  // 'Y'
            memcpy( &task_MasterRec.Cmd_Y, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_Y));
            Util_SwapArray( &task_MasterRec.Cmd_Y.New_Z, sizeof( task_MasterRec.Cmd_Y.New_Z));

            if( Task_Application_Close_Z( task_MasterRec.Cmd_Y.New_Z) == TRUE)
            {
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_L_POINTERS);
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_V_POINTERS);
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_Z_POINTERS);
            }
            break;

      case  SERVER_CMD_ID              :  // 'Z'
            
            memcpy( &task_MasterRec.Cmd_Z, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_Z));

            App_Version_Server   =  task_MasterRec.Cmd_Z.Version;
            //task_fRequest_Z_Command =  FALSE;
            SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RX_INIT_CMD);
            task_TxCommand =  SERVER_REPLY_ID;
            break;
   }

   if( task_fRxBroadcastMsg == TRUE)
      task_TxCommand =  SERVER_REPLY_VOID;
}



/*----------------------------------------------------------------------------*/
static   bool                 Task_Server_IsMsg4Me( void)
/*----------------------------------------------------------------------------*/
{
byte     EquipmentID;
bool     fResult;


   task_fRxBroadcastMsg =  FALSE;
   EquipmentID          =  0;
   fResult              =  TRUE;

   switch( task_pServerPacket_Rx->Command)
   {
      case  SERVER_CMD_ACK             :  // '.'
      case  SERVER_CMD_NACK            :  // '*'
      case  SERVER_CMD_INC_EMERGENCY   :  // 'E'
      case  SERVER_CMD_EVENT_CONFIRM   :  // 'V'
      case  SERVER_CMD_PARAMS_READ     :  // 'R'
      case  SERVER_CMD_SET_TIME_DATE   :  // 'T'
      case  SERVER_CMD_PARAMS_WRITE    :  // 'W'
            break;

      case  SERVER_CMD_DO_ACTION       :  // 'D'
            memcpy( &task_MasterRec.Cmd_D, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_D));
            switch( task_MasterRec.Cmd_D.SubCmd)
            {
               case  D_CMD_EMERGENCY_CREDIT_CARD   :  // 0x10
                     EquipmentID =  task_MasterRec.Cmd_D.Data.CreditCard.EquipmentID;
                     break;

               case  D_CMD_DB                      :  // 0x20
                     EquipmentID =  task_MasterRec.Cmd_D.Data.DB.EquipmentID;
                     break;

               case  D_CMD_DIGITEL_AUTHENTICATION  :  // 0x81
                     EquipmentID =  task_MasterRec.Cmd_D.Data.Digitel.EquipmentID;
                     break;

               case  D_CMD_PRODUCT_ENABLE_DISABLE  :  // 0x82
                     EquipmentID =  task_MasterRec.Cmd_D.Data.Product_EnaDis.EquipmentID;
                     break;

               case  D_CMD_TECHNICIAN              :  // 0x99
                     EquipmentID =  task_MasterRec.Cmd_D.Data.Tech.EquipmentID;
                     break;
            }
            break;
/*
case  SERVER_CMD_FORMAT          :  // 'F'
      memcpy( &bData, (task_pServerPacket_Rx->pBuffer + Idx), sizeof( bData));   Idx +=   sizeof( bData);
      switch( bData)
      {
         case  1:
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_L_POINTERS);
               break;

         case  2:
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_V_POINTERS);
               break;

         case  3:
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_Z_POINTERS);
               break;

         case  4:
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_DEL_CRNT_Z_RECORD);
               break;

         case  5:
               SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_VITAL_PARAMS);
               break;

         case  0xFF:
               Task_Server_EraseAllData();
               break;
      }
      break;
*/
      case  SERVER_CMD_GET_STATUS      :  // 'L'
      case  SERVER_CMD_READ_TRANS      :  // 'l'
            memcpy( &task_MasterRec.Cmd_L, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_L));
            EquipmentID =  task_MasterRec.Cmd_L.EquipmentID;
            break;

      case  SERVER_CMD_END_Z_SESSION   :  // 'Y'
            memcpy( &task_MasterRec.Cmd_Y, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_Y));
            EquipmentID =  task_MasterRec.Cmd_Y.EquipmentID;
            break;

      case  SERVER_CMD_ID              :  // 'Z'
            memcpy( &task_MasterRec.Cmd_Z, task_pServerPacket_Rx->pBuffer, sizeof( task_MasterRec.Cmd_Z));
            EquipmentID =  task_MasterRec.Cmd_Z.EquipmentID;
            break;
   }

   if( (EquipmentID == 0) || (EquipmentID == 0xFF)) 
   {
      task_fRxBroadcastMsg =  TRUE;
      fResult              =  TRUE;
   }
   else
   {
      task_fRxBroadcastMsg =  FALSE;
      fResult              =  (EquipmentID == task_EquipmentID);
   }

   if( (task_fMode_IamMaster == TRUE) && (fResult == TRUE))
      task_fRxBroadcastMsg =  FALSE;   // makes a 'Master' machine to reply on broadcast messages

   if( task_fReplyToAnyMsg == TRUE)
   {
      task_fRxBroadcastMsg =  FALSE;
      fResult              =  TRUE;
      if( EquipmentID > 0)
         task_EquipmentID  =  EquipmentID;
   }

   return( fResult);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Server_HandleComm_Tx( void)
/*----------------------------------------------------------------------------*/
{
ulong    lData;
usint    uData;
byte     bData;
byte     Idx;


   p_NVRAM_SERVER_TX_PACKET[0]   =  0; // task_TxBuffer[0]              =  0;
   task_TxBufferIdx              =  0;
   task_ServerPacket_Tx.BufSize  =  0;

   switch( task_TxCommand)
   {
      case  SERVER_REPLY_ACK           :  // '.'
            bData    =  task_EquipmentID;                Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));
            break;

      case  SERVER_REPLY_NACK          :  // '*'
            break;

      case  SERVER_REPLY_EMERGENCY     :  // 'E'
            break;

      case  SERVER_REPLY_TANSACTION    :  // 'L'
      case  SERVER_REPLY_EVENT         :  // 'V'                      Tx
            {
            byte     *pBuf;
            byte     Idx;
            byte     Size;

               task_TxCommand =  SERVER_REPLY_ACK;

               task_pEmergency_Info =  Task_Log_Emergency_GetInfo();
               task_pTrans_Info     =  Task_Log_Trans_GetInfo();

               // Try first to check if there is a request to send an Emergencies packet ('E') ****** E E E ******* EMERGENCY
               if( (task_fRequestForEmergency == TRUE) && (task_Timer_Emergency == 0))
                  task_fRequestForEmergency  =  FALSE;

               if( task_fRequestForEmergency == TRUE)
               {

                  task_fRequestForEmergency  =  FALSE;

                  if( task_Cmd_E_Type == EMERGENCY_TYPE_VOID)
                     break;

                  task_SlaveRec.Cmd_E.Idx_W  =  task_pEmergency_Info->Idx_W;
                  task_SlaveRec.Cmd_E.Idx_R  =  task_pEmergency_Info->Idx_R;
                  task_SlaveRec.Cmd_E.Type   =  task_Cmd_E_Type;

                  Task_Log_Emergency_Step_Fwd_Server();

                  bData =  SERVER_DELIMITER;          Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));      // '_'
                  uData =  task_SlaveRec.Cmd_E.Idx_W; Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));      // E pointer (write)
                  bData =  SERVER_DELIMITER;          Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));      // '_'
                  uData =  task_SlaveRec.Cmd_E.Idx_R; Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));      // E pointer (read)

                  memcpy( &task_SlaveRec.Cmd_E.Data, Task_Application_GetEmergency(), sizeof( task_SlaveRec.Cmd_E.Data));
                  pBuf  =  (byte *)&task_SlaveRec.Cmd_E.Data;                  
                  Size  =  0;
                  switch( task_SlaveRec.Cmd_E.Type)
                  {
                     case  EMERGENCY_TYPE_CREDIT_CARD             :   Size  =  sizeof( Cmd_E_CreditCard) ; break;
                     case  EMERGENCY_TYPE_DIGITEL_AUTHENTICATION  :   Size  =  sizeof( Cmd_E_Digitel)    ; break;
                  }

                  bData =  task_SlaveRec.Cmd_E.Type;  Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));
                  for( Idx = 0; Idx < Size; Idx ++)
                  {
                    bData =  pBuf[Idx];  Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));   // For DIGITEL (Cmd_E_Digitel): Void1[32] Void2[8] EquipmentID[8] Void3[8] Void4[32] DateAndTime[32] Track2[8x32] 
                  }                                                                                     //

                  task_TxCommand =  SERVER_REPLY_EMERGENCY;
                  task_Cmd_E_Type   =  EMERGENCY_TYPE_VOID;

                  Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);
                  uData =  task_Param_Var;   Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));

                  // Case of Digitel - Send products
                  switch( task_SlaveRec.Cmd_E.Type)
                  {
                     case  EMERGENCY_TYPE_DIGITEL_AUTHENTICATION  :
                           memcpy( &task_SlaveRec.Cmd_E.Products, Task_Application_GetEmergency_Products(), sizeof( task_SlaveRec.Cmd_E.Products));
                           pBuf  =  (byte *)&task_SlaveRec.Cmd_E.Products;
                           Size  =  sizeof( Cmd_E_Products);                          
                           for( Idx = 0; Idx < Size; Idx ++)
                           {
                              bData =  pBuf[Idx];  Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));
                           }
                           break;
                  }
                  break;
               }

               // If there are no Emergencies - Try to send Transactions ('L') ****** L L L ******* TRANSACTIONS
               if( task_MasterRec.Cmd_L.Idx_R_Trans >= MAX_LOG_TRANS_RECORDS)
                  task_MasterRec.Cmd_L.Idx_R_Trans =  0;
               
               if( (task_MasterRec.Cmd_L.Idx_R_Trans != task_pTrans_Info->Idx_W) && (task_pTrans_Info->RecordsInDB > 0))
               {
               bool     fReadToTheEnd;
                  //printf(" L  Idx_R %u  Idx_W %u  RecordsInDB %u\n", task_MasterRec.Cmd_L.Idx_R_Trans, task_pTrans_Info->Idx_W, task_pTrans_Info->RecordsInDB );
                  fReadToTheEnd  =  FALSE;

                  task_pTrans_Info->Idx_R_Server  =  task_MasterRec.Cmd_L.Idx_R_Trans;

                  Task_Log_Trans_ReadRecord_Server( &task_SlaveRec.Cmd_L.Data);

                  task_SlaveRec.Cmd_L.Idx_W  =  task_pTrans_Info->Idx_W;         // CT-20
                  task_SlaveRec.Cmd_L.Idx_R  =  task_pTrans_Info->Idx_R_Server;  // Server

                  bData =  SERVER_DELIMITER;                                                             Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
                  uData =  (fReadToTheEnd == FALSE) ? task_SlaveRec.Cmd_L.Idx_W : MAX_LOG_TRANS_RECORDS; Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));   // L pointer (write)
                  bData =  SERVER_DELIMITER;                                                             Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
                  uData =  task_SlaveRec.Cmd_L.Idx_R;                                                    Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));   // L pointer (read)

                  pBuf  =  (byte *)&task_SlaveRec.Cmd_L.Data;
                  for( Idx = 0; Idx < sizeof( task_SlaveRec.Cmd_L.Data); Idx ++)
                  {
                     bData =  pBuf[Idx];  Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));
                  }

                  task_TxCommand =  SERVER_REPLY_TANSACTION;

                  Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);
                  uData =  task_Param_Var;   Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));
                  break;
               }
                             
               // If there are no Transactions - Try to send Events ('V'), if there is one. ****** V V V ******* EVENTS
               task_pEvent_Info  =  Task_Log_Event_GetInfo();           // See Task_Application_SetEvent();

               if( task_pEvent_Info->Idx_R_Server != task_pEvent_Info->Idx_W)                                           // Any more Events ?
               {

                  memset( &task_SlaveRec.Cmd_V, 0, sizeof( task_SlaveRec.Cmd_V));
                  if( Task_Log_Event_ReadRecord_Server( &task_SlaveRec.Cmd_V.Data) == TRUE)
                  {
                     
                     task_SlaveRec.Cmd_V.Idx_W  =  task_pEvent_Info->Idx_W;         // CT-20
                     task_SlaveRec.Cmd_V.Idx_R  =  task_pEvent_Info->Idx_R_Server;  // Server

                     bData =  SERVER_DELIMITER;          Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
                     uData =  task_SlaveRec.Cmd_V.Idx_W; Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));   // V pointer (W CT-20)
                     bData =  SERVER_DELIMITER;          Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
                     uData =  task_SlaveRec.Cmd_V.Idx_R; Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));   // V pointer (R Server)

                     pBuf  =  (byte *)&task_SlaveRec.Cmd_V.Data;                                                        // See above function Task_Log_Event_ReadRecord_Server().
                     for( Idx = 0; Idx < sizeof( task_SlaveRec.Cmd_V.Data); Idx ++)                                     // task_SlaveRec.Cmd_V.Data:
                     {                                                                                                  // SubCmd (byte), EvCode (byte), EquipmentID (byte), DateAndTime (long), ...
                        bData =  pBuf[Idx];  Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));               // ... Data (8 bytes union), Void2 (byte)
                     }                                                                                                  //

                     task_TxCommand =  SERVER_REPLY_EVENT;                                                              // 'V' Command
                     //< Send that once ! No matter if ACK was sent or not. ONESHOT
                     //AC  1/12/2016 task_pEvent_Info->Idx_R_Server = task_pEvent_Info->Idx_W;
                     //> Send that once ! No matter if ACK was sent or not. ONESHOT - END !
                     break;
                  }
                  break;
               }

               if( task_Timer_SendStatus == 0)
                  task_fSendStatus  =  TRUE;

               // If requested locally - send Status ('Q')              ****** Q Q Q ******* STATUS
               if( task_fSendStatus == TRUE)    //from version 3.02.00
               {
               byte  BytesCount;

                  task_fSendStatus        =  FALSE;
                  task_Timer_SendStatus   =  Task_Timer_SetTimeSec( task_Time_SendStatus);

                  task_SlaveRec.Cmd_L.Idx_W  =  task_pTrans_Info->Idx_W;            // CT-20
                  task_SlaveRec.Cmd_L.Idx_R  =  task_MasterRec.Cmd_L.Idx_R_Trans;   // Server

                  bData =  SERVER_DELIMITER;          Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
                  uData =  task_SlaveRec.Cmd_L.Idx_W; Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));   // L pointer (W CT-20)
                  bData =  SERVER_DELIMITER;          Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
                  uData =  task_SlaveRec.Cmd_L.Idx_R; Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));   // L pointer (R Server)

                  // Current Z #
                  Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);
                  uData =  task_Param_Var;   Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));

                  // Equipment ID
                  bData =  task_EquipmentID;          Task_Server_AddObject( TYPE_BYTE, &bData , sizeof( bData));

                  // Equipment Type
                  bData =  0;                Task_Server_AddObject( TYPE_BYTE, &bData , sizeof( bData));

                  // Status variables
                  {
                     // Read 'Status' structure
                     Task_Server_ReadStatusVars();                      // Currently, do nothing (30/11/2016 AC)

                     // Machine's errors
                     {
                     tApp_Status    *p_AppStatus;

                        p_AppStatus =  Task_Application_GetStatus();

                        /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Inactivity     =  0;
                        // Inactivity
                        if( BIT_IS_SET( p_AppStatus->Error_Flags,    APP_STATUS_ERR_FLAG_DISABLED))            SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Inactivity, (BIT_0 | BIT_1));
                                                                                                               SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Inactivity, BIT_5);   // BIT 5 always ON
                        //          Bit 6 is set, if TibaPay server is online (30/11/2016 AC)
                        if (Task_TibaPay_IsCommLoss() == FALSE)                                                SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Inactivity, BIT_6);

                                                                                                               
                        // Errors (I)
                        /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_1 =  0;
                        if( BIT_IS_SET( p_AppStatus->Error_Flags,    APP_STATUS_ERR_FLAG_DOOR_CTRL_OFFLINE))   SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_1, BIT_1);
                        if( BIT_IS_SET( p_AppStatus->Error_Flags,    APP_STATUS_ERR_FLAG_PRN_OUT_OF_PAPER))    SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_1, BIT_2);

                        // Errors (II)
                        /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_2 =  0;
                        if( BIT_IS_SET( p_AppStatus->Error_Flags,    APP_STATUS_ERR_FLAG_PRN_OFFLINE))         SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_2, BIT_0);
                        if( BIT_IS_SET( p_AppStatus->Warning_Flags,  APP_STATUS_WRN_FLAG_HOPPER_A_LOW_LVL))    SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_2, BIT_1);
                        if( BIT_IS_SET( p_AppStatus->Warning_Flags,  APP_STATUS_WRN_FLAG_HOPPER_B_LOW_LVL))    SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_2, BIT_2);
                        if( BIT_IS_SET( p_AppStatus->Warning_Flags,  APP_STATUS_WRN_FLAG_HOPPER_C_LOW_LVL))    SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_2, BIT_3);
                        if( BIT_IS_SET( p_AppStatus->Warning_Flags,  APP_STATUS_WRN_FLAG_DOOR_OPENED))         SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->SystemErrors_2, BIT_7);

                        // Security
                        /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Security       =  0;
                        if( BIT_IS_SET( p_AppStatus->Warning_Flags,  APP_STATUS_WRN_FLAG_HOPPER_D_LOW_LVL))    SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Security      , BIT_5);
                        //if( Task_Coin_IsActive() == TRUE)                           SET_BIT( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Inactivity, ACTIVE_COIN_CHANGER   );
                        //if( Task_Bill_IsActive() == TRUE)                           SET_BIT( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Inactivity, ACTIVE_BILL_VALIDATOR );
                        //if( task_StatusStruct.Status_Printer != PRN_STATUS_OFF)     SET_BIT( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Inactivity, ACTIVE_PRINTER        );

                        // Printer
                        /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Status_Printer =  0;
                        if( BIT_IS_SET( p_AppStatus->Error_Flags,    APP_STATUS_ERR_FLAG_PRN_OUT_OF_PAPER))    SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Status_Printer, BIT_0);
                        if( BIT_IS_SET( p_AppStatus->Warning_Flags,  APP_STATUS_WRN_FLAG_PRN_NEARLY_EOP))      SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Status_Printer, BIT_1);
                        if( BIT_IS_SET( p_AppStatus->Error_Flags,    APP_STATUS_ERR_FLAG_PRN_OFFLINE))         SET_BIT  ( /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Status_Printer, BIT_2);
                     }

                     // B2B
                     {
                     tB2B_FromSlave       *pRxData;

                        pRxData   =  Task_B2B_GetRxDataPtr();
                        /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Inputs_B2B_DIP_Switches =  pRxData->GetStatus.IO_DIP_Switches;
                     }

                     // General
                     {
                        /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->Void_06_BV  =  0x01;
                     }

                     // Comm. Quality (%)
                     {
                        /*task_StatusStruct.*/p_NVRAM_SERVER_Q_CMD->CommQuality_Percent  =  (long)(-1);
                     }

                     // Insert 'Status' record into Tx buffer
                     BytesCount  =  sizeof( Cmd_Q_Data);
                     pBuf        =  (byte *)/*&task_StatusStruct*/p_NVRAM_SERVER_Q_CMD;
                     for( Idx = 0; Idx < BytesCount; Idx ++)
                     {
                        bData =  pBuf[Idx];  Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));
                     }
                  }

                  task_TxCommand =  SERVER_REPLY_STATUS;
                  break;
               }

               // If there is nothing to send - send ACK (done automatically by this module)
            }
            break;

      case  SERVER_REPLY_ENFORCE_Z_CMD :  // 'P'
            break;

      case  SERVER_REPLY_PARAMS_READ   :  // 'R'
            bData    =  '.';                 Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));

            //xEsqrAPI_Read_Address( task_Buf_Params, sizeof( task_Buf_Params), task_Address, TRUE);
            xSramAPI_Read_Address( task_Buf_Params, sizeof( task_Buf_Params), task_Address, TRUE);

            for( Idx = 0; Idx < 8; Idx ++)   // Send 8 Longs
            {
               lData =  0;
               lData =  *(ulong *)(task_Buf_Params + (Idx * 4));
               Util_SwapArray( &lData, sizeof( lData));
               Task_Server_AddObject( TYPE_ULONG, &lData , sizeof( lData));
            }
            break;

      case  SERVER_REPLY_PARAMS_WRITE  :  // 'W'
            bData    =  '.';                             Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
            break;

      case  SERVER_REPLY_ID            :  // 'Z'
            {
            usint    Idx_W;
      
               task_pTrans_Info  =  Task_Log_Trans_GetInfo();
               Idx_W =  (task_pTrans_Info->fMadeCyclicRound == FALSE) ? task_pTrans_Info->Idx_W : MAX_LOG_TRANS_RECORDS;

               bData    =  SERVER_DELIMITER;                Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
               uData    =  Idx_W;                           Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));
               bData    =  SERVER_DELIMITER;                Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
               bData    =  '2';                             Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));   // constant
               bData    =  SERVER_DELIMITER;                Task_Server_AddObject( TYPE_CHAR , &bData , sizeof( bData));
               uData    =  Idx_W;                           Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));
               Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);
               uData    =  task_Param_Var;                  Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));
               bData    =  App_Version_Local;               Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));
               lData    =  ClockAPI_DateAndTimeToSeconds();
               Util_SwapArray( &lData, sizeof( lData));     Task_Server_AddObject( TYPE_ULONG, &lData , sizeof( lData));
               lData    =  (MAX_LOG_TRANS_RECORDS - 1);
               Util_SwapArray( &lData, sizeof( lData));     Task_Server_AddObject( TYPE_ULONG, &lData , sizeof( lData));
               uData    =  0x173;                           Task_Server_AddObject( TYPE_USINT, &uData , sizeof( uData));   // constant (173)
               bData    =  task_EquipmentID;                Task_Server_AddObject( TYPE_BYTE , &bData , sizeof( bData));
            }
            break;

      default:
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Server_AddObject( tType  Type, void  *pData, byte   Len)
/*----------------------------------------------------------------------------*/
{
char     Str[12];
byte     StrLen;


   Str[0]   =  0;

   switch( Type)
   {
      case  TYPE_CHAR   :  sprintf( Str, "%c", *(char *)pData);      break;
      case  TYPE_BYTE   :  sprintf( Str, "%.2X", *(byte *)pData);    break;
      case  TYPE_USINT  :  sprintf( Str, "%.4X", *(usint *)pData);   break;
      case  TYPE_ULONG  :  sprintf( Str, "%.8lX", *(ulong *)pData);  break;
      case  TYPE_STRING :                                            break;
      default           :                                            return;
   }

   if( Type == TYPE_STRING)   StrLen   =  strlen( (char *)pData);
   else                       StrLen   =  strlen( Str);

   if( (task_TxBufferIdx + StrLen + 1) >= SERVER_MAX_LEN_BUFFER /*sizeof( task_TxBuffer)*/)
      return;

   if( Type == TYPE_STRING)   strcat( p_NVRAM_SERVER_TX_PACKET/*task_TxBuffer*/, (char *)pData);
   else                       strcat( p_NVRAM_SERVER_TX_PACKET /*task_TxBuffer*/, Str);

   /*task_TxBuffer*/p_NVRAM_SERVER_TX_PACKET[task_TxBufferIdx   +  StrLen]  =  0;
   task_TxBufferIdx                 += StrLen;
   task_ServerPacket_Tx.BufSize     += StrLen;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Server_EraseAllData( void)
/*----------------------------------------------------------------------------*/
{
   SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_L_POINTERS);
   SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_V_POINTERS);
   SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_Z_POINTERS);
   //SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_DEL_CRNT_Z_RECORD);
   //SET_BIT( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_RESET_VITAL_PARAMS);

   memset( /*&task_StatusStruct*/p_NVRAM_SERVER_Q_CMD, 0, sizeof( Cmd_Q_Data));
   Task_Server_SaveStatusVars();
}




/*----------------------------------------------------------------------------*/
void						Task_Server_DbgIncrTransac( void)
/*----------------------------------------------------------------------------*/
{
   task_pTrans_Info->Idx_W++;
   task_pTrans_Info->RecordsInDB++;
}


/*----------------------------------------------------------------------------*/
void						Task_Server_DbgIncrIdx_R_Trans( void)
/*----------------------------------------------------------------------------*/
{
   task_MasterRec.Cmd_L.Idx_R_Trans++;  // DEBUG
}


/*----------------------------------------------------------------------------*/
void						Task_Server_DbgClearTransac( void)
/*----------------------------------------------------------------------------*/
{
   task_pTrans_Info->Idx_W = 0;
   task_pTrans_Info->RecordsInDB = 0;
}


/*----------------------------------------------------------------------------*/
void						Task_Server_View_Idx_R_Server( void)
/*----------------------------------------------------------------------------*/
{
   //printf("Idx_R_Server %i\n", task_pTrans_Info->Idx_R_Server);
}     
     
     
     
     