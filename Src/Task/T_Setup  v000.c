/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  ERASE_SETUP_INFO_PASSWORD        9101
#define  ERASE_SALES_INFO_PASSWORD        9202
#define  SECURITY_PASSWORD                9303
#define  ERASE_LOG_RECORDS_PASSWORD       9404

#define  SECURITY_LEVEL_1                 0
#define  SECURITY_LEVEL_2                 1
#define  SECURITY_LEVEL_3                 2
#define  MAX_SECURITY_LEVELS              3

#define  VOID                             0
#define  CLEAR_LINE                       1

#define  MAX_LEVELS                       9
#define  MAX_SELECTIONS                   9

#define  ITEM_TYPE_VOID                   0
#define  ITEM_TYPE_MENU                   1
#define  ITEM_TYPE_DATA_ENTRY             2
#define  ITEM_TYPE_FUNCTION               3
#define  ITEM_TYPE_PASSWORD               4

#define  FIELD_TYPE_ALPHA_NUM             0
#define  FIELD_TYPE_MONEY                 1
#define  FIELD_TYPE_NUMERIC               2
#define  FIELD_TYPE_IP_ADDR               3
#define  FIELD_TYPE_TIME                  4
#define  FIELD_TYPE_DATE                  5
#define  FIELD_TYPE_SYSTEM_TIME           6
#define  FIELD_TYPE_SYSTEM_DATE           7
#define  FIELD_TYPE_PHONE_NUMBER          8
#define  FIELD_TYPE_PULL_DOWN             9

#define  KEY_TYPE_VOID                    0
#define  KEY_TYPE_ENTER                   1
#define  KEY_TYPE_ESC                     2
#define  KEY_TYPE_FUNCTION                3
#define  KEY_TYPE_DIGIT                   4

#define  REPORT_PRN                       0
#define  REPORT_LCD                       1

#define  DATA_ENTRY_TYPE_HOPPER           1

#define  FLAG_MENU_ACCESS_IF_SALES_ZEROED BIT_0

#define  FLAG_DATA_ENTRY_CANT_BE_ZERO     BIT_0
#define  FLAG_DATA_ENTRY_MULTIPLE_BY_100  BIT_1
#define  FLAG_DATA_ENTRY_FORCE_LTR        BIT_2 // Force Left-to-Right
#define  FLAG_DATA_ENTRY_BITS_PARAM       BIT_3 // Parameter of bits (see 'DataIdx' member)
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   STATE_PASSWORD_IDLE        ,
   STATE_PASSWORD_DATA_ENTRY  ,
   STATE_PASSWORD_VERIFY_DATA ,
   STATE_PASSWORD_INVALID     ,
   STATE_PASSWORD_APPROVED    ,
   /******************/
   STATE_PASSWORD_LAST
}  tStatePassword;



typedef  enum
{
   EV_VOID                 =  0,
   EV_UPDATE_DATA          =  0x0020,
   EV_DISABLE_THIS_TASK    =  0x0040,
   EV_ENABLE_THIS_TASK     =  0x0080,
   EV_DOOR_OPENED          =  0x0100,
   EV_DOOR_CLOSED          =  0x0200,
}  tEvent;



typedef  enum
{
   MENU_0            ,
   MENU_1            ,
   MENU_2            ,
   MENU_3            ,
   MENU_11           ,
   MENU_111          ,
   MENU_12           ,
   MENU_13           ,
   MENU_14           ,
   MENU_141          ,
   MENU_144          ,
   MENU_15           ,                          // 21/9/2016
   MENU_21           ,
   MENU_31           ,
   MENU_311          ,
   MENU_32           ,
   MENU_37           ,
   MENU_371          ,
   MENU_38           ,
   /**********************/
   MENU_LAST
}  tMenuCode;



typedef  enum
{
   DATA_ENTRY_1111   ,
   DATA_ENTRY_1112   ,
   DATA_ENTRY_121    ,
   DATA_ENTRY_122    ,
   DATA_ENTRY_123    ,
   DATA_ENTRY_124    ,
   DATA_ENTRY_131    ,
   DATA_ENTRY_132    ,
   DATA_ENTRY_133    ,
   DATA_ENTRY_134    ,
   DATA_ENTRY_135    ,
   DATA_ENTRY_136    ,
   DATA_ENTRY_137    ,
   DATA_ENTRY_1411   ,
   DATA_ENTRY_1412   ,
   DATA_ENTRY_142    ,
   DATA_ENTRY_143    ,
   DATA_ENTRY_1441   ,
   DATA_ENTRY_1442   ,
   DATA_ENTRY_1443   ,
   //DATA_ENTRY_15     ,
   DATA_ENTRY_152    ,                  // 22/9/2016    Disable Credit
   DATA_ENTRY_3111   ,
   DATA_ENTRY_321    ,
   DATA_ENTRY_322    ,
   DATA_ENTRY_33     ,
   DATA_ENTRY_34     ,
   DATA_ENTRY_35     ,
   DATA_ENTRY_3711   ,
   /**********************/
   DATA_ENTRY_LAST
}  tDataEntryCode;



typedef  enum
{
   FUNCTION_VOID     ,
   FUNCTION_211      ,
   FUNCTION_212      ,
   FUNCTION_213      ,
   FUNCTION_22       ,
   FUNCTION_23       ,
   FUNCTION_36       ,
   FUNCTION_39       ,
   FUNCTION_3712     ,
   FUNCTION_381      ,
   FUNCTION_382      ,
   /**********************/
   FUNCTION_LAST
}  tFunctionCode;



typedef  enum
{
   MENU_MSG_VOID     ,
   MENU_MSG_0        ,
   MENU_MSG_1        ,
   MENU_MSG_2        ,
   MENU_MSG_3        ,
   MENU_MSG_11       ,
   MENU_MSG_12       ,
   MENU_MSG_13       ,
   MENU_MSG_14       ,
   MENU_MSG_15       ,                  // 21/9/2016
   MENU_MSG_21       ,
   MENU_MSG_22       ,
   MENU_MSG_23       ,
   MENU_MSG_31       ,
   MENU_MSG_32       ,
   MENU_MSG_33       ,
   MENU_MSG_34       ,
   MENU_MSG_35       ,
   MENU_MSG_36       ,
   MENU_MSG_37       ,
   MENU_MSG_38       ,
   MENU_MSG_39       ,
   MENU_MSG_111      ,
   MENU_MSG_1111     ,
   MENU_MSG_1112     ,
   MENU_MSG_121      ,
   MENU_MSG_122      ,
   MENU_MSG_123      ,
   MENU_MSG_124      ,
   MENU_MSG_131      ,
   MENU_MSG_132      ,
   MENU_MSG_133      ,
   MENU_MSG_134      ,
   MENU_MSG_135      ,
   MENU_MSG_136      ,
   MENU_MSG_137      ,
   MENU_MSG_141      ,
   MENU_MSG_142      ,
   MENU_MSG_143      ,
   MENU_MSG_144      ,
   MENU_MSG_1411     ,
   MENU_MSG_1412     ,
   MENU_MSG_1441     ,
   MENU_MSG_1442     ,
   MENU_MSG_1443     ,
   MENU_MSG_152      ,                  // 21/9/2016
   MENU_MSG_211      ,
   MENU_MSG_212      ,
   MENU_MSG_213      ,
   MENU_MSG_311      ,
   MENU_MSG_3111     ,
   MENU_MSG_321      ,
   MENU_MSG_322      ,
   MENU_MSG_371      ,
   MENU_MSG_3711     ,
   MENU_MSG_3712     ,
   MENU_MSG_381      ,
   MENU_MSG_382      ,
   /**********************/
   MENU_MSG_LAST
}  tMenuMsgCode;



typedef  enum
{
   LABEL_MSG_VOID,
   /**********************/
   LABEL_MSG_LAST
}  tLabelMsgCode;



typedef  enum
{
   FUNCTION_LABEL_MSG_VOID             ,
   FUNCTION_LABEL_MSG_UNAVILABLE_L1    ,  // don't delete this message
   FUNCTION_LABEL_MSG_UNAVILABLE_L2    ,  // don't delete this message
   FUNCTION_LABEL_MSG_PRINTING_REPORT  ,
   FUNCTION_LABEL_MSG_PLEASE_WAIT      ,
   FUNCTION_LABEL_MSG_REQ_TO_PRINT_COPY,
   FUNCTION_LABEL_MSG_DELETE_DATABASE  ,
   FUNCTION_LABEL_MSG_YES_NO           ,
   FUNCTION_LABEL_MSG_3713L1           ,
   FUNCTION_LABEL_MSG_382              ,
   /**********************/
   FUNCTION_LABEL_MSG_LAST
}  tFunctionLabelMsgCode;



typedef  enum
{
   REPORT_LCD_LABEL_BILL_20                           ,
   REPORT_LCD_LABEL_BILL_50                           ,
   REPORT_LCD_LABEL_BILL_100                          ,
   REPORT_LCD_LABEL_BILL_ZEROED                       ,
   REPORT_LCD_LABEL_BILL_ACCUMULATED                  ,
   REPORT_LCD_LABEL_COIN_1                            ,
   REPORT_LCD_LABEL_COIN_2                            ,
   REPORT_LCD_LABEL_COIN_5                            ,
   REPORT_LCD_LABEL_COIN_10                           ,
   REPORT_LCD_LABEL_COIN_ZEROED                       ,
   REPORT_LCD_LABEL_COIN_ACCUMULATED                  ,
   REPORT_LCD_LABEL_TOTAL_INCOME_ZEROED               ,
   REPORT_LCD_LABEL_TOTAL_INCOME_ACCUMULATED          ,
   REPORT_LCD_LABEL_HOPPER1                           ,
   REPORT_LCD_LABEL_HOPPER2                           ,
   REPORT_LCD_LABEL_HOPPER3                           ,
   REPORT_LCD_LABEL_HOPPER4                           ,
   REPORT_LCD_LABEL_HOPPER_TOTAL_OUTCOME_ZEROED       ,
   REPORT_LCD_LABEL_HOPPER_TOTAL_OUTCOME_ACCUMULATED  ,
   REPORT_LCD_LABEL_BALANCE_ZEROED                    ,
   REPORT_LCD_LABEL_BALANCE_ACCUMULATED               ,
   /**********************/
   REPORT_LCD_LABEL_LAST
}  tReportLcdLabelMsgCode;



typedef  enum
{
   PULL_DOWN_0,
   PULL_DOWN_1,
   PULL_DOWN_2,
   PULL_DOWN_3,
   PULL_DOWN_4,
   /**********************/
   PULL_DOWN_LAST
}  tPullDownCode;



typedef  enum
{
   MSG_VOID                   ,
   MSG_ENTER_PASSWORD         ,
   MSG_ENTER_PASSWORD_2       ,
   MSG_INVALID_PASSWORD       ,
   MSG_ACCESS_DENIED          ,
   MSG_PRINT_SALES_REPORT     ,
   MSG_THEN_ERASE_DATA        ,
}  tMessages;



typedef  enum
{
   STRING_SHORT_u             ,
   STRING_SHORT_lu            ,
   STRING_SHORT_s             ,
   /******************/
   STRING_SHORT_LAST
}  tStringShort;



typedef  enum
{
   STRING_LONG_2lu2lu2lu_TIME ,
   STRING_LONG_2lu2lu2lu_DATE ,
   STRING_LONG_lu2lu          ,
   STRING_LONG_lu2lu3u        ,
   /******************/
   STRING_LONG_LAST
}  tStringLong;



typedef  struct
{
   char           Text[MAX_LANGUAGES][LCD_LINE_WIDTH + 1];
   byte           Value;
}  tPullDown;



typedef  struct
{
   byte           HeadLine;
   byte           PrevItem;
   byte           Entries;
   byte           DataEntryType;

   struct
   {
      byte        Label;
      byte        NextItemCode;
      byte        NextItemType;
      byte        SecurityLevel;
      byte        Flags;
   }  Selection[MAX_SELECTIONS];
}  tMenuItem;



typedef  struct
{
   byte           HeadLine;
   byte           Label;
   byte           PrevMenu;
   byte           FieldType;
   byte           Length;
   byte           Entries;
   byte           EntriesOffset;
   byte           DataEntryType;
   byte           Flags;
   byte           PullDownIndex;
   usint          DataIdx;
   usint          ParamCode;  // see T_Params.h and T_Params.c files
}  tDataEntry;



typedef  struct
{
   byte           Label_L1;
   byte           Label_L2;
   byte           PrevMenu;
   void           (*Function)(byte);
   byte           Param1;
}  tFunction;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tApp_SalesInfo      *task_pSalesInfo;
static   tApp_CoinsInfo      *task_pCoinsInfo;
static   tApp_BillsInfo      *task_pBillsInfo;
static   tHoppers_Info       *task_pHoppersInfo;
static   char                 task_Text[LCD_LINE_WIDTH + 1];
static   char                 task_Text_Temp[LCD_LINE_WIDTH + 1];
static   char                 task_Code[4 + 1];
static   tEvent               task_Event;
static   tStatePassword       task_StatePassword;
static   tParams_A_Rec        task_Rec_Type_A;
static   tParams_B_Rec        task_Rec_Type_B;
static   tTimerSec            task_TimerSec;
static   tTimerSec            task_TimerSec_2;
static   tTimerMsec           task_TimerMsec;
static   ulong                task_Password;
static   byte                 task_Key;
static   byte                 task_KeyType;
static   byte                 task_ItemCode;
static   byte                 task_ItemType;
static   byte                 task_Idx;
static   byte                 task_Entry;
static   byte                 task_Offset;
static   byte                 task_Level;
static   byte                 task_Selection[MAX_LEVELS];
static   byte                 task_FunctionState;
static   byte                 task_EntryIdx;
static   byte                 task_CharTableIdx;
static   byte                 task_LastKey;
static   byte                 task_PullDownIdx;
static   byte                 task_SecurityLevel;
static   byte                 task_LanguageTemp;
static   bool                 task_fDoorSwitch;
static   bool                 task_fRefreshScreen;
static   bool                 task_fFirstKeyStroke;
static   bool                 task_fCodeIsEntered;
static   bool                 task_fEndOfSelections;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						Task_Setup_HandleTimer( void);
static	void						Task_Setup_HandleExtEvent( void);
static	void						Task_Setup_HandleState( void);
static	void						Task_Setup_HandleEvent( void);
static	void						Task_Setup_HandleKey( void);
static	void						Task_Setup_HandleKey_Menu( byte Key);
static	bool						Task_Setup_CheckMenuAccess( void);
static	void						Task_Setup_HandleKey_DataEntry( byte Key);
static	void						Task_Setup_DataEntry_AlphaNum( byte Key);
static	void						Task_Setup_HandleKey_Function( byte Key);
static	void						Task_Setup_HandleKey_Password( byte Key);
static	void						Task_Setup_GetDataFromField( void);
static	bool						Task_Setup_SetDataIntoField( void);
static	void						Task_Setup_ClearDataEntry( void);
static	void						Task_Setup_ShowData( void);
static	void						Task_Setup_Func_PrintReport( byte   Code);
static	void						Task_Setup_Func_ShowReport( byte   Void);
static	void						Task_Setup_Func_EraseSalesInfo( byte   Void);
static	void						Task_Setup_Func_SwVer( byte   Void);
static   void                 Task_Setup_Func_SetDeafults( byte   Void);
static	void						Task_Setup_Func_EmptyHopper( byte   Void);
static   void                 Task_Setup_Func_LogFiles( byte Void);
static   void                 Task_Setup_Func_TestRelay( byte Void);
static   void                 Task_Setup_Func_TestPrinter( byte Void);
static   void                 Task_Setup_Func_TestCoinRouting( byte Void);
/*--------------------------- Constant Definitions ---------------------------*/
CONST char        MenuMsg[][MAX_LANGUAGES][(LCD_LINE_WIDTH - 2) + 1]	=
                  {
                     {	"--------------",  "--------------"}, // MENU_MSG_VOID
                                           // MENU_0
                     {	"Main Menu"     ,  "����� ����"    }, // MENU_MSG_0
                     {	"Settings"      ,  "������"        }, // MENU_MSG_1
                     {	"Reports"       ,  "�����"         }, // MENU_MSG_2
                     {	"Maintenance"   ,  "������"        }, // MENU_MSG_3
                                           // MENU_1
                     {	"Hoppers"       ,  "����� ����"    }, // MENU_MSG_11
                     {	"Bills Block"   ,  "����� �����"   }, // MENU_MSG_12
                     {	"Coins Block"   ,  "����� ������"  }, // MENU_MSG_13
                     {	"General"       ,  "����"          }, // MENU_MSG_14
                     {  "Credit Block"  ,  "����� �����"   }, // MENU_MSG_15
                                           // MENU_2
                     {	"Print"         ,  "����"          }, // MENU_MSG_21
                     {	"Show"          ,  "���"           }, // MENU_MSG_22
                     {	"Data Reset"    ,  "����� ������"  }, // MENU_MSG_23
                                           // MENU_3
                     {	"Refill Coins"  ,  "����� ������"  }, // MENU_MSG_31
                     {	"Clock"         ,  "����"          }, // MENU_MSG_32
                     {	"Service Tel."  ,  "����� ����"    }, // MENU_MSG_33
                     {	"Mach. Address" ,  "����� �����"   }, // MENU_MSG_34
                     {	"Language"      ,  "���"           }, // MENU_MSG_35
                     {	"SW Version"    ,  "����� �����"   }, // MENU_MSG_36
                     {	"Empty Hoppers" ,  "����� ������"  }, // MENU_MSG_37
                     {	"Tests"         ,  "������"        }, // MENU_MSG_38
                     {  "Deafult Set"   ,  "������ ����"   }, // MENU_MSG_39
                                           // MENU_11
                     {	"Hopper"        ,  "����"          }, // MENU_MSG_111
                                           // MENU_111
                     {	"Coin Value"    ,  "��� ����"      }, // MENU_MSG_1111
                     {	"Max Capacity"  ,  "������ ���'"   }, // MENU_MSG_1112
                                           // MENU_12
                     {	"20 NIS Bill"   ,  "��� 02 �\"�"   }, // MENU_MSG_121
                     {	"50 NIS Bill"   ,  "��� 05 �\"�"   }, // MENU_MSG_122
                     {	"100 NIS Bill"  ,  "��� 001 �\"�"  }, // MENU_MSG_123
                     {	"200 NIS Bill"  ,  "��� 002 �\"�"  }, // MENU_MSG_124
                                           // MENU_13
                     {	"5 Ag. Coin"    ,  "���� 5 ��'"    }, // MENU_MSG_131
                     {	"10 Ag. Coin"   ,  "���� 01 ��'"   }, // MENU_MSG_132
                     {	"50 Ag. Coin"   ,  "���� 05 ��'"   }, // MENU_MSG_133
                     {	"1 NIS Coin"    ,  "���� 1 �\"�"   }, // MENU_MSG_134
                     {	"2 NIS Coin"    ,  "���� 2 �\"�"   }, // MENU_MSG_135
                     {	"5 NIS Coin"    ,  "���� 5 �\"�"   }, // MENU_MSG_136
                     {	"10 NIS Coin"   ,  "���� 01 �\"�"  }, // MENU_MSG_137
                                           // MENU_14
                     {	"Times"         ,  "�����"         }, // MENU_MSG_141
                     {	"Change Limit"  ,  "���� ����"     }, // MENU_MSG_142
                     {	"Min Change"    ,  "���� ����"     }, // MENU_MSG_143
                     {	"Security"      ,  "�����"         }, // MENU_MSG_144
                                           // MENU_141
                     {	"Hoppers"       ,  "����� ����"    }, // MENU_MSG_1411
                     {	"Dispensing"    ,  "����� ����"    }, // MENU_MSG_1412
                                           // MENU_144
                     {	"Password Lvl1" ,  "����� ��� 1"   }, // MENU_MSG_1441
                     {	"Password Lvl2" ,  "����� ��� 2"   }, // MENU_MSG_1442
                     {	"Password Lvl3" ,  "����� ��� 3"   }, // MENU_MSG_1443
                                           // MENU_15
                     {	"Disable Credit",  "����� �����"   }, // MENU_MSG_152                   /* 22/9/2016 */
                                           // MENU_21
                     {	"Door-Open Rep" ,  "��� ����� ���" }, // MENU_MSG_211
                     {	"Door-Close R." ,  "��� ����� ���" }, // MENU_MSG_212
                     {	"Fiscal Report" ,  "��� ����"      }, // MENU_MSG_213
                                           // MENU_31
                     {	"Hopper"        ,  "����"          }, // MENU_MSG_311
                                           // MENU_311
                     {	"Count of Coins",  "���� ������"   }, // MENU_MSG_3111
                                           // MENU_32
                     {	"Time Setting"  ,  "����� ���"     }, // MENU_MSG_321
                     {	"Date Setting"  ,  "����� �����"   }, // MENU_MSG_322
                                           // MENU_37
                     {	"Hopper"        ,  "����"          }, // MENU_MSG_371
                                           // MENU_371
                     {	"Qtty. to Empty",  "���� ������"   }, // MENU_MSG_3711
                     {	"Empty Hopper"  ,  "��� ����� ����"}, // MENU_MSG_3712
                                           // MENU_38
                     {	"Test Printer"  ,  "����� �����"   }, // MENU_MSG_381
                     {	"Coin Routing"  ,  "����� ������"  }, // MENU_MSG_382

                     {	"--------------",  "--------------"}  // MENU_MSG_LAST
                  };


CONST tMenuItem   Menu[]   =
                  {
                     // MENU_0
                     {MENU_MSG_0, MENU_0  ,  0, 0,
                        {  {MENU_MSG_1    ,  MENU_1         ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_2    ,  MENU_2         ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_3    ,  MENU_3         ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_1
                     {MENU_MSG_1, MENU_0  ,  0, 0,
                        {  {MENU_MSG_11   ,  MENU_11        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_12   ,  MENU_12        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_13   ,  MENU_13        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_14   ,  MENU_14        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           // new 21/9/2016
                           {MENU_MSG_15   ,  MENU_15        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},                             // complete to totaly 9 menus

                     // MENU_2
                     {MENU_MSG_2, MENU_0  ,  0, 0,
                        {  {MENU_MSG_21   ,  MENU_21        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_22   ,  FUNCTION_22    ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_23   ,  FUNCTION_23    ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_3
                     {MENU_MSG_3, MENU_0  ,  0, 0,
                        {  {MENU_MSG_31   ,  MENU_31        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_32   ,  MENU_32        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_33   ,  DATA_ENTRY_33  ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_34   ,  DATA_ENTRY_34  ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_2, 0     },
                           {MENU_MSG_35   ,  DATA_ENTRY_35  ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_2, 0     },
                           {MENU_MSG_36   ,  FUNCTION_36    ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_37   ,  MENU_37        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_38   ,  MENU_38        ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_2, 0     },
                           {MENU_MSG_39   ,  FUNCTION_39    ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_2, 0     },
                           }},
                     // MENU_11
                     {MENU_MSG_11, MENU_1 ,  MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/, DATA_ENTRY_TYPE_HOPPER,
                        {  {MENU_MSG_111  ,  MENU_111       ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_111
                     {MENU_MSG_111, MENU_11,  0, DATA_ENTRY_TYPE_HOPPER,
                        {  {MENU_MSG_1111 ,  DATA_ENTRY_1111,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_1112 ,  DATA_ENTRY_1112,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_12
                     {MENU_MSG_12, MENU_1 ,  0, 0,
                        {  {MENU_MSG_121  ,  DATA_ENTRY_121 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_122  ,  DATA_ENTRY_122 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_123  ,  DATA_ENTRY_123 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_124  ,  DATA_ENTRY_124 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_13
                     {MENU_MSG_13, MENU_1 ,  0, 0,
                        {  {MENU_MSG_131  ,  DATA_ENTRY_131 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_132  ,  DATA_ENTRY_132 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_133  ,  DATA_ENTRY_133 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_134  ,  DATA_ENTRY_134 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_135  ,  DATA_ENTRY_135 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_136  ,  DATA_ENTRY_136 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_137  ,  DATA_ENTRY_137 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_14
                     {MENU_MSG_14, MENU_1 ,  0, 0,
                        {  {MENU_MSG_141  ,  MENU_141       ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_3, 0     },
                           {MENU_MSG_142  ,  DATA_ENTRY_142 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_2, 0     },
                           {MENU_MSG_143  ,  DATA_ENTRY_143 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_2, 0     },
                           {MENU_MSG_144  ,  MENU_144       ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_3, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_141
                     {MENU_MSG_141, MENU_14, 0, 0,
                        {  {MENU_MSG_1411 ,  DATA_ENTRY_1411,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_1412 ,  DATA_ENTRY_1412,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_3, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_144
                     {MENU_MSG_144, MENU_14, 0, 0,
                        {  {MENU_MSG_1441 ,  DATA_ENTRY_1441,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_3, 0     },
                           {MENU_MSG_1442 ,  DATA_ENTRY_1442,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_3, 0     },
                           {MENU_MSG_1443 ,  DATA_ENTRY_1443,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_3, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},

                     // MENU_15
                     {MENU_MSG_15, MENU_1 ,  0, 0,
                        {   {MENU_MSG_152  ,  DATA_ENTRY_152 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},

                           
                     // MENU_21
                     {MENU_MSG_21, MENU_2 ,  0, 0,
                        {  {MENU_MSG_211  ,  FUNCTION_211   ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_212  ,  FUNCTION_212   ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_213  ,  FUNCTION_213   ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_31
                     {MENU_MSG_31, MENU_3 ,  MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/    ,  DATA_ENTRY_TYPE_HOPPER,
                        {  {MENU_MSG_311  ,  MENU_311       ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_311
                     {MENU_MSG_311, MENU_31, 0              ,  DATA_ENTRY_TYPE_HOPPER,
                        {  {MENU_MSG_3111 ,  DATA_ENTRY_3111,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_32
                     {MENU_MSG_32, MENU_3 ,  0, 0,
                        {  {MENU_MSG_321  ,  DATA_ENTRY_321 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_322  ,  DATA_ENTRY_322 ,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_37
                     {MENU_MSG_37, MENU_3 ,  MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/    ,  DATA_ENTRY_TYPE_HOPPER,
                        {  {MENU_MSG_371  ,  MENU_371       ,  ITEM_TYPE_MENU       ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_371
                     {MENU_MSG_371, MENU_37, 0              ,  DATA_ENTRY_TYPE_HOPPER,
                        {  {MENU_MSG_3711 ,  DATA_ENTRY_3711,  ITEM_TYPE_DATA_ENTRY ,  SECURITY_LEVEL_1, 0     },
                           {MENU_MSG_3712 ,  FUNCTION_3712  ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_1, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                     // MENU_38
                     {MENU_MSG_38, MENU_3 ,  0, 0,
                        {  {MENU_MSG_381  ,  FUNCTION_381   ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_2, 0     },
                           {MENU_MSG_382  ,  FUNCTION_382   ,  ITEM_TYPE_FUNCTION   ,  SECURITY_LEVEL_2, 0     },
                           {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0},  {0, 0, 0, 0, 0}}},
                  };

CONST	char		   LabelMsg[][MAX_LANGUAGES][LCD_LINE_WIDTH + 1]	=
                  {
                     {	"----------------",  "----------------"}, // LABEL_MSG_VOID
                     {	"----------------",  "----------------"}  // void
                  };

CONST	tPullDown   PullDown[4][3]   =
                  {
                     {  {"ENGLISH"        , "������"           ,  LANGUAGE_ENGLISH     },
                        {"HEBREW"         , "�����"            ,  LANGUAGE_HEBREW      }},

                     {  {"Disabled"       , "�� ����"          ,  FALSE                },
                        {"Enabled"        , "����"             ,  TRUE                 }},

                     {  {"Unblocked"      , "�� ����"          ,  FALSE                },
                        {"Blocked"        , "����"             ,  TRUE                 }},

                     {  {"No"             , "��"               ,  FALSE                },
                        {"Yes"            , "��"               ,  TRUE                 }}
                  };

CONST tDataEntry  DataEntry[]   =
                  {
/*DATA_ENTRY_1111 */ {MENU_MSG_1111 ,  0  ,  MENU_111 ,  FIELD_TYPE_MONEY        , 4, 0, 0 , DATA_ENTRY_TYPE_HOPPER  ,  0x00, 0,  0 ,  PARAM_A_HOPPER_A_COIN_VALUE   }  ,
/*DATA_ENTRY_1112 */ {MENU_MSG_1112 ,  0  ,  MENU_111 ,  FIELD_TYPE_NUMERIC      , 5, 0, 0 , DATA_ENTRY_TYPE_HOPPER  ,  0x00, 0,  0 ,  PARAM_A_HOPPER_A_CAPACITY     }  ,
/*DATA_ENTRY_121  */ {MENU_MSG_121  ,  0  ,  MENU_12  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  BILL_20     ,  PARAM_A_BLOCKED_BILLS         }  ,
/*DATA_ENTRY_122  */ {MENU_MSG_122  ,  0  ,  MENU_12  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  BILL_50     ,  PARAM_A_BLOCKED_BILLS         }  ,
/*DATA_ENTRY_123  */ {MENU_MSG_123  ,  0  ,  MENU_12  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  BILL_100    ,  PARAM_A_BLOCKED_BILLS         }  ,
/*DATA_ENTRY_124  */ {MENU_MSG_124  ,  0  ,  MENU_12  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  BILL_200    ,  PARAM_A_BLOCKED_BILLS         }  ,
/*DATA_ENTRY_131  */ {MENU_MSG_131  ,  0  ,  MENU_13  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  COIN_00_05  ,  PARAM_A_BLOCKED_COINS         }  ,
/*DATA_ENTRY_132  */ {MENU_MSG_132  ,  0  ,  MENU_13  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  COIN_00_10  ,  PARAM_A_BLOCKED_COINS         }  ,
/*DATA_ENTRY_133  */ {MENU_MSG_133  ,  0  ,  MENU_13  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  COIN_00_50  ,  PARAM_A_BLOCKED_COINS         }  ,
/*DATA_ENTRY_134  */ {MENU_MSG_134  ,  0  ,  MENU_13  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  COIN_01_00  ,  PARAM_A_BLOCKED_COINS         }  ,
/*DATA_ENTRY_135  */ {MENU_MSG_135  ,  0  ,  MENU_13  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  COIN_02_00  ,  PARAM_A_BLOCKED_COINS         }  ,
/*DATA_ENTRY_136  */ {MENU_MSG_136  ,  0  ,  MENU_13  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  COIN_05_00  ,  PARAM_A_BLOCKED_COINS         }  ,
/*DATA_ENTRY_137  */ {MENU_MSG_137  ,  0  ,  MENU_13  ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x08, PULL_DOWN_2    ,  COIN_10_00  ,  PARAM_A_BLOCKED_COINS         }  ,
/*DATA_ENTRY_1411 */ {MENU_MSG_1411 ,  0  ,  MENU_141 ,  FIELD_TYPE_NUMERIC      , 2, 0, 0 , 0, 0x01, 0              ,  0           ,  PARAM_A_HOPPER_TIMEOUT_EMPTY  }  ,
/*DATA_ENTRY_1412 */ {MENU_MSG_1412 ,  0  ,  MENU_141 ,  FIELD_TYPE_NUMERIC      , 3, 0, 0 , 0, 0x01, 0              ,  0           ,  PARAM_A_HOPPER_TIMEOUT_DISPENSE} ,
/*DATA_ENTRY_142  */ {MENU_MSG_142  ,  0  ,  MENU_14  ,  FIELD_TYPE_MONEY        , 3, 0, 0 , 0, 0x02, 0              ,  0           ,  PARAM_A_CHANGE_LIMIT          }  ,
/*DATA_ENTRY_143  */ {MENU_MSG_143  ,  0  ,  MENU_14  ,  FIELD_TYPE_MONEY        , 3, 0, 0 , 0, 0x02, 0              ,  0           ,  PARAM_A_CHANGE_MIN_LEVEL      }  ,
/*DATA_ENTRY_1441 */ {MENU_MSG_1441 ,  0  ,  MENU_144 ,  FIELD_TYPE_NUMERIC      , 6, 0, 0 , 0, 0x01, 0              ,  0           ,  PARAM_A_PASSWORD_LEVEL_1      }  ,
/*DATA_ENTRY_1442 */ {MENU_MSG_1442 ,  0  ,  MENU_144 ,  FIELD_TYPE_NUMERIC      , 6, 0, 0 , 0, 0x01, 0              ,  0           ,  PARAM_A_PASSWORD_LEVEL_2      }  ,
/*DATA_ENTRY_1443 */ {MENU_MSG_1443 ,  0  ,  MENU_144 ,  FIELD_TYPE_NUMERIC      , 6, 0, 0 , 0, 0x01, 0              ,  0           ,  PARAM_A_PASSWORD_LEVEL_3      }  ,
/*DATA_ENTRY_15   *//*{MENU_MSG_15   ,  0  ,  MENU_1   ,  FIELD_TYPE_NUMERIC      , 1, 0, 0 , 0, 0x00, 0              ,  0           ,  PARAM_VOID                    }  ,*/                    /* 22/9/2016*/
/*DATA_ENTRY_152  */ {MENU_MSG_152  ,  0  ,  MENU_15  ,  FIELD_TYPE_NUMERIC      , 1, 0, 0 , 0, 0x00, 0              ,  0           ,  PARAM_VOID                    }  ,                       /* 22/9/2016*/
/*DATA_ENTRY_3111 */ {MENU_MSG_3111 ,  0  ,  MENU_311 ,  FIELD_TYPE_NUMERIC      , 4, 0, 0 , DATA_ENTRY_TYPE_HOPPER  ,  0x00, 0,  0 ,  PARAM_A_HOPPER_A_REFILLED     }  ,
/*DATA_ENTRY_321  */ {MENU_MSG_321  ,  0  ,  MENU_32  ,  FIELD_TYPE_SYSTEM_TIME  , 8, 0, 0 , 0, 0x00, 0              ,  0           ,  PARAM_VOID                    }  ,
/*DATA_ENTRY_322  */ {MENU_MSG_322  ,  0  ,  MENU_32  ,  FIELD_TYPE_SYSTEM_DATE  , 8, 0, 0 , 0, 0x01, 0              ,  0           ,  PARAM_VOID                    }  ,
/*DATA_ENTRY_33   */ {MENU_MSG_33   ,  0  ,  MENU_3   ,  FIELD_TYPE_PHONE_NUMBER ,12, 0, 0 , 0, 0x00, 0              ,  0           ,  PARAM_B_SERVICE_PHONE_NUMBER  }  ,
/*DATA_ENTRY_34   */ {MENU_MSG_34   ,  0  ,  MENU_3   ,  FIELD_TYPE_NUMERIC      , 2, 0, 0 , 0, 0x00, 0              ,  0           ,  PARAM_A_MACHINE_ADDRESS       }  ,
/*DATA_ENTRY_35   */ {MENU_MSG_35   ,  0  ,  MENU_3   ,  FIELD_TYPE_PULL_DOWN    , 1, 2, 0 , 0, 0x00, PULL_DOWN_0    ,  0           ,  PARAM_A_LANGUAGE              }  ,
/*DATA_ENTRY_3711 */ {MENU_MSG_3711 ,  0  ,  MENU_371 ,  FIELD_TYPE_NUMERIC      , 3, 0, 0 , DATA_ENTRY_TYPE_HOPPER  ,  0x00, 0,  0 ,  PARAM_A_HOPPER_A_QTTY_TO_EMPTY}  ,
                  };


CONST	char		   FunctionLabelMsg[][MAX_LANGUAGES][LCD_LINE_WIDTH + 1]	=
                  {
                     {	"----------------",  "----------------"}, // FUNCTION_LABEL_MSG_VOID
                     {	"Option Disabled ",  "������ �� ����� "}, // FUNCTION_LABEL_MSG_UNAVILABLE_L1
                     {	"   Press <*>    ",  "     ��� >*<    "}, // FUNCTION_LABEL_MSG_UNAVILABLE_L2
                     {	"Printing Report ",  "  ����� ��\"�   "}, // FUNCTION_LABEL_MSG_PRINTING_REPORT
                     {	" Please Wait... ",  "  ��� ����...   "}, // FUNCTION_LABEL_MSG_PLEASE_WAIT
                     {	"Print a Copy ?  ",  "������ ���� ?   "}, // FUNCTION_LABEL_MSG_REQ_TO_PRINT_COPY
                     {	"Erase Database ?",  "���� ������ ?   "}, // FUNCTION_LABEL_MSG_DELETE_DATABASE
                     {	"<*> No   Yes <#>",  ">#< ��    �� >*<"}, // FUNCTION_LABEL_MSG_YES_NO
                     {	"Qtty Out Value  ",  "  ����  ��� ����"}, // FUNCTION_LABEL_MSG_3713L1
                     {	"Insert Coin     ",  "���� ���� ������"}, // FUNCTION_LABEL_MSG_382
                     {	"----------------",  "----------------"}  // void
                  };

CONST	char		   ReportShowMsg[][MAX_LANGUAGES][LCD_LINE_WIDTH + 1]	=
                  {
                     {	"Total Sales     ",  "��\"� ���� ������"},
                     {	"Count of Sales  ",  "��\"� ���� ������"},
                     {	"Total Sales Cash",  "��\"� �����      "},
                     {	"Total Sales Crdt",  "��\"� �����      "},
                     {	"Money to Hoppers",  "��� ������ ���� "},
                     {	"Money in CashBox",  "��� ����� ������"},
                     {	"Money in BillBox",  "��� ����� ����� "},
                     {	"Bills in BillBox",  "����� �������   "},
                     {	"Money in Hopper ",  "��� ����� ����  "},
                     {	"Coins in Hopper ",  "������ �����    "},
                  };

CONST tFunction   Functions[]   =
                  {
/* FUNCTION_VOID  */ {FUNCTION_LABEL_MSG_UNAVILABLE_L1   ,  FUNCTION_LABEL_MSG_UNAVILABLE_L2 ,  MENU_0   ,  NULL                             ,  0},
/* FUNCTION_211   */ {0                                  ,  0                                ,  MENU_21  ,  Task_Setup_Func_PrintReport      ,  0},
/* FUNCTION_212   */ {0                                  ,  0                                ,  MENU_21  ,  Task_Setup_Func_PrintReport      ,  1},
/* FUNCTION_213   */ {0                                  ,  0                                ,  MENU_21  ,  Task_Setup_Func_PrintReport      ,  2},
/* FUNCTION_22    */ {0                                  ,  0                                ,  MENU_21  ,  Task_Setup_Func_ShowReport       ,  0},
/* FUNCTION_23    */ {0                                  ,  0                                ,  MENU_2   ,  Task_Setup_Func_EraseSalesInfo   ,  0},
/* FUNCTION_36    */ {0                                  ,  0                                ,  MENU_3   ,  Task_Setup_Func_SwVer            ,  0},
/* FUNCTION_39    */ {0                                  ,  0                                ,  MENU_3   ,  Task_Setup_Func_SetDeafults      ,  0},
/* FUNCTION_3712  */ {FUNCTION_LABEL_MSG_3713L1          ,  0                                ,  MENU_37  ,  Task_Setup_Func_EmptyHopper      ,  0},
/* FUNCTION_381   */ {0                                  ,  0                                ,  MENU_38  ,  Task_Setup_Func_TestPrinter      ,  0},
/* FUNCTION_382   */ {FUNCTION_LABEL_MSG_382             ,  0                                ,  MENU_38  ,  Task_Setup_Func_TestCoinRouting  ,  0},
                  };

CONST	char		   CharTable[][3 + 1]	=  // Start-Middle-End
                  {
                     {0x00, 0x00, 0x00, 0x00}   ,  // KEY 0 Unused
                     {0x00, 0x00, 0x00, 0x00}   ,  // KEY 1 (1 step up)
                     {0x00, 0x00, 0x00, 0x00}   ,  // KEY 2 (1 step down)
                     {0x00, 0x00, 0x00, 0x00}   ,  // KEY 3 Unused
                     {0xA0, 0xAC, 0xBA, 0x00}   ,  // KEY 4 range: 0xA0 - 0xBA ('�' - '�')
                     {0x30, 0x35, 0x39, 0x00}   ,  // KEY 5 range: 0x30 - 0x39 (digits)
                     {0x20, 0x28, 0x2F, 0x00}   ,  // KEY 6 range: 0x20 - 0x2F (symbols)
                     {0x41, 0x4D, 0x5A, 0x00}   ,  // KEY 7 range: 0x41 - 0x5A ('A' - 'Z')
                     {0x61, 0x6D, 0x7A, 0x00}   ,  // KEY 8 range: 0x61 - 0x7A ('a' - 'z')
                     {0x2E, 0x2D, 0x2F, 0x00}   ,  // KEY 9 '.' '-' '/'
                  };

CONST	char		   Messages[][MAX_LANGUAGES][LCD_LINE_WIDTH + 1]	=
                  {
                     {	""                ,  ""                }  ,  // MSG_VOID
                     {	" Enter Password ",  "   ��� �����    "}  ,  // MSG_ENTER_PASSWORD
                     {	"  Enter Again   ",  " ��� ����� ���� "}  ,  // MSG_ENTER_PASSWORD_2
                     {	"Invalid Password",  "  ����� �����   "}  ,  // MSG_INVALID_PASSWORD
                     {	" Access Denied  ",  " ��� ����� ���� "}  ,  // MSG_ACCESS_DENIED
                     {	" Print Z Report ",  "���� ��\"� ������"} ,  // MSG_PRINT_SALES_REPORT
                     {	"Then Erase Data ",  "���\"� ��� ������"} ,  // MSG_THEN_ERASE_DATA
                  };


CONST	char		Setup_Strings_short[STRING_SHORT_LAST][3 + 1]	=
               {//"       "
                  "%u"                                            ,  // STRING_SHORT_u
                  "%lu"                                           ,  // STRING_SHORT_lu
                  "%s"                                            ,  // STRING_SHORT_s
               };

CONST	char		Setup_Strings_long[STRING_LONG_LAST][20 + 1]	=
               {//"                    "
                  "%.2lu:%.2lu:%.2lu"                             ,  // STRING_LONG_2lu2lu2lu_TIME
                  "%.2lu/%.2lu/%.2lu"                             ,  // STRING_LONG_2lu2lu2lu_DATE
                  "%lu.%.2lu"                                     ,  // STRING_LONG_lu2lu
                  "%lu.%.2lu %3u"                                 ,  // STRING_LONG_lu2lu3u
               };
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Setup_Init( void)
/*----------------------------------------------------------------------------*/
{
   task_pSalesInfo               =  Task_Application_GetSalesInfo();
   task_pCoinsInfo               =  Task_Application_GetCoinsInfo();
   task_pBillsInfo               =  Task_Application_GetBillsInfo();
   task_pHoppersInfo             =  Task_Hopper_GetInfo();
   memset( task_Text             ,  0, sizeof( task_Text));
   memset( task_Text_Temp        ,  0, sizeof( task_Text_Temp));
   memset( task_Code             ,  0, sizeof( task_Code));
   task_Event                    =  EV_VOID;
   task_StatePassword            =  STATE_PASSWORD_IDLE;
   task_TimerSec                 =  0;
   task_TimerMsec                =  0;
   task_Password                 =  0;
   task_Key                      =  KEY_NO_KEY;
   task_KeyType                  =  KEY_TYPE_VOID;
   task_ItemCode                 =  MENU_0;
   task_ItemType                 =  ITEM_TYPE_PASSWORD;
   task_Idx                      =  0;
   task_Entry                    =  0;
   task_Offset                   =  0;
   task_Level                    =  0;
   memset( task_Selection        ,  0, sizeof( task_Selection));
   task_FunctionState            =  0;
   task_EntryIdx                 =  0;
   task_CharTableIdx             =  0;
   task_LastKey                  =  0;
   task_PullDownIdx              =  0;
   task_SecurityLevel            =  MAX_SECURITY_LEVELS;
   task_LanguageTemp             =  App_Language;
   task_fDoorSwitch              =  FALSE;
   task_fRefreshScreen           =  TRUE;
   task_fFirstKeyStroke          =  TRUE;
   task_fCodeIsEntered           =  FALSE;
   task_fEndOfSelections         =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Setup_Main( void)
/*----------------------------------------------------------------------------*/
{
   if( Task_Init_IsInitDone() == FALSE)
      return;

   Task_Setup_HandleTimer();
   Task_Setup_HandleExtEvent();
   Task_Setup_HandleEvent();

	if( App_TaskStatus[TASK_SETUP] == DISABLED)
		return;

   if( (task_Event == EV_VOID) && (task_fDoorSwitch == TRUE) && (task_TimerSec == 0))
   {
      Task_Setup_HandleKey();

      if( task_TimerSec == 0)  // this is not a bug; 'task_TimerSec' might be set within Task_Setup_HandleKey()
         Task_Setup_ShowData();
   }

   Task_Setup_HandleState();
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( task_TimerSec    <  Gap)  task_TimerSec     =  0; else  task_TimerSec     -= Gap;
      if( task_TimerSec_2  <  Gap)  task_TimerSec_2   =  0; else  task_TimerSec_2   -= Gap;
      if( task_TimerMsec	<  Gap)  task_TimerMsec    =  0; else  task_TimerMsec    -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleExtEvent( void)
/*----------------------------------------------------------------------------*/
{
   App_TaskEvent[TASK_SETUP]  =  EV_APP_VOID;

   if( App_TaskEvent[TASK_APPL] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_DOOR_LOCK_OPENED))
      {
         task_fDoorSwitch  =  TRUE;
         SET_BIT( task_Event, EV_DOOR_OPENED);
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_DOOR_LOCK_CLOSED))
      {
         task_fDoorSwitch  =  FALSE;
         SET_BIT( task_Event, EV_DOOR_CLOSED);
      }
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleState( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleEvent( void)
/*----------------------------------------------------------------------------*/
{
   if( BIT_IS_SET( task_Event, EV_UPDATE_DATA))
   {
      CLEAR_BIT( task_Event, EV_UPDATE_DATA);
      SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_DATA_UPDATED);
      return;
   }

   if( BIT_IS_SET( task_Event, EV_ENABLE_THIS_TASK))
   {
      CLEAR_BIT( task_Event, (EV_ENABLE_THIS_TASK | EV_DISABLE_THIS_TASK));
      App_TaskStatus[TASK_SETUP] =  ENABLED;
      return;
   }

   if( BIT_IS_SET( task_Event, EV_DISABLE_THIS_TASK))
   {
      CLEAR_BIT( task_Event, (EV_DISABLE_THIS_TASK | EV_ENABLE_THIS_TASK));
      App_TaskStatus[TASK_SETUP] = DISABLED;
      return;
   }

   if( BIT_IS_SET( task_Event, EV_DOOR_OPENED))
   {
      CLEAR_BIT( task_Event, (EV_DOOR_OPENED | EV_DOOR_CLOSED | EV_DISABLE_THIS_TASK));
      SET_BIT( task_Event, EV_ENABLE_THIS_TASK);
      SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_ACTIVE);

      task_ItemCode           =  MENU_0;
      task_ItemType           =  ITEM_TYPE_PASSWORD;
      task_Level              =  0;
      memset( task_Selection  ,  0, sizeof( task_Selection));
      task_PullDownIdx        =  0;
      task_EntryIdx           =  0;
      task_LanguageTemp       =  App_Language;
      task_fRefreshScreen     =  TRUE;
      task_StatePassword      =  STATE_PASSWORD_IDLE;
      return;
   }

   if( BIT_IS_SET( task_Event, EV_DOOR_CLOSED))
   {
      CLEAR_BIT( task_Event, (EV_DOOR_CLOSED | EV_DOOR_OPENED | EV_ENABLE_THIS_TASK));
      SET_BIT  ( task_Event, (EV_UPDATE_DATA | EV_DISABLE_THIS_TASK));
      SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_INACTIVE);

      LcdAPI_SetDisplayBlinking( TEXT_MODE_NORMAL);
      LcdAPI_SetCursorMode( CURSOR_MODE_HIDE);
      LcdAPI_WriteStringConst( LINE1, MSG_READY);
      return;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleKey( void)
/*----------------------------------------------------------------------------*/
{
   task_Key =  Keyboard_GetKey();

   if( (task_Key == KEY_NO_KEY) && (task_ItemType != ITEM_TYPE_FUNCTION) && (task_ItemType != ITEM_TYPE_PASSWORD))
      return;

   switch( task_Key)
   {
      case  KEY_NO_KEY:
            task_KeyType   =  KEY_TYPE_VOID;
            break;

      case  KEY_ENTER:  // '#'
            task_KeyType   =  KEY_TYPE_ENTER;
            break;

      case  KEY_ESC:    // '*'
            task_KeyType   =  KEY_TYPE_ESC;
            break;

      default:          // 0..9  F1..F4
            if( (task_Key >= KEY_0) && (task_Key <= KEY_9))
               task_KeyType   =  KEY_TYPE_DIGIT;

            if( (task_Key >= KEY_F1) && (task_Key <= KEY_F4))
               task_KeyType   =  KEY_TYPE_FUNCTION;
            break;
   }

   if( task_ItemType != ITEM_TYPE_MENU)
      task_fCodeIsEntered  =  FALSE;

   switch( task_ItemType)
   {
      case  ITEM_TYPE_MENU:
            task_fRefreshScreen  =  TRUE;
            Task_Setup_HandleKey_Menu( task_Key);
            break;

      case  ITEM_TYPE_DATA_ENTRY:
            task_fRefreshScreen  =  TRUE;
            Task_Setup_HandleKey_DataEntry( task_Key);
            break;

      case  ITEM_TYPE_FUNCTION:
            Task_Setup_HandleKey_Function( task_Key);
            break;

      case  ITEM_TYPE_PASSWORD:
            Task_Setup_HandleKey_Password( task_Key);
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleKey_Menu( byte Key)
/*----------------------------------------------------------------------------*/
{
   printf("Key %d Menu %d\n", Key, task_ItemCode);
   switch( task_KeyType)
   {
      case  KEY_TYPE_ENTER:
            if( Task_Setup_CheckMenuAccess() == FALSE)
               break;

            memset( task_Text, 0, sizeof( task_Text));

            // like case of setting up products
            if( (Menu[task_ItemCode].DataEntryType > 0) && (Menu[task_ItemCode].Entries > 0))
            {
               task_ItemType  =  Menu[task_ItemCode].Selection[0].NextItemType;
               task_ItemCode  =  Menu[task_ItemCode].Selection[0].NextItemCode;
            }
            else
            {
               task_ItemType  =  Menu[task_ItemCode].Selection[task_Selection[task_Level]].NextItemType;
               task_ItemCode  =  Menu[task_ItemCode].Selection[task_Selection[task_Level]].NextItemCode;
            }

            if( (task_Level + 1) < MAX_LEVELS)
               task_Level  ++;

            if( task_ItemType == ITEM_TYPE_MENU)
               task_Selection[task_Level] =  0;

            task_Idx                =  0;
            task_Entry              =  0;
            task_Offset             =  0;
            task_PullDownIdx        =  0;
            task_fCodeIsEntered     =  FALSE;
            task_LanguageTemp       =  App_Language;

            if( task_ItemType == ITEM_TYPE_DATA_ENTRY)
            {
               if( BIT_IS_SET( DataEntry[task_ItemCode].Flags, FLAG_DATA_ENTRY_FORCE_LTR))
                  task_LanguageTemp =  LANGUAGE_ENGLISH;

               task_fFirstKeyStroke =  TRUE;
               Task_Setup_GetDataFromField();
            }

            if( task_ItemType == ITEM_TYPE_FUNCTION)
               task_FunctionState   =  1;
            break;

      case  KEY_TYPE_ESC:
            task_ItemCode           =  Menu[task_ItemCode].PrevItem;
            task_ItemType           =  ITEM_TYPE_MENU;

            if( task_Level > 0)
               task_Level           --;

            task_Selection[task_Level] =  0;
            task_EntryIdx           =  0;
            task_fCodeIsEntered     =  FALSE;
            break;

      case  KEY_TYPE_FUNCTION:
            switch( Key)
            {
               case  KEY_F1:  // 'A'   (Up)
                     if( task_Selection[task_Level] > 0)
                        task_Selection[task_Level] --;
                     break;

               case  KEY_F2:  // 'B'   (Down)
                     if( Menu[task_ItemCode].Entries > 0)
                     {
                        if( (task_Selection[task_Level] + 1) < Menu[task_ItemCode].Entries)
                           task_Selection[task_Level] ++;
                           
                        if( task_Selection[task_Level] >= MAX_SELECTIONS)
                           task_Selection[task_Level] --;
                        break;
                     }

                     if( Menu[task_ItemCode].Selection[task_Selection[task_Level] + 1].Label != 0)
                        task_Selection[task_Level] ++;

                     if( task_Selection[task_Level] >= MAX_SELECTIONS)
                        task_Selection[task_Level] --;
                     break;

               case  KEY_F3:  // 'C'
                     memset( task_Code    ,  0, sizeof( task_Code));
                     task_Idx             =  0;
                     task_fCodeIsEntered  =  TRUE;
                     break;

               case  KEY_F4:  // 'D'
                     task_fCodeIsEntered  =  FALSE;
                     break;
            }
            break;

      case  KEY_TYPE_DIGIT:
            if( task_fCodeIsEntered == TRUE)
            {
               if( task_Idx >= (sizeof( task_Code) - 1))
                  task_Idx             =  0;

               task_Code[task_Idx ++]  =  Key;
               task_Code[task_Idx]     =  0;

               if( Menu[task_ItemCode].Selection[task_Selection[task_Level]].Label == MENU_MSG_3)
               {
                  // erase reports database
                  if( atoi( task_Code) == ERASE_SALES_INFO_PASSWORD)
                  {
                     BuzzAPI_SetMode( ON, 1);   // don't delete
                     task_fCodeIsEntered  =  FALSE;
                     task_Idx             =  0;
                     SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_RESET_SALES);
                  }

                  // erase setup database (products, parameters [tSetup_DB_Params structure])
                  if( atoi( task_Code) == ERASE_SETUP_INFO_PASSWORD)
                  {
                     BuzzAPI_SetMode( ON, 1);   // don't delete
                     task_fCodeIsEntered  =  FALSE;
                     task_Idx             =  0;
                     SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_RESET_INFO);
                  }

                  // erase log records database
                  if( atoi( task_Code) == ERASE_LOG_RECORDS_PASSWORD)
                  {
                     BuzzAPI_SetMode( ON, 1);   // don't delete
                     task_fCodeIsEntered  =  FALSE;
                     task_Idx             =  0;
                     SET_BIT( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_INFO);
                  }
               }
            }
            else
            if( (Key > KEY_0) && (Menu[task_ItemCode].Selection[Key - KEY_1].NextItemType != ITEM_TYPE_VOID))
            {
               task_Selection[task_Level] =  (Key - KEY_1);
               task_KeyType   =  KEY_TYPE_ENTER;
               Task_Setup_HandleKey_Menu( KEY_ENTER); // recursive function call
            }
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	bool						Task_Setup_CheckMenuAccess( void)
/*----------------------------------------------------------------------------*/
{
bool           fAccess;
tMessages      Message_L1;
tMessages      Message_L2;
byte           Time;
//byte           Flags;


   fAccess     =  TRUE; // by default - access granted
   Message_L1  =  MSG_VOID;
   Message_L2  =  MSG_VOID;
   Time        =  0;

   // Start with least-significant warning - then procceed to the highest level of warning (like access denial)

   /*
   Flags =  ((Menu[task_ItemCode].DataEntryType > 0) && (Menu[task_ItemCode].Entries > 0))   ?
               Menu[task_ItemCode].Selection[0].Flags                                        :
               Menu[task_ItemCode].Selection[task_Selection[task_Level]].Flags               ;
   */
   /*
   if( Flags > 0)
   {
      if( BIT_IS_SET( Flags, FLAG_MENU_ACCESS_IF_SALES_ZEROED))
      {
         if( Task_VM_DidSoldAnyProduct() == TRUE)
         {
            fAccess     =  FALSE;
            Message_L1  =  MSG_PRINT_SALES_REPORT;
            Message_L2  =  MSG_THEN_ERASE_DATA;
            Time        =  3;
         }
      }
   }
   */

   {
   byte     SecurityLevel;

      SecurityLevel  =  ((Menu[task_ItemCode].DataEntryType > 0) && (Menu[task_ItemCode].Entries > 0))   ?
                           Menu[task_ItemCode].Selection[0].SecurityLevel                                :
                           Menu[task_ItemCode].Selection[task_Selection[task_Level]].SecurityLevel       ;

      if( task_SecurityLevel < SecurityLevel)
      {
         fAccess     =  FALSE;
         Message_L1  =  MSG_ACCESS_DENIED;
         Message_L2  =  MSG_VOID;
         Time        =  2;
      }
   }

   if( fAccess == FALSE)
   {
      if( Message_L1 != MSG_VOID)
      {
         strcpy_P( task_Text_Temp, Messages[Message_L1][App_Language]);
         LcdAPI_WriteString( LINE1, 0, task_Text_Temp, LCD_LINE_WIDTH);
         LcdAPI_ClearLine( LINE2);
      }

      if( Message_L2 != MSG_VOID)
      {
         strcpy_P( task_Text_Temp, Messages[Message_L2][App_Language]);
         LcdAPI_WriteString( LINE2, 0, task_Text_Temp, LCD_LINE_WIDTH);
      }

      BuzzAPI_SetMode( FRAGMENTED_2, 2);

      task_TimerSec  =  Task_Timer_SetTimeSec( Time);
   }

   return( fAccess);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleKey_DataEntry( byte Key)
/*----------------------------------------------------------------------------*/
{
static   bool     fEnter   =  FALSE;


   switch( task_KeyType)
   {
      case  KEY_TYPE_ENTER:

            if( BIT_IS_SET( DataEntry[task_ItemCode].Flags, FLAG_DATA_ENTRY_CANT_BE_ZERO))
            {
            bool     fFieldIsZero   =  FALSE;

               if( DataEntry[task_ItemCode].FieldType == FIELD_TYPE_IP_ADDR)
               {

                  if( Util_Str2IP( task_Text) == 0)
                     fFieldIsZero   =  TRUE;
               }
               else
               {

                  if( atol( task_Text) == 0)
                     fFieldIsZero   =  TRUE;
               }

               if( fFieldIsZero == TRUE)
               {

                  BuzzAPI_SetMode( FRAGMENTED_2, 1);  // don't delete
                  task_KeyType   =  KEY_TYPE_FUNCTION;
                  Task_Setup_HandleKey_DataEntry( KEY_F3); // recursive function call
                  break;
               }
            }

            if( Task_Setup_SetDataIntoField() == FALSE)
            {
               BuzzAPI_SetMode( FRAGMENTED_2, 1);  // don't delete
               task_KeyType   =  KEY_TYPE_FUNCTION;
               Task_Setup_HandleKey_DataEntry( KEY_F3); // recursive function call
               break;
            }

            task_LanguageTemp =  App_Language;

            if( DataEntry[task_ItemCode].FieldType == FIELD_TYPE_PULL_DOWN)
            {
               task_ItemCode     =  DataEntry[task_ItemCode].PrevMenu;
               task_ItemType     =  ITEM_TYPE_MENU;
               task_Level        --;
               if( Menu[task_ItemCode].Selection[task_Selection[task_Level] + 1].Label != 0)
                  task_Selection[task_Level] ++;
               break;
            }

            if( (task_Entry + 1) < DataEntry[task_ItemCode].Entries)
            {

               task_KeyType         =  KEY_TYPE_FUNCTION;
               Task_Setup_HandleKey_DataEntry( KEY_F3); // Clear data entry; recursive function call

               task_Entry  ++;
               task_fFirstKeyStroke =  TRUE;
               Task_Setup_GetDataFromField();
               return; // because of 'task_fFirstKeyStroke'
            }
            else
            {

               fEnter         =  TRUE;
               task_KeyType   =  KEY_TYPE_ESC;
               Task_Setup_HandleKey_DataEntry( KEY_TYPE_ESC); // Exit to previous menu; recursive function call
            }
            break;

      case  KEY_TYPE_ESC:
            task_ItemCode     =  DataEntry[task_ItemCode].PrevMenu;
            task_ItemType     =  ITEM_TYPE_MENU;
            if( fEnter == TRUE)
            {
               task_Level  --;
               if( Menu[task_ItemCode].Selection[task_Selection[task_Level] + 1].Label != 0)
                  task_Selection[task_Level] ++;
               else
                  task_fEndOfSelections   =  TRUE;
            }
            else
               task_Selection[task_Level] =  0;

            task_LanguageTemp =  App_Language;
            task_Entry        =  0;
            fEnter            =  FALSE;
            break;

      case  KEY_TYPE_FUNCTION:
            if( DataEntry[task_ItemCode].FieldType == FIELD_TYPE_ALPHA_NUM)
            {
               Task_Setup_DataEntry_AlphaNum( Key);
               break;
            }

            switch( Key)
            {
               case  KEY_F1:  // 'A'   (Left or Up)
                     if( DataEntry[task_ItemCode].FieldType == FIELD_TYPE_PULL_DOWN)
                     {
                        if( (task_PullDownIdx + 1) < DataEntry[task_ItemCode].Entries)
                           task_PullDownIdx  ++;
                     }
                     else
                     {
                        if( task_Idx > 0)
                        {
                           task_Idx    --;
                           task_Offset --;

                           // skip non-digit characters (like ':' '.' or '/')
                           if( (task_Text[task_Idx] < KEY_0) || (task_Text[task_Idx] > KEY_9))
                              Task_Setup_HandleKey_DataEntry( Key); // recursive function call
                        }
                     }
                     break;

               case  KEY_F2:  // 'B'   (Right or Down)
                     if( DataEntry[task_ItemCode].FieldType == FIELD_TYPE_PULL_DOWN)
                     {
                        if( task_PullDownIdx > 0)
                           task_PullDownIdx  --;
                     }
                     else
                     {
                        if( task_Text[task_Idx + 1] != 0)
                        {
                           task_Idx    ++;
                           task_Offset ++;

                           // skip non-digit characters (like ':' '.' or '/')
                           if( (task_Text[task_Idx] < KEY_0) || (task_Text[task_Idx] > KEY_9))
                              Task_Setup_HandleKey_DataEntry( Key); // recursive function call
                        }
                     }
                     break;

               case  KEY_F3:  // 'C'   (Clear data entry)
                     Task_Setup_ClearDataEntry();
                     task_Idx          =  0;
                     task_Offset       =  0;
                     break;

               case  KEY_F4:  // 'D'
                     {
                     char     Char  =  0;

                        switch( DataEntry[task_ItemCode].FieldType)
                        {
                           case  FIELD_TYPE_PHONE_NUMBER :  Char  =  '-';  break;
                           case  FIELD_TYPE_IP_ADDR      :  Char  =  '.';  break;
                           default                       :  Char  =  0  ;  break;
                        }

                        if( Char != 0)
                        {
                           if( task_fFirstKeyStroke == TRUE) // if first key stroke - clear data entry
                           {
                              task_KeyType         =  KEY_TYPE_FUNCTION;
                              Task_Setup_HandleKey_DataEntry( KEY_F3); // recursive function call
                           }

                           task_Text[task_Idx]  =  Char;

                           if( (task_Idx + 1) < DataEntry[task_ItemCode].Length)
                           {
                              if( task_Text[task_Idx + 1] == 0)
                                 task_Text[task_Idx + 1] =  SPACE;
                              task_KeyType   =  KEY_TYPE_FUNCTION;
                              Task_Setup_HandleKey_DataEntry( KEY_F2); // Move 1 position to the right; recursive function call
                           }
                        }
                     }
                     break;
            }
            break;

      case  KEY_TYPE_DIGIT:
            if( task_fFirstKeyStroke == TRUE) // if first key stroke - clear data entry
            {
               task_CharTableIdx =  0;
               task_LastKey      =  0;

               task_KeyType   =  KEY_TYPE_FUNCTION;
               Task_Setup_HandleKey_DataEntry( KEY_F3); // recursive function call
               task_KeyType   =  KEY_TYPE_DIGIT;
               // DON'T 'break' HERE !!! (recursive call)
            }

            if( DataEntry[task_ItemCode].FieldType == FIELD_TYPE_ALPHA_NUM)
            {
               Task_Setup_DataEntry_AlphaNum( Key);
               break;
            }

            task_Text[task_Idx]  =  Key;

            if( (task_Idx + 1) < DataEntry[task_ItemCode].Length)
            {
               switch( DataEntry[task_ItemCode].FieldType)
               {
                  case  FIELD_TYPE_MONEY:
                        LcdAPI_SetCursorMode( CURSOR_MODE_HIDE);
                        task_Idx ++;
                        break;

                  default:
                        if( task_Text[task_Idx + 1] == 0)
                           task_Text[task_Idx + 1] =  SPACE;
                        task_KeyType   =  KEY_TYPE_FUNCTION;
                        Task_Setup_HandleKey_DataEntry( KEY_F2); // Move 1 position to the right; recursive function call
                        break;
               }
            }
            break;
   }

   task_fFirstKeyStroke =  FALSE;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_DataEntry_AlphaNum( byte Key)
/*----------------------------------------------------------------------------*/
{
char     Char;


   if( task_KeyType == KEY_TYPE_DIGIT)
      Char  =  task_Text[task_Idx];
   else
   {
      task_LastKey      =  0;
      task_CharTableIdx =  0;
   }

   switch( Key)
   {
      case  KEY_1    :  // '1'
            Char  ++;         // 1 step up

            if( (Char >= 0x80) && (Char <= 0x9F))  // 'dead' zone in LCD ASCII table
               Char  =  0xA0; // high boundary
            break;

      case  KEY_2    :  // '2'
            Char  --;         // 1 step down

            if( (Char >= 0x80) && (Char <= 0x9F))  // 'dead' zone in LCD ASCII table
               Char  =  0x7F; // low boundary
            break;

      case  KEY_3    :  // '3'
            break;

      case  KEY_4    :  // '4'
      case  KEY_5    :  // '5'
      case  KEY_6    :  // '6'
      case  KEY_7    :  // '7'
      case  KEY_8    :  // '8'
      case  KEY_9    :  // '9'
      case  KEY_0    :  // '0'
            if( CharTable[Key - '0'][0] == 0x00)
               break;

            if( task_LastKey != Key)
               task_CharTableIdx =  0;

            task_LastKey   =  Key;

            Char  =  CharTable[Key - '0'][task_CharTableIdx];

            if( CharTable[Key - '0'][task_CharTableIdx + 1] ==  0)
               task_CharTableIdx =  0;
            else
               task_CharTableIdx ++;
            break;

      case  KEY_F1   :  // 'A'   (left)
            if( (task_Idx > 0) && (task_Idx > (LCD_LINE_WIDTH - DataEntry[task_ItemCode].Length)))
               task_Idx --;

            task_Offset =  task_Idx;
            break;

      case  KEY_F2   :  // 'B'   (right)
            task_Idx ++;
            if( task_Idx >= LCD_LINE_WIDTH)
               task_Idx =  (LCD_LINE_WIDTH - 1);
            task_Offset =  task_Idx;
            break;

      case  KEY_F3   :  // 'C'   (Clear data entry)
            Task_Setup_ClearDataEntry();

            if( task_LanguageTemp == LANGUAGE_HEBREW)
            {
               task_Idx    =  (LCD_LINE_WIDTH - 1);
               task_Offset =  task_Idx;
            }
            else
            {
               task_Idx    =  0;
               task_Offset =  0;
            }

            task_CharTableIdx =  0;
            task_LastKey      =  0;
            break;

      case  KEY_F4   :  // 'D'
            Char  =  '.';
            break;

      default:
            break;
   }

   if( task_KeyType == KEY_TYPE_DIGIT)
   {
      if( Char > 0xBA /*'�'*/)
         Char  =  0x20;

      if( Char < 0x20)
         Char  =  0xBA /*'�'*/;
      task_Text[task_Idx]  =  Char;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleKey_Function( byte Key)
/*----------------------------------------------------------------------------*/
{
   if( Functions[task_ItemCode].Function == NULL)
      task_FunctionState   =  0; // for KEY_ESC
   else
      Functions[task_ItemCode].Function( Functions[task_ItemCode].Param1);

   switch( task_KeyType)
   {
      case  KEY_TYPE_ENTER:
            if( task_FunctionState == 0)
               task_FunctionState   =  1;
            break;

      case  KEY_TYPE_ESC:
            if( task_FunctionState == 0)
            {
               task_ItemCode        =  Functions[task_ItemCode].PrevMenu;
               task_ItemType        =  ITEM_TYPE_MENU;
               task_fRefreshScreen  =  TRUE;

               if( Menu[task_ItemCode].Entries > 0)
                  task_Level  --;   // stay pointing at last entry
               /*
               // proceed automatically to next entry
               if( Menu[task_ItemCode].Entries > 0)
               {
                  if( (task_Selection[task_Level] + 1) < Menu[task_ItemCode].Entries)
                     task_Selection[task_Level] ++;
               }
               else
               if( Menu[task_ItemCode].Selection[task_Selection[task_Level] + 1].Label != 0)
                  task_Selection[task_Level] ++;
               */
            }
            break;

      case  KEY_TYPE_FUNCTION:
            switch( Key)
            {
               case  KEY_F1:  // 'A'   (Up [Previous])
                     if( task_FunctionState == 0)
                        task_FunctionState   =  1;
                     break;

               case  KEY_F2:  // 'B'   (Down [Next])
                     task_KeyType   =  KEY_TYPE_ENTER;
                     Task_Setup_HandleKey_Function( KEY_ENTER);
                     break;
            }
            break;

      case  KEY_TYPE_DIGIT:
            break;

      default:
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_HandleKey_Password( byte Key)
/*----------------------------------------------------------------------------*/
{
#define  MAX_DIGITS           6
static   byte     Digits   =  0;


   switch( task_StatePassword)
   {
      case  STATE_PASSWORD_IDLE:
            task_SecurityLevel   =  MAX_SECURITY_LEVELS;
            task_Password        =  0;
            Digits               =  0;
            memset( task_Text    ,  0, sizeof( task_Text));

            LcdAPI_ClearScreen();
            LcdAPI_SetDisplayBlinking( OFF);
            ClockAPI_DisplayClock( DISABLED);
            strcpy_P( task_Text_Temp, Messages[MSG_ENTER_PASSWORD][App_Language]);
            LcdAPI_WriteString( LINE1, 0, task_Text_Temp, LCD_LINE_WIDTH);

            task_StatePassword      =  STATE_PASSWORD_DATA_ENTRY;
            break;

      case  STATE_PASSWORD_DATA_ENTRY:
            switch( Key)
            {
               case  KEY_NO_KEY:
                     break;

               case  KEY_F3:     // 'C'
                     task_StatePassword   =  STATE_PASSWORD_IDLE;
                     break;

               case  KEY_ENTER:  // '#'
                     task_StatePassword   =  STATE_PASSWORD_VERIFY_DATA;
                     break;

               case  KEY_ESC:    // '*'
                     break;

               default:          // 0..9  F1..F4
                     if( (Key >= KEY_0) && (Key <= KEY_9) && (Digits < MAX_DIGITS))
                     {
                        task_Password  *= 10;
                        task_Password  += (Key - '0');
                        Digits         ++;
                        memset( task_Text, '*', Digits);
                        LcdAPI_WriteString( LINE2, 0, task_Text, LCD_LINE_WIDTH);
                     }
                     break;
            }
            break;

      case  STATE_PASSWORD_VERIFY_DATA:
            task_StatePassword   =  STATE_PASSWORD_INVALID;

            if( task_Password == SECURITY_PASSWORD)
            {
               task_SecurityLevel   =  SECURITY_LEVEL_3;
               task_StatePassword   =  STATE_PASSWORD_APPROVED;
               break;
            }

            {
            byte           Index;
            byte           Index_Highest;
            tParam_Code_A  ParamCode;

               Index_Highest  =  MAX_SECURITY_LEVELS;
               for( Index = 0; Index < MAX_SECURITY_LEVELS; Index ++)
               {
                  if( Index == SECURITY_LEVEL_1)   ParamCode   =  PARAM_A_PASSWORD_LEVEL_1;
                  if( Index == SECURITY_LEVEL_2)   ParamCode   =  PARAM_A_PASSWORD_LEVEL_2;
                  if( Index == SECURITY_LEVEL_3)   ParamCode   =  PARAM_A_PASSWORD_LEVEL_3;

                  Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, ParamCode);

                  if( task_Password == task_Rec_Type_A)
                     Index_Highest  =  Index;   // Same password can be shared by multiple security levels
               }

               if( Index_Highest < MAX_SECURITY_LEVELS)
               {
                  task_SecurityLevel   =  Index_Highest;
                  task_StatePassword   =  STATE_PASSWORD_APPROVED;
               }
            }
            break;

      case  STATE_PASSWORD_INVALID:
            BuzzAPI_SetMode( FRAGMENTED_2, 1);
            task_StatePassword   =  STATE_PASSWORD_IDLE;
            break;

      case  STATE_PASSWORD_APPROVED:
            BuzzAPI_SetMode( FRAGMENTED_3, 1);
            task_StatePassword   =  STATE_PASSWORD_IDLE;

            task_fRefreshScreen     =  TRUE;
            task_ItemType           =  ITEM_TYPE_MENU;
            break;

      default:
            break;
   }
#undef   MAX_DIGITS
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_GetDataFromField( void)
/*----------------------------------------------------------------------------*/
{
tParam_Code_A  ParamCode;


   ParamCode   =  (tParam_Code_A)DataEntry[task_ItemCode].ParamCode;

   switch( DataEntry[task_ItemCode].FieldType)
   {
      case  FIELD_TYPE_SYSTEM_TIME:
      case  FIELD_TYPE_SYSTEM_DATE:
            break;

      default:
            if( ParamCode == PARAM_VOID)
               return;
            break;
   }

   if( DataEntry[task_ItemCode].DataEntryType > 0)
   {
      switch( DataEntry[task_ItemCode].DataEntryType)
      {
         case  DATA_ENTRY_TYPE_HOPPER:
               ParamCode  += task_EntryIdx;
               break;
      }
   }

   memset( task_Text_Temp, 0, sizeof( task_Text_Temp));

   switch( DataEntry[task_ItemCode].FieldType)
   {
      case  FIELD_TYPE_ALPHA_NUM:
            {
            byte  Length;

               Task_Setup_ClearDataEntry();
               Task_Params_Read( task_Rec_Type_B, PARAM_TYPE_B, (tParam_Code_B)ParamCode);
               memcpy( task_Text_Temp, task_Rec_Type_B, (sizeof( task_Text_Temp) - 1));

               Length   =  strlen( task_Text_Temp);
               if( Length > LCD_LINE_WIDTH)
                  Length   =  LCD_LINE_WIDTH;

               if( task_LanguageTemp == LANGUAGE_HEBREW)
                  memcpy( task_Text + (LCD_LINE_WIDTH - Length), task_Text_Temp, Length);
               else
                  memcpy( task_Text, task_Text_Temp, Length);

               if( task_LanguageTemp == LANGUAGE_HEBREW)
               {
                  task_Idx    =  (LCD_LINE_WIDTH - 1);
                  task_Offset =  task_Idx;
               }
               else
               {
                  task_Idx    =  0;
                  task_Offset =  0;
               }
            }
            break;

      case  FIELD_TYPE_MONEY:
      case  FIELD_TYPE_NUMERIC:
            Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, ParamCode);
            if( BIT_IS_SET( DataEntry[task_ItemCode].Flags, FLAG_DATA_ENTRY_MULTIPLE_BY_100))
               task_Rec_Type_A  /= 100;
            sprintf_P( task_Text, Setup_Strings_short[STRING_SHORT_lu] , task_Rec_Type_A);
            break;

      case  FIELD_TYPE_IP_ADDR:
            Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, ParamCode);
            Util_IP2Str( task_Rec_Type_A, task_Text, FALSE);
            break;

      case  FIELD_TYPE_TIME:
            break;

      case  FIELD_TYPE_DATE:
            break;

      case  FIELD_TYPE_SYSTEM_TIME:
            {
            ulong    Time;

               Time  =  ClockAPI_GetTime( TIME_FORMAT_HHMMSS);
               sprintf_P( task_Text, Setup_Strings_long[STRING_LONG_2lu2lu2lu_TIME], Time / 10000, (Time / 100) % 100, Time % 100);
            }
            task_fFirstKeyStroke =  FALSE;
            break;

      case  FIELD_TYPE_SYSTEM_DATE:
            {
            ulong    Date;

               Date  =  ClockAPI_GetDate( DATE_FORMAT_DDMMYY);
               sprintf_P( task_Text, Setup_Strings_long[STRING_LONG_2lu2lu2lu_DATE], Date / 10000, (Date / 100) % 100, Date % 100);
            }
            task_fFirstKeyStroke =  FALSE;
            break;

      case  FIELD_TYPE_PHONE_NUMBER:
            Task_Params_Read( task_Rec_Type_B, PARAM_TYPE_B, (tParam_Code_B)ParamCode);
            if( task_Rec_Type_B[0] > 0)
               sprintf_P( task_Text, Setup_Strings_short[STRING_SHORT_s], task_Rec_Type_B);
            else
               memset( task_Text, 0, sizeof( task_Text));
            break;

      case  FIELD_TYPE_PULL_DOWN:
            Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, ParamCode);
            task_PullDownIdx  =  task_Rec_Type_A;

            if( BIT_IS_SET( DataEntry[task_ItemCode].Flags, FLAG_DATA_ENTRY_BITS_PARAM))
            {
               task_PullDownIdx  =  0; // FALSE
               if( BIT_IS_SET( task_Rec_Type_A, (1 << DataEntry[task_ItemCode].DataIdx)))
                  task_PullDownIdx  =  1; // TRUE
            }
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	bool						Task_Setup_SetDataIntoField( void)
/*----------------------------------------------------------------------------*/
{
byte          *pByte;
byte           Idx;
bool           fResult;
byte           ParamType;
tParam_Code_A  ParamCode;


   ParamCode   =  (tParam_Code_A)DataEntry[task_ItemCode].ParamCode;

   switch( DataEntry[task_ItemCode].FieldType)
   {
      case  FIELD_TYPE_SYSTEM_TIME:
      case  FIELD_TYPE_SYSTEM_DATE:
            break;

      default:
            if( ParamCode == PARAM_VOID)
               return( FALSE);
            break;
   }

   if( DataEntry[task_ItemCode].DataEntryType > 0)
   {
      switch( DataEntry[task_ItemCode].DataEntryType)
      {
         case  DATA_ENTRY_TYPE_HOPPER:
#if 0
           if( ParamCode == PARAM_A_HOPPER_A_REFILLED)
               SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_STOP_REFILLING);
#endif
            switch(ParamCode)
            {
               case PARAM_A_HOPPER_A_REFILLED:
                  SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_STOP_REFILLING);
                  break;
                  
               case PARAM_A_HOPPER_A_COIN_VALUE:
               case PARAM_A_HOPPER_B_COIN_VALUE:
               case PARAM_A_HOPPER_C_COIN_VALUE:
               case PARAM_A_HOPPER_D_COIN_VALUE:
               case PARAM_A_HOPPER_A_CAPACITY:
               case PARAM_A_HOPPER_B_CAPACITY:
               case PARAM_A_HOPPER_C_CAPACITY:
               case PARAM_A_HOPPER_D_CAPACITY:

                  SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_DATA_UPDATED);
                  break;
            }
            
            ParamCode  += task_EntryIdx;
            break;
      }
   }

   fResult  =  TRUE;

   switch( DataEntry[task_ItemCode].FieldType)
   {
      case  FIELD_TYPE_ALPHA_NUM:
            ParamType   =  PARAM_TYPE_B;
            memset( task_Text_Temp, 0, sizeof( task_Text_Temp));

            if( task_LanguageTemp == LANGUAGE_HEBREW)
               Util_StrCpy( task_Text_Temp, task_Text + (LCD_LINE_WIDTH - DataEntry[task_ItemCode].Length), DataEntry[task_ItemCode].Length);
            else
               Util_StrCpy( task_Text_Temp, task_Text, DataEntry[task_ItemCode].Length);

            // eliminate ending spaces
            pByte =  (byte *)task_Text_Temp;
            for( Idx = (DataEntry[task_ItemCode].Length - 1); (pByte[Idx] == SPACE); Idx --)
            {
               pByte[Idx]  =  0;
               if( Idx == 0)
                  break;
            }
            break;

      case  FIELD_TYPE_MONEY:
            {
            char  *pStr;
            bool  fFound;

               fFound = FALSE;

               // remove decimal point from string
               for( pStr = task_Text; *pStr != 0; pStr ++)
               {
                  if( *pStr == '.')
                     fFound   =  TRUE;

                  if( fFound == TRUE)
                     *pStr =  *(pStr + 1);
               }
            }
            // DON'T BREAK !!! (Money is now a numeric string)

      case  FIELD_TYPE_NUMERIC:
            ParamType   =  PARAM_TYPE_A;
            task_Rec_Type_A  =  atol( task_Text);
            if( BIT_IS_SET( DataEntry[task_ItemCode].Flags, FLAG_DATA_ENTRY_MULTIPLE_BY_100))
               task_Rec_Type_A  *= 100;
            break;

      case  FIELD_TYPE_IP_ADDR:
            ParamType   =  PARAM_TYPE_A;
            task_Rec_Type_A  =  Util_Str2IP( task_Text);
            break;

      case  FIELD_TYPE_TIME:
            break;

      case  FIELD_TYPE_DATE:
            break;

      case  FIELD_TYPE_SYSTEM_TIME:
      case  FIELD_TYPE_SYSTEM_DATE:
            {
            char     Str[6 + 1];
            byte     Idx_r;
            byte     Idx_w;


               Str[0] = 0;
               for( Idx_r = 0, Idx_w = 0; task_Text[Idx_r] != 0; Idx_r ++)
               {
                  if( (task_Text[Idx_r] >= '0') && (task_Text[Idx_r] <= '9'))
                  {
                     Str[Idx_w ++]  =  task_Text[Idx_r];
                     Str[Idx_w]     =  0;
                  }
               }

               if( strlen( Str) == (sizeof( Str) - 1))
               {
                  switch( DataEntry[task_ItemCode].FieldType)
                  {
                     case  FIELD_TYPE_SYSTEM_TIME:
                           if( ClockAPI_SetTime( atol( Str), TIME_FORMAT_HHMMSS) == FALSE)
                              fResult  =  FALSE;
                           break;

                     case  FIELD_TYPE_SYSTEM_DATE:
                           if( ClockAPI_SetDate( atol( Str), DATE_FORMAT_DDMMYY) == FALSE)
                              fResult  =  FALSE;
                           break;
                  }
               }
            }
            break;

      case  FIELD_TYPE_PHONE_NUMBER:
            ParamType   =  PARAM_TYPE_B;
            break;

      case  FIELD_TYPE_PULL_DOWN:
            ParamType   =  PARAM_TYPE_A;

            if( BIT_IS_SET( DataEntry[task_ItemCode].Flags, FLAG_DATA_ENTRY_BITS_PARAM))
            {
               if( task_PullDownIdx == 0)
                  CLEAR_BIT( task_Rec_Type_A, (1 << DataEntry[task_ItemCode].DataIdx));
               else
                  SET_BIT  ( task_Rec_Type_A, (1 << DataEntry[task_ItemCode].DataIdx));
            }
            else
               task_Rec_Type_A   =  task_PullDownIdx;

            // Set Application Language
            if( ParamCode == PARAM_A_LANGUAGE)
               App_Language   =  task_PullDownIdx;
            break;
   }

   printf("Save\n");
   if( ParamType == PARAM_TYPE_A)
      Task_Params_Write( &task_Rec_Type_A, PARAM_TYPE_A, ParamCode);

   if( ParamType == PARAM_TYPE_B)
   {
      memcpy( task_Rec_Type_B, task_Text, sizeof( task_Rec_Type_B));
      Task_Params_Write( task_Rec_Type_B, PARAM_TYPE_B, (tParam_Code_B)ParamCode);
   }

   BuzzAPI_SetMode( FRAGMENTED_3, 1);

   return( fResult);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_ClearDataEntry( void)
/*----------------------------------------------------------------------------*/
{
   switch( DataEntry[task_ItemCode].FieldType)
   {
      case  FIELD_TYPE_ALPHA_NUM:
            memset( task_Text, ' ', sizeof( task_Text));
            task_Text[sizeof( task_Text) - 1]   =  0;
            break;

      case  FIELD_TYPE_MONEY:
      case  FIELD_TYPE_NUMERIC:
      case  FIELD_TYPE_IP_ADDR:
      case  FIELD_TYPE_PHONE_NUMBER:
            memset( task_Text, 0, sizeof( task_Text));
            break;

      case  FIELD_TYPE_TIME:
            memset( task_Text ,  SPACE, sizeof( task_Text));
            task_Text[2]   =  ':';
            task_Text[5]   =  ':';
            task_Text[sizeof( task_Text) - 1] = 0;
            break;

      case  FIELD_TYPE_DATE:
            memset( task_Text ,  SPACE, sizeof( task_Text));
            task_Text[3]   =  '/';
            task_Text[7]   =  '/';
            task_Text[11]  =  '/';
            task_Text[sizeof( task_Text) - 1] = 0;
            break;

      case  FIELD_TYPE_SYSTEM_TIME:
      case  FIELD_TYPE_SYSTEM_DATE:
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_ShowData( void)
/*----------------------------------------------------------------------------*/
{
   if( task_fRefreshScreen == TRUE)
   {
   char  String[LCD_LINE_WIDTH + 1];

      task_fRefreshScreen  =  FALSE;

      switch( task_ItemType)
      {
         case  ITEM_TYPE_MENU:
               {
               byte     Length1;
               byte     Length2;
               byte     Length3;
               byte     Length4;
               byte     Offset1;
               byte     Offset2;
               byte     Offset3;
               byte     Offset4;

                  ClockAPI_DisplayClock( DISABLED);
                  LcdAPI_ClearScreen();

                  if( task_fEndOfSelections == TRUE)
                  {
                     task_fEndOfSelections   =  FALSE;

                     if( task_Level > 0)
                     {
                        task_ItemCode     =  Menu[task_ItemCode].PrevItem;
                        task_ItemType     =  ITEM_TYPE_MENU;
                        task_Level        --;

                        if( Menu[task_ItemCode].Entries > 0)
                        {
                           if( (task_Selection[task_Level] + 1) < Menu[task_ItemCode].Entries)
                              task_Selection[task_Level] ++;
                        }
                        else
                           if( Menu[task_ItemCode].Selection[task_Selection[task_Level] + 1].Label != 0)
                              task_Selection[task_Level] ++;
                     }
                  }

                  // Headline
                  LcdAPI_SetDisplayBlinking( TEXT_MODE_NORMAL);
                  strcpy_P( String, MenuMsg[Menu[task_ItemCode].HeadLine][App_Language]);
                  Length3  =  strlen( String);
                  Offset3  =  (LCD_LINE_WIDTH - Length3) / 2;
                  LcdAPI_WriteString( LINE1, Offset3, String, Length3);

                  if( (Menu[task_ItemCode].DataEntryType > 0) && (Menu[task_ItemCode].Entries == 0))
                  {
                     switch( Menu[task_ItemCode].DataEntryType)
                     {
                        case  DATA_ENTRY_TYPE_HOPPER:
                              Length4  =  (task_EntryIdx + 1) < 10 ? 1 : 2;
                              Offset4  =  (App_Language == LANGUAGE_HEBREW) ? (Offset3 - Length4 - 1) : (Offset3 + Length3 + 1);
                              //LcdAPI_WriteNumber( LINE1, Offset4, task_EntryIdx + 1, Length4);
                              LcdAPI_WriteChar( LINE1, Offset4, (task_EntryIdx + 'A'));
                              break;
                     }
                  }

                  // Selection (1..n)
                  Length1  =  ((task_Selection[task_Level] + 1) < 10) ? 1 : 2;
                  Offset1  =  (App_Language == LANGUAGE_HEBREW) ? (LCD_LINE_WIDTH - Length1) : 0;
                  LcdAPI_WriteNumber( LINE2, Offset1, (task_Selection[task_Level] + 1), Length1);

                  // '.'
                  Length2  =  1;
                  Offset2  =  (App_Language == LANGUAGE_HEBREW) ? (Offset1 - Length2) : (Offset1 + Length1);
                  LcdAPI_WriteChar( LINE2, Offset2, '.');

                  if( Menu[task_ItemCode].Entries > 0)
                  {
                     // Lable
                     strcpy_P( String, MenuMsg[Menu[task_ItemCode].Selection[0].Label][App_Language]);
                     Length3  =  strlen( String);
                     Offset3  =  (App_Language == LANGUAGE_HEBREW) ? (Offset2 - Length3) : (Offset2 + Length2);
                     LcdAPI_WriteString( LINE2, Offset3, String, Length3);

                     switch( Menu[task_ItemCode].DataEntryType)
                     {
                        case  DATA_ENTRY_TYPE_HOPPER:
                              task_EntryIdx  =  task_Selection[task_Level];
                              Length4  =  (task_EntryIdx + 1) < 10 ? 1 : 2;
                              Offset4  =  (App_Language == LANGUAGE_HEBREW) ? (Offset3 - Length4 - 1) : (Offset3 + Length3 + 1);
                              //LcdAPI_WriteNumber( LINE2, Offset4, (task_EntryIdx + 1), Length4);
                              LcdAPI_WriteChar( LINE2, Offset4, (task_EntryIdx + 'A'));
                              break;
                     }
                  }
                  else
                  {
                     // Lable
                     strcpy_P( String, MenuMsg[Menu[task_ItemCode].Selection[task_Selection[task_Level]].Label][App_Language]);
                     Length3  =  strlen( String);
                     Offset3  =  (App_Language == LANGUAGE_HEBREW) ? (Offset2 - Length3) : (Offset2 + Length2);
                     LcdAPI_WriteString( LINE2, Offset3, String, Length3);
                  }
               }
               break;

         case  ITEM_TYPE_DATA_ENTRY:
               {
               byte     Length;
               byte     Offset;

                  ClockAPI_DisplayClock( DISABLED);
                  LcdAPI_ClearScreen();

                  if( DataEntry[task_ItemCode].FieldType == FIELD_TYPE_PULL_DOWN)
                  {
                     strcpy_P( String, MenuMsg[DataEntry[task_ItemCode].HeadLine][App_Language]);
                     Length   =  strlen( String);
                     Offset   =  (App_Language == LANGUAGE_HEBREW) ? (LCD_LINE_WIDTH - Length) : 0;
                     LcdAPI_WriteString( LINE1, Offset, String, Length);

                     strcpy_P( String, PullDown[DataEntry[task_ItemCode].PullDownIndex][task_PullDownIdx].Text[App_Language]);
                     Length   =  strlen( String);
                     Offset   =  (task_LanguageTemp == LANGUAGE_HEBREW) ? (LCD_LINE_WIDTH - Length) : 0;
                     LcdAPI_WriteString( LINE2, Offset, String, Length);
                  }
                  else
                  {
                     strcpy_P( String, MenuMsg[DataEntry[task_ItemCode].HeadLine][App_Language]);
                     Length   =  strlen( String);
                     Offset   =  (App_Language == LANGUAGE_HEBREW) ? (LCD_LINE_WIDTH - Length) : 0;
                     LcdAPI_WriteString( LINE1, Offset, String, Length);

                     if( DataEntry[task_ItemCode].Label > 0)
                     {
                        strcpy_P( String, LabelMsg[DataEntry[task_ItemCode].Label][App_Language]);
                        Length   =  strlen( String);
                        Offset   =  (task_LanguageTemp == LANGUAGE_HEBREW) ? (LCD_LINE_WIDTH - Length) : 0;
                        LcdAPI_WriteString( LINE2, Offset, String, Length);
                     }

                     if( (DataEntry[task_ItemCode].Entries > 0) && (task_Entry < DataEntry[task_ItemCode].Entries))
                     {
                        if( task_LanguageTemp == LANGUAGE_HEBREW)
                        {
                           LcdAPI_WriteNumber(  DataEntry[task_ItemCode].EntriesOffset / LCD_LINE_WIDTH,
                                                DataEntry[task_ItemCode].EntriesOffset % LCD_LINE_WIDTH,
                                                task_Entry + 1, 1);
                        }
                        else
                           LcdAPI_WriteNumber(  LINE1, 15, task_Entry + 1, 1);
                     }

                     switch( DataEntry[task_ItemCode].FieldType)
                     {
                        case  FIELD_TYPE_MONEY:
                              {
                              ulong    Value_l;

                                 Value_l  =  atol( task_Text);
                                 if( BIT_IS_SET( DataEntry[task_ItemCode].Flags, FLAG_DATA_ENTRY_MULTIPLE_BY_100))
                                    Value_l  *= 100;

                                 sprintf_P( task_Text_Temp, Setup_Strings_long[STRING_LONG_lu2lu] , Value_l / 100, Value_l % 100);
                                 LcdAPI_WriteString( LINE2, 0, task_Text_Temp, strlen( task_Text_Temp));
                              }
                              break;

                        default:
                              LcdAPI_SetCursorPosition( LINE2, task_Offset);
                              LcdAPI_SetCursorMode( CURSOR_MODE_BLINK);                              
                              LcdAPI_DontSwapRight2Left();                                              //LcdAPI_DontSwapHebrew();
                              LcdAPI_WriteString( LINE2, 0, task_Text, strlen( task_Text));
                              break;
                     }
                  }
               }
               break;

         case  ITEM_TYPE_FUNCTION:
               ClockAPI_DisplayClock( DISABLED);
               LcdAPI_ClearScreen();

               if( Functions[task_ItemCode].Function == NULL)
               {
                  BuzzAPI_SetMode( FRAGMENTED_2, 1);
                  strcpy_P( String, FunctionLabelMsg[FUNCTION_LABEL_MSG_UNAVILABLE_L1][App_Language]);
                  LcdAPI_WriteString( LINE1, 0, String, LCD_LINE_WIDTH);
                  strcpy_P( String, FunctionLabelMsg[FUNCTION_LABEL_MSG_UNAVILABLE_L2][App_Language]);
                  LcdAPI_WriteString( LINE2, 0, String, LCD_LINE_WIDTH);
               }
               else
               {
                  if( Functions[task_ItemCode].Label_L1 > 0)
                  {
                     strcpy_P( String, FunctionLabelMsg[Functions[task_ItemCode].Label_L1][App_Language]);
                     LcdAPI_WriteString( LINE1, 0, String, LCD_LINE_WIDTH);
                  }

                  if( Functions[task_ItemCode].Label_L2 > 0)
                  {
                     strcpy_P( String, FunctionLabelMsg[Functions[task_ItemCode].Label_L2][App_Language]);
                     LcdAPI_WriteString( LINE2, 0, String, LCD_LINE_WIDTH);
                  }
               }
               break;
      }
   }
}



#if 0
0000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Menu Functions      0x0000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_Func_PrintReport( byte   Code)
/*----------------------------------------------------------------------------*/
{
static   byte     OpCode;

#define  STATE_IDLE                          0
#define  STATE_ASSIGN_REPORT_CODE            1
#define  STATE_PRINT_REPORT                  2
#define  STATE_DURING_PRINTOUT               3
#define  STATE_ASK_IF_WANT_ANOTHER_COPY      4
#define  STATE_ASK_IF_WANT_TO_DELETE         5
#define  STATE_GET_ANSWER_ANOTHER_COPY       6
#define  STATE_GET_ANSWER_WANT_TO_DELETE     7
#define  STATE_DELETE_DATA                   8
#define  STATE_END                           9


   switch( task_FunctionState)
   {
      case  STATE_IDLE:
            OpCode   =  0;
            break;

      case  STATE_ASSIGN_REPORT_CODE:
            OpCode   =  Code;
            task_FunctionState   ++;
            break;

      case  STATE_PRINT_REPORT:
            switch( OpCode)
            {
               case  0: SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RPRT_DOOR_OPEN);    break;
               case  1: SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RPRT_DOOR_CLOSE);   break;
               case  2: SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_REPORT_FISCAL);     break;
               case  3: SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_EVENTS);            break;
               case  4: SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_REPORT_Z);          break;
               case  5: SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_COIN_ROUTING);      break;
               case  6: SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_SETUP_INFO);        break;
               default:                                                                            break;
            }

            strcpy_P( task_Text_Temp, FunctionLabelMsg[FUNCTION_LABEL_MSG_PRINTING_REPORT][App_Language]);
            LcdAPI_WriteString( LINE1, 0, task_Text_Temp, LCD_LINE_WIDTH);
            strcpy_P( task_Text_Temp, FunctionLabelMsg[FUNCTION_LABEL_MSG_PLEASE_WAIT][App_Language]);
            LcdAPI_WriteString( LINE2, 0, task_Text_Temp, LCD_LINE_WIDTH);
            LcdAPI_SetDisplayBlinking( ON);

            switch( OpCode)
            {
               //case  0: // Sales report
               //case  1: // Fiscal report
               //case  2: // Events report
               case  3: // Z report
                     task_FunctionState   =  STATE_DURING_PRINTOUT;
                     break;

               default:
                     task_FunctionState   =  STATE_END;
                     break;
            }
            break;

      case  STATE_DURING_PRINTOUT:
            if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_END_PRINTING_JOB))
            {
               switch( OpCode)
               {
                  case  3: // Z report
                        task_FunctionState   =  STATE_ASK_IF_WANT_TO_DELETE;
                        break;

                  default:
                        task_FunctionState   =  STATE_END;
                        break;
               }
            }

            if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR))
               task_FunctionState   =  STATE_END;
            break;

      case  STATE_ASK_IF_WANT_ANOTHER_COPY:
            strcpy_P( task_Text_Temp, FunctionLabelMsg[FUNCTION_LABEL_MSG_REQ_TO_PRINT_COPY][App_Language]);
            LcdAPI_WriteString( LINE1, 0, task_Text_Temp, LCD_LINE_WIDTH);
            strcpy_P( task_Text_Temp, FunctionLabelMsg[FUNCTION_LABEL_MSG_YES_NO][App_Language]);
            LcdAPI_WriteString( LINE2, 0, task_Text_Temp, LCD_LINE_WIDTH);
            LcdAPI_SetDisplayBlinking( OFF);

            BuzzAPI_SetMode( FRAGMENTED_3, 2);
            task_FunctionState   =  STATE_GET_ANSWER_ANOTHER_COPY;
            break;

      case  STATE_ASK_IF_WANT_TO_DELETE:
            strcpy_P( task_Text_Temp, FunctionLabelMsg[FUNCTION_LABEL_MSG_DELETE_DATABASE][App_Language]);
            LcdAPI_WriteString( LINE1, 0, task_Text_Temp, LCD_LINE_WIDTH);
            strcpy_P( task_Text_Temp, FunctionLabelMsg[FUNCTION_LABEL_MSG_YES_NO][App_Language]);
            LcdAPI_WriteString( LINE2, 0, task_Text_Temp, LCD_LINE_WIDTH);
            LcdAPI_SetDisplayBlinking( OFF);

            BuzzAPI_SetMode( FRAGMENTED_3, 2);
            task_FunctionState   =  STATE_GET_ANSWER_WANT_TO_DELETE;
            break;

      case  STATE_GET_ANSWER_ANOTHER_COPY:
            if( task_Key == KEY_NO_KEY)
               break;

            if( task_Key == KEY_ENTER) // #
               task_FunctionState   =  STATE_PRINT_REPORT;
            else
            {
               switch( OpCode)
               {
                  case  0: // Sales report
                  case  2: // Events report
                  case  3: // Z report
                        task_FunctionState   =  STATE_ASK_IF_WANT_TO_DELETE;
                        break;

                  default:
                        task_FunctionState   =  STATE_END;
                        break;
               }
            }
            break;

      case  STATE_GET_ANSWER_WANT_TO_DELETE:
            if( task_Key == KEY_NO_KEY)
               break;

            if( task_Key == KEY_ENTER) // #
               task_FunctionState   =  STATE_DELETE_DATA;
            else
               task_FunctionState   =  STATE_END;
            break;

      case  STATE_DELETE_DATA:
            BuzzAPI_SetMode( ON, 1);

            if( OpCode == 0)  // Sales report
               Task_Log_Trans_ClearDB();

            if( OpCode == 2)  // Events report
            {
               BuzzAPI_SetMode( ON, 1);
               Task_Log_Event_ClearDB();
            }

            if( OpCode == 3)  // Z report
               Task_Setup_Func_EraseSalesInfo( 0);

            task_FunctionState   =  STATE_END;
            break;

      case  STATE_END:
            LcdAPI_SetDisplayBlinking( OFF);
            task_FunctionState   =  0; // for KEY_ESC (see Task_Setup_HandleKey_Function)
            task_KeyType         =  KEY_TYPE_ESC; // simulate as if Esc was pressed
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_Func_ShowReport( byte   Void)
/*----------------------------------------------------------------------------*/
{
static   byte     DatumIndex;
static   byte     Idx;
static   byte     MaxIdx;
static   byte     DataType;
static   bool     fRefreshDisplay;
#define  STATE_IDLE              0
#define  STATE_START_ACTION      1
#define  STATE_DURING_ACTION     2

#define  DATA_TYPE_VOID          0
#define  DATA_TYPE_HOPPERS       1


   switch( task_FunctionState)
   {
      case  STATE_IDLE:
            break;

      case  STATE_START_ACTION:
            DatumIndex           =  0;
            Idx                  =  0;
            MaxIdx               =  0;
            DataType             =  DATA_TYPE_VOID;
            fRefreshDisplay      =  TRUE;
            task_FunctionState   ++;
            break;

      case  STATE_DURING_ACTION:
      default:
            if( fRefreshDisplay == TRUE)
            {
            ulong    Value;
            byte     PaymentTypeIdx;
            bool     fValueIsMoney;

               Value             =  0;
               fRefreshDisplay   =  FALSE;
               fValueIsMoney     =  TRUE;

               switch( DatumIndex)
               {
                  case  0: for( PaymentTypeIdx = 0; PaymentTypeIdx < MAX_PAYMENT_TYPES; PaymentTypeIdx ++)
                              Value += task_pSalesInfo->Erasable.SalesValue[PaymentTypeIdx];
                           break;

                  case  1: for( PaymentTypeIdx = 0; PaymentTypeIdx < MAX_PAYMENT_TYPES; PaymentTypeIdx ++)
                              Value += task_pSalesInfo->Erasable.SalesCount[PaymentTypeIdx];
                           fValueIsMoney  =  FALSE;
                           break;

                  case  2: Value =  task_pSalesInfo->Erasable.SalesValue[PAYMENT_TYPE_CASH];
                           break;

                  case  3: Value =  task_pSalesInfo->Erasable.SalesValue[PAYMENT_TYPE_CREDIT_CARD];
                           break;

                  case  4: Value =  task_pHoppersInfo->MoneyInTubes;
                           break;

                  case  5: Value =  task_pCoinsInfo->Erasable.MoneyInCashBox;
                           break;

                  case  6: Value =  task_pBillsInfo->Erasable.MoneyDeposited;
                           break;

                  case  7: Value =  task_pBillsInfo->Erasable.BillsInStacker;
                           fValueIsMoney  =  FALSE;
                           break;

                  case  8: Value =  task_pHoppersInfo->MoneyInTube[Idx];
                           MaxIdx   =  MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/;
                           DataType =  DATA_TYPE_HOPPERS;
                           break;

                  case  9: Value =  task_pHoppersInfo->CoinsInTube[Idx];
                           MaxIdx   =  MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/;
                           DataType =  DATA_TYPE_HOPPERS;
                           fValueIsMoney  =  FALSE;
                           break;

                  default: DatumIndex        =  0;
                           Idx               =  0;
                           DataType          =  DATA_TYPE_VOID;
                           fRefreshDisplay   =  TRUE;
                           return;  // don't replace this with 'break' !!!
               }

               strcpy_P( task_Text_Temp, ReportShowMsg[DatumIndex][App_Language]);
               LcdAPI_WriteString( LINE1, 0, task_Text_Temp, LCD_LINE_WIDTH);
               if( fValueIsMoney == TRUE)
                  sprintf_P( task_Text_Temp, Setup_Strings_long[STRING_LONG_lu2lu], (Value / 100), (Value % 100));
               else
                  sprintf_P( task_Text_Temp, Setup_Strings_short[STRING_SHORT_lu], Value);
               LcdAPI_WriteString( LINE2, 0, task_Text_Temp, LCD_LINE_WIDTH);

               if( DataType != DATA_TYPE_VOID)
                  //LcdAPI_WriteNumber( LINE1, (App_Language == LANGUAGE_HEBREW) ? 0 : (LCD_LINE_WIDTH - 1), (Idx + 1), 1);
                  LcdAPI_WriteChar( LINE1, (App_Language == LANGUAGE_HEBREW) ? 0 : (LCD_LINE_WIDTH - 1), (Idx + 'A'));
            }

            if( task_KeyType == KEY_TYPE_ENTER)
            {
               fRefreshDisplay   =  TRUE;

               if( (DataType != DATA_TYPE_VOID) && ((Idx + 1) < MaxIdx))
               {
                  Idx   ++;
                  break;
               }

               Idx         =  0;
               DataType    =  DATA_TYPE_VOID;
               DatumIndex  ++;
            }

            if( task_Key == KEY_F1)   // 'A'
            {
               fRefreshDisplay   =  TRUE;

               if( (DataType != DATA_TYPE_VOID) && (Idx > 0))
               {
                  Idx   --;
                  break;
               }

               if( DatumIndex > 0)
               {
                  DatumIndex  --;
                  DataType    =  DATA_TYPE_VOID;
                  Idx   =  MaxIdx;
                  if( Idx > 0)
                     Idx   --;
               }
            }

            if( task_KeyType == KEY_TYPE_ESC)
            {
               SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_STOP_TESTING);
               task_fRefreshScreen  =  TRUE;
               task_FunctionState   =  0;
            }
            break;
   }
#undef   STATE_IDLE
#undef   STATE_GET_DATA
#undef   STATE_DURING_ACTION
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_Func_EraseSalesInfo( byte   Void)
/*----------------------------------------------------------------------------*/
{
   BuzzAPI_SetMode( ON, 1);   // don't delete

   Task_Log_Event_ClearDB();
   Task_Log_Trans_ClearDB();
   SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_CLEAR_SALES);
   SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_ADVANCE_Z_NUM);

   task_FunctionState   =  0; // for KEY_ESC (see Task_Setup_HandleKey_Function)
   task_KeyType         =  KEY_TYPE_ESC; // simulate as if Esc was pressed
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_Func_SwVer( byte   Void)
/*----------------------------------------------------------------------------*/
{
   LcdAPI_WriteStringConst( LINE1, MSG_APP_VER);
   task_FunctionState   =  0; // for KEY_ESC (see Task_Setup_HandleKey_Function)
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Setup_Func_SetDeafults( byte   Void)
/*----------------------------------------------------------------------------*/
{
   Task_Setup_Func_EraseSalesInfo( 0);
   SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_SET_DEFAULT_DEBUG);
   task_FunctionState   =  0; // for KEY_ESC (see Task_Setup_HandleKey_Function)
   task_KeyType         =  KEY_TYPE_ESC; // simulate as if Esc was pressed
}



/*----------------------------------------------------------------------------*/
static	void						Task_Setup_Func_EmptyHopper( byte   Void)
/*----------------------------------------------------------------------------*/
{
ulong          Value;
ulong          CoinsDispensed;
tHopperStatus  Status;


   switch( task_FunctionState)
   {
      case  0:
            break;

      case  1:
            Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, PARAM_A_HOPPER_TIMEOUT_EMPTY);
            HopperAPI_SetTimeoutValue( task_Rec_Type_A);

            Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, PARAM_A_HOPPER_TIMEOUT_DISPENSE);
            HopperAPI_SetTimeoutValue_Dispensing( task_Rec_Type_A);

            Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_QTTY_TO_EMPTY + task_EntryIdx));
            if( task_Rec_Type_A == 0)
            {
               LcdAPI_WriteStringConst( LINE1, MSG_SETUP_ERR_1);
               LcdAPI_WriteStringConst( LINE2, MSG_SET_QTTY_EMPTYING);
               BuzzAPI_SetMode( ON, 2);
               task_TimerSec_2   =  Task_Timer_SetTimeSec( 4);
               task_FunctionState   =  3;
               break;
            }

            LcdAPI_WriteNumber( LINE2, 1, task_Rec_Type_A, 3);
            Task_Hopper_Dispense_Count( (tHopper)task_EntryIdx, task_Rec_Type_A, FALSE);
            task_FunctionState   ++;
            break;

      case  2:
            CoinsDispensed =  Task_Hopper_GetCoinsDispensed( (tHopper)task_EntryIdx);
            Value =  (CoinsDispensed * (ulong)Task_Hopper_GetCoinValue( (tHopper)task_EntryIdx));

            LcdAPI_WriteNumber( LINE2, 5, CoinsDispensed, 3);
            LcdAPI_WritePrice( LINE2, 9, (void*)&Value, sizeof(ulong), JUSTIFY_RIGHT);

            Status   =  Task_Hopper_GetStatus();
            if( Status != HOPPER_STATUS_BUSY)
            {
               Task_Params_Write( &task_Rec_Type_A, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_QTTY_TO_EMPTY + task_EntryIdx));
               task_Rec_Type_A   =  CoinsDispensed;
               Task_Params_Write( &task_Rec_Type_A, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_EMPTYING + task_EntryIdx));
               SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_COINS_EMPTYING);

               BuzzAPI_SetMode( (CoinsDispensed == task_Rec_Type_A) ? FRAGMENTED_3 : FRAGMENTED_2, 2);
               task_TimerSec_2   =  Task_Timer_SetTimeSec( 30);
               task_FunctionState   ++;
            }
            break;

      case  3:
            if( (task_Key != KEY_NO_KEY) || (task_TimerSec_2 == 0))
               task_FunctionState   ++;
            break;

      case  4:
            task_FunctionState   =  0; // for KEY_ESC (see Task_Setup_HandleKey_Function)
            task_KeyType         =  KEY_TYPE_ESC; // simulate as if Esc was pressed
            task_Level           --;
            task_Selection[task_Level] =  0;
            break;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Setup_Func_LogFiles( byte Void)
/*----------------------------------------------------------------------------*/
{
tLogEvent_Info    *task_pLog_Event_Info;


   task_pLog_Event_Info =  Task_Log_Event_GetInfo();

   switch( task_FunctionState)
   {
      case  0:
            break;

      case  1:
            task_TimerMsec =  Task_Timer_SetTimeMsec( 500);
            task_FunctionState   ++;
            break;

      case  2:
            LcdAPI_ClearLine( LINE2);
            sprintf( task_Text_Temp, "W%u", task_pLog_Event_Info->Idx_W);
            LcdAPI_WriteString( LINE2, 0, task_Text_Temp, strlen( task_Text_Temp));
            sprintf( task_Text_Temp, "R%u", task_pLog_Event_Info->Idx_R);
            LcdAPI_WriteString( LINE2, (LCD_LINE_WIDTH / 2), task_Text_Temp, strlen( task_Text_Temp));

            /*
            LcdAPI_ClearLine( LINE3);
            sprintf( task_Text_Temp, "Rec %u", task_pLog_Event_Info->RecordsInDB);
            LcdAPI_WriteString( LINE3, 0, task_Text_Temp, strlen( task_Text_Temp));

            LcdAPI_ClearLine( LINE4);
            sprintf( task_Text_Temp, "Evts %u", task_pLog_Event_Info->NumberOfEvents);
            LcdAPI_WriteString( LINE4, 0, task_Text_Temp, strlen( task_Text_Temp));
            sprintf( task_Text_Temp, "Ev# %u", task_pLog_Event_Info->EvNumerator);
            LcdAPI_WriteString( LINE4, (LCD_LINE_WIDTH / 2), task_Text_Temp, strlen( task_Text_Temp));
            */

            task_FunctionState   ++;
            break;

      case  3:
            if( task_TimerMsec == 0)
               task_FunctionState   =  1;

            if( task_KeyType == KEY_TYPE_ESC)
            {
               task_fRefreshScreen  =  TRUE;
               task_FunctionState   =  0;
            }
            break;

      default:
            task_fRefreshScreen  =  TRUE;
            task_FunctionState   =  0;
            break;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Setup_Func_TestRelay( byte Void)
/*----------------------------------------------------------------------------*/
{
   switch( task_FunctionState)
   {
      default:
            task_fRefreshScreen  =  TRUE;
            task_FunctionState   =  0;
            task_KeyType         =  KEY_TYPE_ESC;
            break;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Setup_Func_TestPrinter( byte Void)
/*----------------------------------------------------------------------------*/
{
   switch( task_FunctionState)
   {
      default:
            SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_TEST_PRINTER);
            task_fRefreshScreen  =  TRUE;
            task_FunctionState   =  0;
            task_KeyType         =  KEY_TYPE_ESC;
            break;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Setup_Func_TestCoinRouting( byte Void)
/*----------------------------------------------------------------------------*/
{
#define  MAX_ENTRIES       (MAX_TUBES + 1)   // 4 Hoppers + 1 cashbox
static   tB2B_FromSlave    *task_pRxData;
static   byte              Idx;
static   struct
         {
            ulong    TotalValue;
            usint    Value;
            usint    Count;
         }  Coins[MAX_ENTRIES];


   if( task_Key == KEY_ESC)   // '*'
      task_FunctionState   =  99;   // go to 'default' case

   switch( task_FunctionState)
   {
      case  0:
            break;

      case  1:
            task_pRxData   =  Task_B2B_GetRxDataPtr();
            Idx            =  0;
            memset( Coins, 0, sizeof( Coins));
            task_FunctionState   ++;
            break;

      case  2:
            SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_TEST_COIN_ROUTING);
            task_FunctionState   ++;
            break;

      case  3:
            if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_COIN_DETECTED))
            {
               CLEAR_BIT( App_TaskEvent[TASK_APPL], EV_APP_APPL_COIN_DETECTED);
               Idx   =  task_pRxData->Coin_Valid_Value.RoutedTo;

               if( Idx < MAX_ENTRIES)
               {
                  BuzzAPI_Beep();
                  Coins[Idx].TotalValue   += task_pRxData->Coin_Valid_Value.Value;
                  Coins[Idx].Value        =  task_pRxData->Coin_Valid_Value.Value;
                  Coins[Idx].Count        ++;
                  task_FunctionState      ++;
               }
               else
                  BuzzAPI_SetMode( ON, 3);
            }

            switch( task_Key)
            {
               case  KEY_F1:  // 'A'   Up   (prev. hopper)
                     if( Idx > 0)
                     {
                        Idx   --;
                        task_FunctionState   ++;
                     }
                     break;

               case  KEY_F2:  // 'B'   Down (next hopper)
                     if( (Idx + 1) < MAX_ENTRIES)
                     {
                        Idx   ++;
                        task_FunctionState   ++;
                     }
                     break;
            }
            break;

      case  4:
            LcdAPI_ClearScreen();
            LcdAPI_WritePrice ( LINE1, 0, (void*)&Coins[Idx].Value, sizeof(usint), JUSTIFY_LEFT);
            LcdAPI_WriteChar  ( LINE1,10, '(');
            //LcdAPI_WriteNumber( LINE1,11, Idx, 1);
            LcdAPI_WriteChar  ( LINE1,11, (Idx == ROUTE_SAFE_BOX) ? 'S' : ((Idx - ROUTE_HOPPER_A) + 'A'));
            LcdAPI_WriteChar  ( LINE1,12, ')');
            LcdAPI_WritePrice ( LINE2, 0, (void*)&Coins[Idx].TotalValue, sizeof(ulong), JUSTIFY_LEFT);
            LcdAPI_WriteNumber( LINE2,10, Coins[Idx].Count, 3);
            task_FunctionState   =  2;
            break;

      default:
            SET_BIT( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_STOP_TESTING);
            task_fRefreshScreen  =  TRUE;
            task_FunctionState   =  0;
            task_KeyType         =  KEY_TYPE_ESC;
            break;
   }
#undef   MAX_ENTRIES
}









