/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tVGA_ObjectRec       task_ObjectRec;
static   char                 task_Str[15];
static   bool                 task_fRun;
static   bool                 task_fHold  =  FALSE;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						Task_Display_HandleTimer( void);
/*----------------------------------------------------------------------------*/
#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Display_Init( void)
/*----------------------------------------------------------------------------*/
{
   task_fRun   =  FALSE;
   task_fHold  =  FALSE;
   DisplayAPI_Init( EXAR_B);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_Main( void)
/*----------------------------------------------------------------------------*/
{
	if( App_TaskStatus[TASK_DISPLAY] == DISABLED)
		return;

   if( task_fRun == TRUE)
   {
      Task_Display_Run();
   }

   DisplayAPI_Main();

   Task_Display_HandleTimer();
}



/*----------------------------------------------------------------------------*/
bool                    		Task_Display_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
   return( DisplayAPI_IsBusy());
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_ClearScreen( void)
/*----------------------------------------------------------------------------*/
{
   Task_Display_WriteChar( 0, 0, SPACE);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_AddOn_Object( bool   fMode)
/*----------------------------------------------------------------------------*/
{
   DisplayAPI_AddOn_Object( fMode);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_SetFont( tVGA_FontName Name, byte  Style, tVGA_FontSize Size)
/*----------------------------------------------------------------------------*/
{
   task_ObjectRec.Object      =  VGA_OBJECT_FONT;
   task_ObjectRec.Font.Name   =  Name;
   task_ObjectRec.Font.Style  =  Style;
   task_ObjectRec.Font.Size   =  Size;

   if( task_fHold == FALSE)
      DisplayAPI_SetObject    ( &task_ObjectRec);

   task_fRun   =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_SetColor( tVGA_Color   Color)
/*----------------------------------------------------------------------------*/
{
   task_ObjectRec.Object      =  VGA_OBJECT_COLOR;
   task_ObjectRec.Color_FG    =  Color;

   if( task_fHold == FALSE)
      DisplayAPI_SetObject( &task_ObjectRec);

   task_fRun   =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_SetAlignment( tVGA_Alignment Mode)
/*----------------------------------------------------------------------------*/
{
   task_ObjectRec.Alignment   =  Mode;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_SetRectange( usint  X, usint Y, usint Height,  usint Width)
/*----------------------------------------------------------------------------*/
{
   task_ObjectRec.Object      =  VGA_OBJECT_RECTANGLE;
   task_ObjectRec.X           =  X;
   task_ObjectRec.Y           =  Y;
   task_ObjectRec.Height      =  Height;
   task_ObjectRec.Width       =  Width;

   if( task_fHold == FALSE)
      DisplayAPI_SetObject( &task_ObjectRec);

   task_fRun   =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_WriteChar     ( byte   Line, byte	Offset, char	Char)
/*----------------------------------------------------------------------------*/
{
   task_Str[0] =  Char;
   task_Str[1] =  0;
   Task_Display_WriteString( Line, Offset, task_Str);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_WriteString   ( byte   Line, byte	Offset, char	*pStr)
/*----------------------------------------------------------------------------*/
{
   if( pStr == NULL)
      return;

   task_ObjectRec.Object      =  VGA_OBJECT_TEXT;
   task_ObjectRec.Line        =  Line;
   task_ObjectRec.Offset      =  Offset;
   task_ObjectRec.pText       =  pStr;

   if( task_fHold == FALSE)
      DisplayAPI_SetObject( &task_ObjectRec);

   task_fRun   =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_WriteNumber   ( byte   Line, byte	Offset, ulong  Number, byte	Digits)
/*----------------------------------------------------------------------------*/
{
ulong    Div;
byte     i, j, Idx;


   task_Str[0] =  0;

	for( i = Digits, Idx = 0; i > 0; i --, Idx ++)
	{
      if( Idx >= (sizeof( task_Str) - 1))
         break;

		for( j = 1, Div = 1; j < i; j ++, Div *= 10);
      task_Str[Idx]     =  ((Number / Div) % 10) + '0';
      task_Str[Idx + 1] =  0;
	}

   Task_Display_WriteString( Line, Offset, task_Str);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_WriteNumberHex( byte   Line, byte	Offset, ulong  Number, byte	Digits)
/*----------------------------------------------------------------------------*/
{
   sprintf( task_Str, "%.*lX", Digits, Number);
   Task_Display_WriteString( Line, Offset, task_Str);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_WritePrice    ( byte   Line, byte	Offset, ulong  Number)
/*----------------------------------------------------------------------------*/
{
   sprintf( task_Str, "%lu.%.2lu", (Number / 100), (Number % 100));
   Task_Display_WriteString( Line, Offset, task_Str);
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_Run( void)
/*----------------------------------------------------------------------------*/
{
   task_fRun   =  FALSE;

   if( task_fHold == FALSE)
      DisplayAPI_Run();
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_Hold( void)
/*----------------------------------------------------------------------------*/
{
   task_fHold  =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Display_Release( void)
/*----------------------------------------------------------------------------*/
{
   task_fHold  =  FALSE;
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Display_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec      OldTimer	=	0;
tTimerMsec              NewTimer	=	0;
//byte                    Gap;


   NewTimer = Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
//      Gap      = (NewTimer - OldTimer);       // ???
      OldTimer = NewTimer;
   }
}

