/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
//#include	"Events.def"
/*------------------------ Precompilation Definitions ------------------------*/
#define     MEM_PAGE_SIZE           0x8000
#define     MAX_SIZE_BUF_PARAMS     32 // 8 X long
#define     TIME_VOID_RX_COMMANDS   15 // seconds
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   TYPE_CHAR   ,
   TYPE_BYTE   ,
   TYPE_USINT  ,
   TYPE_ULONG  ,
   TYPE_STRING ,
   TYPE_TIME_DATE,
}  tType;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/

//--------------------------------------------------------------------
//*********************************************************
// All static variables are now, in a typedef struct 
// Physically it is located in an ExtRAM. 
// The dedicated xRAM block size is 1024 bytes (0x400)
// Pointer THIS is used to accress all variables.
// 5/7/2016: Currently, 483 of 1024 bytes, are in use. 
// See function Task_TibaPay_Init() marked code part.
//*********************************************************
typedef struct
{
   TP_Cmd_Q_Data        task_StatusStruct;
   char                 task_TxBuffer[TIBAPAY_MAX_LEN_BUFFER];
   byte                 task_Buf_Params[MAX_SIZE_BUF_PARAMS];
   tServerPacket        *task_pServerPacket_Rx;
   tServer_MasterRec    task_MasterRec;
   tParams_C_Rec        task_Param_C_Rec;
   ulong                task_Param_Var;
   tServerPacket        task_TibaPayPacket_Tx;     //task_ServerPacket_Tx;
   tServer_SlaveRec     task_SlaveRec;
   tLogEmergency_Info  *task_pEmergency_Info;
   tLogTrans_Info      *task_pTrans_Info;
   tLogEvent_Info      *task_pEvent_Info;
   tTimerSec            task_Timer_ReadParams;
   tTimerSec            task_Timer_MachineID;
   usint                task_TxBufferIdx;
   usint                task_MachineID;
   usint                task_MachineID_Server;
   bool                 task_fPowerup;
   bool                 task_fRequest_Z_Command;
   tServerReplyCmd      task_TxCommand;
   byte                 task_Cmd_E_Type;
   bool                 task_fReadParams;
   bool                 task_RFID_fConnected;
   bool                 task_fRequestForEmergency;
   bool                 task_fParkParam;
   tTimerSec            task_Timer_SendStatus;
   tTimerSec            task_Timer_RxActive;
   byte                 task_Time_SendStatus;
   bool                 task_fSendStatus;
   // Parameters for replying to the Server
   byte                 task_Page;
   usint                task_AddressInPage;
   ulong                task_Address;
   bool                 task_fCommLoss;                 // task_fCommLoss > 0 means: communication loss with Tib@Pay server..
   bool                 task_fTibaPayCommLoss_Reported;
} tdTP_XData;

tdTP_XData *THIS = (tdTP_XData *)NVRAM_ADDR_T_TIBAPAY_VARS;
//--------------------------------------------------------------------

/*------------------------ Local Function Prototypes -------------------------*/
static	void			Task_TibaPay_HandleTimer( void);
static  void                    Task_TibaPay_ReadDB( void);
static  void                    Task_TibaPay_ReadParams( void);
static	void			Task_TibaPay_ReadStatusVars( void);
static	void			Task_TibaPay_HandleExtEvent( void);
static	void			Task_TibaPay_HandleComm( void);
static	void			Task_TibaPay_HandleComm_Rx( void);
static	void			Task_TibaPay_HandleComm_Tx( void);
static	void			Task_TibaPay_AddObject( tType  Type, void  *pData, byte   Len);
static	void			Task_TibaPay_PaddingObject( tType  Type, void  *pData, byte   Len);
static	void			Task_TibaPay_EraseAllData( void);
static  void                    Task_TibaPay_CleanTransactionInformation(void);
void                            Task_TibaPay_CommLoss_TO(void);
/*----------------------------------------------------------------------------*/
#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif
void Util_SuperSwapArray(void * pArray, byte Length);


/*----------------------------------------------------------------------------*/
void                    		Task_TibaPay_Init( void)
/*----------------------------------------------------------------------------*/
{
   memset( THIS, 0, sizeof( tdTP_XData));
  
   THIS->task_fPowerup                 =  TRUE;
   THIS->task_fReadParams              =  TRUE;
   THIS->task_TxCommand                =  SERVER_REPLY_VOID;
   THIS->task_fRequest_Z_Command       =  TRUE;
   THIS->task_fCommLoss                =  FALSE;
   TibaPayAPI_Init( COM_7);

   THIS->task_pEmergency_Info =  Task_Log_Emergency_GetInfo();
   THIS->task_pTrans_Info     =  Task_Log_Trans_GetInfo();
   
   // Open W.D timer for TibaPay packets watch
   Timers_SetTimer( 5, TIME_UNIT_1Sec, Task_TibaPay_CommLoss_TO );
   THIS->task_fTibaPayCommLoss_Reported = FALSE;
}



/*----------------------------------------------------------------------------*/
void                    		Task_TibaPay_Main( void)
/*----------------------------------------------------------------------------*/
{

   if( App_TaskStatus[TASK_TIBAPAY] == DISABLED)
      return;

   Task_TibaPay_HandleTimer();

   if( THIS->task_Timer_MachineID == 0)
   {
      THIS->task_Timer_MachineID =  Task_Timer_SetTimeSec( 60);
      ClockAPI_NVRAM_Read( (byte *)&THIS->task_MachineID, sizeof( THIS->task_MachineID), RTC_NVRAM_TYPE_MACHINE_ID);
   }

   if( THIS->task_fPowerup == TRUE)
   {
      Task_TibaPay_ReadDB();
      Task_TibaPay_ReadParams();
      Task_TibaPay_ReadStatusVars();
   }
   //----------
   
   TibaPayAPI_Main();

   Task_TibaPay_HandleExtEvent();

   Task_TibaPay_HandleComm();   // 26mS

   THIS->task_fPowerup  =  FALSE;
}



/*----------------------------------------------------------------------------*/
TP_Cmd_Q_Data                              *Task_TibaPay_GetStatusStructPtr( void)
/*----------------------------------------------------------------------------*/
{
   return( &THIS->task_StatusStruct);
}



/*----------------------------------------------------------------------------*/
void					Task_TibaPay_SaveStatusVars( void)
/*----------------------------------------------------------------------------*/
{
   //Task_Params_Write_Var( task_StatusStruct.Qtty_Coins_Hopper, (VAR_STATUS_END - VAR_STATUS_START), VAR_STATUS_QTTY_COINS_HOPPER_A);
   Task_Params_Write_Var( THIS->task_StatusStruct.Qtty_Coins_Hopper, (VAR_STATUS_END - VAR_STATUS_START), VAR_STATUS_QTTY_COINS_HOPPER_A);
}



/*----------------------------------------------------------------------------*/
void                                    Task_TibaPay_SendStatus( void)
/*----------------------------------------------------------------------------*/
{
   THIS->task_fSendStatus  =  TRUE;
}



/*----------------------------------------------------------------------------*/
usint                                   Task_TibaPay_GetMachineID_Server( void)
/*----------------------------------------------------------------------------*/
{
   return( THIS->task_MachineID_Server);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( THIS->task_Timer_ReadParams     <  Gap)  THIS->task_Timer_ReadParams      =  0; else  THIS->task_Timer_ReadParams      -= Gap;
      if( THIS->task_Timer_MachineID      <  Gap)  THIS->task_Timer_MachineID       =  0; else  THIS->task_Timer_MachineID       -= Gap;
      if( THIS->task_Timer_SendStatus     <  Gap)  THIS->task_Timer_SendStatus      =  0; else  THIS->task_Timer_SendStatus      -= Gap;
      if( THIS->task_Timer_RxActive       <  Gap)  THIS->task_Timer_RxActive        =  0; else  THIS->task_Timer_RxActive        -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_TibaPay_ReadDB( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
static   void                 Task_TibaPay_ReadParams( void)
/*----------------------------------------------------------------------------*/
{
}



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_ReadStatusVars( void)
/*----------------------------------------------------------------------------*/
{
   Task_Params_Read_Var( THIS->task_StatusStruct.Qtty_Coins_Hopper, (VAR_STATUS_END - VAR_STATUS_START), VAR_STATUS_QTTY_COINS_HOPPER_A);
}



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_HandleExtEvent( void)
/*----------------------------------------------------------------------------*/
{
   if( App_TaskEvent[TASK_TIBAPAY] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_READ_PARAMS))
         Task_TibaPay_ReadParams();

      if( BIT_IS_SET( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_L_POINTERS))
         THIS->task_fRequest_Z_Command =  TRUE;
   }
   
   App_TaskEvent[TASK_TIBAPAY] =  EV_APP_VOID;

   if( (THIS->task_fReadParams == TRUE) && (THIS->task_Timer_ReadParams == 0))
   {
      THIS->task_fReadParams  =  FALSE;
      SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_READ_PARAMS);

      Task_Params_Read( &THIS->task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_TIME_SEND_STATUS);
      THIS->task_Time_SendStatus =  THIS->task_Param_C_Rec;
   }

   if( App_TaskEvent[TASK_APPL] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_RFID_CONNECTED))     THIS->task_RFID_fConnected =  TRUE;
      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_RFID_DISCONNECTED))  THIS->task_RFID_fConnected =  FALSE;

      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_EMERGENCY_CREDIT))
      {
         if (THIS->task_fCommLoss == TRUE)
         {
            Task_Application_DisplayCommLoss();         // 18/7/2016 In case of communication loss with Tib@Pay server - Display a message
         }
         else
         {
            THIS->task_fRequestForEmergency =  TRUE;
            THIS->task_Cmd_E_Type   =  EMERGENCY_TYPE_CREDIT_CARD;
         }
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_NEW_MACHINE_ID))
         THIS->task_Timer_MachineID =  0;

      if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_REQ_TO_ERASE_ALL_DATA))
         Task_TibaPay_EraseAllData();
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_HandleComm( void)
/*----------------------------------------------------------------------------*/
{
   
   if( TibaPayAPI_IsBusy() == TRUE)
      return;
    
   /* Receive */
   if( THIS->task_Timer_RxActive == 0) // if lost connection with the server, enforce the server (once resumed) to send back 'Z' command
   {
      THIS->task_fCommLoss = TRUE;

      Task_TibaPay_CleanTransactionInformation();       // Communication Loss. Clear TRACK2 information.
      THIS->task_fRequest_Z_Command    =  TRUE;
   }

   if( TibaPayAPI_IsRxNewPacket() == TRUE)
   {
      Task_TibaPay_HandleComm_Rx();             // 300uS
   }

   /* Transmit */
   if( THIS->task_TxCommand != SERVER_REPLY_VOID)
   {

      Task_TibaPay_HandleComm_Tx();             // 25mS !

      THIS->task_TibaPayPacket_Tx.pBuffer  =  (byte *)THIS->task_TxBuffer;
      THIS->task_TibaPayPacket_Tx.Command  =  THIS->task_TxCommand;
      THIS->task_TxCommand                =  SERVER_REPLY_VOID;

      TibaPayAPI_SendData( &THIS->task_TibaPayPacket_Tx);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_HandleComm_Rx( void)
/*----------------------------------------------------------------------------*/
{
static   tLogEvent_Data       task_Event_Data_2server;
usint    Idx;
byte     State;
byte     bData;
usint    uData;
ulong    lData;

   THIS->task_TxCommand =  SERVER_REPLY_ACK;
   Idx   =  0;
   State =  0;

   THIS->task_pServerPacket_Rx   =  TibaPayAPI_GetData();

   if( THIS->task_pServerPacket_Rx == NULL)
      return;

   // check if message came from ParkParam (first message after at least 5 seconds of silence)
   if( THIS->task_Timer_RxActive == 0)
   {
      THIS->task_fParkParam   =  FALSE;

      switch( THIS->task_pServerPacket_Rx->Command)
      {
         // only these commands are used by ParkParam
         case  TP_SERVER_CMD_PARAMS_READ  :  // 'R'
         case  TP_SERVER_CMD_PARAMS_WRITE :  // 'W'
               THIS->task_fParkParam   =  TRUE;
               break;
      }
   }

   SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_ACTIVE);
   THIS->task_Timer_RxActive  =  Task_Timer_SetTimeSec( 5);

   
   switch( THIS->task_pServerPacket_Rx->Command)
   {     
      case  TP_SERVER_CMD_ACK             :  // '.'

            break;
#if 0   //Not Supported
      case  TP_SERVER_CMD_NACK            :  // '*'     //not used
            break;
#endif
      case  TP_SERVER_CMD_GET_STATUS      :  // 'L'
            
         THIS->task_fCommLoss = FALSE;       // No more "Communication Loss" situation.
         Timers_SetTimer( 5, TIME_UNIT_1Sec, Task_TibaPay_CommLoss_TO );        // ReInitiate W.D timer
         if (THIS->task_fTibaPayCommLoss_Reported == TRUE)
         {
            // Report about TibaPay Comm Loss, was sent to AFCON Server. Send a TibaPay Comm Back
            task_Event_Data_2server.Type_2.Field1 = 0x01234567;
            task_Event_Data_2server.Type_2.Field2 = 0x89ABCDEF;
            Task_Application_SetEvent( VMC_EVENT_TIBA_PAY_COMM_BACK, 0, &task_Event_Data_2server);
            THIS->task_fTibaPayCommLoss_Reported = FALSE;         // Remember: Report on LOSS was sent
   
         }
            
            memcpy( &uData, (THIS->task_pServerPacket_Rx->pBuffer + Idx), sizeof( uData));   Idx +=   sizeof( uData);
            Util_SwapArray( &uData, sizeof( uData));
            THIS->task_MasterRec.Cmd_L.Idx_R_Trans =  uData;

            // Compare Machine ID
            CLEAR_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_MISMATCH_MACHINE_ID);
            THIS->task_MachineID_Server   =  0;
            Idx   =  THIS->task_pServerPacket_Rx->Idx_CtrlChar_Underscore;
            if( THIS->task_pServerPacket_Rx->pBuffer[Idx] == SERVER_DELIMITER)
            {
               Idx ++;
               memcpy( &uData, (THIS->task_pServerPacket_Rx->pBuffer + Idx), sizeof( uData));   Idx +=   sizeof( uData);
               Util_SwapArray( &uData, sizeof( uData));
               if( uData != 0)
               {
                  THIS->task_MachineID_Server   =  uData;
                  if( uData != THIS->task_MachineID)
                  {
                     THIS->task_Timer_MachineID =  0; // enforce reading again Machine ID
                     SET_BIT  ( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_MISMATCH_MACHINE_ID);
                  }
               }
            }
            THIS->task_TxCommand =  SERVER_REPLY_TANSACTION;
            break;
#if 0   //Not Supported
      case  TP_SERVER_CMD_READ_TRANS      :  // 'l'
            break;
#endif
      case  TP_SERVER_CMD_INC_EMERGENCY   :  // 'E'

            Task_TibaPay_CleanTransactionInformation();
            break;
#if 0   //Not Supported
      case  TP_SERVER_CMD_FORMAT          :  // 'F'
            memcpy( &bData, (THIS->task_pServerPacket_Rx->pBuffer + Idx), sizeof( bData));   Idx +=   sizeof( bData);
            switch( bData)
            {
               case  1:
                     SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_L_POINTERS);
                     break;

               case  2:
                     SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_V_POINTERS);
                     break;

               case  3:
                     SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_Z_POINTERS);
                     break;

               case  4:
                     SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_DEL_CRNT_Z_RECORD);
                     break;

               case  5:
                     SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_VITAL_PARAMS);
                     break;

               case  6:
                     SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_DEBT);
                     break;
                     
               case  0xFF:
                     Task_TibaPay_EraseAllData();
                     break;
            }
            break;
#endif
      case  TP_SERVER_CMD_DO_ACTION       :  // 'D'

            Task_TibaPay_CleanTransactionInformation();
            
            memcpy( &THIS->task_MasterRec.Cmd_D, THIS->task_pServerPacket_Rx->pBuffer, sizeof( THIS->task_MasterRec.Cmd_D));
            Task_Application_SetDoAction( THIS->task_MasterRec.Cmd_D.SubCmd, &THIS->task_MasterRec.Cmd_D.Data);         // See typedef struct Cmd_D_Data in T_Server.h
                                                                                                                        // See Task_Application_MGC_CreditCard_2()
            /*printf( "%x %x %lu %u %d", THIS->task_MasterRec.Cmd_D.Data.CreditCard.EquipmentID, 
                  THIS->task_MasterRec.Cmd_D.Data.CreditCard.ResultCode,
                  THIS->task_MasterRec.Cmd_D.Data.CreditCard.Value, 
                  THIS->task_MasterRec.Cmd_D.Data.CreditCard.Last4Digits,
                  THIS->task_MasterRec.Cmd_D.Data.CreditCard.CardIssuer                  
                   );*/
               /* Set Event */
            task_Event_Data_2server.Type_2.Field1 =  THIS->task_MasterRec.Cmd_D.Data.CreditCard.Value;
            task_Event_Data_2server.Type_2.Field2 =  (long)THIS->task_MasterRec.Cmd_D.Data.CreditCard.ResultCode;
            Task_Application_SetEvent( VMC_EVENT_TIBA_PAY_TRANSACTION_END, 0, &task_Event_Data_2server);
            break;

   case  TP_SERVER_CMD_ID              :  // 'Z'

            while( Idx < THIS->task_pServerPacket_Rx->BufSize)
            {
               switch( State)
               {
                  case  0: // byte
                        if( THIS->task_pServerPacket_Rx->pBuffer[Idx] == '_')
                           State ++;
                        break;

                  case  1: // byte
                        if( THIS->task_pServerPacket_Rx->pBuffer[Idx] == '_')
                           State ++;
                        break;

                  case  2: // int
                        memcpy( &uData, (THIS->task_pServerPacket_Rx->pBuffer + Idx), sizeof( uData));   Idx +=   sizeof( uData);
                        Util_SwapArray( &uData, sizeof( uData));
                        App_Version_Server   =  uData;
                        Idx   ++;

                        THIS->task_fRequest_Z_Command =  FALSE;
                        BuzzAPI_SetMode( FRAGMENTED_3, 1);

                        SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RX_INIT_CMD);
                        THIS->task_TxCommand =  SERVER_REPLY_ID;
                        State ++;
                        break;
               }

               Idx   ++;
            }
            break;
   }

   //************  The below part is relevant to MIKVE protocol !!! ************
   
   // Till receiving 'Z' command (init) - void all other commands from the server and request the server to send back 'Z' command
   // 'Z' command is important to know the server's version number
   //if( (THIS->task_fRequest_Z_Command == TRUE) && (THIS->task_fParkParam == FALSE)) // ParkParam doesn't support 'Z' and 'P' commands                                       
   //{
      //BuzzAPI_ShortBeep();
      //THIS->task_TxCommand =  SERVER_REPLY_ENFORCE_Z_CMD; // 'P'
   //}
}



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_HandleComm_Tx( void)
/*----------------------------------------------------------------------------*/
{
static   byte     fGT_CTL_Active =  0;
ulong    lData;
usint    uData;
byte     bData;
byte     Idx;


   THIS->task_TxBuffer[0]              =  0;
   THIS->task_TxBufferIdx              =  0;
   THIS->task_TibaPayPacket_Tx.BufSize  =  0;

   switch( THIS->task_TxCommand)
   {
      case  SERVER_REPLY_ACK           :  // '.'
            
            break;

      case  SERVER_REPLY_NACK          :  // '*'
            
            break;

      case  SERVER_REPLY_EMERGENCY     :  // 'E'
            
            break;

      case  SERVER_REPLY_TANSACTION    :  // 'L'
      //case  SERVER_REPLY_EVENT         :  // 'V'
            
         {
            byte     *pBuf;
            byte     Idx;
            byte     Size;
               
            THIS->task_TxCommand =  SERVER_REPLY_ACK;

            //====================================== Send 'E' Response ======================================
            // Try first to check if there is a request to send an Emergencies packet (E)
            if( THIS->task_fRequestForEmergency == TRUE)
            {
               THIS->task_fRequestForEmergency  =  FALSE;

               if( THIS->task_Cmd_E_Type == EMERGENCY_TYPE_VOID)
                  break;
            
               THIS->task_SlaveRec.Cmd_E.Idx_W  =  0;
               THIS->task_SlaveRec.Cmd_E.Idx_R      = THIS->task_pEmergency_Info->Idx_W;
               THIS->task_SlaveRec.Cmd_E.Type = THIS->task_Cmd_E_Type;

               Task_Log_Emergency_Step_Fwd_Server();

               bData =  '_';                                 Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));                   // '_'
               uData =  THIS->task_SlaveRec.Cmd_E.Idx_W;     Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));                   // Idx Write    2 bytes
               bData =  '_';                                 Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));                   // '_'
               uData =  THIS->task_SlaveRec.Cmd_E.Idx_R;     Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));                   // Idx Read     2 bytes

               memcpy( &THIS->task_SlaveRec.Cmd_E.Data, Task_Application_GetEmergency(), sizeof( THIS->task_SlaveRec.Cmd_E.Data));           // Get Transaction Info.
               pBuf  =  (byte *)&THIS->task_SlaveRec.Cmd_E.Data;     // Transaction Information string. Starting from the "Amount of the Transaction".

               switch( THIS->task_SlaveRec.Cmd_E.Type)
               {
                  case  EMERGENCY_TYPE_CREDIT_CARD:   Size  =  sizeof( Cmd_E_CreditCard); break;
                     default:                            Size  =  sizeof( Cmd_E_RdrMpSw);    break;
               }

               bData =  THIS->task_SlaveRec.Cmd_E.Type;      Task_TibaPay_AddObject( TYPE_BYTE , &bData , sizeof( bData));                   // Emergency Type  1 byte
                  
               // pBuf is already hold the Transaction Information string. Starting from the "Amount of the Transaction" (the money value).
               for( Idx = 0; Idx < Size; Idx ++)
               {
                  bData =  pBuf[Idx];  Task_TibaPay_AddObject( TYPE_BYTE , &bData , sizeof( bData));
               }

               THIS->task_TxCommand =  SERVER_REPLY_EMERGENCY;
               THIS->task_Cmd_E_Type   =  EMERGENCY_TYPE_VOID;

               Task_Params_Read_Var( &THIS->task_Param_Var, sizeof( THIS->task_Param_Var), VAR_CURRENT_Z_NUMBER);
               THIS->task_SlaveRec.Cmd_L.Current_Z_Num   =  THIS->task_Param_Var;
               uData =  THIS->task_SlaveRec.Cmd_L.Current_Z_Num;  Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));
               break;
            }

            //====================================== Send 'L' Response ======================================
            bData =  '_';                                 Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));
            uData =  0;                                   Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));
            bData =  '_';                                 Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));
            uData =  0;                                   Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));         
                  
            bData = 0;                                    Task_TibaPay_PaddingObject( TYPE_BYTE, &bData, 16);     // A.C ZERO Padding 16 bytes (32 characters)
               
            THIS->task_TxCommand =  SERVER_REPLY_TANSACTION;

            Task_Params_Read_Var( &THIS->task_Param_Var, sizeof( THIS->task_Param_Var), VAR_CURRENT_Z_NUMBER);
            THIS->task_SlaveRec.Cmd_L.Current_Z_Num   =  THIS->task_Param_Var;
            uData =  THIS->task_SlaveRec.Cmd_L.Current_Z_Num;  Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));
            break;
            //====================================== Send 'L' Response end  =================================
               
               
         }
         break;

      case  SERVER_REPLY_ENFORCE_Z_CMD :  // 'P'
            break;

   case  SERVER_REPLY_PARAMS_WRITE  :  // 'W'
            bData    =  '.';                 Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));
            break;

      case  SERVER_REPLY_ID            :  // 'Z'
            {
            usint    Idx_W;
      
               THIS->task_pTrans_Info  =  Task_Log_Trans_GetInfo();
               Idx_W =  (THIS->task_pTrans_Info->fMadeCyclicRound == FALSE) ? THIS->task_pTrans_Info->Idx_W : MAX_LOG_TRANS_RECORDS;

               bData    =  '_';                             Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));
               uData    =  Idx_W;                           Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));
               bData    =  '_';                             Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));
               bData    =  '2';                             Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));
               bData    =  '_';                             Task_TibaPay_AddObject( TYPE_CHAR , &bData , sizeof( bData));
               uData    =  Idx_W;                           Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));
               Task_Params_Read_Var( &THIS->task_Param_Var, sizeof( THIS->task_Param_Var), VAR_CURRENT_Z_NUMBER);
               uData    =  THIS->task_Param_Var;            Task_TibaPay_AddObject( TYPE_USINT, &uData , sizeof( uData));
               bData    =  App_Version_Local;               Task_TibaPay_AddObject( TYPE_BYTE , &bData , sizeof( bData));
               //lData    =  ClockAPI_DateAndTimeToSeconds(); Task_TibaPay_AddObject( TYPE_ULONG, &lData , sizeof( lData));
               lData    =  ClockAPI_DateAndTimeToSeconds(); Task_TibaPay_AddObject( TYPE_TIME_DATE, &lData , sizeof( lData));
            }
            break;

      default:
            break;
   }

   if( THIS->task_TxCommand == SERVER_REPLY_ACK)
      THIS->task_TibaPayPacket_Tx.BufSize  =  0;

}



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_AddObject( tType  Type, void  *pData, byte   Len)
/*----------------------------------------------------------------------------*/
{
char     Str[12];
byte     StrLen;


   Str[0]   =  0;

   switch( Type)
   {
      case  TYPE_CHAR     :  sprintf( Str, "%c", *(char *)pData);      break;
      case  TYPE_BYTE     :  sprintf( Str, "%.2X", *(byte *)pData);    break;
      case  TYPE_USINT    :  sprintf( Str, "%.4X", *(usint *)pData);   break;
      case  TYPE_ULONG    :  sprintf( Str, "%.8lX", *(ulong *)pData);  break;
      case  TYPE_TIME_DATE:  sprintf( Str, "%.8lX", *(ulong *)pData);  break; 
      case  TYPE_STRING   :                                            break;
      default             :                                            return;
   }

   if( Type == TYPE_STRING)   StrLen   =  strlen( (char *)pData);
   else                       StrLen   =  strlen( Str);

   if( (THIS->task_TxBufferIdx + StrLen + 1) >= sizeof( THIS->task_TxBuffer))
      return;

   if( Type == TYPE_STRING)   
      strcat( THIS->task_TxBuffer, (char *)pData);
   else
   {
      if ( Type == TYPE_TIME_DATE)      // A.C SuperSwap Time and Date data
      {    // super swap the 8 bytes: B7->B1 B6->B0 
         Util_SuperSwapArray(Str, 8);   // Swap Pairs of Bytes
      }
      //--------------------------------
      strcat( THIS->task_TxBuffer, Str);
   }
   
   THIS->task_TxBuffer[THIS->task_TxBufferIdx + StrLen] = 0;
   THIS->task_TxBufferIdx                               += StrLen;
   THIS->task_TibaPayPacket_Tx.BufSize                  += StrLen;
}



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_PaddingObject( tType  Type, void  *pData, byte   Len)
/*----------------------------------------------------------------------------*/
{
char    Str[12];
byte    StrLen;
byte    index;


   Str[0]   =  0;

   switch( Type)
   {
      case  TYPE_BYTE:  
         sprintf( Str, "%.2X", *(byte *)pData);
         StrLen   =  Len * strlen( Str);
         break;
   
   }  
   //Str[0] = 0x55;
   index = strlen(THIS->task_TxBuffer);         // find length till the \0, not included.
   memset(&THIS->task_TxBuffer[index], Str[0], StrLen);
     
   THIS->task_TxBuffer[THIS->task_TxBufferIdx + StrLen] = 0;
   THIS->task_TxBufferIdx                               += StrLen;
   THIS->task_TibaPayPacket_Tx.BufSize                  += StrLen;   
}



/*----------------------------------------------------------------------------*/
static	void						Task_TibaPay_EraseAllData( void)
/*----------------------------------------------------------------------------*/
{
   SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_L_POINTERS);
   SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_V_POINTERS);
   SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_Z_POINTERS);
   SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_DEL_CRNT_Z_RECORD);
   SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_VITAL_PARAMS);
   SET_BIT( App_TaskEvent[TASK_TIBAPAY], EV_APP_TIBAPAY_RESET_DEBT);

   memset( &THIS->task_StatusStruct, 0, sizeof( TP_Cmd_Q_Data));
   Task_TibaPay_SaveStatusVars();
}



/*----------------------------------------------------------------------------*/
void Util_SuperSwapArray(void * pArray, byte Length)
/*
Swap the input array in the following way: Each pair of bytes are one unit 
   ...to be swapped with other pair of bytes.
Example: 8 bytes: A, B,  C, D,  E, F,  G, H
Result:           G, H,  E, F,  C, D,  A, B
*/
/*----------------------------------------------------------------------------*/
{
byte    *pTail, *pHead, Temp;

   if( (pArray == NULL) || (Length < 4) || ((Length&0x01)==0x01 /*Odd value*/) )
      return;


   pTail =  (byte *)pArray;
   pHead =  ((byte *)pArray + Length - 2);
   
   while (pTail < pHead)
   {
      Temp      = *pTail;
      *pTail    = *pHead;
      *pHead    = Temp;
      //
      pTail++;  
      pHead++;
      Temp      = *pTail;
      *pTail    = *pHead;
      *pHead    = Temp;
      //
      pHead -= 3;  
      pTail++;
   }
}



/*----------------------------------------------------------------------------*/
static void                     Task_TibaPay_CleanTransactionInformation(void)
// Clean buffers with TRACK2 information
/*----------------------------------------------------------------------------*/
{
   memset(&THIS->task_SlaveRec.Cmd_E.Data, 0, sizeof( THIS->task_SlaveRec.Cmd_E.Data));
   memset(THIS->task_TxBuffer, 0, TIBAPAY_MAX_LEN_BUFFER);
}


/*----------------------------------------------------------------------------*/
void                            Task_TibaPay_ShowTR2buffers(void)
// This function allows to watch the TRACK2 buffer, if there is remnant of TRACK2 numbers
/*----------------------------------------------------------------------------*/
{
   Debug_PrintHex((byte*)&THIS->task_SlaveRec.Cmd_E.Data, sizeof( THIS->task_SlaveRec.Cmd_E.Data));
   
}


/*----------------------------------------------------------------------------*/
void                            Task_TibaPay_ShowTxbuffers(void)
// This function allows to watch the TRACK2 buffer, if there is remnant of TRACK2 numbers
/*----------------------------------------------------------------------------*/
{
   Debug_PrintHex((byte*)THIS->task_TxBuffer, TIBAPAY_MAX_LEN_BUFFER);
}
                  


/*----------------------------------------------------------------------------*/
void                                    Task_TibaPay_CommLoss_TO(void)
/*----------------------------------------------------------------------------*/
{
   static   tLogEvent_Data       task_Event_Data;
   if ( THIS->task_fTibaPayCommLoss_Reported == FALSE)  // if new situation
   {
            /* Set Event */
      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_TIBA_PAY_COMM_LOSS, 0, &task_Event_Data);
                                 
            /*           */
      THIS->task_fTibaPayCommLoss_Reported = TRUE;         // Remember: Report on LOSS was sent
   
   }
}



/*----------------------------------------------------------------------------*/
bool                                    Task_TibaPay_IsCommLoss(void)
/*----------------------------------------------------------------------------*/
{
   if (THIS->task_fTibaPayCommLoss_Reported == TRUE) 
      return TRUE;
   else
      return FALSE;
}

/*----------------------------------------------------------------------------*/