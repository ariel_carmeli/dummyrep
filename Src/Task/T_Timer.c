/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  RESET_TIMER_VALUE    (30 * HW_DEF_TMR0_TICKS_PER_SEC) // 30 seconds    
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   usint                task_ResetTimer;
static   byte                 task_Counter   =  0;
/*------------------------ Local Function Prototypes -------------------------*/
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Timer_Init( void)
/*----------------------------------------------------------------------------*/
{
   task_ResetTimer   =  RESET_TIMER_VALUE;   // don't set it to 0 !
   task_Counter      =  0;
}


/*----------------------------------------------------------------------------*/
void                    		Task_Timer_Main( void)
/*----------------------------------------------------------------------------*/
{
   if( BIT_IS_SET( App_TaskEvent[TASK_APPL], EV_APP_APPL_REBOOT))
      HW_DEF_SYSTEM_RESET;
}



#pragma vector = TIMER0_OVF_vect
/*----------------------------------------------------------------------------*/
static __interrupt void       Task_Timer_INT( void)
/*----------------------------------------------------------------------------*/
{
   //if( task_fReset == TRUE)
      //for(;;)  {}   // Endless loop, to cause a WDR

   _WDR(); // Watch Dog Reset

   HW_DEF_INIT_TCCR0;
   HW_DEF_INIT_TCNT0;

   task_Counter   ++;
   
   Timers_FreeRunCnt();         // increment debug 32bit counter

   // the following mechanism is designed to monitor the system (application) from being stuck in a dead-lock
   // The Main-loop (main function) sets every round the value 0xFF into the variable 'App_ResetGuard'
   // The Timer interrupt checks this value against a countdown timer of 30 seconds
   // If - within 30 seconds - 'App_ResetGuard' will not contain this value of 0xFF - the system will RESET
   task_ResetTimer   --;

   if( App_ResetGuard == 0xFF)
   {
      App_ResetGuard    =  0; // toggle 'App_ResetGuard' (to ensure that main-loop set it back to 0xFF)
      task_ResetTimer   =  RESET_TIMER_VALUE;
   }

   if( task_ResetTimer == 0)
      HW_DEF_SYSTEM_RESET; // RESET the application, if application is stuck in a dead-lock
   // END of RESET handler section
}



/*----------------------------------------------------------------------------*/
tTimerMsec							Task_Timer_GetTimer( void)
/*----------------------------------------------------------------------------*/
{
	return( task_Counter);
}



/*----------------------------------------------------------------------------*/
tTimerMsec							Task_Timer_SetTimeMsec( usint	Value)
/*----------------------------------------------------------------------------*/
{
	return( Value / HW_DEF_TMR0_TICK_INTERVAL);
}



/*----------------------------------------------------------------------------*/
tTimerSec							Task_Timer_SetTimeSec( usint     Value)
/*----------------------------------------------------------------------------*/
{
	return( (tTimerSec)Value * (tTimerSec)HW_DEF_TMR0_TICKS_PER_SEC);
}



/*----------------------------------------------------------------------------*/
tTimerMin							Task_Timer_SetTimeMin( byte Value)
/*----------------------------------------------------------------------------*/
{
	return( (tTimerMin)Value * (tTimerMin)HW_DEF_TMR0_TICKS_PER_SEC * (tTimerMin)60);
}



/*----------------------------------------------------------------------------*/
tTimerHour                   Task_Timer_SetTimeHour( byte Value)
/*----------------------------------------------------------------------------*/
{
	return( (tTimerHour)Value * (tTimerHour)HW_DEF_TMR0_TICKS_PER_SEC * (tTimerHour)60 * (tTimerHour)60);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif




