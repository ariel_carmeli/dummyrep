/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  TYPE_VOID                           0
#define  TYPE_NUMBER_REGULAR                 1
#define  TYPE_NUMBER_PRICE                   2

#define  MODE_VOID                           0
#define  MODE_SPACE                          1
#define  MODE_ERASABLE                       2
#define  MODE_NON_ERASABLE                   3
#define  MODE_CURRENT                        4
#define  MODE_WITHOUT_VALUE                  5
/*---------------------------- Typedef Directives ----------------------------*/
static	tEpson_Setup		Epson_Setup;
static	tUart			task_Printer_UartParams;

typedef  enum
{
   STATE_IDLE                 ,
   STATE_PRINTING             ,
   STATE_PRINT_PORTION        ,
   STATE_PRINT_CUT_THE_PAPER  ,
   STATE_ADVANCE_PAPER        ,
   STATE_PRINT_END            ,
}  tState;


typedef  enum
{
   PRINT_TYPE_VOID            =  0           ,
   PRINT_TYPE_HEADLINE        =  0x00000001  ,
   PRINT_TYPE_BRANCH          =  0x00000002  ,
   PRINT_TYPE_Z_NUMBER        =  0x00000004  ,
   PRINT_TYPE_REP_NUMERATOR_1 =  0x00000008  ,
   PRINT_TYPE_RECEIPT_NUMBER  =  0x00000010  ,
   PRINT_TYPE_DAILY_COUNTER   =  0x00000020  ,
   PRINT_TYPE_DATE_TIME       =  0x00000040  ,
   PRINT_TYPE_RECEIPT         =  0x00000080  ,
   PRINT_TYPE_DOOR_OPEN_CLOSE =  0x00000100  ,
   PRINT_TYPE_COINS_IN_TUBES  =  0x00000200  ,
   PRINT_TYPE_COINS_IN_CENTRAL=  0x00000400  ,
   PRINT_TYPE_BILLS_IN_STACKER=  0x00000800  ,
   PRINT_TYPE_FISCAL_REPORT   =  0x00001000  ,
   PRINT_TYPE_TOTAL_SALES     =  0x00002000  ,
   PRINT_TYPE_DEVICE_STATUS   =  0x00004000  ,
   PRINT_TYPE_EVENTS          =  0x00008000  ,
   PRINT_TYPE_TRANSACTIONS    =  0x00010000  ,
   PRINT_TYPE_TRANSACTIONS_CC =  0x00020000  ,
   PRINT_TYPE_COIN_ROUTING    =  0x00040000  ,
   PRINT_TYPE_SETUP_INFO      =  0x00080000  ,
   PRINT_TYPE_TRANS_CANCELLED =  0x00100000  ,
   PRINT_TYPE_Z_HISTORY       =  0x00200000  ,
   PRINT_TYPE_FOOTER          =  0x00400000  ,
   PRINT_TYPE_FOOTER_2        =  0x00800000  ,  // Add second footer: "������ �� ����"
   PRINT_TYPE_FOOTER_3        =  0x01000000  ,  // Add third footer: "�� ���� ������"
   PRINT_TYPE_FOOTER_4        =  0x02000000  ,  // Add fourth footer: ""
   PRINT_TYPE_ADVANCE_PAPER   =  0x04000000  ,
   //PRINT_TYPE_ADVANCE_PAPER   =  0x02000000  ,
   //PRINT_TYPE_FOOTER_4        =  0x04000000  ,  // Add fourth footer: ""
   //PRINT_TYPE_ADVANCE_PAPER   =  0x00800000  ,
   /******************/
   PRINT_TYPE_LAST            =  0x80000000  ,
}  tPrintType;



typedef  enum
{
   HEADER_REPORT_DOOR_OPEN             ,  // "��''� ����� ���"
   HEADER_REPORT_DOOR_CLOSE            ,  // "��''� ����� ���"
   HEADER_RECEIPT                      ,  // "����"
   HEADER_REPORT_TITLE_1               ,  // "��� ������"
   HEADER_REPORT_TITLE_2               ,  // "��� ����"
   HEADER_REPORT_TITLE_3               ,  // "����� ��������"
   HEADER_REPORT_TITLE_4               ,  // "����� ������"
   HEADER_REPORT_TITLE_5               ,  // "����� ������"
   HEADER_REPORT_TITLE_6               ,  // "������ �����"
   HEADER_REPORT_TITLE_7               ,  // "��� ������"
   HEADER_REPORT_TITLE_8               ,  // "����� ����"
   /******************/
   HEADER_LAST
}  tHeader;



typedef  enum
{
   LABEL_LICENSED_DEALER               ,
   LABEL_PRIVATE_COMPANY_NUMBER        ,
   LABEL_BRANCH_NUMBER                 ,
   LABEL_SITE_NAME                     ,
   LABEL_ZONE_NAME                     ,
   LABEL_MACHINE_NAME                  ,
   LABEL_MACHINE_NUMBER                ,
   LABEL_BRANCH_PHONE_NUMBER           ,
   LABEL_RECEIPT_NUMBER                ,
   LABEL_DATE                          ,
   LABEL_TIME                          ,
   LABEL_PRODUCT_CODE_WIDE             ,
   LABEL_PRODUCT_CODE                  ,
   LABEL_PRODUCT                       ,
   LABEL_TRANSACTION_VALUE             ,
   LABEL_AMOUNT_RECEIVED               ,
   LABEL_PAID_IN_CASH                  ,
   LABEL_CHANGE                        ,
   LABEL_PAID_IN_CC                    ,
   LABEL_CC_CARD_NUMBER                ,
   LABEL_CC_TRANS_NUMBER               ,
   LABEL_CC_AUTH_CODE                  ,
   LABEL_RESIDENT_DISCOUNT_TOTAL       ,
   LABEL_FOOTER_1                      ,
   LABEL_FOOTER_2                      ,
   LABEL_FOOTER_3                      ,
   LABEL_FOOTER_4                      ,
   LABEL_REPORT_Z_NUMBER               ,
   LABEL_DAILY_COUNTER                 ,
   LABEL_REPORT_NUMERATOR              ,  
   LABEL_LAST_REPORT_DATE_AND_TIME     ,
   LABEL_MONETARY_REPORTS              ,
   LABEL_CHANGER_MONEY_IN_TUBES        ,
   LABEL_CHANGER_MONEY_REFILLED        ,
   LABEL_CHANGER_MONEY_DEPOSITED       ,
   LABEL_CHANGER_MONEY_TO_CASH_BOX     ,
   LABEL_BV_MONEY_TO_STACKER           ,
   LABEL_BV_BILLS_IN_STACKER           ,
   LABEL_CHANGER_CHANGE_BACK           ,
   LABEL_CHANGER_EMPTYING              ,
   LABEL_CASH_TOTAL_SALES_QTTY         ,
   LABEL_CASH_TOTAL_SALES_VALUE        ,
   LABEL_CC_TOTAL_SALES_QTTY           ,
   LABEL_CC_TOTAL_SALES_VALUE          ,
   LABEL_TOTAL_SALES_QTTY              ,
   LABEL_TOTAL_SALES_VALUE             ,
   LABEL_TOTAL                         ,
   LABEL_CENTRAL_EMPTY                 ,
   LABEL_BILLS_CASHBOX_EMPTY           ,

   LABEL_BILL                          ,
   LABEL_COIN                          ,
   LABEL_LANGUAGE                      ,
   LABEL_CHANGE_LIMIT                  ,
   LABEL_MACHINE_ID                    ,
   LABEL_CC_TERMINAL_NUMBER            ,
   LABEL_CC_J_PARAM                    ,
   LABEL_CC_X_PARAM                    ,
   LABEL_CC_SERVER_TIMEOUT             ,
   LABEL_CC_MIN_TRANSACTION_VALUE      ,
   LABEL_CC_FUNCTION_STATUS            ,
   LABEL_CC_AUTO_RECEIPT               ,

   LABEL_SERVICE                       ,
   /******************/
   LABEL_LAST
}  tLabel;


typedef  enum
{
   HEADLINE_A_LOCATION                 ,
   HEADLINE_A_CUSTOMER_NAME            ,
   HEADLINE_A_MONETARY_REPORT_LEGEND_1 ,
   HEADLINE_A_MONETARY_REPORT_LEGEND_2 ,
   HEADLINE_A_MONETARY_REPORT_LEGEND_3 ,
   HEADLINE_A_COINS_IN_TUBES_INTERNAL  ,
   HEADLINE_A_COINS_IN_CENTRAL         ,
   HEADLINE_A_BILLS_IN_CASHBOX         ,
   HEADLINE_A_COINS_IN_TUBE_LINE1      ,
   HEADLINE_A_COINS_IN_CENTRAL_LINE1   ,
   HEADLINE_A_BILLS_IN_CASHBOX_LINE1   ,
   HEADLINE_A_TRANS_CANCELLED_LINE1    ,
   HEADLINE_A_TRANS_CANCELLED_LINE2    ,
   HEADLINE_A_TRANS_CASH_HEADLINE      ,
   HEADLINE_A_TRANS_CC_HEADLINE        ,
   /******************/
   HEADLINE_A_LAST
}  tHeadLine_A;


typedef  enum
{
   HEADLINE_B_SALES_LINE1              ,
   HEADLINE_B_SALES_LINE2              ,
   HEADLINE_B_Z_HISTORY                ,
   /******************/
   HEADLINE_B_LAST
}  tHeadLine_B;


typedef  enum
{
   VALUE_LANGUAGE_ENGLISH              ,
   VALUE_LANGUAGE_HEBREW               ,
   VALUE_DISABLED                      ,
   VALUE_ENABLED                       ,
   VALUE_UNBLOCKED                     ,
   VALUE_BLOCKED                       ,
   /******************/
   VALUE_LAST
}  tValue;


typedef  enum
{
   STRING_SHORT_LF   ,
   STRING_SHORT_1    ,
   STRING_SHORT_2    ,
   STRING_SHORT_3    ,
   /******************/
   STRING_SHORT_LAST
}  tStringShort;


typedef  enum
{
   STRING_LONG_1     ,
   STRING_LONG_2     ,
   STRING_LONG_3     ,
   STRING_LONG_4     ,
   STRING_LONG_5     ,
   STRING_LONG_6     ,
   STRING_LONG_7     ,
   STRING_LONG_8     ,
   STRING_LONG_9     ,
   STRING_LONG_10    ,
   STRING_LONG_11    ,
   STRING_LONG_12    ,
   STRING_LONG_13    ,
   STRING_LONG_14    ,
   STRING_LONG_15    ,
   STRING_LONG_16    ,
   STRING_LONG_17    ,
   /******************/
   STRING_LONG_LAST
}  tStringLong;


typedef  enum
{
   STATE_CC_IDLE                       ,
   STATE_CC_START                      ,

   STATE_CC_TX_TERMINAL_ID             ,
   STATE_CC_TX_TRACK_2                 ,
   STATE_CC_TX_TRANS_VALUE             ,
   STATE_CC_TX_TRANS_TYPE              ,
   STATE_CC_TX_J_TYPE                  ,
   STATE_CC_TX_X_TYPE                  ,
   STATE_CC_TX_CR_LF                   ,

   STATE_CC_RX_STATUS                  ,  // 3  bytes
   STATE_CC_RX_VOID_1                  ,  // 57 bytes
   STATE_CC_RX_TRANS_TYPE              ,  // 2
   STATE_CC_RX_VOID_2                  ,  // 7  bytes
   STATE_CC_RX_AUTHORIZED_BY           ,  // 1  byte
   STATE_CC_RX_AUTHORIZATION_NUM       ,  // 7  bytes
   STATE_CC_RX_VOID_3                  ,  // 61 bytes
   STATE_CC_RX_LAST                    ,  // /*** must be the last entry in the STATE_CC_RX_???? list ***/

   STATE_CC_STOP                       ,
   /** Last entry **/
   STATE_CC_LAST
}  tState_CreditCard;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/

/*
                  Site Title

Site title is the text which is printed in the Reciept.
It describe the name of the Site, like: "��� ��� ����", "��� �����", "��� �� ����" etc.
The bellow text mechanism, will simplify the ability of System configuraion in the aspect of SITE NAME.
*/
CONST	char            SiteName[][SITE_NAME_LENGTH]   =
                         { // "                    " 
/*                    */ {    "                    "    }  ,  //0
/*                    */ {    "��� ��� ����"            }  ,  //1
/*                    */ {    "��� �����"               }  ,  //2
/*                    */ {    "��� �� ����"             }  ,  //3
/*                    */ {    "��� ��� ����"            }  ,  //4
/*                    */ {    "��� ������"              }  ,  //5
/*                    */ {    "��� ������"              }  ,  //6
/*                    */ {    "��� �������"             }  ,  //7
/*                    */ {    "��� �����"               }  ,  //8
/*                    */ {    "��� ������"              }  ,  //9
/*                    */ {    "��� ������"              }  ,  //10
/*                    */ {    "��� ������"              }  ,  //11
/*                    */ {    "��� �������"             }  ,  //12
/*                    */ {    "��� ����"                }  ,  //13
/*                    */ {    "��� ���� ����"           }  ,  //14
/*                    */ {    "��� ���� ����"           }  ,  //15
                         };



CONST	char		Headers[HEADER_LAST][15 + 1]  =
					{//"               "
                  "��''� ����� ���"    ,  // HEADER_REPORT_DOOR_OPEN
                  "��''� ����� ���"    ,  // HEADER_REPORT_DOOR_CLOSE
                  "����"               ,  // HEADER_RECEIPT
                  "��� ������"         ,  // HEADER_REPORT_TITLE_1
                  "��� ����"           ,  // HEADER_REPORT_TITLE_2
                  "������� ������"     ,  // HEADER_REPORT_TITLE_3
                  "����� ������"       ,  // HEADER_REPORT_TITLE_4
                  "����� ������"       ,  // HEADER_REPORT_TITLE_5
                  "������ �����"       ,  // HEADER_REPORT_TITLE_6
                  "��� ������"         ,  // HEADER_REPORT_TITLE_7
                  "����� ����"         ,  // HEADER_REPORT_TITLE_8
					};

CONST	char		Labels[LABEL_LAST][21 + 1]	=
					{//"                    "
                  "�.�. "              ,  // LABEL_LICENSED_DEALER
                  "�.�. "              ,  // LABEL_PRIVATE_COMPANY_NUMBER
                  "���� ���� :  "      ,  // LABEL_BRANCH_NUMBER
                  "�� ����   :  "      ,  // LABEL_SITE_NAME
                  "�� ����   :  "      ,  // LABEL_ZONE_NAME
                  "��� ����� :  "      ,  // LABEL_MACHINE_NAME
                  "���� �����:  "      ,  // LABEL_MACHINE_NUMBER
                  "��' "               ,  // LABEL_BRANCH_PHONE_NUMBER
                  "���� ���� :  "      ,  // LABEL_RECEIPT_NUMBER
                  "�����     :  "      ,  // LABEL_DATE
                  "���       :  "      ,  // LABEL_TIME
                  "����: "             ,  // LABEL_PRODUCT_CODE_WIDE
                  "���� ���� :  "      ,  // LABEL_PRODUCT_CODE
                  ""                   ,  // LABEL_PRODUCT
                  "���� ���� :  "      ,  // LABEL_TRANSACTION_VALUE
                  "��\"� ���� :  "     ,  // LABEL_AMOUNT_RECEIVED
                  "���� ������: "      ,  // LABEL_PAID_IN_CASH
                  "����� ����:  "      ,  // LABEL_CHANGE
                  "���� ������: "      ,  // LABEL_PAID_IN_CC
                  "���� ������: "      ,  // LABEL_CC_CARD_NUMBER
                  "���� ����� : "      ,  // LABEL_CC_TRANS_NUMBER
                  "��� �����  : "      ,  // LABEL_CC_AUTH_CODE
                  "���� ����  : "      ,  // LABEL_RESIDENT_DISCOUNT_TOTAL
                  "���� �������"       ,  // LABEL_FOOTER_1
                  "������ �� ����-"    ,  // LABEL_FOOTER_2
                  "�� ���� ������"     ,  // LABEL_FOOTER_3
                  "����� ���� ���� �����",// LABEL_FOOTER_4
                  "���� ��\"� :  "     ,  // LABEL_REPORT_Z_NUMBER
                  "���� ���� :  "      ,  // LABEL_DAILY_COUNTER
                  "���� �����:  "      ,  // LABEL_REPORT_NUMERATOR
                  "��� �-    :  "      ,  // LABEL_LAST_REPORT_DATE_AND_TIME
                  "����� ������:"      ,  // LABEL_MONETARY_REPORTS
                  "��� ������ ���� "   ,  // LABEL_CHANGER_MONEY_IN_TUBES
                  "��� ������ �������" ,  // LABEL_CHANGER_MONEY_REFILLED
                  "��� ����� �������"  ,  // LABEL_CHANGER_MONEY_DEPOSITED
                  "��� ����� ����� "   ,  // LABEL_CHANGER_MONEY_TO_CASH_BOX
                  "��� ����� ����� "   ,  // LABEL_BV_MONEY_TO_STACKER
                  "����� ����� ����� " ,  // LABEL_BV_BILLS_IN_STACKER
                  "����� �����"        ,  // LABEL_CHANGER_CHANGE_BACK
                  "����� ����"         ,  // LABEL_CHANGER_EMPTYING
                  "���� ����� ������"  ,  // LABEL_CASH_TOTAL_SALES_QTTY
                  "���� ����� ������"  ,  // LABEL_CASH_TOTAL_SALES_VALUE
                  "���� ����� ������"  ,  // LABEL_CC_TOTAL_SALES_QTTY
                  "���� ����� ������"  ,  // LABEL_CC_TOTAL_SALES_VALUE
                  "��''� ���� ������"  ,  // LABEL_TOTAL_SALES_QTTY
                  "��''� ���� ������"  ,  // LABEL_TOTAL_SALES_VALUE
                  "��''�:  "           ,  // LABEL_TOTAL
                  "*** ����� ���"      ,  // LABEL_CENTRAL_EMPTY
                  "*** ���� ����� ����",  // LABEL_BILLS_CASHBOX_EMPTY

                  "���  "              ,  // LABEL_BILL
                  "���� "              ,  // LABEL_COIN
                  "��� ����� ����:  "  ,  // LABEL_LANGUAGE
                  "���� ����:       "  ,  // LABEL_CHANGE_LIMIT
                  "����� �����:     "  ,  // LABEL_MACHINE_ID
                  "���� ���� �����: "  ,  // LABEL_CC_TERMINAL_NUMBER
                  "����� J:         "  ,  // LABEL_CC_J_PARAM
                  "����� X:         "  ,  // LABEL_CC_X_PARAM
                  "��� ����� �����: "  ,  // LABEL_CC_SERVER_TIMEOUT
                  "���' ����� ����� "  ,  // LABEL_CC_MIN_TRANSACTION_VALUE
                  "���� ������:     "  ,  // LABEL_CC_FUNCTION_STATUS
                  "���� ��������:   "  ,  // LABEL_CC_AUTO_RECEIPT

                  "�����"              ,  // LABEL_SERVICE
					};

CONST	char		HeadLines_A[HEADLINE_A_LAST][CHARS_PER_LINE_FONT_A_NORMAL + 1]	=
               {//"                                  "
#if 0
                  #ifdef   __TZUK_DAROM__
                  "��� ��� ����"                      ,  // HEADLINE_A_LOCATION
                  #endif
                  #ifdef   __SHERATON__
                  "��� �����"                         ,  // HEADLINE_A_LOCATION
                  #endif
                  #ifdef   __TEL_BARUCH__
                  "��� �� ����"                       ,  // HEADLINE_A_LOCATION
                  #endif
#endif
                //"           "
                  "�����"                             ,  // HEADLINE_A_LOCATION
                  "������ ��-���� - ���"              ,  // HEADLINE_A_CUSTOMER_NAME
                  "� = ���� ������"                   ,  // HEADLINE_A_MONETARY_REPORT_LEGEND_1
                  "� = ���� �����"                    ,  // HEADLINE_A_MONETARY_REPORT_LEGEND_2
                  "� = ���� �����"                    ,  // HEADLINE_A_MONETARY_REPORT_LEGEND_3
                  "����� ������ ������ ����"          ,  // HEADLINE_A_COINS_IN_TUBES_INTERNAL
                  "����� ������ ����� �����"          ,  // HEADLINE_A_COINS_IN_CENTRAL
                  "����� ����� �������-�����"         ,  // HEADLINE_A_BILLS_IN_CASHBOX
                  "���� ����  ���� ����"              ,  // HEADLINE_A_COINS_IN_TUBE_LINE1
                  "����  ���� ����"                   ,  // HEADLINE_A_COINS_IN_CENTRAL_LINE1
                  "���     ���� ����"                 ,  // HEADLINE_A_BILLS_IN_CASHBOX_LINE1
                  "������ ������ ������ �����"        ,  // HEADLINE_A_TRANS_CANCELLED_LINE1
                  "������ �� ������ ���� ����� ��"    ,  // HEADLINE_A_TRANS_CANCELLED_LINE2
                  "����� ������ ������"               ,  // HEADLINE_A_TRANS_CASH_HEADLINE
                  "����� ������ ������� �����"        ,  // HEADLINE_A_TRANS_CC_HEADLINE
               };

CONST	char		HeadLines_B[HEADLINE_B_LAST][CHARS_PER_LINE_FONT_B_NORMAL + 1]	=
               {//"                                                "    // this is not a bug: string is limited to 47 chars + 1 (LF)
                  "�����    ���   ����   �����   ����� ���"          ,  // HEADLINE_B_SALES_LINE1
                  "�����    ���   ����   ����� ����  �����"          ,  // HEADLINE_B_SALES_LINE2
                  "����� ��� ���� ��� ���� �����   �����   �����"    ,  // HEADLINE_B_Z_HISTORY
               };

CONST	char		Values[VALUE_LAST][11 + 1]	=
               {//"           "
                  "������"                            ,  // VALUE_LANGUAGE_ENGLISH
                  "�����"                             ,  // VALUE_LANGUAGE_HEBREW
                  "�� ����"                           ,  // VALUE_DISABLED
                  "����"                              ,  // VALUE_ENABLED
                  "�� ����"                           ,  // VALUE_UNBLOCKED
                  "����"                              ,  // VALUE_BLOCKED
               };

/*
CONST	char		LogEvents[LOG_EV_LAST][24 + 1]	=
               {//"                        "
                  ""                                  ,  // LOG_EV_VOID

                  // T_SETUP Events
                  "��� ������ �����"                  ,  // LOG_EV_DOOR_OPENED
                  "��� ������ �����"                  ,  // LOG_EV_DOOR_CLOSED
                  "����� ������ ����� A"              ,  // LOG_EV_TUBES_REFILL_1
                  "����� ������ ����� B"              ,  // LOG_EV_TUBES_REFILL_2
                  "����� ������ ����� C"              ,  // LOG_EV_TUBES_REFILL_3
                  "����� ������ ����� D"              ,  // LOG_EV_TUBES_REFILL_4
                  "����� ������ ����� A"              ,  // LOG_EV_TUBES_EMPTY_1
                  "����� ������ ����� B"              ,  // LOG_EV_TUBES_EMPTY_2
                  "����� ������ ����� C"              ,  // LOG_EV_TUBES_EMPTY_3
                  "����� ������ ����� D"              ,  // LOG_EV_TUBES_EMPTY_4

                  // T_APP Events
                  "����� �������"                     ,  // LOG_EV_TRANSACTION_COMPLETE
                  "����� ������"                      ,  // LOG_EV_TRANSACTION_CANCELLED
                  "����� �����"                       ,  // LOG_EV_TRANSACTION_VALUE
                  "��� ������"                        ,  // LOG_EV_MONEY_RECEIVED
                  "��� ������"                        ,  // LOG_EV_MONEY_BACK
                  "���� ������ ������"                ,  // LOG_EV_MACHINE_HALTED
                  "����� ����� ����"                  ,  // LOG_EV_CC_CHECKED
                  "����� ����� ����"                  ,  // LOG_EV_CC_CHARGED
                  "���� ������ ����� �����"           ,  // LOG_EV_CC_DENIED
                  "��� ����� ����� �����"             ,  // LOG_EV_CC_CHARGED_FAILED (v 3.26)
                  "����� ������ ����"                 ,  // LOG_EV_COIN_ROUTING
                  "����� �����"                       ,  // LOG_EV_POWER_UP
                  "���� ����� ������"                 ,  // LOG_EV_SALES_DATA_ERASED
                  "����� ������"                      ,  // LOG_EV_TRANS_INFO_ERASED
                  "����� ������"                      ,  // LOG_EV_SALES_INFO_ERASED
                  "����� ������"                      ,  // LOG_EV_COINS_INFO_ERASED
                  "�����  �����"                      ,  // LOG_EV_BILLS_INFO_ERASED

                  // Bills events
                  "������ ����� �����"                ,  // LOG_EV_STACKER_REMOVED
                  "������ ����� ������"               ,  // LOG_EV_STACKER_INSERTED
                  "������ ����� ����"                 ,  // LOG_EV_STACKER_IS_FULL

                  // Coins events
                  "���� ������ �����"                 ,  // LOG_EV_COINS_SAFE_REMOVED
                  "���� ������ ������"                ,  // LOG_EV_COINS_SAFE_RETURNED
                  "���� ������ ����"                  ,  // LOG_EV_COINS_SAFE_IS_FULL
               };
*/

static   char           Strings_short[STRING_SHORT_LAST][7 + 1]	=
                  {//"       "
                     "\n"                             ,  // STRING_SHORT_LF
                     "%u"                             ,  // STRING_SHORT_1
                     "%u.%.2u"                        ,  // STRING_SHORT_2
                     "%s [%u]"                        ,  // STRING_SHORT_3
                  };

CONST	static   char		Strings_long[STRING_LONG_LAST][60 + 1]	=
                  {//"                                                            "
                     "%5u %3lu.%.2lu\n"                                             ,  // STRING_LONG_1
                     "  (%2lu.%.2lu)\n"                                             ,  // STRING_LONG_2
                     "%lu.%.2lu (%2lu.%.2lu) �\n"                                   ,  // STRING_LONG_3
                     "%6lu.%.2lu %4u %5u %5u %3u.%.2u  %.2u\n"                      ,  // STRING_LONG_4
                     "(%lu.%.2lu) %s %s\n"                                          ,  // STRING_LONG_5
                     "(%lu) %s %s\n"                                                ,  // STRING_LONG_6
                     "    %2u %3u.%.2u %3u.%.2u %6lu  %2u \n"                       ,  // STRING_LONG_7
                     " %3lu.%.2lu"                                                  ,  // STRING_LONG_8
                     "%lu.%.2lu %4u %2lu.%.2lu    %c"                               ,  // STRING_LONG_9
                     "%lu.%.2lu %4u %2lu.%.2lu"                                     ,  // STRING_LONG_10
                     "%lu.%.2lu %4u  %3lu.%.2lu"                                    ,  // STRING_LONG_11
                     "%c %3lu.%.2lu %4lu.%.2lu%4lu.%.2lu %s"                        ,  // STRING_LONG_12
                     "%5lu %5lu  %.4u%4lu.%.2lu %6lu %s"                            ,  // STRING_LONG_13
                     "%u (%2u.%.2u) %s\n"                                           ,  // STRING_LONG_14
                     #ifndef  __MOATZA__
                     "%3lu.%.2lu %2lu.%.2lu %2u %14s\n"                             ,  // STRING_LONG_15
                     #else
                     "%6lu %5lu %2u %14s\n"                                         ,  // STRING_LONG_15
                     #endif
                     "%c%3lu.%.2lu %4lu.%.2lu %5lu.%.2lu %3u %3u %4u %3u %s\n"      ,  // STRING_LONG_16
                     " %3lu.%.2lu %4lu.%.2lu %5lu.%.2lu                       \n"   ,  // STRING_LONG_17
                  };
/*----------------------------------------------------------------------------*/
//static   tApp_TransInfo         *task_pTransInfo;
//static   tApp_TransInfo_CC      *task_pTransInfo_CC;
//static   tApp_ReceiptInfo       *task_pReceiptInfo;
static   tApp_SalesInfo         *task_pSalesInfo;
static   tApp_CoinsInfo         *task_pCoinsInfo;
static   tApp_BillsInfo         *task_pBillsInfo;
static   tHoppers_Info          *task_pHoppers_Info;
//static   tLogEvent_Rec           task_Log_Event_Rec;
static   tLogTrans_Rec           task_Log_Trans_Rec;
//static   tLogCoinRouting_Rec     task_Log_CoinRouting_Rec;
static   tParams_C_Rec           task_Param_C_Rec;
static   tParams_D_Rec           task_Param_Str16_Rec;
static   tParams_E_Rec           task_Param_E_Rec;
static   tParams_Var_Rec         task_Param_Var;
static   Cmd_Q_Data             *task_pStatusStruct;
static   Cmd_L_Data_Product      task_Product;
static   tState                  task_State;
static   tHeader                 task_Header;
static   tPrintType              task_PrintType;
static   tPrintType              task_PrintType_temp;
static   char                    task_Str[PRINT_MAX_BUF_SIZE + 2];
static   char                    task_StrTemp[PRINT_MAX_BUF_SIZE + 2];
static   byte                    task_State_Print;
static   bool                    task_fPrinterIsBusy;
static   bool                    task_fTestPrinter;
static   byte                    task_CutterMode;
/* group of common variables for printouts */
static   byte                    task_Index;
static   ulong                   task_Value;
static   byte                    task_LineFeed;
static   byte                    task_Type;
static   byte                    task_Mode;
static   tLabel                  task_Label;
static   byte                    task_Copies;
static   bool                    task_fDontPrintLabel;
static   bool                    task_fDontInitPrn;
static   bool                    task_fReceipt_PrintingAttachment;

static   ulong                   task_Total;
char                             SiteNameBuff[20];
/*------------------------ Local Function Prototypes -------------------------*/
void                    	Task_Printer_OpenPort( void);
static	void			Task_Printer_HandleTimer            ( void);
static	void			Task_Printer_HandleEvent            ( void);
static	void			Task_Printer_HandleAppEvent         ( void);
static	void			Task_Printer_HandleState            ( void);
static	void			Task_Printer_PrintHeadline          ( void);
static	void			Task_Printer_PrintBranch            ( void);
static	void			Task_Printer_PrintReportZnumber     ( void);
static	void			Task_Printer_PrintReportNumerator_1 (  void);
static	void			Task_Printer_PrintReceiptNumber     ( void);
static	void			Task_Printer_PrintDailyCounter      ( void);
static	void			Task_Printer_PrintDateTime          ( void);
static	void                    Task_Printer_PrintReceipt           ( void);
//static   void                 Task_Printer_PrintZHistory       ( void);
static   void                   Task_Printer_PrintFooter            ( void);
static   void                   Task_Printer_PrintFooter_2          ( void);
static   void                   Task_Printer_PrintFooter_3          ( void);
static   void                   Task_Printer_PrintFooter_4          ( void);
static   void                   Task_Printer_AdvancePaper           ( void);
static   void                   Task_Printer_PrintTransCancelled    ( void);
static	void			Task_Printer_PrintDoorOpenClose     ( void);

static	void			Task_Printer_PrintCoinsInTubes      ( void);
static   void                   Task_Printer_PrintCoinsInCentral    ( void);
static	void			Task_Printer_PrintBillsInStacker    ( void);
static	void			Task_Printer_PrintFiscalReport      ( void);
static	void			Task_Printer_PrintTotalSales        ( void);
static	void			Task_Printer_PrintDeviceStatus      ( void);
static	void			Task_Printer_PrintEvents            ( void);
static   void                   Task_Printer_PrintTransactions      ( void);
static   void                   Task_Printer_PrintCoinRouting       ( void);
static	void			Task_Printer_PrintSetupInfo         ( void);
static	void			Task_Printer_PrintHeader            ( tHeader   Header);
static	void			Task_Printer_PrintString            ( tLabel Label, byte Index);
static	void			Task_Printer_PrintNumber            ( void  *pNumber, byte  Size, byte   Type, byte   Mode);
static	void			Task_Printer_SetNumber              ( void  *pNumber, byte  Size);
static	void			Task_Printer_SetPrice               ( void  *pPrice, byte  Size);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Printer_Init( void)
/*----------------------------------------------------------------------------*/
{

   //task_pTransInfo                                    =  Task_Application_GetTransactionInfo();
   //task_pReceiptInfo                                  =  Task_Application_GetReceiptInfo();
   task_pSalesInfo                                      =  Task_Application_GetSalesInfo();
   task_pCoinsInfo                                      =  Task_Application_GetCoinsInfo();
   task_pBillsInfo                                      =  Task_Application_GetBillsInfo();
   task_pHoppers_Info                                   =  Task_Hopper_GetInfo();
   task_State                                           =  STATE_IDLE;
   task_pStatusStruct                                   =  Task_Server_GetStatusStructPtr();
   task_fPrinterIsBusy                                  =  TRUE;
   task_CutterMode                                      =  PRINT_CUTTER_FULL;
   task_Label                                           =  LABEL_LAST;
  
   //PrinterAPI_Init( /*EXAR_B*/);

   // initial Epson API library
   Epson_Setup.func_Task_Timer_GetTimer                 =	Task_Timer_GetTimer;
   Epson_Setup.func_OpenPort                            =	NULL;                                   //Task_Printer_OpenPort;
   Epson_Setup.HwDef_TMR0_TickInterval                  =	HW_DEF_TMR0_TICK_INTERVAL;              // HW_DEF_TMR0_TICK_INTERVAL (usually it equals to 10msec)
   Epson_Setup.func_Task_Application_Prn_InsertText     =       Task_Application_Prn_InsertText;
   Epson_Setup.func_Task_Printer_COM2_HandleRx          =       NULL;                                   // Currently not used
   Epson_Setup.func_Task_Application_Prn_IsBusy         =       Task_Application_Prn_IsBusy;
   Epson_Setup.Printer_Type                             =       PRINTER_T_102;
   Epson_Setup.Printer_Local                            =       FALSE;                                  // A remote printer (through Door Controller).
   Epson_Setup.Epson_Valid_Alef_offset                  =       HW_DEF_PRN_HEBREW_OFFSET_IDE;
   Epson_Setup.Lcd_Valid_Alef_offset                    =       HW_DEF_PRN_HEBREW_OFFSET_LCD;
   Epson_Init(&Epson_Setup);

}


#if 0
/*----------------------------------------------------------------------------*/
void                    		Task_Printer_OpenPort( void)
/*----------------------------------------------------------------------------*/
{
   // Initial ComPort
   task_Printer_UartParams.Com.Port             =	COM_6;                  /* EXAR_B */
   task_Printer_UartParams.Com.BaudRate         =	BAUD_RATE_9600;
   task_Printer_UartParams.Com.DataBits         =	DATA_BITS_8;
   task_Printer_UartParams.Com.Parity           =	PARITY_NONE;
   task_Printer_UartParams.Com.StopBits         =	STOP_BITS_1;
   task_Printer_UartParams.Com.RS485_Address    =	0;
   task_Printer_UartParams.TxFunc               =	Epson_Tx_ISR;
   task_Printer_UartParams.RxFunc               =	Epson_Rx_ISR;
   //
   UartDRV_SetPort(&task_Printer_UartParams);																	// prepare function pointers: Uart_TxByte, Uart_TxEnd, Uart_RxByte.

   // Epson does not recognize the type tUart. Thats why it can Not recognize Task_Scanner_UartParams !!!
   Epson_Setup.func_Uart_TxByte                 =	task_Printer_UartParams.Uart_TxByte;			// DRV Tx function
   Epson_Setup.func_Uart_TxEnd                  =	task_Printer_UartParams.Uart_TxEnd;			// DRV Tx-End function
   Epson_Setup.func_Uart_RxByte                 =	task_Printer_UartParams.Uart_RxByte;			// DRV Rx function
}
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Printer_Main( void)
/*----------------------------------------------------------------------------*/
{
	if( App_TaskStatus[TASK_PRINTER] == DISABLED)
		return;

   Task_Printer_HandleTimer();

   task_fPrinterIsBusy  =  Epson_IsBusy();

   Task_Printer_HandleEvent();
   Task_Printer_HandleAppEvent();
   Task_Printer_HandleState();
   Epson_Main();

}



/*----------------------------------------------------------------------------*/
void                          Task_Printer_SetCopies( byte  Copies)
/*----------------------------------------------------------------------------*/
{
   task_Copies =  Copies;
}



/*----------------------------------------------------------------------------*/
byte                          Task_Printer_GetStatus( void)
/*----------------------------------------------------------------------------*/
{
   return( Epson_GetStatus());
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
   	OldTimer		=	NewTimer;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_HandleEvent( void)
/*----------------------------------------------------------------------------*/
{
   if( task_State != STATE_IDLE)
      return;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_HandleAppEvent( void)
/*----------------------------------------------------------------------------*/
{
   if( (task_State != STATE_IDLE) || (task_fPrinterIsBusy == TRUE))
      return;

   CLEAR_BIT( App_TaskEvent[TASK_PRINTER], (EV_APP_PRN_ERROR | EV_APP_PRN_END_PRINTING_JOB));

   if( App_TaskEvent[TASK_PRINTER] & EV_APP_PRN_ALL_BITS)
   {
      if( (task_State != STATE_IDLE) || (task_fPrinterIsBusy == TRUE))
         return;
         
      task_CutterMode   =  PRINT_CUTTER_FULL;

      //if( task_fDontInitPrn == FALSE)
      //   Task_Application_Prn_Init();

      task_fDontInitPrn =  FALSE;

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RPRT_DOOR_OPEN))
      {
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_RPRT_DOOR_OPEN);

         if( BIT_IS_SET( Epson_GetStatus(), PRINTER_STATUS_PAPER_OUT))
         {
            SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR);
            return;
         }

         task_Header =  HEADER_REPORT_DOOR_OPEN;  // "��''� ����� ���"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_Z_NUMBER        |
                                    PRINT_TYPE_REP_NUMERATOR_1 |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_DOOR_OPEN_CLOSE |
                                    PRINT_TYPE_COINS_IN_TUBES  |
                                    PRINT_TYPE_COINS_IN_CENTRAL|
                                    PRINT_TYPE_BILLS_IN_STACKER));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RPRT_DOOR_CLOSE))
      {
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_RPRT_DOOR_CLOSE);

         if( BIT_IS_SET( Epson_GetStatus(), PRINTER_STATUS_PAPER_OUT))
         {
            SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR);
            return;
         }

         task_Header =  HEADER_REPORT_DOOR_CLOSE;  // "��''� ����� ���"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_Z_NUMBER        |
                                    PRINT_TYPE_REP_NUMERATOR_1 |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_DOOR_OPEN_CLOSE |
                                    PRINT_TYPE_COINS_IN_TUBES  |
                                    PRINT_TYPE_COINS_IN_CENTRAL|
                                    PRINT_TYPE_BILLS_IN_STACKER));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RECEIPT))
      {
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_RECEIPT);

         if( BIT_IS_SET( Epson_GetStatus(), PRINTER_STATUS_PAPER_OUT))
         {
            SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR);
            return;
         }

         task_Header =  HEADER_RECEIPT;  // "����"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_Z_NUMBER        |
                                    PRINT_TYPE_DAILY_COUNTER   |
                                    PRINT_TYPE_RECEIPT_NUMBER  |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_RECEIPT         |
                                    PRINT_TYPE_FOOTER          |
                                    PRINT_TYPE_FOOTER_2        |        // Add second footer: "������ �� ����-�� ���� ������"
                                    PRINT_TYPE_FOOTER_3        |  
                                    PRINT_TYPE_FOOTER_4        |
                                    PRINT_TYPE_ADVANCE_PAPER   ));
         task_State  =  STATE_PRINTING;

         //if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT))
         //   task_CutterMode   =  PRINT_CUTTER_HALF;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT))
      {
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT);

         if( BIT_IS_SET( Epson_GetStatus(), PRINTER_STATUS_PAPER_OUT))
         {
            SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR);
            return;
         }

         task_Header =  HEADER_RECEIPT;  // "����" (receipt attachment)
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_RECEIPT_NUMBER  |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_RECEIPT         ));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_REPORT_SALES))
      {
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_REPORT_SALES);

         task_Header =  HEADER_REPORT_TITLE_1;  // "��� ������"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_Z_NUMBER        |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_TRANSACTIONS    ));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_REPORT_FISCAL))
      {
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_REPORT_FISCAL);

         task_Header =  HEADER_REPORT_TITLE_2;  // "��� ����"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_Z_NUMBER        |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_COINS_IN_TUBES  |
                                    PRINT_TYPE_COINS_IN_CENTRAL|
                                    PRINT_TYPE_BILLS_IN_STACKER|
                                    PRINT_TYPE_FISCAL_REPORT   ));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_EVENTS))
      {
         if( BIT_IS_SET( Epson_GetStatus(), PRINTER_STATUS_PAPER_OUT))
         {
            SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR);
            return;
         }

         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_EVENTS);
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_DEVICES_STATUS);

         task_Header =  HEADER_REPORT_TITLE_3;  // "������� ������"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_Z_NUMBER        |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_DEVICE_STATUS   |
                                    PRINT_TYPE_EVENTS          ));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_REPORT_Z))
      {
         if( BIT_IS_SET( Epson_GetStatus(), PRINTER_STATUS_PAPER_OUT))
         {
            SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR);
            return;
         }

         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_REPORT_Z);

         task_Header =  HEADER_REPORT_TITLE_4;  // "����� ������"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_Z_NUMBER        |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_TOTAL_SALES     ));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_COIN_ROUTING))
      {
         if( BIT_IS_SET( Epson_GetStatus(), PRINTER_STATUS_PAPER_OUT))
         {
            SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR);
            return;
         }

         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_COIN_ROUTING);

         task_Header =  HEADER_REPORT_TITLE_5;  // "����� ������"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_Z_NUMBER        |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_DEVICE_STATUS   |
                                    PRINT_TYPE_COIN_ROUTING    ));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_SETUP_INFO))
      {
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_SETUP_INFO);

         task_Header =  HEADER_REPORT_TITLE_6;  // "������ �����"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_SETUP_INFO      ));
         task_State  =  STATE_PRINTING;
         return;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_DEVICES_STATUS))
      {
         CLEAR_BIT( App_TaskEvent[TASK_PRINTER] , EV_APP_PRN_PRINT_DEVICES_STATUS);

         task_Header =  HEADER_REPORT_TITLE_7;  // "��� ������"
         SET_BIT( task_PrintType, ( PRINT_TYPE_HEADLINE        |
                                    PRINT_TYPE_BRANCH          |
                                    PRINT_TYPE_DATE_TIME       |
                                    PRINT_TYPE_DEVICE_STATUS   ));
         task_State  =  STATE_PRINTING;
         return;
      }
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_HandleState( void)
/*----------------------------------------------------------------------------*/
{
static   byte        CopiesPrinted  =  0;
static   tPrintType  PrintType      =  PRINT_TYPE_VOID;



   task_fDontPrintLabel =  FALSE;

   switch( task_State)
   {
      case  STATE_IDLE:
            task_Copies          =  0;
            CopiesPrinted        =  0;
            PrintType            =  PRINT_TYPE_VOID;
            break;

      case  STATE_PRINTING:
            if( task_fPrinterIsBusy == TRUE)
               return;
            
            if( task_PrintType == PRINT_TYPE_VOID)
            {
               task_State  =  STATE_PRINT_CUT_THE_PAPER;
               break;
            }

            for( task_PrintType_temp = (tPrintType)1; task_PrintType_temp < PRINT_TYPE_LAST; task_PrintType_temp <<= 1)
            {
               if( BIT_IS_SET( task_PrintType, task_PrintType_temp))
               {
                  CLEAR_BIT( task_PrintType  , task_PrintType_temp);
                  SET_BIT  ( PrintType       , task_PrintType_temp);
                  task_State_Print  =  0;
                  task_Index        =  0;
                  task_State  =  STATE_PRINT_PORTION;
                  break;
               }
            }

            if( task_PrintType_temp == PRINT_TYPE_LAST)
               task_State  =  STATE_PRINT_CUT_THE_PAPER;
            break;

      case  STATE_PRINT_PORTION:
            task_Str[0] =  0;

            switch( task_PrintType_temp)
            {
               case  PRINT_TYPE_HEADLINE        :  Task_Printer_PrintHeadline();             break;
               case  PRINT_TYPE_BRANCH          :  Task_Printer_PrintBranch();               break;
               case  PRINT_TYPE_Z_NUMBER        :  Task_Printer_PrintReportZnumber();        break;
               case  PRINT_TYPE_REP_NUMERATOR_1 :  Task_Printer_PrintReportNumerator_1();    break;
               case  PRINT_TYPE_RECEIPT_NUMBER  :  Task_Printer_PrintReceiptNumber();        break;
               case  PRINT_TYPE_DAILY_COUNTER   :  Task_Printer_PrintDailyCounter();         break;
               case  PRINT_TYPE_DATE_TIME       :  Task_Printer_PrintDateTime();             break;
               case  PRINT_TYPE_RECEIPT         :  Task_Printer_PrintReceipt();              break;
               case  PRINT_TYPE_DOOR_OPEN_CLOSE :  Task_Printer_PrintDoorOpenClose();        break;
               case  PRINT_TYPE_COINS_IN_TUBES  :  Task_Printer_PrintCoinsInTubes();         break;
               case  PRINT_TYPE_COINS_IN_CENTRAL:  Task_Printer_PrintCoinsInCentral();       break;
               case  PRINT_TYPE_BILLS_IN_STACKER:  Task_Printer_PrintBillsInStacker();       break;
               case  PRINT_TYPE_FISCAL_REPORT   :  Task_Printer_PrintFiscalReport();         break;
               //case  PRINT_TYPE_TOTAL_SALES     :  Task_Printer_PrintTotalSales();           break;
               //case  PRINT_TYPE_DEVICE_STATUS   :  Task_Printer_PrintDeviceStatus();         break;
               //case  PRINT_TYPE_EVENTS          :  Task_Printer_PrintEvents();               break;
               //case  PRINT_TYPE_TRANSACTIONS    :  Task_Printer_PrintTransactions();         break;
               //case  PRINT_TYPE_COIN_ROUTING    :  Task_Printer_PrintCoinRouting();          break;
               //case  PRINT_TYPE_SETUP_INFO      :  Task_Printer_PrintSetupInfo();            break;
               case  PRINT_TYPE_FOOTER          :  Task_Printer_PrintFooter();               break;
               case  PRINT_TYPE_FOOTER_2        :  Task_Printer_PrintFooter_2();             break;
               case  PRINT_TYPE_FOOTER_3        :  Task_Printer_PrintFooter_3();             break;
               case  PRINT_TYPE_FOOTER_4        :  Task_Printer_PrintFooter_4();             break;
               case  PRINT_TYPE_ADVANCE_PAPER   :  Task_Printer_AdvancePaper();              break;
               //case  PRINT_TYPE_TRANS_CANCELLED :  Task_Printer_PrintTransCancelled();       break;
               //case  PRINT_TYPE_Z_HISTORY       :  Task_Printer_PrintZHistory();             break;
               default                          :  task_State  =  STATE_PRINT_CUT_THE_PAPER; break;
            }

            if( task_Label != LABEL_LAST)
            {
               Task_Printer_PrintString( task_Label, task_Index);
               task_Label  =  LABEL_LAST;
            }

            if( task_Mode != MODE_VOID)
            {
               if( task_Mode != MODE_WITHOUT_VALUE)
                  Task_Printer_PrintNumber( &task_Value, sizeof( task_Value), task_Type, task_Mode);
               task_Mode   =  MODE_VOID;
            }

            if( task_LineFeed > 0)
            {
               Epson_LineFeed( task_LineFeed);
               task_LineFeed  =  0;
            }
            break;

      case  STATE_PRINT_CUT_THE_PAPER:
            if( BIT_IS_CLEAR( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT))
               Epson_Cutter( task_CutterMode);
            task_State  =  STATE_ADVANCE_PAPER;
            break;

      case  STATE_ADVANCE_PAPER:
            if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT))
            {
               task_fDontInitPrn                =  TRUE;
               task_fReceipt_PrintingAttachment =  TRUE;
               task_State        =  STATE_IDLE;
               break;
            }

            task_fReceipt_PrintingAttachment =  FALSE;

            Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_PRN_EXTRA_LF);
            Epson_LineFeed( task_Param_C_Rec);
            task_State  =  STATE_PRINT_END;
            break;

      case  STATE_PRINT_END:
            if( (++CopiesPrinted) < task_Copies)
            {
               task_PrintType =  PrintType;
               task_State     =  STATE_PRINTING;
               break;
            }

            BuzzAPI_SetMode( FRAGMENTED_3, 2);
            SET_BIT( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_END_PRINTING_JOB);
            //Task_Application_Prn_Go();
            /* don't 'break' ; continue to 'default' */

      default:
            task_CutterMode   =  PRINT_CUTTER_FULL;
            task_fTestPrinter =  FALSE;
            task_State        =  STATE_IDLE;
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintHeadline( void)
/*----------------------------------------------------------------------------*/
{
tParams_A_Rec   task_Rec_Type_A;
char*           pApp_code_string;

   switch( task_State_Print)
   {
                  // Print extra LF before printing the header
      case  0  :  /*Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);*/   break;
      case  1  :                                                                                break;
      case  2  :  /*Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_DOUBLE);*/
                  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  3  :  //
                  Task_Params_Read( &task_Rec_Type_A, PARAM_TYPE_A, PARAM_A_SITE_NAME /*ParamCode*/);     // Get from param A, the index of Site Name
                  pApp_code_string = Task_Printer_GetSiteName((byte) task_Rec_Type_A);                    // Get pointer to the Site Name string
                  strcpy( task_Str, pApp_code_string );
                  //
                  //strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_CENTER);                              break;

      case  4  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  5  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_CUSTOMER_NAME]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_CENTER);                              break;
      case  6  :  Task_Printer_PrintString( LABEL_PRIVATE_COMPANY_NUMBER, 0);                   break;

      case  7  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  8  :  Epson_FullLine_SameChar( '=');                                           break;
      case  9  :  if( task_fReceipt_PrintingAttachment == FALSE)
                     //Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_DOUBLE);   break;
                     Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  10 :  Task_Printer_PrintHeader( task_Header);                                       break;
      case  11 :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  12 :  Epson_FullLine_SameChar( '=');                                           break;

      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintBranch( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  /*task_Label  =  LABEL_SITE_NAME;    */       break;
      case  1  :  /*task_Label  =  LABEL_ZONE_NAME;    */       break;
      case  2  :  /*task_Label  =  LABEL_MACHINE_NAME; */       break;
      case  3  :  /*task_Label  =  LABEL_BRANCH_NUMBER;*/       break;
      case  4  :  task_Label  =  LABEL_MACHINE_NUMBER;      break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;            break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintReportZnumber( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  task_Label  =  LABEL_REPORT_Z_NUMBER;           break;
      //case  1  :  task_Label  =  LABEL_LAST_REPORT_DATE_AND_TIME; break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                  break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintReportNumerator_1 (  void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  task_Label  =  LABEL_REPORT_NUMERATOR;          break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                  break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintReceiptNumber( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  task_Label  =  LABEL_RECEIPT_NUMBER;            break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                  break;
   }

   task_State_Print  ++;
}


/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintDailyCounter   ( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  task_Label  =  LABEL_DAILY_COUNTER;             break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                  break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintDateTime( void)
/*----------------------------------------------------------------------------*/
{
   if( task_Header == HEADER_RECEIPT /*"����"*/)
   {
      switch( task_State_Print)
      {
         case  0  :  if( task_fReceipt_PrintingAttachment == FALSE)
                        Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_DOUBLE);   break;
         case  1  :  task_Label  =  LABEL_DATE;       task_fDontPrintLabel = TRUE;                 break;
         case  2  :  task_Label  =  LABEL_TIME;       task_fDontPrintLabel = TRUE;                 break;
         case  3  :  /*task_LineFeed  =  1;*/                                                          break;
         default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                     task_State  =  STATE_PRINTING;                                                break;
      }
   }
   else
   {
      switch( task_State_Print)
      {
         case  0  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
         case  1  :  task_Label  =  LABEL_DATE;                                                    break;
         case  2  :  task_Label  =  LABEL_TIME;                                                    break;
         case  3  :  task_LineFeed  =  1;                                                          break;
         default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                     task_State  =  STATE_PRINTING;                                                break;
      }
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintReceipt( void)
/*----------------------------------------------------------------------------*/
{
static   bool     fCash          =  FALSE;
static   bool     fCC            =  FALSE;
static   bool     fResidentCard  =  FALSE;
ulong             Price;


   task_Value  =  0;
   task_Type   =  TYPE_VOID;

   switch( task_State_Print)
   {
      case  0  :  task_Index     =  0;
                  fCash          =  FALSE;
                  fCC            =  FALSE;
                  fResidentCard  =  FALSE;
                  if( task_fReceipt_PrintingAttachment == FALSE)
                     Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_DOUBLE);   break;
      case  1  :  /* Products */
                  Task_Params_Read_Var ( &task_Param_Var, sizeof( task_Param_Var), (VAR_RCPT_PRODUCTS + (task_Index * sizeof( task_Param_Var))));
                  Task_Params_Read     ( task_Param_E_Rec, PARAM_TYPE_E, (STR_E_PROD_01_NAME_HE + (task_Index * sizeof( task_Param_E_Rec))));
                  task_Label  =  LABEL_PRODUCT;
                  if( (++ task_Index) < MAX_L_REC_RECEIPT_ITEMS)  task_State_Print  --;
                  else                                            task_Index  =  0;             break;
      case  2  :  /*task_LineFeed =  1;*/                                                       break;          // Line feed after products list. Before Transaction Value
      case  3  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);        break;

      case  4  :  if( task_fReceipt_PrintingAttachment == TRUE)
                  {
                     task_State_Print  =  16;
                     goto  RECEIPT_ATTACHMENT;
                  }                                                                             break;

      case  5  :  /* Transaction Value */
                  task_Label  =  LABEL_TRANSACTION_VALUE;                                       break;
      case  6  :  /* Resident Card section */
                  Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_RESIDENT_DISCOUNT_TOTAL);
                  if( task_Param_Var > 0)
                  {
                     fResidentCard  =  TRUE;
                     task_Label     =  LABEL_RESIDENT_DISCOUNT_TOTAL;
                  }                                                                             break;
      case  7  :  if( fResidentCard == TRUE) task_LineFeed =  1;                                break;
      case  8  :  /* Cash section */
                  Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CASH_AMOUNT_RECEIVED);
                  if( task_Param_Var > 0)
                  {
                     fCash =  TRUE;
                     task_Label  =  LABEL_PAID_IN_CASH;
                  }                                                                             break;
      case  9  :  /* Change */
                  if( fCash == TRUE)   task_Label  =  LABEL_CHANGE;                             break;
      case 10  :  if( fCash == TRUE)   task_LineFeed =  1;                                      break;
      case 11  :  /* Credit Card section */
                  Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_AMOUNT_RECEIVED);
                  if( task_Param_Var > 0)
                  {
                     fCC   =  TRUE;
                     task_Label  =  LABEL_PAID_IN_CC;
                  }                                                                             break;
      case 12  :  if( fCC == TRUE)   task_Label =  LABEL_CC_CARD_NUMBER;                        break;
      case 13  :  if( fCC == TRUE)   task_Label =  LABEL_CC_TRANS_NUMBER;                       break;
      case 14  :  if( fCC == TRUE)   task_Label =  LABEL_CC_AUTH_CODE;                          break;
      case 15  :  if( fCC == TRUE)   task_LineFeed =  1;                                        break;

      RECEIPT_ATTACHMENT   :
      case 16  :  Epson_FullLine_SameChar( '=');                                           break;
      case 17  :  if( task_fReceipt_PrintingAttachment == FALSE)
                     //Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_DOUBLE);   break;
                     //Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
                     Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_DOUBLE);   break;
      case 18  :  Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_TRANS_PRICE);        // price
                  task_Total  =  task_Param_Var;
                  task_Label  =  LABEL_TOTAL;                                                   break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}



#if 0 // [
/*----------------------------------------------------------------------------*/
static   void                 Task_Printer_PrintZHistory       ( void)
/*----------------------------------------------------------------------------*/
{
static   ulong    Total_1     =  0;
static   ulong    Total_2     =  0;
static   ulong    Total_3     =  0;
static   byte     Guard       =  0;
static   bool     fFirstRec   =  TRUE;
char              Date[10];
char              LeadingChar;
bool              fReadStatus;
bool              fPrint;
tLogZ_Rec         LogZ_Rec;


   switch( task_State_Print)
   {
                  // Print extra LF before printing the header
      case  0  :  Total_1     =  0;
                  Total_2     =  0;
                  Total_3     =  0;
                  Guard       =  0;
                  fFirstRec   =  TRUE;
                  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  1  :  strcpy_P( task_Str, HeadLines_B[HEADLINE_B_Z_HISTORY]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);                               break;
      case  2  :  Epson_FullLine_SameChar( '-');                                           break;
      case  3  :  fPrint      =  FALSE;
                  fReadStatus =  Task_Log_Z_ReadRecord( &LogZ_Rec, fFirstRec);
                  // Read historic Z records
                  if( (fReadStatus == TRUE) && (Guard < MAX_LOG_Z_RECORDS))
                  {
                     strcpy( Date, ClockAPI_DaysToStr( LogZ_Rec.Date));   // DD/MM/YY
                     Date[5]  =  0; // DD/MM/YY --> DD/MM
                     Guard       ++;
                     fFirstRec   =  FALSE;
                     fPrint      =  TRUE;
                     LeadingChar =  SPACE;
                     task_State_Print  --;
                  }
                  else
                  {
                     // Read current Z record
                     strcpy( Date, ClockAPI_GetDateStr( DATE_FORMAT_DDMMYY, DATE_DELIMITER_SLASH));   // DD/MM/YY
                     Date[5]  =  0; // DD/MM/YY --> DD/MM
                     fPrint      =  TRUE;
                     LeadingChar =  '*';
                     Task_Params_Read_Var( &LogZ_Rec, sizeof( LogZ_Rec), VAR_Z_RECORD);   // print also current Z report
                  }

                  if( fPrint == TRUE)
                  {
                     Total_1  += LogZ_Rec.Val_Confirmations;
                     Total_2  += LogZ_Rec.Val_Credit;
                     Total_3  += LogZ_Rec.Val_Cash;

                     sprintf_P( task_StrTemp, Strings_long[STRING_LONG_16],
                                 ((LeadingChar                          )),
                                 ((LogZ_Rec.Val_Confirmations  /  100   ) % 1000L)  ,
                                 ((LogZ_Rec.Val_Confirmations  %  100   )),
                                 ((LogZ_Rec.Val_Credit         /  100   ) % 10000L) ,
                                 ((LogZ_Rec.Val_Credit         %  100   )),
                                 ((LogZ_Rec.Val_Cash           /  100   ) % 100000L),
                                 ((LogZ_Rec.Val_Cash           %  100   )),
                                 ((LogZ_Rec.Qty_Confirmations  %  1000  )),
                                 ((LogZ_Rec.Qty_RenewCard      %  1000  )),
                                 ((LogZ_Rec.Qty_Entrances      %  10000 )),
                                 ((LogZ_Rec.Z_Num              %  1000  )),
                                 ((Date                                 )));
                     Epson_Print( task_StrTemp, PRINT_ALIGN_RIGHT);
                  }
                  break;
      case  4  :  Epson_FullLine_SameChar( '-');                                           break;
      case  5  :  sprintf_P( task_StrTemp, Strings_long[STRING_LONG_17],
                              ((Total_1   /  100) % 10000L)    ,
                              ((Total_1   %  100)),
                              ((Total_2   /  100) % 100000L)   ,
                              ((Total_2   %  100)),
                              ((Total_3   /  100) % 1000000L)  ,
                              ((Total_3   %  100)));
                  Epson_Print( task_StrTemp, PRINT_ALIGN_RIGHT);
                  break;
      case  6  :  task_LineFeed =  1;                                                           break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}
#endif   // ]



/*----------------------------------------------------------------------------*/
static   void                 Task_Printer_PrintFooter( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  Epson_FullLine_SameChar( '=');                                                break;
      /*case  1  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_DOUBLE);        break;*/
      case  1  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_DOUBLE);        break;
      case  2  :  task_Label  =  LABEL_FOOTER_1;                                                break;          //"���� �������"
      case  3  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);        break;
      //case  4  :  task_LineFeed  =  1;                                                          break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}


/*----------------------------------------------------------------------------*/
static   void                   Task_Printer_PrintFooter_2( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  Epson_FullLine_SameChar( '=');                                           break;
      case  1  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_DOUBLE);   break;

      case  2  :  task_Label  =  LABEL_FOOTER_2;                                           break;       // "������ �� ����-"
      case  3  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_DOUBLE);   break;
                 
      case  4  :  task_LineFeed  =  1;                                                     break;
      default  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_DOUBLE);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                           break;
   }

   task_State_Print  ++;  
}



/*----------------------------------------------------------------------------*/
static   void                   Task_Printer_PrintFooter_3( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {           
      case  0  :  task_Label  =  LABEL_FOOTER_3;                                           break;       // "�� ���� ������"
      case  1  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_DOUBLE);   break;     
      
      case  2  :  task_LineFeed  =  1;                                                     break;
      default  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_DOUBLE);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                           break;
   }

   task_State_Print  ++;  
}


/*----------------------------------------------------------------------------*/
static   void                   Task_Printer_PrintFooter_4( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {           
      case  0  :  task_Label  =  LABEL_FOOTER_4;                                           break;       // "����� ���� ���� �����"
      case  1  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_DOUBLE);   break;     
      
      case  2  :  task_LineFeed  =  1;                                                     break;
      default  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_DOUBLE);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                           break;
   }

   task_State_Print  ++;
}


/*----------------------------------------------------------------------------*/
static   void                 Task_Printer_AdvancePaper( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      //case  0  :  task_LineFeed  =  3;                                                          break;
      case  0  :  task_LineFeed  =  1;                                                          break;
      case  1  :  Epson_FullLine_SameChar( 'x');                                                break;
      //case  2  :  task_LineFeed  =  3;                                                          break;
      case  2  :  task_LineFeed  =  2;                                                          break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintCoinsInTubes( void)
/*----------------------------------------------------------------------------*/
{
ulong       CoinValue;
ulong       TotalValue;


   switch( task_State_Print)
   {
      case  0  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_COINS_IN_TUBES_INTERNAL]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);    break;
      case  1  :  task_LineFeed  =  1;                               break;
      case  2  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_COINS_IN_TUBE_LINE1]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);    break;
      case  3  :  Epson_FullLine_SameChar( '-');
                  task_Index =  HOPPER_A; /* HoperAPI.h */           break;
      case  4  :  task_StrTemp[0]   =  0;
                  CoinValue   =  Task_Hopper_GetCoinValue( (tHopper)task_Index);
                  TotalValue  =  task_pHoppers_Info->MoneyInTube[task_Index];
                  sprintf_P( task_StrTemp, Strings_long[STRING_LONG_9],
                              (TotalValue /  100),
                              (TotalValue %  100),
                              (task_pHoppers_Info->CoinsInTube[task_Index]),
                              (CoinValue  /  100),
                              (CoinValue  %  100),
                              (task_Index + 'A')); // A, B, C, D
                  strcat( task_StrTemp, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_StrTemp, PRINT_ALIGN_RIGHT);

                  if( (++ task_Index) < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/)  task_State_Print  --;
                  else                                task_Index  =  0;
                  break;
      case  5  :  task_LineFeed  =  1;                               break;
      case  6  :  task_Label  =  LABEL_CHANGER_MONEY_IN_TUBES;       break;
      case  7  :  task_LineFeed  =  2;                               break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                     break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintCoinsInCentral( void)
/*----------------------------------------------------------------------------*/
{
static   bool     fFound   =  FALSE;
ulong             CoinValue;
usint             Qtty;
ulong             TotalValue;


   switch( task_State_Print)
   {
      case  0  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_COINS_IN_CENTRAL]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);    break;
      case  1  :  task_LineFeed  =  1;                               break;
      case  2  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_COINS_IN_CENTRAL_LINE1]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);    break;
      case  3  :  Epson_FullLine_SameChar( '-');
                  fFound   =  FALSE;
                  task_Index =  0;                                   break;
      case  4  :  task_StrTemp[0]   =  0;
                  switch( task_Index)
                  {
                     case  0: CoinValue   =  5;       break;
                     case  1: CoinValue   =  10;      break;
                     case  2: CoinValue   =  50;      break;
                     case  3: CoinValue   =  100;     break;
                     case  4: CoinValue   =  200;     break;
                     case  5: CoinValue   =  500;     break;
                     case  6: CoinValue   =  1000;    break;
                     default: CoinValue   =  0;       break;
                  }
                  Qtty              =  task_pCoinsInfo->Erasable.CashBox_Qtty[task_Index];
                  TotalValue        =  (CoinValue * Qtty);

                  if( TotalValue > 0)
                  {
                     fFound   =  TRUE;
                     sprintf_P( task_StrTemp, Strings_long[STRING_LONG_10],
                                 (TotalValue /  100),
                                 (TotalValue %  100),
                                 (Qtty),
                                 (CoinValue  /  100),
                                 (CoinValue  %  100));
                     strcat( task_StrTemp, Strings_short[STRING_SHORT_LF]);
                     Epson_Print( task_StrTemp, PRINT_ALIGN_RIGHT);
                  }

                  if( (++ task_Index) < MAX_COIN_TYPES)
                     task_State_Print  --;
                  else
                     task_Index  =  0;
                  break;
      case  5  :  if( fFound == FALSE)
                     task_Label  =     LABEL_CENTRAL_EMPTY;          break;
      case  6  :  task_LineFeed  =  2;                               break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                     break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintBillsInStacker( void)
/*----------------------------------------------------------------------------*/
{
static   bool     fFound   =  FALSE;
ulong             BillValue;
usint             Qtty;
ulong             TotalValue;


   switch( task_State_Print)
   {
      case  0  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_BILLS_IN_CASHBOX]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);    break;
      case  1  :  task_LineFeed  =  1;                               break;
      case  2  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_BILLS_IN_CASHBOX_LINE1]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);    break;
      case  3  :  Epson_FullLine_SameChar( '-');
                  fFound   =  FALSE;
                  task_Index =  0;                                   break;
      case  4  :  task_StrTemp[0]   =  0;
                  switch( task_Index)
                  {
                     case  0: BillValue   =  2000;    break;
                     case  1: BillValue   =  5000;    break;
                     case  2: BillValue   =  10000;   break;
                     case  3: BillValue   =  20000;   break;
                     default: BillValue   =  0;       break;
                  }
                  Qtty              =  task_pBillsInfo->Erasable.BillsQtty[task_Index];
                  TotalValue        =  (BillValue * Qtty);

                  if( TotalValue > 0)
                  {
                     fFound   =  TRUE;
                     sprintf_P( task_StrTemp, Strings_long[STRING_LONG_11],
                                 (TotalValue /  100),
                                 (TotalValue %  100),
                                 (Qtty),
                                 (BillValue  /  100),
                                 (BillValue  %  100));
                     strcat( task_StrTemp, Strings_short[STRING_SHORT_LF]);
                     Epson_Print( task_StrTemp, PRINT_ALIGN_RIGHT);
                  }

                  if( (++ task_Index) < MAX_BILL_TYPES)
                     task_State_Print  --;
                  else
                     task_Index  =  0;
                  break;
      case  5  :  if( fFound == FALSE)
                     task_Label  =     LABEL_BILLS_CASHBOX_EMPTY;    break;
      case  6  :  task_LineFeed  =  2;                               break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                     break;
   }

   task_State_Print  ++;
}



#if 0 // [
/*----------------------------------------------------------------------------*/
static   void                 Task_Printer_PrintTransCancelled( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  Epson_FullLine_SameChar( '-');             break;
      case  1  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_TRANS_CANCELLED_LINE1]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT); break;
      case  2  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_TRANS_CANCELLED_LINE2]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT); break;
      case  3  :  task_Label  =  LABEL_PRODUCT_CODE;              break;
      case  4  :  Epson_FullLine_SameChar( '-');             break;
      case  5  :  task_LineFeed  =  6;                            break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                  break;
   }

   task_State_Print  ++;
}
#endif // ]



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintFiscalReport( void)
/*----------------------------------------------------------------------------*/
{
   task_Value  =  0;
   task_Type   =  TYPE_VOID;

   switch( task_State_Print)
   {
      case  0  :  task_Label  =  LABEL_MONETARY_REPORTS;                                                                                           break;
      case  1  :  task_LineFeed  =  1;                                                                                                             break;
      case  2  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_MONETARY_REPORT_LEGEND_1]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);                                                                                  break;
      case  3  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_MONETARY_REPORT_LEGEND_2]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);                                                                                  break;
      case  4  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_MONETARY_REPORT_LEGEND_3]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);                                                                                  break;
      case  5  :  task_LineFeed  =  1;                                                                                                             break;
      case  6  :  task_Label  =  LABEL_CHANGER_MONEY_REFILLED;                                                                                     break;
      case  7  :  task_Value  =  task_pHoppers_Info->MoneyRefilled;           task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_ERASABLE;          break;
      case  8  :  task_LineFeed =  1;                                                                                                              break;
      case  9  :  task_Label  =  LABEL_CHANGER_MONEY_DEPOSITED;                                                                                    break;
      case 10  :  task_Value  =  task_pCoinsInfo->Erasable.MoneyDeposited;    task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_ERASABLE;          break;
      case 11  :  task_Value  =  task_pCoinsInfo->NonErasable.MoneyDeposited; task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_NON_ERASABLE;      break;
      case 12  :  task_LineFeed =  1;                                                                                                              break;
      case 13  :  task_Label  =  LABEL_CHANGER_MONEY_TO_CASH_BOX;                                              task_Mode = MODE_WITHOUT_VALUE;     break;
      case 14  :  task_Value  =  task_pCoinsInfo->Erasable.MoneyInCashBox;    task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_ERASABLE;          break;
      case 15  :  task_Value  =  task_pCoinsInfo->NonErasable.MoneyInCashBox; task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_NON_ERASABLE;      break;
      case 16  :  task_LineFeed =  1;                                                                                                              break;
      case 17  :  task_Label  =  LABEL_BV_MONEY_TO_STACKER;                                                    task_Mode = MODE_WITHOUT_VALUE;     break;
      case 18  :  task_Value  =  task_pBillsInfo->Erasable.MoneyDeposited;    task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_ERASABLE;          break;
      case 19  :  task_Value  =  task_pBillsInfo->NonErasable.MoneyDeposited; task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_NON_ERASABLE;      break;
      case 20  :  task_LineFeed =  1;                                                                                                              break;
      case 21  :  task_Label  =  LABEL_BV_BILLS_IN_STACKER;                                                    task_Mode = MODE_WITHOUT_VALUE;     break;
      case 22  :  task_Value  =  task_pBillsInfo->Erasable.BillsInStacker;    task_Type = TYPE_NUMBER_REGULAR; task_Mode = MODE_ERASABLE;          break;
      case 23  :  task_LineFeed =  1;                                                                                                              break;
      case 24  :  task_Label  =  LABEL_CHANGER_CHANGE_BACK;                                                    task_Mode = MODE_WITHOUT_VALUE;     break;
      case 25  :  task_Value  =  task_pCoinsInfo->Erasable.ChangeBack;        task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_ERASABLE;          break;
      case 26  :  task_Value  =  task_pCoinsInfo->NonErasable.ChangeBack;     task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_NON_ERASABLE;      break;
      case 27  :  task_LineFeed =  1;                                                                                                              break;
      case 28  :  task_Label  =  LABEL_CHANGER_EMPTYING;                                                                                           break;
      case 29  :  task_Value  =  task_pHoppers_Info->MoneyEmptied;            task_Type =  TYPE_NUMBER_PRICE;  task_Mode = MODE_ERASABLE;          break;
      case 30  :  task_LineFeed  =  1;                                                                                                             break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                                                                   break;
   }

   task_State_Print  ++;
}



#if 0 // [
/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintTotalSales( void)
/*----------------------------------------------------------------------------*/
{
byte     Idx;


   task_Value  =  0;
   task_Type   =  TYPE_VOID;

   switch( task_State_Print)
   {
      case   0 :  task_Label  =  LABEL_CASH_TOTAL_SALES_QTTY;                                                                                                              break;
      case   1 :  task_Value  =  task_pSalesInfo->Erasable.SalesCount[PAYMENT_TYPE_CASH];             task_Type = TYPE_NUMBER_REGULAR; task_Mode = MODE_ERASABLE;          break;
      case   2 :  task_Value  =  task_pSalesInfo->NonErasable.SalesCount[PAYMENT_TYPE_CASH];          task_Type = TYPE_NUMBER_REGULAR; task_Mode = MODE_NON_ERASABLE;      break;
      case   3 :  task_LineFeed  =  1;                                                                                                                                     break;
      case   4 :  task_Label  =  LABEL_CASH_TOTAL_SALES_VALUE;                                                                                                             break;
      case   5 :  task_Value  =  task_pSalesInfo->Erasable.SalesValue[PAYMENT_TYPE_CASH];             task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_ERASABLE;          break;
      case   6 :  task_Value  =  task_pSalesInfo->NonErasable.SalesValue[PAYMENT_TYPE_CASH];          task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_NON_ERASABLE;      break;
      case   7 :  task_LineFeed  =  1;                                                                                                                                     break;
      case   8 :  task_Label  =  LABEL_CC_TOTAL_SALES_QTTY;                                                                                                                break;
      case   9 :  task_Value  =  task_pSalesInfo->Erasable.SalesCount[PAYMENT_TYPE_CREDIT_CARD];      task_Type = TYPE_NUMBER_REGULAR; task_Mode = MODE_ERASABLE;          break;
      case  10 :  task_Value  =  task_pSalesInfo->NonErasable.SalesCount[PAYMENT_TYPE_CREDIT_CARD];   task_Type = TYPE_NUMBER_REGULAR; task_Mode = MODE_NON_ERASABLE;      break;
      case  11 :  task_LineFeed  =  1;                                                                                                                                     break;
      case  12 :  task_Label  =  LABEL_CC_TOTAL_SALES_VALUE;                                                                                                               break;
      case  13 :  task_Value  =  task_pSalesInfo->Erasable.SalesValue[PAYMENT_TYPE_CREDIT_CARD];      task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_ERASABLE;          break;
      case  14 :  task_Value  =  task_pSalesInfo->NonErasable.SalesValue[PAYMENT_TYPE_CREDIT_CARD];   task_Type = TYPE_NUMBER_PRICE;   task_Mode = MODE_NON_ERASABLE;      break;
      case  15 :  task_LineFeed  =  1;                                                                                                                                     break;

      case  16 :  task_Label  =  LABEL_TOTAL_SALES_QTTY;                            break;
      case  17 :  for( Idx = 0, task_Value = 0; Idx < MAX_PAYMENT_TYPES; Idx ++)
                     task_Value  += task_pSalesInfo->Erasable.SalesCount[Idx];
                  task_Type = TYPE_NUMBER_REGULAR; task_Mode = MODE_ERASABLE;       break;
      case  18 :  for( Idx = 0, task_Value = 0; Idx < MAX_PAYMENT_TYPES; Idx ++)
                     task_Value  += task_pSalesInfo->NonErasable.SalesCount[Idx];
                  task_Type = TYPE_NUMBER_REGULAR; task_Mode = MODE_NON_ERASABLE;   break;
      case  19 :  task_LineFeed  =  1;                                              break;

      case  20 :  task_Label  =  LABEL_TOTAL_SALES_VALUE;                           break;
      case  21 :  for( Idx = 0, task_Value = 0; Idx < MAX_PAYMENT_TYPES; Idx ++)
                     task_Value  += task_pSalesInfo->Erasable.SalesValue[Idx];
                  task_Type = TYPE_NUMBER_PRICE; task_Mode = MODE_ERASABLE;         break;
      case  22 :  for( Idx = 0, task_Value = 0; Idx < MAX_PAYMENT_TYPES; Idx ++)
                     task_Value  += task_pSalesInfo->NonErasable.SalesValue[Idx];
                  task_Type = TYPE_NUMBER_PRICE; task_Mode = MODE_NON_ERASABLE;     break;
      case  23 :  task_LineFeed  =  1;                                              break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                    break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintDeviceStatus( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case  0  :  task_Index  =  0;                                                             break;
      case  1  :  task_LineFeed  =  ( (task_PrintType > 0) ? 1 : 6); /* don't break */       // since this is a short report
                  /* if there is more to print - print only 1 CR; otherwise - print 6 CR */  // print extra CR to extend the
                  /* read the comment on the right to understand why */                      // printout and ease grabbing the paper
                  if( task_fTestPrinter == TRUE)
                  {
                     task_LineFeed     =  0;
                     task_CutterMode   =  PRINT_CUTTER_HALF;
                  }
                  break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintEvents( void)
/*----------------------------------------------------------------------------*/
{
static   bool  fReadFirstRec  =  TRUE;


   switch( task_State_Print)
   {
      case  0  :  fReadFirstRec  =  TRUE;
                  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  1  :  if( Task_Log_Event_ReadRecord( &task_Log_Event_Rec, fReadFirstRec) == FALSE)  break;
                  fReadFirstRec  =  FALSE;

                  strcpy_P( task_StrTemp, LogEvents[task_Log_Event_Rec.EvCode]);
                  Util_SwapString( task_StrTemp);

                  if( task_Log_Event_Rec.fValueIsPrice == TRUE)
                     sprintf_P( task_Str,   Strings_long[STRING_LONG_5],
                                    task_Log_Event_Rec.Value / 100,
                                    task_Log_Event_Rec.Value % 100,
                                    task_StrTemp,
                                    ClockAPI_SecondsToStr( task_Log_Event_Rec.TimeStamp));
                  else
                     sprintf_P( task_Str,   Strings_long[STRING_LONG_6],
                                    task_Log_Event_Rec.Value,
                                    task_StrTemp,
                                    ClockAPI_SecondsToStr( task_Log_Event_Rec.TimeStamp));

                  Epson_VoidHebrew();
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);
                  return;
      case  2  :  task_LineFeed  =  1; break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Printer_PrintTransactions( void)
/*----------------------------------------------------------------------------*/
{
static   bool  fReadFirstRec     =  TRUE;
static   ulong Paid;
static   ulong MoneyBack;


   switch( task_State_Print)
   {
      case  0  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_NORMAL);   break;
      case  1  :  strcpy_P( task_Str, HeadLines_A[HEADLINE_A_TRANS_CASH_HEADLINE]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);                               break;
      case  2  :  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  3  :  strcpy_P( task_Str, HeadLines_B[HEADLINE_B_SALES_LINE1]);
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);                               break;
      case  4  :  Epson_FullLine_SameChar( '-');
                  fReadFirstRec  =  TRUE;
                  task_Index     =  0;
                  Paid           =  0;
                  MoneyBack      =  0;
                                                                                                break;
      case  5  :  if( Task_Log_Trans_ReadRecord( &task_Log_Trans_Rec, fReadFirstRec) == FALSE)  break;
                  fReadFirstRec  =  FALSE;
                  if( task_Log_Trans_Rec.PaymentType != PAYMENT_TYPE_CASH)                      return;
                  sprintf_P( task_Str, Strings_long[STRING_LONG_10],
                                 (task_Log_Trans_Rec.Status == MONEY_BACK_REASON_CANCEL) ? 'X' : ' ',   // 1 = Cancelled; 0 = Complete
                                 (task_Log_Trans_Rec.MoneyBack /  100),
                                 (task_Log_Trans_Rec.MoneyBack %  100),
                                 (task_Log_Trans_Rec.Paid      /  100),
                                 (task_Log_Trans_Rec.Paid      %  100),
                                 (task_Log_Trans_Rec.Value     /  100),
                                 (task_Log_Trans_Rec.Value     %  100),
                                 //(task_Log_Trans_Rec.VoucherNumber),
                                 ClockAPI_SecondsToStr( task_Log_Trans_Rec.TimeStamp));
                  Paid        += task_Log_Trans_Rec.Paid;
                  MoneyBack   += task_Log_Trans_Rec.MoneyBack;
                  strcat( task_Str, Strings_short[STRING_SHORT_LF]);
                  Epson_VoidHebrew();
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);                               return;
      case  6  :  task_LineFeed  =  2;                                                          break;
      case  7  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_NORMAL);   break;
      case  8  :  task_Total  =  (Paid >= MoneyBack) ? (Paid - MoneyBack) : 0;
                  task_Label  =  LABEL_TOTAL;                                                   break;
      case  9  :  task_LineFeed  =  2;                                                          break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Printer_PrintCoinRouting( void)
/*----------------------------------------------------------------------------*/
{
static   bool  fReadFirstRec  =  TRUE;

   switch( task_State_Print)
   {
      case  0  :  fReadFirstRec  =  TRUE;
                  Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  1  :  if( Task_Log_CoinRouting_ReadRecord( &task_Log_CoinRouting_Rec, fReadFirstRec) == FALSE)   break;
                  fReadFirstRec  =  FALSE;

                  sprintf_P( task_Str,   Strings_long[STRING_LONG_12],
                                 task_Log_CoinRouting_Rec.RoutedTo   ,
                                 task_Log_CoinRouting_Rec.Value / 100,
                                 task_Log_CoinRouting_Rec.Value % 100,
                                 ClockAPI_SecondsToStr( task_Log_CoinRouting_Rec.TimeStamp));

                  Epson_VoidHebrew();
                  Epson_Print( task_Str, PRINT_ALIGN_RIGHT);
                  return;
      case  2  :  task_LineFeed  =  1; break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintSetupInfo( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_State_Print)
   {
      case   0 :  task_LineFeed  =  1;                                                       break;
      case   1 :  task_Label  =  LABEL_CHANGE_LIMIT;                                         break;
      case   2 :  task_Label  =  LABEL_LANGUAGE;                                             break;
      case   3 :  task_Label  =  LABEL_MACHINE_ID;                                           break;
      case   4 :  task_Label  =  LABEL_CC_TERMINAL_NUMBER;                                   break;
      case   5 :  task_Label  =  LABEL_CC_J_PARAM;                                           break;
      case   6 :  task_Label  =  LABEL_CC_X_PARAM;                                           break;
      case   7 :  task_Label  =  LABEL_CC_SERVER_TIMEOUT;                                    break;
      case   8 :  task_Label  =  LABEL_CC_MIN_TRANSACTION_VALUE;                             break;
      case   9 :  task_Label  =  LABEL_CC_FUNCTION_STATUS;                                   break;
      case  10 :  task_Label  =  LABEL_CC_AUTO_RECEIPT;                                      break;
      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                             break;
   }

   task_State_Print  ++;
}
#endif // ]


/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintDoorOpenClose( void)
/*----------------------------------------------------------------------------*/
{
   task_Value  =  0;
   task_Type   =  TYPE_VOID;

   switch( task_State_Print)
   {
      case  0  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   break;
      case  1  :  task_Label  =  LABEL_CHANGER_MONEY_IN_TUBES;                                  break;
      case  2  :  task_Label  =  LABEL_CHANGER_MONEY_TO_CASH_BOX;                               break;
      case  3  :  task_Label  =  LABEL_BV_MONEY_TO_STACKER;                                     break;
      case  4  :  task_LineFeed =  1;                                                           break;

      default  :  Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);   // don't 'break'
                  task_State  =  STATE_PRINTING;                                                break;
   }

   task_State_Print  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintHeader( tHeader   Header)
/*----------------------------------------------------------------------------*/
{
   if( Header < HEADER_LAST)
   {
      strcpy_P( task_StrTemp, Headers[Header]);
      if( (strlen( task_StrTemp) % 2) == 0)
         strcat( task_StrTemp, " ");
      task_Str[0] =  0;
   }

   strcat( task_Str, task_StrTemp);
   strcat( task_Str, Strings_short[STRING_SHORT_LF]);
   Epson_Print( task_Str, PRINT_ALIGN_CENTER);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintString( tLabel Label, byte Index)
/*----------------------------------------------------------------------------*/
{
byte     Alignment;
bool     fSwapString;
bool     fVoidHebrew;


   memset( task_Str, 0, sizeof( task_Str));
   memset( task_StrTemp, 0, sizeof( task_StrTemp));

   if( Label < LABEL_LAST)
   {
      if( task_fDontPrintLabel == FALSE)
         strcpy_P( task_StrTemp, Labels[Label]);
      task_Str[0] =  0;
   }

   Alignment   =  PRINT_ALIGN_RIGHT;
   fSwapString =  FALSE;
   fVoidHebrew =  FALSE;

   switch( Label)
   {
      case  LABEL_LICENSED_DEALER            :
            Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_LICENSED_NUMBER);
            if( task_Param_C_Rec == 0)
               return;
            Task_Printer_SetNumber( &task_Param_C_Rec, sizeof( task_Param_C_Rec));
            Alignment   =  PRINT_ALIGN_CENTER;
            break;

      case  LABEL_PRIVATE_COMPANY_NUMBER     :
            Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_COMPANY_NUMBER);
            if( task_Param_C_Rec == 0)
               return;
            Task_Printer_SetNumber( &task_Param_C_Rec, sizeof( task_Param_C_Rec));
            Alignment   =  PRINT_ALIGN_CENTER;
            break;

      case  LABEL_BRANCH_NUMBER              :
            Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_SITE_CODE);
            if( task_Param_C_Rec == 0)
               return;
            Task_Printer_SetNumber( &task_Param_C_Rec, sizeof( task_Param_C_Rec));
            break;

      case  LABEL_SITE_NAME                  :
            /*
            Task_Params_Read_String( PARAM_TYPE_D, task_Param_Str16_Rec, sizeof( task_Param_Str16_Rec), STR_D_SITE_NAME);
            if( task_Param_Str16_Rec[0] == 0)
               return;
            Util_StrCpy( task_Str, task_Param_Str16_Rec, sizeof( task_Param_Str16_Rec));
            fSwapString =  TRUE;
            */
            break;

      case  LABEL_ZONE_NAME                  :
            /*
            Task_Params_Read_String( PARAM_TYPE_D, task_Param_Str16_Rec, sizeof( task_Param_Str16_Rec), STR_D_ZONE_NAME);
            if( task_Param_Str16_Rec[0] == 0)
               return;
            Util_StrCpy( task_Str, task_Param_Str16_Rec, sizeof( task_Param_Str16_Rec));
            fSwapString =  TRUE;
            */
            break;

      case  LABEL_MACHINE_NAME               :
            /*
            Task_Params_Read_String( PARAM_TYPE_D, task_Param_Str16_Rec, sizeof( task_Param_Str16_Rec), STR_D_MACHINE_NAME);
            if( task_Param_Str16_Rec[0] == 0)
               return;
            Util_StrCpy( task_Str, task_Param_Str16_Rec, sizeof( task_Param_Str16_Rec));
            fSwapString =  TRUE;
            */
            break;

      case  LABEL_MACHINE_NUMBER             :
            Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_MACHINE_NUMBER);
            if( task_Param_C_Rec == 0)
            {
               return;
            }
            Task_Printer_SetNumber( &task_Param_C_Rec, sizeof( task_Param_C_Rec));
            break;

      case  LABEL_BRANCH_PHONE_NUMBER        :
            /*
            Task_Params_Read( task_Param_B, PARAM_B_BRANCH_PHONE_NUMBER);
            if( task_Param_B[0] == 0)
               return;
            Util_StrCpy( task_Str, task_Param_B, sizeof( task_Param_B));
            Alignment   =  PRINT_ALIGN_CENTER;
            */
            break;

      case  LABEL_RECEIPT_NUMBER             :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_RECEIPT_NUMBER);
            Task_Printer_SetNumber( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_DATE                       :
            strcpy( task_Str, ClockAPI_GetDateStr( DATE_FORMAT_DDMMYY, DATE_DELIMITER_SLASH));
            break;

      case  LABEL_TIME                       :
            strcpy( task_Str, ClockAPI_GetTimeStr( TIME_FORMAT_HHMMSS, TIME_DELIMITER_COLON));
            break;

      case  LABEL_PRODUCT_CODE_WIDE          :
      case  LABEL_PRODUCT_CODE               :
            //Task_Printer_SetNumber( &task_pReceiptInfo->VoucherNumber, sizeof( task_pReceiptInfo->VoucherNumber));
            break;

      case  LABEL_PRODUCT                    :
            task_Product   =  *(Cmd_L_Data_Product *)&task_Param_Var;
            if( task_Product.Amount == 0)
               return;

            Util_SwapArray( task_Param_E_Rec, strlen( task_Param_E_Rec));
            sprintf( task_Str, Strings_short[STRING_SHORT_3], task_Param_E_Rec, task_Product.Amount);
            fVoidHebrew =  TRUE;
            break;

      case  LABEL_TRANSACTION_VALUE          :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_TRANS_TOTAL_VALUE);
            Task_Printer_SetPrice( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_AMOUNT_RECEIVED            :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_AMOUNT_RECEIVED);
            Task_Printer_SetPrice( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_PAID_IN_CASH               :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CASH_AMOUNT_RECEIVED);
            Task_Printer_SetPrice( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_CHANGE                     :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CHANGE);
            Task_Printer_SetPrice( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_PAID_IN_CC                 :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_AMOUNT_RECEIVED);
            Task_Printer_SetPrice( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_CC_CARD_NUMBER             :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_LAST_4_DIGITS);
            Task_Printer_SetNumber( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_CC_TRANS_NUMBER            :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_CONFIRMATION_CODE);
            Task_Printer_SetNumber( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_CC_AUTH_CODE               :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_AUTHORIZATION_CODE);
            Task_Printer_SetNumber( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_RESIDENT_DISCOUNT_TOTAL    :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_RESIDENT_DISCOUNT_TOTAL);
            Task_Printer_SetPrice( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_FOOTER_1                   :
            Alignment   =  PRINT_ALIGN_CENTER;
            break;

      case  LABEL_FOOTER_2                   :
            Alignment   =  PRINT_ALIGN_CENTER;
            break;

      case  LABEL_FOOTER_3                   :
            Alignment   =  PRINT_ALIGN_CENTER;
            break;

      case  LABEL_FOOTER_4                   :
            Alignment   =  PRINT_ALIGN_CENTER;
            break;
            
      case  LABEL_REPORT_Z_NUMBER            :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);
            Task_Printer_SetNumber( &task_Param_Var, sizeof( task_Param_Var));
            break;

      case  LABEL_DAILY_COUNTER              :
            return;
            /*
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_DAILY_COUNTER);
            if( task_Param_Var == 0)
               return;
            Task_Printer_SetNumber( &task_Param_Var, sizeof( task_Param_Var));
            */
            break;

      case  LABEL_REPORT_NUMERATOR           :
            Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_REPORT_NUMERATOR_1);
            if( task_Param_Var == 0)   task_Param_Var =  1;
            Task_Printer_SetNumber( &task_Param_Var, sizeof( task_Param_Var));
            task_Param_Var ++;
            Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_REPORT_NUMERATOR_1);
            break;

      case  LABEL_LAST_REPORT_DATE_AND_TIME  :
            //if( task_Info.LastReport_DateAndTime > 0)
            //   strcpy( task_Str, ClockAPI_SecondsToStr( task_Info.LastReport_DateAndTime));
            break;

      case  LABEL_CHANGER_MONEY_IN_TUBES     :
            Task_Printer_SetPrice( &task_pHoppers_Info->MoneyInTubes, sizeof( task_pHoppers_Info->MoneyInTubes));
            break;

      case  LABEL_CHANGER_MONEY_TO_CASH_BOX  :
            if( task_Mode == MODE_WITHOUT_VALUE)
               break;
            Task_Printer_SetPrice( &task_pCoinsInfo->Erasable.MoneyInCashBox, sizeof( task_pCoinsInfo->Erasable.MoneyInCashBox));
            break;

      case  LABEL_BV_MONEY_TO_STACKER        :
            if( task_Mode == MODE_WITHOUT_VALUE)
               break;
            Task_Printer_SetPrice( &task_pBillsInfo->Erasable.MoneyDeposited, sizeof( task_pBillsInfo->Erasable.MoneyDeposited));
            break;

      case  LABEL_BV_BILLS_IN_STACKER        :
            if( task_Mode == MODE_WITHOUT_VALUE)
               break;
            Task_Printer_SetNumber( &task_pBillsInfo->Erasable.BillsInStacker, sizeof( task_pBillsInfo->Erasable.BillsInStacker));
            break;

      case  LABEL_MACHINE_ID                 :
            //Task_Params_Read( &task_Param_A, PARAM_A_MACHINE_ADDRESS);
            //Task_Printer_SetNumber( &task_Param_A, sizeof( task_Param_A));
            break;

      case  LABEL_CC_TERMINAL_NUMBER         :
            //Task_Params_Read( &task_Param_A, PARAM_A_CC_TERMINAL_NUMBER);
            //Task_Printer_SetNumber( &task_Param_A, sizeof( task_Param_A));
            break;

      case  LABEL_CC_J_PARAM                 :
            //Task_Params_Read( &task_Param_A, PARAM_A_CC_J_PARAM);
            //Task_Printer_SetNumber( &task_Param_A, sizeof( task_Param_A));
            break;

      case  LABEL_CC_X_PARAM                 :
            //Task_Params_Read( &task_Param_A, PARAM_A_CC_X_PARAM);
            //Task_Printer_SetNumber( &task_Param_A, sizeof( task_Param_A));
            break;

      case  LABEL_CC_SERVER_TIMEOUT          :
            //Task_Params_Read( &task_Param_A, PARAM_A_CC_SERVER_TIMEOUT);
            //Task_Printer_SetNumber( &task_Param_A, sizeof( task_Param_A));
            break;

      case  LABEL_CC_MIN_TRANSACTION_VALUE   :
            //Task_Params_Read( &task_Param_A, PARAM_A_CC_MIN_TRANSACTION_VALUE);
            //task_Param_A   *= 100;
            //Task_Printer_SetPrice( &task_Param_A, sizeof( task_Param_A));
            //task_Param_A   /= 100;
            break;

      case  LABEL_CC_FUNCTION_STATUS         :
            //Task_Params_Read( &task_Param_A, PARAM_A_CC_FUNCTION_STATUS);
            //strcpy_P( task_Str,  (task_Param_A == TRUE) ? Values[VALUE_BLOCKED] : Values[VALUE_UNBLOCKED]);
            //fSwapString =  TRUE;
            break;

      case  LABEL_CC_AUTO_RECEIPT            :
            //Task_Params_Read( &task_Param_A, PARAM_A_CC_AUTO_RECEIPT);
            //strcpy_P( task_Str,  (task_Param_A == TRUE) ? Values[VALUE_ENABLED] : Values[VALUE_DISABLED]);
            //fSwapString =  TRUE;
            break;

      case  LABEL_BILL                       :
            /*
            {
            char  *pStr;

               strcpy_P( task_Str,  (task_pSetupInfo->fBlockedBills[Index] == TRUE) ? Values[VALUE_BLOCKED] : Values[VALUE_UNBLOCKED]);

               if( Util_IsStringHebrew( task_Str) == TRUE)
                  Util_SwapString( task_Str);

               pStr  =  strchr( task_Str, 0);
               sprintf_P( pStr, Strings_long[STRING_LONG_8],
                                 (task_pSetupInfo->Bills[Index] / 100),
                                 (task_pSetupInfo->Bills[Index] % 100));
            }
            */
            break;

      case  LABEL_COIN                       :
            /*
            {
            char  *pStr;

               strcpy_P( task_Str,  (task_pSetupInfo->fBlockedCoins[Index] == TRUE) ? Values[VALUE_BLOCKED] : Values[VALUE_UNBLOCKED]);

               if( Util_IsStringHebrew( task_Str) == TRUE)
                  Util_SwapString( task_Str);

               pStr  =  strchr( task_Str, 0);
               sprintf_P( pStr, Strings_long[STRING_LONG_8],
                                 (task_pSetupInfo->Coins[Index] / 100),
                                 (task_pSetupInfo->Coins[Index] % 100));
            }
            */
            break;

      case  LABEL_LANGUAGE                   :
            //Task_Params_Read( &task_Param_A, PARAM_A_LANGUAGE);
            //switch( task_Param_A)
            //{
            //   case  LANGUAGE_ENGLISH        :  strcpy_P( task_Str, Values[VALUE_LANGUAGE_ENGLISH]);  break;
            //   case  LANGUAGE_HEBREW         :  strcpy_P( task_Str, Values[VALUE_LANGUAGE_HEBREW]);   break;
            //}
            //
            //fSwapString =  TRUE;
            break;

      case  LABEL_CHANGE_LIMIT               :
            //Task_Params_Read( &task_Param_A, PARAM_A_CHANGE_LIMIT);
            //task_Param_A   *= 100;
            //Task_Printer_SetPrice( &task_Param_A, sizeof( task_Param_A));
            //task_Param_A   /= 100;
            break;

      case  LABEL_TOTAL                      :
            Task_Printer_SetPrice( &task_Total, sizeof( task_Total));
            break;

      case  LABEL_CENTRAL_EMPTY:
      case  LABEL_BILLS_CASHBOX_EMPTY:
            break;

      default:
            break;
   }

   if( (fSwapString == TRUE) && (fVoidHebrew == FALSE))
   {
      if( Util_IsStringHebrew( task_Str) == TRUE)
         Util_SwapString( task_Str);
   }

   if( (Util_IsStringHebrew( task_Str) == TRUE) && (fVoidHebrew == FALSE))
   {
      Util_SwapString( task_Str);
      strcat( task_StrTemp, task_Str);
      strcat( task_StrTemp, Strings_short[STRING_SHORT_LF]);
      Epson_Print( task_StrTemp, Alignment);
   }
   else
   {
      strcat( task_Str, task_StrTemp);
      strcat( task_Str, Strings_short[STRING_SHORT_LF]);
      if( fVoidHebrew == TRUE)
         Epson_VoidHebrew();
      Epson_Print( task_Str, Alignment);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_PrintNumber( void  *pNumber, byte  Size, byte   Type, byte   Mode)
/*----------------------------------------------------------------------------*/
{
   switch( Type)
   {
      case  TYPE_NUMBER_REGULAR: Task_Printer_SetNumber( pNumber, Size);   break;
      case  TYPE_NUMBER_PRICE:   Task_Printer_SetPrice( pNumber, Size);    break;
   }

   switch( Mode)
   {
      case  MODE_SPACE        :  strcat( task_Str, "  ");   break;
      case  MODE_ERASABLE     :  strcat( task_Str, " �");   break;
      case  MODE_NON_ERASABLE :  strcat( task_Str, " �");   break;
      case  MODE_CURRENT      :  strcat( task_Str, " �");   break;
   }

   strcat( task_Str, Strings_short[STRING_SHORT_LF]);
   Epson_Print( task_Str, PRINT_ALIGN_RIGHT);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_SetNumber( void  *pNumber, byte  Size)
/*----------------------------------------------------------------------------*/
{
   if( (pNumber == NULL) || (Size == 0))
      return;

   switch( Size)
   {
      case  1: // byte
            sprintf( task_Str, Strings_short[STRING_SHORT_1], (*(byte *)pNumber));
            break;

      case  2: // usint
            sprintf( task_Str, Strings_short[STRING_SHORT_1], (*(usint *)pNumber));
            break;

      case  4: // ulong
            sprintf( task_Str, "%lu", (*(ulong *)pNumber));
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Printer_SetPrice( void  *pPrice, byte  Size)
/*----------------------------------------------------------------------------*/
{
   if( (pPrice == NULL) || (Size == 0))
      return;

   switch( Size)
   {
      case  1: // byte
            sprintf( task_Str, Strings_short[STRING_SHORT_2], ((*(byte *)pPrice) / 100), ((*(byte *)pPrice) % 100));
            break;

      case  2: // usint
            sprintf( task_Str, Strings_short[STRING_SHORT_2], ((*(usint *)pPrice) / 100), ((*(usint *)pPrice) % 100));
            break;

      case  4: // ulong
            sprintf( task_Str, "%lu.%.2lu", ((*(ulong *)pPrice) / 100), ((*(ulong *)pPrice) % 100));
            break;
   }
}



/*----------------------------------------------------------------------------*/
char* Task_Printer_GetSiteName(byte SiteIndex)
/*----------------------------------------------------------------------------*/
{
   byte idx;
   for(idx=0; idx<(SITE_NAME_LENGTH-1); idx++)
   {
      SiteNameBuff[idx] = SiteName[SiteIndex][idx];
   }
   return (char*)SiteNameBuff;
}

