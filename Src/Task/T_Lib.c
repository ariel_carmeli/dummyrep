/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
tT_Lib_Error    T_Lib_ErrorInfo;


/*---------------------------- Typedef Directives ----------------------------*/



/*---------------------------- External Variables ----------------------------*/



/*----------------------------- Global Variables -----------------------------*/
CONST   Lib_GetError    task_Lib_GetError_Func_Array[] = 
{
   Epson_GetError,              // 0
   Led_GetError,                // 1
   Buzz_GetError,               // 2
   ClockAPI_GetError,           // 3
   InOutAPI_GetError,           // 4
   Util_GetError,               // 5
   LcdAPI_GetError,             // 6
   KeyboardAPI_GetError,           // 7
   Hopper_GetError,             // 8
   
   /*         Last entry        */
   NULL                         //
};


// For debug purpose, use Libraries name text
#define T_LIB_LIB_NAME_MAX 15
CONST char  task_Lib_NameOfLib[][T_LIB_LIB_NAME_MAX] =
{
   "Epson Error:  ",            // 0
   "Led Error:    ",            // 1
   "Buzz Error:   ",            // 2
   "Clock Error:  ",            // 3
   "InOut Error:  ",            // 4
   "Util Error:   ",            // 5
   "LCD Error:    ",            // 6
   "KBD Error:    ",            // 7
   "Hopper Error  ",            // 8
   
   /*         Last entry        */
  "last error   "
};


/*----------------------------------------------------------------------------*/



/*------------------------ Local Function Prototypes -------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Lib_Init( void)
/*----------------------------------------------------------------------------*/
{


   
   

}




#define T_LIB_MAIN_SLOWER 0
/*----------------------------------------------------------------------------*/
void                    		Task_Lib_Main( void )
/*----------------------------------------------------------------------------*/
{
   static byte task_Lib_GetError_Index = 0;
   byte error;

   //==================== For debug, increase time between access
#if T_LIB_MAIN_SLOWER
   // Increase the gap time between ERRORS CHECKS 
   static       tTimerMsec OldTimer     = 0;
   tTimerMsec   NewTimer	        = 0;
   byte         Gap;
   
   NewTimer = Task_Timer_GetTimer();
   if( OldTimer	!= NewTimer)
   {
      Gap = (NewTimer - OldTimer);
      
      if (Gap > 100)
      {
         OldTimer = NewTimer;
#endif
   //====================

         task_Lib_GetError_Index = 0;
         while ( task_Lib_GetError_Func_Array[task_Lib_GetError_Index] != NULL )
         {        
            error = task_Lib_GetError_Func_Array[task_Lib_GetError_Index]();
            if (error > 0) 
            {
               SET_BIT( App_TaskEvent[TASK_LIB], EV_APP_LIB_ERROR );

               T_Lib_ErrorInfo.LibraryCode      = (tT_LIB_LIB_CODE)(task_Lib_GetError_Index);
               T_Lib_ErrorInfo.ErrorCode        = error;
               
               Debug_StrPrint_Const(&task_Lib_NameOfLib[task_Lib_GetError_Index][0]);

            }
            task_Lib_GetError_Index++;
         }

         
         
         
   //====================
#if T_LIB_MAIN_SLOWER
      } //if (Gap > 100)
   } // if( OldTimer != NewTimer)
#endif
   //====================
}


/*----------------------------------------------------------------------------*/
void                    Task_Lib_GetLastError(byte* pLibCode, byte* pErrorCode )
/*----------------------------------------------------------------------------*/
{
  
   *pLibCode    = T_Lib_ErrorInfo.LibraryCode;
   *pErrorCode  = T_Lib_ErrorInfo.ErrorCode;
    
}





#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
//static	void						Task_Lib_
/*----------------------------------------------------------------------------*/


