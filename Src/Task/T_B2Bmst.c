/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  MAX_COMMANDS                  5
#define  MAX_SLAVES                    10
/*---------------------------- Typedef Directives ----------------------------*/
typedef  struct
{
   byte        Type;
   byte        Address;
   bool        fActive;
}  tSlaves;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
#define  B2B_MAX_LEN_TX_BUFFER     60 //55
static   tB2BPacket           task_TxPacket;
static   tB2BPacket          *task_pRxPacket;
static   tB2B_FromMaster      task_TxData;
static   tB2B_FromSlave       task_RxData;
static   tB2B_Command         task_Command;
static   tSlaves              task_Slaves[MAX_SLAVES];
static   tTimerMsec           task_RxTimer;
static   byte                 task_TxBuffer[B2B_MAX_LEN_TX_BUFFER];
static   bool                 task_fSendCommand;
static   bool                 task_fTxActive;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						Task_B2B_Master_HandleTimer( void);
static	void						Task_B2B_Master_HandleExtEvent( void);
static	void						Task_B2B_Master_HandleTxMsg( void);
static	void						Task_B2B_Master_HandleRxMsg( void);
static	bool						Task_B2B_Master_IsMsgForMe( void);
static	void						task_B2B_Master_SetSlaveStatus( byte   Type, byte  Address, bool  Mode);
/*----------------------------------------------------------------------------*/
#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_B2B_Master_Init( void)
/*----------------------------------------------------------------------------*/
{
   task_pRxPacket =  B2BAPI_GetData();

   B2BAPI_Init( EXAR_A);
}



/*----------------------------------------------------------------------------*/
void                    		Task_B2B_Master_Main( void)
/*----------------------------------------------------------------------------*/
{
	if( App_TaskStatus[TASK_B2B] == DISABLED)
		return;

   B2BAPI_Main();

   Task_B2B_Master_HandleTimer();
   Task_B2B_Master_HandleExtEvent();
   Task_B2B_Master_HandleRxMsg();
   Task_B2B_Master_HandleTxMsg();
}



/*----------------------------------------------------------------------------*/
bool                    		Task_B2B_Master_IsClearToSend( void)
/*----------------------------------------------------------------------------*/
{
   return( (task_fSendCommand == FALSE) && (task_RxTimer == 0));
}



/*----------------------------------------------------------------------------*/
tB2B_FromMaster              *Task_B2B_GetTxDataPtr( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_TxData);
}



/*----------------------------------------------------------------------------*/
tB2B_FromSlave               *Task_B2B_GetRxDataPtr( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_RxData);
}



/*----------------------------------------------------------------------------*/
void                          Task_B2B_SendCommand( tB2B_Command  *pCommand, tB2B_FromMaster  *pData)
/*----------------------------------------------------------------------------*/
{
   if( (pCommand == NULL) || (pData == NULL))
      return;

   task_Command      =  *pCommand;
   task_TxData       =  *pData;
   task_fSendCommand =  TRUE;
}



/*----------------------------------------------------------------------------*/
bool                    		Task_B2B_IsSlaveActive( byte Type, byte  Address)
/*----------------------------------------------------------------------------*/
{
byte     Index;


   // Broadcast messages are treated as if remote Slaves are active
   //if( (task_Slaves[Index].Type == 0) || (task_Slaves[Index].Address == 0))
   //   return( ACTIVE);

   for( Index = 0; Index < MAX_SLAVES; Index ++)
   {
      if( (task_Slaves[Index].Type == Type) && (task_Slaves[Index].Address == Address))
         return( task_Slaves[Index].fActive);
   }

   return( ACTIVE);
}


/*----------------------------------------------------------------------------*/
bool                    		Task_B2B_IsSlaveCoinActive(void)
/*----------------------------------------------------------------------------*/
{
   if ( task_RxData.Coin_Valid_Status > 0 )
      return TRUE;
   else
      return FALSE;
}


/*----------------------------------------------------------------------------*/
bool                    		Task_B2B_IsSlaveBillActive(void)
/*----------------------------------------------------------------------------*/
{
   if ( task_RxData.Bill_Valid_Status.VendorCode > 0 )
      return TRUE;
   else
      return FALSE;
}

#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_B2B_Master_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( task_RxTimer  <  Gap)  task_RxTimer   =  0; else  task_RxTimer   -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_B2B_Master_HandleExtEvent( void)
/*----------------------------------------------------------------------------*/
{
   App_TaskEvent[TASK_B2B] =  EV_APP_VOID;
}



/*----------------------------------------------------------------------------*/
static	void						Task_B2B_Master_HandleTxMsg( void)
/*----------------------------------------------------------------------------*/
{
   if( (task_fTxActive == TRUE) && (task_RxTimer == 0))
   {
      task_fTxActive =  FALSE;
      task_B2B_Master_SetSlaveStatus( task_TxPacket.UnitType, task_TxPacket.Address, INACTIVE);
   }

   if( (B2BAPI_IsBusy() == TRUE) || (task_RxTimer > 0))
      return;

   if( task_fSendCommand == TRUE)
   {
      task_fSendCommand          =  FALSE;
      task_TxPacket.UnitType     =  task_Command.UnitType;
      task_TxPacket.Address      =  task_Command.Address;
      task_TxPacket.Command      =  task_Command.Command;
      task_TxPacket.Sub_Command  =  task_Command.Sub_Command;
      task_TxPacket.Index        =  task_Command.Index;
   }
   else
   {
      task_TxPacket.UnitType     =  0; // By default, Target Type is undefined
      task_TxPacket.Address      =  1; // By default, Target Address is 1 (0 = Broadcast)
      task_TxPacket.Command      =  B2B_CMD_GET_STATUS;
      task_TxPacket.Sub_Command  =  0;
      task_TxPacket.Index        =  0;
   }

   task_TxPacket.fFromMaster  =  TRUE;   // Msg sender is 'Master'
   task_TxPacket.pBuffer      =  task_TxBuffer;
   task_TxPacket.BufSize      =  0; // default value

   switch( task_TxPacket.Command)
   {
      /*
      Master unit never sends either ACK or NACK commands

      case  B2B_CMD_ACK            	   :  // 'A'
            break;

      case  B2B_CMD_NACK           	   :  // 'N'
            break;
      */

      case  B2B_CMD_BAR_CODE_READER    :  // 'B'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_BRC_SET_MODE             :  // 1 - Set Operation Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.BC_Reader_SetOperationMode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.BC_Reader_SetOperationMode, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_BRC_GET_BARCODE_LABEL    :  // 2 - Get barcode label value
                     CLEAR_BIT( task_RxData.GetStatus.BC_Reader_1, B2B_STATUS_READER_LABEL_DETECTED);
                     break;

               case  SUB_CMD_BRC_HANDLE_TICKET        :  // 3 - Handle Ticket
                     task_TxPacket.BufSize   =  sizeof( task_TxData.BC_Reader_HandleTicket);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.BC_Reader_HandleTicket, task_TxPacket.BufSize);
                     break;
            }
            break;

      case  B2B_CMD_CLOCK              :  // 'C'
            task_TxPacket.BufSize   =  sizeof( task_TxData.Clock_SetDateAndTime);
            memcpy( task_TxPacket.pBuffer, &task_TxData.Clock_SetDateAndTime, task_TxPacket.BufSize);
            break;

      case  B2B_CMD_DEFAULT_PARAMS     :  // 'D'
            break;

      case  B2B_CMD_BILL_VALIDATOR     :  // 'G'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_BLV_SET_MODE             :  // 1 - Set Operation Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Bill_Valid_SetOperationMode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Bill_Valid_SetOperationMode, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_BLV_HANDLE_BILL          :  // 3 - Handle Bill
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Bill_Valid_HandleBill);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Bill_Valid_HandleBill, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_BLV_SET_ENABLED_BILLS    :  // 5 - Set Enabled Bills
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Bill_Valid_EnabledBills);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Bill_Valid_EnabledBills, task_TxPacket.BufSize);
                     break;
            }
            break;

      #if 0 // [
      case  B2B_CMD_LCD                :  // 'L'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_LCD_WRITE_STRING         :  // 0 - Write String
                     {
                     usint    Size;

                        Size  =  strlen( (char *)task_TxData.LCD_WriteString.Text);
                        if( Size > sizeof( task_TxData.LCD_WriteString.Text))
                           Size  =  sizeof( task_TxData.LCD_WriteString.Text);

                        task_TxPacket.BufSize   =  (sizeof( task_TxData.LCD_WriteString) - sizeof( task_TxData.LCD_WriteString.Text) + Size);
                        memcpy( task_TxPacket.pBuffer, &task_TxData.LCD_WriteString, task_TxPacket.BufSize);
                     }
                     break;

               case  SUB_CMD_LCD_SET_TEXT_BLINKING    :  // 1 - Set Text Blinking Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.LCD_SetTextBlinkingMode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.LCD_SetTextBlinkingMode, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_LCD_SET_CURSOR_POSITION  :  // 2 - Set Cursor Position
                     task_TxPacket.BufSize   =  sizeof( task_TxData.LCD_SetCursorPosition);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.LCD_SetCursorPosition, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_LCD_SET_CURSOR_MODE      :  // 3 - Set Cursor Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.LCD_SetCursorMode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.LCD_SetCursorMode, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_LCD_SET_BACKLIGHT        :  // 4 - Set Backlight Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.LCD_SetBacklightMode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.LCD_SetBacklightMode, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_LCD_CLEAR_LINE           :  // 5 - Clear Line
                     task_TxPacket.BufSize   =  sizeof( task_TxData.LCD_ClearLine);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.LCD_ClearLine, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_LCD_WRITE_PRICE          :  // 7 - Write price
               case  SUB_CMD_LCD_WRITE_NUMBER         :  // 8 - Write number
               case  SUB_CMD_LCD_WRITE_NUMBER_HEX     :  // 9 - Write number Hex
                     task_TxPacket.BufSize   =  sizeof( task_TxData.LCD_WriteNumber);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.LCD_WriteNumber, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_LCD_WRITE_CHAR           :  // 10- Write char
                     task_TxPacket.BufSize   =  sizeof( task_TxData.LCD_WriteChar);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.LCD_WriteChar, task_TxPacket.BufSize);
                     break;
            }
            break;
      #endif   // ]

      case  B2B_CMD_MGC                :  // 'M'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_MGC_SET_MODE             :  // 1 - Set Operation Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.MGC_SetOperationMode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.MGC_SetOperationMode, task_TxPacket.BufSize);
                     break;
            }

      case  B2B_CMD_PRINTER            :  // 'P'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_PRN_SET_FONT             :  // 1 - Set Font
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Printer_SetFont);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Printer_SetFont, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_PRN_PRINT_TEXT           :  // 2 - Print Text
                     {
                     usint    Size;

                        //Size  =  strlen( (char *)task_TxData.Printer_PrintText.Text);
                        Size  =  task_TxData.Printer_PrintText.Count;
                        if( Size > sizeof( task_TxData.Printer_PrintText.Text))
                           Size  =  sizeof( task_TxData.Printer_PrintText.Text);

                        task_TxPacket.BufSize   =  (sizeof( task_TxData.Printer_PrintText) - sizeof( task_TxData.Printer_PrintText.Text) + Size);
                        memcpy( task_TxPacket.pBuffer, &task_TxData.Printer_PrintText, task_TxPacket.BufSize);
                     }
                     break;

               case  SUB_CMD_PRN_CUTTER               :  // 3 - Activate Cutter
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Printer_SetCutterMode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Printer_SetCutterMode, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_PRN_PRINT_BARCODE_LABLE  :  // 4 - Print Bar-Code Label
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Printer_BarCode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Printer_BarCode, task_TxPacket.BufSize);
                     break;
            }
            break;

      case  B2B_CMD_RELAY              :  // 'R'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_RLY_SET_MODE             :  // 0 - Set Operation Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Relay_SetOperation);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Relay_SetOperation, task_TxPacket.BufSize);
                     break;
            }
            break;

      case  B2B_CMD_GET_STATUS         :  // 'S'
            break;

      case  B2B_CMD_LED                :  // 'T'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_LED_SET_MODE             :  // 0 - Set Operation Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.LED_SetOperation);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.LED_SetOperation, task_TxPacket.BufSize);
                     break;
            }
            break;

      case  B2B_CMD_COIN_VALIDATOR     :  // 'V'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_CNV_SET_MODE             :  // 1 - Set Operation Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Coin_Valid_SetOperationMode);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Coin_Valid_SetOperationMode, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_CNV_SET_ENABLED_COINS    :  // 3 - Set Enabled Coins
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Coin_Valid_EnabledCoins);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Coin_Valid_EnabledCoins, task_TxPacket.BufSize);
                     break;

               case  SUB_CMD_CNV_SET_COIN             :  // 4 - Set Coin (Assign Value and Routing)
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Coin_Valid_SetCoin);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Coin_Valid_SetCoin, task_TxPacket.BufSize);
                     break;
            }
            break;

      case  B2B_CMD_BUZZER             :  // 'Z'
            switch( task_TxPacket.Sub_Command)
            {
               case  SUB_CMD_BUZ_SET_MODE             :  // 0 - Set Operation Mode
                     task_TxPacket.BufSize   =  sizeof( task_TxData.Buzzer_SetOperation);
                     memcpy( task_TxPacket.pBuffer, &task_TxData.Buzzer_SetOperation, task_TxPacket.BufSize);
                     break;
            }
            break;

      default:
            break;
   }

   task_RxTimer   =  Task_Timer_SetTimeMsec( 500);
   task_fTxActive =  TRUE;
   
   //******************* Debug ***********************
   //if (task_TxPacket.Command == 'P')
   //{    
   //   if (task_TxPacket.Sub_Command == SUB_CMD_PRN_PRINT_TEXT)
   //   {
   //      Debug_PrintHex((byte*)&task_TxPacket, 12);
   //      Debug_PrintHex((byte*)task_TxPacket.pBuffer, 10);
   //   }
   //}
   //*************************************************
   
   B2BAPI_SendData( &task_TxPacket);
}



/*----------------------------------------------------------------------------*/
static	void						Task_B2B_Master_HandleRxMsg( void)
/*----------------------------------------------------------------------------*/
{
   if( B2BAPI_IsRxNewPacket() == FALSE)   // if no new msg received...
      return;

   task_pRxPacket =  B2BAPI_GetData();

   if( Task_B2B_Master_IsMsgForMe() == FALSE) // if msg is not addressed to me... (directly or broadcast)
      return;

   task_RxTimer   =  0;
   task_fTxActive =  FALSE;
   task_B2B_Master_SetSlaveStatus( task_pRxPacket->UnitType, task_pRxPacket->Address, ACTIVE);

   SET_BIT( App_TaskEvent[TASK_B2B], EV_APP_B2B_RX_MSG);

   switch( task_pRxPacket->Command)
   {
      case  B2B_CMD_ACK            	   :  // 'A'
            break;

      case  B2B_CMD_NACK           	   :  // 'N'
            break;
   }

   if( task_pRxPacket->BufSize == 0)
      return;

   switch( task_pRxPacket->Command)
   {
      case  B2B_CMD_BAR_CODE_READER    :  // 'B'
            switch( task_pRxPacket->Sub_Command)
            {
               case  SUB_CMD_BRC_GET_STATUS           :  // 0 - Get Vendor Status
                     memcpy( &task_RxData.BC_Reader_1_Status, task_pRxPacket->pBuffer, sizeof( task_RxData.BC_Reader_1_Status));
                     break;

               case  SUB_CMD_BRC_GET_BARCODE_LABEL    :  // 2 - Get Bar-Code Label
                     memcpy( &task_RxData.BC_Reader_1_Label, task_pRxPacket->pBuffer, sizeof( task_RxData.BC_Reader_1_Label));
                     if( task_pRxPacket->BufSize < sizeof( task_RxData.BC_Reader_1_Label))
                        task_RxData.BC_Reader_1_Label[task_pRxPacket->BufSize]   =  0;
                     else
                        task_RxData.BC_Reader_1_Label[sizeof( task_RxData.BC_Reader_1_Label) - 1]  =  0;
                     SET_BIT( App_TaskEvent[TASK_B2B], EV_APP_B2B_RX_BAR_CODE_LABEL_1);
                     break;
            }
            break;

      case  B2B_CMD_CLOCK              :  // 'C'
            memcpy( &task_RxData.Clock_DateAndTime, task_pRxPacket->pBuffer, sizeof( task_RxData.Clock_DateAndTime));
            break;

      case  B2B_CMD_DEFAULT_PARAMS     :  // 'D'
            break;

      case  B2B_CMD_BILL_VALIDATOR     :  // 'G'
            switch( task_pRxPacket->Sub_Command)
            {
               case  SUB_CMD_BLV_GET_STATUS           :  // 0 - Get Vendor Status
                     memcpy( &task_RxData.Bill_Valid_Status, task_pRxPacket->pBuffer, sizeof( task_RxData.Bill_Valid_Status));
                     break;

               case  SUB_CMD_BLV_GET_BILL_VALUE       :  // 2 - Get Bill Value
                     memcpy( &task_RxData.Bill_Valid_Value, task_pRxPacket->pBuffer, sizeof( task_RxData.Bill_Valid_Value));
                     SET_BIT( App_TaskEvent[TASK_B2B], EV_APP_B2B_RX_BILL_VALUE);
                     break;

               case  SUB_CMD_BLV_GET_NUM_BILLS_IN_STACKER:  // 4 - Get Number of Bills in the Stacker
                     memcpy( &task_RxData.Bill_Valid_BillsInStacker, task_pRxPacket->pBuffer, sizeof( task_RxData.Bill_Valid_BillsInStacker));
                     break;
            }
            break;

      case  B2B_CMD_LCD                :  // 'L'
            break;

      case  B2B_CMD_MGC                :  // 'M'

            switch( task_pRxPacket->Sub_Command)
            {
               case  SUB_CMD_MGC_GET_STATUS           :  // 0 - Get Status
                     memcpy( &task_RxData.MGC_Status, task_pRxPacket->pBuffer, sizeof( task_RxData.MGC_Status));
                     break;

               case  SUB_CMD_MGC_GET_TRACK2           :  // 2 - Get Track2
                     memset( &task_RxData.MGC_Track2, 0, sizeof( task_RxData.MGC_Track2));      // 29/5/2017 Clear all buffer to avoid garbage left 
                     memcpy( &task_RxData.MGC_Track2, task_pRxPacket->pBuffer, task_pRxPacket->BufSize );
                     putchar('g');
                     Debug_PrintHex((byte*)task_RxData.MGC_Track2, task_pRxPacket->BufSize);
                     break;
            }
            break;

      case  B2B_CMD_PRINTER            :  // 'P'
            switch( task_pRxPacket->Sub_Command)
            {
               case  SUB_CMD_PRN_GET_STATUS           :  // 0 - Get Vendor Status
                     memcpy( &task_RxData.Printer_Status, task_pRxPacket->pBuffer, sizeof( task_RxData.Printer_Status));
                     break;
            }
            break;

      case  B2B_CMD_RELAY              :  // 'R'
            break;

      case  B2B_CMD_GET_STATUS         :  // 'S'
            
            switch( task_pRxPacket->Sub_Command)
            {
               case  SUB_CMD_VOID                     :  // 0 - Get Device Status
                     memcpy( &task_RxData.GetStatus, task_pRxPacket->pBuffer, sizeof( task_RxData.GetStatus));
                     break;
            }
            break;

      case  B2B_CMD_LED                :  // 'T'
            break;

      case  B2B_CMD_COIN_VALIDATOR     :  // 'V'
            switch( task_pRxPacket->Sub_Command)
            {
               case  SUB_CMD_CNV_GET_STATUS           :  // 0 - Get Status
                     memcpy( &task_RxData.Coin_Valid_Status, task_pRxPacket->pBuffer, sizeof( task_RxData.Coin_Valid_Status));
                     break;

               case  SUB_CMD_CNV_GET_COIN_VALUE       :  // 2 - Get Coin Value
                     memcpy( &task_RxData.Coin_Valid_Value, task_pRxPacket->pBuffer, sizeof( task_RxData.Coin_Valid_Value));
                     SET_BIT( App_TaskEvent[TASK_B2B], EV_APP_B2B_RX_COIN_VALUE);
                     break;
            }
            break;

      case  B2B_CMD_BUZZER             :  // 'Z'
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	bool						Task_B2B_Master_IsMsgForMe( void)
/*----------------------------------------------------------------------------*/
{
bool     ReturnValue;


   ReturnValue =  TRUE;

   // If msg was sent from a Slave unit
   if( task_pRxPacket->fFromMaster == TRUE)   ReturnValue =  FALSE;

   return( ReturnValue);
}



/*----------------------------------------------------------------------------*/
static	void						task_B2B_Master_SetSlaveStatus( byte   Type, byte  Address, bool  Mode)
/*----------------------------------------------------------------------------*/
{
byte     Index;
byte     VacantEntry =  0xFF;


   for( Index = 0; Index < MAX_SLAVES; Index ++)
   {
      if( (task_Slaves[Index].Type == Type) && (task_Slaves[Index].Address == Address))
      {
         task_Slaves[Index].fActive =  Mode;
         return;
      }

      if( (task_Slaves[Index].Type == 0) && (task_Slaves[Index].Address == 0) && (VacantEntry == 0xFF))
         VacantEntry =  Index;
   }

   if( VacantEntry != 0xFF)
   {
      Index =  VacantEntry;
      task_Slaves[Index].Type    =  Type;
      task_Slaves[Index].Address =  Address;
      task_Slaves[Index].fActive =  Mode;
   }
}

