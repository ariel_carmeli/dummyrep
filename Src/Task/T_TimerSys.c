/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/

/*---------------------------- Typedef Directives ----------------------------*/

/*---------------------------- External Variables ----------------------------*/

/*----------------------------- Global Variables -----------------------------*/
tTimerS_API_Setup*                      api_pTimerS_Setup;
static	bool				fTimerSSetupStruct_Valid;

/*typedef enum 
{
   ACTION_ADD = 0,
   ACTION_REM,
   ACTION_TO,
   ACTION_UPD,
   ACTION_ERR
} eActionPrint;*/


CONST char ActMsg[5][4] = 
{
   "Add",               // enum: ACTION_ADD
   "Rem",               // enum: ACTION_REM
   "TO_",               // enum: ACTION_TO
   "UPD",               // enum: ACTION_UPD
   "ERR"                // enum: ACTION_ERR
};

/*------------------------ Local Function Prototypes -------------------------*/
static void                             BuildFreeLinkList( byte ListType );
static usint                            TimeTypeAssign ( usint time, byte TimeUnit, byte* TimeType );
static bool                             TimerSearch ( usint time, byte ListType, pCallBack pCBF );
static bool                             TimerInsert ( usint time, byte ListType, pCallBack pCBF );
static byte                             FindTimerIndexByCBF( pCallBack pCBF );
static void                             TimerRemove ( byte index, byte ListType );
static byte                             DisconnectTimerFromActiveList( byte ListType, byte prev_index );
static void                             ReturnTimerToFreeList( /*byte ListType,*/ byte Index );
static byte                             GetFreeTimer(byte ListType);
static byte                             InsertActiveTimer(byte ListType, byte NewIndex, /*byte LastIndex,*/ usint time,  pCallBack pCBF);
static byte                             GetLastActiveTimer(byte ListType);

// Unit Test
//void                                    TimersPrintActiveLink(eActionPrint Action, usint cbf, byte tab);
void                                    Timers_Test_1(void);
void                                    Timers_Test_2(void);
void                                    Timers_Test_3(void);
void                                    Timers_Test_4(void);
void                                    Timers_Test_OnDisplayTFT ( byte MessageType );

/*----------------------------------------------------------------------------*/
#define TIMERS_EXTERNAL_RAM 0

#if TIMERS_EXTERNAL_RAM
// External RAM
static tTimersSystem *pTimersSystem = (tTimersSystem *)NVRAM_ADDR_T_TIMERS_SYSTEM;      
//#endif
#else
//#if 1
// Internal RAM
//static char *pTimersSystemPrints = (char *)NVRAM_ADDR_T_TIMERS_SYSTEM;          // a XRAM 256 bytes
static tTimersSystem *pTimersSystem;
static tTimersSystem TimersSystem;
#endif


#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif


/*
* 
* 
* 
*/
/*----------------------------------------------------------------------------*/
void                    		Timers_Init( tTimerS_API_Setup* pTimerS_API_Setup)
/*----------------------------------------------------------------------------*/
{
byte ind;
   fTimerSSetupStruct_Valid = FALSE;

   if ( pTimerS_API_Setup != NULL)
   {
      // pTimerS_API_Setup pointer is relevant
      api_pTimerS_Setup = pTimerS_API_Setup;
      fTimerSSetupStruct_Valid = TRUE;
     
      api_pTimerS_Setup->TimersOvfPeakMeter = 0;
   }

#if TIMERS_EXTERNAL_RAM
#else   
   // Internal RAM
   pTimersSystem = &TimersSystem;
#endif
   
   pTimersSystem->SecondsCounter                                = 0;
   // Short time (x10mS) timers system init
   pTimersSystem->ShortTimeTimers.Control.ActiveFlags		= 0;
   pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead	= TIMER_SYS_NULL;
   pTimersSystem->ShortTimeTimers.Control.FreeLinkHead		= 0;
   pTimersSystem->ShortTimeTimers.Control.PeakMeter		= 0;
   //pTimersSystem->ShortTimeStatus                               = 0;                            // counts the amount of expired Timers
   BuildFreeLinkList(TIMER_10mS_TYPE);                                                          // Organize the Free Link list
   pTimersSystem->ShortTimeTimers.Control.TimTypeFlags          = 0;
   pTimersSystem->FreeCounter10mS_Prev                          = 0;

   pTimersSystem->FreeRunning                                   = 0;                            // Free running for debug


   //for (ind=0;ind<16;ind++)
   //{
   //   pTimersSystem->TestFreeRunningCnt[ind] = 0;
   //}
   
}



/*
* 
* 
* 
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_Main(void)
/*----------------------------------------------------------------------------*/
{
byte ind;
byte prev_ind;
static pCallBack p_call_b_f;

   // ------------------------------
   // 10mS period
   if ( pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead != TIMER_SYS_NULL )
   {
      if (pTimersSystem->ShortTimeTimers.Control.ActiveFlags != 0 )
      {
         // active list is NOT empty
         ind = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;        // prepare first timer to be Decremented.
         prev_ind = TIMER_SYS_HEAD_ADDRESS;
         do
         { 
            // 'walk' over the link list. Look for an expired counter. NO MATTER IF 1SEOND OR 10MSEC !!!
            // Each Timer expiration is handled  and Timer is REMOVED immediately !!!
            if (pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter == 0)
            {
               // O.K. Timer expiration !!!   Call CBF and Remove Timer from the linked list. Then, get out.
               p_call_b_f = pTimersSystem->ShortTimeTimers.TimersPool[ind].CBFunc;      // backup callback function
               
               //------ Remove timer from the list -----------
               TimerRemove ( prev_ind, TIMER_10mS_TYPE );
               CLR_BIT_INDEX(pTimersSystem->ShortTimeTimers.Control.ActiveFlags, ind);
               //---------------------------------------------
               // Run call back function

               if (p_call_b_f != NULL)
                  p_call_b_f();

               return;                                                          // Do only one CBF per Timers_Main() cycle
            }
            // This part will continue and work, ONLY if there wasn't a match (& Run & Remove) yet.
            prev_ind = ind;
            ind = pTimersSystem->ShortTimeTimers.TimersPool[ind].Next;          // prepare Next timer.
         } while ( ind != TIMER_SYS_NULL );
      }
   }
   // -------------------------------
   
}



/*
* Timers_ISR is called every 10mS
* It is handling Timers system for 10mS periods and Seconds periods.
* 
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_10mS_Periods(void)
/*----------------------------------------------------------------------------*/
{
byte ind;
tTimerMsec new_time;
int     gap;
  

   new_time = Task_Timer_GetTimer();                            // return up-counter (of 10mS periods). counter range: 0..255 (0 .. 2.55 seconds)
   gap = new_time - pTimersSystem->FreeCounter10mS_Prev;          // Normaly, gap can be 0 or 1. Sometimes 2. While overrun 255 back to 0 or 1, gap will be negative.
     
   if (gap == 0)
   {
      return;   // We need a step of at least 1 count or 10mS
   }
   else if ( gap < 0 )  // overrun the 255 (goes back to 0, 1..
   {
      gap += 256;
   }
   pTimersSystem->FreeCounter10mS_Prev = new_time;                // old = new
   
   //DEBUG_LINE_ON;

   // ------------------------------
   // 1 Second tick handling
   if ( pTimersSystem->SecondsCounter < 100 )
   {
      pTimersSystem->SecondsCounter++;
   }
   else
   {
      pTimersSystem->SecondsCounter = 0;                                // 1 second had passed
   }    
   
   // ------------------------------
   // 10mS period
   if ( pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead != TIMER_SYS_NULL )
   {
      // active list is NOT empty
      ind = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;        // prepare first timer to be Decremented.
      do
      {
         // if this timer-link, a x1Seconds timer ?
         if ( IS_BIT_INDEX(pTimersSystem->ShortTimeTimers.Control.TimTypeFlags, ind) )
         {
            // This is a 1Seconds timer
            if ( pTimersSystem->SecondsCounter == 0 )
            {
               // decrement 1Seconds timer
               if (pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter > 0)
               {
                  pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter--;
                  if (pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter == 0)         // This was "if (pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter > 0)"
                  {
                     SET_BIT_INDEX(pTimersSystem->ShortTimeTimers.Control.ActiveFlags, ind);  // Notify about active flag which expired
                  }
               }
            }
         }
         else
         {
            // This is a 10mS timer
            // one timer-link handlind
            if (pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter > gap)
            {
               pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter -= gap;
               // result is above zero
            }
            else
            {
               // Timeout !!!
               pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter = 0;
               SET_BIT_INDEX(pTimersSystem->ShortTimeTimers.Control.ActiveFlags, ind);  // Notify about active flag which expired
            }
         }

         // increment to the next timer-link
         ind = pTimersSystem->ShortTimeTimers.TimersPool[ind].Next;       // prepare Next timer.
      } while ( ind != TIMER_SYS_NULL );
   }
  
   // ------------------------------
   //DEBUG_LINE_OFF;
}



/*
* Set new timer
* Arguments:
* time - time in:
*    TimeType: TIME_UNIT_1mS   or   TIME_UNIT_1Sec
* 
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_SetTimer( usint time, byte TimeUnit, pCallBack pCBF )
/*----------------------------------------------------------------------------*/
{
   usint asgn_time;
   byte time_type;
   byte assigned;
   bool fExitAction;
   
   asgn_time = TimeTypeAssign ( time, TimeUnit, &time_type );

   fExitAction = TimerSearch ( asgn_time, time_type, pCBF );
   if ( fExitAction == TRUE )
   {
      // All actions are done
      return;
   }
   else
   {
      // Insert
      TimerInsert ( asgn_time, time_type, pCBF );
   }
   
}




/*
* Find an active timer which matched with the Callback function argument.
* Return back it's current time value (counter) and type of time (x10mS or x1Sec)
* TimerInfo:
*          0           |          1
* --------------------------------------------- 
* |  TimeType          |     TimerCounter     |
* ---------------------------------------------
*
*  Return:
*          TRUE = O.K
*          FALSE = Not found
*
*/
/*----------------------------------------------------------------------------*/
bool                                    Timers_GetTimerValue( pCallBack pCBF, usint* TimerInfo )
/*----------------------------------------------------------------------------*/
{
byte ind;
byte prev_index;

   prev_index = FindTimerIndexByCBF( pCBF );                                    // Might return TIMER_SYS_HEAD_ADDRESS as previous index
   
   if ( prev_index == TIMER_SYS_NULL )
   {
      // no match in short time list
      return FALSE;                         // Not found
   }
   
   if ( prev_index == TIMER_SYS_HEAD_ADDRESS )
   {
      ind = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;
   }
   else
   {   
      ind = pTimersSystem->ShortTimeTimers.TimersPool[prev_index].Next;
   }
   if (TimerInfo != NULL)
   {
      TimerInfo[1] = pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter;         // time count
   
      // Single Link List method - find TimeType
      if ( IS_BIT_INDEX( pTimersSystem->ShortTimeTimers.Control.TimTypeFlags, ind ) )
      {
         // 1Seconds timer
         TimerInfo[0] = TIMER_1Sec_TYPE;
      }
      else
      {
         // 10mS timers
         TimerInfo[0] = TIMER_10mS_TYPE;
      }
   }
   return TRUE;
}





/*
* 
* 
* 
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_StopTimer( pCallBack pCBF )
/*----------------------------------------------------------------------------*/
{
byte prev_index;

   prev_index = FindTimerIndexByCBF( pCBF );
   if ( prev_index != TIMER_SYS_NULL )
   {
      // A match
      TimerRemove ( prev_index, TIMER_10mS_TYPE );
      return;
   }
  
   return;
}




/*
* Blow a Timer:
*   Find the Timer by its CBF.
*   Set its counter to zero.
*   Remove timer from the list
*   Call the CBF immediately
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_BlowTimer( pCallBack pCBF )
/*----------------------------------------------------------------------------*/
{
   Timers_StopTimer( pCBF );
   pCBF();
}



/*----------------------------------------------------------------------------*/
void                                    Timers_FreeRunCnt(void)
/*----------------------------------------------------------------------------*/
{
   pTimersSystem->FreeRunning++;                                        // free running counter for debug purpose
}


#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

/*
* Function gets time (0..65535) and TimeUnit (1mS or 1Sec).
* It decided if to use a 10mS timers or 1sec timers
*
* In case of 1mS TimeUnit, it always return TimeType of 10mS and time is divided by 10
*
* In case of 1Sec TimeUnit:
*   a. Between 0 to 655 Seconds, it return TimeType of 10mS and time multiplied by 100 (655 x 100 = 65500 (x 10mS)
*   b. Above 655 (Seconds) it returns TimeType of 1Sec and time value is the same.
*/
/*----------------------------------------------------------------------------*/
static usint                             TimeTypeAssign ( usint time, byte TimeUnit, byte* TimeType )
/*----------------------------------------------------------------------------*/
{
   if ( TimeUnit == TIME_UNIT_1mS )
   {
      *TimeType = TIMER_10mS_TYPE;
      return (time / 10);
   }
   
   if ( TimeUnit == TIME_UNIT_1Sec )
   {
      if ( time < 656 )
      {
         // time is between 0..655 seconds or 0..65500x10mS
         *TimeType = TIMER_10mS_TYPE;
         return ( time * 100 );
      }
      else
      {
         // time is above 655 seconds or above 65500x10mS. Use 1Sec and time stay the same.
         *TimeType = TIMER_1Sec_TYPE;
         return time;
      }
   }
  
   return 0;
}



/*
-------------------------------------------------------------------------------------

 It runs over the 10mS Timers Link List            //////(and in some cases over the 1Sec Timers Link List)
  (see "Search () behavioure" table above). It retuns a boolean.
 When founding an active timer, which using the same CBF, it updates the timer with the new time... and return TRUE (mission complete !)
 Else (if CBF is not exist in the list), it return FALSE. The caller will Insert a new timer, later.


-------------------------------------------------------------------------------------
*/
/*----------------------------------------------------------------------------*/
static bool                             TimerSearch ( usint time, byte ListType, pCallBack pCBF )
/*----------------------------------------------------------------------------*/
{
byte prev_index;
byte ind;

   prev_index = FindTimerIndexByCBF( pCBF );                      // Might return TIMER_SYS_HEAD_ADDRESS as previous index
   if ( prev_index != TIMER_SYS_NULL )
   {
      // // match timer - Update
      if ( prev_index == TIMER_SYS_HEAD_ADDRESS )
      {
        ind = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;
      }
      else
      {
         ind = pTimersSystem->ShortTimeTimers.TimersPool[prev_index].Next;        // I have prev. Find real index
      }
      pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter = time;      // update 10mS timer
      // Update Type Of Timers
      if (ListType == TIMER_10mS_TYPE)
      {
         CLR_BIT_INDEX(pTimersSystem->ShortTimeTimers.Control.TimTypeFlags, ind);    // '0' = x10mS set the specific bit (from the 32 bits)
      }
      else if (ListType == TIMER_1Sec_TYPE)
      {
         SET_BIT_INDEX(pTimersSystem->ShortTimeTimers.Control.TimTypeFlags, ind);    // '1' = x1Sec
      }
      // exit TRUE
      return TRUE;
   }
   
   return FALSE;
}




/*
* This function, gets: time, type of time (10mS or 1Seconds) and pointer to the CBF (Call Back Function).
* It pools a free timer from the Free link list.
* It tie the free timer on the Active link list and update it with the time and CBF
*/
/*----------------------------------------------------------------------------*/
static bool                             TimerInsert ( usint time, byte ListType, pCallBack pCBF )
/*----------------------------------------------------------------------------*/
{
byte index_to_insert;
byte status;
byte last_timer;


   // Pull free timer from the Free link list
   index_to_insert = GetFreeTimer(ListType);
   
   if ( index_to_insert == TIMER_SYS_NO_FREE_TIMERS )
   {
      // Problem !!! no free timer in the free link list
      TimersPrintActiveLink(ACTION_ERR, (usint) pCBF, 0);
   }
   
   // Find location to insert the timer
   last_timer = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;

   if (last_timer == TIMER_SYS_NULL)
   {

   }
   
   // Insert the timer. Handle Active Tail and sets Active Flags
   status = InsertActiveTimer(ListType, index_to_insert, time, pCBF);
   if ( status == TIMER_SYS_NO_FREE_TIMERS )
   {
      // Problem !!! Probabely, peak meter shows: too much timers in active link list.
      if ( fTimerSSetupStruct_Valid == TRUE)
      {
         //1. Call the OverFlow function
         if ( api_pTimerS_Setup->func_TimerS_OVF != NULL)
         {
            api_pTimerS_Setup->func_TimerS_OVF(0);
         } 
      }
      //2. Call the Delayed CBF, now !
      if (pCBF != NULL)
         pCBF();

      
      TimersPrintActiveLink(ACTION_ERR, (usint) pCBF, 51);
   }
   else
   {
      // new timer has added
      if ( fTimerSSetupStruct_Valid == TRUE)
      {
         // compare dynamic peak meter with Maximum peak meter
         if ( pTimersSystem->ShortTimeTimers.Control.PeakMeter > api_pTimerS_Setup->TimersOvfPeakMeter )
            api_pTimerS_Setup->TimersOvfPeakMeter = pTimersSystem->ShortTimeTimers.Control.PeakMeter;
            
      }
   }

   return 0;
}




/*
* 
* 
* TimerRemove is not handling a case of TIMER_SYS_HEAD_ADDRESS as prev_index.
* Instead, DisconnectTimerFromActiveList() is handling this cases (TIMER_SYS_HEAD_ADDRESS)
*/
/*----------------------------------------------------------------------------*/
static void                             TimerRemove ( byte prev_index, byte ListType )
/*----------------------------------------------------------------------------*/
{
byte timer_to_remove;
   timer_to_remove = DisconnectTimerFromActiveList( ListType, prev_index );    // index pointing to the prev-link timer. Handling ActiveFlags
   ReturnTimerToFreeList( /*ListType,*/ timer_to_remove );
}





/*
* This function gets the LIST indication (10mS or 1Sec list). 
* It find the first timer in the Free List. 
* Connect the Free List Head to the next Free timer.
* Return the index of this new Timer.
* Return value: 
*   0xFF means there are no free timers anymore.
*  0 .. N is the Timer index to the pool of timers.
*/
/*----------------------------------------------------------------------------*/
static byte                             GetFreeTimer(byte ListType)
/*----------------------------------------------------------------------------*/
{
byte 	result;
	
   result = pTimersSystem->ShortTimeTimers.Control.FreeLinkHead;	// get first link
   if ( result == TIMER_SYS_NULL )
   {
      // Head points to NULL. no free timer
      return TIMER_SYS_NO_FREE_TIMERS;
   }
   // Now, Head can point to the next link in the Free link list.
   pTimersSystem->ShortTimeTimers.Control.FreeLinkHead = pTimersSystem->ShortTimeTimers.TimersPool[result].Next;
   // The chosen link is not connected any more. return it's index
   return result;
}




/*
* With the NewIndex arguments, it goes directly to the right timer. 
* Update its Timer counter. Set the right Active flag ( flags |= 0x00000001<<index ).
* With LastIndex argument, it connect the new timer to the Last active timer in the Active 
*  link list.
* New timer will be pointing to NULL
*
* ALLWAYS Insert to the Head
*/
/*----------------------------------------------------------------------------*/
static byte                             InsertActiveTimer(byte ListType, byte NewIndex, /*byte LastIndex,*/ usint time,  pCallBack pCBF)
/*----------------------------------------------------------------------------*/
{
   
   if (NewIndex > TIMER_SYS_10mS_TIMERS_MAX)
   {
      TimersPrintActiveLink(ACTION_ERR, (usint) pCBF, 50);
      return TIMER_SYS_NO_FREE_TIMERS;
   }
   
   if ( pTimersSystem->ShortTimeTimers.Control.PeakMeter < TIMER_SYS_10mS_TIMERS_MAX )
   {
      // New Link Next,  will point to the previous value of the Head, before the bellow Insert
      pTimersSystem->ShortTimeTimers.TimersPool[NewIndex].Next = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;
        
      // Head
      pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead = NewIndex;                    // Head is Tied !
      
      // Update timer information
      pTimersSystem->ShortTimeTimers.TimersPool[NewIndex].CBFunc = pCBF;
      pTimersSystem->ShortTimeTimers.TimersPool[NewIndex].TimerCounter = time;
      // set time type
      if (ListType == TIMER_10mS_TYPE)
      {
         CLR_BIT_INDEX( pTimersSystem->ShortTimeTimers.Control.TimTypeFlags, NewIndex );
      }
      else if (ListType == TIMER_1Sec_TYPE)
      {
         SET_BIT_INDEX( pTimersSystem->ShortTimeTimers.Control.TimTypeFlags, NewIndex );
      }
      pTimersSystem->ShortTimeTimers.Control.PeakMeter++;
         
      TimersPrintActiveLink(ACTION_ADD, (usint)pCBF, 0);
      return 0;
   }
  
   return TIMER_SYS_NO_FREE_TIMERS;
}



/*
* The function runs over the First Timers list in order to find the requested Timer.
* If failed to find, it run over the second Timers list.
* During the search, it always keep the Previous timer who points to the
* required Timer. 
* When it find the required Timer, it returns a pointer the Previous Timer index and the 
*  ListType (a pointer to a total of 2 bytes..
* The caller of this function can use this Prev Index or easily get the required Timer Index.
* 
* Return previous index: can be real index (0..16 or more) or TIMER_SYS_HEAD_ADDRESS (0xFD)
*/
/*----------------------------------------------------------------------------*/
static byte                             FindTimerIndexByCBF( pCallBack pCBF )
/*----------------------------------------------------------------------------*/
{
byte ind, prev_ind;
   
   // Run over the first, Short Time active link list (x 10mS)
   prev_ind = TIMER_SYS_HEAD_ADDRESS;              //pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;	// Head holds the index to the first link in the list.
   for (ind = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead; ind < TIMER_SYS_10mS_TIMERS_MAX;   )
   {
      if (pTimersSystem->ShortTimeTimers.TimersPool[ind].CBFunc == pCBF)
      {
         // Match !

         return prev_ind;	// exit with results
      }
      else
      {
         // increment to the next link
         prev_ind = ind;
         ind = pTimersSystem->ShortTimeTimers.TimersPool[ind].Next;
         
         if (ind == TIMER_SYS_NULL)
         {
            return TIMER_SYS_NULL;		// no match in this link list. 
         }
      }
   }         // for   

   return TIMER_SYS_NULL;
}




/*
* Disconnect a timer from the active list.
* Clear Active Flag (Flags &= (~(0x00000001<<index))).
* Argument index is actualy pointing to the Prev (!) link of the required Timer.
* 
* Tie back the link list (optional case):
*  a. Timer was connected to the Head. It was only one Timer in the list
*  b. Timer was connected to the Head. More timers on the list
*  c. Timer was connected between two timers.
*  d. Timer was at the end of more-then-one timers list.
* 
* Return real index to the disconnected timer  or   TIMER_SYS_NO_FREE_TIMERS (0xFF)
*/
/*----------------------------------------------------------------------------*/
static byte                             DisconnectTimerFromActiveList( byte ListType, byte prev_index )
/*----------------------------------------------------------------------------*/
{
byte timer_to_remove;
byte next_timer_to_tie;
usint value_c_b_f;

   // Short time timers
   if (prev_index != TIMER_SYS_HEAD_ADDRESS)
   {
      // The prev timer to the timer to be removed, is not Head
      timer_to_remove = pTimersSystem->ShortTimeTimers.TimersPool[prev_index].Next;            // Index points to the prev. timer.

      value_c_b_f = (usint)pTimersSystem->ShortTimeTimers.TimersPool[timer_to_remove].CBFunc;        // Debug
      pTimersSystem->ShortTimeTimers.TimersPool[timer_to_remove].CBFunc = NULL;
         
      next_timer_to_tie = pTimersSystem->ShortTimeTimers.TimersPool[timer_to_remove].Next;     // timer beyond the one to be removed or NULL case
      pTimersSystem->ShortTimeTimers.TimersPool[prev_index].Next = next_timer_to_tie;          // Prev. Timer will be tied to the next Timer, while skipping the removed one.
   }
   else
   {
      // Head is currently pointing to the Timer to be removed.
      timer_to_remove = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;
         
      value_c_b_f = (usint)pTimersSystem->ShortTimeTimers.TimersPool[timer_to_remove].CBFunc;        // Debug
      pTimersSystem->ShortTimeTimers.TimersPool[timer_to_remove].CBFunc = NULL;
         
      next_timer_to_tie = pTimersSystem->ShortTimeTimers.TimersPool[timer_to_remove].Next;
      pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead = next_timer_to_tie;
   }
   
   if (pTimersSystem->ShortTimeTimers.Control.PeakMeter > 0)
      pTimersSystem->ShortTimeTimers.Control.PeakMeter--;
      
   TimersPrintActiveLink(ACTION_REM, value_c_b_f, 0);
   return timer_to_remove;
}



/*
* Connect Timer back to the Free Timers link list.
* Insert the returned link, to the Head
*/
/*----------------------------------------------------------------------------*/
static void                             ReturnTimerToFreeList( /*byte ListType,*/ byte Index )
/*----------------------------------------------------------------------------*/
{
byte last_free_timer;
byte ind;

   // New Link Next, will point to the current Head pointed Link
   pTimersSystem->ShortTimeTimers.TimersPool[Index].Next = pTimersSystem->ShortTimeTimers.Control.FreeLinkHead;
   // Tie to the Head
   pTimersSystem->ShortTimeTimers.Control.FreeLinkHead = Index;
}


/*
* Build the Free Timers link list
* It is required at the begining of the work with the timers.
* Insert the returned link, to the Head
*/
/*----------------------------------------------------------------------------*/
static void                             BuildFreeLinkList( byte ListType )
/*----------------------------------------------------------------------------*/
{
   byte ind;
   
   pTimersSystem->ShortTimeTimers.Control.FreeLinkHead = 0;    // Head points to the first timer in the pool
      
   for (ind = 0; ind < TIMER_SYS_10mS_TIMERS_MAX; ind++ )
   {
      pTimersSystem->ShortTimeTimers.TimersPool[ind].CBFunc = NULL;
      pTimersSystem->ShortTimeTimers.TimersPool[ind].Next = ind+1;
   }
   pTimersSystem->ShortTimeTimers.TimersPool[TIMER_SYS_10mS_TIMERS_MAX-1].Next = TIMER_SYS_NULL;       // the last timer will point to the NULL
         
}


/******************************************************************************/
/******************************************************************************/
/*                              Unit-Testing area                             */
/******************************************************************************/
/******************************************************************************/
/*
*
*
*
*/
/*----------------------------------------------------------------------------*/
void                                    TimersPrintActiveLink(eActionPrint Action, usint cbf, byte tab)
/*----------------------------------------------------------------------------*/
{
   byte ind;
   usint value;
   char str[5];

   
#if 0
   // Show the action
   strcpy_P(str, ActMsg[Action]);
   //36

   //printf("\n%s: 0x%x  t=%lu  pr=%d  sts=0x%lx  ", 
          str, cbf, pTimersSystem->FreeRunning, pTimersSystem->FreeCounter10mS_Prev, pTimersSystem->ShortTimeTimers.Control.ActiveFlags/*, tab*/ );
   
   //printf("peak %d  max peak %d\n", pTimersSystem->ShortTimeTimers.Control.PeakMeter, api_pTimerS_Setup->TimersOvfPeakMeter);
#endif

#if 0   
   // Short Link
   ind = pTimersSystem->ShortTimeTimers.Control.ActiveLinkHead;         // ind points to the first link
   while (ind != TIMER_SYS_NULL)
   {
      value = pTimersSystem->ShortTimeTimers.TimersPool[ind].TimerCounter;
      
      //printf("SL [%d].%d |  ", ind, value);

      ind = pTimersSystem->ShortTimeTimers.TimersPool[ind].Next;
   };
   //printf("End \n");
#endif
}


/*
*
*
*
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_Test_1(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_1, 1 );
   // for debug, set ON the pocket leds
   Task_Application_Test_Leds (LED_CHANGE_POCKET, ON );
   
   Timers_SetTimer( 1, TIME_UNIT_1Sec, Timers_Test_2 );
}


/*----------------------------------------------------------------------------*/
void Timers_Test_2(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_2, 2);
   Task_Application_Test_Leds (LED_CHANGE_POCKET, OFF );
   Timers_SetTimer( 1, TIME_UNIT_1Sec, Timers_Test_1 );
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_3(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_3, 3);
   Timers_SetTimer( 2, TIME_UNIT_1Sec, Timers_Test_4 );
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_4(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_4, 4);
   Timers_SetTimer( 1, TIME_UNIT_1Sec, Timers_Test_3 );
   Timers_SetTimer( 1, TIME_UNIT_1Sec, Timers_Test_3a );
}

//---------------
/*----------------------------------------------------------------------------*/
void                                    Timers_Test_5(void)
/*----------------------------------------------------------------------------*/
{       
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_5, 5);
   Timers_SetTimer( 660, TIME_UNIT_1Sec, Timers_Test_6 );
   
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_6(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_6, 6);
   Timers_SetTimer( 820, TIME_UNIT_1Sec, Timers_Test_5 );
   Timers_SetTimer( 820, TIME_UNIT_1Sec, Timers_Test_5a );
}



//---------------
/*----------------------------------------------------------------------------*/
void                                    Timers_Test_7(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_7, 7);
   Timers_SetTimer( 690, TIME_UNIT_1Sec, Timers_Test_8 );
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_8(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
}


//==================================



/*----------------------------------------------------------------------------*/
void                                    Timers_Test_9(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_10(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_11(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_12(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}



/*----------------------------------------------------------------------------*/
void                                    Timers_Test_13(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_14(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_15(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_16(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}





/*----------------------------------------------------------------------------*/
void                                    Timers_Test_17(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_18(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_19(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_20(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}


///////////////////////////////////////////////////////////////////////////////////




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_1a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_1, 1 );
   Timers_SetTimer( 5, TIME_UNIT_1Sec, Timers_Test_2 );
   Timers_SetTimer( 5, TIME_UNIT_1Sec, Timers_Test_4a );
   Timers_SetTimer( 5, TIME_UNIT_1Sec, Timers_Test_6a );
   
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_2a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_2, 2);
   Timers_SetTimer( 5, TIME_UNIT_1Sec, Timers_Test_1 );
   Timers_SetTimer( 5, TIME_UNIT_1Sec, Timers_Test_1a );
   Timers_SetTimer( 5, TIME_UNIT_1Sec, Timers_Test_4a );
   Timers_SetTimer( 5, TIME_UNIT_1Sec, Timers_Test_8a );
   
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_3a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_3, 3);
   Timers_SetTimer( 2, TIME_UNIT_1Sec, Timers_Test_4 );
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_4a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_4, 4);
   Timers_SetTimer( 1, TIME_UNIT_1Sec, Timers_Test_3 );
   
}

//---------------
/*----------------------------------------------------------------------------*/
void                                    Timers_Test_5a(void)
/*----------------------------------------------------------------------------*/
{       
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_5, 5);
   Timers_SetTimer( 660, TIME_UNIT_1Sec, Timers_Test_6 );
   
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_6a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_6, 6);
   Timers_SetTimer( 820, TIME_UNIT_1Sec, Timers_Test_5 );
   
}



//---------------
/*----------------------------------------------------------------------------*/
void                                    Timers_Test_7a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_7, 7);
   Timers_SetTimer( 690, TIME_UNIT_1Sec, Timers_Test_8 );
   
}


/*----------------------------------------------------------------------------*/
void                                    Timers_Test_8a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_9a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_10a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_11a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   Timers_SetTimer( 420, TIME_UNIT_1Sec, Timers_Test_6 );
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_12a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   Timers_SetTimer( 320, TIME_UNIT_1Sec, Timers_Test_15a );
}



/*----------------------------------------------------------------------------*/
void                                    Timers_Test_13a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_14a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_15a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_16a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_17a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_18a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_19a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




/*----------------------------------------------------------------------------*/
void                                    Timers_Test_20a(void)
/*----------------------------------------------------------------------------*/
{
   TimersPrintActiveLink(ACTION_TO, (usint)Timers_Test_8, 8);
   Timers_SetTimer( 720, TIME_UNIT_1Sec, Timers_Test_7 );
   
}




//========================================================================================================
//                                   Use TFT Display, to show debug message
//========================================================================================================
#if 0
typedef  enum
{
   VGA_MSG_TEST1                        ,
   VGA_MSG_TEST2                        ,
   VGA_MSG_TEST3                        ,
   VGA_MSG_TEST4                        ,
   VGA_MSG_TEST5                        ,
   VGA_MSG_TEST6                        ,
   VGA_MSG_TEST7                        ,
   VGA_MSG_TEST8                        ,
   VGA_MSG_TEST9                        ,
   VGA_MSG_TEST10                       ,
   VGA_MSG_TEST11                       ,
   VGA_MSG_TEST12                       ,
   VGA_MSG_TEST13                       ,
   VGA_MSG_TEST14                       ,
   VGA_MSG_TEST15                       ,
   VGA_MSG_TEST16                       ,
   /******************/
   VGA_MSG_TIMERS_MAX_MESSAGES
}  eVGA_TIMERS_TESTS;


CONST	static   char     DBG_TIMERS_MESSAGES [VGA_MSG_TIMERS_MAX_MESSAGES][MAX_LANGUAGES_EXTENDED][16 + 1]   =
                            { //        "                "     "                "
/* VGA_MSG_TEST1            */ {	" Test 1         "  ,  " Test 1         "  }  ,  // Font 3   [22]
/* VGA_MSG_TEST2            */ {	" Test 2         "  ,  " Test 2         "  }  ,  // Font 7 / Font 5
/* VGA_MSG_TEST3            */ {	" Test 3         "  ,  " Test 3         "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST4            */ {	" Test 4         "  ,  " Test 4         "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST5            */ {	" Test 5         "  ,  " Test 5         "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST6            */ {	" Test 6         "  ,  " Test 6         "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST7            */ {	" Test 7         "  ,  " Test 7         "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST8            */ {	" Test 8         "  ,  " Test 8         "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST9            */ {	" Test 9         "  ,  " Test 9         "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST10           */ {	" Test 10        "  ,  " Test 10        "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST11           */ {	" Test 11        "  ,  " Test 11        "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST12           */ {	" Test 12        "  ,  " Test 12        "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST13           */ {	" Test 13        "  ,  " Test 13        "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST14           */ {	" Test 14        "  ,  " Test 14        "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST15           */ {	" Test 15        "  ,  " Test 15        "  }  ,  // Font 5   [17]
/* VGA_MSG_TEST16           */ {	" Test 16        "  ,  " Test 16        "  }  ,  // Font 5   [17]
                           };



/*----------------------------------------------------------------------------*/
void                                    Timers_Test_OnDisplayTFT ( byte MessageType )
/*----------------------------------------------------------------------------*/
{
byte ind;
  
   Task_Display_SetAlignment     ( VGA_ALIGNMENT_LEFT);  
   Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_2);
   Task_Display_SetColor         ( VGA_COLOR_LIGHT_RED);

   // TibaPay communication Loss - Label
   
   for (ind = 0; ind < 15; ind++)
   {
      strcpy_P( p_NVRAM_APP_TXT_BUF, DBG_TIMERS_MESSAGES[ind][App_Language]);
      Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);                  //( char   *pStr  )  
      Task_Display_SetAlignment     ( VGA_ALIGNMENT_LEFT); 
      Task_Display_WriteString      ( ind, 0, p_NVRAM_APP_TXT_BUF);      //( byte Line, byte Offset, char *pStr)

      pTimersSystem->TestFreeRunningCnt[MessageType] = pTimersSystem->FreeRunning;              // update relevant test counter indicator
      Task_Display_SetAlignment     ( VGA_ALIGNMENT_RIGHT);
      Task_Display_WriteNumber      ( ind, 18, pTimersSystem->TestFreeRunningCnt[ind], 8);          //( byte Line, byte Offset, ulong Number, byte Digits)
   }
}
#endif
//========================================================================================================



/*----------------------------------------------------------------------------*/
void                                    Timers_Test_Print(byte type, byte* Info, byte NumOfBytes)
/*----------------------------------------------------------------------------*/
{
#if 0  
   union
   {
      byte      t_byte[4];
      usint     t_word[2];
   } u_num;
   
  
   switch (type)
   {
      case 0:
         u_num.t_byte[0] = Info[0];
         u_num.t_byte[1] = Info[1];
         u_num.t_byte[2] = Info[2];
         u_num.t_byte[3] = Info[3];
         //printf("Timer typ %u  time %u\n", u_num.t_word[0], u_num.t_word[1]);
         break;
     
   }
#endif  
}



/******************************************************************************/
/******************************************************************************/
/*                            End of file                                     */
/******************************************************************************/
/******************************************************************************/
