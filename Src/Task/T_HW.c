/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  MAX_IO         3
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
//byte    T_HW_TamperGlobalStatus;
static	tTimerSec				   task_TimerSec;
static	tTimerMsec				   task_TimerMsec[MAX_IO];
static	bool                    task_fPowerup;
static   bool                    task_fCall_B2B;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						Task_Hardware_HandleTimer  ( void);
static   void                 Task_Hardware_BuzzAPI_Init ( void);
static   void                 Task_Hardware_LedAPI_Init  ( void);
static	void						Task_Hardware_HandleEvent  ( void);
static	void						Task_Hardware_HandleIO     ( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Hardware_Init( void)
/*----------------------------------------------------------------------------*/
{
   task_fPowerup  =  TRUE;
   task_fCall_B2B =  FALSE;
   //T_HW_TamperGlobalStatus = IO_STATUS_CLOSED;
}



/*----------------------------------------------------------------------------*/
void                    		Task_Hardware_Main( bool   fInternalCall)
/*----------------------------------------------------------------------------*/
{
   if( App_TaskStatus[TASK_HW] == DISABLED)
	return;

   _WDR();

   // Drivers (DRV files)
   // API's (API files)
   
   BuzzAPI_Main();
   ClockAPI_Main( fInternalCall);
   LcdAPI_Main();
   LedAPI_Main();
   InOutAPI_Main(); 
   if( fInternalCall == TRUE)
   {
      App_ResetGuard =  0xFF; // to prevent WDR [see also main() function]
      if( task_fCall_B2B == TRUE)
         Task_B2B_Master_Main();
      return;
   }
   task_fCall_B2B =  FALSE;

   KeyboardAPI_Main();     // !!!

   EsqrAPI_Main();
   xSramAPI_Main();
   _WDR();

   if( App_fReady == FALSE)
      return;
   Task_Hardware_HandleTimer();
   Task_Hardware_HandleEvent();
   Task_Hardware_HandleIO();
   
   Debug_Main();
   
   task_fPowerup  =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                          Task_Hardware_Call_B2B( bool   fMode)
/*----------------------------------------------------------------------------*/
{
   task_fCall_B2B =  fMode;
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Hardware_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;
byte                 Idx;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( task_TimerSec    <  Gap)  task_TimerSec     =  0; else  task_TimerSec     -= Gap;

      for( Idx = 0; Idx < MAX_IO; Idx ++)
         if( task_TimerMsec[Idx] <  Gap)  task_TimerMsec[Idx]  =  0; else  task_TimerMsec[Idx]  -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hardware_HandleEvent( void)
/*----------------------------------------------------------------------------*/
{
   App_TaskEvent[TASK_HW]  =  EV_APP_VOID;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Hardware_HandleIO( void)
/*----------------------------------------------------------------------------*/
{
static   tIO_Status     Status_Current[MAX_IO];
static   tIO_Status     Status_Last[MAX_IO];
static   tIO_Status     Status_Temp[MAX_IO];
static   usint          Time[MAX_IO];
byte     Idx;


   if( task_fPowerup == TRUE)
   {
      for( Idx = 0; Idx < MAX_IO; Idx ++)
      {
         Status_Last[Idx]     =  IO_STATUS_CLOSED;
         Status_Current[Idx]  =  IO_STATUS_CLOSED;
         Time[Idx]            =  1000;

         if( Idx == 2)  // Coins safebox
            Time[Idx]         =  3000;
      }
   }

   Status_Temp[0] =  InOutAPI_GetStatus( INPUT_DSR_A, TRUE);   // Door's main lock
   Status_Temp[1] =  InOutAPI_GetStatus( INPUT_CTS_D, TRUE);   // unassigned
   Status_Temp[2] =  InOutAPI_GetStatus( INPUT_U18_0, TRUE);   // Coins safebox

   for( Idx = 0; Idx < MAX_IO; Idx ++)
   {
      if( Status_Last[Idx] != Status_Temp[Idx])
      {
         Status_Last[Idx]     =  Status_Temp[Idx];
         task_TimerMsec[Idx]  =  Task_Timer_SetTimeMsec( Time[Idx]);
      }

      if( (Status_Current[Idx] != Status_Last[Idx]) && (task_TimerMsec[Idx] == 0))
      {
         Status_Current[Idx]  =  Status_Last[Idx];
         
         switch( Idx)
         {
            case  0:
                  if( Status_Current[Idx] == IO_STATUS_OPENED)  
                  {
                     SET_BIT( App_TaskEvent[TASK_HW], EV_APP_HW_DOOR_LOCK_OPENED);
                     Task_Application_SetEvent_OpenDoor();
                  }
                  if( Status_Current[Idx] == IO_STATUS_CLOSED)  
                  {
                     SET_BIT( App_TaskEvent[TASK_HW], EV_APP_HW_DOOR_LOCK_CLOSED);
                     Task_Application_SetEvent_CloseDoor();
                  }
                  break;

            case  1:
                  break;

            case  2:
                  if( Status_Current[Idx] == IO_STATUS_OPENED)  SET_BIT( App_TaskEvent[TASK_HW], EV_APP_HW_SAFE_REMOVED);
                  if( Status_Current[Idx] == IO_STATUS_CLOSED)  SET_BIT( App_TaskEvent[TASK_HW], EV_APP_HW_SAFE_RETURNED);
                  break;
         }
      }
   }
}

