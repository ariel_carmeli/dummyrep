/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
//void                          Util_StrCat_Const( char *pStrDest, PGM_P pStrConst);  // see IAR\..CLIB\..pgmspace.h
//void                          Util_Str2BCD_Memset( char   *pStr, byte *pBCD, byte SizeOfBCD, byte   memsetValue);

#define  TIME_SERVER_IS_ACTIVE            10
#define  CREDIT_CARD_SERVER_TIMOUT        30

#define  FLAG_CLEAR_LINE_1                BIT_0 // LCD_ClearLine (1)
#define  FLAG_CLEAR_LINE_2                BIT_1 // LCD_ClearLine (2)
#define  FLAG_CLEAR_LINE_3                BIT_2 // LCD_ClearLine (3)
#define  FLAG_CLEAR_LINE_4                BIT_3 // LCD_ClearLine (4)

#define  FLAG_ERROR_SLAVE_OFF             BIT_0 // 00000000 00000001    0x0001
#define  FLAG_ERROR_READER_1_OFF          BIT_1 // 00000000 00000010    0x0002
#define  FLAG_ERROR_READER_2_OFF          BIT_2 // 00000000 00000100    0x0004
#define  FLAG_ERROR_BILL_VAL_OFF          BIT_3 // 00000000 00001000    0x0008
#define  FLAG_ERROR_BILL_VAL_JAM          BIT_4 // 00000000 00010000    0x0010
#define  FLAG_ERROR_BILL_VAL_STACKER_FULL BIT_5 // 00000000 00100000    0x0020
#define  FLAG_ERROR_PRINTER_OFF           BIT_6 // 00000000 01000000    0x0040
#define  FLAG_ERROR_PRINTER_OUT_OF_PAPER  BIT_7 // 00000000 10000000    0x0080
#define  FLAG_ERROR_INVALID_MACHINE_ID    BIT_8 // 00000001 00000000    0x0100

#define  FLAG_WARNING_HOOPER_1_LOW_LEVEL  BIT_0 // 00000000 00000001    0x0001
#define  FLAG_WARNING_HOOPER_2_LOW_LEVEL  BIT_1 // 00000000 00000010    0x0002
#define  FLAG_WARNING_HOOPER_3_LOW_LEVEL  BIT_2 // 00000000 00000100    0x0004
#define  FLAG_WARNING_HOOPER_4_LOW_LEVEL  BIT_3 // 00000000 00001000    0x0008
#define  FLAG_WARNING_PRINTER_EOP_SENSOR  BIT_4 // 00000000 00010000    0x0010
#define  FLAG_WARNING_CHANGE_LOW_LEVEL    BIT_5 // 00000000 00100000    0x0020
#define  FLAG_WARNING_SERVER_OFFLINE      BIT_6 // 00000000 01000000    0x0040

#define  DEVICE_NONE                      0
#define  DEVICE_BARCODE_READER_1          BIT_0
#define  DEVICE_BARCODE_READER_2          BIT_1
#define  DEVICE_COIN_VALIDATOR            BIT_2
#define  DEVICE_BILL_VALIDATOR            BIT_3
#define  DEVICE_MGC_READER                BIT_4
#define  DEVICE_BUTTON_SELECTION_1        BIT_5
#define  DEVICE_BUTTON_SELECTION_2        BIT_6
#define  DEVICE_BUTTON_SELECTION_3        BIT_7
#define  DEVICE_BUTTON_LANGUAGE           BIT_8
#define  DEVICE_BUTTON_RECEIPT            BIT_9
#define  DEVICE_BUTTON_CANCEL             BIT_10
#define  DEVICE_CHANGE_POCKET             BIT_11
#define  DEVICE_ALL                       0xFFFF

#define  BUTTON_SELECTION_1               B2B_STATUS_BUTTON_5
#define  BUTTON_SELECTION_2               B2B_STATUS_BUTTON_6
#define  BUTTON_SELECTION_3               B2B_STATUS_BUTTON_1
#define  BUTTON_LANGUAGE                  B2B_STATUS_BUTTON_2
#define  BUTTON_RECEIPT                   B2B_STATUS_BUTTON_3
#define  BUTTON_CANCEL                    B2B_STATUS_BUTTON_4

#define  PRODUCT_1                        0
#define  PRODUCT_2                        1
#define  PRODUCT_3                        2
#define  PRODUCT_LAST                     MAX_PRODUCTS   // see T_App.h

/*---------------------------- Typedef Directives ----------------------------*/
// task_OccasionClient_State values:
typedef enum
{
   T_SELECT_BUTTONS_IDLE = 0    ,
   T_SELECT_BUTTONS_TO_OFF      ,
   T_SELECT_BUTTONS_IN_OFF      ,
   T_SELECT_BUTTONS_TO_ON       ,
   /****************************/
   T_SELECT_BUTTONS_LAST
} tOccasionClientState;


typedef  enum
{
   STATE_IDLE                       ,  //    0
   STATE_CRITICAL_ERROR             ,  //    1
   STATE_CANCEL                     ,  //    2
   STATE_DISABLED                   ,  //    3
   STATE_DISABLED_BY_SETUP          ,  //    4
   STATE_DISABLED_BY_SERVER         ,  //    5
   STATE_SHOW_ERRORS_AND_WARNINGS   ,  //    6
   STATE_GET_READY                  ,  //    7
   STATE_VGA_SCREEN_1               ,  //    8
   STATE_GET_PRODUCTS_AND_PAYMENT   ,  //    9
   STATE_MGC_GET_TRACK              ,  //   10
   STATE_MGC_CREDIT_CARD_1          ,  //   11
   STATE_MGC_CREDIT_CARD_2          ,  //   12
   STATE_MGC_DIGITEL_1              ,  //   13
   STATE_MGC_DIGITEL_2              ,  //   14
   STATE_MGC_WAIT_MSG               ,  //   15
   STATE_DISPENSE_CHANGE            ,  //   16
   STATE_MONEY_BACK                 ,  //   17
   STATE_LOG_TRANSACTION            ,  //   18
   STATE_PRINT_TRANSACTION          ,  //   19
   STATE_WAIT_FOR_PRINTOUT_END      ,  //   20
   STATE_END_TRANSACTION            ,  //   21
   STATE_END_DURING_SETUP           ,  //   22    7/Nov/2016  A.C We are during Setup menu
   /******************/
   // In case we are in a "block ocassionally clients" mode...
   STATE_SET_BLOCK_OCCASSION_CLIENTS,  //   23          // Enter mode: Set buttons off, set MGC on.
   STATE_WAIT_MGC_DIGITEL_IDENT     ,  //   24          // 
   STATE_GET_MGC_DIGITEL_TRACK      ,  //   25
   /******************/
   STATE_LAST
}  tState;


typedef  enum
{
   TYPE_CHAR   ,
   TYPE_BYTE   ,
   TYPE_USINT  ,
   TYPE_ULONG  ,
   TYPE_STRING ,
}  tType;


typedef  enum
{
   SPRINTF_STR_1                    ,  //
   SPRINTF_STR_2                    ,  //
   SPRINTF_STR_3                    ,  //
   /******************/
   SPRINTF_LAST
}  tSprintf;


typedef  enum
{
   EVENT_VOID                       =  0     ,
   EVENT_COIN_INSERTED              =  BIT_0 ,
   EVENT_BILL_STACKED               =  BIT_1 ,
   EVENT_TRANS_COMPLETE             =  BIT_2 ,
   EVENT_BUTTON_PRESSED_SELECT_1    =  BIT_3 ,
   EVENT_BUTTON_PRESSED_SELECT_2    =  BIT_4 ,
   EVENT_BUTTON_PRESSED_SELECT_3    =  BIT_5 ,
   EVENT_BUTTON_PRESSED_LANGUAGE    =  BIT_6 ,
   EVENT_BUTTON_PRESSED_RECEIPT     =  BIT_7 ,
   EVENT_BUTTON_PRESSED_CANCEL      =  BIT_8 ,
   EVENT_TERMINATE_Z                =  BIT_10,
   EVENT_RESET_DEVICE               =  BIT_11,
   EVENT_TX_CHANGER_STATUS          =  BIT_12,
   /******************/
   EVENT_LAST
}  tEvent;


typedef  enum
{
   WARNING_VOID                     =  0     ,
   WARNING_SERVER_OFFLINE           =  BIT_0 ,  // 0x0001
   WARNING_PRINTER_EOP_SENSOR       =  BIT_1 ,  // 0x0002
   WARNING_PRINTER_PAPER_OUT        =  BIT_2 ,  // 0x0004
   WARNING_PRINTER_OFF              =  BIT_3 ,  // 0x0008
   WARNING_LOW_CHANGE_LEVEL         =  BIT_4 ,  // 0x0010
   WARNING_BILL_VALIDATOR           =  BIT_5 ,  // 0x0020
   WARNING_COIN_CHANGER             =  BIT_6 ,  // 0x0040
   WARNING_MISMATCH_MACHINE_ID      =  BIT_15,  // 0x8000
   /******************/
   WARNING_LAST
}  tWarning;


typedef  enum
{
   PROCESS_IDLE                     ,
   /******************/
   PROCESS_LAST
}  tProcess;


typedef  enum
{
   VGA_MSG_OUT_OF_ORDER             ,
   VGA_MSG_TOTAL_LABEL              ,
   VGA_MSG_DISCOUNT                 ,
   VGA_MSG_PAID_LABEL               ,
   VGA_MSG_DUE_LABEL                ,
   VGA_MSG_CHANGE                   ,
   VGA_MSG_TRANSACTION_CANCELLED    ,
   VGA_MSG_PLEASE_WAIT              ,
   VGA_MSG_CHECKING_CARD            ,
   VGA_MSG_MGC_ACTION_FAILED        ,
   VGA_MSG_MGC_DIGITEL_OVER_LIMIT   ,
   VGA_MSG_CARD_IS_DENIED           ,
   VGA_MSG_AFTER_CASH_PAYMENT       ,
   VGA_MSG_MGC_NO_ELIGIBILITY       ,
   VGA_MSG_MGC_DISCOUNT_GRANTED     ,
   VGA_MSG_MGC_IN_VALUE_OF          ,
   VGA_MSG_TAKE_THE_RECEIPT         ,
   VGA_MSG_AND_THE_CHANGE           ,  
   VGA_MSG_TIBA_PAY_COMM_LOSS       ,           // In case of Tib@Pay communication loss...
   VGA_MSG_NO_CREDIT_SERVICE        ,
   VGA_MSG_PRESENT                  ,
   VGA_MSG_DIGITEL_CARD             ,
   /******************/
   VGA_MSG_MAX_MESSAGES
}  tExtLCD_MsgCode;


typedef  enum
{
   VGA_SCREEN_VOID                  ,
   VGA_SCREEN_READY                 ,
   VGA_SCREEN_OUT_OF_ORDER          ,
   VGA_SCREEN_PLEASE_WAIT           ,
   VGA_SCREEN_MGC_ACTION_FAILED     ,
   VGA_SCREEN_MGC_DIGITEL_OVER_LIMIT,
   VGA_SCREEN_MGC_USAGE_DENIED      ,
   VGA_SCREEN_MGC_NO_ELIGIBILITY    ,
   VGA_SCREEN_MGC_DISCOUNT_GRANTED  ,
   VGA_SCREEN_RECEIPT_AND_CHANGE    ,
   VGA_SCREEN_TRANSACTION_CANCELLED ,
   VGA_SCREEN_TIBAPAY_COMM_LOSS     ,
   VGA_SCREEN_NO_CREDIT_SERVICE     ,
   VGA_SCREEN_PRESENT_DIGITEL_CARD  
   /*********************/
}  tDisplay_Screen;


typedef  enum
{
   VGA_ADD_ON_VOID				      =  0    	,
   VGA_ADD_ON_INFO      	         =  BIT_0	,
   VGA_ADD_ON_CLOCK				      =  BIT_1	,
   VGA_ADD_ON_DEBUG_INFO		      =  BIT_2	,
}  tDisplay_AddOn;


typedef  struct
{
   byte        EvCode;
   byte        EvType;
}  tServerEvent;


typedef  struct
{
   bool        fChangeLowLevel;
   bool        fMachineDoor_Lock;
   bool        fMachineDoor;
   bool        fCoinsSafe;
   bool        fBillsStacker;
   bool        fCoinsChanger;
   bool        fBillValidator;
   byte        Printer;
}  tAlert;


typedef  struct
{
   byte        Offset;                       // Offset (position); From which point (in a specific line) the context should be displayed
   char        Text[LCD_LINE_WIDTH + 1];     // Text to display
   ulong       Value;                        // Price / Number to show
   byte        Digits;                       // Digits (length) of the number
   bool        Justify;                      // Justify text (void / left / center / right)
   char        Char;                         // Character to show
}  tLCD;                                     // LCD_WritePrice, LCD_WriteNumber, LCD_WriteNumberHex, LCD_WriteChar


typedef  struct
{
   byte        Font_Type;
   byte        Font_Height;
   byte        Font_Width;
   byte        Text_Alignment;
   byte       *pText;
   byte        Cutter_Mode;
   byte        Barcode_Type;
   ulong       Barcode_Number;
   usint       Count;
}  tPrinter;


typedef  struct
{
   byte        Mode;                         // Mode
   byte        Time;                         // Time
}  tModeAndTime;


typedef  struct
{
   byte        Type;
   ulong       ConfirmationCode;
   ulong       AuthorizationCode;
}  tMagStripeCard;


typedef  struct
{  /* NOTE !!! Each member MUST be from type of ulong (size of tParams_C_Rec) */
   ulong       Code;
   ulong       Price;
   ulong       Discount_Money;
   ulong       Discount_Percentage;
   ulong       MaxUnitsPerTrans;
   ulong       Spare;
}  tProduct;   // See T_Params.h | PARAM_C_PROD_xx_?????


CONST	static   tServerEvent   ServerEvents[] =
                           {
                              {VMC_EVENT_TRANSACTION_DONE            ,  1},   // 1
                              {VMC_EVENT_COIN                        ,  3},   // 2
                              {VMC_EVENT_BILL                        ,  0},   // 3
                              {VMC_EVENT_OPENDOOR                    ,  0},   // 4
                              {VMC_EVENT_CLOSEDOOR                   ,  0},   // 5
                              {VMC_EVENT_EMPTY_SAFE                  ,  3},   // 6
                              {VMC_EVENT_EMPTY_CENTRAL               ,  3},   // 16
                              {VMC_EVENT_CANCEL_PAYMENT              ,  0},   // 18
                              {VMC_EVENT_ODEF_CAL                    ,  1},   // 27
                              {VMC_EVENT_ODEF_ISSUE                  ,  1},   // 28
                              {VMC_EVENT_ALERT_ON                    ,  2},   // 29 Variable, default data type is 2
                              {VMC_EVENT_ALERT_OFF                   ,  2},   // 30 Variable, default data type is 2
                              {VMC_EVENT_TIME_AND_ATTEND_IN          ,  1},   // 98
                              {VMC_EVENT_TIME_AND_ATTEND_OUT         ,  1},   // 99
                              {VMC_EVENT_KEEP_COINS                  ,  4},   // 101
                              {VMC_EVENT_KEEP_BANKNOTES              ,  3},   // 102
                              {VMC_EVENT_KEEP_CENTRAL                ,  3},   // 104
                              {VMC_EVENT_KEEP_CENTRAL_2              ,  3},   // 105
                              {VMC_EVENT_MEMBER_PASSAGE              ,  2},   // 130
                              {VMC_EVENT_OPEN_GATE                   ,  1},   // 131
                              {VMC_EVENT_CLOSE_GATE                  ,  1},   // 132
                              {VMC_EVENT_GATE_OPENED                 ,  1},   // 133
                              {VMC_EVENT_GATE_CLOSED                 ,  1},   // 134
                              {VMC_EVENT_LOW_PAPER                   ,  0},   // 147
                              {VMC_EVENT_NO_PAPER                    ,  0},   // 148
                              {VMC_EVENT_TIBA_PAY_COMM_LOSS          ,  0},   // 160   //        0xA0
                              {VMC_EVENT_TIBA_PAY_COMM_BACK          ,  0},   // 161   //        0xA1
                              {VMC_EVENT_TIBA_PAY_TRANSACTION_END    ,  2},   // 162   //        0xA2
                              {VMC_EVENT_DIGITEL_ACCEPTED            ,  0},   // 163   ,  //         0xA3           ����� ����� ������
                              {VMC_EVENT_DIGITEL_ENTITLEMENT_CHECK   ,  0},   // 164   ,  //         0xA4           ������-����� ����� �������
                              {VMC_EVENT_DIGITEL_ENTITLEMENT_ANSWER  ,  0},   // 165   ,  //         0xA5           ������-����� ���� �� �����
                              {VMC_EVENT_CREDIT_C_ACCEPTED           ,  0},   // 166   ,  //         0xA6           Credit card (C.C) was inserted with card details
                              {VMC_EVENT_CREDIT_C_REQUEST_TO_CONFIRM ,  0},   // 167   ,  //         0xA7           C.C request to confirm
                              {VMC_EVENT_CREDIT_C_REQUEST_ANSWER     ,  0},   // 168   ,  //         0xA8           C.C request answer (confirmed or not)
                              // ----------------------------------------------------
                              {VMC_EVENT_BUTTON_PRESSED              ,  0},   // 170   ,  //         0xAA           Any button pressed with number of the button
                              {VMC_EVENT_BILL_DROP_CASSETTE_REMOVAL  ,  0},   // 171   ,  //         0xAB           Bill Drop cassette is removed with how many bills were inside
                              {VMC_EVENT_BILL_DROP_CASSETTE_REPLACEMENT, 0},  // 172   ,  //         0xAC           Bill Drop cassette is is replaced back
                              {VMC_EVENT_COIN_CENTRAL_REMOVAL        ,  0},   // 173   ,  //         0xAD           COIN Central is removed with how many coins were inside
                              {VMC_EVENT_COIN_CENTRAL_REPLACEMENT    ,  0},   // 174   ,  //         0xAE           COIN Central is replaced back
                              {VMC_EVENT_COIN_HOPER_FILL             ,  0},   // 175   ,  //         0xAF           
                              {VMC_EVENT_EXCESS_CALCULATED           ,  0},   // 176   ,  //         0xB0           
                              {VMC_EVENT_EXCESS_RETURN               ,  0},   // 177   ,  //         0xB1           
                              {VMC_EVENT_COIN_HOPPER_REFILL          ,  0},   // 178   ,  //         0xB2           
                              // ---------------------------------------------------
                              {VMC_EVENT_MIKVE_OPENED                ,  0},   // 200
                              {VMC_EVENT_MIKVE_CLOSED                ,  0},   // 201
                              {VMC_EVENT_DEBT_CREATION               ,  2},   // 202
                              {VMC_EVENT_DEBT_PAYMENT                ,  2},   // 203
                              {VMC_EVENT_APPL_POWERUP                ,  1},   // 204
                              {VMC_EVENT_TOTAL_VAL_COINS             ,  1},   // 205
                              {VMC_EVENT_TOTAL_VAL_CENTRAL           ,  1},   // 206
                              {VMC_EVENT_TOTAL_VAL_BILLS             ,  1},   // 207
                              {VMC_EVENT_DEBUG_CARD_DETECTED         ,  2},   // 220
                              {VMC_EVENT_DEBUG_CARD_REMOVED          ,  2},   // 221
                              {VMC_EVENT_DEBUG_CARD_UPDATE           ,  2},   // 222
                              {VMC_EVENT_DEBUG_CARD_WRITE_OK         ,  2},   // 223
                              {VMC_EVENT_DEBUG_CARD_READ_ERR         ,  2},   // 224
                              {VMC_EVENT_DEBUG_CARD_WRITE_ERR        ,  2},   // 225
                              {VMC_EVENT_DEBUG_RFID_DB_VER           ,  2},   // 226
                              {VMC_EVENT_DEBUG_RFID_APP_VER          ,  2},   // 227
                              {VMC_EVENT_DEBUG_RFID_SITE_CODE        ,  2},   // 228
                              {VMC_EVENT_DEBUG_RFID_ZONE_CODE        ,  2},   // 229
                              {VMC_EVENT_DEBUG_RFID_DEVICE_TYPE      ,  2},   // 230
                              {VMC_EVENT_DEBUG_RFID_LOW_CREDIT       ,  2},   // 231
                              {VMC_EVENT_DEBUG_RFID_CARD_EXPIRED     ,  2},   // 232
                              {VMC_EVENT_DEBUG_RFID_OVER_USAGE       ,  2},   // 233
                              {VMC_EVENT_DEBUG_RFID_BACK_TOO_SOON    ,  2},   // 234
                              {VMC_EVENT_DEBUG_RFID_RENEW_PRICE_0    ,  2},   // 235
                              {VMC_EVENT_DEBUG_CC_TX_TO_SERVER       ,  2},   // 236
                              {VMC_EVENT_DEBUG_CC_TIMEOUT            ,  2},   // 237
                              {VMC_EVENT_DEBUG_CC_SERVER_REPLY_OK    ,  2},   // 238
                              {VMC_EVENT_DEBUG_CC_SERVER_REPLY_ERR   ,  2},   // 239
                              {VMC_EVENT_DEBUG_DGTL_TX_TO_SERVER     ,  2},   // 240
                              {VMC_EVENT_DEBUG_DGTL_TIMEOUT          ,  2},   // 241
                              {VMC_EVENT_DEBUG_DGTL_SERVER_REPLY_OK  ,  2},   // 242
                              {VMC_EVENT_DEBUG_DGTL_SERVER_REPLY_ERR ,  2},   // 243
                              {VMC_EVENT_DEBUG_DGTL_NO_ELIGIBILITY   ,  2},   // 244
                              /******************/
                              {VMC_EVENT_LAST                     , INVALID_VALUE}, // this event MUST be the last one
                           };

CONST static   char		   Sprintf_Strings[SPRINTF_LAST][20 + 1]	=
                           {//"                    "
                              "[%u]"                           ,  // SPRINTF_STR_1
                              "%u.%.2u %s"                     ,  // SPRINTF_STR_2
                              "%.4X "                          ,  // SPRINTF_STR_3
                           };

CONST	static   char        Messages[VGA_MSG_MAX_MESSAGES][MAX_LANGUAGES_EXTENDED][20 + 1]   =
                           {              //    "                    "     "                    "
/* VGA_MSG_OUT_OF_ORDER                   */ {	" Temp. Out of Order "  ,  "** ����� �� ���� ** "  }  ,  // Font 3   [22]
/* VGA_MSG_TOTAL_LABEL                    */ {	"Total               "  ,  "��\"�               "  }  ,  // Font 7 / Font 5
/* VGA_MSG_DISCOUNT                       */ {	"Disc.               "  ,  "����                "  }  ,  // Font 5   [17]
/* VGA_MSG_PAID_LABEL                     */ {	"Paid                "  ,  "����                "  }  ,  // Font 5   [17]
/* VGA_MSG_DUE_LABEL                      */ {	"Due                 "  ,  "����                "  }  ,  // Font 5   [17]
/* VGA_MSG_CHANGE                         */ {	"Change              "  ,  "����                "  }  ,  // Font 5   [17]
/* VGA_MSG_TRANSACTION_CANCELLED          */ {	"Cancelled"             ,  "������ �����"          }  ,  // Font 6   [14]
/* VGA_MSG_PLEASE_WAIT                    */ {	"Please Wait...      "  ,  "��� ����            "  }  ,  // Font 6   [14]
/* VGA_MSG_CHECKING_CARD                  */ {	"Checking Card       "  ,  "����� ������        "  }  ,  // Font 6   [14]
/* VGA_MSG_MGC_ACTION_FAILED              */ {	"Action Failed       "  ,  "������ �����        "  }  ,  // Font 6   [14]
/* VGA_MSG_MGC_DIGITEL_OVER_LIMIT         */ {	" Card Usage Limited "  ,  "����� ����� ������� "  }  ,  // Font 4   [22]
/* VGA_MSG_CARD_IS_DENIED                 */ {	"Card is Denied      "  ,  "������ ������ ����  "  }  ,  // Font 4   [22]
/* VGA_MSG_AFTER_CASH_PAYMENT             */ {	"After Cash Payment  "  ,  "���� ����� ������   "  }  ,  // Font 4   [22]
/* VGA_MSG_MGC_NO_ELIGIBILITY             */ {	"No Eligibility      "  ,  "�� ���� �����       "  }  ,  // Font 6   [14]
/* VGA_MSG_MGC_DISCOUNT_GRANTED           */ {	"Discount Granted    "  ,  "���� ������ �����   "  }  ,  // Font 5   [17]
/* VGA_MSG_MGC_IN_VALUE_OF                */ {	"In Value Of         "  ,  "����� ��            "  }  ,  // Font 5   [17]
/* VGA_MSG_TAKE_THE_RECEIPT               */ {	"Take The Receipt    "  ,  "�� ���� �� �����    "  }  ,  // Font 5   [17]
/* VGA_MSG_AND_THE_CHANGE                 */ {	"And the Change      "  ,  "��� �����           "  }  ,  // Font 5   [17]
/* VGA_MSG_TIBA_PAY_COMM_LOSS             */ {  "Service not active  "  ,  "������ ���� ����    "  }  ,  // Font #   [# ]
/* VGA_MSG_NO_CREDIT_SERVICE              */ {  "Cash Only           "  ,  "����� ����� �� ���� "  }  ,   // 
/* VGA_MSG_PRESENT                        */ {  "Present             "  ,  "����                "  }  ,   // 
/* VGA_MSG_DIGITEL_CARD                   */ {  "Digitel Card        "  ,  "����� ������        "  }      // 
                           };


CONST static   char     LCD_Messages[MAX_MESSAGES][MAX_LANGUAGES_EXTENDED][LCD_LINE_WIDTH + 1]   =
                        {
                                       {  ""                ,  ""                },    // reserved for MSG_Lib_APP_VER
                                       {  ""                ,  ""                },    // reserved for MSG_Lib_APP_DATE
                                       {  ""                ,  ""                },    // reserved for MSG_Lib_APP_CODE
/* MSG_READY                        */ {  "     Ready      ",  "���� ������     "},
/* MSG_PRESS_ANY_KEY                */ {  "Press Any Key...",  "��� �� ��� �����"},
/* MSG_SETUP_ERR_1                  */ {  "Error: Qtty = 0 ",  "�����: ���� = 0 "},
/* MSG_SET_QTTY_EMPTYING            */ {  "Set Emptying Qty",  "���� ���� ������"},

// Errors
/* MSG_ERROR                        */ {  "CRITICAL ERROR !",  "  ���� ����� !  "},
/* MSG_ERR_SLAVE_OFF                */ {  "Slave Ctrlr OFF ",  "���: ��� ���    "},
/* MSG_ERR_READER_1_OFF             */ {  "Reader 1 OFF    ",  "���: ���� ������"},
/* MSG_ERR_READER_2_OFF             */ {  "Reader 2 OFF    ",  "���: ���� ����� "},
/* MSG_ERR_BILL_VAL_OFF             */ {  "Bill Val. OFF   ",  "���: ���� ����� "},
/* MSG_ERR_BILL_VAL_JAM             */ {  "Bill Val. Jammed",  "��� ����        "},
/* MSG_ERR_BILL_VAL_STACKER_FULL    */ {  "Bill Val. Full  ",  "������ ��� ���� "},
/* MSG_ERR_PRINTER_OFF              */ {  "Printer OFF     ",  "���: �����      "},
/* MSG_ERR_PRINTER_OUT_OF_PAPER     */ {  "End of Paper    ",  "��� ���� ������ "},

// Warnings
/* MSG_WARNING                      */ {  "    WARNING !   ",  "    ����� !     "},
/* MSG_WARNING_HOOPER_1_LOW_LEVEL   */ {  "Low Level (H1)  ",  "���� ������� )A("},
/* MSG_WARNING_HOOPER_2_LOW_LEVEL   */ {  "Low Level (H2)  ",  "���� ������� )B("},
/* MSG_WARNING_HOOPER_3_LOW_LEVEL   */ {  "Low Level (H3)  ",  "���� ������� )C("},
/* MSG_WARNING_HOOPER_4_LOW_LEVEL   */ {  "Low Level (H4)  ",  "���� ������� )D("},
/* MSG_WARNING_PRINTER_EOP_SENSOR   */ {  "Low Level Paper ",  "���� ���� ������"},
/* MSG_WARNING_CHANGE_LOW_LEVEL     */ {  "Change Low Level",  "��� ����� ����  "},
                        };

/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tTimerSec            task_Timer_Wait;
//static   tTimerSec            task_Timer_Cancel;
//static   tTimerSec            task_Timer_ChangePocketLight;
static   tTimerSec            task_Timer_Errors;
static   tTimerSec            task_Timer_Warnings;
static   tTimerSec            task_Timer_ButtonPressed;
static   tTimerSec            task_Timer_VoidPrinter;
static   tTimerMsec           task_Timer_EnableCV;
static   tTimerMsec           task_Timer_EnableBV;
static   tTimerMsec           task_Timer_GetInfo;
static   tParams_A_Rec        task_Param_A_Rec;
static   tParams_C_Rec        task_Param_C_Rec;
static   tParams_E_Rec        task_Param_E_Rec;
static   tParams_Var_Rec      task_Param_Var;
static   tHoppers_Info       *task_pHoppers_Info;
static   tApp_Status          task_Status;
static   tState               task_State;
//
static   tState               task_State_Prev;          // Debug
//
static   tEvent               task_Event;
static   ulong                task_ResidentCard_Discount;
static   usint                task_Flags_Error;    // FLAG_ERROR_???
static   usint                task_Flags_Warning;  // FLAG_WARNING_???
static   usint                task_fReqToEnable;
static   usint                task_fReqToDisable;
static   usint                task_fDeviceEnabled;
static   byte                 task_ResidentCards;
static   byte                 task_EquipmentID;
static   byte                 task_Slave_UnitType;
static   byte                 task_Slave_Address;
static   byte                 task_DefaultLanguage;
static   byte                 task_ErrMsgState;
static   byte                 task_ErrCode;
static   bool                 task_fPowerUp;
static   bool                 task_fUpdateInfo;
static   byte                 task_KB_Key;
static   bool                 task_fGetKey_Setup;
static   bool                 task_fDoorIsOpened;
static   bool                 task_fAutoCancel;
static   bool                 task_fVoidButton_Cancel;

static   Cmd_Q_Data          *task_pStatusStruct;

static   bool                 task_fDoAction;
static   byte                 task_DoAction_Code;

static   Cmd_E_Products       task_Emergency_Products;

// Cmd_L
static   tLogTrans_Rec        task_LogTrans_Rec;
static   tLogEvent_Rec        task_LogEvent_Rec;
static   tLogEvent_Data       task_Event_Data;
static   byte                 task_LogTrans_RecType;
static   byte                 task_LogTrans_ItemsIdx;
static   byte                 task_LogTrans_CardsIdx;

static   tAlert               task_Alerts;
static   tAlert               task_Alerts_Last;

// 'tB2B_FromMaster' members
static   tB2B_Command         task_B2B_TxCommand;
static   tB2B_FromSlave      *task_B2B_pRxData;
static   tB2BPacket          *task_B2B_pRxPacket;
static   tModeAndTime         task_Buzzer;                  // Buzzer_SetOperation
static   usint                task_EnabledBills;            // Bill_Valid_EnabledBills
static   usint                task_EnabledCoins;            // Coin_Valid_EnabledCoins
static   usint                task_Coin_Value;              // Coin_Valid_SetCoin.Value
static   byte                 task_Coin_Routing;            // Coin_Valid_SetCoin.Routing
//static   tLCD                 task_LCD[APP_MAX_LCD_LINES];  // LCD_???
static   tPrinter             task_Printer;                 // Printer_???
static   byte                 task_LCD_Flags;               // LCD_SetTextBlinkingMode, LCD_SetCursorMode, LCD_SetBacklightMode
static   tModeAndTime         task_LED[APP_MAX_LEDS];       // LED_SetOperation

// Commands queue
static   tCommand             task_Q_Record;
static   byte                 task_Q_Cmd_WriteIdx;
static   byte                 task_Q_Cmd_ReadIdx;
static   bool                 task_Q_fIsEmpty;

static   tDisplay_Screen      task_Screen;
static   tDisplay_Screen      task_Screen_last;
static   tDisplay_AddOn       task_Display_AddOn;

static   tProduct             task_Product;
static   byte                 task_TransProducts_Selected[MAX_PRODUCTS];
static   byte                 task_TransProducts_Idx;
static   bool                 task_TransProducts_Disabled[MAX_PRODUCTS];

// for Task Setup
static   byte                 task_Test_State;
static   bool                 task_fTest_CoinRouting;

// Product Enable/Disable
static   bool                 task_ProductEnaDis_fChangeMode;
static   byte                 task_ProductEnaDis_ProductCode;
static   bool                 task_ProductEnaDis_fMode;

// MGC - Void Card
static   bool                 task_MGC_fVoidCard;
static   usint                task_MGC_XOR;
static   usint                task_MGC_XOR_last;
static   byte                 task_MGC_Len;
static   byte                 task_MGC_Len_last;
// For debug:
//static   bool                 task_CreditCardNotSupported;

// Printer Buffer
static   bool                 task_fPrinterIsBusy;
static   usint                task_PrinterBuf_W_Idx;
static   usint                task_PrinterBuf_R_Idx;
static   usint                task_PrinterBuf_Count;

// Monitor 
static   usint                task_Flags_Error_current;         // sense a change from one or more errors to 0 errors.
//static   bool                 task_fOnDoorClosed;

/*------------------------ Local Function Prototypes -------------------------*/
static	void			Task_Application_HandleTimer              ( void);
static	void			Task_Application_HandleExtEvent           ( void);
static	void			Task_Application_HandleIntEvent           ( void);
static   void                   Task_Application_ReadDB                   ( void);
static	void			Task_Application_HandleState              ( void);
static	void 			Task_Application_UpdateTransInfo          ( void);
static	void			Task_Application_ProductSelected          ( byte Product);

// MGC functions
static	void			Task_Application_MGC_SetCardType          ( void);
static	bool			Task_Application_MGC_IsDigitel            ( void);
static	void			Task_Application_MGC_CreditCard_1         ( void);
static	void			Task_Application_MGC_CreditCard_2         ( void);
static   void                   Task_Application_MGC_Digitel_1            ( void);
static	void			Task_Application_MGC_Digitel_2            ( void);
static  void                    Task_Application_BlockOccasClient         ( void);

static	bool			Task_Application_HandleDigitel_Discount   ( void);

static	void			Task_Application_LogTransaction           ( void);
static	void			Task_Application_LogTransaction4Printer   ( void);
static	void			Task_Application_LogTransaction4Setup     ( void);

static	void			Task_Application_SetEquipmentID           ( void);
static	void			Task_Application_IsMasterMachine          ( void);

static	void			Task_Application_HandleWarnings           ( void);
static	void			Task_Application_HandleErrors             ( void);
static	void			Task_Application_HandleButtons            ( void);
static	void			Task_Application_HandleBillValidator      ( void);
static   void                   Task_Application_HandlePrinter            ( void);
static   void                   Task_Application_HandleClock              ( void);
static	void			Task_Application_HandleCoinRouting        ( void);
static	void			Task_Application_HandleMachineStatus      ( void);

// Commands queue
static	void			Task_Application_HandleExtDevices         ( void);
static	void			Task_Application_HandleTx                 ( void);
static	void			Task_Application_SetTxData                ( void);
static	void			Task_Application_CommandQueueInit         ( void);
static	void			Task_Application_CommandQueueIn           ( byte  Command, byte  Sub_Command, byte   Index, byte Sub_Index, byte Value);
static	bool                    Task_Application_CommandQueueOut          ( void);

// External peripherals
static	void			Task_Application_Reader_SetMode           ( byte   Device, byte   Mode);
static	void			Task_Application_Reader_GetLabel          ( byte   Device);
static	void			Task_Application_Reader_HandleTicket      ( byte   Device, byte   Mode);
static	void			Task_Application_Bills_SetMode            ( byte   Mode);
static	void			Task_Application_Bills_GetValue           ( void);
static	void			Task_Application_Bills_HandleBill         ( byte   Mode);
static	void			Task_Application_Bills_EnableBills        ( usint  Enabled);
static	void			Task_Application_Buzzer_SetMode           ( byte   Mode, byte  Time);
static	void			Task_Application_Coins_SetMode            ( byte   Mode);
static	void			Task_Application_Coins_EnableCoins        ( usint  Enabled);
static	void			Task_Application_Coins_SetCoin            ( byte   Index,   usint  Value, byte Routing);
static	void			Task_Application_Coins_GetValue           ( void);
static	void			Task_Application_Led_SetMode              ( byte   Led,  byte   Mode, byte  Time);
static	void			Task_Application_Mgc_SetMode              ( byte   Mode);
static	void			Task_Application_Mgc_GetStatus            ( void);
static	void			Task_Application_Mgc_GetTrack2            ( void);

// TFT Display
static	void			Task_Application_Display_HandleScreen     ( void);
static	void			Task_Application_Display_HandleDebugInfo  ( void);
static   void                   Task_Application_Display_HandleAddOn      ( void);

// Product's parameters DB
static	void			Task_Application_Product_EnaDis           ( byte ProductCode, bool  fMode);
static	void			Task_Application_Product_Read             ( byte Idx);
static	void			Task_Application_Product_Write            ( byte Idx);

// Task Setup service functions
static	void			Task_Application_Test_CoinRouting         ( void);
static	void			Task_Application_ShowErrorsAndWarnings    ( void);

void                            Task_Application_ButtonPressedCancle      ( void);
void                            Task_Application_ChangePocketLight        ( void);

// Set Events (to send Server about special events)
void                            Task_Application_SetEvent_DigitelAccepted(void);
void                            Task_Application_SetEvent_Coin(usint Coin_Value);
void                            Task_Application_SetEvent_Bill(usint Bill_Value);
void                            Task_Application_SetEvent_ButtonPressed(byte Button);
void                            Task_Application_SetEvent_BillDropCassetteRemoval(tLogEvent_Data *pData);         // Stacker Out
void                            Task_Application_SetEvent_BillDropCassetteReplacement(void);     // Stacker In
void                            Task_Application_SetEvent_CoinCentralRemoval(tLogEvent_Data *pData);
void                            Task_Application_SetEvent_CoinCentralReplacement(void);
void                            Task_Application_SetEvent_DigitelEntitlementCheck(Cmd_E_Products* pProdBuf);
void                            Task_Application_SetEvent_DigitelEntitlementAnswer(Cmd_D_Data* pDigitel);
void                            Task_Application_SetEvent_CreditCardAccepted(void);
void                            Task_Application_SetEvent_CreditCardRequestConfirm(ulong TotalPrice);
void                            Task_Application_SetEvent_CreditCardRequestAnswer(ulong TotalPrice, usint Last4Digits);
void                            Task_Application_SetEvent_ExcessCalculated(ulong ExcessCalc);
void                            Task_Application_SetEvent_ExcessReturn(ulong ExcessReturned);



// Debug
static	void						Task_Application_Debug( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		Task_Application_Init( void)
/*----------------------------------------------------------------------------*/
{
   tParams_Var_Rec      task_Param_Var;
   
   task_Slave_UnitType     =  0;
   task_EquipmentID        =  0;
   task_Slave_Address      =  1;
   task_Timer_Wait         =  Task_Timer_SetTimeSec( 5);
   task_DefaultLanguage    =  App_Language;
   task_fPowerUp           =  TRUE;
   task_fGetKey_Setup      =  FALSE;

   task_B2B_pRxData        =  Task_B2B_GetRxDataPtr();
   task_B2B_pRxPacket      =  B2BAPI_GetData();
   task_pStatusStruct      =  Task_Server_GetStatusStructPtr();
   task_pHoppers_Info      =  Task_Hopper_GetInfo();

   Task_Application_Prn_Init();
   Task_Application_CommandQueueInit();
 
   task_Flags_Error_current = task_Flags_Error;
   //task_fOnDoorClosed       = FALSE;
   //task_OccasionClient_State = T_SELECT_BUTTONS_IDLE;             // occasionally client
}



/*----------------------------------------------------------------------------*/
void                    		Task_Application_Main( void)
/*----------------------------------------------------------------------------*/
{
	if( App_TaskStatus[TASK_APPL] == DISABLED)
		return;

   if( task_fPowerUp == TRUE)
   {
      Task_Application_ReadDB();
      task_Timer_Errors    =  Task_Timer_SetTimeSec( 2);
      task_Timer_Warnings  =  Task_Timer_SetTimeSec( 2);
   }

   Task_Application_HandleTimer();
   Task_Application_HandleExtEvent();
   Task_Application_HandleIntEvent();
   Task_Application_HandleState();
   Task_Application_UpdateTransInfo();
   Task_Application_HandleExtDevices();
   Task_Application_HandleTx();
   Task_Application_Display_HandleScreen();
   Task_Application_Display_HandleDebugInfo();
   Task_Application_Display_HandleAddOn();

   if( task_fTest_CoinRouting == TRUE)
      Task_Application_Test_CoinRouting();

   task_fPowerUp  =  FALSE;
}

char debug_msg[16];
/*----------------------------------------------------------------------------*/
void                          Task_Application_ExtractConstMsg( byte *pDestStr,   byte  MsgCode)
/*----------------------------------------------------------------------------*/
{
tParams_A_Rec   task_Rec_Type_A;
char*           pApp_code_string;

   if( MsgCode < MAX_LCD_API_MESSAGES)
   {
	switch (MsgCode)
	{
		case MSG_APP_VER:
			strcpy_P((char*)pDestStr, App_Version);                       
			break;

		case MSG_APP_DATE:
			strcpy((char*)pDestStr, App_Date);                             
			break;

		case MSG_APP_CODE:
			strcpy_P((char*)pDestStr, App_Code);
                        break;

		default:
			strcpy_P( (char*)pDestStr, LCD_Messages[MsgCode][App_Language]);                        
			break;
	}
   } 
}



/*----------------------------------------------------------------------------*/
bool                          Task_Application_IsMsgToHaltClkDisplay( byte MsgCode)
/*----------------------------------------------------------------------------*/
{
   return( TRUE);
}




/*----------------------------------------------------------------------------*/
void						         Task_Application_SetEvent( usint EvCode, byte   SubCmd, tLogEvent_Data  *pData)
/*----------------------------------------------------------------------------*/
{
byte     Type;
byte     Idx;


   if( pData == NULL)
   {
      putchar('B');     putchar('d');     putchar('E');     putchar('1');     putchar('\n');
      return;
   }

   Type  =  INVALID_VALUE;
   Idx   =  0;
   do
   {
      Type =   ServerEvents[Idx].EvType;
      if( ServerEvents[Idx].EvCode == EvCode)
         break;

      // Double-check to ensure we exit this uncontrolled loop
      if( ServerEvents[Idx].EvCode == VMC_EVENT_LAST) break;
      if( ServerEvents[Idx].EvType == INVALID_VALUE)  break;

      Idx   ++;
   }  while( Type != INVALID_VALUE);

   if( Type == INVALID_VALUE)
   {
      putchar('B');     putchar('d');     putchar('E');     putchar('2');     putchar('\n');
      return;
   }
  
   memset( &task_LogEvent_Rec, 0, sizeof( task_LogEvent_Rec));
   task_LogEvent_Rec.SubCmd      =  SubCmd;
   task_LogEvent_Rec.EvCode      =  EvCode;
   task_LogEvent_Rec.EquipmentID =  Task_Server_GetEquipmentID();
   task_LogEvent_Rec.DateAndTime =  ClockAPI_DateAndTimeToSeconds();
   memcpy( &task_LogEvent_Rec.Data, pData, sizeof( task_LogEvent_Rec.Data));

   Task_Log_Event_WriteRecord( &task_LogEvent_Rec);

   memset( pData, 0, sizeof( tLogEvent_Data));
}



/*----------------------------------------------------------------------------*/
void						         Task_Application_SetAlert( byte  AlertCode, byte   Value,   bool   fMode)
/*----------------------------------------------------------------------------*/
{
//usint    EvCode;
byte     Idx;
byte     IdxCount;

return;  // *** not applicable yet ***
#if 0
   EvCode         =  0;
   memset( &task_Event_Data, 0, sizeof( task_Event_Data));

   Idx      =  (AlertCode > 0) ? (AlertCode - 1) : 0;
   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (PARAM_C_ALERT_01_TIME + Idx));

   switch( AlertCode)
   {
      case  SUB_CMD_EV_ALERT_LOW_CHANGE_LEVEL   :
            task_Alerts.fChangeLowLevel   =  fMode;
            #if 0
            IdxCount =  (sizeof( task_pCoin_DB->Value) / sizeof( task_pCoin_DB->Value[0]));
            for( Idx = 0; Idx < IdxCount; Idx ++)
            {
               /*  1 NIS */   if( task_pCoin_DB->Value[Idx] == 100)  task_Event_Data.Type_3.Field1 =  task_pCoin_DB->CoinsInTube[Idx];
               /*  2 NIS */   if( task_pCoin_DB->Value[Idx] == 200)  task_Event_Data.Type_3.Field2 =  task_pCoin_DB->CoinsInTube[Idx];
               /*  5 NIS */   if( task_pCoin_DB->Value[Idx] == 500)  task_Event_Data.Type_3.Field3 =  task_pCoin_DB->CoinsInTube[Idx];
               /* 10 NIS */   if( task_pCoin_DB->Value[Idx] == 1000) task_Event_Data.Type_3.Field4 =  task_pCoin_DB->CoinsInTube[Idx];
            }
            #endif
            break;

      case  SUB_CMD_EV_ALERT_MACHINE_DOOR_LOCK  :       
            task_Alerts.fMachineDoor_Lock =  fMode;
            break;

      case  SUB_CMD_EV_ALERT_MACHINE_DOOR       :
            task_Alerts.fMachineDoor      =  fMode;
            break;

      case  SUB_CMD_EV_ALERT_COINS_SAFE         :
            task_Alerts.fCoinsSafe        =  fMode;
            if( fMode == ON)
            {
               task_Event_Data.Type_3.Field1 =  task_pStatusStruct->Qtty_Coins_Central_100;
               task_Event_Data.Type_3.Field2 =  task_pStatusStruct->Qtty_Coins_Central_200;
               task_Event_Data.Type_3.Field3 =  task_pStatusStruct->Qtty_Coins_Central_500;
               task_Event_Data.Type_3.Field4 =  task_pStatusStruct->Qtty_Coins_Central_1000;
            }
            break;

      case  SUB_CMD_EV_ALERT_BILLS_SAFE         :
            task_Alerts.fBillsStacker     =  fMode;
            if( fMode == ON)
            {
               task_Event_Data.Type_3.Field1 =  task_pStatusStruct->Qtty_Bills_20;
               task_Event_Data.Type_3.Field2 =  task_pStatusStruct->Qtty_Bills_50;
               task_Event_Data.Type_3.Field3 =  task_pStatusStruct->Qtty_Bills_100;
               task_Event_Data.Type_3.Field4 =  task_pStatusStruct->Qtty_Bills_200;
            }
            break;

      case  SUB_CMD_EV_ALERT_COINS_CHANGER      :
            task_Alerts.fCoinsChanger     =  fMode;
            break;

      #if 0
      case  SUB_CMD_EV_ALERT_BILL_VALIDATOR     :
            task_Event_Data.Type_5.Field1[0] =  Value;

            if( Value == 1) // Stacker is full
            {
               task_Event_Data.Type_5.Field1[1] =  task_pStatusStruct->Qtty_Bills_20;
               task_Event_Data.Type_5.Field1[2] =  task_pStatusStruct->Qtty_Bills_50;
               task_Event_Data.Type_5.Field1[3] =  task_pStatusStruct->Qtty_Bills_100;
               task_Event_Data.Type_5.Field1[4] =  task_pStatusStruct->Qtty_Bills_200;
            }

            task_Alerts.fBillValidator       =  fMode;
            break;
      #endif
      #if 0
      case  SUB_CMD_EV_ALERT_PRINTER            :
            task_Alerts.Printer  =  0;
            if( fMode == ON)
            {
               SET_BIT  ( task_Alerts.Printer, (1 << Value));
               SET_BIT  ( task_Event_Data.Type_5.Field1[0], (1 << Value));
            }
            break;
      #endif
   }

   {
   byte  *pSrc;
   byte  *pDst;

      pSrc  =  (byte *)&task_Alerts;
      pDst  =  (byte *)&task_Alerts_Last;

      for( Idx = 0; Idx < sizeof( task_Alerts_Last); Idx ++)
      {
         if( pDst[Idx] != pSrc[Idx])
         {
            pDst[Idx]   =  pSrc[Idx];
            EvCode      =  (pSrc[Idx] > 0) ? VMC_EVENT_ALERT_ON : VMC_EVENT_ALERT_OFF;
            //Task_Application_SetEvent( EvCode, AlertCode, &task_Event_Data);
         }
      }
   }
#endif
}



/*----------------------------------------------------------------------------*/
void                          Task_Application_SetDoAction( byte  ActionCode, Cmd_D_Data  *pRec)   // Cmd_D  ActionCode = SubCmd
/*----------------------------------------------------------------------------*/
{
   task_fDoAction       =  FALSE;
   task_DoAction_Code   =  ActionCode;
   memcpy( p_NVRAM_SERVER_D_CMD, pRec, sizeof( Cmd_D_Data));

   switch( task_DoAction_Code)
   {
      case  D_CMD_EMERGENCY_CREDIT_CARD   :  // 0x10
            task_fDoAction =  TRUE;
            break;

      case  D_CMD_DB                      :  // 0x20
            switch( pRec->DB.ActionCode)
            {
               case  D_SUB_CMD_DB_RESET_V_POINTERS    :  // 0x01
                     SET_BIT( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_V_POINTERS);

                     task_Param_Var =  0;
                     Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_LAST_EVENT_NUMERATOR       );
                     break;

               case  D_SUB_CMD_DB_RESET_L_POINTERS    :  // 0x02
                     SET_BIT( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_L_POINTERS);

                     task_Param_Var =  0;
                     Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_RECEIPT_NUMBER     );
                     break;

               case  D_SUB_CMD_DB_RESET_COIN_ROUTING  :  // 0x03
                     SET_BIT( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_COIN_ROUTING);
                     break;

               case  D_SUB_CMD_DB_RESET_Z_POINTERS    :  // 0x04
                     SET_BIT( App_TaskEvent[TASK_LOG], EV_APP_LOG_RESET_Z_POINTERS);

                     task_Param_Var =  1;
                     Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER           );
                     break;

               case  D_SUB_CMD_DB_RESET_VARS          :  // 0x05
                     {
                     usint    Var;

                        task_Param_Var =  0;
                        for( Var = 0; Var < VAR_END; Var += sizeof( task_Param_Var))
                           Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), Var);
                     }
                     break;

               case  D_SUB_CMD_DB_RESET_HOPPERS       :  // 0x06
                     Task_Hopper_ResetInfo();
                     break;

               case  D_SUB_CMD_DB_RESET_ALL           :  // 0x0F  15
                     pRec->DB.ActionCode  =  D_SUB_CMD_DB_RESET_VARS          ;  Task_Application_SetDoAction( D_CMD_DB, pRec);  // DON'T CHANGE POSITION OF THIS LINE !!!
                     pRec->DB.ActionCode  =  D_SUB_CMD_DB_RESET_HOPPERS       ;  Task_Application_SetDoAction( D_CMD_DB, pRec);  // DON'T CHANGE POSITION OF THIS LINE !!!
                     pRec->DB.ActionCode  =  D_SUB_CMD_DB_RESET_V_POINTERS    ;  Task_Application_SetDoAction( D_CMD_DB, pRec);
                     pRec->DB.ActionCode  =  D_SUB_CMD_DB_RESET_L_POINTERS    ;  Task_Application_SetDoAction( D_CMD_DB, pRec);
                     pRec->DB.ActionCode  =  D_SUB_CMD_DB_RESET_COIN_ROUTING  ;  Task_Application_SetDoAction( D_CMD_DB, pRec);
                     pRec->DB.ActionCode  =  D_SUB_CMD_DB_RESET_Z_POINTERS    ;  Task_Application_SetDoAction( D_CMD_DB, pRec);
                     break;
            }
            break;

      case  D_CMD_DIGITEL_AUTHENTICATION  :  // 0x81
            task_fDoAction =  TRUE;
            break;

      case  D_CMD_PRODUCT_ENABLE_DISABLE  :  // 0x82
            task_ProductEnaDis_fChangeMode   =  TRUE;
            task_ProductEnaDis_ProductCode   =  pRec->Product_EnaDis.ProductCode;
            task_ProductEnaDis_fMode         =  pRec->Product_EnaDis.fMode;
            break;

      case  D_CMD_TECHNICIAN              :  // 0x99
            //Task_Application_DoAction_Tech();
            switch( pRec->Tech.ActionCode)
            {
               case  D_SUB_CMD_TECH_RESET             :  // 0x01
                     HW_DEF_SYSTEM_RESET;
                     break;

               case  D_SUB_CMD_TECH_REPLY_TO_ANY_MSG  :  // 0x02
                     Task_Server_SetMode_ReplyToAnyMsg( (bool)pRec->Tech.Value);
                     break;

               case  D_SUB_CMD_TECH_PRINT_RECEIPT     :  // 0x04
                     SET_BIT( App_TaskEvent[TASK_PRINTER], (EV_APP_PRN_PRINT_RECEIPT | EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT));
                     break;

               case  D_SUB_CMD_BILL_VALIDATOR         :  // 0x05
                     switch( pRec->Tech.Value)
                     {
                        case  0: SET_BIT( task_fReqToDisable, DEVICE_BILL_VALIDATOR);  break;
                        case  1: SET_BIT( task_fReqToEnable , DEVICE_BILL_VALIDATOR);  break;
                     }
                     break;

               case  D_SUB_CMD_TECH_DEBUG_PARAMS      :  // 0xEE
                     BuzzAPI_SetMode( ON, 2);
                     Task_Application_Debug();
                     task_State  =  STATE_IDLE;
                     break;

               case  D_SUB_CMD_TECH_FACTORY_DEFAULTS  :  // 0xFF
                     BuzzAPI_SetMode( ON, 2);
                     Task_Params_SetFactoryDefaults();
                     BuzzAPI_SetMode( FRAGMENTED_3, 1);
                     break;
            }
            break;
   }
}



/*----------------------------------------------------------------------------*/
Cmd_D_Data                   *Task_Application_GetDoAction_CC( void)
/*----------------------------------------------------------------------------*/
{
   return( p_NVRAM_SERVER_D_CMD);
}



/*----------------------------------------------------------------------------*/
Cmd_E_Data                   *Task_Application_GetEmergency( void)   // Cmd_E
/*----------------------------------------------------------------------------*/
{
   return( p_NVRAM_APP_E_CMD);
}



/*----------------------------------------------------------------------------*/
Cmd_E_Products                *Task_Application_GetEmergency_Products( void)  // Cmd_E
/*----------------------------------------------------------------------------*/
{
   return( &task_Emergency_Products);
}



/*----------------------------------------------------------------------------*/
tApp_Status                  *Task_Application_GetStatus( void)
/*----------------------------------------------------------------------------*/
{
   return( &task_Status);
}



/*----------------------------------------------------------------------------*/
tApp_TransInfo               *Task_Application_GetTransInfo( void)
/*----------------------------------------------------------------------------*/
{
   return( p_NVRAM_APP_TRANS_INFO);
}



/*----------------------------------------------------------------------------*/
tApp_SalesInfo               *Task_Application_GetSalesInfo( void)   // for Task Setup
/*----------------------------------------------------------------------------*/
{
   return( p_NVRAM_APP_SALES_INF0);
}



/*----------------------------------------------------------------------------*/
tApp_CoinsInfo               *Task_Application_GetCoinsInfo( void)   // for Task Setup
/*----------------------------------------------------------------------------*/
{
   return( p_NVRAM_APP_COINS_INF0);
}



/*----------------------------------------------------------------------------*/
tApp_BillsInfo               *Task_Application_GetBillsInfo( void)   // for Task Setup
/*----------------------------------------------------------------------------*/
{
   return( p_NVRAM_APP_BILLS_INF0);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Application_Close_Z( usint  New_Z)   // Cmd_Y
/*----------------------------------------------------------------------------*/
{
   Task_Params_Read_Var ( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);

   if( task_Param_Var == New_Z)  // Void action if new Z == current Z
      return( FALSE);

   task_Param_Var =  New_Z;
   if( task_Param_Var == 0)
      task_Param_Var =  1;
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);

   task_Param_Var =  0;
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_LAST_EVENT_NUMERATOR);
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_RECEIPT_NUMBER);
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_CONFIRM_NUMBER);

   task_Timer_Wait   =  Task_Timer_SetTimeSec( 1); // see note in Task_Application_HandleState()::STATE_LOG_TRANSACTION

   return( TRUE);
}



/*----------------------------------------------------------------------------*/
bool                          Task_Application_Prn_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
bool  fIsBusy;


   fIsBusy  =  task_fPrinterIsBusy;  
   if( task_PrinterBuf_Count > 300)
      fIsBusy  =  TRUE;

   return( fIsBusy);
}



/*----------------------------------------------------------------------------*/
void                          Task_Application_Prn_Init( void)
/*----------------------------------------------------------------------------*/
{
   task_fPrinterIsBusy     =  FALSE;
   task_PrinterBuf_W_Idx   =  0;
   task_PrinterBuf_R_Idx   =  0;
   task_PrinterBuf_Count   =  0;
}



/*----------------------------------------------------------------------------*/
void                          Task_Application_Prn_InsertText( byte *pText, usint   Count)
/*----------------------------------------------------------------------------*/
{
usint i;


   if( pText == NULL)
      return;

   if( task_PrinterBuf_W_Idx >= PRN_TX_BUF_SIZE)   task_PrinterBuf_W_Idx   =  0;

   for( i = 0; i < Count; i ++)
   {
      p_NVRAM_PRN_TX_BUF[task_PrinterBuf_W_Idx ++] =  pText[i];
      if( task_PrinterBuf_W_Idx >= PRN_TX_BUF_SIZE)   task_PrinterBuf_W_Idx   =  0;
   }
   
   task_PrinterBuf_Count   += Count;
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( task_Timer_Wait              <  Gap)  task_Timer_Wait               =  0; else  task_Timer_Wait               -= Gap;
      //if( task_Timer_Cancel            <  Gap)  task_Timer_Cancel             =  0; else  task_Timer_Cancel             -= Gap;
      //if( task_Timer_ChangePocketLight <  Gap)  task_Timer_ChangePocketLight  =  0; else  task_Timer_ChangePocketLight  -= Gap;
      if( task_Timer_Errors            <  Gap)  task_Timer_Errors             =  0; else  task_Timer_Errors             -= Gap;
      if( task_Timer_Warnings          <  Gap)  task_Timer_Warnings           =  0; else  task_Timer_Warnings           -= Gap;
      if( task_Timer_ButtonPressed     <  Gap)  task_Timer_ButtonPressed      =  0; else  task_Timer_ButtonPressed      -= Gap;
      if( task_Timer_VoidPrinter       <  Gap)  task_Timer_VoidPrinter        =  0; else  task_Timer_VoidPrinter        -= Gap;
      if( task_Timer_EnableCV          <  Gap)  task_Timer_EnableCV           =  0; else  task_Timer_EnableCV           -= Gap;
      if( task_Timer_EnableBV          <  Gap)  task_Timer_EnableBV           =  0; else  task_Timer_EnableBV           -= Gap;
      if( task_Timer_GetInfo           <  Gap)  task_Timer_GetInfo            =  0; else  task_Timer_GetInfo            -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleExtEvent( void)
/*----------------------------------------------------------------------------*/
{
   byte  i;
   ulong Hopper_CoinValue;
   ulong Hopper_Capacity;

   
   App_TaskEvent[TASK_APPL]   =  EV_APP_VOID;

   if( App_TaskEvent[TASK_SERVER] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_SERVER], EV_APP_SERVER_READ_PARAMS))
      {
         task_Screen =  task_Screen_last;
      }
   }

   if( App_TaskEvent[TASK_HW] != EV_APP_VOID)
   {
      if( BIT_IS_SET( App_TaskEvent[TASK_HW], EV_APP_HW_DOOR_LOCK_OPENED))
      {
         BuzzAPI_SetMode( ON, 1);
         Task_Application_SetAlert( SUB_CMD_EV_ALERT_MACHINE_DOOR_LOCK, 0, ON);
         task_fGetKey_Setup   =  TRUE;          //On "Door opened", after a key press, Warnings will be shown
         task_fDoorIsOpened   =  TRUE;
#if 0   // Skipping the "Wait for key press" state
         LcdAPI_WriteStringConst( LINE1, MSG_PRESS_ANY_KEY);
#endif
      }

      if ( BIT_IS_CLEAR( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER ) )   
      {
         if( BIT_IS_SET( App_TaskEvent[TASK_HW], EV_APP_HW_DOOR_LOCK_CLOSED))
         {
            BuzzAPI_SetMode( FRAGMENTED_3, 1);
            Task_Application_SetAlert( SUB_CMD_EV_ALERT_MACHINE_DOOR_LOCK, 0, OFF);
            SET_BIT( App_TaskEvent[TASK_APPL], EV_APP_APPL_DOOR_LOCK_CLOSED);
            task_fGetKey_Setup   =  FALSE;
            task_fDoorIsOpened   =  FALSE;
         }
      } //if ( BIT_IS_CLEAR( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER ) )
      
      if( BIT_IS_SET( App_TaskEvent[TASK_HW], EV_APP_HW_SAFE_REMOVED))
      {
         task_Event_Data.Type_3.Field1 =  task_pStatusStruct->Qtty_Coins_Central_100;
         task_Event_Data.Type_3.Field2 =  task_pStatusStruct->Qtty_Coins_Central_200;
         task_Event_Data.Type_3.Field3 =  task_pStatusStruct->Qtty_Coins_Central_500;
         task_Event_Data.Type_3.Field4 =  task_pStatusStruct->Qtty_Coins_Central_1000;
         //Task_Application_SetEvent( VMC_EVENT_EMPTY_CENTRAL, 0, &task_Event_Data);
         Task_Application_SetEvent_CoinCentralRemoval(&task_Event_Data);

         BuzzAPI_SetMode( ON, 1);
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_HW], EV_APP_HW_SAFE_RETURNED))
      {
         BuzzAPI_SetMode( FRAGMENTED_3, 1);

         memset( &p_NVRAM_APP_COINS_INF0->Erasable, 0, sizeof( p_NVRAM_APP_COINS_INF0->Erasable));
         p_NVRAM_APP_COINS_INF0->Checksum =  Checksum_Calc( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), p_NVRAM_APP_COINS_INF0->Checksum);
         
         xSramAPI_Write( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), xSRAM_TYPE_COINS_INFO, 0, TRUE);

         task_pStatusStruct->Qtty_Coins_Central_100   =  0;
         task_pStatusStruct->Qtty_Coins_Central_200   =  0;
         task_pStatusStruct->Qtty_Coins_Central_500   =  0;
         task_pStatusStruct->Qtty_Coins_Central_1000  =  0;

         Task_Application_SetEvent_CoinCentralReplacement();
         //Task_Application_Log_Event( LOG_EV_COINS_SAFE_INSERTED, 0, FALSE);
      }
   }

   if( App_TaskEvent[TASK_SETUP] != EV_APP_VOID)
   {
              // New data setups updated
      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_DATA_UPDATED))
      {
         /*****************************/
        for( i = 0; i < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; i ++)
         {
            Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_COIN_VALUE + i));       // get long data from Internal EEPROM (PARAM_TYPE_A)
            Hopper_CoinValue  =  task_Param_A_Rec;
         
            Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_CAPACITY + i));
            Hopper_Capacity   =  task_Param_A_Rec;
            
            Task_Hopper_SetxRam_CoinVal_Capacity( Hopper_CoinValue, Hopper_Capacity, i);
            

         }
         /*****************************/
      }
      //................................................................................
      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_ACTIVE))          // We are during SETUP.
      {                                                                         // 

         // Clear values of count of coins to dispense manually (see Task_Setup_Func_EmptyHopper() function)
         task_Param_A_Rec   =  0;
         for( i = 0; i < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; i ++)
            Task_Params_Write( &task_Param_A_Rec, PARAM_TYPE_A, (PARAM_A_HOPPER_A_QTTY_TO_EMPTY + i));

         LcdAPI_ClearScreen();

         task_fReqToDisable   =  DEVICE_ALL;
         task_State           =  STATE_DISABLED_BY_SETUP;               // Avoid Door activity and also allows the PASSWORD menu

      }
      //................................................................................
      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_INACTIVE))
      {
         //Task_Application_ReadParams();
         //Task_Application_SetEvent( VMC_EVENT_CLOSEDOOR, 0, &task_Event_Data);
         //Task_Application_Buzzer_SetMode( FRAGMENTED_4, 3);
         task_State  =  STATE_IDLE;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_CLEAR_SALES))
      {
         //Task_Application_Log_Event( LOG_EV_SALES_DATA_ERASED, 0, FALSE);

         memset( &p_NVRAM_APP_SALES_INF0->Erasable, 0, sizeof( p_NVRAM_APP_SALES_INF0->Erasable));
         p_NVRAM_APP_SALES_INF0->Checksum =  Checksum_Calc( p_NVRAM_APP_SALES_INF0, sizeof( tApp_SalesInfo), p_NVRAM_APP_SALES_INF0->Checksum);
         xSramAPI_Write( p_NVRAM_APP_SALES_INF0, sizeof( tApp_SalesInfo), xSRAM_TYPE_SALES_INFO, 0, TRUE);

         memset( &p_NVRAM_APP_COINS_INF0->Erasable, 0, sizeof( p_NVRAM_APP_COINS_INF0->Erasable));
         p_NVRAM_APP_COINS_INF0->Checksum =  Checksum_Calc( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), p_NVRAM_APP_COINS_INF0->Checksum);
         xSramAPI_Write( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), xSRAM_TYPE_COINS_INFO, 0, TRUE);

         memset( &p_NVRAM_APP_BILLS_INF0->Erasable, 0, sizeof( p_NVRAM_APP_BILLS_INF0->Erasable));
         p_NVRAM_APP_BILLS_INF0->Checksum =  Checksum_Calc( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), p_NVRAM_APP_BILLS_INF0->Checksum);
         xSramAPI_Write( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), xSRAM_TYPE_BILLS_INFO, 0, TRUE);
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_RESET_INFO))
      {
         // 2'nd of 3 RESET steps
         //memset( &task_TransInfo, 0, sizeof( task_TransInfo));
         //xSramAPI_Write( &task_TransInfo, sizeof( task_TransInfo), xSRAM_TYPE_TRANS_INFO, 0, TRUE);

         memset( p_NVRAM_APP_SALES_INF0, 0, sizeof( tApp_SalesInfo));
         xSramAPI_Write( p_NVRAM_APP_SALES_INF0, sizeof( tApp_SalesInfo), xSRAM_TYPE_SALES_INFO, 0, TRUE);

         memset( p_NVRAM_APP_COINS_INF0, 0, sizeof( tApp_CoinsInfo));
         xSramAPI_Write( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), xSRAM_TYPE_COINS_INFO, 0, TRUE);

         memset( p_NVRAM_APP_BILLS_INF0, 0, sizeof( tApp_BillsInfo));
         xSramAPI_Write( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), xSRAM_TYPE_BILLS_INFO, 0, TRUE);

         task_pStatusStruct->Qtty_Coins_Central_100   =  0;
         task_pStatusStruct->Qtty_Coins_Central_200   =  0;
         task_pStatusStruct->Qtty_Coins_Central_500   =  0;
         task_pStatusStruct->Qtty_Coins_Central_1000  =  0;

         task_pStatusStruct->Qtty_Bills_1             =  0;
         task_pStatusStruct->Qtty_Bills_5             =  0;
         task_pStatusStruct->Qtty_Bills_10            =  0;
         task_pStatusStruct->Qtty_Bills_20            =  0;
         task_pStatusStruct->Qtty_Bills_50            =  0;
         task_pStatusStruct->Qtty_Bills_100           =  0;
         task_pStatusStruct->Qtty_Bills_200           =  0;
         
         // 25/5/2017
         // Recover Eqquipment ID
         Task_Application_RecoverEquipmentID();
         //task_EquipmentID  =  (task_B2B_pRxData->GetStatus.IO_DIP_Switches & (BIT_0 | BIT_1 | BIT_2 | BIT_3));
         //Task_Server_SetEquipmentID( task_EquipmentID);
         //task_Param_C_Rec  =  Task_Server_GetEquipmentID();
         //Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_MACHINE_NUMBER);
         //         
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_TEST_PRINTER))
      {
      byte      Buf[3];  
      byte      Idx;
        
         Idx    =       0;
         Buf[Idx ++]   =  ESC;
         Buf[Idx ++]   =  'I';
         Task_Application_Prn_InsertText( Buf, Idx);
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_TEST_COIN_ROUTING))
      {
         task_Test_State         =  0;
         task_fTest_CoinRouting  =  TRUE;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_STOP_TESTING))
      {
         task_fReqToDisable      =  DEVICE_ALL;
         task_Test_State         =  0;
         task_fTest_CoinRouting  =  FALSE;
      }

      if( BIT_IS_SET( App_TaskEvent[TASK_SETUP], EV_APP_SETUP_SET_DEFAULT_DEBUG))
      {
         Task_Params_SetFactoryDefaults();
         _WDR();_WDR();_WDR();_WDR();_WDR();_WDR();_WDR();_WDR();
         Task_Application_Debug();
         
         Task_Application_RecoverEquipmentID();
      }
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleIntEvent( void)
/*----------------------------------------------------------------------------*/
{
byte  Idx;
   
   if( BIT_IS_SET( task_Event, EVENT_BUTTON_PRESSED_LANGUAGE))
   {
      CLEAR_BIT( task_Event, EVENT_BUTTON_PRESSED_LANGUAGE);

      if( (++ App_Language) >= MAX_LANGUAGES)
         App_Language   =  0;
      task_Screen =  task_Screen_last; // this will re-draw the screen, in a new language
   }

   if( BIT_IS_SET( task_Event, EVENT_BUTTON_PRESSED_RECEIPT))
   {
      CLEAR_BIT( task_Event, EVENT_BUTTON_PRESSED_RECEIPT);
   }

   if( BIT_IS_SET( task_Event, EVENT_BUTTON_PRESSED_CANCEL))
   {
      CLEAR_BIT( task_Event, EVENT_BUTTON_PRESSED_CANCEL);
      BuzzAPI_SetMode( FRAGMENTED_2, 1);

      //task_Timer_Cancel =  Task_Timer_SetTimeSec( 2);
      Timers_SetTimer( 2, TIMER_1Sec_TYPE, (pCallBack) Task_Application_ButtonPressedCancle );
        
      task_Screen =  VGA_SCREEN_TRANSACTION_CANCELLED;
      task_State  =  STATE_CANCEL;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Application_ReadDB( void)
/*----------------------------------------------------------------------------*/
{
   /*
   xSramAPI_Read( &task_TransInfo, sizeof( task_TransInfo), xSRAM_TYPE_TRANS_INFO, 0, TRUE);
   if( Checksum_Compare( &task_TransInfo, sizeof( task_TransInfo), task_TransInfo.Checksum) == FALSE)
   {
      memset( &task_TransInfo, 0, sizeof( task_TransInfo));
      xSramAPI_Write( &task_TransInfo, sizeof( task_TransInfo), xSRAM_TYPE_TRANS_INFO, 0, TRUE);
   }
   */

   xSramAPI_Read( p_NVRAM_APP_SALES_INF0, sizeof( tApp_SalesInfo), xSRAM_TYPE_SALES_INFO, 0, TRUE);
   if( Checksum_Compare( p_NVRAM_APP_SALES_INF0, sizeof( tApp_SalesInfo), p_NVRAM_APP_SALES_INF0->Checksum) == FALSE)
   {
      memset( p_NVRAM_APP_SALES_INF0, 0, sizeof( tApp_SalesInfo));
      xSramAPI_Write( p_NVRAM_APP_SALES_INF0, sizeof( tApp_SalesInfo), xSRAM_TYPE_SALES_INFO, 0, TRUE);
   }

   xSramAPI_Read( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), xSRAM_TYPE_COINS_INFO, 0, TRUE);
   if( Checksum_Compare( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), p_NVRAM_APP_COINS_INF0->Checksum) == FALSE)
   {
      memset( p_NVRAM_APP_COINS_INF0, 0, sizeof( tApp_CoinsInfo));
      xSramAPI_Write( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), xSRAM_TYPE_COINS_INFO, 0, TRUE);
   }

   xSramAPI_Read( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), xSRAM_TYPE_BILLS_INFO, 0, TRUE);
   if( Checksum_Compare( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), p_NVRAM_APP_BILLS_INF0->Checksum) == FALSE)
   {
      memset( p_NVRAM_APP_BILLS_INF0, 0, sizeof( tApp_BillsInfo));
      xSramAPI_Write( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), xSRAM_TYPE_BILLS_INFO, 0, TRUE);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleState( void)
/*----------------------------------------------------------------------------*/
{
static   ulong    lValue;
static   bool     fCancel        =  FALSE;
static   bool     fCriticalError =  FALSE;
static   bool     fUpdateScreen  =  FALSE;
tHopperStatus     Status_Hoppers;
byte              Idx;
usint             time_info[2];         // For reading  Timers_GetTimerValue()
tParams_A_Rec     task_Param_Var;
bool              fIsDigitel;
byte              Len;

   Task_Application_SetEquipmentID();
   Task_Application_IsMasterMachine();
   Task_Application_HandleMachineStatus();
   Task_Application_HandleClock();
   Task_Application_HandleBillValidator();
   Task_Application_HandlePrinter();

   if( task_State == STATE_DISABLED_BY_SETUP)
   {
      
      return;
   }


   Task_Application_HandleWarnings();
   Task_Application_HandleErrors();
   Task_Application_HandleButtons();
   
   if( task_Flags_Error > 0)
   {
      if( fCriticalError == FALSE)
      {
         task_State  =  STATE_CRITICAL_ERROR;
      }
   }
   task_KB_Key =  (task_State == STATE_DISABLED_BY_SETUP) ? KEY_NO_KEY : KeyboardAPI_GetKey();

   if( task_fGetKey_Setup == TRUE)
   {
#if 0 // Wait a key
      if( task_KB_Key != KEY_NO_KEY)
      {
         task_fGetKey_Setup   =  FALSE;
         task_ErrMsgState  =  0;
         task_State  =  STATE_SHOW_ERRORS_AND_WARNINGS;
      }
#endif      

#if 1   // Lets skip this state. directly to the warnings or directly to the Password Entry
      task_fGetKey_Setup   =  FALSE;
      task_ErrMsgState  =  0;
      task_State  =  STATE_SHOW_ERRORS_AND_WARNINGS;
#endif
   }

   // Turn light OFF in change pocket
   /*if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_CHANGE_POCKET) && (task_Timer_ChangePocketLight == 0)) // See Task_Application_ChangePocketLight()
      SET_BIT( task_fReqToDisable, DEVICE_CHANGE_POCKET);*/
   if ( fUpdateScreen == TRUE)
   {
      fUpdateScreen  =  FALSE;
      task_Screen =  task_Screen_last;
   }

   // Keep Coin Validator enabled - when it needs to be enabled
   if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_COIN_VALIDATOR) && (task_Timer_EnableCV == 0))
   {
      task_Timer_EnableCV  =  Task_Timer_SetTimeMsec( 2000);
      if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Coin_Validator, B2B_STATUS_COIN_VAL_DISABLED))
         SET_BIT( task_fReqToEnable , DEVICE_COIN_VALIDATOR);
   }

   // Keep Bill Validator enabled - when it needs to be enabled
   if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BILL_VALIDATOR) && (task_Timer_EnableBV == 0))
   {
      task_Timer_EnableBV  =  Task_Timer_SetTimeMsec( 2000);
      if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_DISABLED))
      {
         SET_BIT( task_fReqToEnable , DEVICE_BILL_VALIDATOR);
         BuzzAPI_Beep();
      }
   }
#if 0   // See new function Task_Application_ButtonPressedCancle()
   if( (task_fAutoCancel == TRUE) && (task_Timer_Cancel == 0))
   {
      task_fAutoCancel  =  FALSE;
      SET_BIT( task_Event, EVENT_BUTTON_PRESSED_CANCEL);
   }
#endif
#if 0
   // DEBUG question
   if ( task_State != task_State_Prev )
   {
      task_State_Prev = task_State;
      printf("State = %d\n", task_State);
   }
#endif
   // Check if Critical situation has recovered
   if ( task_Flags_Error_current != task_Flags_Error )
   {
      // Change !
      if ( task_Flags_Error == 0 )
      {
         // No errors any more
         CLEAR_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_DISABLED);
         CLEAR_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_CRITICAL_ERROR);
         fCriticalError          =  FALSE;
      }
      task_Flags_Error_current = task_Flags_Error;
   }
   
   switch( task_State)
   {
      case  STATE_IDLE                       :
            App_Language                        =  task_DefaultLanguage;
            memset( p_NVRAM_APP_TRANS_INFO      ,  0  ,  sizeof( tApp_TransInfo));
            memset( p_NVRAM_APP_PRODUCTS        ,  0  , (sizeof( tProduct_Transaction) * MAX_TRANS_REC_PRODUCTS));
            memset( task_TransProducts_Selected ,  0  ,  sizeof( task_TransProducts_Selected));
            memset( p_NVRAM_APP_LOG_TRANS_ITEMS ,  0  , (sizeof( tRcptItem) * MAX_LOG_TRANS_ITEMS));
            
            task_LogTrans_CardsIdx              =  0;
            task_Timer_Wait                     =  0;
            task_ResidentCards                  =  0;
            task_fReqToEnable                   =  0;
            task_fReqToDisable                  =  DEVICE_ALL;
            task_fVoidButton_Cancel             =  FALSE;
            CLEAR_BIT( task_fReqToDisable, DEVICE_CHANGE_POCKET); // keep Change-pocket light Enabled

            task_TransProducts_Idx              =  0;
            task_Status.Error_Flags             =  0;
            task_Status.Warning_Flags           =  0;
            task_fDoAction                      =  FALSE;

            task_MGC_fVoidCard                  =  FALSE;
            task_MGC_XOR                        =  0;
            task_MGC_XOR_last                   =  0;
            task_MGC_Len                        =  0;
            task_MGC_Len_last                   =  0;

            task_fUpdateInfo                    =  TRUE;
            task_fAutoCancel                    =  FALSE;
            fCancel                             =  FALSE;
            task_State                          =  STATE_GET_READY;
            break;

      case  STATE_CRITICAL_ERROR             :
            SET_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_DISABLED);
            SET_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_CRITICAL_ERROR);
            // >>> Report the server about the error
            fCriticalError       =  TRUE;
            task_fReqToDisable   =  DEVICE_ALL;
            task_Screen          =  VGA_SCREEN_OUT_OF_ORDER;
            task_State           =  STATE_DISABLED;
            break;

      case  STATE_CANCEL                     :          // CANCEL Button was pressed
            if( fCancel == FALSE)
            {
               fCancel  =  TRUE;
               task_fReqToDisable   =  DEVICE_ALL;
            }

            //--------------------------------------
            /*if( task_Timer_Cancel > 0)
               break;*/
            if ( Timers_GetTimerValue( (pCallBack) Task_Application_ButtonPressedCancle, time_info ) == TRUE )
            {
               if ( time_info[TIMER_INFO_COUNTER] > 0 )
                  break;
            }
            //--------------------------------------

            task_fAutoCancel  =  FALSE;
            task_State        =  STATE_DISPENSE_CHANGE;
            break;

      case  STATE_DISABLED                   :
            if( task_Flags_Error == 0)
            {
               CLEAR_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_DISABLED);
               CLEAR_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_CRITICAL_ERROR);
               fCriticalError          =  FALSE;
               task_State              =  STATE_IDLE;
            }
            break;

      case  STATE_DISABLED_BY_SETUP          :
            break;

      case  STATE_DISABLED_BY_SERVER         :
            task_State  =  STATE_IDLE;
            break;
/*
      case  STATE_SHOW_ERRORS_AND_WARNINGS   :
            Task_Application_ShowErrorsAndWarnings();
            //task_State  =  STATE_DISABLED_BY_SETUP;
            break; */  // 2016_10_27

      case  STATE_GET_READY                    :
            SET_BIT( task_fReqToEnable, DEVICE_BUTTON_LANGUAGE);

            if( task_ProductEnaDis_fChangeMode == TRUE)
            {
               task_ProductEnaDis_fChangeMode   =  FALSE;
               Task_Application_Product_EnaDis( task_ProductEnaDis_ProductCode, task_ProductEnaDis_fMode);
            }
           
            // BUTTONS
            for( Idx = 0; Idx < MAX_PRODUCTS; Idx ++)
            {
               if( task_TransProducts_Disabled[Idx] == FALSE)  SET_BIT( task_fReqToEnable , (DEVICE_BUTTON_SELECTION_1 << Idx));
               else                                            SET_BIT( task_fReqToDisable, (DEVICE_BUTTON_SELECTION_1 << Idx));
            }

            //task_State  =  STATE_VGA_SCREEN_1;
            task_State  =  STATE_SET_BLOCK_OCCASSION_CLIENTS;
            break;

      case  STATE_VGA_SCREEN_1               :
            task_Screen =  VGA_SCREEN_READY;
            task_State  =  STATE_GET_PRODUCTS_AND_PAYMENT;
            //task_State  =  STATE_SET_BLOCK_OCCASSION_CLIENTS;
            LcdAPI_WriteStringConst( LINE1, MSG_READY);
            break;

//-------------------------------- 2016-10-27            
      case  STATE_SHOW_ERRORS_AND_WARNINGS   :          // We here, after door is opened and warning + password menu
            Task_Application_ShowErrorsAndWarnings();
            //break;
//---------------------------------------------            
      case  STATE_GET_PRODUCTS_AND_PAYMENT   :
            // This is the "select product and pay" state
            if( (task_ProductEnaDis_fChangeMode == TRUE) && (p_NVRAM_APP_TRANS_INFO->Value_Total == 0))
            {
               task_State  =  STATE_GET_READY;
               break;
            }

            if( (p_NVRAM_APP_TRANS_INFO->Value_Total > 0) && (p_NVRAM_APP_TRANS_INFO->Value_Due == 0))
            {
               BuzzAPI_SetMode( FRAGMENTED_3, 1);
               task_Timer_Wait      =  Task_Timer_SetTimeSec( 2); // Give enough time to Door-controller to disable payment devices
               task_fReqToDisable   =  DEVICE_ALL;
               task_State           =  STATE_DISPENSE_CHANGE;
               break;
            }

            if( BIT_IS_SET( task_Event, EVENT_BUTTON_PRESSED_SELECT_1))
            {
               CLEAR_BIT( task_Event, EVENT_BUTTON_PRESSED_SELECT_1);
               Task_Application_ProductSelected( PRODUCT_1);
            }

            if( BIT_IS_SET( task_Event, EVENT_BUTTON_PRESSED_SELECT_2))
            {
               CLEAR_BIT( task_Event, EVENT_BUTTON_PRESSED_SELECT_2);
               Task_Application_ProductSelected( PRODUCT_2);
            }

            if( BIT_IS_SET( task_Event, EVENT_BUTTON_PRESSED_SELECT_3))
            {
               CLEAR_BIT( task_Event, EVENT_BUTTON_PRESSED_SELECT_3);
               Task_Application_ProductSelected( PRODUCT_3);
            }

            // Handle Coins
            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Coin_Validator, B2B_STATUS_COIN_VAL_COIN_DETECTED))
            {
               CLEAR_BIT( task_B2B_pRxData->GetStatus.Coin_Validator, B2B_STATUS_COIN_VAL_COIN_DETECTED);
               Task_Application_Coins_GetValue();
            }

            if( BIT_IS_SET( App_TaskEvent[TASK_B2B], EV_APP_B2B_RX_COIN_VALUE))
            {
               BuzzAPI_Beep();
               p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash   += task_B2B_pRxData->Coin_Valid_Value.Value;
               Task_Application_HandleCoinRouting();

               p_NVRAM_APP_COINS_INF0->Erasable.MoneyDeposited    += task_B2B_pRxData->Coin_Valid_Value.Value;
               p_NVRAM_APP_COINS_INF0->NonErasable.MoneyDeposited += task_B2B_pRxData->Coin_Valid_Value.Value;

               Task_Application_SetEvent_Coin(task_B2B_pRxData->Coin_Valid_Value.Value);     // Set Coin event with coin value

               if( task_B2B_pRxData->Coin_Valid_Value.RoutedTo == ROUTE_SAFE_BOX)
               {
                  p_NVRAM_APP_COINS_INF0->Erasable.MoneyInCashBox    += task_B2B_pRxData->Coin_Valid_Value.Value;
                  p_NVRAM_APP_COINS_INF0->NonErasable.MoneyInCashBox += task_B2B_pRxData->Coin_Valid_Value.Value;
                  
                  switch( task_B2B_pRxData->Coin_Valid_Value.Value)
                  {
                     case  100   :
                           p_NVRAM_APP_COINS_INF0->Erasable.CashBox_Qtty[COIN_01_00]   ++;
                           task_pStatusStruct->Qtty_Coins_Central_100   =  p_NVRAM_APP_COINS_INF0->Erasable.CashBox_Qtty[COIN_01_00];
                           break;

                     case  200   :
                           p_NVRAM_APP_COINS_INF0->Erasable.CashBox_Qtty[COIN_02_00]   ++;
                           task_pStatusStruct->Qtty_Coins_Central_200   =  p_NVRAM_APP_COINS_INF0->Erasable.CashBox_Qtty[COIN_02_00];
                           break;

                     case  500   :
                           p_NVRAM_APP_COINS_INF0->Erasable.CashBox_Qtty[COIN_05_00]   ++;
                           task_pStatusStruct->Qtty_Coins_Central_500   =  p_NVRAM_APP_COINS_INF0->Erasable.CashBox_Qtty[COIN_05_00];
                           break;

                     case  1000  :
                           p_NVRAM_APP_COINS_INF0->Erasable.CashBox_Qtty[COIN_10_00]   ++;
                           task_pStatusStruct->Qtty_Coins_Central_1000  =  p_NVRAM_APP_COINS_INF0->Erasable.CashBox_Qtty[COIN_10_00];
                           break;
                  }
               }

               task_fUpdateInfo  =  TRUE;
               fUpdateScreen     =  TRUE;
            }

            // Handle Bills (Banknotes)
            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.BV_Status_2, BILL_STATUS2_ACCEPTING))
            {
               if( task_fVoidButton_Cancel == FALSE)
               {
                  task_fVoidButton_Cancel =  TRUE;
                  SET_BIT( task_fReqToDisable, DEVICE_BUTTON_CANCEL);

                  if( task_fAutoCancel == TRUE)
                  {
                     Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_TIME_AUTO_CANCEL);
                     if( task_Param_C_Rec > 0)
                     {
                        //task_Timer_Cancel =  Task_Timer_SetTimeSec( task_Param_C_Rec);
                        Timers_SetTimer( task_Param_C_Rec, TIMER_1Sec_TYPE, (pCallBack) Task_Application_ButtonPressedCancle );
                        
                     }
                  }
               }
            }

            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.BV_Status_2, BILL_STATUS2_IDLE))
            {
               if( task_fVoidButton_Cancel == TRUE)
               {
                  task_fVoidButton_Cancel =  FALSE;
                  SET_BIT( task_fReqToEnable, DEVICE_BUTTON_CANCEL);
               }
            }

            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_BILL_DETECTED))
            {
               CLEAR_BIT( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_BILL_DETECTED);
               lValue   =  0;
               Task_Application_Bills_GetValue();
            }

            if( BIT_IS_SET( App_TaskEvent[TASK_B2B], EV_APP_B2B_RX_BILL_VALUE))
            {
               if( task_B2B_pRxData->Bill_Valid_Value > 0)
               {
                  // Set event for Bill insert
                  Task_Application_SetEvent_Bill( task_B2B_pRxData->Bill_Valid_Value );
                  //printf("Bill %u\n", task_B2B_pRxData->Bill_Valid_Value);
                  lValue   =  task_B2B_pRxData->Bill_Valid_Value;
                  /*
                  // Check for potential change to return if bill is accepted
                  Task_Params_Read( &task_Param_A_Rec, PARAM_A_CHANGE_LIMIT);
                  if( task_Param_A_Rec > 0)
                  {      // money paid so far         + value of the inserted bill > transaction value
                     if( (task_Transaction.Paid_Total + task_BillValue) > task_Transaction.Price)
                     {
                        if( ((task_Transaction.Paid_Total + task_BillValue) - task_Transaction.Price) > task_Param_A_Rec)
                        {
                           task_State  =  STATE_REJECT_BILL;
                           break;
                        }
                     }
                  }

                  task_Timer_GetInfo   =  Task_Timer_SetTimeMsec( 5000);   // It has to be a long timeout (time to stack a bill inside the cassette)
                  */
                  Task_Application_Bills_HandleBill( ACCEPT);
               }
            }

            //if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_RETURNED))
            //   task_State  =  STATE_POST_PAYMENT;

            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKED))
            {
               CLEAR_BIT( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKED);
               BuzzAPI_Beep();
               p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash   += lValue;
               task_fUpdateInfo  =  TRUE;
               fUpdateScreen     =  TRUE;

               p_NVRAM_APP_BILLS_INF0->Erasable.MoneyDeposited    += lValue;
               p_NVRAM_APP_BILLS_INF0->NonErasable.MoneyDeposited += lValue;
               p_NVRAM_APP_BILLS_INF0->Erasable.BillsInStacker    ++;

               switch( lValue)
               {
                  case  2000  :  // 20  NIS
                        p_NVRAM_APP_BILLS_INF0->Erasable.BillsQtty[BILL_20]   ++;
                        task_pStatusStruct->Qtty_Bills_20   =  p_NVRAM_APP_BILLS_INF0->Erasable.BillsQtty[BILL_20];
                        break;

                  case  5000  :  // 50  NIS
                        p_NVRAM_APP_BILLS_INF0->Erasable.BillsQtty[BILL_50]   ++;
                        task_pStatusStruct->Qtty_Bills_50   =  p_NVRAM_APP_BILLS_INF0->Erasable.BillsQtty[BILL_50];
                        break;

                  case  10000 :  // 100 NIS
                        p_NVRAM_APP_BILLS_INF0->Erasable.BillsQtty[BILL_100]  ++;
                        task_pStatusStruct->Qtty_Bills_100  =  p_NVRAM_APP_BILLS_INF0->Erasable.BillsQtty[BILL_100];
                        break;

                  case  20000 :  // 200 NIS
                        p_NVRAM_APP_BILLS_INF0->Erasable.BillsQtty[BILL_200]  ++;
                        task_pStatusStruct->Qtty_Bills_200  =  p_NVRAM_APP_BILLS_INF0->Erasable.BillsQtty[BILL_200];
                        break;
               }
            }

            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.MGC_Reader, B2B_STATUS_MGC_CARD_DETECTED))
            {
               CLEAR_BIT( task_B2B_pRxData->GetStatus.MGC_Reader, B2B_STATUS_MGC_CARD_DETECTED);

               if( task_fAutoCancel == TRUE)
               {
                  Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_TIME_AUTO_CANCEL);
                  if( task_Param_C_Rec > 0)
                  {
                     //task_Timer_Cancel =  Task_Timer_SetTimeSec( task_Param_C_Rec);
                     Timers_SetTimer( task_Param_C_Rec, TIMER_1Sec_TYPE, (pCallBack) Task_Application_ButtonPressedCancle );
                     
                  }
                     
               }

               memset( task_B2B_pRxData->MGC_Track2, 0, sizeof( task_B2B_pRxData->MGC_Track2));
               Task_Application_Mgc_GetTrack2();

               SET_BIT( task_fReqToDisable, (DEVICE_COIN_VALIDATOR | DEVICE_BILL_VALIDATOR));
               SET_BIT( task_fReqToDisable, DEVICE_BUTTON_CANCEL);

               task_Timer_GetInfo   =  Task_Timer_SetTimeMsec( 1000);
               task_State  =  STATE_MGC_GET_TRACK;
            }
            else
            {

            }
            break;      // end of <STATE_GET_PRODUCTS_AND_PAYMENT> state

      case  STATE_MGC_GET_TRACK              :
            if( task_Timer_GetInfo == 0)
               task_State  =  STATE_VGA_SCREEN_1;

            if( task_B2B_pRxData->MGC_Track2[0] == 0)
               break;

            task_MGC_XOR   =  Util_XOR_16b( (byte *)task_B2B_pRxData->MGC_Track2, strlen( task_B2B_pRxData->MGC_Track2));
            task_MGC_Len   =  strlen( task_B2B_pRxData->MGC_Track2);

            if( (task_MGC_XOR != task_MGC_XOR_last) || (task_MGC_Len != task_MGC_Len_last))
            {
               task_MGC_fVoidCard   =  FALSE;
               task_MGC_XOR_last    =  task_MGC_XOR;
               task_MGC_Len_last    =  task_MGC_Len;
            }

            if( (task_MGC_fVoidCard == TRUE) && (task_MGC_XOR == task_MGC_XOR_last) && (task_MGC_Len == task_MGC_Len_last))
            {
               task_Timer_Wait   =  Task_Timer_SetTimeSec( 1);
               task_State        =  STATE_MGC_WAIT_MSG;  // re-enable payment devices
               break;
            }

            Task_Application_MGC_SetCardType();
            break;

      case  STATE_MGC_CREDIT_CARD_1  :
            Task_Application_MGC_CreditCard_1();
            break;

      case  STATE_MGC_CREDIT_CARD_2  :
            Task_Application_MGC_CreditCard_2();
            break;

      case  STATE_MGC_DIGITEL_1       :
            Task_Application_MGC_Digitel_1();
            break;

      case  STATE_MGC_DIGITEL_2       :
            Task_Application_MGC_Digitel_2();
            break;

      case  STATE_MGC_WAIT_MSG               :
            if( task_Timer_Wait > 0)
               break;

            if( p_NVRAM_APP_TRANS_INFO->Value_Due > 0)
            {
               SET_BIT( task_fReqToEnable, (DEVICE_COIN_VALIDATOR | DEVICE_BILL_VALIDATOR));
               SET_BIT( task_fReqToEnable, DEVICE_BUTTON_CANCEL);
            }

            task_State  =  STATE_VGA_SCREEN_1;
            break;

      case  STATE_DISPENSE_CHANGE            :
            if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_COIN_VALIDATOR)) SET_BIT( task_fReqToDisable, DEVICE_COIN_VALIDATOR);
            if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BILL_VALIDATOR)) SET_BIT( task_fReqToDisable, DEVICE_BILL_VALIDATOR);
            if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_CANCEL))  SET_BIT( task_fReqToDisable, DEVICE_BUTTON_CANCEL);

            if( task_Timer_Wait > 0)
               break;

            if( task_Q_fIsEmpty == FALSE)
               break;

            if( fCancel == TRUE)
            {
               lValue      =  p_NVRAM_APP_TRANS_INFO->Value_Paid;
            }
            else
            {
               lValue      =  p_NVRAM_APP_TRANS_INFO->Value_ChangeToGive;
               task_Screen =  VGA_SCREEN_RECEIPT_AND_CHANGE;
            }

            if( lValue > 0)
            {
               Task_Hopper_Dispense_Value( lValue, FALSE);
               task_State  =  STATE_MONEY_BACK;
            }
            else
               task_State  =  (fCancel == TRUE) ? STATE_END_TRANSACTION : STATE_LOG_TRANSACTION;

            SET_BIT( task_fReqToEnable, DEVICE_CHANGE_POCKET);
            //task_Timer_ChangePocketLight  =  Task_Timer_SetTimeSec( 10);
            Timers_SetTimer( 10, TIMER_1Sec_TYPE, (pCallBack) Task_Application_ChangePocketLight );
            break;

      case  STATE_MONEY_BACK                 :
            Status_Hoppers =  Task_Hopper_GetStatus();

            switch( Status_Hoppers)
            {
               case  HOPPER_STATUS_IDLE         :  task_Timer_Wait   =  0;             break;   // 0
               case  HOPPER_STATUS_BUSY         :                                      break;   // 1
               case  HOPPER_STATUS_DONE_OK      :  task_Timer_Wait   =  0;             break;   // 2
               case  HOPPER_STATUS_DONE_ERROR   :  BuzzAPI_SetMode( FRAGMENTED_2, 2);  break;   // 3
            }

            p_NVRAM_APP_TRANS_INFO->Value_ChangeGiven =  Task_Hopper_GetValueDispensed();
            task_fUpdateInfo  =  TRUE;
            task_State        =  STATE_LOG_TRANSACTION;
            break;

      case  STATE_LOG_TRANSACTION            :
            if( task_Timer_Wait > 0)   // see Task_Application_Close_Z() and Task_Server_HandleComm_Rx()::SERVER_CMD_END_Z_SESSION [hold on to reset Transactions DB pointers before saving new transaction]
               break;

            Task_Application_LogTransaction4Setup();  // update cash/credit info (due to potential cash/credit flow in case of cancelling)

            if( fCancel == FALSE)
            {
/*               printf( "%lu    %lu    %lu    %lu    %lu\n",
                      p_NVRAM_APP_TRANS_INFO->Value_Total,                  // Total
                      p_NVRAM_APP_TRANS_INFO->Value_Total_Actual,           // Total
                      p_NVRAM_APP_TRANS_INFO->Value_ChangeGiven,            // ���� �����
                      p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash,              // ���� �����
                      p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident);     // ����
*/
               Task_Application_SetEvent_ExcessCalculated(p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash - p_NVRAM_APP_TRANS_INFO->Value_Total_Actual);
               Task_Application_SetEvent_ExcessReturn(p_NVRAM_APP_TRANS_INFO->Value_ChangeGiven);
              
               Task_Application_LogTransaction();  // for 'L' records
               Task_Application_LogTransaction4Printer();   // for receipt printout
            }

            task_State  =  (fCancel == TRUE) ? STATE_END_TRANSACTION : STATE_PRINT_TRANSACTION;
            break;

      case  STATE_PRINT_TRANSACTION          :
            SET_BIT( App_TaskEvent[TASK_PRINTER], (EV_APP_PRN_PRINT_RECEIPT | EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT));
            task_State  =  STATE_WAIT_FOR_PRINTOUT_END;
            break;

      case  STATE_WAIT_FOR_PRINTOUT_END      :
            if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_ERROR))
               task_State  =  STATE_END_TRANSACTION;

            if( BIT_IS_SET( App_TaskEvent[TASK_PRINTER], EV_APP_PRN_END_PRINTING_JOB))
               task_State  =  STATE_END_TRANSACTION;
            break;

      case  STATE_END_TRANSACTION            :
            if( task_Timer_Wait == 0)
            {
               if( fCancel == TRUE)     // CANCEL Button was pressed
               {

                  Task_Application_SetEvent( VMC_EVENT_CANCEL_PAYMENT, 0, &task_Event_Data);
               }

               task_State  =  STATE_IDLE;
            }
            break;

      case STATE_END_DURING_SETUP:
     
         break;

         
      case STATE_SET_BLOCK_OCCASSION_CLIENTS:
         // Is mode Block Occasionally Client ?  if not, return to 
         Task_Params_Read( &task_Param_Var, PARAM_TYPE_A, PARAM_A_BLOCK_OCCAS_CLIENT);
         if (task_Param_Var == 0)
         {
            task_Screen =  VGA_SCREEN_READY;
            task_State = STATE_GET_PRODUCTS_AND_PAYMENT;
            LcdAPI_WriteStringConst( LINE1, MSG_READY);
         }
         else   // We are in "Block Occasionally Client" mode
         {
            task_Screen       =  VGA_SCREEN_PRESENT_DIGITEL_CARD;
            // Block SELECTION BUTTONS
            SET_BIT( task_fReqToDisable,(DEVICE_BUTTON_SELECTION_1 | DEVICE_BUTTON_SELECTION_2 | DEVICE_BUTTON_SELECTION_3));

            // Enable MGC Reader
            SET_BIT( task_fReqToEnable, DEVICE_MGC_READER);
            task_State = STATE_WAIT_MGC_DIGITEL_IDENT;
         }
         //
         task_Timer_GetInfo   =  0;     //Task_Timer_SetTimeMsec( 1000);
         break;
            
      
      case STATE_WAIT_MGC_DIGITEL_IDENT:
         // *************************************/*/*/*/*/*/*/        
         if( BIT_IS_SET( task_B2B_pRxData->GetStatus.MGC_Reader, B2B_STATUS_MGC_CARD_DETECTED))
         {
            // Card identified !
            CLEAR_BIT( task_B2B_pRxData->GetStatus.MGC_Reader, B2B_STATUS_MGC_CARD_DETECTED);
            //
            memset( task_B2B_pRxData->MGC_Track2, 0, sizeof( task_B2B_pRxData->MGC_Track2));
            Task_Application_Mgc_GetTrack2();                   // Send Command to Door: Bring new card track
            task_State = STATE_GET_MGC_DIGITEL_TRACK;
         }
         else
            break;      // No card yet
        
         break;
         
      case STATE_GET_MGC_DIGITEL_TRACK:
         if (task_B2B_pRxData->MGC_Track2[0] == 0)
         {

            break;
         }

         memset( p_NVRAM_TRACK_2, 0, B2B_MAX_SIZE_TRACK_2);                     // clean app buffer
         Len   =  task_B2B_pRxPacket->BufSize;
         if( Len >= B2B_MAX_SIZE_TRACK_2)
            Len   =  (B2B_MAX_SIZE_TRACK_2 - 1);
         
         Util_StrCpy( p_NVRAM_TRACK_2, task_B2B_pRxData->MGC_Track2, Len);      // copy track data 
         
         //Debug_PrintHex((byte*)task_B2B_pRxData->MGC_Track2, Len);
        // *****************************************/*/*/*/*/*/        
         fIsDigitel  =  Task_Application_MGC_IsDigitel();                       // Is Digitel ?

         if( fIsDigitel == FALSE)
         {            // ==== Not yet DIGITEL CARD
            task_State = STATE_WAIT_MGC_DIGITEL_IDENT;
            break;
         }

         // After Digitel Card identification....
         // Allow SELECTION BUTTONS
         SET_BIT( task_fReqToEnable, (DEVICE_BUTTON_SELECTION_1 | DEVICE_BUTTON_SELECTION_2 | DEVICE_BUTTON_SELECTION_3));
         
         // Disable MGC Reader
         SET_BIT( task_fReqToDisable, DEVICE_MGC_READER);
         
         // Clean Track buffer (we don't need the digitel data. Only it's existance
         memset( task_B2B_pRxData->MGC_Track2, 0, sizeof( task_B2B_pRxData->MGC_Track2));
         memset( p_NVRAM_TRACK_2, 0, B2B_MAX_SIZE_TRACK_2);                             // clean app buffer         
#if 0         
         Timers_SetTimer( 10, TIME_UNIT_1Sec, Task_Application_BlockOccasClient );    // open dynamic timeout timer with a "void func(void)" callback func.
#endif
         task_Screen       =  VGA_SCREEN_READY;
         task_State = STATE_GET_PRODUCTS_AND_PAYMENT;
         break;
         
      default:
            task_State  =  STATE_IDLE;
            break;
   }
}       // end "Task_Application_HandleState"


/*----------------------------------------------------------------------------*/
void                            Task_Application_SystemDuringSetup(void)
/*----------------------------------------------------------------------------*/
{
      task_State = STATE_END_DURING_SETUP;
}

/*----------------------------------------------------------------------------*/
static	void 					   Task_Application_UpdateTransInfo( void)
/*----------------------------------------------------------------------------*/
{
   if( task_fUpdateInfo == TRUE)
   {
      task_fUpdateInfo  =  FALSE;

      p_NVRAM_APP_TRANS_INFO->Value_Total_Actual      =  (p_NVRAM_APP_TRANS_INFO->Value_Total >= p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident) ? (p_NVRAM_APP_TRANS_INFO->Value_Total - p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident) : 0;
      p_NVRAM_APP_TRANS_INFO->Value_Paid              =  (p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash + p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard);
      p_NVRAM_APP_TRANS_INFO->Value_Due               =  (p_NVRAM_APP_TRANS_INFO->Value_Total_Actual >= p_NVRAM_APP_TRANS_INFO->Value_Paid) ? (p_NVRAM_APP_TRANS_INFO->Value_Total_Actual - p_NVRAM_APP_TRANS_INFO->Value_Paid) : 0;

      p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net     =  p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash;
      if( p_NVRAM_APP_TRANS_INFO->Value_Paid >= p_NVRAM_APP_TRANS_INFO->Value_Total_Actual)
      {
         p_NVRAM_APP_TRANS_INFO->Value_ChangeToGive   =  (p_NVRAM_APP_TRANS_INFO->Value_Paid - p_NVRAM_APP_TRANS_INFO->Value_Total_Actual);

         if( p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash > 0)
            p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net  =  (p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash - p_NVRAM_APP_TRANS_INFO->Value_ChangeToGive);
      }

      task_fAutoCancel  =  FALSE;
      if( (p_NVRAM_APP_TRANS_INFO->Value_Total > 0) && (p_NVRAM_APP_TRANS_INFO->Value_Paid == 0))
      {
         Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_TIME_AUTO_CANCEL);
         if( task_Param_C_Rec > 0)
         {
            task_fAutoCancel  =  TRUE;
            //task_Timer_Cancel =  Task_Timer_SetTimeSec( task_Param_C_Rec);
            Timers_SetTimer( task_Param_C_Rec, TIMER_1Sec_TYPE, (pCallBack) Task_Application_ButtonPressedCancle );     // After choosing a product (button)
            
         }
      }
   }
}



/*----------------------------------------------------------------------------*/
static	void 					   Task_Application_ProductSelected( byte Product)
/*----------------------------------------------------------------------------*/
{
byte     Idx;
bool     fError;
        
   if( Product >= MAX_PRODUCTS)
      return;
#if 0   // In case of using timeout for:  "����� �������"
   Timers_StopTimer( Task_Application_BlockOccasClient );
#endif
   Task_Application_Product_Read( Product);     // Read product information (price etc.)
   
   fError   =  FALSE;
   if( task_Product.Code               == 0) fError   =  TRUE;
   if( task_Product.Price              == 0) fError   =  TRUE;
   if( task_Product.MaxUnitsPerTrans   == 0) fError   =  TRUE;
   if( fError == TRUE)
   {
      BuzzAPI_SetMode( FRAGMENTED_2, 2);
      return;
   }
   
   // Check if selection is not over limit
   if( task_TransProducts_Selected[Product] >= task_Product.MaxUnitsPerTrans)
   {
      BuzzAPI_SetMode( FRAGMENTED_2, 1);
      return;
   }
   task_TransProducts_Selected[Product]   ++;
   if( task_TransProducts_Idx >= MAX_TRANS_REC_PRODUCTS)
      BuzzAPI_SetMode( FRAGMENTED_2, 1);
   else
   {
      p_NVRAM_APP_PRODUCTS[task_TransProducts_Idx].Code            =  task_Product.Code;
      p_NVRAM_APP_PRODUCTS[task_TransProducts_Idx].Price           =  task_Product.Price;
      p_NVRAM_APP_PRODUCTS[task_TransProducts_Idx].fDigitelBenefit =  FALSE;
      task_TransProducts_Idx   ++;
      if( p_NVRAM_APP_TRANS_INFO->Value_Total == 0)
      {
         SET_BIT( task_fReqToEnable, DEVICE_COIN_VALIDATOR);
         SET_BIT( task_fReqToEnable, DEVICE_BILL_VALIDATOR);
         SET_BIT( task_fReqToEnable, DEVICE_MGC_READER);
         SET_BIT( task_fReqToEnable, DEVICE_BUTTON_CANCEL);
      }

      p_NVRAM_APP_TRANS_INFO->Value_Total += task_Product.Price;
      task_fUpdateInfo  =  TRUE;
      task_Screen       =  task_Screen_last; // this will re-draw the screen
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_MGC_SetCardType( void)  // Check Card Type
/*----------------------------------------------------------------------------*/
{
byte                    Len;
bool                    fIsDigitel;
//tParams_Var_Rec         task_Param_Var;
tParams_A_Rec           task_Param_Var;

   if(   (task_Timer_GetInfo == 0)           ||
         (task_B2B_pRxData->MGC_Track2[0] == 0)  ||
         (task_LogTrans_CardsIdx >= MAX_MGC_CARDS))
   {
      BuzzAPI_SetMode( FRAGMENTED_2, 1);
      task_State  =  STATE_VGA_SCREEN_1;
      return;
   }

   memset( p_NVRAM_TRACK_2, 0, B2B_MAX_SIZE_TRACK_2);
   Len   =  task_B2B_pRxPacket->BufSize;
   if( Len >= B2B_MAX_SIZE_TRACK_2)
      Len   =  (B2B_MAX_SIZE_TRACK_2 - 1);
   Util_StrCpy( p_NVRAM_TRACK_2, task_B2B_pRxData->MGC_Track2, Len);

   task_ResidentCard_Discount =  0;

   task_State  =  STATE_MGC_CREDIT_CARD_1;   // by default - MGC is Credit Card
   fIsDigitel  =  Task_Application_MGC_IsDigitel();

   if( fIsDigitel == TRUE)
   {            // ==== DIGITEL CARD
      if( task_ResidentCards >= MAX_DIGITEL_CARDS)
      {
         BuzzAPI_SetMode( FRAGMENTED_2, 2);
         task_Timer_Wait   =  Task_Timer_SetTimeSec( 3);
         task_Screen       =  VGA_SCREEN_MGC_DIGITEL_OVER_LIMIT;
         task_State        =  STATE_MGC_WAIT_MSG;
      }
      else
      {
         task_State  =  STATE_MGC_DIGITEL_1;

         // Reject using Digitel (resident) card if any amount was paid already in cash
         if( p_NVRAM_APP_TRANS_INFO->Value_Paid > 0)
         {
            BuzzAPI_SetMode( FRAGMENTED_2, 2);
            task_Timer_Wait   =  Task_Timer_SetTimeSec( 3);
            task_Screen       =  VGA_SCREEN_MGC_USAGE_DENIED;
            task_State        =  STATE_MGC_WAIT_MSG;
         }
      }
   }
   else
   {
      // ==== NOT DIGITEL CARD (Other c. cards)      
      //Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_T_APP_FLAGS);
      Task_Params_Read( &task_Param_Var, PARAM_TYPE_A, PARAM_A_CREDIT_CARD_BLOCKED);
      if ( task_Param_Var == 1 )
      {
         // Credit card is not supported. Display notiffication for few seconds  and  back to main screen
         BuzzAPI_SetMode( FRAGMENTED_2, 2);
         task_Timer_Wait   =  Task_Timer_SetTimeSec( 3);
         task_Screen       =  VGA_SCREEN_NO_CREDIT_SERVICE;
         task_State        =  STATE_MGC_WAIT_MSG;
      }
   }
}

/*
*              Task_Application_MGC_IsDigitel
*
* Return TRUE   for DIGITEL CARD
* Return FALSE  for Not a DIGITEL CARD (other cards or nothing good?)
*/
/*----------------------------------------------------------------------------*/
static	bool						Task_Application_MGC_IsDigitel( void)
/*----------------------------------------------------------------------------*/
{
char     Str[10];
bool     fResult;
byte     Start;
byte     Len;
ulong    Value;

   fResult  =  TRUE;

   if( p_NVRAM_TRACK_2[0] == 0)
      fResult  =  FALSE;

   // Check Track-length
   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_DIGITEL_TRACK_2_LENGTH);
   if( strlen( p_NVRAM_TRACK_2) != task_Param_C_Rec)
      fResult  =  FALSE;

   if( fResult == TRUE)
   {
      // Check Unique Card ID
      Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_DIGITEL_CARD_ID_START);  Start =  task_Param_C_Rec;
      Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_DIGITEL_CARD_ID_LEN);    Len   =  task_Param_C_Rec;
      Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_DIGITEL_CARD_ID_VALUE);  Value =  task_Param_C_Rec;

      if( Len == 0)
         fResult  =  FALSE;
      else
      {
         Util_LongToStr( Value,  Str);
         if( Util_StrCmp( Str, (p_NVRAM_TRACK_2 + Start), Len) == FALSE)
            fResult  =  FALSE;
      }
   }
  
   return( fResult);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_MGC_CreditCard_1( void)  // Send Emergency
/*----------------------------------------------------------------------------*/
{
   memset( p_NVRAM_SERVER_D_CMD, 0, sizeof( Cmd_D_Data));
   task_fDoAction =  FALSE;
   
   memset( p_NVRAM_APP_E_CMD, 0, sizeof( Cmd_E_Data));       // Zero padding of p_NVRAM_APP_E_CMD
   
   p_NVRAM_APP_E_CMD->CreditCard.ValueToCharge  =  p_NVRAM_APP_TRANS_INFO->Value_Due;
   p_NVRAM_APP_E_CMD->CreditCard.EquipmentID    =  Task_Server_GetEquipmentID();
   p_NVRAM_APP_E_CMD->CreditCard.TicketNum      =  0;
   p_NVRAM_APP_E_CMD->CreditCard.DateAndTime    =  ClockAPI_DateAndTimeToSeconds();

   // Replace the first occurance of '=' with 'D'
   {
   char  *pStr;

      pStr  =  strchr( p_NVRAM_TRACK_2, '=');
      if( pStr != NULL)
         *pStr =  'D';
   }

   // Convert string to BCD
   Util_Str2BCD_Memset( p_NVRAM_TRACK_2, p_NVRAM_APP_E_CMD->CreditCard.Track2, sizeof( p_NVRAM_APP_E_CMD->CreditCard.Track2), 0xFF);

   // Set Event
   task_Event_Data.Type_2.Field1 =  p_NVRAM_APP_E_CMD->CreditCard.ValueToCharge;
   task_Event_Data.Type_2.Field2 =  0;
   //Task_Application_SetEvent( VMC_EVENT_DEBUG_CC_TX_TO_SERVER, 0, &task_Event_Data);
   Task_Application_SetEvent_CreditCardAccepted();

   Task_Application_SetEvent_CreditCardRequestConfirm(p_NVRAM_APP_E_CMD->CreditCard.ValueToCharge);
   //Debug_PrintHex(p_NVRAM_APP_E_CMD->CreditCard.Track2, 32);
     
   // Send Emergency to the Server
   SET_BIT  ( App_TaskEvent[TASK_APPL], EV_APP_APPL_EMERGENCY_CREDIT);
   BuzzAPI_SetMode( ON, 1);

   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_CRDT_SRVR_TIMOUT);
   if( task_Param_C_Rec == 0) task_Param_C_Rec  =  CREDIT_CARD_SERVER_TIMOUT;
   task_Timer_Wait   =  Task_Timer_SetTimeSec( task_Param_C_Rec);
   task_Screen       =  VGA_SCREEN_PLEASE_WAIT;
   task_State        =  STATE_MGC_CREDIT_CARD_2;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_MGC_CreditCard_2( void)  // Receive 'D' packet, as a response to the Emergency
/*----------------------------------------------------------------------------*/
{
   if( task_Timer_Wait == 0)
   {
      task_ErrCode      =  0; // Timeout
      BuzzAPI_SetMode( FRAGMENTED_2, 2);
      task_Timer_Wait   =  Task_Timer_SetTimeSec( 3);
      task_Screen       =  VGA_SCREEN_MGC_ACTION_FAILED;
      task_State        =  STATE_MGC_WAIT_MSG;

      /* Set Event */
      //task_Event_Data.Type_2.Field1 =  0;
      //task_Event_Data.Type_2.Field2 =  0;
      //Task_Application_SetEvent( VMC_EVENT_DEBUG_CC_TIMEOUT, 0, &task_Event_Data);
   }

   if( (task_fDoAction == TRUE) && (task_DoAction_Code == D_CMD_EMERGENCY_CREDIT_CARD))
   {
      // Credit Card Confirmation Answer
      //p_NVRAM_SERVER_D_CMD->CreditCard.ResultCode                       // ubyte
      //p_NVRAM_SERVER_D_CMD->CreditCard.Value                            // ulong
      //p_NVRAM_SERVER_D_CMD->CreditCard.AuthorizationCode                // ulong
      //p_NVRAM_SERVER_D_CMD->CreditCard.ConfirmationCode                 // ulong
      //p_NVRAM_SERVER_D_CMD->CreditCard.Last4Digits                      // usint
      Task_Application_SetEvent_CreditCardRequestAnswer(p_NVRAM_SERVER_D_CMD->CreditCard.Value, p_NVRAM_SERVER_D_CMD->CreditCard.Last4Digits);
     
      task_fDoAction =  FALSE;

      if( p_NVRAM_SERVER_D_CMD->CreditCard.ResultCode != 1)  // 1 == OK; other = FAILED
      {
         task_ErrCode   =  p_NVRAM_SERVER_D_CMD->CreditCard.ResultCode;  // Server's Error-code
         BuzzAPI_SetMode( FRAGMENTED_2, 2);
         task_Timer_Wait   =  Task_Timer_SetTimeSec( 3);
         task_Screen       =  VGA_SCREEN_MGC_ACTION_FAILED;
         task_State        =  STATE_MGC_WAIT_MSG;

         // Set Event
         task_Event_Data.Type_2.Field1 =  0;
         task_Event_Data.Type_2.Field2 =  p_NVRAM_SERVER_D_CMD->CreditCard.ResultCode;
         //Task_Application_SetEvent( VMC_EVENT_DEBUG_CC_SERVER_REPLY_ERR, 0, &task_Event_Data);
      }
      else
      {
         // Set Event
         task_Event_Data.Type_2.Field1 =  p_NVRAM_SERVER_D_CMD->CreditCard.ConfirmationCode;
         task_Event_Data.Type_2.Field2 =  p_NVRAM_SERVER_D_CMD->CreditCard.Value;
         //Task_Application_SetEvent( VMC_EVENT_DEBUG_CC_SERVER_REPLY_OK, 0, &task_Event_Data);

         // Log MGC card (mainly for 'L' command)
         if( task_LogTrans_CardsIdx < MAX_MGC_CARDS)
         {
            p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].Type                =  MGC_TYPE_CREDIT_CARD;
            p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].ConfirmationCode    =  p_NVRAM_SERVER_D_CMD->CreditCard.ConfirmationCode;
            p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].AuthorizationCode   =  p_NVRAM_SERVER_D_CMD->CreditCard.AuthorizationCode;
            p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].Resident_Discount   =  0;
            p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].Last4Digits         =  Util_BCDInt2Int( p_NVRAM_SERVER_D_CMD->CreditCard.Last4Digits);
            p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].CardIssuer          =  p_NVRAM_SERVER_D_CMD->CreditCard.CardIssuer;
            task_LogTrans_CardsIdx  ++;
         }

         p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard   =  p_NVRAM_APP_E_CMD->CreditCard.ValueToCharge;
         task_MGC_fVoidCard   =  TRUE;
         task_fUpdateInfo     =  TRUE;
         task_Timer_Wait      =  0;
         task_State           =  STATE_MGC_WAIT_MSG;
      }
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_MGC_Digitel_1( void)  // Emergency call: Digitel - Authentication
/*----------------------------------------------------------------------------*/
{
byte     Idx;
byte     Idx_2;
byte     Idx_3;


   memset( p_NVRAM_SERVER_D_CMD, 0, sizeof( Cmd_D_Data));
   task_fDoAction =  FALSE;

   p_NVRAM_APP_E_CMD->Digitel.EquipmentID =  Task_Server_GetEquipmentID();
   p_NVRAM_APP_E_CMD->Digitel.DateAndTime =  ClockAPI_DateAndTimeToSeconds();

   // Replace the first occurance of '=' with 'D'
   {
   char  *pStr;

      pStr  =  strchr( p_NVRAM_TRACK_2, '=');
      if( pStr != NULL)
         *pStr =  'D';
   }

   // Convert string to BCD
   Util_Str2BCD_Memset( p_NVRAM_TRACK_2, p_NVRAM_APP_E_CMD->Digitel.Track2, sizeof( p_NVRAM_APP_E_CMD->Digitel.Track2), 0xFF);

   // Insert Products to Emergency record
   memset( &task_Emergency_Products, 0, sizeof( task_Emergency_Products));

   for( Idx = 0; Idx < MAX_PRODUCTS; Idx ++)
   {
      Task_Application_Product_Read( Idx);

      if( (task_Product.Code == 0) || (task_Product.Price == 0))
         continue;

      task_Emergency_Products.Products[Idx].Code    =  task_Product.Code;
      task_Emergency_Products.Products[Idx].Amount  =  0;

      for( Idx_2 = 0; Idx_2 < MAX_TRANS_REC_PRODUCTS; Idx_2 ++)
      {
         if( (p_NVRAM_APP_PRODUCTS[Idx_2].Code == 0) || (p_NVRAM_APP_PRODUCTS[Idx_2].fDigitelBenefit == TRUE))
            continue;

         if( p_NVRAM_APP_PRODUCTS[Idx_2].Code == task_Product.Code)
            task_Emergency_Products.Products[Idx].Amount ++;
      }
   }
   
   // Set Event
   task_Event_Data.Type_2.Field1 =  0;
   task_Event_Data.Type_2.Field2 =  0;
   //Task_Application_SetEvent( VMC_EVENT_DEBUG_DGTL_TX_TO_SERVER, 0, &task_Event_Data);
   Task_Application_SetEvent_DigitelAccepted();                                                 // 8/8/2017

   // send event for DigiTel Entitlement Check
   Task_Application_SetEvent_DigitelEntitlementCheck(&task_Emergency_Products);
  
   // Send Emergency to the Server
   SET_BIT  ( App_TaskEvent[TASK_APPL], EV_APP_APPL_EMERGENCY_DIGITEL_AUTHENTICATION);          // 8/8/2017  This will set an 'E' mergency message to Server
   BuzzAPI_SetMode( ON, 1);

   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_CRDT_SRVR_TIMOUT);
   if( task_Param_C_Rec == 0) task_Param_C_Rec  =  CREDIT_CARD_SERVER_TIMOUT;
   task_Timer_Wait   =  Task_Timer_SetTimeSec( task_Param_C_Rec);
   task_Screen       =  VGA_SCREEN_PLEASE_WAIT;
   task_State        =  STATE_MGC_DIGITEL_2;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_MGC_Digitel_2( void)  // Handle response: Digitel - Authorization
/*----------------------------------------------------------------------------*/
{
   if( task_Timer_Wait == 0)
   {
      task_ErrCode      =  0; // Timeout
      BuzzAPI_SetMode( FRAGMENTED_2, 2);
      task_Timer_Wait   =  Task_Timer_SetTimeSec( 3);
      task_Screen       =  VGA_SCREEN_MGC_ACTION_FAILED;
      task_State        =  STATE_MGC_WAIT_MSG;

      /* Set Event */
      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      //Task_Application_SetEvent( VMC_EVENT_DEBUG_DGTL_TIMEOUT, 0, &task_Event_Data);
   }

   if( (task_fDoAction == TRUE) && (task_DoAction_Code == D_CMD_DIGITEL_AUTHENTICATION))
   {
      //****************************************
      // Got answer for DigiTel Authentication
      //****************************************
      task_fDoAction =  FALSE;
/*
         printf("EqID %d   Result %d   Author %lu   Confirm %lu   Lst4Digit 0x%x  Prd1 %d qnt1 %d  Prd2 %d qnt2 %d  Prd3 %d qnt3 %d\n",
                           p_NVRAM_SERVER_D_CMD->Digitel.EquipmentID,
                           p_NVRAM_SERVER_D_CMD->Digitel.ResultCode,
                           p_NVRAM_SERVER_D_CMD->Digitel.AuthorizationCode,        // ulong
                           p_NVRAM_SERVER_D_CMD->Digitel.ConfirmationCode,         // ulong
                           p_NVRAM_SERVER_D_CMD->Digitel.Last4Digits,              // usint
                           p_NVRAM_SERVER_D_CMD->Digitel.Products[0].Code,
                           p_NVRAM_SERVER_D_CMD->Digitel.Products[0].Amount,
                           p_NVRAM_SERVER_D_CMD->Digitel.Products[1].Code,
                           p_NVRAM_SERVER_D_CMD->Digitel.Products[1].Amount,
                           p_NVRAM_SERVER_D_CMD->Digitel.Products[2].Code,
                           p_NVRAM_SERVER_D_CMD->Digitel.Products[2].Amount);
*/      
      //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Send Event !!!
      Task_Application_SetEvent_DigitelEntitlementAnswer(p_NVRAM_SERVER_D_CMD);
      
      if( p_NVRAM_SERVER_D_CMD->Digitel.ResultCode != 1)  // 1 == OK; other = FAILED
      {
         task_ErrCode   =  p_NVRAM_SERVER_D_CMD->Digitel.ResultCode;  // Server's Error-code
         BuzzAPI_SetMode( FRAGMENTED_2, 2);
         task_Timer_Wait   =  Task_Timer_SetTimeSec( 3);
         task_Screen       =  VGA_SCREEN_MGC_ACTION_FAILED;
         task_State        =  STATE_MGC_WAIT_MSG;

         // Set Event
         task_Event_Data.Type_2.Field1 =  0;
         task_Event_Data.Type_2.Field2 =  p_NVRAM_SERVER_D_CMD->Digitel.ResultCode;
         //Task_Application_SetEvent( VMC_EVENT_DEBUG_DGTL_SERVER_REPLY_ERR, 0, &task_Event_Data);
      }
      else
      {
         task_Timer_Wait   =  0;
         task_State        =  STATE_MGC_WAIT_MSG;

         // Set Event
         task_Event_Data.Type_2.Field1 =  p_NVRAM_SERVER_D_CMD->Digitel.ConfirmationCode;
         task_Event_Data.Type_2.Field2 =  Task_Application_HandleDigitel_Discount();
         if( task_Event_Data.Type_2.Field2 == FALSE)
         {
            //Task_Application_SetEvent( VMC_EVENT_DEBUG_DGTL_NO_ELIGIBILITY, 0, &task_Event_Data);

            BuzzAPI_SetMode( FRAGMENTED_2, 2);
            task_Timer_Wait   =  Task_Timer_SetTimeSec( 3);
            task_Screen       =  VGA_SCREEN_MGC_NO_ELIGIBILITY;
            task_State        =  STATE_MGC_WAIT_MSG;
         }
         else
         {
            //Task_Application_SetEvent( VMC_EVENT_DEBUG_DGTL_SERVER_REPLY_OK, 0, &task_Event_Data);

            // Log MGC card (mainly for 'L' command)
            if( task_LogTrans_CardsIdx < MAX_MGC_CARDS)
            {
               p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].Type                =  MGC_TYPE_DIGITEL_CARD;
               p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].ConfirmationCode    =  p_NVRAM_SERVER_D_CMD->Digitel.ConfirmationCode;
               p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].AuthorizationCode   =  p_NVRAM_SERVER_D_CMD->Digitel.AuthorizationCode;
               p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].Resident_Discount   =  task_ResidentCard_Discount;
               p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].Last4Digits         =  Util_BCDInt2Int( p_NVRAM_SERVER_D_CMD->Digitel.Last4Digits);
               p_NVRAM_APP_TRANS_INFO->MGC[task_LogTrans_CardsIdx].CardIssuer          =  0; // p_NVRAM_SERVER_D_CMD->Digitel.CardIssuer
               task_LogTrans_CardsIdx  ++;
               task_ResidentCards      ++;
            }

            BuzzAPI_SetMode( FRAGMENTED_3, 1);
            task_Timer_Wait      =  Task_Timer_SetTimeSec( 2);
            task_MGC_fVoidCard   =  TRUE;
            task_fUpdateInfo     =  TRUE;
            task_Screen          =  VGA_SCREEN_MGC_DISCOUNT_GRANTED;
            task_State           =  STATE_MGC_WAIT_MSG;
         }
      }
   }
}



/*----------------------------------------------------------------------------*/
static	bool						Task_Application_HandleDigitel_Discount( void)
/*----------------------------------------------------------------------------*/
{
ulong    Discount_Value;
byte     Idx;
byte     Idx_2;
byte     Idx_3;
byte     Offset;
bool     fFound;
byte     Remainder;


   task_ResidentCard_Discount =  0;
   fFound   =  FALSE;

   for( Idx =  0; Idx < MAX_PRODUCTS; Idx ++)
   {
      if( (p_NVRAM_SERVER_D_CMD->Digitel.Products[Idx].Code > 0) && (p_NVRAM_SERVER_D_CMD->Digitel.Products[Idx].Amount > 0))
      {
         for( Idx_2 = 0; Idx_2 < MAX_PRODUCTS; Idx_2 ++)
         {
            Task_Application_Product_Read( Idx_2);

            if( p_NVRAM_SERVER_D_CMD->Digitel.Products[Idx].Code == task_Product.Code)
            {
               for( Idx_3 = 0; Idx_3 < MAX_TRANS_REC_PRODUCTS; Idx_3 ++)
               {
                  if( (p_NVRAM_APP_PRODUCTS[Idx_3].Code == task_Product.Code) && (p_NVRAM_APP_PRODUCTS[Idx_3].fDigitelBenefit == FALSE) && (p_NVRAM_SERVER_D_CMD->Digitel.Products[Idx].Amount > 0))
                  {
                     fFound   =  TRUE;

                     p_NVRAM_APP_PRODUCTS[Idx_3].fDigitelBenefit  =  TRUE;
                     p_NVRAM_SERVER_D_CMD->Digitel.Products[Idx].Amount  --;

                     Discount_Value =  0;

                     if( task_Product.Discount_Money > 0)
                        Discount_Value =  task_Product.Discount_Money;

                     if( (task_Product.Discount_Percentage > 0) && (task_Product.Discount_Percentage <= 100))
                     {
                        Discount_Value =  p_NVRAM_APP_PRODUCTS[Idx_3].Price;
                        Discount_Value *= task_Product.Discount_Percentage;
                        Discount_Value /= 100;

                        // Round-up to the nearest multiple of 100 (1 NIS)
                        Remainder      =  (Discount_Value % 100);
                        if( Remainder >= 50)
                           Discount_Value += (100 - Remainder);
                        else
                           Discount_Value -= Remainder;
                     }

                     if( p_NVRAM_APP_PRODUCTS[Idx_3].Price >= Discount_Value)
                     {
                        task_ResidentCard_Discount             += Discount_Value;
                        p_NVRAM_APP_PRODUCTS[Idx_3].Price   -= Discount_Value;
                     }
                     else
                     {
                        task_ResidentCard_Discount             += p_NVRAM_APP_PRODUCTS[Idx_3].Price;
                        p_NVRAM_APP_PRODUCTS[Idx_3].Price   =  0;
                     }
                  }
               }
            }
         }
      }
   }

   p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident += task_ResidentCard_Discount;
   task_fUpdateInfo  =  TRUE;

   return( fFound);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_LogTransaction( void)
/*----------------------------------------------------------------------------*/
{
ulong    DateAndTime;
usint    RcptConfirm_Num;
usint    Receipt_Z_Num;
byte     LogTrans_ItemsIdx;
byte     ContCode;
byte     Idx;
byte     Idx_2;
bool     fWriteRec;
bool     fBreak;


   task_LogTrans_ItemsIdx  =  0;

   // Insert Cash data
   if( p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net > 0)
   {
      p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Type   =  TRANS_REC_DATA_TYPE_CASH_VALUE;  // 1
      p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Value  =  p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net;
      task_LogTrans_ItemsIdx  ++;
   }

   // Insert Credit-Card data
   for( Idx = 0; Idx < MAX_MGC_CARDS; Idx ++)
   {
      if( p_NVRAM_APP_TRANS_INFO->MGC[Idx].Type == MGC_TYPE_CREDIT_CARD)
      {
         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Type   =  TRANS_REC_DATA_TYPE_CREDIT_VALUE;      // 200
         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Value  =  p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard;
         task_LogTrans_ItemsIdx  ++;

         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Type   =  TRANS_REC_DATA_TYPE_CREDIT_CONF_FP;    // 201
         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Value  =  p_NVRAM_APP_TRANS_INFO->MGC[Idx].ConfirmationCode;
         task_LogTrans_ItemsIdx  ++;

         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Type   =  TRANS_REC_DATA_TYPE_CREDIT_CONF_CS;    // 207
         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Value  =  p_NVRAM_APP_TRANS_INFO->MGC[Idx].AuthorizationCode;
         task_LogTrans_ItemsIdx  ++;
      }
   }

   // Insert Resident card (Digitel) data
   for( Idx = 0; Idx < MAX_MGC_CARDS; Idx ++)
   {
      if( p_NVRAM_APP_TRANS_INFO->MGC[Idx].Type == MGC_TYPE_DIGITEL_CARD)
      {
         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Type   =  TRANS_REC_DATA_TYPE_DIGITEL_CONF_FP;   // 251
         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Value  =  p_NVRAM_APP_TRANS_INFO->MGC[Idx].ConfirmationCode;
         task_LogTrans_ItemsIdx  ++;

         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Type   =  TRANS_REC_DATA_TYPE_DIGITEL_DISCOUNT;  // 252
         p_NVRAM_APP_LOG_TRANS_ITEMS[task_LogTrans_ItemsIdx].Value  =  p_NVRAM_APP_TRANS_INFO->MGC[Idx].Resident_Discount;
         task_LogTrans_ItemsIdx  ++;
      }
   }

   DateAndTime       =  ClockAPI_DateAndTimeToSeconds();
   LogTrans_ItemsIdx =  0;

   /**********************/
   /*   Receipt Header   */
   /**********************/
   memset( &task_LogTrans_Rec, 0, sizeof( task_LogTrans_Rec));

   if( p_NVRAM_APP_TRANS_INFO->Value_Paid > 0)
      task_LogTrans_RecType   =  TRANS_REC_TYPE_CASH; // type 1

   task_LogTrans_Rec.RcptHeader.Type         =  task_LogTrans_RecType;
   task_LogTrans_Rec.RcptHeader.DateAndTime  =  DateAndTime;

   Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_RECEIPT_NUMBER);
   task_Param_Var  ++;
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_RECEIPT_NUMBER);
   RcptConfirm_Num   =  task_Param_Var;
   task_LogTrans_Rec.RcptHeader.RcptConfirm_Num =  RcptConfirm_Num;

   Task_Params_Read_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);
   if( task_Param_Var == 0)
   {
      task_Param_Var =  1;
      Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_CURRENT_Z_NUMBER);
   }
   Receipt_Z_Num  =  task_Param_Var;
   task_LogTrans_Rec.RcptHeader.Receipt_Z_Num  =  Receipt_Z_Num;

   task_LogTrans_Rec.RcptHeader.EquipmentID     =  Task_Server_GetEquipmentID();
   task_LogTrans_Rec.RcptHeader.Item            =  p_NVRAM_APP_LOG_TRANS_ITEMS[LogTrans_ItemsIdx ++];

   Task_Log_Trans_WriteRecord( &task_LogTrans_Rec);

   /****************************/
   /*   Receipt Header (2nd)   */
   /****************************/
   memset( &task_LogTrans_Rec, 0, sizeof( task_LogTrans_Rec));

   task_LogTrans_Rec.RcptHeader_2.DateAndTime   =  DateAndTime;
   if( LogTrans_ItemsIdx < task_LogTrans_ItemsIdx)
      task_LogTrans_Rec.RcptHeader_2.Item       =  p_NVRAM_APP_LOG_TRANS_ITEMS[LogTrans_ItemsIdx ++];
   task_LogTrans_Rec.RcptHeader_2.DataTypes     =  (0xC0 + task_LogTrans_ItemsIdx);

   ContCode =  0;
   if( task_LogTrans_ItemsIdx >= LogTrans_ItemsIdx)
   {
      task_LogTrans_Rec.RcptHeader_2.ContCode      =  0;
      task_LogTrans_Rec.RcptHeader_2.ContCode      += (((task_LogTrans_ItemsIdx - LogTrans_ItemsIdx) / MAX_L_REC_RECEIPT_ITEMS) + 1);
      if( ((task_LogTrans_ItemsIdx - LogTrans_ItemsIdx) % MAX_L_REC_RECEIPT_ITEMS) == 0)
         task_LogTrans_Rec.RcptHeader_2.ContCode   --;
      ContCode =  task_LogTrans_Rec.RcptHeader_2.ContCode;
      task_LogTrans_Rec.RcptHeader_2.ContCode      += TRANS_REC_TYPE_CASH_NEXT;
   }

   Task_Log_Trans_WriteRecord( &task_LogTrans_Rec);


   /***************************/
   /*   Extra Items (cards)   */
   /***************************/
   Idx         =  0;
   fWriteRec   =  FALSE;
   fBreak      =  FALSE;
   memset( &task_LogTrans_Rec, 0, sizeof( task_LogTrans_Rec));

   for(;;)
   {
      if( LogTrans_ItemsIdx < task_LogTrans_ItemsIdx)
         fWriteRec   =  TRUE;
      else
         fBreak      =  TRUE;

      if( fBreak == FALSE)
         task_LogTrans_Rec.RcptData.Item[Idx ++]   =  p_NVRAM_APP_LOG_TRANS_ITEMS[LogTrans_ItemsIdx ++];

      if( (Idx >= MAX_L_REC_RECEIPT_ITEMS) || ((fWriteRec == TRUE) && (fBreak == TRUE)))
      {
         if( ContCode > 0)
         {
            ContCode --;
            task_LogTrans_Rec.RcptData.ContCode += (TRANS_REC_TYPE_CASH_NEXT + ContCode);
         }

         Task_Log_Trans_WriteRecord( &task_LogTrans_Rec);

         Idx         =  0;
         fWriteRec   =  FALSE;
         memset( &task_LogTrans_Rec, 0, sizeof( task_LogTrans_Rec));
      }

      if( fBreak == TRUE)
         break;
   }


   /**********************/
   /*   Products Header  */
   /**********************/
   memset( &task_LogTrans_Rec, 0, sizeof( task_LogTrans_Rec));

   task_LogTrans_Rec.ProductsHeader.Type              =  TRANS_REC_TYPE_PRODUCTS_HEADER;
   task_LogTrans_Rec.ProductsHeader.DateAndTime       =  DateAndTime;
   task_LogTrans_Rec.ProductsHeader.RcptConfirm_Num   =  RcptConfirm_Num;
   task_LogTrans_Rec.ProductsHeader.Receipt_Z_Num     =  Receipt_Z_Num;
   task_LogTrans_Rec.ProductsHeader.EquipmentID       =  Task_Server_GetEquipmentID();

   Task_Log_Trans_WriteRecord( &task_LogTrans_Rec);


   /********************/
   /*   Products Data  */
   /********************/
   for( Idx = 0; Idx < MAX_PRODUCTS; Idx ++)
   {
      memset( &task_LogTrans_Rec, 0, sizeof( task_LogTrans_Rec));
      Task_Application_Product_Read( Idx);

      task_LogTrans_Rec.ProductData.Type              =  (TRANS_REC_TYPE_PRODUCTS_PROD_1 + Idx);
      task_LogTrans_Rec.ProductData.Product.Code      =  task_Product.Code;
      task_LogTrans_Rec.ProductData.Product.UnitPrice =  task_Product.Price;

      for( Idx_2 = 0; Idx_2 < MAX_TRANS_REC_PRODUCTS; Idx_2 ++)
      {
         if( p_NVRAM_APP_PRODUCTS[Idx_2].Code == 0)
            break;

         if( p_NVRAM_APP_PRODUCTS[Idx_2].Code == task_Product.Code)
         {
            if( p_NVRAM_APP_PRODUCTS[Idx_2].fDigitelBenefit == FALSE)
            {
               task_LogTrans_Rec.ProductData.Product.Count_FullPrice ++;
            }
            else
            {
               task_LogTrans_Rec.ProductData.Product.Count_Discount  ++;
               task_LogTrans_Rec.ProductData.Product.Total_Discount  += p_NVRAM_APP_PRODUCTS[Idx_2].Price;
            }
         }
      }

      Task_Log_Trans_WriteRecord( &task_LogTrans_Rec);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_LogTransaction4Printer( void)
/*----------------------------------------------------------------------------*/
{
usint                Var;
byte                 Idx;
byte                 Idx_2;
Cmd_L_Data_Product   Product;


   // Clear VAR_RCPT_??? section (see T_Params.h)
   {
   usint    Var;

      task_Param_Var =  0;
      for( Var = VAR_RCPT_START; Var < VAR_RCPT_END; Var += sizeof( task_Param_Var))
         Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), Var);
   }

   task_Param_Var =  p_NVRAM_APP_TRANS_INFO->Value_Total;
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_TRANS_TOTAL_VALUE);

   task_Param_Var =  (p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net + p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard);
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_AMOUNT_RECEIVED);

   task_Param_Var =  p_NVRAM_APP_TRANS_INFO->Value_Total_Actual;
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_TRANS_PRICE);

   task_Param_Var =  p_NVRAM_APP_TRANS_INFO->Value_ChangeGiven;
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CHANGE);

   task_Param_Var =  p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash;
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CASH_AMOUNT_RECEIVED);

   task_Param_Var =  p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident;
   Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_RESIDENT_DISCOUNT_TOTAL);

   //printf( "%lu    %lu    %lu    %lu    %lu\n",
   //       p_NVRAM_APP_TRANS_INFO->Value_Total,                  // Total
   //       p_NVRAM_APP_TRANS_INFO->Value_Total_Actual,           // Total
   //       p_NVRAM_APP_TRANS_INFO->Value_ChangeGiven,            // ���� �����
   //       p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash,              // ���� �����
   //       p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident);     // ����     
   
   // Insert Credit-Card data
   for( Idx = 0; Idx < MAX_MGC_CARDS; Idx ++)
   {
      if( p_NVRAM_APP_TRANS_INFO->MGC[Idx].Type == MGC_TYPE_CREDIT_CARD)
      {
         task_Param_Var =  p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard;
         Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_AMOUNT_RECEIVED);

         task_Param_Var =  p_NVRAM_APP_TRANS_INFO->MGC[Idx].AuthorizationCode;
         Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_AUTHORIZATION_CODE);

         task_Param_Var =  p_NVRAM_APP_TRANS_INFO->MGC[Idx].ConfirmationCode;
         Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_CONFIRMATION_CODE);

         task_Param_Var =  p_NVRAM_APP_TRANS_INFO->MGC[Idx].Last4Digits;
         Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_LAST_4_DIGITS);

         task_Param_Var =  p_NVRAM_APP_TRANS_INFO->MGC[Idx].CardIssuer;
         Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), VAR_RCPT_CC_CARD_ISSUER);
      }
   }

   // Insert Products data
   for( Idx = 0; Idx < MAX_PRODUCTS; Idx ++)
   {
      memset( &Product, 0, sizeof( Product));
      Task_Application_Product_Read( Idx);

      Product.Price  =  task_Product.Price;
      Product.Amount =  0;
      Product.Code   =  task_Product.Code;

      for( Idx_2 = 0; Idx_2 < MAX_TRANS_REC_PRODUCTS; Idx_2 ++)
      {
         if( p_NVRAM_APP_PRODUCTS[Idx_2].Code == 0)
            break;

         if( p_NVRAM_APP_PRODUCTS[Idx_2].Code == task_Product.Code)
            Product.Amount ++;
      }

      task_Param_Var =  *(tParams_Var_Rec *)&Product;
      Task_Params_Write_Var( &task_Param_Var, sizeof( task_Param_Var), (VAR_RCPT_PRODUCTS + (Idx * sizeof( task_Param_Var))));
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_LogTransaction4Setup  ( void)
/*----------------------------------------------------------------------------*/
{
   // Update Sales Info
   p_NVRAM_APP_SALES_INF0->Erasable.SalesValue[PAYMENT_TYPE_CASH]                += p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net;
   p_NVRAM_APP_SALES_INF0->Erasable.SalesValue[PAYMENT_TYPE_CREDIT_CARD]         += p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard;
   p_NVRAM_APP_SALES_INF0->NonErasable.SalesValue[PAYMENT_TYPE_CASH]             += p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net;
   p_NVRAM_APP_SALES_INF0->NonErasable.SalesValue[PAYMENT_TYPE_CREDIT_CARD]      += p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard;
   
   if( p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net > 0)
      p_NVRAM_APP_SALES_INF0->Erasable.SalesCount[PAYMENT_TYPE_CASH]             ++;
      
   if( (p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net == 0) && (p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard > 0))
      p_NVRAM_APP_SALES_INF0->Erasable.SalesCount[PAYMENT_TYPE_CREDIT_CARD]      ++;
      
   if( p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net > 0)
      p_NVRAM_APP_SALES_INF0->NonErasable.SalesCount[PAYMENT_TYPE_CASH]          ++;
      
   if( (p_NVRAM_APP_TRANS_INFO->Value_Paid_Cash_Net == 0) && (p_NVRAM_APP_TRANS_INFO->Value_Paid_CreditCard > 0))
      p_NVRAM_APP_SALES_INF0->NonErasable.SalesCount[PAYMENT_TYPE_CREDIT_CARD]   ++;
      
   p_NVRAM_APP_SALES_INF0->Checksum =  Checksum_Calc( p_NVRAM_APP_SALES_INF0, sizeof( tApp_SalesInfo), p_NVRAM_APP_SALES_INF0->Checksum);
   xSramAPI_Write( p_NVRAM_APP_SALES_INF0, sizeof( tApp_SalesInfo), xSRAM_TYPE_SALES_INFO, 0, TRUE);

   // Update Coins Info
   p_NVRAM_APP_COINS_INF0->Erasable.ChangeBack     += p_NVRAM_APP_TRANS_INFO->Value_ChangeGiven;
   p_NVRAM_APP_COINS_INF0->NonErasable.ChangeBack  += p_NVRAM_APP_TRANS_INFO->Value_ChangeGiven;
   p_NVRAM_APP_COINS_INF0->Checksum =  Checksum_Calc( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), p_NVRAM_APP_COINS_INF0->Checksum);
   xSramAPI_Write( p_NVRAM_APP_COINS_INF0, sizeof( tApp_CoinsInfo), xSRAM_TYPE_COINS_INFO, 0, TRUE);

   // Update Bills Info
   p_NVRAM_APP_BILLS_INF0->Checksum =  Checksum_Calc( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), p_NVRAM_APP_BILLS_INF0->Checksum);
   xSramAPI_Write( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), xSRAM_TYPE_BILLS_INFO, 0, TRUE);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_SetEquipmentID( void)
/*----------------------------------------------------------------------------*/
{
byte     Temp;


   // Equipment ID base-value is determined by DIP switches 1-4 in Door-Controller
   Temp  =  (task_B2B_pRxData->GetStatus.IO_DIP_Switches & (BIT_0 | BIT_1 | BIT_2 | BIT_3));
   
   if( (Temp > 0) && (task_EquipmentID != Temp))
   {     
      task_EquipmentID  =  Temp;
      Task_Server_SetEquipmentID( task_EquipmentID);

      task_Param_C_Rec  =  Task_Server_GetEquipmentID();
      Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_MACHINE_NUMBER);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_IsMasterMachine( void)
/*----------------------------------------------------------------------------*/
{
bool  fMode;

   // Master machine acts like CT-20
   fMode =  BIT_IS_SET( task_B2B_pRxData->GetStatus.IO_DIP_Switches, BIT_7);   // DIP switch No. 8 in Door-Controller
   Task_Server_SetMode_IamMaster( fMode);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleWarnings( void)
/*----------------------------------------------------------------------------*/
{
static   usint    Warnings =  0;


   if( task_Timer_Warnings == 0)
   {
      task_Timer_Warnings  =  Task_Timer_SetTimeSec( 2);

      Warnings =  task_Flags_Warning;

      if( Task_Hopper_IsLowLevel( HOPPER_A) == TRUE)                                            SET_BIT  ( task_Flags_Warning, FLAG_WARNING_HOOPER_1_LOW_LEVEL);
      else                                                                                      CLEAR_BIT( task_Flags_Warning, FLAG_WARNING_HOOPER_1_LOW_LEVEL);

      if( Task_Hopper_IsLowLevel( HOPPER_B) == TRUE)                                            SET_BIT  ( task_Flags_Warning, FLAG_WARNING_HOOPER_2_LOW_LEVEL);
      else                                                                                      CLEAR_BIT( task_Flags_Warning, FLAG_WARNING_HOOPER_2_LOW_LEVEL);

      if( Task_Hopper_IsLowLevel( HOPPER_C) == TRUE)                                            SET_BIT  ( task_Flags_Warning, FLAG_WARNING_HOOPER_3_LOW_LEVEL);
      else                                                                                      CLEAR_BIT( task_Flags_Warning, FLAG_WARNING_HOOPER_3_LOW_LEVEL);

      if( Task_Hopper_IsLowLevel( HOPPER_D) == TRUE)                                            SET_BIT  ( task_Flags_Warning, FLAG_WARNING_HOOPER_4_LOW_LEVEL);
      else                                                                                      CLEAR_BIT( task_Flags_Warning, FLAG_WARNING_HOOPER_4_LOW_LEVEL);

      if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_NEARLY_OUT_OF_PAPER))     SET_BIT  ( task_Flags_Warning, FLAG_WARNING_PRINTER_EOP_SENSOR);
      else                                                                                      CLEAR_BIT( task_Flags_Warning, FLAG_WARNING_PRINTER_EOP_SENSOR);

      Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, PARAM_A_CHANGE_MIN_LEVEL);
      if( task_Param_A_Rec >= task_pHoppers_Info->MoneyInTubes)                                 SET_BIT  ( task_Flags_Warning, FLAG_WARNING_CHANGE_LOW_LEVEL);
      else                                                                                      CLEAR_BIT( task_Flags_Warning, FLAG_WARNING_CHANGE_LOW_LEVEL);

      if( Warnings != task_Flags_Warning)
      {
         if( Warnings < task_Flags_Warning)  BuzzAPI_SetMode( ON, 2);
         else                                BuzzAPI_SetMode( FRAGMENTED_3, 1);
         task_Screen =  task_Screen_last; // this will re-draw the screen
      }

      if( Task_Server_IsServerOnline() == FALSE)                                                SET_BIT  ( task_Flags_Warning, FLAG_WARNING_SERVER_OFFLINE);
      else                                                                                      CLEAR_BIT( task_Flags_Warning, FLAG_WARNING_SERVER_OFFLINE);

      if( Warnings != task_Flags_Warning)
      {
         task_Screen =  task_Screen_last; // this will re-draw the screen
      }
   }

   if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_HOOPER_1_LOW_LEVEL))   SET_BIT  ( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_HOPPER_A_LOW_LVL);
   else                                                                    CLEAR_BIT( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_HOPPER_A_LOW_LVL);

   if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_HOOPER_2_LOW_LEVEL))   SET_BIT  ( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_HOPPER_B_LOW_LVL);
   else                                                                    CLEAR_BIT( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_HOPPER_B_LOW_LVL);

   if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_HOOPER_3_LOW_LEVEL))   SET_BIT  ( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_HOPPER_C_LOW_LVL);
   else                                                                    CLEAR_BIT( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_HOPPER_C_LOW_LVL);

   if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_HOOPER_4_LOW_LEVEL))   SET_BIT  ( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_HOPPER_D_LOW_LVL);
   else                                                                    CLEAR_BIT( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_HOPPER_D_LOW_LVL);

   if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_PRINTER_EOP_SENSOR))   SET_BIT  ( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_PRN_NEARLY_EOP);
   else                                                                    CLEAR_BIT( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_PRN_NEARLY_EOP);

   if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_SERVER_OFFLINE))       SET_BIT  ( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_SERVER_OFFLINE);
   else                                                                    CLEAR_BIT( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_SERVER_OFFLINE);

   if( task_fDoorIsOpened == TRUE)                                         SET_BIT  ( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_DOOR_OPENED);      // Report to T_Server
   else                                                                    CLEAR_BIT( task_Status.Warning_Flags, APP_STATUS_WRN_FLAG_DOOR_OPENED);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleErrors( void)
/*----------------------------------------------------------------------------*/
{
bool     fNoErrors;


   if( task_Timer_Errors == 0)
   {
      //task_Timer_Errors =  Task_Timer_SetTimeSec( 2);

      fNoErrors   =  (task_Flags_Error == 0);

      if( Task_B2B_IsSlaveActive( task_Slave_UnitType, task_Slave_Address) == FALSE)
         SET_BIT( task_Flags_Error, FLAG_ERROR_SLAVE_OFF);
      //if( task_B2B_pRxData->GetStatus.BC_Reader_1      == B2B_STATUS_READER_DISCONNECTED)                 SET_BIT( task_Flags_Error, FLAG_ERROR_READER_1_OFF);
      if( task_B2B_pRxData->GetStatus.Bill_Validator   == B2B_STATUS_BILL_VAL_DISCONNECTED)                 SET_BIT  ( task_Flags_Error, FLAG_ERROR_BILL_VAL_OFF);
      else
      {
         if(   BIT_IS_SET( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_BILL_JAM) ||
               BIT_IS_SET( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKER_JAM))    SET_BIT  ( task_Flags_Error, FLAG_ERROR_BILL_VAL_JAM);
         if(   BIT_IS_SET( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKER_FULL))   SET_BIT  ( task_Flags_Error, FLAG_ERROR_BILL_VAL_STACKER_FULL);
      }

      if( task_Timer_VoidPrinter == 0)
      {
         if( task_B2B_pRxData->GetStatus.Printer       == B2B_STATUS_PRN_DISCONNECTED)                      SET_BIT  ( task_Flags_Error, FLAG_ERROR_PRINTER_OFF);
         else
         {
            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_ERROR))                     
               SET_BIT  ( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER);
#if 0
            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_NEARLY_OUT_OF_PAPER))   // BIT_1 Papr Roll is empty
            {
               SET_BIT  ( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER);   // bit 7        // Arrive here always, when the paper roll is pulled out or empty.
            }
#endif
            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.PRN_Status, BIT_5) || BIT_IS_SET( task_B2B_pRxData->GetStatus.PRN_Status, BIT_7) )
            {
               SET_BIT  ( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER);   // bit 7        // Arrive here always, when the paper roll is pulled out or empty.
            }
            
         }
      }

      if( task_EquipmentID == 0)                                                                            SET_BIT  ( task_Flags_Error, FLAG_ERROR_INVALID_MACHINE_ID);

      if( (fNoErrors == TRUE) && (task_Flags_Error != 0))
      {
         BuzzAPI_SetMode( FRAGMENTED_2, 2);
      }

      if( task_Flags_Error != 0)
      {
         if( Task_B2B_IsSlaveActive( task_Slave_UnitType, task_Slave_Address) == TRUE)                      CLEAR_BIT( task_Flags_Error, FLAG_ERROR_SLAVE_OFF);
         if( task_B2B_pRxData->GetStatus.BC_Reader_1      != B2B_STATUS_READER_DISCONNECTED)                CLEAR_BIT( task_Flags_Error, FLAG_ERROR_READER_1_OFF);
         if( task_B2B_pRxData->GetStatus.Bill_Validator   != B2B_STATUS_BILL_VAL_DISCONNECTED)              CLEAR_BIT( task_Flags_Error, FLAG_ERROR_BILL_VAL_OFF);
         if( task_B2B_pRxData->GetStatus.Printer          != B2B_STATUS_PRN_DISCONNECTED)                   CLEAR_BIT( task_Flags_Error, FLAG_ERROR_PRINTER_OFF);
         // v v v   23/5/2017
         //if( BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_ERROR))                      
         if( (BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_ERROR)) && (BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_NEARLY_OUT_OF_PAPER)))
         {
           CLEAR_BIT( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER);
         }
         
         if(   BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_BILL_JAM) &&
               BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKER_JAM))  CLEAR_BIT( task_Flags_Error, FLAG_ERROR_BILL_VAL_JAM);
         if(   BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKER_FULL)) CLEAR_BIT( task_Flags_Error, FLAG_ERROR_BILL_VAL_STACKER_FULL);
         if( task_EquipmentID > 0)                                                                          CLEAR_BIT( task_Flags_Error, FLAG_ERROR_INVALID_MACHINE_ID);

         if( task_Flags_Error == 0)
            BuzzAPI_SetMode( FRAGMENTED_3, 1);
      }
   }

   if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_SLAVE_OFF))             SET_BIT  ( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_DOOR_CTRL_OFFLINE);
   else                                                                 CLEAR_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_DOOR_CTRL_OFFLINE);

   if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER))  SET_BIT  ( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_PRN_OUT_OF_PAPER );
   else                                                                 CLEAR_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_PRN_OUT_OF_PAPER );

   if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_PRINTER_OFF))           SET_BIT  ( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_PRN_OFFLINE      );
   else                                                                 CLEAR_BIT( task_Status.Error_Flags, APP_STATUS_ERR_FLAG_PRN_OFFLINE      );
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleButtons( void)
/*----------------------------------------------------------------------------*/
{
static   usint    ButtonPressed  =  0;
static   bool     fDoAction      =  FALSE;

   // If any form of money/resident card was in use - disable all selection buttons
   if( (p_NVRAM_APP_TRANS_INFO->Value_Paid > 0) || (p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident > 0))
   {
      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_SELECTION_1))  // If button is enabled
         SET_BIT( task_fReqToDisable, DEVICE_BUTTON_SELECTION_1);       // disable it

      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_SELECTION_2))
         SET_BIT( task_fReqToDisable, DEVICE_BUTTON_SELECTION_2);

      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_SELECTION_3))
         SET_BIT( task_fReqToDisable, DEVICE_BUTTON_SELECTION_3);
   }

   if (Task_Setup_GetDoorOpenStatus() == FALSE)
   {
      // **************** Door is Closed or Internal Keyboards is not at "Entry Password" or later...
     
   // SELECTION_1 Button
   if( BIT_IS_SET( task_B2B_pRxData->GetStatus.IO_Buttons, BUTTON_SELECTION_1))   // If button was pressed
   {
      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_SELECTION_1))          // If button is enabled
      {
         if( BIT_IS_CLEAR( ButtonPressed, BUTTON_SELECTION_1))                  // If button was not release from before, do not handle it.
         {
            BuzzAPI_Beep();
            SET_BIT( ButtonPressed, BUTTON_SELECTION_1);
            SET_BIT( task_Event, EVENT_BUTTON_PRESSED_SELECT_1);
            Task_Application_SetEvent_ButtonPressed(SET_EVENT_BUTTON_S_1);
         }
      }
   }
   else
      if( BIT_IS_SET( ButtonPressed, BUTTON_SELECTION_1))
         CLEAR_BIT( ButtonPressed, BUTTON_SELECTION_1);    // Button is released. Clear the Debounce flag

   // SELECTION_2 Button
   if( BIT_IS_SET( task_B2B_pRxData->GetStatus.IO_Buttons, BUTTON_SELECTION_2))
   {
      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_SELECTION_2))
      {
         if( BIT_IS_CLEAR( ButtonPressed, BUTTON_SELECTION_2))
         {
            BuzzAPI_Beep();
            SET_BIT( ButtonPressed, BUTTON_SELECTION_2);
            SET_BIT( task_Event, EVENT_BUTTON_PRESSED_SELECT_2);
            Task_Application_SetEvent_ButtonPressed(SET_EVENT_BUTTON_S_2);
         }
      }
   }
   else
      if( BIT_IS_SET( ButtonPressed, BUTTON_SELECTION_2))
         CLEAR_BIT( ButtonPressed, BUTTON_SELECTION_2);

   // SELECTION_3 Button
   if( BIT_IS_SET( task_B2B_pRxData->GetStatus.IO_Buttons, BUTTON_SELECTION_3))
   {
      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_SELECTION_3))
      {
         if( BIT_IS_CLEAR( ButtonPressed, BUTTON_SELECTION_3))
         {
            BuzzAPI_Beep();
            SET_BIT( ButtonPressed, BUTTON_SELECTION_3);
            SET_BIT( task_Event, EVENT_BUTTON_PRESSED_SELECT_3);
            Task_Application_SetEvent_ButtonPressed(SET_EVENT_BUTTON_S_3);
         }
      }
   }
   else
      if( BIT_IS_SET( ButtonPressed, BUTTON_SELECTION_3))
         CLEAR_BIT( ButtonPressed, BUTTON_SELECTION_3);
   
   } // **************** End of  "Door is Closed" area.
   
   // LANGUAGE Button
   if( BIT_IS_SET( task_B2B_pRxData->GetStatus.IO_Buttons, BUTTON_LANGUAGE))
   {
      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_LANGUAGE))
      {
         if( BIT_IS_CLEAR( ButtonPressed, BUTTON_LANGUAGE))
         {
            BuzzAPI_Beep();
            SET_BIT( ButtonPressed, BUTTON_LANGUAGE);
            SET_BIT( task_Event, EVENT_BUTTON_PRESSED_LANGUAGE);
            Task_Application_SetEvent_ButtonPressed(SET_EVENT_BUTTON_LANGUAGE);
         }
      }
   }
   else
      CLEAR_BIT( ButtonPressed, BUTTON_LANGUAGE);

   // RECEIPT Button
   if( BIT_IS_SET( task_B2B_pRxData->GetStatus.IO_Buttons, BUTTON_RECEIPT))  // If button was pressed
   {
      if( BIT_IS_CLEAR( ButtonPressed, BUTTON_RECEIPT))
         task_Timer_ButtonPressed   =  Task_Timer_SetTimeSec( 3);

      if( (task_Timer_ButtonPressed == 0) && (fDoAction == FALSE))
      {
         fDoAction   =  TRUE;
         HW_DEF_SYSTEM_RESET; // v 1.63   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      }

      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_RECEIPT)) // If button is enabled
      {
         if( BIT_IS_CLEAR( ButtonPressed, BUTTON_RECEIPT))
         {
            BuzzAPI_Beep();
            SET_BIT( task_Event, EVENT_BUTTON_PRESSED_RECEIPT);
            Task_Application_SetEvent_ButtonPressed(SET_EVENT_BUTTON_RECEIPT);
         }
      }

      SET_BIT( ButtonPressed, BUTTON_RECEIPT);
   }
   else
      CLEAR_BIT( ButtonPressed, BUTTON_RECEIPT);

   // CANCEL Button
   if( BIT_IS_SET( task_B2B_pRxData->GetStatus.IO_Buttons, BUTTON_CANCEL))  // If button was pressed
   {
      if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_BUTTON_CANCEL)) // If button is enabled
      {
         if( BIT_IS_CLEAR( ButtonPressed, BUTTON_CANCEL))
         {
            BuzzAPI_Beep();
            SET_BIT( ButtonPressed, BUTTON_CANCEL);
            SET_BIT( task_Event, EVENT_BUTTON_PRESSED_CANCEL);
            Task_Application_SetEvent_ButtonPressed(SET_EVENT_BUTTON_CANCEL);
         }
      }
   }
   else
      CLEAR_BIT( ButtonPressed, BUTTON_CANCEL);

   if( ButtonPressed == 0)
      fDoAction   =  FALSE;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleBillValidator   ( void)
/*----------------------------------------------------------------------------*/
{
static   bool     fBV_StackerOut    =  FALSE;


   if( task_B2B_pRxData->GetStatus.Bill_Validator != B2B_STATUS_BILL_VAL_DISCONNECTED)
   {
      if( task_fPowerUp == TRUE)
      {
         if( BIT_IS_SET ( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKER_OUT))  fBV_StackerOut =  TRUE;
         else                                                                                            fBV_StackerOut =  FALSE;
      }

      // Stacker OUT
      if( BIT_IS_SET    ( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKER_OUT) && (fBV_StackerOut == FALSE))
      {
         fBV_StackerOut =  TRUE;
         BuzzAPI_SetMode( ON, 1);
         
         task_Event_Data.Type_2.Field1  = 0;
         task_Event_Data.Type_2.Field2  = 0;       
         Task_Application_SetEvent_BillDropCassetteRemoval(&task_Event_Data);
      }

      // Stacker IN
      if( BIT_IS_CLEAR  ( task_B2B_pRxData->GetStatus.Bill_Validator, B2B_STATUS_BILL_VAL_STACKER_OUT) && (fBV_StackerOut == TRUE))
      {
         fBV_StackerOut =  FALSE;
         BuzzAPI_SetMode( FRAGMENTED_3, 1);
         Task_Application_SetEvent_BillDropCassetteReplacement();

         memset( &p_NVRAM_APP_BILLS_INF0->Erasable, 0, sizeof( p_NVRAM_APP_BILLS_INF0->Erasable));
         p_NVRAM_APP_BILLS_INF0->Checksum =  Checksum_Calc( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), p_NVRAM_APP_BILLS_INF0->Checksum);
         xSramAPI_Write( p_NVRAM_APP_BILLS_INF0, sizeof( tApp_BillsInfo), xSRAM_TYPE_BILLS_INFO, 0, TRUE);

         task_pStatusStruct->Qtty_Bills_1    =  0;
         task_pStatusStruct->Qtty_Bills_5    =  0;
         task_pStatusStruct->Qtty_Bills_10   =  0;
         task_pStatusStruct->Qtty_Bills_20   =  0;
         task_pStatusStruct->Qtty_Bills_50   =  0;
         task_pStatusStruct->Qtty_Bills_100  =  0;
         task_pStatusStruct->Qtty_Bills_200  =  0;

         Task_Application_SetAlert( SUB_CMD_EV_ALERT_COINS_SAFE, 0, OFF);
      }
   }
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Application_HandlePrinter( void)
/*----------------------------------------------------------------------------*/
{
   if( task_PrinterBuf_R_Idx >= PRN_TX_BUF_SIZE)   task_PrinterBuf_R_Idx   =  0;

   if( (task_PrinterBuf_R_Idx == task_PrinterBuf_W_Idx) || (task_fPrinterIsBusy == TRUE))
      return;

   task_fPrinterIsBusy     =  TRUE;
   task_Timer_VoidPrinter  =  Task_Timer_SetTimeSec( 20);
   Task_Application_CommandQueueIn( B2B_CMD_PRINTER, SUB_CMD_PRN_PRINT_TEXT, 0, 0, 0);
}



/*----------------------------------------------------------------------------*/
static   void                 Task_Application_HandleClock( void)
/*----------------------------------------------------------------------------*/
{
   // Update on-screen Clock, every 1 minute
   if( BIT_IS_SET( App_TaskEvent[TASK_TIMER], EV_APP_TIMER_TICK_MINUTE))
      //task_Screen =  task_Screen_last;
      SET_BIT( task_Display_AddOn, VGA_ADD_ON_CLOCK);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleCoinRouting( void)
/*----------------------------------------------------------------------------*/
{
usint    Hopper_CoinValue;
usint    Hopper_Capacity;
usint    CoinValue;
byte     RoutedTo;
byte     Idx;


   CoinValue   =  task_B2B_pRxData->Coin_Valid_Value.Value;
   RoutedTo    =  task_B2B_pRxData->Coin_Valid_Value.RoutedTo;

   Hopper_CoinValue  =  0;
   Hopper_Capacity   =  0;

   if( CoinValue == 0)
      return;

   if( RoutedTo == ROUTE_SAFE_BOX)
   {
      for( Idx = 0; Idx < MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/; Idx ++)
      {
         Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_COIN_VALUE + Idx));
         Hopper_CoinValue  =  task_Param_A_Rec;
         
         Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_CAPACITY + Idx));
         Hopper_Capacity   =  task_Param_A_Rec;

         if( (Hopper_CoinValue == 0) || (Hopper_Capacity == 0) || (Hopper_CoinValue != CoinValue))
            continue;

         if( task_pHoppers_Info->CoinsInTube[Idx] < Hopper_Capacity)
         {
            Task_Application_Coins_SetCoin( 0, CoinValue, (ROUTE_HOPPER_A + Idx));  // from next time - route the coin to the hopper (instead of the safe-box)
            break;   // from 'for' loop
         }
      }
   }
   else
   {
      if( RoutedTo > 0)
         Idx   =  (RoutedTo - 1);   // Hoppers ID starts from 1 (0 = is the Safe-Box)

      if( Idx >= MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/)
         return;

      Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_COIN_VALUE + Idx));
      Hopper_CoinValue  =  task_Param_A_Rec;

      Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, (tParam_Code_A)(PARAM_A_HOPPER_A_CAPACITY + Idx));
      Hopper_Capacity   =  task_Param_A_Rec;

      if( (Hopper_CoinValue == 0) || (Hopper_Capacity == 0) || (Hopper_CoinValue != CoinValue))
         return;

      Task_Hopper_AddCoinsToHopper( (tHopper)Idx, 1); // Add 1 coin to the hopper

      if( task_pHoppers_Info->CoinsInTube[Idx] >= Hopper_Capacity)
         Task_Application_Coins_SetCoin( 0, CoinValue, ROUTE_SAFE_BOX); // from next time - route the coin to the safe-box (instead of the hopper)
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleMachineStatus( void)
/*----------------------------------------------------------------------------*/
{
   if( task_pStatusStruct->Qtty_Coins_Hopper_A != task_pHoppers_Info->CoinsInTube[HOPPER_A])
      task_pStatusStruct->Qtty_Coins_Hopper_A =  task_pHoppers_Info->CoinsInTube[HOPPER_A];

   if( task_pStatusStruct->Qtty_Coins_Hopper_B != task_pHoppers_Info->CoinsInTube[HOPPER_B])
      task_pStatusStruct->Qtty_Coins_Hopper_B =  task_pHoppers_Info->CoinsInTube[HOPPER_B];

   if( task_pStatusStruct->Qtty_Coins_Hopper_C != task_pHoppers_Info->CoinsInTube[HOPPER_C])
      task_pStatusStruct->Qtty_Coins_Hopper_C =  task_pHoppers_Info->CoinsInTube[HOPPER_C];

   if( task_pStatusStruct->Qtty_Coins_Hopper_D != task_pHoppers_Info->CoinsInTube[HOPPER_D])
      task_pStatusStruct->Qtty_Coins_Hopper_D =  task_pHoppers_Info->CoinsInTube[HOPPER_D];
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleExtDevices( void)
/*----------------------------------------------------------------------------*/
{
   if( task_fReqToEnable > 0)
   {
      SET_BIT( task_fDeviceEnabled, task_fReqToEnable);

      if( BIT_IS_SET( task_fReqToEnable, DEVICE_COIN_VALIDATOR       )) task_Timer_EnableCV  =  Task_Timer_SetTimeMsec( 2000);
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BILL_VALIDATOR       )) task_Timer_EnableBV  =  Task_Timer_SetTimeMsec( 2000);

      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BARCODE_READER_1     )) Task_Application_Reader_SetMode  ( BC_READER1         , ENABLED            );
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BARCODE_READER_2     )) Task_Application_Reader_SetMode  ( BC_READER2         , ENABLED            );
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_COIN_VALIDATOR       )) Task_Application_Coins_SetMode   ( ENABLED                                 );
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BILL_VALIDATOR       )) Task_Application_Bills_SetMode   ( ENABLED                                 );
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_MGC_READER           )) Task_Application_Mgc_SetMode     ( ENABLED                                 );
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BUTTON_SELECTION_1   )) Task_Application_Led_SetMode     ( LED_BUTTON_5       ,  BLINK_SLOW  , 0xFF);
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BUTTON_SELECTION_2   )) Task_Application_Led_SetMode     ( LED_BUTTON_6       ,  BLINK_SLOW  , 0xFF);
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BUTTON_SELECTION_3   )) Task_Application_Led_SetMode     ( LED_BUTTON_1       ,  BLINK_SLOW  , 0xFF);
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BUTTON_LANGUAGE      )) Task_Application_Led_SetMode     ( LED_BUTTON_2       ,  BLINK_SLOW  , 0xFF);
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BUTTON_RECEIPT       )) Task_Application_Led_SetMode     ( LED_BUTTON_3       ,  BLINK_SLOW  , 0xFF);
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_BUTTON_CANCEL        )) Task_Application_Led_SetMode     ( LED_BUTTON_4       ,  BLINK_SLOW  , 0xFF);
      if( BIT_IS_SET( task_fReqToEnable, DEVICE_CHANGE_POCKET        )) Task_Application_Led_SetMode     ( LED_CHANGE_POCKET  ,  ON          , 20);
   }
   task_fReqToEnable =  0;

   if( task_fReqToDisable > 0)
   {
      CLEAR_BIT( task_fDeviceEnabled, task_fReqToDisable);

      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BARCODE_READER_1    )) Task_Application_Reader_SetMode  ( BC_READER1         , DISABLED           );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BARCODE_READER_2    )) Task_Application_Reader_SetMode  ( BC_READER2         , DISABLED           );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_COIN_VALIDATOR      )) Task_Application_Coins_SetMode   ( DISABLED                                );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BILL_VALIDATOR      )) Task_Application_Bills_SetMode   ( DISABLED                                );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_MGC_READER          )) Task_Application_Mgc_SetMode     ( DISABLED                                );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BUTTON_SELECTION_1  )) Task_Application_Led_SetMode     ( LED_BUTTON_5       ,  OFF         , 0   );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BUTTON_SELECTION_2  )) Task_Application_Led_SetMode     ( LED_BUTTON_6       ,  OFF         , 0   );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BUTTON_SELECTION_3  )) Task_Application_Led_SetMode     ( LED_BUTTON_1       ,  OFF         , 0   );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BUTTON_LANGUAGE     )) Task_Application_Led_SetMode     ( LED_BUTTON_2       ,  OFF         , 0   );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BUTTON_RECEIPT      )) Task_Application_Led_SetMode     ( LED_BUTTON_3       ,  OFF         , 0   );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_BUTTON_CANCEL       )) Task_Application_Led_SetMode     ( LED_BUTTON_4       ,  OFF         , 0   );
      if( BIT_IS_SET( task_fReqToDisable, DEVICE_CHANGE_POCKET       )) Task_Application_Led_SetMode     ( LED_CHANGE_POCKET  ,  OFF         , 0   );
   }
   task_fReqToDisable   =  0;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Test_CoinRouting( void)
/*----------------------------------------------------------------------------*/
{
   switch( task_Test_State)
   {
      case  0:
            SET_BIT( task_fReqToEnable, DEVICE_COIN_VALIDATOR);
            task_Timer_GetInfo   =  Task_Timer_SetTimeMsec( 500);
            task_Test_State   ++;
            break;

      case  1:
            if( BIT_IS_SET( task_B2B_pRxData->GetStatus.Coin_Validator, B2B_STATUS_COIN_VAL_COIN_DETECTED))
            {
               CLEAR_BIT( task_B2B_pRxData->GetStatus.Coin_Validator, B2B_STATUS_COIN_VAL_COIN_DETECTED);
               Task_Application_Coins_GetValue();
               task_Timer_GetInfo   =  Task_Timer_SetTimeMsec( 1000);
               task_Test_State   ++;
            }
            else
            {
               if( (task_Timer_GetInfo == 0) && (BIT_IS_SET( task_B2B_pRxData->GetStatus.Coin_Validator, B2B_STATUS_COIN_VAL_DISABLED)))
                  task_Test_State   --;
            }
            break;

      case  2:
            if( task_Timer_GetInfo == 0)
               task_Test_State   =  0;

            if( BIT_IS_SET( App_TaskEvent[TASK_B2B], EV_APP_B2B_RX_COIN_VALUE))
            {
               SET_BIT( App_TaskEvent[TASK_APPL], EV_APP_APPL_COIN_DETECTED);
               task_Test_State   =  0;
            }
            break;

      default:
            task_Test_State   =  0;
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_ShowErrorsAndWarnings( void)
/*----------------------------------------------------------------------------*/
{
#define  CASE_ERROR           1
#define  CASE_WARNING         20
#define  CASE_LAST            40
 
   switch( task_ErrMsgState)
   {
      case  0:
            ClockAPI_DisplayClock( DISABLED);
            LcdAPI_ClearScreen();
            task_ErrMsgState  =  CASE_ERROR;
            break;

      // Show Errors
      case  CASE_ERROR:
            if( task_Flags_Error != 0)
            {
               LcdAPI_WriteStringConst( LINE1, MSG_ERROR);
               task_ErrMsgState  ++;
            }
            else
               task_ErrMsgState  =  CASE_WARNING;
            break;

      case  (CASE_ERROR + 1):
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_SLAVE_OFF))                LcdAPI_WriteStringConst( LINE2, MSG_ERR_SLAVE_OFF);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_ERROR + 2):
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_READER_1_OFF))             LcdAPI_WriteStringConst( LINE2, MSG_ERR_READER_1_OFF);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_ERROR + 3):
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_READER_2_OFF))             LcdAPI_WriteStringConst( LINE2, MSG_ERR_READER_2_OFF);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_ERROR + 4):
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_BILL_VAL_OFF))             LcdAPI_WriteStringConst( LINE2, MSG_ERR_BILL_VAL_OFF);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_ERROR + 5):
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_BILL_VAL_OFF))             {  task_ErrMsgState  ++;   break;   }
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_BILL_VAL_JAM))             LcdAPI_WriteStringConst( LINE2, MSG_ERR_BILL_VAL_JAM);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_ERROR + 6):
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_BILL_VAL_OFF))             {  task_ErrMsgState  ++;   break;   }
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_BILL_VAL_STACKER_FULL))    LcdAPI_WriteStringConst( LINE2, MSG_ERR_BILL_VAL_STACKER_FULL);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_ERROR + 7):
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_PRINTER_OFF))              LcdAPI_WriteStringConst( LINE2, MSG_ERR_PRINTER_OFF);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_ERROR + 8):
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_PRINTER_OFF))              {  task_ErrMsgState  ++;   break;   }
            if( BIT_IS_SET( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER))     LcdAPI_WriteStringConst( LINE2, MSG_ERR_PRINTER_OUT_OF_PAPER);
            else                                                                    task_ErrMsgState  ++;
            break;
            
      // Show Warnings
      case  CASE_WARNING:
            if( task_Flags_Warning != 0)
            {
               LcdAPI_WriteStringConst( LINE1, MSG_WARNING);
               task_ErrMsgState  ++;
            }
            else
               task_ErrMsgState  =  CASE_LAST;
            break;

      case  (CASE_WARNING + 1):
            if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_HOOPER_1_LOW_LEVEL))   LcdAPI_WriteStringConst( LINE2, MSG_WARNING_HOOPER_1_LOW_LEVEL);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_WARNING + 2):
            if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_HOOPER_2_LOW_LEVEL))   LcdAPI_WriteStringConst( LINE2, MSG_WARNING_HOOPER_2_LOW_LEVEL);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_WARNING + 3):
            if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_HOOPER_3_LOW_LEVEL))   LcdAPI_WriteStringConst( LINE2, MSG_WARNING_HOOPER_3_LOW_LEVEL);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_WARNING + 4):
            if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_HOOPER_4_LOW_LEVEL))   LcdAPI_WriteStringConst( LINE2, MSG_WARNING_HOOPER_4_LOW_LEVEL);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_WARNING + 5):
            if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_PRINTER_EOP_SENSOR))   LcdAPI_WriteStringConst( LINE2, MSG_WARNING_PRINTER_EOP_SENSOR);
            else                                                                    task_ErrMsgState  ++;
            break;

      case  (CASE_WARNING + 6):
            if( BIT_IS_SET( task_Flags_Warning, FLAG_WARNING_CHANGE_LOW_LEVEL))     LcdAPI_WriteStringConst( LINE2, MSG_WARNING_CHANGE_LOW_LEVEL);
            else                                                                    task_ErrMsgState  ++;
            break;

      // Last case - command Setup Task to get started
      case  CASE_LAST:

            task_ErrMsgState  =  0;
            SET_BIT( App_TaskEvent[TASK_APPL], EV_APP_APPL_DOOR_LOCK_OPENED);

            break;

      default:
            task_ErrMsgState  ++;
            break;
   }

   if( task_KB_Key != KEY_NO_KEY)
      task_ErrMsgState  ++;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_HandleTx( void)
/*----------------------------------------------------------------------------*/
{
   // First check if B2B application is cleared to send a new message ...
   if( Task_B2B_Master_IsClearToSend() == FALSE)
      return;

   // ... then check if there are any pending messages to send
   if( Task_Application_CommandQueueOut() == FALSE)
      return;

   task_B2B_TxCommand.UnitType    =  task_Q_Record.UnitType;
   task_B2B_TxCommand.Address     =  task_Q_Record.Address;
   task_B2B_TxCommand.Command     =  task_Q_Record.Command;
   task_B2B_TxCommand.Sub_Command =  task_Q_Record.Sub_Command;
   task_B2B_TxCommand.Index       =  task_Q_Record.Index;

   Task_Application_SetTxData();

   Task_B2B_SendCommand( &task_B2B_TxCommand, p_NVRAM_APP_B2B_TX_DATA);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_SetTxData( void)
/*----------------------------------------------------------------------------*/
{
   memset( p_NVRAM_APP_B2B_TX_DATA, 0, sizeof( tB2B_FromMaster));

   switch( task_B2B_TxCommand.Command)
   {
      case  B2B_CMD_BAR_CODE_READER :  // 'B'
            switch( task_B2B_TxCommand.Sub_Command)
            {
               case  SUB_CMD_BRC_SET_MODE             :  // 1
                     switch( task_Q_Record.Index)
                     {
                        case  BC_READER1  :
                              p_NVRAM_APP_B2B_TX_DATA->BC_Reader_SetOperationMode   =  task_Q_Record.Value;
                              break;

                        case  BC_READER2  :
                              p_NVRAM_APP_B2B_TX_DATA->BC_Reader_SetOperationMode   =  task_Q_Record.Value;
                              break;
                     }
                     break;

               case  SUB_CMD_BRC_HANDLE_TICKET        :  // 3
                     switch( task_Q_Record.Index)
                     {
                        case  BC_READER1  :
                              p_NVRAM_APP_B2B_TX_DATA->BC_Reader_HandleTicket       =  task_Q_Record.Value;
                              break;

                        case  BC_READER2  :
                              p_NVRAM_APP_B2B_TX_DATA->BC_Reader_HandleTicket       =  task_Q_Record.Value;
                              break;
                     }
                     break;
            }
            break;

      case  B2B_CMD_CLOCK           :  // 'C'
            break;

      case  B2B_CMD_DEFAULT_PARAMS  :  // 'D'
            break;

      case  B2B_CMD_BILL_VALIDATOR  :  // 'G'
            switch( task_B2B_TxCommand.Sub_Command)
            {
               case  SUB_CMD_BLV_SET_MODE             :  // 1
                     p_NVRAM_APP_B2B_TX_DATA->Bill_Valid_SetOperationMode  =  task_Q_Record.Value;
                     break;

               case  SUB_CMD_BLV_HANDLE_BILL          :  // 3
                     p_NVRAM_APP_B2B_TX_DATA->Bill_Valid_HandleBill        =  task_Q_Record.Value;
                     break;

               case  SUB_CMD_BLV_SET_ENABLED_BILLS    :  // 5
                     p_NVRAM_APP_B2B_TX_DATA->Bill_Valid_EnabledBills      =  task_EnabledBills;
                     break;
            }
            break;

      case  B2B_CMD_MGC             :  // 'M'
            switch( task_B2B_TxCommand.Sub_Command)
            {
               case  SUB_CMD_MGC_SET_MODE             :  // 1
                     p_NVRAM_APP_B2B_TX_DATA->MGC_SetOperationMode         =  task_Q_Record.Value;
                     break;
            }
            break;

      case  B2B_CMD_PRINTER         :  // 'P'
            switch( task_B2B_TxCommand.Sub_Command)
            {
               case  SUB_CMD_PRN_SET_FONT             :  // 1
                     p_NVRAM_APP_B2B_TX_DATA->Printer_SetFont.Type   =  task_Printer.Font_Type;
                     p_NVRAM_APP_B2B_TX_DATA->Printer_SetFont.Height =  task_Printer.Font_Height;
                     p_NVRAM_APP_B2B_TX_DATA->Printer_SetFont.Width  =  task_Printer.Font_Width;
                     break;

               case  SUB_CMD_PRN_PRINT_TEXT           :  // 2
                     {
                     byte     BytesToPrint;

                        
                        for( BytesToPrint = 0; BytesToPrint < MAX_BYTES_PRINT_TEXT; BytesToPrint ++)
                        {
                           if( task_PrinterBuf_R_Idx == task_PrinterBuf_W_Idx)
                              break;
                              
                           p_NVRAM_APP_B2B_TX_DATA->Printer_PrintText.Text[BytesToPrint]  =  p_NVRAM_PRN_TX_BUF[task_PrinterBuf_R_Idx ++];  
                           if( task_PrinterBuf_R_Idx >= PRN_TX_BUF_SIZE)   task_PrinterBuf_R_Idx   =  0;
                        }
                        
                        task_fPrinterIsBusy  =  FALSE;

                        if( BytesToPrint == 0)
                           break;

                        task_PrinterBuf_Count   =  (task_PrinterBuf_Count >= BytesToPrint) ? (task_PrinterBuf_Count - BytesToPrint) : 0;
                        p_NVRAM_APP_B2B_TX_DATA->Printer_PrintText.Alignment  =  PRINT_ALIGN_VOID;
                        p_NVRAM_APP_B2B_TX_DATA->Printer_PrintText.Count      =  BytesToPrint;
                     }
                     break;

               case  SUB_CMD_PRN_CUTTER               :  // 3
                     p_NVRAM_APP_B2B_TX_DATA->Printer_SetCutterMode  =  task_Printer.Cutter_Mode;
                     break;

               case  SUB_CMD_PRN_PRINT_BARCODE_LABLE  :  // 4
                     p_NVRAM_APP_B2B_TX_DATA->Printer_BarCode.Type   =  task_Printer.Barcode_Type;
                     p_NVRAM_APP_B2B_TX_DATA->Printer_BarCode.Number =  task_Printer.Barcode_Number;
                     break;
            }
            break;

      case  B2B_CMD_GET_STATUS      :  // 'S'
            break;

      case  B2B_CMD_LED             :  // 'T'
            switch( task_B2B_TxCommand.Sub_Command)
            {
               case  SUB_CMD_LED_SET_MODE             :  // 0
                     p_NVRAM_APP_B2B_TX_DATA->LED_SetOperation.Mode  =  task_LED[task_Q_Record.Index].Mode;
                     p_NVRAM_APP_B2B_TX_DATA->LED_SetOperation.Time  =  task_LED[task_Q_Record.Index].Time;
                     break;
            }
            break;

      case  B2B_CMD_COIN_VALIDATOR  :  // 'V'
            switch( task_B2B_TxCommand.Sub_Command)
            {
               case  SUB_CMD_CNV_SET_MODE             :  // 1
                     p_NVRAM_APP_B2B_TX_DATA->Coin_Valid_SetOperationMode  =  task_Q_Record.Value;
                     break;

               case  SUB_CMD_CNV_SET_ENABLED_COINS    :  // 3
                     p_NVRAM_APP_B2B_TX_DATA->Coin_Valid_EnabledCoins      =  task_EnabledCoins;
                     break;

               case  SUB_CMD_CNV_SET_COIN             :  // 4
                     p_NVRAM_APP_B2B_TX_DATA->Coin_Valid_SetCoin.Value     =  task_Coin_Value;
                     p_NVRAM_APP_B2B_TX_DATA->Coin_Valid_SetCoin.Routing   =  task_Coin_Routing;
                     break;
            }
            break;

      case  B2B_CMD_BUZZER          :  // 'Z'
            switch( task_B2B_TxCommand.Sub_Command)
            {
               case  SUB_CMD_BUZ_SET_MODE             :  // 0
                     p_NVRAM_APP_B2B_TX_DATA->Buzzer_SetOperation.Mode     =  task_Buzzer.Mode;
                     p_NVRAM_APP_B2B_TX_DATA->Buzzer_SetOperation.Time     =  task_Buzzer.Time;
                     break;
            }
            break;
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Reader_SetMode( byte  Device, byte   Mode)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_BAR_CODE_READER, SUB_CMD_BRC_SET_MODE, Device, 0, Mode);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Reader_GetLabel( byte   Device)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_BAR_CODE_READER, SUB_CMD_BRC_GET_BARCODE_LABEL, Device, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Reader_HandleTicket( byte   Device, byte   Mode)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_BAR_CODE_READER, SUB_CMD_BRC_HANDLE_TICKET, Device, 0, Mode);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Bills_SetMode( byte   Mode)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_BILL_VALIDATOR, SUB_CMD_BLV_SET_MODE, 0, 0, Mode);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Bills_GetValue        ( void)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_BILL_VALIDATOR, SUB_CMD_BLV_GET_BILL_VALUE, 0, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Bills_HandleBill( byte   Mode)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_BILL_VALIDATOR, SUB_CMD_BLV_HANDLE_BILL, 0, 0, Mode);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Bills_EnableBills( usint  Enabled)
/*----------------------------------------------------------------------------*/
{
   task_Param_A_Rec   =  0;
   Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, PARAM_A_BLOCKED_BILLS);
   task_EnabledBills =  task_Param_A_Rec;
   Task_Application_CommandQueueIn( B2B_CMD_BILL_VALIDATOR, SUB_CMD_BLV_SET_ENABLED_BILLS, 0, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Buzzer_SetMode( byte   Mode, byte  Time)
/*----------------------------------------------------------------------------*/
{
   task_Buzzer.Mode  =  Mode;
   task_Buzzer.Time  =  Time;
   Task_Application_CommandQueueIn( B2B_CMD_BUZZER, SUB_CMD_BUZ_SET_MODE, 0, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Coins_SetMode( byte   Mode)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_COIN_VALIDATOR, SUB_CMD_CNV_SET_MODE, 0, 0, Mode);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Coins_EnableCoins( usint  Enabled)
/*----------------------------------------------------------------------------*/
{
   task_Param_A_Rec   =  0;
   Task_Params_Read( &task_Param_A_Rec, PARAM_TYPE_A, PARAM_A_BLOCKED_COINS);
   task_EnabledCoins =  task_Param_A_Rec;
   Task_Application_CommandQueueIn( B2B_CMD_COIN_VALIDATOR, SUB_CMD_CNV_SET_ENABLED_COINS, 0, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Coins_SetCoin( byte   Index,   usint  Value, byte Routing)
/*----------------------------------------------------------------------------*/
{
   task_Coin_Value   =  Value;
   task_Coin_Routing =  Routing;
   Task_Application_CommandQueueIn( B2B_CMD_COIN_VALIDATOR, SUB_CMD_CNV_SET_COIN, Index, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Coins_GetValue( void)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_COIN_VALIDATOR, SUB_CMD_CNV_GET_COIN_VALUE, 0, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Led_SetMode( byte   Led,  byte   Mode, byte  Time)
/*----------------------------------------------------------------------------*/
{
   task_LED[Led].Mode   =  Mode;
   task_LED[Led].Time   =  Time;
   Task_Application_CommandQueueIn( B2B_CMD_LED, SUB_CMD_LED_SET_MODE, Led, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Mgc_SetMode( byte   Mode)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_MGC, SUB_CMD_MGC_SET_MODE, 0, 0, Mode);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Mgc_GetStatus( void)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_MGC, SUB_CMD_MGC_GET_STATUS, 0, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Mgc_GetTrack2( void)
/*----------------------------------------------------------------------------*/
{
   Task_Application_CommandQueueIn( B2B_CMD_MGC, SUB_CMD_MGC_GET_TRACK2, 0, 0, 0);
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_CommandQueueInit( void)
/*----------------------------------------------------------------------------*/
{
   memset( p_NVRAM_APP_COMMANDS, 0, (MAX_APP_COMMANDS *  sizeof( tCommand)));
   task_Q_Cmd_WriteIdx  =  0;
   task_Q_Cmd_ReadIdx   =  0;
   task_Q_fIsEmpty      =  TRUE;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_CommandQueueIn( byte  Command, byte  Sub_Command, byte   Index, byte Sub_Index, byte Value)
/*----------------------------------------------------------------------------*/
{
   task_Q_Record.UnitType     =  task_Slave_UnitType;
   task_Q_Record.Address      =  task_Slave_Address;
   task_Q_Record.Command      =  Command;
   task_Q_Record.Sub_Command  =  Sub_Command;
   task_Q_Record.Index        =  Index;
   task_Q_Record.Sub_Index    =  Sub_Index;
   task_Q_Record.Value        =  Value;

   if( task_Q_Cmd_WriteIdx >= MAX_APP_COMMANDS)
      task_Q_Cmd_WriteIdx  =  0;

   p_NVRAM_APP_COMMANDS[task_Q_Cmd_WriteIdx] =  task_Q_Record;

   if( (++task_Q_Cmd_WriteIdx) >= MAX_APP_COMMANDS)
      task_Q_Cmd_WriteIdx  =  0;

   if( task_Q_Cmd_WriteIdx == task_Q_Cmd_ReadIdx)
   {
      if( (++task_Q_Cmd_ReadIdx) >= MAX_APP_COMMANDS)
         task_Q_Cmd_ReadIdx   =  0;
   }

   task_Q_fIsEmpty            =  FALSE;
}



/*----------------------------------------------------------------------------*/
static	bool                 Task_Application_CommandQueueOut( void)
/*----------------------------------------------------------------------------*/
{
   if( (task_Q_Cmd_ReadIdx == task_Q_Cmd_WriteIdx) || (task_Q_Cmd_ReadIdx >= MAX_APP_COMMANDS))
   {
      task_Q_Cmd_WriteIdx  =  0;
      task_Q_Cmd_ReadIdx   =  0;
      task_Q_fIsEmpty      =  TRUE;
      return( FALSE);
   }
   else
   {
      task_Q_Record        =  p_NVRAM_APP_COMMANDS[task_Q_Cmd_ReadIdx];

      if( (++task_Q_Cmd_ReadIdx) >= MAX_APP_COMMANDS)
         task_Q_Cmd_ReadIdx   =  0;

      return( TRUE);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Display_HandleScreen( void)
/*----------------------------------------------------------------------------*/
{
static   char  Label[MAX_STRING_LEN_LBL];
ulong          lValue;
bool           fRun;


   if( Task_Display_IsBusy() == TRUE)
   {
      
      return;
   }

   fRun  =  TRUE;
   
   switch( task_Screen)
   {
      case  VGA_SCREEN_VOID               :
            fRun        =  FALSE;
            break;

      case  VGA_SCREEN_READY              :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_LEFT);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_3);
            Task_Display_SetColor         ( VGA_COLOR_CYAN);

            // PRODUCTS
            // Product 1 - Name
            if( task_TransProducts_Disabled[0] == FALSE)
            {
               Task_Params_Read              ( task_Param_E_Rec, PARAM_TYPE_E, (App_Language == LANGUAGE_HEBREW) ? STR_E_PROD_01_NAME_HE : STR_E_PROD_01_NAME_EN);
               Util_StrCpy                   ( p_NVRAM_APP_TXT_BUF,  task_Param_E_Rec, (MAX_STRING_LEN_VGA - 1));
               Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
               Task_Display_WriteString      ( LINE1, 0, p_NVRAM_APP_TXT_BUF);
            }

            // Product 2 - Name
            if( task_TransProducts_Disabled[1] == FALSE)
            {
               Task_Params_Read              ( task_Param_E_Rec, PARAM_TYPE_E, (App_Language == LANGUAGE_HEBREW) ? STR_E_PROD_02_NAME_HE : STR_E_PROD_02_NAME_EN);
               Util_StrCpy                   ( p_NVRAM_APP_TXT_BUF,  task_Param_E_Rec, (MAX_STRING_LEN_VGA - 1));
               Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
               Task_Display_WriteString      ( LINE4, 0, p_NVRAM_APP_TXT_BUF);
            }

            // Product 3 - Name
            if( task_TransProducts_Disabled[2] == FALSE)
            {
               Task_Params_Read              ( task_Param_E_Rec, PARAM_TYPE_E, (App_Language == LANGUAGE_HEBREW) ? STR_E_PROD_03_NAME_HE : STR_E_PROD_03_NAME_EN);
               Util_StrCpy                   ( p_NVRAM_APP_TXT_BUF,  task_Param_E_Rec, (MAX_STRING_LEN_VGA - 1));
               Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
               Task_Display_WriteString      ( LINE7, 0, p_NVRAM_APP_TXT_BUF);
            }
            // Product 1 - Price
            if( task_TransProducts_Disabled[0] == FALSE)
            {
               Task_Params_Read              ( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_PROD_01_PRICE);
               Task_Display_WritePrice       ( LINE2, 0, task_Param_C_Rec);
            }

            // Product 2 - Price
            if( task_TransProducts_Disabled[1] == FALSE)
            {
               Task_Params_Read              ( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_PROD_02_PRICE);
               Task_Display_WritePrice       ( LINE5, 0, task_Param_C_Rec);
            }

            // Product 3 - Price
            if( task_TransProducts_Disabled[2] == FALSE)
            {
               Task_Params_Read              ( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_PROD_03_PRICE);
               Task_Display_WritePrice       ( LINE8, 0, task_Param_C_Rec);
            }

            /***********/
            if( p_NVRAM_APP_TRANS_INFO->Value_Total > 0)
            {
               Task_Display_SetAlignment     ( VGA_ALIGNMENT_VOID);
               Task_Display_SetColor         ( VGA_COLOR_YELLOW);

               // Product 1 - Amount Selected
               if( task_TransProducts_Selected[PRODUCT_1] > 0)
               {
                  sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_1], task_TransProducts_Selected[PRODUCT_1]);
                  Task_Display_WriteString      ( LINE2, 5, p_NVRAM_APP_TXT_BUF);
               }

               // Product 2 - Amount Selected
               if( task_TransProducts_Selected[PRODUCT_2] > 0)
               {
                  sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_1], task_TransProducts_Selected[PRODUCT_2]);
                  Task_Display_WriteString      ( LINE5, 5, p_NVRAM_APP_TXT_BUF);
               }

               // Product 3 - Amount Selected
               if( task_TransProducts_Selected[PRODUCT_3] > 0)
               {
                  sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_1], task_TransProducts_Selected[PRODUCT_3]);
                  Task_Display_WriteString      ( LINE8, 5, p_NVRAM_APP_TXT_BUF);
               }

               // TOTAL
               if( (p_NVRAM_APP_TRANS_INFO->Value_Paid == 0) && (p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident == 0))
               {
                  Task_Display_SetAlignment     ( VGA_ALIGNMENT_RIGHT);
                  Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_7);
                  Task_Display_SetColor         ( VGA_COLOR_LIGHT_GREEN);

                  // Total - Lebel
                  strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_TOTAL_LABEL][App_Language]);
                  Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
                  Task_Display_WriteString      ( LINE2, 0, p_NVRAM_APP_TXT_BUF);

                  // (Actual) Total - Value
                  Task_Display_WritePrice       ( LINE4, 0, p_NVRAM_APP_TRANS_INFO->Value_Total);
               }
               else
               {
                  Task_Display_SetAlignment     ( VGA_ALIGNMENT_RIGHT);
                  Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_4);
                  Task_Display_SetColor         ( VGA_COLOR_LIGHT_GREEN);

                  strcpy_P( Label, Messages[VGA_MSG_TOTAL_LABEL][App_Language]);
                  Util_String_RemoveEdgeSpaces( Label);
                  sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_2],
                                          (usint)(p_NVRAM_APP_TRANS_INFO->Value_Total / 100),
                                          (usint)(p_NVRAM_APP_TRANS_INFO->Value_Total % 100),
                                          Label);
                  Task_Display_WriteString      ( LINE2, 0, p_NVRAM_APP_TXT_BUF);

                  strcpy_P( Label, Messages[VGA_MSG_DISCOUNT][App_Language]);
                  Util_String_RemoveEdgeSpaces( Label);
                  sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_2],
                                          (usint)(p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident / 100),
                                          (usint)(p_NVRAM_APP_TRANS_INFO->Value_Discount_Resident % 100),
                                          Label);
                  Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

                  strcpy_P( Label, Messages[VGA_MSG_PAID_LABEL][App_Language]);
                  Util_String_RemoveEdgeSpaces( Label);
                  sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_2],
                                          (usint)(p_NVRAM_APP_TRANS_INFO->Value_Paid / 100),
                                          (usint)(p_NVRAM_APP_TRANS_INFO->Value_Paid % 100),
                                          Label);
                  Task_Display_WriteString      ( LINE4, 0, p_NVRAM_APP_TXT_BUF);

                  strcpy_P( Label, Messages[VGA_MSG_DUE_LABEL][App_Language]);
                  Util_String_RemoveEdgeSpaces( Label);
                  sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_2],
                                          (usint)(p_NVRAM_APP_TRANS_INFO->Value_Due / 100),
                                          (usint)(p_NVRAM_APP_TRANS_INFO->Value_Due % 100),
                                          Label);
                  Task_Display_WriteString      ( LINE6, 0, p_NVRAM_APP_TXT_BUF);
               }
            }
            break;

      case  VGA_SCREEN_OUT_OF_ORDER          :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_3);
            Task_Display_SetColor         ( VGA_COLOR_LIGHT_RED);

            // Out of Order - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_OUT_OF_ORDER][App_Language]);
            Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

            // Out of Order - Value
            Task_Display_WriteNumberHex   ( LINE4, 0, task_Flags_Error, 4);
            break;

      case  VGA_SCREEN_PLEASE_WAIT           :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_6);
            Task_Display_SetColor         ( VGA_COLOR_YELLOW);

            // Please Wait - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_PLEASE_WAIT][App_Language]);
            Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

            // Checking Card - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_CHECKING_CARD][App_Language]);
            Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE5, 0, p_NVRAM_APP_TXT_BUF);
            break;

      case  VGA_SCREEN_MGC_ACTION_FAILED     :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_6);
            Task_Display_SetColor         ( VGA_COLOR_YELLOW);

            // MGC Action Failed - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_MGC_ACTION_FAILED][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

            // Out of Order - Value
            Task_Display_WriteNumber      ( LINE4, 0, task_ErrCode, 3);
            break;

      case  VGA_SCREEN_MGC_DIGITEL_OVER_LIMIT:
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_4);
            Task_Display_SetColor         ( VGA_COLOR_YELLOW);

            // MGC Digitel Over Limit - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_MGC_DIGITEL_OVER_LIMIT][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);
            break;

      case  VGA_SCREEN_MGC_USAGE_DENIED      :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_4);
            Task_Display_SetColor         ( VGA_COLOR_LIGHT_RED);

            // Card is Denied - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_CARD_IS_DENIED][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

            // After Cash Payment - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_AFTER_CASH_PAYMENT][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE4, 0, p_NVRAM_APP_TXT_BUF);
            break;

      case  VGA_SCREEN_MGC_NO_ELIGIBILITY    :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_6);
            Task_Display_SetColor         ( VGA_COLOR_YELLOW);

            // MGC No Eligibility - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_MGC_NO_ELIGIBILITY][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);
            break;

      case  VGA_SCREEN_MGC_DISCOUNT_GRANTED  :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_5);
            Task_Display_SetColor         ( VGA_COLOR_LIGHT_GREEN);

            // Discount Granted - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_MGC_DISCOUNT_GRANTED][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

            // In Total Value Of - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_MGC_IN_VALUE_OF][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE4, 0, p_NVRAM_APP_TXT_BUF);

            // Discount - Value
            Task_Display_WritePrice       ( LINE5, 0, task_ResidentCard_Discount);
            break;

      case  VGA_SCREEN_RECEIPT_AND_CHANGE    :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_5);
            Task_Display_SetColor         ( VGA_COLOR_LIGHT_GREEN);

            // Take the Receipt - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_TAKE_THE_RECEIPT][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

            if( p_NVRAM_APP_TRANS_INFO->Value_ChangeToGive > 0)
            {
               // ...And the Change - Lebel
               strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_AND_THE_CHANGE][App_Language]);
               Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
               Task_Display_WriteString      ( LINE4, 0, p_NVRAM_APP_TXT_BUF);

               /*
               strcpy_P( Label, Messages[VGA_MSG_CHANGE][App_Language]);
               Util_String_RemoveEdgeSpaces( Label);
               sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_2],
                                       (usint)(p_NVRAM_APP_TRANS_INFO->Value_ChangeToGive / 100),
                                       (usint)(p_NVRAM_APP_TRANS_INFO->Value_ChangeToGive % 100),
                                       Label);
               Task_Display_WriteString      ( LINE6, 0, p_NVRAM_APP_TXT_BUF);
               */
            }
            break;

      case  VGA_SCREEN_TRANSACTION_CANCELLED :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_6);
            Task_Display_SetColor         ( VGA_COLOR_LIGHT_RED);

            // Transaction Cancelled - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_TRANSACTION_CANCELLED][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);
            break;

      case  VGA_SCREEN_TIBAPAY_COMM_LOSS:
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_5);
            Task_Display_SetColor         ( VGA_COLOR_LIGHT_RED);

            // TibaPay communication Loss - Label
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_TIBA_PAY_COMM_LOSS][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

            // Out of Order - Value
            Task_Display_WriteNumber      ( LINE4, 0, task_ErrCode, 3);   
            break;
            
      case VGA_SCREEN_NO_CREDIT_SERVICE:
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_4);
            Task_Display_SetColor         ( VGA_COLOR_LIGHT_RED);

            // No Credit Card Service
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_NO_CREDIT_SERVICE][App_Language]);
            Util_String_RemoveEdgeSpaces( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);
            break;

//------------------------------------------------------------------------------------------------            
      case  VGA_SCREEN_PRESENT_DIGITEL_CARD  :
            Task_Display_SetAlignment     ( VGA_ALIGNMENT_CENTER);
            Task_Display_SetFont          ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_6);
            Task_Display_SetColor         ( VGA_COLOR_YELLOW);

            // Please Wait - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_PRESENT][App_Language]);
            Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE3, 0, p_NVRAM_APP_TXT_BUF);

            // Checking Card - Lebel
            strcpy_P( p_NVRAM_APP_TXT_BUF, Messages[VGA_MSG_DIGITEL_CARD][App_Language]);
            Util_String_RemoveEdgeSpaces  ( p_NVRAM_APP_TXT_BUF);
            Task_Display_WriteString      ( LINE5, 0, p_NVRAM_APP_TXT_BUF);
            break;
//------------------------------------------------------------------------------------------------            
            
            
      default:
            fRun        =  FALSE;
            break;
   }

   if( fRun == TRUE)
   {
      SET_BIT( task_Display_AddOn, VGA_ADD_ON_INFO);
      SET_BIT( task_Display_AddOn, VGA_ADD_ON_CLOCK);
      SET_BIT( task_Display_AddOn, VGA_ADD_ON_DEBUG_INFO);

      task_Screen_last  =  task_Screen;
      task_Screen       =  VGA_SCREEN_VOID;
      Task_Display_Run();
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Display_HandleDebugInfo( void)
/*----------------------------------------------------------------------------*/
{
static   byte  BV_Status   =  0xFF;
static   byte  BV_Status_1 =  0xFF;
static   byte  BV_Status_2 =  0xFF;
static   byte  BV_Status_3 =  0xFF;
static   byte  PRN_Status  =  0;
bool           fShowInfo;


   fShowInfo   =  FALSE;

   if( BV_Status != task_B2B_pRxData->GetStatus.Bill_Validator)
   {
      BV_Status   =  task_B2B_pRxData->GetStatus.Bill_Validator;
      fShowInfo   =  TRUE;
   }

   if( BV_Status_1 != task_B2B_pRxData->GetStatus.BV_Status_1)
   {
      BV_Status_1 =  task_B2B_pRxData->GetStatus.BV_Status_1;
      fShowInfo   =  TRUE;
   }

   if( BV_Status_2 != task_B2B_pRxData->GetStatus.BV_Status_2)
   {
      BV_Status_2 =  task_B2B_pRxData->GetStatus.BV_Status_2;
      fShowInfo   =  TRUE;
   }

   if( BV_Status_3 != task_B2B_pRxData->GetStatus.BV_Status_3)
   {
      BV_Status_3 =  task_B2B_pRxData->GetStatus.BV_Status_3;
      fShowInfo   =  TRUE;
   }

   if( PRN_Status != task_B2B_pRxData->GetStatus.PRN_Status)
   {
      //-----------------------------------------------------------------------------------------//
      // task_B2B_pRxData->GetStatus.PRN_Status: 0x11=o.k  0x91= Paper roll is absent Bit 7
      // task_B2B_pRxData->GetStatus.Printer:     0x00=o.k  0x02= Paper roll is absent (Bit 1
      //-----------------------------------------------------------------------------------------//
      PRN_Status  =  task_B2B_pRxData->GetStatus.PRN_Status;
      fShowInfo   =  TRUE;

   }

   if( fShowInfo == TRUE)
      SET_BIT( task_Display_AddOn, VGA_ADD_ON_DEBUG_INFO);      // bit 2
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Display_HandleAddOn( void)
/*----------------------------------------------------------------------------*/
{
ulong    lValue;
usint    Mask;
usint    AddOn;
bool     fRun;


   if( Task_Display_IsBusy() == TRUE)
      return;

   if( task_Display_AddOn == 0)
      return;

   for( Mask = BIT_0; Mask != 0; Mask <<= 1)
   {
      AddOn =  (task_Display_AddOn & Mask);
      CLEAR_BIT( task_Display_AddOn, Mask);

      Task_Display_AddOn_Object( TRUE);

      fRun  =  FALSE;
      
      switch( AddOn)
      {
         case  VGA_ADD_ON_INFO      	      :
               Task_Display_SetAlignment  ( VGA_ALIGNMENT_RIGHT);
               Task_Display_SetFont       ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_2);
               Task_Display_SetColor      ( VGA_COLOR_WHITE);

               sprintf_P( p_NVRAM_APP_TXT_BUF, Sprintf_Strings[SPRINTF_STR_3], task_Flags_Warning);
               Util_StrCat_Const( p_NVRAM_APP_TXT_BUF, App_Version);
               Task_Display_WriteString   ( (VGA_FONT_2_MAX_LINES - 1), 0, p_NVRAM_APP_TXT_BUF);
               break;

         case  VGA_ADD_ON_CLOCK				   :
               Task_Display_SetAlignment  ( VGA_ALIGNMENT_RIGHT);
               Task_Display_SetFont       ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_2);
               Task_Display_SetColor      ( VGA_COLOR_WHITE);
               strcpy( p_NVRAM_APP_TXT_BUF, ClockAPI_GetDateStr( DATE_FORMAT_DDMMYY, DATE_DELIMITER_SLASH));
               Task_Display_WriteString   ( LINE1, 0, p_NVRAM_APP_TXT_BUF);
               strcpy( p_NVRAM_APP_TXT_BUF, ClockAPI_GetTimeStr( TIME_FORMAT_HHMM, TIME_DELIMITER_COLON));
               Task_Display_WriteString   ( LINE2, 0, p_NVRAM_APP_TXT_BUF);
               break;

         case  VGA_ADD_ON_DEBUG_INFO		   :
               Task_Display_SetAlignment  ( VGA_ALIGNMENT_RIGHT);
               Task_Display_SetFont       ( VGA_FONT_NAME_ARIAL, VGA_FONT_STYLE_BOLD, VGA_FONT_SIZE_2);
               Task_Display_SetColor      ( VGA_COLOR_WHITE);

               sprintf( p_NVRAM_APP_TXT_BUF, "BV-%.2X %.2X%.2X%.2X",
                                          task_B2B_pRxData->GetStatus.Bill_Validator,
                                          task_B2B_pRxData->GetStatus.BV_Status_1,
                                          task_B2B_pRxData->GetStatus.BV_Status_2,
                                          task_B2B_pRxData->GetStatus.BV_Status_3);
               Task_Display_WriteString   ( (VGA_FONT_2_MAX_LINES - 2), 0, p_NVRAM_APP_TXT_BUF);

               Task_Display_SetAlignment  ( VGA_ALIGNMENT_VOID);
               sprintf( p_NVRAM_APP_TXT_BUF, "PR-%.2X",
                                          task_B2B_pRxData->GetStatus.PRN_Status);
               Task_Display_WriteString   ( (VGA_FONT_2_MAX_LINES - 1), 17, p_NVRAM_APP_TXT_BUF);
               break;

         default                             :
               Task_Display_AddOn_Object( FALSE);
               continue;
      }

      fRun  =  TRUE;
      break;
   }

   if( fRun == TRUE)
      Task_Display_Run();
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Product_EnaDis( byte ProductCode, bool  fMode)
/*----------------------------------------------------------------------------*/
{
byte     Idx;


   if( (fMode != DISABLED) && (fMode != ENABLED))
      return;

   for( Idx = 0; Idx < MAX_PRODUCTS; Idx ++)
   {
      Task_Application_Product_Read( Idx);

      if( (task_Product.Code == ProductCode) || (ProductCode == 0xFF /* All */))
         task_TransProducts_Disabled[Idx] =  (fMode == DISABLED);
   }
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Product_Read( byte Idx)
/*----------------------------------------------------------------------------*/
{
usint    Offset;


   Offset   =  Idx;
   Offset   *= (sizeof( task_Product) / sizeof( task_Param_C_Rec));

   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_CODE         + Offset)); task_Product.Code                =  task_Param_C_Rec;
   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_PRICE        + Offset)); task_Product.Price               =  task_Param_C_Rec;
   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_DISCOUNT_M   + Offset)); task_Product.Discount_Money      =  task_Param_C_Rec;
   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_DISCOUNT_P   + Offset)); task_Product.Discount_Percentage =  task_Param_C_Rec;
   Task_Params_Read( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_MAX_PROD     + Offset)); task_Product.MaxUnitsPerTrans    =  task_Param_C_Rec;
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Product_Write( byte Idx)
/*----------------------------------------------------------------------------*/
{
usint    Offset;


   Offset   =  Idx;
   Offset   *= (sizeof( task_Product) / sizeof( task_Param_C_Rec));

   task_Param_C_Rec  =  task_Product.Code                ;  Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_CODE         + Offset));
   task_Param_C_Rec  =  task_Product.Price               ;  Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_PRICE        + Offset));
   task_Param_C_Rec  =  task_Product.Discount_Money      ;  Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_DISCOUNT_M   + Offset));
   task_Param_C_Rec  =  task_Product.Discount_Percentage ;  Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_DISCOUNT_P   + Offset));
   task_Param_C_Rec  =  task_Product.MaxUnitsPerTrans    ;  Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, (tParam_Code_C)(PARAM_C_PROD_01_MAX_PROD     + Offset));
}



/*----------------------------------------------------------------------------*/
static	void						Task_Application_Debug( void)
/*----------------------------------------------------------------------------*/
{
#if 1
byte     Idx;

CONST	static   char        Products[MAX_PRODUCTS][MAX_LANGUAGES_EXTENDED][20 + 1]   =
                           {
                              {	"Umbrella"     ,  "�����"        }  ,
                              {	"Beach Chair"  ,  "���� ���"     }  ,
                              {	"Lounge Chair" ,  "���� �����"   }  ,
                           };


   BuzzAPI_SetMode( ON, 3);
   for( Idx =  0; Idx < MAX_PRODUCTS; Idx ++)
   {
      strcpy_P( task_Param_E_Rec, Products[Idx][LANGUAGE_HEBREW]);
      Task_Params_Write( task_Param_E_Rec, PARAM_TYPE_E, (STR_E_PROD_01_NAME_HE + (Idx * sizeof( task_Param_E_Rec))));

      strcpy_P( task_Param_E_Rec, Products[Idx][LANGUAGE_ENGLISH]);
      Task_Params_Write( task_Param_E_Rec, PARAM_TYPE_E, (STR_E_PROD_01_NAME_EN + (Idx * sizeof( task_Param_E_Rec))));

      task_Product.Code                =  (Idx + 1);
      task_Product.Price               =  (Idx == 2) ? 1200 : 600;
      task_Product.Discount_Money      =  (Idx == 2) ? 800 : 400;;
      task_Product.Discount_Percentage =  0;
      task_Product.MaxUnitsPerTrans    =  5;
      Task_Application_Product_Write( Idx);
   }
#endif
}




/*----------------------------------------------------------------------------*/
bool		        Task_Application_isMsgToHalt_ClockDisplay(byte MsgCode)
/*----------------------------------------------------------------------------*/
{
   return ( (MsgCode == MSG_READY) ? FALSE : TRUE );
}


/*----------------------------------------------------------------------------*/
void                    Task_Application_DisplayCommLoss(void)
/*----------------------------------------------------------------------------*/
{
   task_Timer_Wait   =  Task_Timer_SetTimeSec( 5);
   task_Screen       =  VGA_SCREEN_TIBAPAY_COMM_LOSS;
   task_State        =  STATE_MGC_WAIT_MSG;
}



/*----------------------------------------------------------------------------*/
void                    Task_Application_DisplayReDraw(void)
/*----------------------------------------------------------------------------*/
{
   task_Screen       =  task_Screen_last; // this will re-draw the screen  
}


//===================================================================================
/*----------------------------------------------------------------------------*/
void                    Task_Application_ButtonPressedCancle(void)
/*----------------------------------------------------------------------------*/
{
   if ( task_fAutoCancel == TRUE )
   {
      task_fAutoCancel  =  FALSE;
      SET_BIT( task_Event, EVENT_BUTTON_PRESSED_CANCEL);
   }
}



/*----------------------------------------------------------------------------*/
void                    Task_Application_ChangePocketLight(void)
/*----------------------------------------------------------------------------*/
{
   if( BIT_IS_SET( task_fDeviceEnabled, DEVICE_CHANGE_POCKET) /*&& (task_Timer_ChangePocketLight == 0)*/)
   {
      SET_BIT( task_fReqToDisable, DEVICE_CHANGE_POCKET);
   }
}




/*----------------------------------------------------------------------------*/
void                    Task_Application_Test_Leds (byte Led, byte Mode   )
/*----------------------------------------------------------------------------*/
{
   byte time;
   if (Mode == ON)
   {
      time = 50;
   }
   else
   {
      time = 0;
   }
   Task_Application_Led_SetMode     ( Led /*LED_CHANGE_POCKET*/, Mode, time);
}



/*----------------------------------------------------------------------------*/
void                    Task_Application_RecoverEquipmentID(void)
/*----------------------------------------------------------------------------*/
{
   task_EquipmentID  =  (task_B2B_pRxData->GetStatus.IO_DIP_Switches & (BIT_0 | BIT_1 | BIT_2 | BIT_3));
   Task_Server_SetEquipmentID( task_EquipmentID);
   task_Param_C_Rec  =  Task_Server_GetEquipmentID();
   Task_Params_Write( &task_Param_C_Rec, PARAM_TYPE_C, PARAM_C_MACHINE_NUMBER);
}

/*----------------------------------------------------------------------------*/
static  void                    Task_Application_BlockOccasClient(void)
/*----------------------------------------------------------------------------*/
{
   task_State  =  STATE_VGA_SCREEN_1;
}

//-----------------------------------------------------------------------------------------

/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_Coin(usint Coin_Value)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_3.Field1 = Coin_Value;
      task_Event_Data.Type_3.Field2 = 0;
      task_Event_Data.Type_3.Field3 = 0;
      task_Event_Data.Type_3.Field4 = 0;
      
      Task_Application_SetEvent( VMC_EVENT_COIN, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_Bill(usint Bill_Value)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_3.Field1 = Bill_Value;
      task_Event_Data.Type_3.Field2 = 0;
      task_Event_Data.Type_3.Field3 = 0;
      task_Event_Data.Type_3.Field4 = 0;
      Task_Application_SetEvent( VMC_EVENT_BILL, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_OpenDoor(void)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_OPENDOOR, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_CloseDoor(void)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_CLOSEDOOR, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_DigitelAccepted(void)
/*----------------------------------------------------------------------------*/
{
      // A Digitel card was accepted. No arguments
      //
      //
      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_DIGITEL_ACCEPTED, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_DigitelEntitlementCheck(Cmd_E_Products* pProdBuf)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_0.Void[0] = pProdBuf->Products[0].Code;
      task_Event_Data.Type_0.Void[1] = pProdBuf->Products[0].Amount;
      task_Event_Data.Type_0.Void[2] = pProdBuf->Products[1].Code;
      task_Event_Data.Type_0.Void[3] = pProdBuf->Products[1].Amount;
      task_Event_Data.Type_0.Void[4] = pProdBuf->Products[2].Code;
      task_Event_Data.Type_0.Void[5] = pProdBuf->Products[2].Amount;
      task_Event_Data.Type_0.Void[6] = 0;
      task_Event_Data.Type_0.Void[7] = 0;
      
      Task_Application_SetEvent( VMC_EVENT_DIGITEL_ENTITLEMENT_CHECK, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_DigitelEntitlementAnswer(Cmd_D_Data* pDigitel)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_0.Void[0] = pDigitel->Digitel.Products[0].Code;
      task_Event_Data.Type_0.Void[1] = pDigitel->Digitel.Products[0].Amount;
      task_Event_Data.Type_0.Void[2] = pDigitel->Digitel.Products[1].Code;
      task_Event_Data.Type_0.Void[3] = pDigitel->Digitel.Products[1].Amount;
      task_Event_Data.Type_0.Void[4] = pDigitel->Digitel.Products[2].Code;
      task_Event_Data.Type_0.Void[5] = pDigitel->Digitel.Products[2].Amount;
      task_Event_Data.Type_0.Void[6] = 0;
      task_Event_Data.Type_0.Void[7] = 0;
      
      Task_Application_SetEvent( VMC_EVENT_DIGITEL_ENTITLEMENT_ANSWER, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_CreditCardAccepted(void)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_CREDIT_C_ACCEPTED, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_CreditCardRequestConfirm(ulong TotalPrice)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 =  TotalPrice;
      task_Event_Data.Type_2.Field2 = 0;
      Task_Application_SetEvent( VMC_EVENT_CREDIT_C_REQUEST_TO_CONFIRM, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_CreditCardRequestAnswer(ulong TotalPrice, usint Last4Digits)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 = TotalPrice;
      task_Event_Data.Type_2.Field2 = (ulong)Last4Digits;
      Task_Application_SetEvent( VMC_EVENT_CREDIT_C_REQUEST_ANSWER, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_ButtonPressed(byte Button)
/*----------------------------------------------------------------------------*/
{
      memset(task_Event_Data.Type_0.Void, 0, sizeof(task_Event_Data.Type_0.Void));
      task_Event_Data.Type_0.Void[0] = Button;
      Task_Application_SetEvent( VMC_EVENT_BUTTON_PRESSED, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_BillDropCassetteRemoval(tLogEvent_Data *pData)         // Stacker Out
/*----------------------------------------------------------------------------*/
{

      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_BILL_DROP_CASSETTE_REMOVAL, 0, &task_Event_Data);
}



/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_BillDropCassetteReplacement(void)     // Stacker In
/*----------------------------------------------------------------------------*/
{

      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_BILL_DROP_CASSETTE_REPLACEMENT, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_CoinCentralRemoval(tLogEvent_Data *pData)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_COIN_CENTRAL_REMOVAL, 0, &task_Event_Data);
}



/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_CoinCentralReplacement(void)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 =  0;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_COIN_CENTRAL_REPLACEMENT, 0, &task_Event_Data);
}







/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_CoinHopperRefill(byte HopperId, usint Quantity)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_3.Field1 =  (usint)HopperId;
      task_Event_Data.Type_3.Field2 =  Quantity;
      task_Event_Data.Type_3.Field3 =  0;
      task_Event_Data.Type_3.Field4 =  0;
      Task_Application_SetEvent( VMC_EVENT_COIN_HOPPER_REFILL, 0, &task_Event_Data);
}









/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_ExcessCalculated(ulong ExcessCalc)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 =  ExcessCalc;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_EXCESS_CALCULATED, 0, &task_Event_Data);
}


/*----------------------------------------------------------------------------*/
void                            Task_Application_SetEvent_ExcessReturn(ulong ExcessReturned)
/*----------------------------------------------------------------------------*/
{
      task_Event_Data.Type_2.Field1 =  ExcessReturned;
      task_Event_Data.Type_2.Field2 =  0;
      Task_Application_SetEvent( VMC_EVENT_EXCESS_RETURN, 0, &task_Event_Data);
}

  
   

/**************************************************************************************************************************/
/*                                                     CHANGES                                                            */
/*
1. Change 23/5/2017  
In function  Task_Application_HandleErrors
Instead of 
if( BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_ERROR))                      
	CLEAR_BIT( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER);
I wrote:
if( (BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_ERROR)) && (BIT_IS_CLEAR( task_B2B_pRxData->GetStatus.Printer, B2B_STATUS_PRN_NEARLY_OUT_OF_PAPER)))
	CLEAR_BIT( task_Flags_Error, FLAG_ERROR_PRINTER_OUT_OF_PAPER);




*/
/**************************************************************************************************************************/