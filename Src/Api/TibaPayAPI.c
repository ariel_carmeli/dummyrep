/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  ORIGINATOR_MASTER          ';'
#define  ORIGINATOR_MASTER_VOID_CS  ''
#define  ORIGINATOR_SLAVE           ':'

#define  END_PACKET                 CR

#define  RX_TIMEOUT                 300   // in msec
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   STATE_ORIGINATOR           ,  // ';' = Master      '' = Master (w/o Checksum)      ':' = Slave
   STATE_COMMAND              ,
   STATE_DATA                 ,
   STATE_CHECKSUM             ,
   STATE_END_PACKET           ,  // CR
   /******************/
   STATE_LAST
}  tRxTxState;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
//---------------------------------------------------------
typedef struct
{
   byte                 api_RxBuffer[SERVER_MAX_LEN_BUFFER];
   tUart                api_Uart;
   tServerPacket        api_TxPacket;
   tServerPacket        api_RxPacket;
   tTimerMsec           api_TimerMsec_Rx;

   usint                api_RxBufferIdx;
   tRxTxState           api_TxState;
   tRxTxState           api_RxState;
   bool                 api_fRxNewPacket;
   bool                 api_fBusy_Tx;
   bool                 api_fBusy_Rx;
   // Debug
   //byte                 Debug_Buffer[100];
   //byte                 Debug_Buffer_Index;
   //usint                api_MonitorRxBytes;
   bool                 api_fShortCommand;
} tdTP_API_XData;

static tdTP_API_XData *THIS = (tdTP_API_XData *)NVRAM_ADDR_T_TIBAPAY_API_VARS;

/*------------------------ Local Function Prototypes -------------------------*/
static	void					TibaPayAPI_HandleTimer( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		TibaPayAPI_Init( byte ComPort)
/*----------------------------------------------------------------------------*/
{
   memset( THIS, 0, sizeof( tdTP_API_XData));
   
   THIS->api_fRxNewPacket           =  FALSE;
   THIS->api_fBusy_Tx               =  FALSE;

   /*** Communication Setting ***/
   THIS->api_Uart.Com.Port          =  ComPort;
   THIS->api_Uart.Com.BaudRate      =  BAUD_RATE_9600;
   THIS->api_Uart.Com.DataBits      =  DATA_BITS_8;
   THIS->api_Uart.Com.Parity        =  PARITY_NONE;
   THIS->api_Uart.Com.StopBits      =  STOP_BITS_1;
   THIS->api_Uart.Com.RS485_Address =  0;
   THIS->api_Uart.TxFunc            =  TibaPayAPI_Tx_ISR;
   THIS->api_Uart.RxFunc            =  TibaPayAPI_Rx_ISR;
   UartDRV_SetPort( &THIS->api_Uart);
   /*****************************/
   
}



/*----------------------------------------------------------------------------*/
void                    		TibaPayAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
   TibaPayAPI_HandleTimer();
}



/*----------------------------------------------------------------------------*/
void                    		TibaPayAPI_AssignPort( byte ComPort)
/*----------------------------------------------------------------------------*/
{
   UartDRV_SetPort( &THIS->api_Uart);
}



/*----------------------------------------------------------------------------*/
bool                    		TibaPayAPI_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
   THIS->api_fBusy_Rx   =  (THIS->api_TimerMsec_Rx > 0);

   return( THIS->api_fBusy_Tx | THIS->api_fBusy_Rx);
}



/*----------------------------------------------------------------------------*/
void                                    TibaPayAPI_SendData( tServerPacket   *pTxPacket)
/*----------------------------------------------------------------------------*/
{
   if( pTxPacket == NULL)
      return;

   THIS->api_TxPacket   =  *pTxPacket;
   THIS->api_TxState    =  STATE_ORIGINATOR;

   TibaPayAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
bool                    		TibaPayAPI_IsRxNewPacket( void)
/*----------------------------------------------------------------------------*/
{
 
   if( THIS->api_fRxNewPacket == TRUE)
   {
      THIS->api_fRxNewPacket  =  FALSE;
      return( TRUE);
   }
   else
      return( FALSE);
}



/*----------------------------------------------------------------------------*/
tServerPacket                           *TibaPayAPI_GetData( void)
/*----------------------------------------------------------------------------*/
{
   THIS->api_RxPacket.pBuffer =  THIS->api_RxBuffer;
   return( &THIS->api_RxPacket);
}



/*----------------------------------------------------------------------------*/
void                    		TibaPayAPI_Tx_ISR( void)
/*----------------------------------------------------------------------------*/
{
static   usint    BufIdx;
static   byte     Checksum;
static   bool     fChecksumEnabled;
static   bool     fHighNibble;
static   char     HexString[2];
byte              Data;
bool              fTxData;

   Data     =  0;
   fTxData  =  TRUE;

   switch( THIS->api_TxState)
   {
      case  STATE_ORIGINATOR  :
            BufIdx            =  0;
            Checksum          =  0;
            fTxData           =  TRUE;
            fHighNibble       =  TRUE;
            fChecksumEnabled  =  TRUE;
            Data              =  ORIGINATOR_SLAVE;
            THIS->api_TxState       =  STATE_COMMAND;
            break;

      case  STATE_COMMAND     :
            Data              =  THIS->api_TxPacket.Command;
            THIS->api_TxState       =  (THIS->api_TxPacket.BufSize > 0) ? STATE_DATA : STATE_CHECKSUM;
            break;

      case  STATE_DATA        :
            Data  =  THIS->api_TxPacket.pBuffer[BufIdx ++];

            if( (BufIdx >= THIS->api_TxPacket.BufSize) || (BufIdx >= SERVER_MAX_LEN_BUFFER))
               THIS->api_TxState =  STATE_CHECKSUM;
            break;

      case  STATE_CHECKSUM    :
            fChecksumEnabled  =  FALSE;

            if( fHighNibble == TRUE)
            {
               fHighNibble    =  FALSE;
               Util_ByteToHexString( Checksum, HexString);
               Data           =  HexString[0];
            }
            else
            {
               fHighNibble    =  TRUE;
               Data           =  HexString[1];
               THIS->api_TxState    =  STATE_END_PACKET;
            }
            break;

      case  STATE_END_PACKET  :
            Data              =  END_PACKET;
            THIS->api_TxState       =  STATE_LAST;
            break;

      default                 :
      case  STATE_LAST        :
            fTxData           =  FALSE;
            THIS->api_TxState       =  STATE_ORIGINATOR;
            break;
   }

   if( fChecksumEnabled == TRUE)
      Checksum += Data;

   if( fTxData == TRUE)
   {
      THIS->api_fBusy_Tx   =  TRUE;
      THIS->api_Uart.Uart_TxByte( Data);
   }
   else
   {
      THIS->api_fBusy_Tx   =  FALSE;
      THIS->api_Uart.Uart_TxEnd();
   }
}



/*----------------------------------------------------------------------------*/
void                    		TibaPayAPI_Rx_ISR( void)
/*----------------------------------------------------------------------------*/
{
static   usint    BufIdx;
static   byte     RcvByte;
static   byte     Checksum;
static   bool     fHighNibble;
static   char     HexString[2];
byte              Data;
byte              swap;                 // used to swap HexString[0] <> HexString[1]
bool              fInsertByte;

   Data     =  THIS->api_Uart.Uart_RxByte();

   RcvByte  =  0;
   if( THIS->api_fRxNewPacket == TRUE) // if last packet was not handled yet
      return;

   THIS->api_TimerMsec_Rx  =  Task_Timer_SetTimeMsec( 20);

   if( Data == ORIGINATOR_MASTER)   THIS->api_RxState =  STATE_ORIGINATOR;
   if( Data == END_PACKET)          
   {
      THIS->api_RxState =  STATE_END_PACKET;
   }


   switch( THIS->api_RxState)
   {
      case  STATE_ORIGINATOR  :
            if( Data != ORIGINATOR_MASTER)
               break;

            THIS->api_RxPacket.fFromMaster               =  TRUE;
            THIS->api_RxPacket.pBuffer                   =  THIS->api_RxBuffer;
            THIS->api_RxPacket.BufSize                   =  0;
            THIS->api_RxPacket.Idx_CtrlChar_Underscore   =  0;
            
            THIS->api_fShortCommand                      = FALSE;

            BufIdx            =  0;
            Checksum          =  0;
            fHighNibble       =  TRUE;
            THIS->api_RxState       =  STATE_COMMAND;
            break;

      case  STATE_COMMAND     :
            THIS->api_RxPacket.Command       =  Data;
            THIS->api_RxState       =  STATE_DATA;
            if (THIS->api_RxPacket.Command == TP_SERVER_CMD_GET_STATUS)
            {   
               THIS->api_fShortCommand                   = TRUE;        // In that case, check sum nibbles will ariive in a wrong order. Do swap !
            }
            break;

      case  STATE_DATA        :
            fInsertByte          =  FALSE;

            if( ((Data >= '0') && (Data <= '9')) || ((Data >= 'A') && (Data <= 'F')))
            {
               if( fHighNibble == TRUE)
               {
                  fHighNibble    =  FALSE;
                  HexString[0]   =  Data;
               }
               else
               {
                  fHighNibble    =  TRUE;
                  HexString[1]   =  Data;
                  RcvByte        =  Util_HexStringToByte( HexString);
                  fInsertByte    =  TRUE;
               }
            }
            else
            {
               if( Data == SERVER_DELIMITER)  // underscore (_) is a delimiter between data and (machine number and checksum)
                  THIS->api_RxPacket.Idx_CtrlChar_Underscore   =  BufIdx;
               RcvByte           =  Data;
               fInsertByte       =  TRUE;
            }

            if( fInsertByte == TRUE)
            {
               if( BufIdx < sizeof( THIS->api_RxBuffer))
               {
                  THIS->api_RxPacket.pBuffer[BufIdx]  =  RcvByte;
                  THIS->api_RxPacket.BufSize ++;
                  BufIdx               ++;
               }
            }
            break;

      default                 :
      case  STATE_END_PACKET  :
      case  STATE_LAST        :
            if( THIS->api_RxPacket.BufSize > 0)
            {
               Checksum -= HexString[0];  // last 2 ASCII chars are checksum
               Checksum -= HexString[1];
               //----------------------
               if (THIS->api_fShortCommand == TRUE)
               {
                  // Odd amount of data bytes. HexString must be swaped
                  swap = HexString[0];
                  HexString[0] = HexString[1];
                  HexString[1] = swap;
               }
               //----------------------
               THIS->api_fRxNewPacket  =  (Checksum == Util_HexStringToByte( HexString));
               THIS->api_RxPacket.BufSize --;   // Reduce buffer size by 1 byte (checksum)
               THIS->api_RxState       =  STATE_ORIGINATOR;

            }
            
            break;
   }

   Checksum += Data;            // Check Sum Accumulation
   
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Local Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						TibaPayAPI_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( THIS->api_TimerMsec_Rx <  Gap)  THIS->api_TimerMsec_Rx  =  0; else  THIS->api_TimerMsec_Rx  -= Gap;
   }
}


