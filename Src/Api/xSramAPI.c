/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  xSRAM_MODE_IDLE                  0
#define  xSRAM_MODE_READ                  1
#define  xSRAM_MODE_WRITE                 2

#define  xSRAM_BASE_ADDRESS                     (ulong)(HW_DEF_xSRAM_PAGE_SIZE)  // Skip Page 0 (SMP-5: page 0 overlaps addresses range 0x1100 - 0x6FFF)                      // 0x8000
#define  xSRAM_ADDRESS_SERVER_PARAMS            (ulong)(xSRAM_BASE_ADDRESS)                                                                                                   // 0x8000   .. 0xDFFF   (6000    hex)
#define  xSRAM_ADDRESS_SERVER_STR_32            (ulong)(xSRAM_ADDRESS_SERVER_PARAMS          +  ((ulong)MAX_PARAMS_GROUP_C             *  sizeof(  tParams_C_Rec        )))   // 0xE000   .. 0xFFFF   (2000    hex)
#define  xSRAM_ADDRESS_SERVER_VARS              (ulong)(xSRAM_ADDRESS_SERVER_STR_32          +  ((ulong)MAX_PARAMS_GROUP_E             *  sizeof(  tParams_E_Rec        )))   // 0x10000  .. 0x11FFF  (2000    hex)
#define  xSRAM_ADDRESS_SALES_INFO               (ulong)(xSRAM_ADDRESS_SERVER_VARS            +  ((ulong)MAX_SIZE_VARS                                                    ))   // 0x12000
#define  xSRAM_ADDRESS_COINS_INFO               (ulong)(xSRAM_ADDRESS_SALES_INFO             +  ((ulong)1                              *  sizeof(  tApp_SalesInfo       )))   // 0x12071
#define  xSRAM_ADDRESS_BILLS_INFO               (ulong)(xSRAM_ADDRESS_COINS_INFO             +  ((ulong)1                              *  sizeof(  tApp_CoinsInfo       )))   // 0x120B2
#define  xSRAM_ADDRESS_HOPPERS_INFO             (ulong)(xSRAM_ADDRESS_BILLS_INFO             +  ((ulong)1                              *  sizeof(  tApp_BillsInfo       )))   // 0x120E7              (40      hex)
#define  xSRAM_ADDRESS_PRINTER_INFO             (ulong)(xSRAM_ADDRESS_HOPPERS_INFO           +  ((ulong)1                              *  sizeof(  tHoppers_Info        )))   // 0x12127
#define  xSRAM_ADDRESS_LOG_EVENT_INFO           (ulong)(xSRAM_ADDRESS_PRINTER_INFO           +  ((ulong)1                              *  sizeof(  tPrinter_Info        )))   // 0x1214E              (20      hex)
#define  xSRAM_ADDRESS_LOG_EVENT_REC            (ulong)(xSRAM_ADDRESS_LOG_EVENT_INFO         +  ((ulong)1                              *  sizeof(  tLogEvent_Info       )))   // 0x1216E              (4000    hex)
#define  xSRAM_ADDRESS_LOG_TRANS_INFO           (ulong)(xSRAM_ADDRESS_LOG_EVENT_REC          +  ((ulong)MAX_LOG_EVENT_RECORDS          *  sizeof(  tLogEvent_Rec        )))   // 0x1616E              (20      hex)
#define  xSRAM_ADDRESS_LOG_TRANS_REC            (ulong)(xSRAM_ADDRESS_LOG_TRANS_INFO         +  ((ulong)1                              *  sizeof(  tLogTrans_Info       )))   // 0x1618E              (18000   hex)
#define  xSRAM_ADDRESS_LOG_COIN_ROUTING_INFO    (ulong)(xSRAM_ADDRESS_LOG_TRANS_REC          +  ((ulong)MAX_LOG_TRANS_RECORDS          *  sizeof(  tLogTrans_Rec        )))   // 0x2E18E
#define  xSRAM_ADDRESS_LOG_COIN_ROUTING_REC     (ulong)(xSRAM_ADDRESS_LOG_COIN_ROUTING_INFO  +  ((ulong)1                              *  sizeof(  tLogCoinRouting_Info )))   // 0x2E1BB
#define  xSRAM_ADDRESS_LOG_EMERG_INFO           (ulong)(xSRAM_ADDRESS_LOG_COIN_ROUTING_REC   +  ((ulong)MAX_LOG_COIN_ROUTING_RECORDS   *  sizeof(  tLogCoinRouting_Rec  )))   // 0x2F2EB              (20      hex)
#define  xSRAM_ADDRESS_LOG_Z_INFO               (ulong)(xSRAM_ADDRESS_LOG_EMERG_INFO         +  ((ulong)1                              *  sizeof(  tLogEmergency_Info   )))   // 0x2F30B              (20      hex)
#define  xSRAM_ADDRESS_LOG_Z_REC                (ulong)(xSRAM_ADDRESS_LOG_Z_INFO             +  ((ulong)1                              *  sizeof(  tLogZ_Info           )))   // 0x2F32B              (3C0     hex)
#define  xSRAM_ADDRESS_END                      (ulong)(xSRAM_ADDRESS_LOG_Z_REC              +  ((ulong)MAX_LOG_Z_RECORDS              *  sizeof(  tLogZ_Rec            )))   // 0x2F82B
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
CONST	   static   ulong       Addresses[xSRAM_MAX_TYPES]  =
                              {
                                 xSRAM_ADDRESS_SERVER_PARAMS         ,
                                 xSRAM_ADDRESS_SERVER_STR_32         ,
                                 xSRAM_ADDRESS_SERVER_VARS           ,
                                 xSRAM_ADDRESS_SALES_INFO            ,
                                 xSRAM_ADDRESS_COINS_INFO            ,
                                 xSRAM_ADDRESS_BILLS_INFO            ,
                                 xSRAM_ADDRESS_HOPPERS_INFO          ,
                                 xSRAM_ADDRESS_PRINTER_INFO          ,
                                 xSRAM_ADDRESS_LOG_EVENT_INFO        ,
                                 xSRAM_ADDRESS_LOG_EVENT_REC         ,
                                 xSRAM_ADDRESS_LOG_TRANS_INFO        ,
                                 xSRAM_ADDRESS_LOG_TRANS_REC         ,
                                 xSRAM_ADDRESS_LOG_COIN_ROUTING_INFO ,
                                 xSRAM_ADDRESS_LOG_COIN_ROUTING_REC  ,
                                 xSRAM_ADDRESS_LOG_EMERG_INFO        ,
                                 xSRAM_ADDRESS_LOG_Z_INFO            ,
                                 xSRAM_ADDRESS_LOG_Z_REC             ,
                              };

CONST	   static   usint       StructSize[xSRAM_MAX_TYPES] =
                              {
                                 sizeof(  tParams_C_Rec        )     ,
                                 sizeof(  tParams_E_Rec        )     ,
                                 1                                   ,
                                 sizeof(  tApp_SalesInfo       )     ,
                                 sizeof(  tApp_CoinsInfo       )     ,
                                 sizeof(  tApp_BillsInfo       )     ,
                                 sizeof(  tHoppers_Info        )     ,
                                 sizeof(  tPrinter_Info        )     ,
                                 sizeof(  tLogEvent_Info       )     ,
                                 sizeof(  tLogEvent_Rec        )     ,
                                 sizeof(  tLogTrans_Info       )     ,
                                 sizeof(  tLogTrans_Rec        )     ,
                                 sizeof(  tLogCoinRouting_Info )     ,
                                 sizeof(  tLogCoinRouting_Rec  )     ,
                                 sizeof(  tLogEmergency_Info   )     ,
                                 sizeof(  tLogZ_Info           )     ,
                                 sizeof(  tLogZ_Rec            )     ,
                              };

static   ulong    api_Size;
static   ulong    api_Count;
static   byte    *api_pData;
static   ulong    api_Address;
static   byte     api_Mode;

static   byte     drv_Device;
static   byte     drv_Page;
static   usint    drv_Address;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						xSramAPI_SetAddress( tSram_DataType   Type, usint Idx);
static	void						xSramAPI_Locate( ulong  Address);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		xSramAPI_Init( void)
/*----------------------------------------------------------------------------*/
{
   api_Size       =  0;
   api_Count      =  0;
   api_pData      =  NULL;
   api_Address    =  0;
   api_Mode       =  xSRAM_MODE_IDLE;

	xSramDRV_Init();
}



/*----------------------------------------------------------------------------*/
void                    		xSramAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
   _WDR();

   if( api_Count >= api_Size)
      api_Mode =  xSRAM_MODE_IDLE;

   switch( api_Mode)
   {
      case  xSRAM_MODE_IDLE:
            break;

      case  xSRAM_MODE_READ:
            xSramAPI_Locate( api_Address ++);
            api_pData[api_Count ++] =  xSramDRV_Read( drv_Device, drv_Page, drv_Address);
            break;

      case  xSRAM_MODE_WRITE:
            xSramAPI_Locate( api_Address ++);
            xSramDRV_Write( drv_Device, drv_Page, drv_Address, api_pData[api_Count ++]);
            break;

      default:
            break;
   }
}



/*----------------------------------------------------------------------------*/
bool                    		xSramAPI_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
	return( (api_Mode == xSRAM_MODE_IDLE) ? FALSE : TRUE);
}



/*----------------------------------------------------------------------------*/
void                    		xSramAPI_Read( void  *Buffer, usint Size, tSram_DataType   Type, usint Idx, bool  fBlockingMode)
/*----------------------------------------------------------------------------*/
{
   if( (Buffer == NULL) || (Size == 0) || (api_Mode != xSRAM_MODE_IDLE))
      return;

   api_Size    =  Size;
   api_Count   =  0;
   api_pData   =  (byte *)Buffer;
   api_Mode    =  xSRAM_MODE_READ;

   xSramAPI_SetAddress( Type, Idx);

   xSramAPI_Locate( api_Address);

   if( fBlockingMode == TRUE)
   {
      for(;;)
      {
         api_pData[api_Count ++] =  xSramDRV_Read( drv_Device, drv_Page, drv_Address);
         if( api_Count >= api_Size)
            break;

         xSramAPI_Locate( ++ api_Address);
         Task_Hardware_Main( TRUE);
      }

      api_Mode =  xSRAM_MODE_IDLE;
   }
}



/*----------------------------------------------------------------------------*/
void                    		xSramAPI_Read_Address( void  *Buffer, usint Size, ulong   Address, bool  fBlockingMode)
/*----------------------------------------------------------------------------*/
{
   if( (Buffer == NULL) || (Size == 0) || (api_Mode != xSRAM_MODE_IDLE) /*|| ((Address + Size) > HW_DEF_xSRAM_SIZE)*/)
      return;

   api_Size    =  Size;
   api_Count   =  0;
   api_pData   =  (byte *)Buffer;
   api_Mode    =  xSRAM_MODE_READ;
   api_Address =  Address;

   xSramAPI_Locate( api_Address);

   if( fBlockingMode == TRUE)
   {
      for(;;)
      {
         api_pData[api_Count ++] =  xSramDRV_Read( drv_Device, drv_Page, drv_Address);
         if( api_Count >= api_Size)
            break;

         xSramAPI_Locate( ++ api_Address);
         Task_Hardware_Main( TRUE);
      }

      api_Mode =  xSRAM_MODE_IDLE;
   }
}



/*----------------------------------------------------------------------------*/
void                    		xSramAPI_Write( void  *Buffer, usint Size, tSram_DataType  Type, usint Idx, bool  fBlockingMode)
/*----------------------------------------------------------------------------*/
{
   if( (Buffer == NULL) || (Size == 0) || (api_Mode != xSRAM_MODE_IDLE))
      return;

   api_Size       =  Size;
   api_Count      =  0;
   api_pData      =  (byte *)Buffer;
   api_Mode       =  xSRAM_MODE_WRITE;

   xSramAPI_SetAddress( Type, Idx);
   xSramAPI_Locate( api_Address);

   if( fBlockingMode == TRUE)
   {
      for(;;)
      {
         xSramDRV_Write( drv_Device, drv_Page, drv_Address, api_pData[api_Count ++]);
         if( api_Count >= api_Size)
            break;

         xSramAPI_Locate( ++ api_Address);
         Task_Hardware_Main( TRUE);
      }

      api_Mode =  xSRAM_MODE_IDLE;
   }
}



/*----------------------------------------------------------------------------*/
void                          xSramAPI_Write_Address( void  *Buffer, usint Size, ulong  Address, bool  fBlockingMode)
/*----------------------------------------------------------------------------*/
{
   if( (Buffer == NULL) || (Size == 0) || (api_Mode != xSRAM_MODE_IDLE) /*|| ((Address + Size) > HW_DEF_xSRAM_SIZE)*/)
      return;

   api_Size       =  Size;
   api_Count      =  0;
   api_pData      =  (byte *)Buffer;
   api_Mode       =  xSRAM_MODE_WRITE;
   api_Address    =  Address;

   xSramAPI_Locate( api_Address);

   if( fBlockingMode == TRUE)
   {
      for(;;)
      {
         xSramDRV_Write( drv_Device, drv_Page, drv_Address, api_pData[api_Count ++]);
         if( api_Count >= api_Size)
            break;

         xSramAPI_Locate( ++ api_Address);
         Task_Hardware_Main( TRUE);
      }

      api_Mode =  xSRAM_MODE_IDLE;
   }
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						xSramAPI_SetAddress( tSram_DataType   Type, usint Idx)
/*----------------------------------------------------------------------------*/
{
ulong    Offset;


   Offset      =  Idx;
   Offset      *= StructSize[Type];
   api_Address =  Addresses[Type] + Offset;
}



/*----------------------------------------------------------------------------*/
static	void						xSramAPI_Locate( ulong  Address)
/*----------------------------------------------------------------------------*/
{
   drv_Device  =  0;
   drv_Page    =  0;

   while( Address >= HW_DEF_xSRAM_PAGE_SIZE)
   {
      Address  -= HW_DEF_xSRAM_PAGE_SIZE;
      drv_Page ++;
      if( drv_Page >= HW_DEF_xSRAM_MAX_PAGES_IN_DEVICE)
      {
         drv_Page =  0;
         drv_Device  ++;
      }
   }

   drv_Address =  Address;
}

