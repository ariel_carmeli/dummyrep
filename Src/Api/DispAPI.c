/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  MAX_LOCAL_BUF_LEN          50

#define  SHIFT_BYTE                 '@'   // 0x40
#define  SCREEN_MODE_GRAPHICS       0x08
#define  SCREEN_MODE_TEXT           0x09
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   COMMAND_INIT                     ,
   COMMAND_FONT                     ,
   COMMAND_COLOR                    ,
   COMMAND_RECTANGLE                ,
   COMMAND_TEXT                     ,
   COMMAND_GOTO                     ,
   /********************************/
   COMMAND_LAST
}  tDisplayCommand;


CONST	static   char		CommandStr[]   =
                        {        // "  "
/* COMMAND_INIT            */       '$',
/* COMMAND_FONT            */       '!',
/* COMMAND_COLOR           */       '#',
/* COMMAND_RECTANGLE       */       '*',
/* COMMAND_TEXT            */       '%',
/* COMMAND_GOTO            */       '$',
                        };


typedef  enum
{
   SPRINTF_STR_1                    ,  //
   SPRINTF_STR_2                    ,  //
   SPRINTF_STR_3                    ,  //
   SPRINTF_STR_4                    ,  //
   SPRINTF_STR_5                    ,  //
   SPRINTF_STR_6                    ,  //
   /******************/
   SPRINTF_LAST
}  tSprintf;


CONST static   char		   Sprintf_Strings[SPRINTF_LAST][20 + 1]	=              
                           {//"                    "
                              "%c%.3u%.3u"                     ,  // SPRINTF_STR_1
                              "%c%c%c%c"                       ,  // SPRINTF_STR_2
                              "%c%c"                           ,  // SPRINTF_STR_3
                              "%c%.3u%.3u%.3u%.3u"             ,  // SPRINTF_STR_4
                              "%c%.3u%.3u%s%c"                 ,  // SPRINTF_STR_5
                              "%c%.3u%.3u"                     ,  // SPRINTF_STR_6
                           };
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tUart                api_Uart;
static   tVGA_ObjectRec       api_ObjectRec;
static   char                 api_LocalBuf[MAX_LOCAL_BUF_LEN];
static   usint                api_TxBufferIdx;
static   usint                api_TxBufferLen;
static   byte                 api_ScreenMode;
static   byte                 api_TxState;
static   byte                 api_FontSize;
static   bool                 api_fTxBusy;
static   bool                 api_fInitScreen;
static   bool                 api_fAddOn_Object;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						DisplayAPI_HandleTimer ( void);
static	void						DisplayAPI_AdjustHebrew( void);
static	void						DisplayAPI_BuildBuffer ( void);
static	void						DisplayAPI_SetTextOrientation( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		DisplayAPI_Init( byte ComPort)
/*----------------------------------------------------------------------------*/
{
   api_fTxBusy                =  FALSE;
   api_fInitScreen            =  TRUE;

   /*** Communication Setting ***/
   api_Uart.Com.Port          =  ComPort;
   api_Uart.Com.BaudRate      =  BAUD_RATE_9600;
   api_Uart.Com.DataBits      =  DATA_BITS_8;
   api_Uart.Com.Parity        =  PARITY_NONE;
   api_Uart.Com.StopBits      =  STOP_BITS_1;
   api_Uart.TxFunc            =  DisplayAPI_Tx_ISR;
   api_Uart.RxFunc            =  NULL;
   UartDRV_SetPort( &api_Uart);
   /*****************************/
}



/*----------------------------------------------------------------------------*/
void                    		DisplayAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
   DisplayAPI_HandleTimer();
}



/*----------------------------------------------------------------------------*/
bool                          DisplayAPI_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
   return( api_fTxBusy);
}



/*----------------------------------------------------------------------------*/
void                    		DisplayAPI_AddOn_Object ( bool   fMode)
/*----------------------------------------------------------------------------*/
{
   api_fAddOn_Object =  fMode;
}



/*----------------------------------------------------------------------------*/
void                          DisplayAPI_SetObject         ( tVGA_ObjectRec   *pObject)
/*----------------------------------------------------------------------------*/
{
   if( pObject == NULL)
      return;

   if( api_fInitScreen == TRUE)
   {
      api_fInitScreen   =  FALSE;
      api_ScreenMode    =  SCREEN_MODE_GRAPHICS;
      api_TxBufferIdx   =  0;
      api_TxBufferLen   =  0;
      p_NVRAM_TFT_DISPLAY[0]   =  0;
   }

   memcpy( &api_ObjectRec, pObject, sizeof( api_ObjectRec));

   switch( api_ObjectRec.Object)
   {
      case  VGA_OBJECT_FONT         :
            api_FontSize   =  api_ObjectRec.Font.Size;
            break;

      case  VGA_OBJECT_TEXT         :
            if( api_ObjectRec.pText == NULL)
               return;

            break;
   }

   DisplayAPI_BuildBuffer();
}



/*----------------------------------------------------------------------------*/
void                    		DisplayAPI_Run( void)
/*----------------------------------------------------------------------------*/
{
   api_fInitScreen         =  TRUE;
   api_ObjectRec.Alignment =  VGA_ALIGNMENT_VOID;

   api_TxState       =  0;
   DisplayAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
void                    		DisplayAPI_Tx_ISR( void)
/*----------------------------------------------------------------------------*/
{
byte     Data;
bool     fTxData;

   //DEBUG_LINE_ON;

   fTxData  =  TRUE;
   Data     =  0;

   switch( api_TxState)
   {
      case  0  :
            api_TxBufferIdx   =  0;
            Data  =  api_ScreenMode;
            api_TxState ++;
            break;

      case  1  :
            api_TxBufferIdx   =  0;
            Data  =  ';';
            api_TxState ++;
            break;

      case  2  :
            Data  =  p_NVRAM_TFT_DISPLAY[api_TxBufferIdx ++];

            if( api_TxBufferIdx >= api_TxBufferLen)
            {
               api_TxBufferIdx   =  0;
               api_TxState ++;
            }
            break;

      case  3  :
            Data  =  CR;
            api_TxState ++;
            break;

      case  4  :
            Data  =  LF;
            api_TxState ++;
            break;

      case  5  :
      default                    :
            api_TxState =  0;
            fTxData  =  FALSE;
            break;
   }

   if( fTxData == TRUE)
   {
      api_fTxBusy =  TRUE;
      api_Uart.Uart_TxByte( Data);
   }
   else
   {
      api_fTxBusy =  FALSE;
      api_TxState =  0;
      api_Uart.Uart_TxEnd();
   }
   //DEBUG_LINE_OFF;
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						DisplayAPI_HandleTimer ( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec      OldTimer	=	0;
tTimerMsec              NewTimer	=	0;
//byte                    Gap;


   NewTimer = Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      //Gap = (NewTimer - OldTimer);    ???
      OldTimer = NewTimer;
   }
}



/*----------------------------------------------------------------------------*/
static	void						DisplayAPI_AdjustHebrew( void)
/*----------------------------------------------------------------------------*/
{
usint    Position_From;
usint    Position_Length;
usint    i;
bool     fHebrew;


   fHebrew           =  FALSE;
   Position_From     =  0;
   Position_Length   =  0;

   for( i = 0; api_ObjectRec.pText[i] != 0; i ++)
   {
      if( (api_ObjectRec.pText[i] >= 0xE0 /*'�'*/) && (api_ObjectRec.pText[i] <= 0xFA /*'�'*/))
      {
         api_ObjectRec.pText[i]  -= 0x60;

         if( fHebrew == FALSE)
            Position_From  =  i;

         fHebrew  =  TRUE;
      }

      if( fHebrew == TRUE)
         Position_Length   ++;
   }

   if( Position_Length > 0)
      Util_SwapArray( (api_ObjectRec.pText + Position_From), Position_Length);
}



/*----------------------------------------------------------------------------*/
static	void                 DisplayAPI_BuildBuffer ( void)
/*----------------------------------------------------------------------------*/
{
byte  Len;
byte  CharsPerLine;


   switch( api_ObjectRec.Object)
   {
      case  VGA_OBJECT_INIT      :  // Init screen
            sprintf_P( api_LocalBuf, Sprintf_Strings[SPRINTF_STR_1],
                     CommandStr[COMMAND_GOTO],
                           0,
                           0);
            break;

      case  VGA_OBJECT_FONT      :  // Font
            sprintf_P( api_LocalBuf, Sprintf_Strings[SPRINTF_STR_2],
                     CommandStr[COMMAND_FONT],
                           (SHIFT_BYTE + api_ObjectRec.Font.Name),
                           (SHIFT_BYTE + api_ObjectRec.Font.Style),
                           (SHIFT_BYTE + api_ObjectRec.Font.Size));
            break;

      case  VGA_OBJECT_COLOR     :  // Color
            sprintf_P( api_LocalBuf, Sprintf_Strings[SPRINTF_STR_3],
                     CommandStr[COMMAND_COLOR],
                           (SHIFT_BYTE + api_ObjectRec.Color_FG));
            break;

      case  VGA_OBJECT_RECTANGLE :  // Rectangle
            sprintf_P( api_LocalBuf, Sprintf_Strings[SPRINTF_STR_4],
                     CommandStr[COMMAND_RECTANGLE],
                           (api_ObjectRec.X),
                           (api_ObjectRec.Y),
                           (api_ObjectRec.X + api_ObjectRec.Width)  ,
                           (api_ObjectRec.Y + api_ObjectRec.Height));
            break;

      case  VGA_OBJECT_TEXT      :  // Text
            switch( api_FontSize)
            {
               case  VGA_FONT_SIZE_2   :  CharsPerLine   =  VGA_FONT_2_MAX_COLUMNS;       break;
               case  VGA_FONT_SIZE_3   :  CharsPerLine   =  VGA_FONT_3_MAX_COLUMNS;       break;
               case  VGA_FONT_SIZE_4   :  CharsPerLine   =  VGA_FONT_4_MAX_COLUMNS;       break;
               case  VGA_FONT_SIZE_5   :  CharsPerLine   =  VGA_FONT_5_MAX_COLUMNS;       break;
               case  VGA_FONT_SIZE_6   :  CharsPerLine   =  VGA_FONT_6_MAX_COLUMNS;       break;
               case  VGA_FONT_SIZE_7   :  CharsPerLine   =  VGA_FONT_7_MAX_COLUMNS;       break;
               default                 :  api_ObjectRec.Alignment =  VGA_ALIGNMENT_VOID;  break;
            }

            switch( api_ObjectRec.Alignment)
            {
               case  VGA_ALIGNMENT_LEFT      :
                     api_ObjectRec.Offset    =  0;
                     break;

               case  VGA_ALIGNMENT_CENTER    :
                     Len   =  strlen( api_ObjectRec.pText);
                     if( CharsPerLine > Len)
                        api_ObjectRec.Offset =  ((CharsPerLine - Len) / 2);
                     break;

               case  VGA_ALIGNMENT_RIGHT     :
                     Len   =  strlen( api_ObjectRec.pText);
                     if( CharsPerLine > Len)
                        api_ObjectRec.Offset =  (CharsPerLine - Len);
                     break;
            }

            DisplayAPI_AdjustHebrew();
            DisplayAPI_SetTextOrientation();

            sprintf_P( api_LocalBuf, Sprintf_Strings[SPRINTF_STR_5],
                     CommandStr[COMMAND_GOTO],
                           (api_ObjectRec.X),
                           (api_ObjectRec.Y),
                           (api_ObjectRec.pText),
                           CommandStr[COMMAND_TEXT]);
            break;

      case  VGA_OBJECT_GOTO      :  // Goto( x, y)
            sprintf_P( api_LocalBuf, Sprintf_Strings[SPRINTF_STR_6],
                     CommandStr[COMMAND_GOTO],
                           (api_ObjectRec.X),
                           (api_ObjectRec.Y));
            break;

      default:
            return;
   }

   Len   =  strlen( api_LocalBuf);

   if( Len == 0)
      return;

   if( (api_TxBufferLen + Len - 1) < TFT_DISPLAY_BUF_SIZE)
   {
      if( api_fAddOn_Object == TRUE)
      {
         api_fAddOn_Object =  FALSE;
         p_NVRAM_TFT_DISPLAY[api_TxBufferLen]   =  SHIFT_BYTE;
         api_TxBufferLen   ++;
         p_NVRAM_TFT_DISPLAY[api_TxBufferLen]   =  0;
      }

      strcat( (p_NVRAM_TFT_DISPLAY + api_TxBufferLen), api_LocalBuf);
      api_TxBufferLen   += Len;
      p_NVRAM_TFT_DISPLAY[api_TxBufferLen] =  0;
   }
}



/*----------------------------------------------------------------------------*/
static	void						DisplayAPI_SetTextOrientation( void)
/*----------------------------------------------------------------------------*/
{
   switch( api_FontSize)
   {
      case  VGA_FONT_SIZE_2   :
            if( api_ObjectRec.Offset   >= VGA_FONT_2_MAX_COLUMNS  )  break;
            if( api_ObjectRec.Line     >= VGA_FONT_2_MAX_LINES    )  break;
            api_ObjectRec.X   =  (api_ObjectRec.Offset   *  VGA_FONT_2_DELTA_COL);
            api_ObjectRec.Y   =  (api_ObjectRec.Line     *  VGA_FONT_2_DELTA_ROW);
            break;

      case  VGA_FONT_SIZE_3   :
            if( api_ObjectRec.Offset   >= VGA_FONT_3_MAX_COLUMNS  )  break;
            if( api_ObjectRec.Line     >= VGA_FONT_3_MAX_LINES    )  break;
            api_ObjectRec.X   =  (api_ObjectRec.Offset   *  VGA_FONT_3_DELTA_COL);
            api_ObjectRec.Y   =  (api_ObjectRec.Line     *  VGA_FONT_3_DELTA_ROW);
            break;

      case  VGA_FONT_SIZE_4   :
            if( api_ObjectRec.Offset   >= VGA_FONT_4_MAX_COLUMNS  )  break;
            if( api_ObjectRec.Line     >= VGA_FONT_4_MAX_LINES    )  break;
            api_ObjectRec.X   =  (api_ObjectRec.Offset   *  VGA_FONT_4_DELTA_COL);
            api_ObjectRec.Y   =  (api_ObjectRec.Line     *  VGA_FONT_4_DELTA_ROW);
            break;

      case  VGA_FONT_SIZE_5   :
            if( api_ObjectRec.Offset   >= VGA_FONT_5_MAX_COLUMNS  )  break;
            if( api_ObjectRec.Line     >= VGA_FONT_5_MAX_LINES    )  break;
            api_ObjectRec.X   =  (api_ObjectRec.Offset   *  VGA_FONT_5_DELTA_COL);
            api_ObjectRec.Y   =  (api_ObjectRec.Line     *  VGA_FONT_5_DELTA_ROW);
            break;

      case  VGA_FONT_SIZE_6   :
            if( api_ObjectRec.Offset   >= VGA_FONT_6_MAX_COLUMNS  )  break;
            if( api_ObjectRec.Line     >= VGA_FONT_6_MAX_LINES    )  break;
            api_ObjectRec.X   =  (api_ObjectRec.Offset   *  VGA_FONT_6_DELTA_COL);
            api_ObjectRec.Y   =  (api_ObjectRec.Line     *  VGA_FONT_6_DELTA_ROW);
            break;

      case  VGA_FONT_SIZE_7   :
            if( api_ObjectRec.Offset   >= VGA_FONT_7_MAX_COLUMNS  )  break;
            if( api_ObjectRec.Line     >= VGA_FONT_7_MAX_LINES    )  break;
            api_ObjectRec.X   =  (api_ObjectRec.Offset   *  VGA_FONT_7_DELTA_COL);
            api_ObjectRec.Y   =  (api_ObjectRec.Line     *  VGA_FONT_7_DELTA_ROW);
            break;
   }
}




