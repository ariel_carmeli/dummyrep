/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tUart                   api_Uart;
static   bool                    api_fActive;
/*------------------------ Local Function Prototypes -------------------------*/
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                          Debug_Start( byte ComPort)
/*----------------------------------------------------------------------------*/
{
   api_fActive    =  TRUE;

   /*** Communication Setting ***/
   memset( &api_Uart          ,  0, sizeof( api_Uart));

   api_Uart.Com.Port          =  ComPort;
   api_Uart.Com.BaudRate      =  BAUD_RATE_115200;
   api_Uart.Com.DataBits      =  DATA_BITS_8;
   api_Uart.Com.Parity        =  PARITY_NONE;
   api_Uart.Com.StopBits      =  STOP_BITS_1;
   UartDRV_SetPort( &api_Uart);
   /*****************************/
}



/*----------------------------------------------------------------------------*/
void                          Debug_Stop( void)
/*----------------------------------------------------------------------------*/
{
   api_fActive    =  FALSE;
}



/*----------------------------------------------------------------------------*/
int                           putchar( int   Char) // *** DON'T CHANGE THE FUNCTION'S NAME *** (reserved and called by C compiler)
/*----------------------------------------------------------------------------*/
{
   if( api_fActive == FALSE)
      return( 0);

   switch( api_Uart.Com.Port)
   {
      case  COM_1 :
            #if   defined(__IOM16_H) || defined(__IOM32_H)  // ATmega16 or ATmega32
            while( ! (UCSRA & 0x20));
            UDR   =  Char;
            #else
            while( ! (UCSR0A & 0x20));
            UDR0  =  Char;
            #endif
            break;

      case  COM_2 :
            #if   defined(__IOM128_H) || defined(__IO2561_H) || defined(__IO2560_H) // ATmega128 or ATmega2561 or ATmega2560
            while( ! (UCSR1A & 0x20));
            UDR1  =  Char;
            #endif
            break;

      case  COM_3 :
            #if   defined(__IO2561_H) || defined(__IO2560_H)    // ATmega2561 or ATmega2560
            while( ! (UCSR2A & 0x20));
            UDR2  =  Char;
            #endif
            break;

      case  COM_4 :
            #if   defined(__IO2561_H) || defined(__IO2560_H)   // ATmega2561 or ATmega2560
            while( ! (UCSR3A & 0x20));
            UDR3  =  Char;
            #endif
            break;

      case  COM_5 :
            #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // XR Dual-port or Quad-port
            HW_DEF_EXAR_THRA  =  Char;
            #endif
            break;

      case  COM_6 :
            #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // XR Dual-port or Quad-port
            HW_DEF_EXAR_THRB  =  Char;
            #endif
            break;

      case  COM_7 :
            #if   defined(__USES_EXAR_4_PORTS__)  // XR Quad-port
            HW_DEF_EXAR_THRC  =  Char;
            #endif
            break;

      case  COM_8 :
            #if   defined(__USES_EXAR_4_PORTS__)  // XR Quad-port
            HW_DEF_EXAR_THRD  =  Char;
            #endif
            break;
   }

   return( Char);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

