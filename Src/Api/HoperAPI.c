/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  DEFAULT_TIMER_ACTIVATION      5  // sec
#define  DEFAULT_TIMER_DISPENSING      50 // msec
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tHopperInfo          api_Info[MAX_HOPPERS];
static   tInput               api_SenseOut[MAX_HOPPERS];
static   tTimerSec            api_TimerSec[MAX_HOPPERS];
static   tTimerMsec           api_TimerMsec[MAX_HOPPERS];
static   usint                api_CoinsToDispense[MAX_HOPPERS];
static   usint                api_TimeoutValue;
static   usint                api_TimeoutValue_Dispensing;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						HopperAPI_HandleTimer( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		HopperAPI_Init( void)
/*----------------------------------------------------------------------------*/
{
tHopper     Hopper;


   api_TimeoutValue              =  DEFAULT_TIMER_ACTIVATION;  // default value of pay-out timeout
   api_TimeoutValue_Dispensing   =  DEFAULT_TIMER_DISPENSING;

   api_SenseOut[HOPPER_A]  =  INPUT_U18_5;
   api_SenseOut[HOPPER_B]  =  INPUT_U18_7;
   api_SenseOut[HOPPER_C]  =  INPUT_U18_3;
   api_SenseOut[HOPPER_D]  =  INPUT_U18_1;

   HopperDRV_Init();

   for( Hopper = (tHopper)0;  Hopper < MAX_HOPPERS; Hopper ++)
   {
      HopperDRV_SetMode( Hopper, OFF); // Ensure all Hoppers are turned OFF
      api_Info[Hopper].fEmpty =  TRUE;
   }
}



/*----------------------------------------------------------------------------*/
void                    		HopperAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
static      bool     fStop[MAX_HOPPERS];
tHopper     Hopper;


   HopperAPI_HandleTimer();

   for( Hopper = (tHopper)0;  Hopper < MAX_HOPPERS; Hopper ++)
   {
      api_Info[Hopper].fLowLevel =  HopperDRV_GetStatus_LowLevel( Hopper);

      if( api_Info[Hopper].fLowLevel == FALSE)
         api_Info[Hopper].fEmpty =  FALSE;

      if( InOutAPI_GetStatus( api_SenseOut[Hopper], FALSE) == IO_STATUS_CLOSED)
      {
         if( fStop[Hopper] == FALSE)
            api_TimerSec[Hopper]    =  Task_Timer_SetTimeSec( api_TimeoutValue);

         api_Info[Hopper].CoinsDispensed  ++;

         if( (api_Info[Hopper].CoinsDispensed >= api_CoinsToDispense[Hopper]) && (fStop[Hopper] == FALSE))
         {
            fStop[Hopper]           =  TRUE;
            api_TimerSec[Hopper]    =  0;
            api_TimerMsec[Hopper]   =  Task_Timer_SetTimeMsec( api_TimeoutValue_Dispensing); // Keep working for a short-while, to dispense the last coin
         }
      }

      if( (api_TimerSec[Hopper] > 0) || (api_TimerMsec[Hopper] > 0))
      {
         HopperDRV_SetMode( Hopper, ON);
         api_Info[Hopper].fBusy  =  TRUE;
      }
      else
      {
         if( api_Info[Hopper].fBusy == TRUE)
            api_Info[Hopper].fEmpty =  (api_Info[Hopper].CoinsDispensed < api_CoinsToDispense[Hopper]);

         HopperDRV_SetMode( Hopper, OFF);
         fStop[Hopper]  =  FALSE;
         api_Info[Hopper].fBusy  =  FALSE;
      }
   }
}



/*----------------------------------------------------------------------------*/
tHopperInfo                  *HopperAPI_GetInfo( void)
/*----------------------------------------------------------------------------*/
{
   return( api_Info);
}



/*----------------------------------------------------------------------------*/
void                    		HopperAPI_Activate( tHopper   Hopper, usint Count)
/*----------------------------------------------------------------------------*/
{
   if( Hopper >= MAX_HOPPERS)
      return;

   api_CoinsToDispense[Hopper]      =  Count;
   api_Info[Hopper].CoinsDispensed  =  0;

   if( Count == 0)
      return;

   if( api_TimeoutValue == 0) api_TimeoutValue =  DEFAULT_TIMER_ACTIVATION;   // Minimum dispensing timeout
   if( api_TimeoutValue > 60) api_TimeoutValue =  60; // Maximum dispensing timeout of 60sec.
   api_TimerSec[Hopper] =  Task_Timer_SetTimeSec( api_TimeoutValue);

   api_Info[Hopper].fBusy  =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                    		HopperAPI_SetTimeoutValue( usint Seconds)
/*----------------------------------------------------------------------------*/
{
   api_TimeoutValue  =  (Seconds > 0) ? Seconds : DEFAULT_TIMER_ACTIVATION;
}



/*----------------------------------------------------------------------------*/
void                    		HopperAPI_SetTimeoutValue_Dispensing( usint Msec)
/*----------------------------------------------------------------------------*/
{
   api_TimeoutValue_Dispensing   =  ((Msec > 0) && (Msec <= 100)) ? Msec : DEFAULT_TIMER_DISPENSING;
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						HopperAPI_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;
tHopper              Hopper;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      for( Hopper = (tHopper)0; Hopper < MAX_HOPPERS; Hopper ++)
      {
         if( api_TimerSec[Hopper]   <  Gap)  api_TimerSec[Hopper]    =  0; else  api_TimerSec[Hopper]    -= Gap;
         if( api_TimerMsec[Hopper]  <  Gap)  api_TimerMsec[Hopper]   =  0; else  api_TimerMsec[Hopper]   -= Gap;
      }
   }
}





