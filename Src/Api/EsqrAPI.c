/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  ESQR_MODE_IDLE                   0
#define  ESQR_MODE_READ                   1
#define  ESQR_MODE_WRITE                  2

#define  ESQR_BASE_ADDRESS                (ulong)(0)

/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
CONST	   static   ulong       Addresses[ESQR_MAX_TYPES]  =
                              {
                                 0
                              };

CONST	   static   usint       StructSize[ESQR_MAX_TYPES] =
                              {
                                 0
                              };

CONST	   static   usint       MaxRecords[ESQR_MAX_TYPES]  =
                              {  // Value MUST NOT be equal to Zero (0)
                                 1
                              };

static   ulong                api_Size;
static   ulong                api_Count;
static   byte                 *api_pData;
static   ulong                api_Address;
static   byte                 api_Mode;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						EsqrAPI_SetAddress( tEsqr_DataType   Type, usint Idx);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		EsqrAPI_Init( void)
/*----------------------------------------------------------------------------*/
{
   api_Size       =  0;
   api_Count      =  0;
   api_pData      =  NULL;
   api_Address    =  0;
   api_Mode       =  ESQR_MODE_IDLE;

	EsqrDRV_Init();
}



/*----------------------------------------------------------------------------*/
void                    		EsqrAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
   _WDR();

   if( api_Count >= api_Size)
      api_Mode =  ESQR_MODE_IDLE;

   switch( api_Mode)
   {
      case  ESQR_MODE_IDLE:
            break;

      case  ESQR_MODE_READ:
            api_pData[api_Count ++] =  EsqrDRV_Read( api_Address ++);
            break;

      case  ESQR_MODE_WRITE:
            EsqrDRV_Write( api_Address ++, api_pData[api_Count ++]);
            break;

      default:
            break;
   }
}



/*----------------------------------------------------------------------------*/
bool                    		EsqrAPI_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
	return( (api_Mode == ESQR_MODE_IDLE) ? FALSE : TRUE);
}



/*----------------------------------------------------------------------------*/
void                    		EsqrAPI_Read( void  *Buffer, usint Size, tEsqr_DataType   Type, usint Idx, bool  fBlockingMode)
/*----------------------------------------------------------------------------*/
{
   if( (Buffer == NULL) || (Size == 0) || (api_Mode != ESQR_MODE_IDLE))
      return;

   api_Size       =  Size;
   api_Count      =  0;
   api_pData      =  (byte *)Buffer;
   api_Mode       =  ESQR_MODE_READ;

   EsqrAPI_SetAddress( Type, Idx);

   if( fBlockingMode == TRUE)
   {
      for(;;)
      {
         api_pData[api_Count ++] =  EsqrDRV_Read( api_Address ++);
         if( api_Count >= api_Size)
            break;

         Task_Hardware_Main( TRUE);
      }

      api_Mode =  ESQR_MODE_IDLE;
   }
}



/*----------------------------------------------------------------------------*/
void                    		EsqrAPI_Read_Address( void  *Buffer, usint Size, ulong   Address, bool  fBlockingMode)
/*----------------------------------------------------------------------------*/
{
   if( (Buffer == NULL) || (Size == 0) || (api_Mode != ESQR_MODE_IDLE) || ((Address + Size) > HW_DEF_ESQR_SIZE))
      return;

   api_Size       =  Size;
   api_Count      =  0;
   api_pData      =  (byte *)Buffer;
   api_Mode       =  ESQR_MODE_READ;
   api_Address    =  Address;

   if( fBlockingMode == TRUE)
   {
      for(;;)
      {
         api_pData[api_Count ++] =  EsqrDRV_Read( api_Address ++);
         if( api_Count >= api_Size)
            break;

         Task_Hardware_Main( TRUE);
      }

      api_Mode =  ESQR_MODE_IDLE;
   }
}



/*----------------------------------------------------------------------------*/
void                    		EsqrAPI_Write( void  *Buffer, usint Size, tEsqr_DataType  Type, usint Idx, bool  fBlockingMode)
/*----------------------------------------------------------------------------*/
{
   if( (Buffer == NULL) || (Size == 0) || (api_Mode != ESQR_MODE_IDLE))
      return;

   api_Size       =  Size;
   api_Count      =  0;
   api_pData      =  (byte *)Buffer;
   api_Mode       =  ESQR_MODE_WRITE;

   EsqrAPI_SetAddress( Type, Idx);

   if( fBlockingMode == TRUE)
   {
      for(;;)
      {
         EsqrDRV_Write( api_Address ++, api_pData[api_Count ++]);
         if( api_Count >= api_Size)
            break;

         Task_Hardware_Main( TRUE);
      }

      api_Mode =  ESQR_MODE_IDLE;
   }
}



/*----------------------------------------------------------------------------*/
void                          EsqrAPI_Write_Address( void  *Buffer, usint Size, ulong  Address, bool  fBlockingMode)
/*----------------------------------------------------------------------------*/
{
   if( (Buffer == NULL) || (Size == 0) || (api_Mode != ESQR_MODE_IDLE) || ((Address + Size) > HW_DEF_ESQR_SIZE))
      return;

   api_Size       =  Size;
   api_Count      =  0;
   api_pData      =  (byte *)Buffer;
   api_Mode       =  ESQR_MODE_WRITE;
   api_Address    =  Address;

   if( fBlockingMode == TRUE)
   {
      for(;;)
      {
         EsqrDRV_Write( api_Address ++, api_pData[api_Count ++]);
         if( api_Count >= api_Size)
            break;

         Task_Hardware_Main( TRUE);
      }

      api_Mode =  ESQR_MODE_IDLE;
   }
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						EsqrAPI_SetAddress( tEsqr_DataType   Type, usint Idx)
/*----------------------------------------------------------------------------*/
{
ulong    Offset;


   Offset      =  Idx;
   Offset      *= StructSize[Type];
   api_Address =  Addresses[Type] + Offset;
}

