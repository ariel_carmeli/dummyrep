/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  ORIGINATOR_MASTER          ';'
#define  ORIGINATOR_MASTER_VOID_CS  '#'
#define  ORIGINATOR_SLAVE           ':'

#define  END_PACKET                 CR

#define  RX_TIMEOUT                 300   // in msec
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   STATE_ORIGINATOR           ,  // ';' = Master      '' = Master (w/o Checksum)      ':' = Slave
   STATE_COMMAND              ,
   STATE_DATA                 ,
   STATE_CHECKSUM             ,
   STATE_END_PACKET           ,  // CR
   /******************/
   STATE_LAST
}  tRxTxState;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tUart                api_Uart;
static   tServerPacket        api_TxPacket;
static   tServerPacket        api_RxPacket;
static   tTimerMsec           api_TimerMsec_Rx;
static   byte                 api_RxBuffer[SERVER_MAX_LEN_BUFFER];
static   usint                api_RxBufferIdx;
static   tRxTxState           api_TxState;
static   tRxTxState           api_RxState;
static   bool                 api_fRxNewPacket;
static   bool                 api_fBusy_Tx;
static   bool                 api_fBusy_Rx;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						ServerAPI_HandleTimer( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		ServerAPI_Init( byte ComPort)
/*----------------------------------------------------------------------------*/
{
   api_fRxNewPacket           =  FALSE;

   /*** Communication Setting ***/
   api_Uart.Com.Port          =  ComPort;
   api_Uart.Com.BaudRate      =  BAUD_RATE_19200;
   api_Uart.Com.DataBits      =  DATA_BITS_8;
   api_Uart.Com.Parity        =  PARITY_NONE;
   api_Uart.Com.StopBits      =  STOP_BITS_1;
   api_Uart.Com.RS485_Address =  0;
   api_Uart.TxFunc            =  ServerAPI_Tx_ISR;
   api_Uart.RxFunc            =  ServerAPI_Rx_ISR;
   UartDRV_SetPort( &api_Uart);
   /*****************************/
}



/*----------------------------------------------------------------------------*/
void                    		ServerAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
   ServerAPI_HandleTimer();
}



/*----------------------------------------------------------------------------*/
void                    		ServerAPI_AssignPort( byte ComPort)
/*----------------------------------------------------------------------------*/
{
   UartDRV_SetPort( &api_Uart);
}



/*----------------------------------------------------------------------------*/
bool                    		ServerAPI_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
   api_fBusy_Rx   =  (api_TimerMsec_Rx > 0);

   return( api_fBusy_Tx | api_fBusy_Rx);
}



/*----------------------------------------------------------------------------*/
void                          ServerAPI_SendData( tServerPacket   *pTxPacket)
/*----------------------------------------------------------------------------*/
{
   if( pTxPacket == NULL)
      return;

   api_TxPacket   =  *pTxPacket;
   api_TxState    =  STATE_ORIGINATOR;

   ServerAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
bool                    		ServerAPI_IsRxNewPacket( void)
/*----------------------------------------------------------------------------*/
{
   if( api_fRxNewPacket == TRUE)
   {
      api_fRxNewPacket  =  FALSE;
      return( TRUE);
   }
   else
      return( FALSE);
}



/*----------------------------------------------------------------------------*/
tServerPacket                   *ServerAPI_GetData( void)
/*----------------------------------------------------------------------------*/
{
   api_RxPacket.pBuffer =  api_RxBuffer;
   return( &api_RxPacket);
}



/*----------------------------------------------------------------------------*/
void                    		ServerAPI_Tx_ISR( void)
/*----------------------------------------------------------------------------*/
{
static   usint    BufIdx;
static   byte     Checksum;
static   bool     fChecksumEnabled;
static   bool     fHighNibble;
static   char     HexString[2];
byte              Data;
bool              fTxData;


   Data     =  0;
   fTxData  =  TRUE;

   switch( api_TxState)
   {
      case  STATE_ORIGINATOR  :
            BufIdx            =  0;
            Checksum          =  0;
            fTxData           =  TRUE;
            fHighNibble       =  TRUE;
            fChecksumEnabled  =  TRUE;
            Data              =  ORIGINATOR_SLAVE;
            api_TxState       =  STATE_COMMAND;
            break;

      case  STATE_COMMAND     :
            Data              =  api_TxPacket.Command;
            api_TxState       =  (api_TxPacket.BufSize > 0) ? STATE_DATA : STATE_CHECKSUM;
            break;

      case  STATE_DATA        :
            Data  =  api_TxPacket.pBuffer[BufIdx ++];

            if( (BufIdx >= api_TxPacket.BufSize) || (BufIdx >= SERVER_MAX_LEN_BUFFER))
               api_TxState =  STATE_CHECKSUM;
            break;

      case  STATE_CHECKSUM    :
            fChecksumEnabled  =  FALSE;

            if( fHighNibble == TRUE)
            {
               fHighNibble    =  FALSE;
               Util_ByteToHexString( Checksum, HexString);
               Data           =  HexString[0];
            }
            else
            {
               fHighNibble    =  TRUE;
               Data           =  HexString[1];
               api_TxState    =  STATE_END_PACKET;
            }
            break;

      case  STATE_END_PACKET  :
            Data              =  END_PACKET;
            api_TxState       =  STATE_LAST;
            break;

      default                 :
      case  STATE_LAST        :
            fTxData           =  FALSE;
            api_TxState       =  STATE_ORIGINATOR;
            break;
   }

   if( fChecksumEnabled == TRUE)
      Checksum += Data;

   if( fTxData == TRUE)
   {
      api_fBusy_Tx   =  TRUE;
      api_Uart.Uart_TxByte( Data);
   }
   else
   {
      api_fBusy_Tx   =  FALSE;
      api_Uart.Uart_TxEnd();
   }
}



/*----------------------------------------------------------------------------*/
void                    		ServerAPI_Rx_ISR( void)
/*----------------------------------------------------------------------------*/
{
static   usint    BufIdx;
static   byte     RcvByte;
static   byte     Checksum;
static   bool     fOriginator =  FALSE;
static   bool     fHighNibble;
static   bool     fVoidCS;
static   char     HexString[2];
byte              Data;
bool              fInsertByte;


   Data     =  api_Uart.Uart_RxByte();

   RcvByte  =  0;

   if( api_fRxNewPacket == TRUE) // if last packet was not handled yet
      return;

   api_TimerMsec_Rx  =  Task_Timer_SetTimeMsec( 20);

   if( Data == ORIGINATOR_MASTER)         api_RxState =  STATE_ORIGINATOR;
   if( Data == ORIGINATOR_MASTER_VOID_CS) api_RxState =  STATE_ORIGINATOR;
   if( Data == END_PACKET)                api_RxState =  STATE_END_PACKET;

   switch( api_RxState)
   {
      case  STATE_ORIGINATOR  :
            if( (Data != ORIGINATOR_MASTER) && (Data != ORIGINATOR_MASTER_VOID_CS))
               break;

            fVoidCS  =  (Data == ORIGINATOR_MASTER_VOID_CS);

            api_RxPacket.fFromMaster               =  TRUE;
            api_RxPacket.pBuffer                   =  api_RxBuffer;
            api_RxPacket.BufSize                   =  0;
            api_RxPacket.Idx_CtrlChar_Underscore   =  0;

            BufIdx            =  0;
            Checksum          =  0;
            fOriginator       =  TRUE;
            fHighNibble       =  TRUE;
            api_RxState       =  STATE_COMMAND;
            break;

      case  STATE_COMMAND     :
            api_RxPacket.Command       =  Data;
            api_RxState       =  STATE_DATA;
            break;

      case  STATE_DATA        :
            fInsertByte          =  FALSE;

            if( ((Data >= '0') && (Data <= '9')) || ((Data >= 'A') && (Data <= 'F')))
            {
               if( fHighNibble == TRUE)
               {
                  fHighNibble    =  FALSE;
                  HexString[0]   =  Data;
               }
               else
               {
                  fHighNibble    =  TRUE;
                  HexString[1]   =  Data;
                  RcvByte        =  Util_HexStringToByte( HexString);
                  fInsertByte    =  TRUE;
               }
            }
            else
            {
               if( Data == SERVER_DELIMITER)  // underscore (_) is a delimiter between data and (machine number and checksum)
                  api_RxPacket.Idx_CtrlChar_Underscore   =  BufIdx;
               RcvByte           =  Data;
               fInsertByte       =  TRUE;
            }

            if( fInsertByte == TRUE)
            {
               if( BufIdx < sizeof( api_RxBuffer))
               {
                  api_RxPacket.pBuffer[BufIdx]  =  RcvByte;
                  api_RxPacket.BufSize ++;
                  BufIdx               ++;
               }
            }
            break;

      default                 :
      case  STATE_END_PACKET  :
      case  STATE_LAST        :
            if( fOriginator == TRUE)
            {
               api_fRxNewPacket  =  FALSE;

               if( fVoidCS == TRUE)
                  api_fRxNewPacket  =  TRUE;
               else
               {
                  if( api_RxPacket.BufSize > 0)
                  {
                     Checksum -= HexString[0];  // last 2 ASCII chars are checksum
                     Checksum -= HexString[1];
                     api_fRxNewPacket  =  (Checksum == Util_HexStringToByte( HexString));
                     api_RxPacket.BufSize --;   // Reduce buffer size by 1 byte (checksum)
                  }
               }
            }

            fOriginator =  FALSE;
            api_RxState =  STATE_ORIGINATOR;
            break;
   }

   Checksum += Data;
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						ServerAPI_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( api_TimerMsec_Rx <  Gap)  api_TimerMsec_Rx  =  0; else  api_TimerMsec_Rx  -= Gap;
   }
}



