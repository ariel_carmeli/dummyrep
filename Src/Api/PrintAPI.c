/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  PRINTER_COMMAND_FONT             0x21
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   usint                api_TxBufferSize;
// static   usint                api_TxBufferIdx;
static   tTimerSec            api_TimerSec_Inactive;
static   byte                 api_TxBuffer[PRINT_MAX_BUF_SIZE + 2];
static   byte                 api_RxBuffer[PRINT_MAX_BUF_SIZE + 2];
static   byte                 api_RxStatus;
static   byte                 api_RxIdx;
static   byte                 api_Font;
static   byte                 api_Width;
static   byte                 api_Height;
static   byte                 api_CharsPerLine;
static   bool                 api_fTx;
static   bool                 api_fBusy;
static   bool                 api_fMgcMode;
static   bool                 api_fHasNewTrack2;
static   bool                 api_fUsingCOM2;
static   bool                 api_fVoidHebrew;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						PrinterAPI_HandleTimer( void);
static   byte                 PrinterAPI_Align( byte   Length, byte   Mode);
static   void                 PrinterAPI_Hebrew( byte  *pStr, byte  Size);
static   void                 PrinterAPI_Rx_Track2( byte  Data);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_Init( void)
/*----------------------------------------------------------------------------*/
{
   api_TxBufferSize        =  0;
   // api_TxBufferIdx         =  0;
   api_TimerSec_Inactive   =  0;
   memset( api_TxBuffer    ,  0, sizeof( api_TxBuffer));
   memset( api_RxBuffer    ,  0, sizeof( api_RxBuffer));
   api_RxStatus            =  0;
   api_RxIdx               =  0;
   api_Font                =  PRINT_FONT_A;
   api_Width               =  PRINT_HEIGHT_NORMAL;
   api_Height              =  PRINT_WIDTH_NORMAL;
   api_fTx                 =  FALSE;
   api_fBusy               =  TRUE;
   api_fMgcMode            =  ENABLED;
   api_fHasNewTrack2       =  FALSE;
   api_fUsingCOM2          =  FALSE;

   //PrinterAPI_Cutter( PRINT_CUTTER_FULL);
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
   api_fVoidHebrew   =  FALSE;

   PrinterAPI_HandleTimer();
}



/*----------------------------------------------------------------------------*/
bool                          PrinterAPI_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
   //if( api_TimerSec_Inactive == 0)
   //   api_RxStatus   =  PRINTER_STATUS_ERROR;

   api_fBusy  =  ((BIT_IS_SET( api_RxStatus, PRINTER_STATUS_PAPER_OUT)) || (api_fTx == TRUE)) ? TRUE : FALSE;
   return( api_fBusy);
}



/*----------------------------------------------------------------------------*/
byte                          PrinterAPI_GetStatus( void)
/*----------------------------------------------------------------------------*/
{
   return( api_RxStatus);
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_VoidHebrew( void)
/*----------------------------------------------------------------------------*/
{
   api_fVoidHebrew   =  TRUE;
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_Print( char  *pStr, byte Alignment)
/*----------------------------------------------------------------------------*/
{
usint    Size;
byte     Spaces;
bool     fLF;
char     *p;


   if( pStr == NULL)
      return;

   Size  =  strlen( pStr);
   p     =  strchr( pStr, LF);
   fLF   =  FALSE;
   if( (Size > 0) && (p != NULL))
      fLF   =  TRUE;

   if( Size == 0)
      return;

   if( Size > PRINT_MAX_BUF_SIZE)
      Size  =  PRINT_MAX_BUF_SIZE;

   memset( api_TxBuffer, 0, sizeof( api_TxBuffer));

   Spaces   =  PrinterAPI_Align( (fLF == TRUE) ? (Size - 1) : Size, Alignment);
   if( Spaces > 0)
      memset( api_TxBuffer, SPACE, Spaces);

   memcpy( &api_TxBuffer[Spaces], pStr, Size);
   PrinterAPI_Hebrew( &api_TxBuffer[Spaces], Size);

   api_TxBufferSize  =  (Spaces + Size);

   // api_TxBufferIdx   =  0;
   PrinterAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_FullLine_SameChar( char  Char)
/*----------------------------------------------------------------------------*/
{
byte     Counter;
bool     fPrintChar;


   memset( api_TxBuffer, 0, sizeof( api_TxBuffer));

   // prints 1 char + 1 space periodically
   fPrintChar  =  0;
   for( Counter = 0; Counter < api_CharsPerLine; Counter ++)
   {
      switch( Char)
      {
         case  '=':  // =-=-=-=-=-=-=-=-=-=-=-
               api_TxBuffer[Counter]   =  (fPrintChar == 0) ? Char : '-';
               fPrintChar  =  ~fPrintChar;   // toggle
               break;

         case  '-':  // - - - - - - - - - - -
               api_TxBuffer[Counter]   =  (fPrintChar == 0) ? Char : ' ';
               fPrintChar  =  ~fPrintChar;   // toggle
               break;

         default:
               api_TxBuffer[Counter]   =  Char;
               break;
      }
   }

   strcat( (char *)api_TxBuffer, "\n");

   api_TxBufferSize  =  strlen( (char *)api_TxBuffer);
   // api_TxBufferIdx   =  0;
   PrinterAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_LineFeed( byte  Count)
/*----------------------------------------------------------------------------*/
{
   if( Count == 0)
      return;

   if( Count > 10)
      Count =  10;

   memset( api_TxBuffer, LF, Count);
   api_TxBufferSize  =  Count;
   // api_TxBufferIdx   =  0;
   PrinterAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_SetFont( byte   Font, byte  Height, byte   Width)
/*----------------------------------------------------------------------------*/
{
byte     Idx;


   api_Font    =  0;
   api_Width   =  0;
   api_Height  =  0;

   switch( Font)
   {
      case  PRINT_FONT_B:
            SET_BIT( api_Font, PRINT_FONT_B);
            api_CharsPerLine  =  CHARS_PER_LINE_FONT_B_NORMAL;
            break;

      case  PRINT_FONT_A:
      default:
            SET_BIT( api_Font, PRINT_FONT_A);
            api_CharsPerLine  =  CHARS_PER_LINE_FONT_A_NORMAL;
            break;
   }

   switch( Height)
   {
      case  PRINT_HEIGHT_DOUBLE: SET_BIT( api_Width, PRINT_HEIGHT_DOUBLE); break;
      case  PRINT_HEIGHT_NORMAL:
      default:                   SET_BIT( api_Width, PRINT_HEIGHT_NORMAL);   break;
   }

   switch( Width)
   {
      case  PRINT_WIDTH_DOUBLE:
            SET_BIT( api_Height, PRINT_WIDTH_DOUBLE);
            api_CharsPerLine  /=  2;
            break;

      case  PRINT_WIDTH_NORMAL:
      default:                   SET_BIT( api_Height, PRINT_WIDTH_NORMAL); break;
   }

   Idx   =  0;
   api_TxBuffer[Idx  ++]   =  ESC;
   api_TxBuffer[Idx  ++]   =  PRINTER_COMMAND_FONT;
   api_TxBuffer[Idx  ++]   =  ((api_Font | api_Height | api_Width) | 0x80);   // to ensure value is not 0x00 (NULL terminating)
   api_TxBufferSize        =  Idx;
   // api_TxBufferIdx         =  0;
   PrinterAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_Cutter( byte Mode)
/*----------------------------------------------------------------------------*/
{
byte     Properties;
byte     Idx;


   Properties  =  0;

   switch( Mode)
   {
      case  PRINT_CUTTER_HALF:   Properties  =  PRINT_CUTTER_HALF;   break;
      case  PRINT_CUTTER_FULL:   Properties  =  PRINT_CUTTER_FULL;   break;
   }

   Idx   =  0;
   for( Idx = 0; Idx < 7; Idx ++)
      api_TxBuffer[Idx]    =  LF;
   api_TxBuffer[Idx  ++]   =  ESC;
   api_TxBuffer[Idx  ++]   =  Properties;
   api_TxBuffer[Idx  ++]   =  LF;
   api_TxBufferSize        =  Idx;
   // api_TxBufferIdx         =  0;
   PrinterAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_SetMgcMode( bool Mode)
/*----------------------------------------------------------------------------*/
{
   api_fMgcMode  =  Mode;
}



/*----------------------------------------------------------------------------*/
bool                          PrinterAPI_IsNewTrack2( void)
/*----------------------------------------------------------------------------*/
{
   if( api_fHasNewTrack2 == TRUE)
   {
      api_fHasNewTrack2 =  FALSE;
      return( TRUE);
   }
   else
      return( FALSE);
}



/*----------------------------------------------------------------------------*/
char                         *PrinterAPI_GetTrack2( void)
/*----------------------------------------------------------------------------*/
{
   return( (char *)api_RxBuffer);
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_ClearTrack2( void)
/*----------------------------------------------------------------------------*/
{
   memset( api_RxBuffer , 0, sizeof( api_RxBuffer));
   api_fHasNewTrack2    =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_COM2_Start( void)
/*----------------------------------------------------------------------------*/
{
byte  Idx   =  0;


   memset( api_TxBuffer, 0, sizeof( api_TxBuffer));
   api_TxBuffer[Idx  ++]   =  ESC;  // ESC 'x' ETX
   api_TxBuffer[Idx  ++]   =  'x';
   api_TxBuffer[Idx  ++]   =  ETX;  // ETX - termination character
   api_TxBufferSize        =  Idx;
   // api_TxBufferIdx         =  0;
   api_fUsingCOM2          =  TRUE;
   api_TimerSec_Inactive   =  Task_Timer_SetTimeSec( 20);
   PrinterAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_COM2_Stop( void)
/*----------------------------------------------------------------------------*/
{
byte  Idx   =  0;


   memset( api_TxBuffer, 0, sizeof( api_TxBuffer));
   api_TxBuffer[Idx  ++]   =  ETX;  // ETX - termination character
   api_TxBufferSize        =  Idx;
   // api_TxBufferIdx         =  0;
   PrinterAPI_Tx_ISR();
   api_fUsingCOM2          =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_COM2_Send( byte *pBuf, byte BufLen)
/*----------------------------------------------------------------------------*/
{
byte  Idx   =  0;


   if( (pBuf == NULL) || (BufLen == 0))
      return;

   for( Idx = 0; ((Idx < BufLen) && (Idx < sizeof( api_TxBuffer))); Idx ++)
      api_TxBuffer[Idx] =  pBuf[Idx];

   api_TxBufferSize        =  Idx;
   // api_TxBufferIdx         =  0;
   api_fUsingCOM2          =  TRUE;
   api_TimerSec_Inactive   =  Task_Timer_SetTimeSec( 20);
   PrinterAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
bool                          PrinterAPI_COM2_IsUsing( void)
/*----------------------------------------------------------------------------*/
{
   return( api_fUsingCOM2);
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_Tx_ISR( void)
/*----------------------------------------------------------------------------*/
{
   Task_Application_Prn_InsertText( api_TxBuffer, api_TxBufferSize);
   api_fTx  =  FALSE;
/*
static   byte     Data;


   if( api_TxBufferIdx < api_TxBufferSize)
   {
      Data     =  api_TxBuffer[api_TxBufferIdx ++];
      api_fTx  =  TRUE;
      //Uart_TxFunc( Data);
   }
   else
   {
      //Uart_TxEndFunc();
      api_TxBufferSize  =  0;
      api_fTx  =  FALSE;
   }
*/
}



/*----------------------------------------------------------------------------*/
void                          PrinterAPI_Rx_ISR( void)
/*----------------------------------------------------------------------------*/
{
static   byte     Data;
static   bool     fRxTrack2      =  FALSE;


   Data  =  0;
   //Data  =  Uart_RxFunc();

   if( api_fUsingCOM2 == TRUE)
      fRxTrack2   =  FALSE;
   else
   {
      if( (Data == TRACK_2_START_SENTINEL) && (api_fMgcMode == ENABLED))
         fRxTrack2      =  TRUE;

      fRxTrack2      =  FALSE;
      api_RxStatus   =  Data;
      api_TimerSec_Inactive   =  Task_Timer_SetTimeSec( 2);
   }

   if( fRxTrack2 == TRUE)
   {
      PrinterAPI_Rx_Track2( Data);

      if( Data == TRACK_2_END_SENTINEL)
         fRxTrack2   =  FALSE;
   }

   if( api_fUsingCOM2 == TRUE)
      Task_Printer_COM2_HandleRx( Data);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						PrinterAPI_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
   	OldTimer		=	NewTimer;

      if( api_TimerSec_Inactive > 0)   api_TimerSec_Inactive   --;
   }
}



/*----------------------------------------------------------------------------*/
static   byte                 PrinterAPI_Align( byte   Length, byte   Mode)
/*----------------------------------------------------------------------------*/
{
byte     Spaces;


   if( (Length == 0) || (Length > api_CharsPerLine))
      return( 0);

   Spaces   =  0;

   switch( Mode)
   {
      case  PRINT_ALIGN_LEFT:
            break;

      case  PRINT_ALIGN_CENTER:
            Spaces   =  ((api_CharsPerLine - Length) / 2);
            break;

      case  PRINT_ALIGN_RIGHT:
            Spaces   =  (api_CharsPerLine - Length);
            break;
   }

   return( Spaces);
}



/*----------------------------------------------------------------------------*/
static   void                 PrinterAPI_Hebrew( byte  *pStr, byte  Size)
/*----------------------------------------------------------------------------*/
{
byte     Tail_Idx;
byte     Head_Idx;
byte     tmpChar;
bool     fHebrew;


   if( (Size == 0) || (pStr == NULL))
      return;

   fHebrew  =  FALSE;

   // check if string contains Hebrew letters...
   // anf if so - adjust letters from PC format to local format
   for( Tail_Idx = 0; Tail_Idx < Size; Tail_Idx ++)
   {
      if( (pStr[Tail_Idx] >= 0xE0 /*'�'*/) && (pStr[Tail_Idx] <= 0xFA /*'�'*/))
      {
         pStr[Tail_Idx] -= HW_DEF_PRN_HEBREW_OFFSET_IDE; // Hebrew Alphabet starts from address 0x80 (0xE0 - 0x80 = 0x60)
         fHebrew  =  TRUE;
      }

      if( (pStr[Tail_Idx] >= 0xA0 /*'�'*/) && (pStr[Tail_Idx] <= 0xBA /*'�'*/))
      {
         pStr[Tail_Idx] -= HW_DEF_PRN_HEBREW_OFFSET_LCD; // Hebrew Alphabet starts from address 0x80 (0xE0 - 0x80 = 0x60)
         fHebrew  =  TRUE;
      }
   }

   // ... if not - quit function
   if( (fHebrew == FALSE) || (api_fVoidHebrew == TRUE))
      return;

   // ... if yes - filter ending CR/LF
   for( Head_Idx = (Size - 1); Head_Idx > 0; Head_Idx --)
   {
      if( (pStr[Head_Idx] != CR) && (pStr[Head_Idx] != LF))
         break;
   }

   // Find first occurence of Hebrew letter (new values)
   for( Tail_Idx = 0; Tail_Idx < Size; Tail_Idx ++)
      if( (pStr[Tail_Idx] >= 0x80 /*'�'*/) && (pStr[Tail_Idx] <= 0x9A /*'�'*/))
         break;

   // swap string (Head to Tail)
   while( Tail_Idx < Head_Idx)
   {
      tmpChar        =  pStr[Head_Idx];
      pStr[Head_Idx] =  pStr[Tail_Idx];
      pStr[Tail_Idx] =  tmpChar;

      Tail_Idx ++;
      Head_Idx --;
   }
}



/*----------------------------------------------------------------------------*/
static   void                 PrinterAPI_Rx_Track2( byte  Data)
/*----------------------------------------------------------------------------*/
{
static   byte  State =  0;


   if( Data == TRACK_2_START_SENTINEL  )  State =  0;
   if( Data == TRACK_2_END_SENTINEL    )  State =  2; // that's not a bug. State 1 is reserved for Track 2 data

   switch( State)
   {
      case  0: // ';'
            api_RxIdx         =  0;
            api_RxBuffer[0]   =  0;
            api_fHasNewTrack2 =  FALSE;
            State             ++;
            break;

      case  1: // Track 2
            if( api_RxIdx < (sizeof( api_RxBuffer) - 1))
            {
               api_RxBuffer[api_RxIdx ++] =  Data;
               api_RxBuffer[api_RxIdx]    =  0;
            }
            break;

      case  2: // '?'
            api_fHasNewTrack2 =  TRUE;
            State             ++;
            break;

      case  3:
            // do nothing
            break;
   }
}

