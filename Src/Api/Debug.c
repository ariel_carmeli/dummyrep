/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
#define API_RXBUFF_SIZE         2
static   tUart                  api_Uart;
static   bool                   api_fActive;
//static   byte                   api_RxStatus;
static   byte                   api_RxBuffer[API_RXBUFF_SIZE];
static   byte                   api_IndexRxBuffer;
static   tTimerSec              api_TimerSec_Inactive;
/*------------------------ Local Function Prototypes -------------------------*/
int                           putchar( int   Char);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

#if 1
/*----------------------------------------------------------------------------*/
void                          Debug_Main( void)
/*----------------------------------------------------------------------------*/
{
   #define DBG_BUF_SIZE         32
   byte         debug_msg_buffer[DBG_BUF_SIZE];
   usint        debug_usint[2];
   ulong  date_time;
   tParams_A_Rec        Param_A_Rec;
   
   //********************* COM 5 onl (EXAR Port C) **********************
   //if (api_RxStatus > 0)
   if (api_IndexRxBuffer > 0)
   {
      api_IndexRxBuffer = 0;
      switch ( api_RxBuffer[0] )
      {
         // Set of TimerSys debugs
         case '1':

            break;

         case '2':

            break;

         case '3':

            break;

         case '4':

            break;

         case '5':

            break;

         case '6':

            break;
            
         case '7':
            break;

         case '8':
            break;

         case '9':
            break;


         case 'a':
            break;

         case 'b':
            break;


         case 'c':
            break;
            
            
         case 'd':
            break;

         case 'e':
            break;

         case 'f':
            break;

         case 'g':
            break;

         case 'h':
            break;

         case 'i':
            break;
         
         case 'j':
            break;

         case 'k':
            break;

         case 'l':
            break;

         case 'm':
            break;

         case 'n':
            break;

         case 'o':
            break;           
         
         case 'p':
           
            break;
            
         case 'u':
            Param_A_Rec = 1;
            Task_Params_Write( &Param_A_Rec, PARAM_TYPE_A, PARAM_A_CREDIT_CARD_BLOCKED);
            Param_A_Rec = 0xff;
            Task_Params_Read( &Param_A_Rec, PARAM_TYPE_A, PARAM_A_CREDIT_CARD_BLOCKED);     
            break;

         case 'v':
            Param_A_Rec = 0;
            Task_Params_Write( &Param_A_Rec, PARAM_TYPE_A, PARAM_A_CREDIT_CARD_BLOCKED);
            Param_A_Rec = 0xff;
            Task_Params_Read( &Param_A_Rec, PARAM_TYPE_A, PARAM_A_CREDIT_CARD_BLOCKED);
            break;         
            
         case 'z':
            TimersPrintActiveLink(ACTION_VOID, 0, 0);
            break;

         
#if 0           //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
         case '1':
            //printf("\nPrint\n");
            if (Epson_IsBusy() == TRUE)
            {
               //temp_sts = 0;
               //printf("Busy\n");
               //temp_sts = Epson_GetStatus();
               //printf("Status = 0x%x\n", temp_sts);
            }

            sprintf((char*)debug_msg_buffer, "Door Controller prn test\n");     
            Epson_Print( (char*)debug_msg_buffer, 26);

            //Task_Application_Prn_Go();
            //SET_BIT( App_TaskEvent[TASK_PRINTER], (EV_APP_PRN_PRINT_RECEIPT | EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT));
            break;
        
         case '2':
            //printf("\nFLine_SmChar\n");
            Epson_FullLine_SameChar('=');          // can be '='  or '-'
            //Task_Application_Prn_Go();
            break;
                   
         case '3':
            //printf("\nLF\n");
            Epson_LineFeed( 20);
            //Task_Application_Prn_Go();
            break;

         case '4':
            //printf("\nCuttHalf\n");
            Epson_Cutter( PRINT_CUTTER_HALF);
            //Task_Application_Prn_Go();
            break;

         case '5':
            //printf("\nCuttFull\n");
            Epson_Cutter( PRINT_CUTTER_FULL);
            //Task_Application_Prn_Go();
            break;

         case '6':
            //printf("\nPrintRaw\n");
            //sprintf((char*)debug_msg_buffer, "PrintRaw\n");
            //Epson_PrintRaw( debug_msg_buffer,   DBG_BUF_SIZE);        ???
            break;

         case '7':
            //printf("\nFonts A  Normal\n");
            Epson_SetFont( PRINT_FONT_A, PRINT_HEIGHT_NORMAL, PRINT_WIDTH_NORMAL);
            break;

         case '8':
            //printf("\nFonts B  Double\n");
            Epson_SetFont( PRINT_FONT_B, PRINT_HEIGHT_DOUBLE, PRINT_WIDTH_DOUBLE);
            break;
#if 0
         case '9':
            //printf("\n Increment Date\n");
            date_time = ClockAPI_GetDate(DATE_FORMAT_DDMMYYYY);
            //printf("Date = %lu\n", date_time);
            date_time = date_time + 1000000;
            //printf("Date = %lu\n", date_time);
            ClockAPI_SetDate( date_time, DATE_FORMAT_DDMMYYYY);
            break;
            
         case 'a':
            //printf("\n Incremtnt Minutes\n");
            date_time = ClockAPI_GetTime( TIME_FORMAT_HHMMSS);
            //printf("Time = %lu\n", date_time);
            date_time = date_time + 100;
            //printf("Time = %lu\n", date_time);
            ClockAPI_SetTime( date_time, TIME_FORMAT_HHMMSS);
            break;
            
         case 'b':
            //printf("\n Increment Hours\n");
            date_time = ClockAPI_GetTime( TIME_FORMAT_HHMMSS);
            //printf("Time = %lu\n", date_time);
            date_time = date_time + 10000;
            //printf("Time = %lu\n", date_time);
            ClockAPI_SetTime( date_time, TIME_FORMAT_HHMMSS);
            break;
            
         case 'c':
            //printf("\n Decrement Date\n");
            date_time = ClockAPI_GetDate(DATE_FORMAT_DDMMYYYY);
            //printf("Date = %lu\n", date_time);
            date_time = date_time - 1000000;
            //printf("Date = %lu\n", date_time);
            ClockAPI_SetDate( date_time, DATE_FORMAT_DDMMYYYY);
            break;
            
         case 'd':
            //printf("\n Decremtnt Minutes\n");
            date_time = ClockAPI_GetTime( TIME_FORMAT_HHMMSS);
            //printf("Time = %lu\n", date_time);
            date_time = date_time - 100;
            //printf("Time = %lu\n", date_time);
            ClockAPI_SetTime( date_time, TIME_FORMAT_HHMMSS);
            break;
            
         case 'e':
            //printf("\n Decrement Hours\n");
            date_time = ClockAPI_GetTime( TIME_FORMAT_HHMMSS);
            //printf("Time = %lu\n", date_time);
            date_time = date_time - 10000;
            //printf("Time = %lu\n", date_time);
            ClockAPI_SetTime( date_time, TIME_FORMAT_HHMMSS);
            break;

         case 'f':
            //printf("\n none\n");
            
            break;
            
         case 'g':
            //printf("\n\n");
            
            break;
            
         case 'h':
            //printf("\n\n");
            
            break;
            
         case 'i':
            //printf("\n\n");
            
            break;
            
         case 'j':
            printf("\n\n");
            
            break;
            
         case 'k':
            //printf("\n\n");
            
            break;
            
         case 'l':
            //printf("\n\n");
            
            break;
            
         case 'm':
            //printf("\n\n");
            
            break;
            
         case 'n':
            //printf("\n\n");
            
            break;
      
#endif      
         case 't':
            Task_Log_DebugClear_Idx_W();
            break;

         case 'u':
           Task_Server_View_Idx_R_Server();
           break;
           
         case 'v':
            Task_Application_DisplayReDraw();
            break;
        
         case 'w':
            Task_Log_DebugShow_RecordsInDB_Server();
            break;
        
            
         case 'x':
            //Task_Server_DbgIncrIdx_R_Trans();
            Task_Log_DebugIncr_RecordsInDB_Server();
            break;
            
         case 'y':
            //Task_TibaPay_ShowTxbuffers();
            //Task_Server_DbgIncrTransac();
            Task_Log_DebugClear_RecordsInDB_Server();
            break;

         case 'z':
            //Debug_PrintHex(p_NVRAM_LOGGER_BUF, 250);
            //Task_TibaPay_ShowTR2buffers();
            //Task_Application_DisplayCommLoss();
            Task_Server_DbgClearTransac();
            break;
#endif          //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<            
      }
      //api_RxStatus = 0;
   }
    
}



/*----------------------------------------------------------------------------*/
void                          Debug_Rx_ISR( void)
/*----------------------------------------------------------------------------*/
{
static   byte     Data;


   Data  =  api_Uart.Uart_RxByte();

   LedAPI_SetMode( LED_7, ON, 1);
   //api_RxStatus   =  Data;
   api_RxBuffer[api_IndexRxBuffer++] = Data;
  
   //api_TimerSec_Inactive   =  Task_Timer_SetTimeSec( 2);
}
#endif



/*----------------------------------------------------------------------------*/
void                          Debug_Start( byte ComPort)
/*----------------------------------------------------------------------------*/
{
   api_fActive    =  TRUE;

   //api_RxStatus         = 0;
   api_IndexRxBuffer    = 0;  
   
   /*** Communication Setting ***/
   memset( &api_Uart          ,  0, sizeof( api_Uart));

   api_Uart.Com.Port          =  ComPort;
   api_Uart.Com.BaudRate      =  BAUD_RATE_115200;
   api_Uart.Com.DataBits      =  DATA_BITS_8;
   api_Uart.Com.Parity        =  PARITY_NONE;
   api_Uart.Com.StopBits      =  STOP_BITS_1;
   api_Uart.RxFunc            =  Debug_Rx_ISR;
   UartDRV_SetPort( &api_Uart);
   /*****************************/

#if 0
   //printf("%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n%lu\n", 
         (ulong)NVRAM_ADDR_BASE,
         (ulong)NVRAM_ADDR_PRN_TX_BUF,
         (ulong)NVRAM_ADDR_APP_COMMANDS,
         (ulong)NVRAM_ADDR_SERVER_TX_PACKET,
         (ulong)NVRAM_ADDR_SERVER_Q_CMD,
         (ulong)NVRAM_ADDR_TRACK_2,
         (ulong)NVRAM_ADDR_APP_TRANS_INFO,
         (ulong)NVRAM_ADDR_SERVER_D_CMD,
         (ulong)NVRAM_ADDR_APP_E_CMD,
         (ulong)NVRAM_ADDR_TFT_DISPLAY,
         (ulong)NVRAM_ADDR_APP_LOG_TRANS_ITEMS,
         (ulong)NVRAM_ADDR_APP_B2B_TX_DATA,
         (ulong)NVRAM_ADDR_APP_TXT_BUF,
         (ulong)NVRAM_ADDR_APP_PRODUCTS,
         (ulong)NVRAM_ADDR_APP_SALES_INF0,
         (ulong)NVRAM_ADDR_APP_COINS_INF0,
         (ulong)NVRAM_ADDR_APP_BILLS_INF0,
         (ulong)NVRAM_ADDR_END);
#endif
}



/*----------------------------------------------------------------------------*/
void                          Debug_Stop( void)
/*----------------------------------------------------------------------------*/
{
   api_fActive    =  FALSE;
}



/*----------------------------------------------------------------------------*/
int                           putchar( int   Char) // *** DON'T CHANGE THE FUNCTION'S NAME *** (reserved and called by C compiler)
/*----------------------------------------------------------------------------*/
{
   if( api_fActive == FALSE)
      return( 0);

   switch( api_Uart.Com.Port)
   {
      case  COM_1 :
            #if   defined(__IOM16_H) || defined(__IOM32_H)  // ATmega16 or ATmega32
            while( ! (UCSRA & 0x20));
            UDR   =  Char;
            #else
            while( ! (UCSR0A & 0x20));
            UDR0  =  Char;
            #endif
            break;

      case  COM_2 :
            #if   defined(__IOM128_H) || defined(__IO2561_H) || defined(__IO2560_H) // ATmega128 or ATmega2561 or ATmega2560
            while( ! (UCSR1A & 0x20));
            UDR1  =  Char;
            #endif
            break;

      case  COM_3 :
            #if   defined(__IO2561_H) || defined(__IO2560_H)    // ATmega2561 or ATmega2560
            while( ! (UCSR2A & 0x20));
            UDR2  =  Char;
            #endif
            break;

      case  COM_4 :
            #if   defined(__IO2561_H) || defined(__IO2560_H)   // ATmega2561 or ATmega2560
            while( ! (UCSR3A & 0x20));
            UDR3  =  Char;
            #endif
            break;

      case  COM_5 :
            #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // XR Dual-port or Quad-port
			while( BIT_IS_CLEAR( HW_DEF_EXAR_LSRA, BIT_5) && BIT_IS_CLEAR( HW_DEF_EXAR_IERA, BIT_1));
            HW_DEF_EXAR_THRA  =  Char;
            #endif
            break;

      case  COM_6 :
            #if   defined(__USES_EXAR_2_PORTS__) || defined(__USES_EXAR_4_PORTS__)  // XR Dual-port or Quad-port
			while( BIT_IS_CLEAR( HW_DEF_EXAR_LSRB, BIT_5) && BIT_IS_CLEAR( HW_DEF_EXAR_IERB, BIT_1));
            HW_DEF_EXAR_THRB  =  Char;
            #endif
            break;

      case  COM_7 :
            #if   defined(__USES_EXAR_4_PORTS__)  // XR Quad-port
			while( BIT_IS_CLEAR( HW_DEF_EXAR_LSRC, BIT_5) && BIT_IS_CLEAR( HW_DEF_EXAR_IERC, BIT_1));
            HW_DEF_EXAR_THRC  =  Char;
            #endif
            break;

      case  COM_8 :
            #if   defined(__USES_EXAR_4_PORTS__)  // XR Quad-port
			while( BIT_IS_CLEAR( HW_DEF_EXAR_LSRD, BIT_5) && BIT_IS_CLEAR( HW_DEF_EXAR_IERD, BIT_1));
            HW_DEF_EXAR_THRD  =  Char;
            #endif
            break;
   }

   return( Char);
}


/*
-----------------------------------------------------------------------
Debug_PrintHex
print to ComPort, amount of bytes, in HEX format
-----------------------------------------------------------------------
*/
/*----------------------------------------------------------------------------*/
void                          Debug_PrintHex(byte* pBuff, byte length)
/*----------------------------------------------------------------------------*/
{
   byte idx;
   byte value[2];
   
   putchar ('\n');
   idx = 0;
   while (idx < length)
   {
      value[0] = pBuff[idx] & 0x0F;             // get 4 bits LSB
      value[1] = (pBuff[idx] & 0xF0) >>4;       // get 4 bits MSB
      // Convert LSB to a char: '0'..'9' and 'A'..'F'
      if ( value[0] <= 9 )
         value[0] += '0';
      else
         value[0] += ('A' - 0xA);

      // Convert MSB to a char: '0'..'9' and 'A'..'F'
      if ( value[1] <= 9 )
         value[1] += '0';
      else
         value[1] += ('A' - 0xA);
     
      putchar(value[1]);
      putchar(value[0]);
      putchar(' ');
      idx++;
   }
}



/*----------------------------------------------------------------------------*/
void                            Debug_StrPrint_Const( PGM_P pStrConst)  // see IAR\..CLIB\..pgmspace.h
/*----------------------------------------------------------------------------*/
{
byte  idx;

   idx = 0;
 
   while ( pStrConst[idx] != 0 )
   {
      putchar(pStrConst[idx]);
      idx++;
   }

}


char *pDbgPrintBuff = (char *)NVRAM_ADDR_T_DEBUG_PRINT_BUF;

/*
*                  Debug_PrintTextParam
*
* Print the inserted text. The, print the inserted number.
* Number can be a byte, usint or ulong (NumberSize = 1,2 or 4)
* Print format can be Decimal or Hex ( format = 0 or 1 ).
*/
/*----------------------------------------------------------------------------*/
void                            Debug_PrintTextParam(char* text, byte NumberSize, void* pNumber, byte format )
/*----------------------------------------------------------------------------*/
{
#if 1
   union
   {
      ulong Value;
      byte buf[4];
   } uValue;
   byte idx;
   char buffer[11];
   byte temp;
   
   
   //******  plot the string *******
   idx = 0;
   while ( text[idx] != 0 )
   {
      putchar(text[idx]);
      idx++;
   }

   //****** plot the number ******
   if (pNumber == NULL)
      return;

   switch( NumberSize)
   {
      case  sizeof( byte):
         uValue.Value =  *(byte *)(pNumber);
         break;

      case  sizeof( usint):
         uValue.Value =  *(usint *)(pNumber);
         break;

      case sizeof(ulong):
         uValue.Value =  *(ulong *)(pNumber);
         break;
         
      default:
         uValue.Value =  *(ulong *)(pNumber);
         break;
   }
   
   switch (format)
   {
      case 0:                // Decimal
         idx = 0;
         while ( uValue.Value > 0 )
         {
            buffer[idx] = uValue.Value % 10;
            uValue.Value = uValue.Value/10;
            idx++;
         }
         do
         {
            idx--;
            putchar( buffer[idx] + '0' );
         } while ( idx > 0 );
         putchar(' ');
         break;
        
      case 1:                // Hex Decimal
         idx = 0;
         for (idx=0; idx<NumberSize; idx++)
         {
            // get first digit of a byte
            buffer[0] = uValue.buf[NumberSize - idx - 1];
            buffer[0] &= 0x0F;
            if ( buffer[0] <= 9 )
               buffer[0] += '0';
            else
               buffer[0] += ('A' - 0xA);
            // get second digiti of a byte
            buffer[1] = uValue.buf[NumberSize - idx - 1];
            buffer[1] = (buffer[1]>>4) & 0x0F;
            if ( buffer[1] <= 9 )
               buffer[1] += '0';
            else
               buffer[1] += ('A' - 0xA);
            
            putchar(buffer[1]);
            putchar(buffer[0]);

         }
         break;
   }
   putchar('\n');
#endif
}






// Options to measure time
   //DEBUG_LINE_ON;                       //UartDRV_SetDTR(COM_6, OFF);         // set output line DTR_B for time measurement
   //DEBUG_LINE_OFF;                      //UartDRV_SetDTR(COM_6, ON);        // clr output line DTR_B for time measurement


#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif

