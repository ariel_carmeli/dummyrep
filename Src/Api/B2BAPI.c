/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  ORIGINATOR_MASTER          '*'
#define  ORIGINATOR_MASTER_VOID_CS  ';'
#define  ORIGINATOR_SLAVE           '#'

#define  START_PACKET               STX
#define  END_OF_DATA                ETX
#define  END_PACKET                 EOT
#define  SHIFT_BYTE                 0x0F

#define  MAX_BYTES_START            2  // 3 bytes of STX
#define  MAX_BYTES_END              1  // 2 bytes of EOT

#define  RX_TIMEOUT                 150   // in msec
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   STATE_START_PACKET         ,  // STX
   STATE_ORIGINATOR           ,  // '*' = Master      ';' = Master (w/o Checksum)      '#' = Slave
   STATE_UNIT_TYPE            ,  //
   STATE_ADDRESS              ,  //
   STATE_COMMAND              ,  //
   STATE_SUB_COMMAND          ,  //
   STATE_INDEX                ,  //
   STATE_DATA                 ,  //
   STATE_END_OF_DATA          ,  // ETX
   STATE_CHECKSUM             ,  //
   STATE_END_PACKET           ,  // EOT
   STATE_END_SESSION          ,
   /******************/
   STATE_LAST
}  tRxTxState;
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static   tB2BPacket          *api_pTxPacket;
static   tB2BPacket           api_RxPacket;
static   tTimerMsec           api_TimerMsec_Rx;
static   tUart                api_Uart;
static   byte                 api_RxBuffer[B2B_MAX_LEN_BUFFER];
static   tRxTxState           api_TxState;
static   tRxTxState           api_RxState;
static   bool                 api_fTxBusy;
static   byte                 api_TxCounter;
static   bool                 api_fRxNewPacket;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						B2BAPI_HandleTimer( void);
static	bool						B2BAPI_IsReservedChar( byte   Data);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		B2BAPI_Init( byte ComPort)
/*----------------------------------------------------------------------------*/
{
   api_fRxNewPacket     =  FALSE;

   /*** Communication Setting ***/
   api_Uart.Com.Port          =  ComPort;
   api_Uart.Com.BaudRate      =  BAUD_RATE_19200;
   api_Uart.Com.DataBits      =  DATA_BITS_8;
   api_Uart.Com.Parity        =  PARITY_NONE;
   api_Uart.Com.StopBits      =  STOP_BITS_1;
   api_Uart.TxFunc            =  B2BAPI_Tx_ISR;
   api_Uart.RxFunc            =  B2BAPI_Rx_ISR;
   UartDRV_SetPort( &api_Uart);
   /*****************************/
}



/*----------------------------------------------------------------------------*/
void                    		B2BAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
   B2BAPI_HandleTimer();
}



/*----------------------------------------------------------------------------*/
bool                    		B2BAPI_IsBusy( void)
/*----------------------------------------------------------------------------*/
{
   return( (api_fTxBusy == TRUE) || (api_TimerMsec_Rx > 0));
}



/*----------------------------------------------------------------------------*/
void                          B2BAPI_SendData( tB2BPacket   *pTxPacket)
/*----------------------------------------------------------------------------*/
{
   if( pTxPacket == NULL)
      return;

   api_pTxPacket  =  pTxPacket;

   api_fTxBusy =  TRUE;
   api_TxState =  STATE_START_PACKET;
   api_TimerMsec_Rx  =  Task_Timer_SetTimeMsec( RX_TIMEOUT);

   B2BAPI_Tx_ISR();
}



/*----------------------------------------------------------------------------*/
bool                    		B2BAPI_IsRxNewPacket( void)
/*----------------------------------------------------------------------------*/
{
   if( api_fRxNewPacket == TRUE)
   {
      api_TimerMsec_Rx  =  Task_Timer_SetTimeMsec( 20);
      api_fRxNewPacket  =  FALSE;
      return( TRUE);
   }
   else
      return( FALSE);
}



/*----------------------------------------------------------------------------*/
tB2BPacket                   *B2BAPI_GetData( void)
/*----------------------------------------------------------------------------*/
{
   api_RxPacket.pBuffer =  api_RxBuffer;
   return( &api_RxPacket);
}



/*----------------------------------------------------------------------------*/
void                    		B2BAPI_Tx_ISR( void)
/*----------------------------------------------------------------------------*/
{
static   bool     fChecksumEnabled  =  FALSE;
static   bool     fShiftByteEnabled =  FALSE;
static   bool     fShiftByte        =  FALSE;
static   byte     LastByte;
static   byte     Checksum;
static   usint    BufIdx;
byte              Data;
bool              fTxData;

   fTxData  =  TRUE;
   Data     =  0;

   if( fShiftByte == FALSE)
   {
      switch( api_TxState)
      {
         case  STATE_START_PACKET:

               fChecksumEnabled  =  FALSE;   // don't calculate Checksum from this point
               fShiftByteEnabled =  FALSE;   // void Shift byte from this point
               fShiftByte        =  FALSE;
               LastByte          =  0;
               Checksum          =  0;
               BufIdx            =  0;

               Data  =  START_PACKET;  // STX
               if( (++api_TxCounter) >= MAX_BYTES_START) // used to send more than once the same byte
               {
                  api_TxCounter  =  0;
                  api_TxState    ++;
               }
               break;

         case  STATE_ORIGINATOR  :
               fChecksumEnabled  =  TRUE; // start calculating Checksum from this point
               if( api_pTxPacket->fFromMaster == TRUE)   Data  =  ORIGINATOR_MASTER;
               else                                      Data  =  ORIGINATOR_SLAVE;

               api_TxState    ++;
               break;

         case  STATE_UNIT_TYPE   :
               fShiftByteEnabled =  TRUE;   // handle Shift byte from this point
               Data  =  api_pTxPacket->UnitType;
               api_TxState    ++;
               break;

         case  STATE_ADDRESS     :
               Data  =  api_pTxPacket->Address;
               api_TxState    ++;
               break;

         case  STATE_COMMAND     :
               Data  =  api_pTxPacket->Command;
               api_TxState    ++;
               break;

         case  STATE_SUB_COMMAND :
               Data  =  api_pTxPacket->Sub_Command;
               api_TxState    ++;
               break;

         case  STATE_INDEX       :
               Data  =  api_pTxPacket->Index;

               BufIdx   =  0;
               if( api_pTxPacket->BufSize > 0)  api_TxState ++;
               else                             api_TxState =  STATE_END_OF_DATA;
               break;

         case  STATE_DATA        :
               Data  =  api_pTxPacket->pBuffer[BufIdx ++];

               if( BufIdx >= api_pTxPacket->BufSize)
                  api_TxState ++;
               break;

         case  STATE_END_OF_DATA :
               fChecksumEnabled  =  FALSE; // stop calculating Checksum from this point
               fShiftByteEnabled =  FALSE; // void Shift byte from this point
               Data  =  ETX;
               api_TxState    ++;
               break;

         case  STATE_CHECKSUM    :
               fShiftByteEnabled =  TRUE;   // handle Shift byte from this point
               Data  =  Checksum;
               api_TxState    ++;
               break;

         case  STATE_END_PACKET  :
               fShiftByteEnabled =  FALSE;   // void Shift byte from this point
               Data  =  EOT;
               if( (++api_TxCounter) >= MAX_BYTES_END) // used to send more than once the same byte
               {
                  api_TxCounter  =  0;
                  api_TxState    ++;
               }
               break;

         case  STATE_END_SESSION :
         default                 :
               fTxData  =  FALSE;
               break;
      }

      if( fChecksumEnabled == TRUE)
          Checksum   += Data;
   }

   if( fShiftByteEnabled == TRUE)
   {
      if( fShiftByte == FALSE)
      {
         if( B2BAPI_IsReservedChar( Data) == TRUE)
         {
            fShiftByte  =  TRUE;
            LastByte    =  Data;
            Data        =  SHIFT_BYTE; // 0x0F
         }
      }
      else
      {
         fShiftByte  =  FALSE;
         Data        =  0;
         Data        =  LastByte;
      }
   }
 
   if( fTxData == TRUE)
   {

      api_Uart.Uart_TxByte( Data);
   }
   else
   {
      api_Uart.Uart_TxEnd();

      api_fTxBusy       =  FALSE;
      api_TimerMsec_Rx  =  Task_Timer_SetTimeMsec( RX_TIMEOUT);

      api_TxState =  STATE_START_PACKET;
   }

}



/*----------------------------------------------------------------------------*/
void                    		B2BAPI_Rx_ISR( void)
/*----------------------------------------------------------------------------*/
{
static   bool     fChecksumEnabled  =  FALSE;
static   bool     fBufferOverrun    =  FALSE;
static   bool     fVoidChecksum     =  FALSE;
static   bool     fShiftByte        =  FALSE;
static   byte     Checksum;
byte              Data;

   Data  =  api_Uart.Uart_RxByte();

   api_TimerMsec_Rx  =  Task_Timer_SetTimeMsec( RX_TIMEOUT);

   if( api_fRxNewPacket == TRUE) // if last packet was not handled yet
   {
      return;
   }

   if( fShiftByte == FALSE)
   {
      if( Data == SHIFT_BYTE)
      {
         fShiftByte  =  TRUE;
         return;
      }

      if( Data == START_PACKET   /* STX */)  api_RxState =  STATE_START_PACKET;
      if( Data == END_OF_DATA    /* ETX */)  api_RxState =  STATE_END_OF_DATA ;
      if( Data == END_PACKET     /* EOT */)  api_RxState =  STATE_END_PACKET  ;
   }

   fShiftByte  =  FALSE;

   switch( api_RxState)
   {
      case  STATE_START_PACKET:  // STX
            if( Data == START_PACKET /* STX */)
            {
               api_RxPacket.BufSize =  0;
               fChecksumEnabled  =  FALSE;
               fBufferOverrun    =  FALSE;
               fVoidChecksum     =  FALSE;
               fShiftByte        =  FALSE;
               Checksum          =  0;
               api_RxState       =  STATE_ORIGINATOR;
            }
            break;

      case  STATE_ORIGINATOR  :
            fChecksumEnabled  =  TRUE; // start calculating Checksum from this point

            if( Data == ORIGINATOR_MASTER          )  // '*'
            {
               api_RxPacket.fFromMaster   =  TRUE;
               api_RxState ++;
            }

            if( Data == ORIGINATOR_MASTER_VOID_CS  )  // ';'
            {
               fVoidChecksum              =  TRUE;
               api_RxPacket.fFromMaster   =  TRUE;
               api_RxState ++;
            }

            if( Data == ORIGINATOR_SLAVE           )  // '#'
            {
               api_RxPacket.fFromMaster   =  FALSE;
               api_RxState ++;
            }
            break;

      case  STATE_UNIT_TYPE   :
            api_RxPacket.UnitType      =  Data;
            api_RxState ++;
            break;

      case  STATE_ADDRESS     :
            api_RxPacket.Address       =  Data;
            api_RxState ++;
            break;

      case  STATE_COMMAND     :
            api_RxPacket.Command       =  Data;
            api_RxState ++;
            break;

      case  STATE_SUB_COMMAND :
            api_RxPacket.Sub_Command   =  Data;
            api_RxState ++;
            break;

      case  STATE_INDEX       :
            api_RxPacket.Index         =  Data;
            api_RxState ++;
            break;

      case  STATE_DATA        :
            if( api_RxPacket.BufSize < B2B_MAX_LEN_BUFFER)
               api_RxBuffer[api_RxPacket.BufSize ++]  =  Data;
            else
               fBufferOverrun    =  TRUE;
            break;

      case  STATE_END_OF_DATA :  // ETX
            fChecksumEnabled  =  FALSE;
            if( fVoidChecksum == TRUE)
               api_fRxNewPacket  =  TRUE;
            else
               api_RxState ++;
            break;

      case  STATE_CHECKSUM    :
            api_fRxNewPacket  =  ((Checksum == Data) && (fBufferOverrun == FALSE));
            break;

      case  STATE_END_PACKET  :  // EOT
      case  STATE_END_SESSION :
      default                 :
            api_RxState =  STATE_START_PACKET;
            break;
   }

   if( fChecksumEnabled == TRUE)
      Checksum += Data;
   
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						B2BAPI_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( api_TimerMsec_Rx <  Gap)  api_TimerMsec_Rx  =  0; else  api_TimerMsec_Rx  -= Gap;
   }
}



/*----------------------------------------------------------------------------*/
static	bool						B2BAPI_IsReservedChar( byte   Data)
/*----------------------------------------------------------------------------*/
{
bool     ReturnValue;


   ReturnValue =  FALSE;

   if( Data == START_PACKET               )  ReturnValue =  TRUE; // STX
   if( Data == END_OF_DATA                )  ReturnValue =  TRUE; // ETX
   if( Data == END_PACKET                 )  ReturnValue =  TRUE; // EOT
   if( Data == SHIFT_BYTE                 )  ReturnValue =  TRUE; // 0x0F
   if( Data == ORIGINATOR_MASTER          )  ReturnValue =  TRUE; // '*'
   if( Data == ORIGINATOR_MASTER_VOID_CS  )  ReturnValue =  TRUE; // ';'
   if( Data == ORIGINATOR_SLAVE           )  ReturnValue =  TRUE; // '#'

   return( ReturnValue);
}

