/*------------------------------ Include Files -------------------------------*/
#include	"App.h"
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*----------------------------- Global Variables -----------------------------*/
static	tTimerMsec				api_TimerMsec;
static	tTimerMsec				api_TimerMsecBlink;
static	tTimerSec				api_TimerSec;
static	byte						api_Lines[MAX_LINES][LCD_LINE_WIDTH + 1];
static	byte                 api_TextBlinkMode;
static	byte                 api_CursorMode;
static	byte                 api_CursorLine;
static	byte                 api_CursorPosition;
static	bool						api_fTextWasChanged;
static	bool						api_fWriteToScreen;
static	bool						api_fInit;
static	bool						api_fBlinkEnable;
static	bool						api_fIdleMode;
static   bool                 api_fSwapHebrew;
/*------------------------ Local Function Prototypes -------------------------*/
static	void						LcdAPI_HandleTimer( void);
/*----------------------------------------------------------------------------*/



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_Init( void)
/*----------------------------------------------------------------------------*/
{
   api_TimerMsec              =  0;
   api_TimerMsecBlink         =  0;
   api_TimerSec               =  0;
	memset( api_Lines		      ,	0, sizeof( api_Lines));
   api_TextBlinkMode          =  OFF;
   api_CursorMode             =  CURSOR_MODE_HIDE;
   api_CursorLine             =  LINE1;
   api_CursorPosition         =  0;
	api_fTextWasChanged	      =	FALSE;
	api_fWriteToScreen	      =	FALSE;
   api_fInit                  =  FALSE;
   api_fBlinkEnable           =  FALSE;
   api_fIdleMode              =  FALSE;
   api_fSwapHebrew            =  TRUE;
   api_fInit                  =  TRUE;

	LcdDRV_Init();
	LcdDRV_SetBackLight( ON);
   LcdDRV_SetContrast( 0x18);
   //LcdDRV_SetCGRAM();
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_Main( void)
/*----------------------------------------------------------------------------*/
{
	LcdAPI_HandleTimer();
   // refresh the display every 5000ms, if not changed
	if( api_TimerMsec == 0)
   {
      LcdDRV_Init();
      api_TimerMsec        =	Task_Timer_SetTimeMsec( 500);
		api_fTextWasChanged	=	TRUE;
   }

	if( api_fTextWasChanged == TRUE)
	{
		api_fTextWasChanged	=	FALSE;
		api_fWriteToScreen	=	TRUE;
	}

	if( api_fWriteToScreen	==	TRUE)
	{
	byte	Line;

		api_fWriteToScreen	=	FALSE;

		for( Line = LINE1; Line < MAX_LINES; Line ++)
			LcdDRV_WriteString( Line, (char *)api_Lines[Line]);

      LcdDRV_SetCursorPosition( api_CursorLine, api_CursorPosition);
      LcdDRV_SetCursorMode( api_CursorMode);
	}

   if( (api_fInit == TRUE) && (api_TimerSec == 0))
   {
      api_fInit      =  FALSE;
      //LcdAPI_WriteStringConst( LINE1, MSG_READY);
   }

   if( (api_fBlinkEnable == TRUE) && (api_TimerMsecBlink == 0))
   {
      api_TimerMsecBlink   =  Task_Timer_SetTimeMsec( 500);
      api_TextBlinkMode    =  (api_TextBlinkMode == ON) ? OFF : ON;
      LcdDRV_HideText( api_TextBlinkMode);
   }
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_DontSwapHebrew( void)
/*----------------------------------------------------------------------------*/
{
   api_fSwapHebrew   =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_WriteChar( byte	Line, byte	Offset, byte	Char)
/*----------------------------------------------------------------------------*/
{
   api_fIdleMode = FALSE;

	api_Lines[Line][Offset]	=	Char;
	api_fTextWasChanged		=	TRUE;
   api_fInit               =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_WriteString( byte	Line, byte	Offset, char	*pStr, byte	Size)
/*----------------------------------------------------------------------------*/
{
bool     fHebrew;
byte     StartOffset;
byte     EndOffset;


   api_fIdleMode  =  FALSE;

	if( (pStr != NULL) && (Size > 0))
	{
      fHebrew     =  FALSE;
      StartOffset =  Offset;
      EndOffset   =  (Offset + Size - 1);

		while( (Offset < LCD_LINE_WIDTH) && (Size > 0))
		{
         if( App_Language == LANGUAGE_HEBREW)
         {
            if( ((*pStr) >= 0xE0 /*'�'*/) && ((*pStr) <= 0xFA /*'�'*/))
            {
               (*pStr) -= 0x40;
               fHebrew  =  TRUE;
            }
         }

			if( *pStr != 0)
				api_Lines[Line][Offset ++]	=	*(pStr ++);
			else
				api_Lines[Line][Offset ++] =	SPACE;

			Size --;
		}

      if( (fHebrew == TRUE) && (api_fSwapHebrew == TRUE))
         LcdAPI_HandleAndSwap_Hebrew( api_Lines[Line], StartOffset, EndOffset);

		api_fTextWasChanged  =  TRUE;
      api_fInit            =  FALSE;
	}

   api_fSwapHebrew   =  TRUE;
}



CONST	static   char		Messages[MAX_MESSAGES][MAX_LANGUAGES_EXTENDED][LCD_LINE_WIDTH + 1]	=
                        {
                                       {	"----------------",  "----------------"},    // reserved for MSG_APP_VER
/* MSG_READY                        */ {	"     Ready      ",  "���� ������     "},
/* MSG_PRESS_ANY_KEY                */ {	"Press Any Key...",  "��� �� ��� �����"},
/* MSG_SETUP_ERR_1                  */ {	"Error: Qtty = 0 ",  "�����: ���� = 0 "},
/* MSG_SET_QTTY_EMPTYING            */ {	"Set Emptying Qty",  "���� ���� ������"},

// Errors
/* MSG_ERROR                        */ {	"CRITICAL ERROR !",  "  ���� ����� !  "},
/* MSG_ERR_SLAVE_OFF                */ {	"Slave Ctrlr OFF ",  "���: ��� ���    "},
/* MSG_ERR_READER_1_OFF             */ {	"Reader 1 OFF    ",  "���: ���� ������"},
/* MSG_ERR_READER_2_OFF             */ {	"Reader 2 OFF    ",  "���: ���� ����� "},
/* MSG_ERR_BILL_VAL_OFF             */ {	"Bill Val. OFF   ",  "���: ���� ����� "},
/* MSG_ERR_BILL_VAL_JAM             */ {	"Bill Val. Jammed",  "��� ����        "},
/* MSG_ERR_BILL_VAL_STACKER_FULL    */ {	"Bill Val. Full  ",  "������ ��� ���� "},
/* MSG_ERR_PRINTER_OFF              */ {	"Printer OFF     ",  "���: �����      "},
/* MSG_ERR_PRINTER_OUT_OF_PAPER     */ {	"End of Paper    ",  "��� ���� ������ "},

// Warnings
/* MSG_WARNING                      */ {	"    WARNING !   ",  "    ����� !     "},
/* MSG_WARNING_HOOPER_1_LOW_LEVEL   */ {	"Low Level (H1)  ",  "���� ������� )A("},
/* MSG_WARNING_HOOPER_2_LOW_LEVEL   */ {	"Low Level (H2)  ",  "���� ������� )B("},
/* MSG_WARNING_HOOPER_3_LOW_LEVEL   */ {	"Low Level (H3)  ",  "���� ������� )C("},
/* MSG_WARNING_HOOPER_4_LOW_LEVEL   */ {	"Low Level (H4)  ",  "���� ������� )D("},
/* MSG_WARNING_PRINTER_EOP_SENSOR   */ {	"Low Level Paper ",  "���� ���� ������"},
/* MSG_WARNING_CHANGE_LOW_LEVEL     */ {	"Change Low Level",  "��� ����� ����  "},
                        };
/*----------------------------------------------------------------------------*/
void                    		LcdAPI_WriteStringConst( byte	Line, byte	MsgCode)
/*----------------------------------------------------------------------------*/
{
   if( MsgCode != MSG_READY)
      ClockAPI_DisplayClock( DISABLED);

   LcdAPI_ClearLine( LINE2);

   switch( MsgCode)
   {
      case  MSG_APP_VER:
            {
            char     Str[LCD_LINE_WIDTH + 1];
            byte     StrLen;

               strcpy_P( (char *)api_Lines[LINE1], App_Code);

               //strcpy_P( Str, App_Date);
               strcpy( Str, App_Date);
               LcdAPI_WriteString( LINE2, 0, Str, LCD_LINE_WIDTH);

               strcpy_P( Str, App_Version);
               StrLen   =  strlen( Str);
               LcdAPI_WriteString( LINE2, LCD_LINE_WIDTH - StrLen, Str, StrLen);
            }
            break;

      default:
            strcpy_P( (char *)api_Lines[Line], Messages[MsgCode][App_Language]);

            if( App_Language == LANGUAGE_HEBREW)
               LcdAPI_HandleAndSwap_Hebrew( api_Lines[Line], 0, (LCD_LINE_WIDTH - 1));

            api_fIdleMode = FALSE;

            switch( MsgCode)
            {
               case  MSG_READY:
                     api_fIdleMode = TRUE;
                     ClockAPI_DisplayClock( ENABLED);
                     LcdAPI_SetDisplayBlinking( OFF);
                     break;

               default:
                     api_fInit   =  FALSE;
                     break;
            }
   }

	api_fTextWasChanged = TRUE;
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_WriteNumber( byte	Line, byte	Offset, ulong  Number, byte	Digits)
/*----------------------------------------------------------------------------*/
{
ulong    Div;
byte     i, j;


   api_fIdleMode = FALSE;

	for( i = Digits; i > 0; i --)
	{
		for( j = 1, Div = 1; j < i; j ++, Div *= 10);

		if( Offset < LCD_LINE_WIDTH)
			api_Lines[Line][Offset ++] = ((Number / Div) % 10) + '0';
	}

	api_fTextWasChanged =   TRUE;
   api_fInit           =   FALSE;
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_WriteNumberHex( byte	Line, byte	Offset, ulong  Number, byte	Digits)
/*----------------------------------------------------------------------------*/
{
char  String[11 + 1];


   api_fIdleMode = FALSE;

   sprintf( String, "%.*lX", Digits, Number);
   LcdAPI_WriteString( Line, Offset, String, strlen( String));
   api_fInit   =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_WritePrice( byte	Line, byte	Offset, ulong  Number, byte Justify)
/*----------------------------------------------------------------------------*/
{
byte     StrLen;
byte     ActualOffset;
char     String[11 + 1];


   api_fIdleMode = FALSE;

   sprintf( String, "%lu.%.2lu", (Number / 100), (Number % 100));
   StrLen   =  strlen( String);

   switch( Justify)
   {
      case  JUSTIFY_VOID   :  ActualOffset   =  Offset;                          break;
      case  JUSTIFY_LEFT   :  ActualOffset   =  0;                               break;
      case  JUSTIFY_CENTER :  ActualOffset   =  ((LCD_LINE_WIDTH - StrLen) / 2); break;
      case  JUSTIFY_RIGHT  :  ActualOffset   =  (LCD_LINE_WIDTH - StrLen);       break;
      default              :  ActualOffset   =  Offset;                          break;
   }

   LcdAPI_WriteString( Line, ActualOffset, String, StrLen);

   api_fInit   =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_ClearLine( byte	Line)
/*----------------------------------------------------------------------------*/
{
   api_fIdleMode = FALSE;

	memset( api_Lines[Line], ' ', sizeof( api_Lines[Line]));
	api_Lines[Line][sizeof( api_Lines[Line]) - 1] = 0;
	api_fTextWasChanged =   TRUE;
   api_fInit           =   FALSE;
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_ClearScreen( void)
/*----------------------------------------------------------------------------*/
{
byte	Line;


   api_fIdleMode = FALSE;

	for( Line = LINE1; Line < MAX_LINES; Line ++)
		LcdAPI_ClearLine( Line);

	// faster way to clear a screen
	LcdDRV_ClearScreen();
	api_fTextWasChanged =   FALSE; // don't change this value to TRUE; It's FALSE deliberately
   api_fInit           =   FALSE;

   LcdAPI_SetCursorPosition( LINE1, 0);
   LcdAPI_SetCursorMode( CURSOR_MODE_HIDE);
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_SetBackLight( bool	Mode)
/*----------------------------------------------------------------------------*/
{
	LcdDRV_SetBackLight( Mode);
}



/*----------------------------------------------------------------------------*/
void                    		LcdAPI_ProgressBar( byte Line, byte Value, byte MaxValue)
/*----------------------------------------------------------------------------*/
{
static   byte  Count =  0;
static   byte  Idx   =  0;


   api_fIdleMode = FALSE;

   if( (MaxValue > 0) && (MaxValue >= Value))
   {
      Count = ((Value * LCD_LINE_WIDTH) / MaxValue) + 1;

      if( Count == 0)
         Count = 1;

      if( Count > LCD_LINE_WIDTH)
         Count = LCD_LINE_WIDTH;

      LcdAPI_ClearLine( Line);
      for( Idx = 0; Idx < Count; Idx ++)
         LcdAPI_WriteChar( Line, Idx, CHAR_BAR_2);

	   api_fTextWasChanged =   TRUE;
   }
   api_fInit   =  FALSE;
}



/*----------------------------------------------------------------------------*/
void                          LcdAPI_SetCursorPosition( byte Line, byte Offset)
/*----------------------------------------------------------------------------*/
{
   api_CursorLine       =  Line;
   api_CursorPosition   =  Offset;
}



/*----------------------------------------------------------------------------*/
void                          LcdAPI_SetCursorMode( byte Mode)
/*----------------------------------------------------------------------------*/
{
   api_CursorMode = Mode;
}



/*----------------------------------------------------------------------------*/
void                          LcdAPI_SetDisplayBlinking( byte Mode)
/*----------------------------------------------------------------------------*/
{
   if( Mode == ON)
   {
      api_TextBlinkMode    =  ON;
      api_fBlinkEnable     =  TRUE;
   }
   else
   {
      api_TextBlinkMode    =  OFF;
      api_fBlinkEnable     =  FALSE;
      api_TimerMsecBlink   =  0;
      LcdDRV_HideText( OFF);
   }
}



/*----------------------------------------------------------------------------*/
void                          LcdAPI_ToggleMsg_Idle( void)
/*----------------------------------------------------------------------------*/
{
static   bool     fShowAppMsg =  FALSE;


   if( api_fIdleMode == FALSE)
      return;

   fShowAppMsg = (fShowAppMsg == FALSE) ? TRUE : FALSE;
}



/*----------------------------------------------------------------------------*/
void                          LcdAPI_HandleAndSwap_Hebrew( byte  *pStr, byte   From, byte  To)
/*----------------------------------------------------------------------------*/
{
byte     Idx;
byte     tmpChar;


   // Adjust Hebrew characters (0xE0..0xFA --> 0xA0..0xBA)
   for( Idx = 0; pStr[Idx] != 0; Idx ++)
   {
      if( (pStr[Idx] >= 0xE0 /*'�'*/) && (pStr[Idx] <= 0xFA /*'�'*/))
         pStr[Idx] -= 0x40;
   }

   while( From < To)
   {
      tmpChar     =  pStr[From];
      pStr[From]  =  pStr[To];
      pStr[To]    =  tmpChar;

      From        ++;
      To          --;
   }
}



/*----------------------------------------------------------------------------*/
void                          LcdAPI_SetContrast( byte   Value)
/*----------------------------------------------------------------------------*/
{
   LcdDRV_SetContrast( Value);
}



#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif



/*----------------------------------------------------------------------------*/
static	void						LcdAPI_HandleTimer( void)
/*----------------------------------------------------------------------------*/
{
static	tTimerMsec	OldTimer	=	0;
tTimerMsec	         NewTimer	=	0;
byte                 Gap;


	NewTimer	=	Task_Timer_GetTimer();

   if( OldTimer	!= NewTimer)
   {
      Gap      =  (NewTimer - OldTimer);
   	OldTimer =	NewTimer;

      if( api_TimerMsec       <  Gap)  api_TimerMsec        =  0; else  api_TimerMsec        -= Gap;
      if( api_TimerMsecBlink  <  Gap)  api_TimerMsecBlink   =  0; else  api_TimerMsecBlink   -= Gap;
      if( api_TimerSec        <  Gap)  api_TimerSec         =  0; else  api_TimerSec         -= Gap;
   }
}

