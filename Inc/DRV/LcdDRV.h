/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define  LCD_LINE_WIDTH             16 // maximum 16 characters per LCD line

//#define  LINE1                      0
//#define  LINE2                      1
//#define  MAX_LINES                  2

#define  TEXT_MODE_NORMAL           0
#define  TEXT_MODE_BLINK            1

#define  CURSOR_MODE_HIDE           0
#define  CURSOR_MODE_BLINK          1

#define  JUSTIFY_VOID               0
#define  JUSTIFY_LEFT               1
#define  JUSTIFY_CENTER             2
#define  JUSTIFY_RIGHT              3
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		LcdDRV_Init( void);
void                    		LcdDRV_SetBackLight( bool	Mode);
void                    		LcdDRV_ClearScreen( void);
void                    		LcdDRV_WriteString( byte	Line, char	*pStr);
void                    		LcdDRV_WriteChar( byte	Line, byte	Offset, char	Char);
void                          LcdDRV_SetCursorPosition( byte Line, byte Offset);
void                          LcdDRV_SetCursorMode( byte Mode);
void                          LcdDRV_HideText( byte Mode);
void                          LcdDRV_SetContrast( byte   Value);
/*----------------------------------------------------------------------------*/

