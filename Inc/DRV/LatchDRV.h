/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
            // Latch U12 bit-map
#define     U12_BIT_KBD_ROW_4       BIT_0
#define     U12_BIT_KBD_ROW_3       BIT_1

            // Latch U13 bit-map
#define     U13_BIT_KBD_ROW_1       BIT_6
#define     U13_BIT_KBD_ROW_2       BIT_7

            // Latch U14 bit-map
#define     U14_BIT_KBD_COL_1       BIT_0
#define     U14_BIT_KBD_COL_3       BIT_1
#define     U14_BIT_KBD_COL_4       BIT_6
#define     U14_BIT_KBD_COL_2       BIT_7

            // Latch U15 bit-map
#define     U15_BIT_LCD_LUMINOUS    (BIT_4 | BIT_3 | BIT_2 | BIT_1 | BIT_0)
#define     U15_BIT_LCD_E           BIT_5
#define     U15_BIT_LCD_A0          BIT_6
#define     U15_BIT_LCD_RW          BIT_7

            // Latch U17 bit-map
#define     U17_BIT_RELAY_1         BIT_0
#define     U17_BIT_RELAY_2         BIT_1
#define     U17_BIT_RELAY_3         BIT_2
#define     U17_BIT_RELAY_4         BIT_3
#define     U17_BIT_4               BIT_4 // not connected
#define     U17_BIT_RELAY_5         BIT_5
#define     U17_BIT_LCD_BACKLIGHT   BIT_6
#define     U17_BIT_BUZZER          BIT_7
#define     U17_BIT_HOPPER_1        U17_BIT_RELAY_5   // same line controls either Relay 5 or Hopper 4
#define     U17_BIT_HOPPER_2        U17_BIT_RELAY_2   // same line controls either Relay 2 or Hopper 1
#define     U17_BIT_HOPPER_3        U17_BIT_RELAY_3   // same line controls either Relay 3 or Hopper 2
#define     U17_BIT_HOPPER_4        U17_BIT_RELAY_4   // same line controls either Relay 4 or Hopper 3

            // Latch U18 bit-map
#define     U18_BIT_HOPPER_4_LOW    BIT_0 // Door sense 2 (Safe box)
#define     U18_BIT_HOPPER_4_SENSE  BIT_1
#define     U18_BIT_HOPPER_3_LOW    BIT_2
#define     U18_BIT_HOPPER_3_SENSE  BIT_3
#define     U18_BIT_HOPPER_1_LOW    BIT_4
#define     U18_BIT_HOPPER_1_SENSE  BIT_5
#define     U18_BIT_HOPPER_2_LOW    BIT_6
#define     U18_BIT_HOPPER_2_SENSE  BIT_7
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   LATCH_U12      ,
   LATCH_U13      ,
   LATCH_U14      ,
   LATCH_U15      ,
   LATCH_U16      ,
   LATCH_U17      ,
   LATCH_U18      ,
   /******************/
   MAX_LATCHES
}  tLatch;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		LatchDRV_Init( void);
void                    		LatchDRV_Main( void);
void                          LatchDRV_SetBit( tLatch Latch, byte Bit);
void                          LatchDRV_ClearBit( tLatch  Latch, byte Bit);
void                          LatchDRV_SetValue( tLatch Latch, byte Value);
byte                          LatchDRV_GetValue( tLatch  Latch);
/*----------------------------------------------------------------------------*/

