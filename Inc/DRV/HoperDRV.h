/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		HopperDRV_Init( void);
void                    		HopperDRV_SetMode( byte Hopper, bool   Mode);
bool                    		HopperDRV_GetStatus_LowLevel( byte Hopper);
/*----------------------------------------------------------------------------*/

