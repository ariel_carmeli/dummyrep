/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		EsqrDRV_Init( void);
byte                          EsqrDRV_Read( ulong  Address);
void                          EsqrDRV_Write( ulong Address, byte Data);
/*----------------------------------------------------------------------------*/

