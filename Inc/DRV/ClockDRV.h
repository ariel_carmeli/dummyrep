/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		ClockDRV_Init( void);
void                    		ClockDRV_WriteRTC( byte Address, byte Data);
byte                    		ClockDRV_ReadRTC( byte Address);
/*----------------------------------------------------------------------------*/

