/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		KeyboardDRV_Init( void);
void                    		KeyboardDRV_Scan( void);
byte                    		KeyboardDRV_GetKey( void);
/*----------------------------------------------------------------------------*/

