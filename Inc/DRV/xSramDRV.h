/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		xSramDRV_Init( void);
void                    		xSramDRV_Write( byte Device, byte Page, usint Address, byte Data);
byte                    		xSramDRV_Read( byte Device, byte Page, usint Address);
/*----------------------------------------------------------------------------*/

