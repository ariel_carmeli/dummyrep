/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
typedef  enum  // I/O lines - Inputs
{
   // Group A -   EXAR A
   INPUT_CTS_A          =  0x00  ,  // Exar A   -  CTS
   INPUT_RI_A           =  0x01  ,  // Exar A   -  RI
   INPUT_DCD_A          =  0x02  ,  // Exar A   -  DCD
   INPUT_DSR_A          =  0x03  ,  // Exar A   -  DSR

   // Group B -   EXAR B
   INPUT_CTS_B          =  0x10  ,  // Exar B   -  CTS
   INPUT_RI_B           =  0x11  ,  // Exar B   -  RI
   INPUT_DCD_B          =  0x12  ,  // Exar B   -  DCD
   INPUT_DSR_B          =  0x13  ,  // Exar B   -  DSR

   // Group C -   EXAR C
   INPUT_CTS_C          =  0x20  ,  // Exar C   -  CTS
   INPUT_RI_C           =  0x21  ,  // Exar C   -  RI
   INPUT_DCD_C          =  0x22  ,  // Exar C   -  DCD
   INPUT_DSR_C          =  0x23  ,  // Exar C   -  DSR

   // Group D -   EXAR D
   INPUT_CTS_D          =  0x30  ,  // Exar D   -  CTS
   INPUT_RI_D           =  0x31  ,  // Exar D   -  RI
   INPUT_DCD_D          =  0x32  ,  // Exar D   -  DCD
   INPUT_DSR_D          =  0x33  ,  // Exar D   -  DSR

   // Group E -   Latch U18 (PORTE 4-7 + PORTF 4-7)
   INPUT_U18_0          =  0x40  ,  // PF:7
   INPUT_U18_1          =  0x41  ,  // PE:7
   INPUT_U18_2          =  0x42  ,  // PF:6
   INPUT_U18_3          =  0x43  ,  // PE:6
   INPUT_U18_4          =  0x44  ,  // PF:4
   INPUT_U18_5          =  0x45  ,  // PE:4
   INPUT_U18_6          =  0x46  ,  // PF:5
   INPUT_U18_7          =  0x47  ,  // PE:5
   /******************/
   INPUT_LAST
}  tInput;


typedef  enum  // I/O lines - Outputs
{
   OUTPUT_MGC_LED_1_A   =  0x00  ,  // DTR   Exar port A
   OUTPUT_MGC_LED_2_A   =  0x01  ,  // RTS   Exar port A
   OUTPUT_MGC_LED_1_B   =  0x02  ,  // DTR   Exar port B
   OUTPUT_MGC_LED_2_B   =  0x03  ,  // RTS   Exar port B
   /******************/
   OUTPUT_LAST
}  tOutput;


typedef  enum
{
   IO_GROUP_A  =  0  ,  // Exar port A
   IO_GROUP_B        ,  // Exar port B
   IO_GROUP_C        ,  // Exar port C
   IO_GROUP_D        ,  // Exar port D
   IO_GROUP_E        ,  // Latch U18 (PORTE 4-7 + PORTF 4-7)
   /******************/
   MAX_IO_GROUPS
}  tInOutGroup;
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		InOutDRV_Init( void);
void                    		InOutDRV_Main( void);
byte                    		InOutDRV_GetStatus( byte  Group);
void                    		InOutDRV_SetStatus( byte   Output, bool   Mode);
/*----------------------------------------------------------------------------*/

