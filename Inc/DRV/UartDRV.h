/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define  COM_VOID             0  // COM port is undefined (not assigned)
#define  COM_1                1  // Internal COM 1
#define  COM_2                2  // Internal COM 2
#define  COM_3                3  // Internal COM 3 (reserved for ATmega2560)
#define  COM_4                4  // Internal COM 4 (reserved for ATmega2560)
#define  COM_5                5  // Exar A
#define  COM_6                6  // Exar B
#define  COM_7                7  // Exar C
#define  COM_8                8  // Exar D

#define  EXAR_A               COM_5
#define  EXAR_B               COM_6
#define  EXAR_C               COM_7
#define  EXAR_D               COM_8

#define  BAUD_RATE_1200       0
#define  BAUD_RATE_2400       1
#define  BAUD_RATE_9600       2
#define  BAUD_RATE_19200      3
#define  BAUD_RATE_38400      4
#define  BAUD_RATE_57600      5
#define  BAUD_RATE_115200     6

#define  DATA_BITS_8          0
#define  DATA_BITS_7          1
#define  DATA_BITS_6          2
#define  DATA_BITS_5          3

#define  PARITY_NONE          0
#define  PARITY_ODD           1
#define  PARITY_EVEN          2

#define  STOP_BITS_1          0
#define  STOP_BITS_2          1

/*  DEBUG LINES  */
#define DEBUG_LINE_OFF          (UartDRV_SetDTR(COM_6, ON))
#define DEBUG_LINE_ON           (UartDRV_SetDTR(COM_6, OFF))

/*---------------------------- Typedef Directives ----------------------------*/
typedef  struct
{
   byte        Port;
   byte        BaudRate;
   byte        DataBits;
   byte        Parity;
   byte        StopBits;
   byte        RS485_Address; // Used by the Application
}  tComPort;


typedef  struct
{
   tComPort    Com;
   void        (*TxFunc)( void);             // API Tx function
   void        (*RxFunc)( void);             // API Rx function
   void        (*Uart_TxByte)( byte);        // DRV Tx function
   void        (*Uart_TxEnd)( void);         // DRV Tx-End function
   byte        (*Uart_RxByte)( void);        // DRV Rx function
   void        (*Uart_RS485_Enable)( void);  // DRV RS485 Enable function
   void        (*Uart_RS485_Disable)( void); // DRV RS485 Disable function
}  tUart;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		UartDRV_Init( void);
void                    		UartDRV_SetPort( tUart  *pUart);

void                          UartDRV_COM1_RS485_Enable( void);
void                          UartDRV_COM1_RS485_Disable( void);
void                          UartDRV_COM1_TxByte( byte Data);
void                          UartDRV_COM1_TxEnd( void);
byte                          UartDRV_COM1_RxByte( void);
void                          UartDRV_COM1_SetModeBit( bool Mode);
bool                          UartDRV_COM1_GetModeBit( void);
void                          UartDRV_COM1_DisableTxInt( void);

void                          UartDRV_COM2_RS485_Enable( void);
void                          UartDRV_COM2_RS485_Disable( void);
void                          UartDRV_COM2_TxByte( byte Data);
void                          UartDRV_COM2_TxEnd( void);
byte                          UartDRV_COM2_RxByte( void);
void                          UartDRV_COM2_SetModeBit( bool Mode);
bool                          UartDRV_COM2_GetModeBit( void);
void                          UartDRV_COM2_DisableTxInt( void);

#ifdef   __IO2560_H
void                          UartDRV_COM3_RS485_Enable( void);
void                          UartDRV_COM3_RS485_Disable( void);
void                          UartDRV_COM3_TxByte( byte Data);
void                          UartDRV_COM3_TxEnd( void);
byte                          UartDRV_COM3_RxByte( void);
void                          UartDRV_COM3_SetModeBit( bool Mode);
bool                          UartDRV_COM3_GetModeBit( void);
void                          UartDRV_COM3_DisableTxInt( void);

void                          UartDRV_COM4_RS485_Enable( void);
void                          UartDRV_COM4_RS485_Disable( void);
void                          UartDRV_COM4_TxByte( byte Data);
void                          UartDRV_COM4_TxEnd( void);
byte                          UartDRV_COM4_RxByte( void);
void                          UartDRV_COM4_SetModeBit( bool Mode);
bool                          UartDRV_COM4_GetModeBit( void);
void                          UartDRV_COM4_DisableTxInt( void);
#endif

void                          UartDRV_COM5_RS485_Enable( void);
void                          UartDRV_COM5_RS485_Disable( void);
void                          UartDRV_COM5_TxByte( byte Data);
void                          UartDRV_COM5_TxEnd( void);
byte                          UartDRV_COM5_RxByte( void);

void                          UartDRV_COM6_RS485_Enable( void);
void                          UartDRV_COM6_RS485_Disable( void);
void                          UartDRV_COM6_TxByte( byte Data);
void                          UartDRV_COM6_TxEnd( void);
byte                          UartDRV_COM6_RxByte( void);

void                          UartDRV_COM7_RS485_Enable( void);
void                          UartDRV_COM7_RS485_Disable( void);
void                          UartDRV_COM7_TxByte( byte Data);
void                          UartDRV_COM7_TxEnd( void);
byte                          UartDRV_COM7_RxByte( void);

void                          UartDRV_COM8_RS485_Enable( void);
void                          UartDRV_COM8_RS485_Disable( void);
void                          UartDRV_COM8_TxByte( byte Data);
void                          UartDRV_COM8_TxEnd( void);
byte                          UartDRV_COM8_RxByte( void);

void                          UartDRV_SetDTR( byte Port, bool Mode);
bool                          UartDRV_GetDTR( byte Port);
void                          UartDRV_SetRTS( byte Port, bool Mode);
bool                          UartDRV_GetRTS( byte Port);
bool                          UartDRV_GetDSR( byte Port);
bool                          UartDRV_GetCTS( byte Port);
bool                          UartDRV_GetDCD( byte Port);
bool                          UartDRV_GetRI ( byte Port);
/*----------------------------------------------------------------------------*/









