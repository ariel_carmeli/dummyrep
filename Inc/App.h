/*------------------------------ Include Files -------------------------------*/
#include "IAR.h"
#include "Atml128.h"
#include "C-Stad.h"
#include "Typedef.h"
#include "All_Drv.h"
#include "All_Api.h"
#include "All_Task.h"
/*------------------------ Precompilation Definitions ------------------------*/
#define  LANGUAGE_ENGLISH              0
#define  LANGUAGE_HEBREW               1
// #define  LANGUAGE_ARABIC               2  // don't change MAX_LANGUAGES to (3) !
#define	MAX_LANGUAGES                 2
// #define  MAX_LANGUAGES_EXTENDED        3
#define  MAX_LANGUAGES_EXTENDED        2

#define  EQUIP_TYPE_CT_20_PARK         '2'
#define  EQUIP_TYPE_MPS_30_MIKVE       '5'
/*---------------------------- Typedef Directives ----------------------------*/
typedef enum
{
   TASK_APPL      ,  //	Task Application
   TASK_HW        ,  //	Task Hardware (LCD, LED, Relay, DIP Switch etc.)
   TASK_INIT	  ,  // Task Initialization
   TASK_LAN       ,  //	Task LAN (RS485 network [Master / Slave(s)] communication)
   TASK_LOG       ,  // Task Logger
   TASK_PARAMS    ,  // Task Parameters
   TASK_PRINTER   ,  // Task Printer
   TASK_SERVER    ,  //	Task Server
   TASK_SETUP     ,  // Task Setup
   TASK_TIMER	  ,  //	Task Timer
   TASK_DISPLAY   ,  //	Task TFT Display
   TASK_B2B       ,  //	Task B2B (Board-to-Board [Master / Slave(s)] communication)
   TASK_HOPPER    ,  //	Task Hoppers
   TASK_LIB       ,  // Task Lib
   TASK_TIBAPAY   ,  // Task TibaPay Server (A.C).
	/********************/
   MAX_TASKS
}  tTask;


// Application Events
#define  EV_APP_VOID                                  0L

// Events from Task Application
#define  EV_APP_APPL_DOOR_LOCK_OPENED                 0x00000001L // TASK_APP: Door's main-lock was opened
#define  EV_APP_APPL_DOOR_LOCK_CLOSED                 0x00000002L // TASK_APP: Door's main-lock was closed
#define  EV_APP_APPL_ENABLE_COIN_VALID                0x00000004L // TASK_APP:
#define  EV_APP_APPL_DISABLE_COIN_VALID               0x00000008L // TASK_APP:
#define  EV_APP_APPL_ENABLE_BILL_VALID                0x00000010L // TASK_APP:
#define  EV_APP_APPL_DISABLE_BILL_VALID               0x00000020L // TASK_APP:
#define  EV_APP_APPL_STACK_BILL                       0x00000040L // TASK_APP:
#define  EV_APP_APPL_REJECT_BILL                      0x00000080L // TASK_APP:
#define  EV_APP_APPL_DISPENSE_CHANGE                  0x00000100L // TASK_APP:
#define  EV_APP_APPL_CANCEL_TRANSACTION               0x00000200L // TASK_APP:
#define  EV_APP_APPL_RFID_CONNECTED                   0x00000400L // TASK_APP:
#define  EV_APP_APPL_RFID_DISCONNECTED                0x00000800L // TASK_APP:
#define  EV_APP_APPL_EMERGENCY_CREDIT                 0x00001000L // TASK_APP: Request to send an Emergency packet type Credit Card to the Server
#define  EV_APP_APPL_EMERGENCY_DIGITEL_AUTHENTICATION 0x00002000L // TASK_APP:
#define  EV_APP_APPL_REBOOT                           0x00004000L // TASK_APP:
#define  EV_APP_APPL_NEW_MACHINE_ID                   0x00008000L // TASK_APP:
#define  EV_APP_APPL_REQ_TO_ERASE_ALL_DATA            0x00010000L // TASK_APP:
#define  EV_APP_APPL_REQ_TO_ERASE_ALL_PARAMS          0x00020000L // TASK_APP:
#define  EV_APP_APPL_COIN_DETECTED                    0x00040000L // TASK_APP: Test mode: Coin is detected (for Task Setup)

// Events from Task Board-to-Board
#define  EV_APP_B2B_RX_MSG                            0x00000001L // Task B2B:   New message was received
#define  EV_APP_B2B_RX_BAR_CODE_LABEL_1               0x00000002L // Task B2B:   Bar-Code Reader #1: Received label
#define  EV_APP_B2B_RX_BILL_VALUE                     0x00000004L // Task B2B:   Detected Bill's value
#define  EV_APP_B2B_RX_COIN_VALUE                     0x00000008L // Task B2B:   Detected Coin's value

// Events from Task Setup
#define  EV_APP_SETUP_ACTIVE                          0x00000001L // TASK_SETUP: Task is running
#define  EV_APP_SETUP_BUSY                            0x00000002L // TASK_SETUP: Reading / Writing data into Esqr
#define  EV_APP_SETUP_DATA_UPDATED                    0x00000004L // TASK_SETUP: Data was updated
#define  EV_APP_SETUP_TEST                            0x00000008L // TASK_SETUP: Perform a mechanical test
#define  EV_APP_SETUP_REPORT                          0x00000010L // TASK_SETUP: Print/Display a report
#define  EV_APP_SETUP_INACTIVE                        0x00000020L // TASK_SETUP: Task is not (or stopped) running
#define  EV_APP_SETUP_CLEAR_SALES                     0x00000040L // TASK_SETUP: erase partially sales database
#define  EV_APP_SETUP_RESET_SALES                     0x00000080L // TASK_SETUP: Reset sales database
#define  EV_APP_SETUP_RESET_INFO                      0x00000100L // TASK_SETUP: Reset info structures
#define  EV_APP_SETUP_START_REFILLING                 0x00000200L // TASK_SETUP: Start refilling coins
#define  EV_APP_SETUP_STOP_REFILLING                  0x00000400L // TASK_SETUP: End refilling coins
#define  EV_APP_SETUP_COINS_EMPTYING                  0x00000800L // TASK_SETUP: Emptying coins from a Hopper
#define  EV_APP_SETUP_STOP_TESTING                    0x00001000L // TASK_SETUP: Stop testing (any test)
#define  EV_APP_SETUP_TEST_PRINTER                    0x00002000L // TASK_SETUP: Test printer (print test page - ESC 'I')
#define  EV_APP_SETUP_ADVANCE_Z_NUM                   0x00004000L // TASK_SETUP: For Task Printer: Advance Z number
#define  EV_APP_SETUP_TEST_COIN_ROUTING               0x00008000L // TASK_SETUP: Test coin routing
#define  EV_APP_SETUP_SET_DEFAULT_DEBUG               0x00010000L // TASK_SETUP: Set Default Values + Debug

// Events from Task Hardware
#define  EV_APP_HW_DOOR_LOCK_OPENED                   0x00000001L // TASK_HW: Door's main-lock was opened
#define  EV_APP_HW_DOOR_LOCK_CLOSED                   0x00000002L // TASK_HW: Door's main-lock was closed
#define  EV_APP_HW_SAFE_REMOVED                       0x00000004L // TASK_HW: Coin-safe was removed
#define  EV_APP_HW_SAFE_RETURNED                      0x00000008L // TASK_HW: Coin-safe was returned back

// Events From and To Task Printer
#define  EV_APP_PRN_ERROR                             0x00000001L // TASK_PRINTER: Printer Error
#define  EV_APP_PRN_END_PRINTING_JOB                  0x00000002L // TASK_PRINTER: End of printing job
#define  EV_APP_PRN_PRINT_RPRT_DOOR_OPEN              0x00000004L // TASK_PRINTER: Print report: Door is Opened
#define  EV_APP_PRN_PRINT_RPRT_DOOR_CLOSE             0x00000008L // TASK_PRINTER: Print report: Door is Closed
#define  EV_APP_PRN_PRINT_RECEIPT                     0x00000010L // TASK_PRINTER: Print receipt
#define  EV_APP_PRN_PRINT_RECEIPT_ATTACHMENT          0x00000020L // TASK_PRINTER: Print receipt attachment
#define  EV_APP_PRN_PRINT_REPORT_SALES                0x00000040L // TASK_PRINTER: Print Sales report
#define  EV_APP_PRN_PRINT_REPORT_FISCAL               0x00000080L // TASK_PRINTER: Print Fiscal report
#define  EV_APP_PRN_PRINT_EVENTS                      0x00000100L // TASK_PRINTER: Print Log Events report
#define  EV_APP_PRN_PRINT_REPORT_Z                    0x00000200L // TASK_PRINTER: Print Z Report
#define  EV_APP_PRN_PRINT_SETUP_INFO                  0x00000400L // TASK_PRINTER: Print Setup info
#define  EV_APP_PRN_PRINT_DEVICES_STATUS              0x00000800L // TASK_PRINTER: Print status of devices (peripherals)
#define  EV_APP_PRN_PRINT_CC_CANCELLATION             0x00001000L // TASK_PRINTER: Print Credit-Card transaction cancellation
#define  EV_APP_PRN_PRINT_COIN_ROUTING                0x00002000L // TASK_PRINTER: Print Coin Routing Events
#define  EV_APP_PRN_CC_RESPONSE                       0x00004000L // TASK_PRINTER: A response came from Credit-Card clearing server
#define  EV_APP_PRN_ALL_BITS                          0x0000FFFCL // TASK_PRINTER: All Bits

// Events From Task Coin
#define  EV_APP_COIN_WAS_INSERTED                     0x00000001L // TASK_COIN: Coin Was Inserted (for on-screen report)

// Events to Task Log
#define  EV_APP_LOG_RESET_INFO                        0x00000001L // TASK_LOG:   Reset info structures

// Events from Clock API
#define  EV_APP_TIMER_TICK_SECOND                     0x00000001L // ClockAPI (T_Timer):  1 second elapsed since last RTC readout
#define  EV_APP_TIMER_TICK_MINUTE                     0x00000002L // ClockAPI (T_Timer):  1 minute elapsed since last RTC readout
#define  EV_APP_TIMER_TICK_HOUR                       0x00000004L // ClockAPI (T_Timer):  1 hour   elapsed since last RTC readout
#define  EV_APP_TIMER_TICK_DAY                        0x00000008L // ClockAPI (T_Timer):  1 day    elapsed since last RTC readout
#define  EV_APP_TIMER_TICK_MONTH                      0x00000010L // ClockAPI (T_Timer):  1 month  elapsed since last RTC readout
#define  EV_APP_TIMER_TICK_YEAR                       0x00000020L // ClockAPI (T_Timer):  1 year   elapsed since last RTC readout
#define  EV_APP_TIMER_SET_NEW_CLOCK                   0x00000040L // ClockAPI (T_Timer):  Date/Time were changed

// Events to Task Params
#define  EV_APP_PARAMS_FORMAT_ACTIVE                  0x00000001L // TASK_PARAMS:
#define  EV_APP_PARAMS_FORMAT_END                     0x00000002L // TASK_PARAMS:

// Events From Task Server
#define  EV_APP_SERVER_READ_PARAMS                    0x00000001L // TASK_SERVER:
#define  EV_APP_SERVER_DATE_TIME_UPDATED              0x00000002L // TASK_SERVER:
#define  EV_APP_SERVER_void                           0x00000004L // TASK_SERVER:
#define  EV_APP_SERVER_ACTIVE                         0x00000008L // TASK_SERVER:
#define  EV_APP_SERVER_RX_INIT_CMD                    0x00000010L // TASK_SERVER:
#define  EV_APP_SERVER_RESET_V_POINTERS               0x00000020L // TASK_SERVER:
#define  EV_APP_SERVER_RESET_L_POINTERS               0x00000040L // TASK_SERVER:
#define  EV_APP_SERVER_RESET_COIN_ROUTING             0x00000080L // TASK_SERVER:
#define  EV_APP_SERVER_RESET_Z_POINTERS               0x00000100L // TASK_SERVER:

// Events From Task LAN
#define  EV_APP_LAN_GATE_1_OPENED                     0x00000001L // TASK_LAN: Gate 1 is Opened
#define  EV_APP_LAN_GATE_1_CLOSED                     0x00000002L // TASK_LAN: Gate 1 is Closed
#define  EV_APP_LAN_GATE_2_OPENED                     0x00000004L // TASK_LAN: Gate 2 is Opened
#define  EV_APP_LAN_GATE_2_CLOSED                     0x00000008L // TASK_LAN: Gate 2 is Closed

// Events To Task LOG
#define  EV_APP_LOG_RESET_V_POINTERS                  0x00000001L // TASK_LOG:
#define  EV_APP_LOG_RESET_L_POINTERS                  0x00000002L // TASK_LOG:
#define  EV_APP_LOG_RESET_COIN_ROUTING                0x00000004L // TASK_LOG:
#define  EV_APP_LOG_RESET_Z_POINTERS                  0x00000008L // TASK_LOG:

// Events From task LIB
#define  EV_APP_LIB_ERROR                             0x00000001L // T_LIB:  A Library error was detected by Task_Lib_Main(). 


// Events From task TibaPay
#define EV_APP_TIBAPAY_READ_PARAMS                     0x00000001L // T_TibaPay 
#define EV_APP_TIBAPAY_DATE_TIME_UPDTD                 0x00000002L // T_TibaPay 

#define EV_APP_TIBAPAY_ACTIVE                          0x00000008L // T_TibaPay 
#define EV_APP_TIBAPAY_RX_INIT_CMD                     0x00000010L // T_TibaPay 
#define EV_APP_TIBAPAY_RESET_V_POINTERS                0x00000020L // T_TibaPay 
#define EV_APP_TIBAPAY_RESET_L_POINTERS                0x00000040L // T_TibaPay 

#define EV_APP_TIBAPAY_RESET_Z_POINTERS                0x00000100L // T_TibaPay 
#define EV_APP_TIBAPAY_DEL_CRNT_Z_RECORD               0x00000200L // T_TibaPay 

#define EV_APP_TIBAPAY_MISMATCH_MACHINE_ID             0x00001000L // T_TibaPay 
#define EV_APP_TIBAPAY_RESET_VITAL_PARAMS              0x00002000L // T_TibaPay 
#define EV_APP_TIBAPAY_RESET_DEBT                      0x00004000L // T_TibaPay 
#define EV_APP_TIBAPAY_COMM_LOSS                       0x00010000L // T_TibaPay
#define EV_APP_TIBAPAY_COMM_BACK                       0x00020000L // T_TibaPay


//
/*---------------------------- External Variables ----------------------------*/
extern	byte                 App_TaskStatus[MAX_TASKS];
extern	ulong                App_TaskEvent[MAX_TASKS];
extern	byte                 App_Language;
extern	byte                 App_ResetGuard;
extern	CONST char           App_Code[];
extern	CONST char           App_Version[];
//extern	CONST char           App_Date[];
extern   char                 App_Date[9];   // DD/MM/YY
extern	bool                 App_fReady;
extern	usint                App_Version_Local;   // For Server application
extern	usint                App_Version_Server;  // For Server application
extern   char                 App_EquipmentType;   // For Server application
/*------------------------ Global Function Prototypes ------------------------*/
/*----------------------------------------------------------------------------*/




