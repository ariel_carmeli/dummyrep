/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   // Once you add more types here - change also <Addresses> and <StructSize> enums in xSramAPI.c
   xSRAM_TYPE_SERVER_PARAMS      ,  // Task Params (Parameters - type C [ulong])
   xSRAM_TYPE_SERVER_STR_32      ,  // Task Params (Parameters - type E [32-chars strings])
   xSRAM_TYPE_SERVER_VARS        ,  // Task Params (Variables)
   xSRAM_TYPE_SALES_INFO             ,  // Task Application
   xSRAM_TYPE_COINS_INFO             ,  // Task Application
   xSRAM_TYPE_BILLS_INFO             ,  // Task Application
   xSRAM_TYPE_HOPPERS_INFO           ,  // Task Hoppers
   xSRAM_TYPE_PRINTER_INFO           ,  // Task Printer
   xSRAM_TYPE_LOG_EVENT_INFO         ,  // Task Error Logger (Events)
   xSRAM_TYPE_LOG_EVENT_REC          ,  // Task Error Logger (Events)
   xSRAM_TYPE_LOG_TRANS_INFO         ,  // Task Error Logger (Transactions)
   xSRAM_TYPE_LOG_TRANS_REC          ,  // Task Error Logger (Transactions)
   xSRAM_TYPE_LOG_COIN_ROUTING_INFO  ,  // Task Error Logger (Coin Routing)
   xSRAM_TYPE_LOG_COIN_ROUTING_REC   ,  // Task Error Logger (Coin Routing)
   xSRAM_TYPE_LOG_EMERG_INFO     ,  // Task Log (Emergencies)
   xSRAM_TYPE_LOG_Z_INFO         ,  // Task Log (Z Records)
   xSRAM_TYPE_LOG_Z_REC          ,  // Task Log (Z Records)
   /******************/
   xSRAM_MAX_TYPES               // !!! MUST BE THE LAST DEFINITION !!!
}  tSram_DataType;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		xSramAPI_Init           ( void);
void                    		xSramAPI_Main           ( void);
bool                    		xSramAPI_IsBusy         ( void);
void                    		xSramAPI_Read           ( void  *Buffer, usint Size, tSram_DataType   Type, usint Idx, bool  fBlockingMode);
void                    		xSramAPI_Read_Address   ( void  *Buffer, usint Size, ulong   Address, bool  fBlockingMode);
void                    		xSramAPI_Write          ( void  *Buffer, usint Size, tSram_DataType  Type, usint Idx, bool  fBlockingMode);
void                          xSramAPI_Write_Address  ( void  *Buffer, usint Size, ulong  Address, bool  fBlockingMode);
/*----------------------------------------------------------------------------*/





