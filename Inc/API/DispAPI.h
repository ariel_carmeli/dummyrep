/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define  TFT_DISPLAY_BUF_SIZE             0x300 // see T_App.h

#define	LINE1						            0
#define	LINE2						            1
#define	LINE3						            2
#define	LINE4						            3
#define	LINE5						            4
#define	LINE6						            5
#define	LINE7						            6
#define	LINE8						            7
#define	LINE9						            8
#define	LINE10					            9
#define	LINE11					            10
#define	LINE12					            11
#define	LINE13					            12
#define	LINE14					            13
#define	LINE15					            14

#define  VGA_FONT_STYLE_BOLD              0
#define  VGA_FONT_STYLE_NORMAL            BIT_0
#define  VGA_FONT_STYLE_ITALIC            BIT_1
#define  VGA_FONT_STYLE_UNDERLINE         BIT_2
#define  VGA_FONT_STYLE_STRIKETHROUGH     BIT_3

#define  VGA_ACTION_VOID                  0
#define  VGA_ACTION_DISABLED              BIT_0
#define  VGA_ACTION_INVISIBLE             BIT_1

// Definitions for 640x480 TFT display
#define  VGA_MAX_X                        640
#define  VGA_MAX_Y                        480

#define  VGA_FONT_2_MAX_COLUMNS           32
#define  VGA_FONT_2_MAX_LINES             15
#define  VGA_FONT_2_DELTA_COL             (VGA_MAX_X / VGA_FONT_2_MAX_COLUMNS)
#define  VGA_FONT_2_DELTA_ROW             (VGA_MAX_Y / VGA_FONT_2_MAX_LINES)

#define  VGA_FONT_3_MAX_COLUMNS           22
#define  VGA_FONT_3_MAX_LINES             8
#define  VGA_FONT_3_DELTA_COL             (VGA_MAX_X / VGA_FONT_3_MAX_COLUMNS)
#define  VGA_FONT_3_DELTA_ROW             (VGA_MAX_Y / VGA_FONT_3_MAX_LINES)

#define  VGA_FONT_4_MAX_COLUMNS           22
#define  VGA_FONT_4_MAX_LINES             8
#define  VGA_FONT_4_DELTA_COL             (VGA_MAX_X / VGA_FONT_4_MAX_COLUMNS)
#define  VGA_FONT_4_DELTA_ROW             (VGA_MAX_Y / VGA_FONT_4_MAX_LINES)

#define  VGA_FONT_5_MAX_COLUMNS           17
#define  VGA_FONT_5_MAX_LINES             8
#define  VGA_FONT_5_DELTA_COL             (VGA_MAX_X / VGA_FONT_5_MAX_COLUMNS)
#define  VGA_FONT_5_DELTA_ROW             (VGA_MAX_Y / VGA_FONT_5_MAX_LINES)

#define  VGA_FONT_6_MAX_COLUMNS           14
#define  VGA_FONT_6_MAX_LINES             8
#define  VGA_FONT_6_DELTA_COL             (VGA_MAX_X / VGA_FONT_6_MAX_COLUMNS)
#define  VGA_FONT_6_DELTA_ROW             (VGA_MAX_Y / VGA_FONT_6_MAX_LINES)

#define  VGA_FONT_7_MAX_COLUMNS           11
#define  VGA_FONT_7_MAX_LINES             6
#define  VGA_FONT_7_DELTA_COL             (VGA_MAX_X / VGA_FONT_7_MAX_COLUMNS)
#define  VGA_FONT_7_DELTA_ROW             (VGA_MAX_Y / VGA_FONT_7_MAX_LINES)
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   VGA_OBJECT_INIT               ,  // Init screen
   VGA_OBJECT_FONT               ,  // Font
   VGA_OBJECT_COLOR              ,  // Color
   VGA_OBJECT_ALIGNMENT          ,  // Alignment
   VGA_OBJECT_RECTANGLE          ,  // Rectangle
   VGA_OBJECT_TEXT               ,  // Text
   VGA_OBJECT_GOTO               ,  // Goto( x, y)
}  tVGA_Object;


typedef  enum
{
   VGA_FONT_NAME_ARIAL           ,  // 0  Arial
   VGA_FONT_NAME_TIMES           ,  // 1  Times New Roman
   VGA_FONT_NAME_DAVID           ,  // 2  David
   VGA_FONT_NAME_MIRIAM          ,  // 3  Miriam
   /******************/
   VGA_FONT_NAME_LAST
}  tVGA_FontName;


typedef  enum
{
   VGA_FONT_SIZE_2         =  2  ,  // 2  S
   VGA_FONT_SIZE_3               ,  // 3  M
   VGA_FONT_SIZE_4               ,  // 4  L
   VGA_FONT_SIZE_5               ,  // 5  XL
   VGA_FONT_SIZE_6               ,  // 6  XXL
   VGA_FONT_SIZE_7               ,  // 7  XXXL
   /******************/
   VGA_FONT_SIZE_LAST
}  tVGA_FontSize;


typedef  enum
{
   VGA_COLOR_BLACK               ,  // 0
   VGA_COLOR_DARK_BLUE           ,  // 1
   VGA_COLOR_LIGHT_GREEN         ,  // 2
   VGA_COLOR_CYAN                ,  // 3
   VGA_COLOR_LIGHT_RED           ,  // 4
   VGA_COLOR_ORANGE              ,  // 5
   VGA_COLOR_PEACH               ,  // 6
   VGA_COLOR_LIGHT_GREY          ,  // 7
   VGA_COLOR_DARK_GREY           ,  // 8
   VGA_COLOR_DARK_BLUE_2         ,  // 9
   VGA_COLOR_LIGHT_GREEN_2       ,  // 10
   VGA_COLOR_CYAN_2              ,  // 11
   VGA_COLOR_LIGHT_RED_2         ,  // 12
   VGA_COLOR_MAGENTA             ,  // 13
   VGA_COLOR_YELLOW              ,  // 14
   VGA_COLOR_WHITE               ,  // 15
}  tVGA_Color;


typedef  enum
{
   VGA_ALIGNMENT_VOID            ,  // 0
   VGA_ALIGNMENT_LEFT            ,  // 1
   VGA_ALIGNMENT_CENTER          ,  // 2
   VGA_ALIGNMENT_RIGHT           ,  // 3
}  tVGA_Alignment;


typedef  enum
{
   VGA_LANGUAGE_VOID             ,  // 0
   VGA_LANGUAGE_HEBREW           ,  // 1
   VGA_LANGUAGE_ENGLISH          ,  // 2
}  tVGA_Language;


typedef  struct
{
   tVGA_FontName        Name;
   byte                 Style;      // VGA_FONT_STYLE_???
   tVGA_FontSize        Size;       // VGA_FONT_SIZE_???
   byte                 Color;      // Font Color
   byte                 Charset;    // Charset
}  tVGA_Font;


typedef  struct
{
   tVGA_Object          Object;     // Object
   tVGA_Font            Font;       // Individual settings for the font
   usint                X;          // X Coordination
   usint                Y;          // Y Coordination
   usint                Height;     // Height
   usint                Width;      // Width
   byte                 Line;       // Text-line
   byte                 Offset;     // Text-column
   byte                 Action;     // VGA_ACTION_???
   tVGA_Alignment       Alignment;  // VGA_ALIGNMENT_???
   byte                 Color_FG;   // Foreground Color
   byte                 Color_BG;   // Background Color
   char                *pText;      // Text to Display
}  tVGA_ObjectRec;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		DisplayAPI_Init         ( byte ComPort);
void                    		DisplayAPI_Main         ( void);
bool                          DisplayAPI_IsBusy       ( void);
void                    		DisplayAPI_AddOn_Object ( bool   fMode);
void                          DisplayAPI_SetObject    ( tVGA_ObjectRec *pObject);
void                    		DisplayAPI_Run          ( void);
void                    		DisplayAPI_Tx_ISR       ( void);
/*----------------------------------------------------------------------------*/





