/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define  B2B_MAX_LEN_BUFFER            64
#define  LAST_PACKET                   BIT_7 // 0x80
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   B2B_CMD_VOID		            =	0,
   B2B_CMD_ACK            	      =  'A',  // ACK   (sent from Slave only)
   B2B_CMD_NACK           	      =  'N',  // NACK  (sent from Slave only)
   B2B_CMD_BAR_CODE_READER       =  'B',  // Bar-Code Reader (Swallow)
   B2B_CMD_CLOCK                 =  'C',  // Set Clock (Date and Time)
   B2B_CMD_DEFAULT_PARAMS        =  'D',  // Default Parameters
   B2B_CMD_BILL_VALIDATOR        =  'G',  // Bill Validator (GPT)
   B2B_CMD_LCD                   =  'L',  // LCD
   B2B_CMD_MGC                   =  'M',  // MGC (Magnetic card Reader)
   B2B_CMD_PRINTER               =  'P',  // Printer
   B2B_CMD_RELAY                 =  'R',  // Relays
   B2B_CMD_GET_STATUS            =  'S',  // Get Status
   B2B_CMD_LED                   =  'T',  // LED
   B2B_CMD_COIN_VALIDATOR        =  'V',  // Coin Validator (T-12 / T-15)
   B2B_CMD_BUZZER                =  'Z',  // Buzzer
   /******************/
   B2B_CMD_LAST                  =  0xF0
}  tB2BCmd;



typedef  struct
{
   bool              fFromMaster;
   byte              UnitType;
   byte              Address;
   byte              Command; // Either 'tMasterCmd' or 'tSlaveCmd'
   byte              Sub_Command;
   byte              Index;   // Distinguish between different devices of the same type; Packet number (for Printer)
   byte             *pBuffer;
   usint             BufSize;
}  tB2BPacket;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		B2BAPI_Init( byte ComPort);
void                    		B2BAPI_Main( void);
bool                    		B2BAPI_IsBusy( void);                        // Check if communication channel is busy
void                          B2BAPI_SendData( tB2BPacket   *pTxPacket);   // Send packet
bool                    		B2BAPI_IsRxNewPacket( void);                 // Check if new packet has been received
tB2BPacket                   *B2BAPI_GetData( void);                       // Receive packet
void                    		B2BAPI_Tx_ISR( void);                        // Interrupt routine (Tx)
void                    		B2BAPI_Rx_ISR( void);                        // Interrupt routine (Rx)
/*----------------------------------------------------------------------------*/

