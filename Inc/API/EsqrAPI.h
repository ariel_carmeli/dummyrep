/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum
{
   ESQR_TYPE_VOID       =  0,
   // Once you add more types here - change also <Addresses> and <StructSize> enums in EsqrAPI.c
   /******************/
   ESQR_MAX_TYPES                // !!! MUST BE THE LAST DEFINITION !!!
}  tEsqr_DataType;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		EsqrAPI_Init( void);
void                    		EsqrAPI_Main( void);
bool                    		EsqrAPI_IsBusy( void);
void                    		EsqrAPI_Read( void  *Buffer, usint Size, tEsqr_DataType   Type, usint Idx, bool  fBlockingMode);
void                    		EsqrAPI_Read_Address( void  *Buffer, usint Size, ulong   Address, bool  fBlockingMode);
void                    		EsqrAPI_Write( void  *Buffer, usint Size, tEsqr_DataType  Type, usint Idx, bool  fBlockingMode);
void                          EsqrAPI_Write_Address( void  *Buffer, usint Size, ulong  Address, bool  fBlockingMode);
/*----------------------------------------------------------------------------*/





