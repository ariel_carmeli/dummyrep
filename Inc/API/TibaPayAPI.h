/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define  TIBAPAY_MAX_LEN_BUFFER         204
#define  SERVER_DELIMITER              '_'
/*---------------------------- Typedef Directives ----------------------------*/
#if 1
typedef  enum
{
   TP_SERVER_CMD_VOID               =  0     ,
   TP_SERVER_CMD_ACK                =  '.'   ,
   TP_SERVER_CMD_NACK               =  '*'   ,
   TP_SERVER_CMD_DO_ACTION          =  'D'   ,  // Do some kind of an action
   TP_SERVER_CMD_INC_EMERGENCY      =  'E'   ,  // Increment Emergency pointer
   TP_SERVER_CMD_FORMAT             =  'F'   ,  // Database format (used by me only. not supported by the server)
   TP_SERVER_CMD_GET_STATUS         =  'L'   ,  // Get Status (req. for transactions / Events / Status)
   TP_SERVER_CMD_READ_TRANS         =  'l'   ,  // Read transactions
   TP_SERVER_CMD_PARAMS_READ        =  'R'   ,  // Read parameters
   TP_SERVER_CMD_SET_TIME_DATE      =  'T'   ,  // Set Time & Date
   TP_SERVER_CMD_EVENT_CONFIRM      =  'V'   ,  // Event confirmation
   TP_SERVER_CMD_PARAMS_WRITE       =  'W'   ,  // Write parameters
   TP_SERVER_CMD_END_Z_SESSION      =  'Y'   ,  // Terminate Z session and start a new one
   TP_SERVER_CMD_ID                 =  'Z'   ,  // ID params of the Server (master)
   /******************/
   TP_SERVER_CMD_LAST
}  tTP_ServerCmd;
#endif

#if 0
typedef  enum
{
   SERVER_REPLY_VOID             =  0     ,
   SERVER_REPLY_ACK              =  '.'   ,
   SERVER_REPLY_NACK             =  '*'   ,
   SERVER_REPLY_EMERGENCY        =  'E'   ,  // Emergency   (reply to 'L' or 'l')
   SERVER_REPLY_TANSACTION       =  'L'   ,  // Transaction (reply to 'L' or 'l')
   SERVER_REPLY_ENFORCE_Z_CMD    =  'P'   ,  // Enforce server to send 'Z' command (init)
   SERVER_REPLY_STATUS           =  'Q'   ,  // Status      (reply to 'L' or 'l')
   SERVER_REPLY_PARAMS_READ      =  'R'   ,  // Read parameters
   SERVER_REPLY_EVENT            =  'V'   ,  // Event       (reply to 'L' or 'l')
   SERVER_REPLY_PARAMS_WRITE     =  'W'   ,  // Write parameters
   SERVER_REPLY_ID               =  'Z'   ,  // ID params of local controller (slave)
   /******************/
   SERVER_REPLY_CMD_LAST
}  tServerReplyCmd;
#endif

#if 0
typedef  struct
{
   bool              fFromMaster;
   byte              UnitType;
   usint             Address;
   byte              Command; // either 'tServerCmd' or 'tServerReplyCmd'
   byte              Sub_Command;
   byte              Index;   // Distinguish between different devices of the same type; Packet number (for Printer)
   byte             *pBuffer;
   usint             BufSize;
   usint             Idx_CtrlChar_Underscore;
}  tServerPacket;
#endif
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		TibaPayAPI_Init( byte ComPort);
void                    		TibaPayAPI_Main( void);
void                    		TibaPayAPI_AssignPort( byte ComPort);
bool                    		TibaPayAPI_IsBusy( void);                           // Check if communication channel is busy
void                                    TibaPayAPI_SendData( tServerPacket   *pTxPacket);   // Send packet
bool                    		TibaPayAPI_IsRxNewPacket( void);                    // Check if new packet has been received
tServerPacket                           *TibaPayAPI_GetData( void);                          // Receive packet
void                    		TibaPayAPI_Tx_ISR( void);                           // Interrupt routine (Tx)
void                    		TibaPayAPI_Rx_ISR( void);                           // Interrupt routine (Rx)
/*----------------------------------------------------------------------------*/
void TibaPay_Test(void);
