/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
extern char *pDbgPrintBuff;
/*------------------------ Global Function Prototypes ------------------------*/
void                            Debug_Start ( byte ComPort);
void                            Debug_Main( void);
void                            Debug_Stop  ( void);
void                            Debug_PrintHex(byte* pBuff, byte length);
void                            Debug_StrPrint_Const( PGM_P pStrConst);
/*
*                  Debug_PrintTextParam
*
* Print the inserted text. The, print the inserted number.
* Number can be a byte, usint or ulong (NumberSize = 1,2 or 4)
* Print format can be Decimal or Hex ( format = 0 or 1 ).
*/
/*----------------------------------------------------------------------------*/
void                            Debug_PrintTextParam(char* text, byte NumberSize, void* pNumber, byte format );
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/



