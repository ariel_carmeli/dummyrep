/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define	T_HW_LCD_LINE_WIDTH				16								// Replaced the LCD_LINE_WIDTH
#define	T_HW_LCD_MAX_LINES				2								// Replaced the MAX_LINES

/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
//extern byte    T_HW_TamperGlobalStatus;
/*------------------------ Global Function Prototypes ------------------------*/
void                    		Task_Hardware_Init( void);
void                    		Task_Hardware_Main( bool   fInternalCall);
void                          Task_Hardware_Call_B2B( bool   fMode);
/*----------------------------------------------------------------------------*/

