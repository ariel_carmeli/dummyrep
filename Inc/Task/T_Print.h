/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
#define SITE_NAME_LENGTH        (20+1)

typedef  struct
{
   ulong          ReportZ_Numerator;
   ulong          LastReport_DateAndTime;
   byte           Void[30];
   // The following fields should not be used by other tasks but 'TASK_SETUP' !
   byte           Checksum;
}  tPrinter_Info;


typedef  enum
{
   CREDIT_CARD_MODE_VOID            ,
   CREDIT_CARD_MODE_AUTHORIZATION   ,
   CREDIT_CARD_MODE_CHARGE          ,
   CREDIT_CARD_MODE_END_SESSION     ,
   /** Last entry **/
   CREDIT_CARD_MODE_LAST
}  tPrinter_CC_Mode;


typedef  struct
{
   bool  fPrintData;

   // Tx part of the structure
   struct
   {
      tPrinter_CC_Mode  Mode;
      ulong             ProductPrice;
   }  Tx;

   // Rx part of the structure
   struct
   {
      usint             Status;
      byte              TransactionType;
      byte              AuthorizedBy;
      ulong             AuthorizationNumber;
   }  Rx;
}  tPrinter_CC_Info;
/*---------------------------- External Variables ----------------------------*/

/*------------------------ Global Function Prototypes ------------------------*/
void                    	Task_Printer_Init( void);
void                    	Task_Printer_Main( void);
void                            Task_Printer_SetCopies( byte  Copies);
byte                            Task_Printer_GetStatus( void);
tPrinter_CC_Info                *Task_Printer_CC_GetInfo( void);
void                            Task_Printer_CreditCard( void);
void                            Task_Printer_COM2_HandleRx( byte  Data);
char                            *Task_Printer_GetSiteName(byte SiteIndex);
/*----------------------------------------------------------------------------*/

