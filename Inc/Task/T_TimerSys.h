/*------------------------------ Include Files -------------------------------*/

/*------------------------ Precompilation Definitions ------------------------*/
#define TIMER_SYS_10mS_TIMERS_MAX               15
#define TIMER_SYS_1Sec_TIMERS_MAX               3
#define TIMER_SYS_NULL			        0xFF
#define TIMER_SYS_NO_FREE_TIMERS	        0xFF
#define TIMER_SYS_HEAD_ADDRESS                  0xFD

// Status
#define TIMER_SYS_SHORT_TIMER_IS_EXPIRED        0x01    // bit 0
#define TIMER_SYS_LONG_TIMER_IS_EXPIRED         0x02    // bit 1


#define SET_BIT_INDEX(val, ind)                 (val) |= (0x00000001<<ind)
#define CLR_BIT_INDEX(val, ind)                 (val) &= (~(0x00000001<<ind))
#define IS_BIT_INDEX(val, ind)                  ((val) & (0x00000001<<ind))


/*---------------------------- Typedef Directives ----------------------------*/
typedef struct
{
	void					(*func_TimerS_OVF)(byte info);
	byte					TimersOvfPeakMeter;

} tTimerS_API_Setup;




typedef void (*pCallBack)(void);

typedef enum 
{
   ACTION_ADD = 0,
   ACTION_REM,
   ACTION_TO,
   ACTION_UPD,
   ACTION_ERR,
   ACTION_VOID
} eActionPrint;


typedef enum
{
	TIMER_VOID_TYPE = 0,
	TIMER_10mS_TYPE,
	TIMER_1Sec_TYPE
} TIMER_SYS_TYPES;

#define TIME_UNIT_1mS   1
#define TIME_UNIT_1Sec  2

// Timers_GetTimerValue()
#define TIMER_INFO_TYPE         0
#define TIMER_INFO_COUNTER      1
//

typedef struct
{
	pCallBack                       CBFunc;
	usint				TimerCounter;
	byte				Next;
}	tTimersLink;

typedef struct
{
	ulong				ActiveFlags;
        ulong                           TimTypeFlags;
	byte				ActiveLinkHead;
	byte				FreeLinkHead;
	byte				PeakMeter;			// measuring the required number of timers.
}	tTimerMechanism;


typedef struct 
{
	tTimersLink			TimersPool[TIMER_SYS_10mS_TIMERS_MAX];
	tTimerMechanism	Control;
}	tShortTimeTimer;



typedef struct 
{
	tTimersLink			TimersPool[TIMER_SYS_1Sec_TIMERS_MAX];
	tTimerMechanism	Control;
}	tLongTimeTimer;

typedef struct
{
	tShortTimeTimer                 ShortTimeTimers;
        byte                            SecondsCounter;
        byte                            FreeCounter10mS_Prev;
        ulong                           FreeRunning;
               
        //Testing
        //ulong                           TestFreeRunningCnt[16];
}	tTimersSystem;

/*---------------------------- External Variables ----------------------------*/

/*----------------------------- Global Variables -----------------------------*/


/*------------------------ Local Function Prototypes -------------------------*/

/*----------------------------------------------------------------------------*/


#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Global Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif


/*
* 
* 
* 
*/
/*----------------------------------------------------------------------------*/
//void                    		Timers_Init( void);
void                    		Timers_Init( tTimerS_API_Setup* pTimerS_API_Setup);
/*----------------------------------------------------------------------------*/

/*
* 
* 
* 
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_Main(void);
/*----------------------------------------------------------------------------*/

/*
* Timers_ISR is called every 10mS
* It is handling Timers system for 10mS periods and Seconds periods.
* 
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_10mS_Periods(void);
/*----------------------------------------------------------------------------*/


/*
* Set new timer
* Arguments:
* time - time in:
*    TimeType: TIME_UNIT_1mS   or   TIME_UNIT_1Sec
* 
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_SetTimer( usint time, byte TimeUnit, pCallBack pCBF );
/*----------------------------------------------------------------------------*/


/*
* Find an active timer which matched with the Callback function argument.
* Return back it's current time value (counter) and type of time (x10mS or x1Sec)
* TimerInfo:
*          0           |          1
* --------------------------------------------- 
* |  TimeType          |     TimerCounter     |
* ---------------------------------------------
* 
*  Return:
*          TRUE = O.K
*          FALSE = Not found
*/
/*----------------------------------------------------------------------------*/
bool					Timers_GetTimerValue( pCallBack pCBF, usint* TimerInfo );
/*----------------------------------------------------------------------------*/


/*
* 
* 
* 
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_StopTimer( pCallBack pCBF );
/*----------------------------------------------------------------------------*/



/*
* Blow a Timer:
*   Find the Timer by its CBF.
*   Set its counter to zero.
*   Remove timer from the list
*   Call the CBF immediately
*/
/*----------------------------------------------------------------------------*/
void                                    Timers_BlowTimer( pCallBack pCBF );
/*----------------------------------------------------------------------------*/



//----------------------- Unit Tests -------------------
void                                    TimersPrintActiveLink(eActionPrint Action, usint cbf, byte tab);
void                                    Timers_Test_1(void);
void                                    Timers_Test_2(void);
void                                    Timers_Test_3(void);
void                                    Timers_Test_4(void);
void                                    Timers_Test_5(void);
void                                    Timers_Test_6(void);
void                                    Timers_Test_7(void);
void                                    Timers_Test_8(void);
void                                    Timers_Test_9(void);
void                                    Timers_Test_10(void);
void                                    Timers_Test_11(void);
void                                    Timers_Test_12(void);
void                                    Timers_Test_13(void);
void                                    Timers_Test_14(void);
void                                    Timers_Test_15(void);
void                                    Timers_Test_16(void);
void                                    Timers_Test_17(void);
void                                    Timers_Test_18(void);
void                                    Timers_Test_19(void);
void                                    Timers_Test_20(void);
void                                    Timers_Test_1a(void);
void                                    Timers_Test_2a(void);
void                                    Timers_Test_3a(void);
void                                    Timers_Test_4a(void);
void                                    Timers_Test_5a(void);
void                                    Timers_Test_6a(void);
void                                    Timers_Test_7a(void);
void                                    Timers_Test_8a(void);
void                                    Timers_Test_9a(void);
void                                    Timers_Test_10a(void);
void                                    Timers_Test_11a(void);
void                                    Timers_Test_12a(void);
void                                    Timers_Test_13a(void);
void                                    Timers_Test_14a(void);
void                                    Timers_Test_15a(void);
void                                    Timers_Test_16a(void);
void                                    Timers_Test_17a(void);
void                                    Timers_Test_18a(void);
void                                    Timers_Test_19a(void);
void                                    Timers_Test_20a(void);










void                                    Timers_Test_OnDisplayTFT ( byte MessageType );
void                                    Timers_FreeRunCnt(void);
void                                    Timers_Test_Print(byte type, byte* Info, byte NumOfBytes);








#if 0
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
0x000000000000000000000000      Static Functions      0x000000000000000000000000
0x000000000000000000000000000000000000000000000000000000000000000000000000000000
#endif


