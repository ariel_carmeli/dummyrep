/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define     MAX_EVENT_VALUES                    3

#define     MAX_LOG_EVENT_RECORDS               1024
#define     MAX_LOG_TRANS_RECORDS               6144
#define     MAX_LOG_COIN_ROUTING_RECORDS        400
#define     MAX_LOG_Z_RECORDS                   40
/*---------------------------- Typedef Directives ----------------------------*/
typedef  struct
{
   usint          Idx_W;
   usint          Idx_R;
   usint          RecordsInDB;
   usint          Idx_R_Server;
   usint          RecordsInDB_Server;
   byte           Void[21];
   byte           Checksum;
}  tLogEvent_Info;

typedef  Cmd_V_Data        tLogEvent_Rec;    // See T_Server.H file
typedef  Cmd_V_Data_Format tLogEvent_Data;   // See T_Server.H file


typedef  struct
{
   usint          Idx_W;
   usint          Idx_R;
   usint          RecordsInDB;
   usint          Idx_R_Server;
   usint          RecordsInDB_Server;
   bool           fMadeCyclicRound;
   byte           Void[20];
   byte           Checksum;
}  tLogTrans_Info;   // 32 bytes


typedef  Cmd_L_Data     tLogTrans_Rec; // See T_Server.H file


typedef  struct
{
   ulong          EvNumerator_Last;
   usint          WriteIndex;
   usint          ReadIndex;
   usint          RecordsInDB;
   usint          ReadIndex_Server;    // see Task Server (report events to the Server)
   usint          RecordsInDB_Server;  // see Task Server (report events to the Server)
   byte           Void[30];

   // The following fields should not be used by other tasks but 'TASK_SETUP' !
   byte           Checksum;
}  tLogCoinRouting_Info;


typedef  struct
{
   ulong          EvNumerator;
   ulong          TimeStamp;  // will be set internally by TASK LOG-ERROR (T_LogErr)
   usint          Value;
   byte           RoutedTo;
}  tLogCoinRouting_Rec;


typedef  struct
{
   usint          Idx_W;
   usint          Idx_R;
   byte           Void[27];
   byte           Checksum;
}  tLogEmergency_Info;  // 32 bytes

typedef  struct
{
   usint          Idx_W;
   usint          Idx_R;
   usint          RecordsInDB;
   usint          Idx_R_Server;
   usint          RecordsInDB_Server;
   byte           Void[21];
   byte           Checksum;
}  tLogZ_Info;   // 32 bytes


typedef  struct
{
   usint          Z_Num;
   usint          Date;
   usint          Time;
   usint          Qty_Entrances;
   usint          Qty_RenewCard;
   usint          Qty_Confirmations;
   ulong          Val_Cash;
   ulong          Val_Credit;
   ulong          Val_Confirmations;
   byte           Void[8];
}  tLogZ_Rec;  // 32 bytes
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		Task_Log_Init( void);
void                    		Task_Log_Main( void);

// Events
void                          Task_Log_Event_WriteRecord( tLogEvent_Rec *pRec);
bool                          Task_Log_Event_ReadRecord( tLogEvent_Rec  *pRec, bool   fReadFirstRecord);
bool                          Task_Log_Event_ReadRecord_Server( tLogEvent_Rec *pRec);
void                          Task_Log_Event_Step_Fwd_Server( void);
tLogEvent_Info               *Task_Log_Event_GetInfo( void);
void                    		Task_Log_Event_ClearDB( void);

// Transactions
void                          Task_Log_Trans_WriteRecord( tLogTrans_Rec *pRec);
bool                          Task_Log_Trans_ReadRecord( tLogTrans_Rec  *pRec, bool   fReadFirstRecord);
bool                          Task_Log_Trans_ReadRecord_Server( tLogTrans_Rec *pRec);
void                          Task_Log_Trans_Step_Fwd_Server( void);
tLogTrans_Info               *Task_Log_Trans_GetInfo( void);
void                    		Task_Log_Trans_ClearDB( void);

// Coin Routing
void                          Task_Log_CoinRouting_WriteRecord( tLogCoinRouting_Rec *pRec);
bool                          Task_Log_CoinRouting_ReadRecord( tLogCoinRouting_Rec  *pRec, bool   fReadFirstRecord);
bool                          Task_Log_CoinRouting_ReadRecord_Server( tLogCoinRouting_Rec *pRec);
void                          Task_Log_CoinRouting_Step_Fwd_Server( void);
tLogCoinRouting_Info         *Task_Log_CoinRouting_GetInfo( void);
void                    		Task_Log_CoinRouting_ClearDB( void);

// Emergencies
void                          Task_Log_Emergency_Step_Fwd_Server( void);
tLogEmergency_Info           *Task_Log_Emergency_GetInfo( void);

#if 0 // [
// Z Records
void                          Task_Log_Z_WriteRecord( tLogZ_Rec *pRec);
bool                          Task_Log_Z_ReadRecord( tLogZ_Rec  *pRec, bool   fReadFirstRecord);
bool                          Task_Log_Z_ReadRecord_Server( tLogZ_Rec *pRec);
void                          Task_Log_Z_Step_Fwd_Server( void);
tLogZ_Info                   *Task_Log_Z_GetInfo( void);
void                    		Task_Log_Z_ClearDB( void);
#endif   // ]
/*----------------------------------------------------------------------------*/
void                    Task_Log_DebugIncr_RecordsInDB_Server(void);
void                    Task_Log_DebugClear_RecordsInDB_Server(void);
void                    Task_Log_DebugShow_RecordsInDB_Server(void);
void                    Task_Log_DebugClear_Idx_W(void);





