/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		Task_Setup_Init( void);
void                    		Task_Setup_Main( void);
bool                    		Task_Setup_GetDoorOpenStatus( void);

/*----------------------------------------------------------------------------*/

