/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		Task_Display_Init          ( void);
void                    		Task_Display_Main          ( void);
bool                    		Task_Display_IsBusy        ( void);
void                    		Task_Display_ClearScreen   ( void);
void                    		Task_Display_AddOn_Object  ( bool   fMode);
void                    		Task_Display_SetFont       ( tVGA_FontName   Name, byte  Style, tVGA_FontSize  Size);
void                    		Task_Display_SetColor      ( tVGA_Color      Color);
void                    		Task_Display_SetAlignment  ( tVGA_Alignment  Mode);
void                    		Task_Display_SetRectange   ( usint  X, usint Y, usint Height,  usint Width);
void                    		Task_Display_WriteChar     ( byte   Line, byte	Offset, char	Char);
void                    		Task_Display_WriteString   ( byte   Line, byte	Offset, char	*pStr);
void                    		Task_Display_WriteNumber   ( byte   Line, byte	Offset, ulong  Number, byte	Digits);
void                    		Task_Display_WriteNumberHex( byte   Line, byte	Offset, ulong  Number, byte	Digits);
void                    		Task_Display_WritePrice    ( byte   Line, byte	Offset, ulong  Number);
void                    		Task_Display_Run           ( void);
void                    		Task_Display_Hold          ( void);
void                    		Task_Display_Release       ( void);
/*----------------------------------------------------------------------------*/




