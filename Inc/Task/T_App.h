/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define	LINE1					0
#define	LINE2					1
#define	LINE3					2
#define	LINE4					3
#define	APP_MAX_LCD_LINES                       4

#define	LED_BUTTON_1				0
#define	LED_BUTTON_2				1
#define	LED_BUTTON_3				2
#define	LED_BUTTON_4				3
#define	LED_BUTTON_5				4
#define	LED_BUTTON_6				5
#define  LED_CHANGE_POCKET                      6
#define	APP_MAX_LEDS                            7

#define	RELAY1					0
#define	RELAY2					1
#define	APP_MAX_RELAYS                          2

#define	BC_READER1			        0  // BC = BarCode
#define	BC_READER2				1
#define	APP_MAX_BC_READERS                      2

#define	PRINTER_CUTTER_FULL                     0
#define	PRINTER_CUTTER_HALF                     1

#define  PAYMENT_TYPE_CASH                      0
#define  PAYMENT_TYPE_CREDIT_CARD               1
#define  MAX_PAYMENT_TYPES                      2

#define  MAX_TUBES                              4  // A = 1.00  B = 2.00  C = 10.00  D = 10.00

#define  COIN_ROUTING_CASH_BOX                  0
#define  COIN_ROUTING_TUBES                     1
#define  MAX_COIN_ROUTING                       2

#define  MONEY_BACK_REASON_CHANGE               0
#define  MONEY_BACK_REASON_CANCEL               1

#define  PAYMENT_TYPE_CASH                      0
#define  PAYMENT_TYPE_CREDIT_CARD               1

#define  MAX_APP_COMMANDS                       100   // to be used with 'tCommand' structure, Task App

#define  MAX_TRANS_REC_HEADR                    1  // used also by Task_Params (Vars section) and Task_Printer (printing receipt)
#define  MAX_TRANS_REC_DATA                     4  // used also by Task_Params (Vars section) and Task_Printer (printing receipt)
#define  MAX_TRANS_REC_PRODUCTS                 20 // number of products, which can be selected altogether in a single transaction

#define  SUB_CMD_EV_ALERT_LOW_CHANGE_LEVEL      1
#define  SUB_CMD_EV_ALERT_MACHINE_DOOR_LOCK     2
#define  SUB_CMD_EV_ALERT_MACHINE_DOOR          3
#define  SUB_CMD_EV_ALERT_COINS_SAFE            4
#define  SUB_CMD_EV_ALERT_BILLS_SAFE            5
#define  SUB_CMD_EV_ALERT_COINS_CHANGER         6
#define  SUB_CMD_EV_ALERT_BILL_VALIDATOR        7
#define  SUB_CMD_EV_ALERT_PRINTER               8

#define  APP_STATUS_ERR_FLAG_DISABLED           BIT_0
#define  APP_STATUS_ERR_FLAG_CRITICAL_ERROR     BIT_1
#define  APP_STATUS_ERR_FLAG_DOOR_CTRL_OFFLINE  BIT_2
#define  APP_STATUS_ERR_FLAG_PRN_OUT_OF_PAPER   BIT_3
#define  APP_STATUS_ERR_FLAG_PRN_OFFLINE        BIT_4

#define  APP_STATUS_WRN_FLAG_HOPPER_A_LOW_LVL   BIT_0 // 0000 0001
#define  APP_STATUS_WRN_FLAG_HOPPER_B_LOW_LVL   BIT_1 // 0000 0002
#define  APP_STATUS_WRN_FLAG_HOPPER_C_LOW_LVL   BIT_2 // 0000 0004
#define  APP_STATUS_WRN_FLAG_HOPPER_D_LOW_LVL   BIT_3 // 0000 0008
#define  APP_STATUS_WRN_FLAG_PRN_NEARLY_EOP     BIT_4 // 0000 0010
#define  APP_STATUS_WRN_FLAG_SERVER_OFFLINE     BIT_5 // 0000 0020
#define  APP_STATUS_WRN_FLAG_DOOR_OPENED        BIT_6 // 0000 0040

#define  MGC_TYPE_VOID                          0
#define  MGC_TYPE_CREDIT_CARD                   1
#define  MGC_TYPE_DIGITEL_CARD                  2

#define  MAX_CREDIT_CARDS                       1  // Application can handle up to 1 Credit cards
#define  MAX_DIGITEL_CARDS                      2  // Application can handle up to 2 Digitel cards
#define  MAX_MGC_CARDS                          (MAX_CREDIT_CARDS + MAX_DIGITEL_CARDS) // Application can handle up to 3 Mag-Stripe cards

#define  MAX_LOG_TRANS_ITEMS                    ((3 * MAX_L_REC_RECEIPT_ITEMS) + 2)
#define  MAX_STRING_LEN_VGA                     50
#define  MAX_STRING_LEN_LBL                     30

#define  INVALID_VALUE                          0xFF
/*---------------------------- Typedef Directives ----------------------------*/

typedef  enum
{
   MSG_Lib_APP_VER                  ,  
   MSG_Lib_APP_DATE                 ,
   MSG_Lib_APP_CODE                 ,
   MSG_READY                        ,
   MSG_PRESS_ANY_KEY                ,
   MSG_SETUP_ERR_1                  ,
   MSG_SET_QTTY_EMPTYING            ,

   // Errors
   MSG_ERROR                        ,
   MSG_ERR_SLAVE_OFF                ,
   MSG_ERR_READER_1_OFF             ,
   MSG_ERR_READER_2_OFF             ,
   MSG_ERR_BILL_VAL_OFF             ,
   MSG_ERR_BILL_VAL_JAM             ,
   MSG_ERR_BILL_VAL_STACKER_FULL    ,
   MSG_ERR_PRINTER_OFF              ,
   MSG_ERR_PRINTER_OUT_OF_PAPER     ,

   // Warnings
   MSG_WARNING                      ,
   MSG_WARNING_HOOPER_1_LOW_LEVEL   ,
   MSG_WARNING_HOOPER_2_LOW_LEVEL   ,
   MSG_WARNING_HOOPER_3_LOW_LEVEL   ,
   MSG_WARNING_HOOPER_4_LOW_LEVEL   ,
   MSG_WARNING_PRINTER_EOP_SENSOR   ,
   MSG_WARNING_CHANGE_LOW_LEVEL     ,
         /************/
   MAX_MESSAGES
}  tLCD_MsgCode;


typedef  enum
{
   VMC_EVENT_TRANSACTION_DONE             =  1     ,  // 1
   VMC_EVENT_COIN	                  =  2	   ,  // 3       0x02
   VMC_EVENT_BILL	                  =  3	   ,  // 0       0x03
   VMC_EVENT_OPENDOOR	                  =  4	   ,  // 0       0x04
   VMC_EVENT_CLOSEDOOR	                  =  5	   ,  // 0       0x05
   VMC_EVENT_EMPTY_SAFE	                  =  6	   ,  // 3
   VMC_EVENT_EMPTY_CENTRAL	          =  16	   ,  // 3
   VMC_EVENT_CANCEL_PAYMENT	          =  18	   ,  // 3
   VMC_EVENT_ODEF_CAL	                  =  27	   ,  // 1
   VMC_EVENT_ODEF_ISSUE	                  =  28	   ,  // 1
   VMC_EVENT_ALERT_ON                     =  29	   ,  // Variable, default data type is 2
   VMC_EVENT_ALERT_OFF                    =  30	   ,  // Variable, default data type is 2
   VMC_EVENT_TIME_AND_ATTEND_IN           =  98	   ,  // 1
   VMC_EVENT_TIME_AND_ATTEND_OUT          =  99	   ,  // 1
   VMC_EVENT_KEEP_COINS	                  =  101   ,  // 4
   VMC_EVENT_KEEP_BANKNOTES	          =  102   ,  // 3
   VMC_EVENT_KEEP_CENTRAL	          =  104   ,  // 3
   VMC_EVENT_KEEP_CENTRAL_2	          =  105   ,  // 3
   VMC_EVENT_MEMBER_PASSAGE               =  130   ,  // 2
   VMC_EVENT_OPEN_GATE	                  =  131   ,  // 1
   VMC_EVENT_CLOSE_GATE	                  =  132   ,  // 1
   VMC_EVENT_GATE_OPENED	          =  133   ,  // 1
   VMC_EVENT_GATE_CLOSED	          =  134   ,  // 1
   VMC_EVENT_LOW_PAPER	                  =  147   ,  // 0
   VMC_EVENT_NO_PAPER	                  =  148   ,  // 0
   VMC_EVENT_TIBA_PAY_COMM_LOSS           =  160   ,  // 0       0xA0           Loss communication with TibaPay server
   VMC_EVENT_TIBA_PAY_COMM_BACK           =  161   ,  // 0       0xA1           Communbication is back
   VMC_EVENT_TIBA_PAY_TRANSACTION_END     =  162   ,  // 2       0xA2           TibaPay transaction is completed
   VMC_EVENT_DIGITEL_ACCEPTED             =  163   ,  //         0xA3           ����� ����� ������
   VMC_EVENT_DIGITEL_ENTITLEMENT_CHECK    =  164   ,  //         0xA4           ������-����� ����� �������
   VMC_EVENT_DIGITEL_ENTITLEMENT_ANSWER   =  165   ,  //         0xA5           ������-����� ���� �� �����
   VMC_EVENT_CREDIT_C_ACCEPTED            =  166   ,  //         0xA6           Credit card (C.C) was inserted with card details
   VMC_EVENT_CREDIT_C_REQUEST_TO_CONFIRM  =  167   ,  //         0xA7           C.C request to confirm
   VMC_EVENT_CREDIT_C_REQUEST_ANSWER      =  168   ,  //         0xA8           C.C request answer (confirmed or not)
   // ----------------------------------------------------
   VMC_EVENT_BUTTON_PRESSED               =  170   ,  //         0xAA           Any button pressed with number of the button
   VMC_EVENT_BILL_DROP_CASSETTE_REMOVAL   =  171   ,  //         0xAB           Bill Drop cassette is removed with how many bills were inside
   VMC_EVENT_BILL_DROP_CASSETTE_REPLACEMENT= 172   ,  //         0xAC           Bill Drop cassette is is replaced back
   VMC_EVENT_COIN_CENTRAL_REMOVAL         =  173   ,  //         0xAD           COIN Central is removed with how many coins were inside
   VMC_EVENT_COIN_CENTRAL_REPLACEMENT     =  174   ,  //         0xAE           COIN Central is replaced back
   VMC_EVENT_COIN_HOPER_FILL              =  175   ,  //         0xAF           
   VMC_EVENT_EXCESS_CALCULATED            =  176   ,  //         0xB0           
   VMC_EVENT_EXCESS_RETURN                =  177   ,  //         0xB1           
   VMC_EVENT_COIN_HOPPER_REFILL           =  178   ,  //         0xB2           
   VMC_EVENT_MIKVE_OPENED                 =  200   ,  // 0
   VMC_EVENT_MIKVE_CLOSED                 =  201   ,  // 0
   VMC_EVENT_DEBT_CREATION                =  202   ,  // 2
   VMC_EVENT_DEBT_PAYMENT                 =  203   ,  // 2
   VMC_EVENT_APPL_POWERUP                 =  204   ,  // 1
   VMC_EVENT_TOTAL_VAL_COINS              =  205   ,  // 1
   VMC_EVENT_TOTAL_VAL_CENTRAL            =  206   ,  // 1
   VMC_EVENT_TOTAL_VAL_BILLS              =  207   ,  // 1
   // Events 230-254 are reserved for DEBUG mode
   VMC_EVENT_DEBUG_CARD_DETECTED          =  220   ,  // 2
   VMC_EVENT_DEBUG_CARD_REMOVED           =  221   ,  // 2
   VMC_EVENT_DEBUG_CARD_UPDATE            =  222   ,  // 2
   VMC_EVENT_DEBUG_CARD_WRITE_OK          =  223   ,  // 2
   VMC_EVENT_DEBUG_CARD_READ_ERR          =  224   ,  // 2
   VMC_EVENT_DEBUG_CARD_WRITE_ERR         =  225   ,  // 2
   VMC_EVENT_DEBUG_RFID_DB_VER            =  226   ,  // 2
   VMC_EVENT_DEBUG_RFID_APP_VER           =  227   ,  // 2
   VMC_EVENT_DEBUG_RFID_SITE_CODE         =  228   ,  // 2
   VMC_EVENT_DEBUG_RFID_ZONE_CODE         =  229   ,  // 2
   VMC_EVENT_DEBUG_RFID_DEVICE_TYPE       =  230   ,  // 2
   VMC_EVENT_DEBUG_RFID_LOW_CREDIT        =  231   ,  // 2
   VMC_EVENT_DEBUG_RFID_CARD_EXPIRED      =  232   ,  // 2
   VMC_EVENT_DEBUG_RFID_OVER_USAGE        =  233   ,  // 2
   VMC_EVENT_DEBUG_RFID_BACK_TOO_SOON     =  234   ,  // 2
   VMC_EVENT_DEBUG_RFID_RENEW_PRICE_0     =  235   ,  // 2
   VMC_EVENT_DEBUG_CC_TX_TO_SERVER        =  236   ,  // 2
   VMC_EVENT_DEBUG_CC_TIMEOUT             =  237   ,  // 2
   VMC_EVENT_DEBUG_CC_SERVER_REPLY_OK     =  238   ,  // 2
   VMC_EVENT_DEBUG_CC_SERVER_REPLY_ERR    =  239   ,  // 2
   VMC_EVENT_DEBUG_DGTL_TX_TO_SERVER      =  240   ,  // 2
   VMC_EVENT_DEBUG_DGTL_TIMEOUT           =  241   ,  // 2
   VMC_EVENT_DEBUG_DGTL_SERVER_REPLY_OK   =  242   ,  // 2
   VMC_EVENT_DEBUG_DGTL_SERVER_REPLY_ERR  =  243   ,  // 2
   VMC_EVENT_DEBUG_DGTL_NO_ELIGIBILITY    =  244   ,  // 2
   /******************/
   VMC_EVENT_LAST                         =  INVALID_VALUE
}  tServerEventCode;


typedef  struct
{
   byte        UnitType;      // Device Type
   byte        Address;       // Device Address
   byte        Command;       // Command
   byte        Sub_Command;   // Sub_Command
   byte        Index;         // Device index (Reader_1, Reader_2, LCD_1, LCD_2 etc.)
   byte        Sub_Index;     // Line
   byte        Value;
}  tCommand;   // Private structure in Task APP


typedef  struct
{
   usint       Error_Flags;
   usint       Warning_Flags;
}  tApp_Status;


typedef  struct
{
   byte        Type;               // 0 = Void; 1 = Credit Card; 2 = Manager Card; 3 = Digitel
   ulong       ConfirmationCode;
   ulong       AuthorizationCode;
   ulong       Resident_Discount;
   usint       Last4Digits;
   byte        CardIssuer;
}  tApp_MGC;


typedef  struct
{
   ulong       Value_Total;               // total before discounts
   ulong       Value_Total_Actual;        // total after discounts
   ulong       Value_Paid;                //
   ulong       Value_Paid_Cash_Net;       //
   ulong       Value_Paid_Cash;           //
   ulong       Value_Paid_CreditCard;     //
   ulong       Value_Discount_Resident;   //
   ulong       Value_Due;                 // money remained to pay
   ulong       Value_ChangeToGive;        // change to give back
   ulong       Value_ChangeGiven;         // change given in fact
   tApp_MGC    MGC[MAX_MGC_CARDS];
}  tApp_TransInfo;


typedef  usint       tProductCode;


typedef  struct
{
   tProductCode      Code;
   usint             Price;
   bool              fDigitelBenefit;
}  tProduct_Transaction;


/******************************************/
/* Definitions for Task Setup (T_Setup.c) */
/******************************************/
typedef  enum
{
   COIN_00_05        ,  // 0.05  (5 Ag)
   COIN_00_10        ,  // 0.10  (10 Ag)
   COIN_00_50        ,  // 0.50  (50 Ag)
   COIN_01_00        ,  // 1.00  (1 NIS)
   COIN_02_00        ,  // 2.00  (2 NIS)
   COIN_05_00        ,  // 5.00  (5 NIS)
   COIN_10_00        ,  // 10.00 (10 NIS)
   /******************/
   MAX_COIN_TYPES    ,
}  tCoins;

/******************/
typedef  struct
{
   ulong          SalesValue[MAX_PAYMENT_TYPES];
   ulong          SalesCount[MAX_PAYMENT_TYPES];
   byte           Void[40];
}  tSalesInfo;


typedef  struct
{
   tSalesInfo     Erasable;
   tSalesInfo     NonErasable;
   byte           Checksum;
}  tApp_SalesInfo;
/******************/


/******************/
typedef  struct
{
   ulong          MoneyDeposited;
   ulong          ChangeBack;
   ulong          MoneyInCashBox;
   usint          CashBox_Qtty[MAX_COIN_TYPES];
   byte           Void[6];
}  tCoinsInfo;


typedef  struct
{
   tCoinsInfo     Erasable;
   tCoinsInfo     NonErasable;
   byte           Checksum;
}  tApp_CoinsInfo;
/******************/


/******************/
typedef  enum
{
   BILL_20           ,  // 20 NIS
   BILL_50           ,  // 50 NIS
   BILL_100          ,  // 100 NIS
   BILL_200          ,  // 200 NIS
   /******************/
   MAX_BILL_TYPES    ,
}  tBills;

typedef  struct
{
   ulong          MoneyDeposited;
   usint          BillsInStacker;
   usint          BillsQtty[MAX_BILL_TYPES];
   byte           Void[12];
}  tBillsInfo;


typedef  struct
{
   tBillsInfo     Erasable;
   tBillsInfo     NonErasable;
   byte           Checksum;
}  tApp_BillsInfo;
/******************/

/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		Task_Application_Init                  ( void);
void                    		Task_Application_Main                  ( void);
void                          Task_Application_ExtractConstMsg       ( byte   *pDestStr   ,  byte        MsgCode  );
bool		              Task_Application_isMsgToHalt_ClockDisplay(byte MsgCode);
bool                          Task_Application_IsMsgToHaltClkDisplay ( byte   MsgCode     );
void						         Task_Application_SetEvent              ( usint  EvCode      ,  byte        SubCmd   ,  tLogEvent_Data *pData);
void						         Task_Application_SetAlert              ( byte   AlertCode   ,  byte        Value    ,  bool           fMode );
void                          Task_Application_SetDoAction           ( byte   ActionCode  ,  Cmd_D_Data  *pRec    );
Cmd_D_Data                   *Task_Application_GetDoAction_CC        ( void);
Cmd_E_Data                   *Task_Application_GetEmergency          ( void);
Cmd_E_Products               *Task_Application_GetEmergency_Products ( void);
tApp_Status                  *Task_Application_GetStatus             ( void);
tApp_TransInfo               *Task_Application_GetTransInfo          ( void);

/* Service functions for Task Setup (T_setup.c */
tApp_SalesInfo               *Task_Application_GetSalesInfo          ( void);
tApp_CoinsInfo               *Task_Application_GetCoinsInfo          ( void);
tApp_BillsInfo               *Task_Application_GetBillsInfo          ( void);

bool                          Task_Application_Close_Z               ( usint  New_Z       );
bool                          Task_Application_Prn_IsBusy            ( void);
void                          Task_Application_Prn_Init              ( void);
void                          Task_Application_Prn_InsertText        ( byte   *pText      , usint        Count    );
// Set Events functions
void                            Task_Application_SetEvent_OpenDoor(void);
void                            Task_Application_SetEvent_CloseDoor(void);


/*----------------------------------------------------------------------------*/
/*--------------- Parameters in External SRAM (direct access) ----------------*/
                     #if 0
                  '  NVRAM Total bytes : 0x5F00 (24,320) bytes
                  '  Base address: 0x1100
                  '  Top  address: 0x6FFF
                     #endif

                                          /* Sizes */
#define  PRN_TX_BUF_SIZE                  0x400

                                          /* Addresses */
#define  NVRAM_ADDR_BASE                  HW_DEF_xSRAM_DMA_BASE_ADDRESS
// temporary buffers                                                                                                                      // Address   Size  (A.C 30/6/2016)
#define  NVRAM_ADDR_PRN_TX_BUF            NVRAM_ADDR_BASE                                                                                 // 0x1100   (0x400   bytes)
#define  NVRAM_ADDR_APP_COMMANDS          (NVRAM_ADDR_PRN_TX_BUF           +  PRN_TX_BUF_SIZE                                          )  // 0x1500   (0x2BC   bytes)
#define  NVRAM_ADDR_SERVER_TX_PACKET      (NVRAM_ADDR_APP_COMMANDS         +  (MAX_APP_COMMANDS *  sizeof( tCommand))                  )  // 0x17BC   (0xCC    bytes)
#define  NVRAM_ADDR_SERVER_Q_CMD          (NVRAM_ADDR_SERVER_TX_PACKET     +  SERVER_MAX_LEN_BUFFER                                    )  // 0x1888   (0x57    bytes)
#define  NVRAM_ADDR_TRACK_2               (NVRAM_ADDR_SERVER_Q_CMD         +  sizeof( Cmd_Q_Data)                                      )  // 0x18DF   (0x28    bytes)
#define  NVRAM_ADDR_APP_TRANS_INFO        (NVRAM_ADDR_TRACK_2              +  B2B_MAX_SIZE_TRACK_2                                     )  // 0x1907   (0x58    bytes)
#define  NVRAM_ADDR_SERVER_D_CMD          (NVRAM_ADDR_APP_TRANS_INFO       +  sizeof( tApp_TransInfo)                                  )  // 0x195F   (0x18    bytes)
#define  NVRAM_ADDR_APP_E_CMD             (NVRAM_ADDR_SERVER_D_CMD         +  sizeof( Cmd_D_Data)                                      )  // 0x1977   (0x2F    bytes)
#define  NVRAM_ADDR_TFT_DISPLAY           (NVRAM_ADDR_APP_E_CMD            +  sizeof( Cmd_E_Data)                                      )  // 0x19A6   (0x300   bytes)
#define  NVRAM_ADDR_APP_LOG_TRANS_ITEMS   (NVRAM_ADDR_TFT_DISPLAY          +  TFT_DISPLAY_BUF_SIZE                                     )  // 0x1CA6   (0x37    bytes)
#define  NVRAM_ADDR_APP_B2B_TX_DATA       (NVRAM_ADDR_APP_LOG_TRANS_ITEMS  +  (sizeof( tRcptItem) * MAX_LOG_TRANS_ITEMS)               )  // 0x1CDD   (0x34    bytes)
#define  NVRAM_ADDR_APP_TXT_BUF           (NVRAM_ADDR_APP_B2B_TX_DATA      +  sizeof( tB2B_FromMaster)                                 )  // 0x1D11   (0x32    bytes)
#define  NVRAM_ADDR_APP_PRODUCTS          (NVRAM_ADDR_APP_TXT_BUF          +  MAX_STRING_LEN_VGA                                       )  // 0x1D43   (0x64    bytes)
#define  NVRAM_ADDR_APP_SALES_INF0        (NVRAM_ADDR_APP_PRODUCTS         +  (sizeof( tProduct_Transaction) * MAX_TRANS_REC_PRODUCTS) )  // 0x1DA7   (0x71    bytes)
#define  NVRAM_ADDR_APP_COINS_INF0        (NVRAM_ADDR_APP_SALES_INF0       +  sizeof( tApp_SalesInfo)                                  )  // 0x1E18   (0x41    bytes)
#define  NVRAM_ADDR_APP_BILLS_INF0        (NVRAM_ADDR_APP_COINS_INF0       +  sizeof( tApp_CoinsInfo)                                  )  // 0x1E59   (0x35    bytes)

#define  NVRAM_ADDR_T_TIBAPAY_VARS        (0x3000                                                                                      )  // 0x3000 - 0x33FF  (0x0400 bytes)
#define  NVRAM_ADDR_T_TIBAPAY_VARS_END    (NVRAM_ADDR_T_TIBAPAY_VARS       +  0x0400                                                   )

#define  NVRAM_ADDR_T_TIBAPAY_API_VARS     (NVRAM_ADDR_T_TIBAPAY_VARS_END                                                              )  // 0x3400 - 0x3600  (0x0200 bytes)
#define  NVRAM_ADDR_T_TIBAPAY_API_VARS_END (NVRAM_ADDR_T_TIBAPAY_VARS      +  0x0200                                                   )

#define  NVRAM_ADDR_T_LOGGER_BUFFER        (NVRAM_ADDR_T_TIBAPAY_API_VARS_END                                                          )  // 0x3600 - 0x3700  (0x0100 bytes )
#define  NVRAM_ADDR_T_LOGGER_BUFFER_END    (NVRAM_ADDR_T_TIBAPAY_API_VARS_END  +  0x0100                                               )  // This 256 bytes buffer is used a logger for debugging
                    
// TimerS optional RAM area
#define  NVRAM_ADDR_T_TIMERS_SYSTEM        (NVRAM_ADDR_T_LOGGER_BUFFER_END                                                             )  // 0x3700 - 0x3800  (0x0100 bytes )
#define  NVRAM_ADDR_T_TIMERS_SYSTEM_END    (NVRAM_ADDR_T_TIMERS_SYSTEM +  0x0100                                                       )  // This 256 bytes buffer is used for Timers System

// Debug Prints area
#define  NVRAM_ADDR_T_DEBUG_PRINT_BUF      (NVRAM_ADDR_T_TIMERS_SYSTEM_END                                                             )  // 0x3800 - 0x3900  (0x0100 bytes )
#define  NVRAM_ADDR_T_DEBUG_PRINT_BUF_END  (NVRAM_ADDR_T_DEBUG_PRINT_BUF +  0x0100                                                     )  // This 256 bytes buffer is used for Timers System
                    
// TFT Lib Scratch pad area
#define  NVRAM_ADDR_T_TFT_LIB              (NVRAM_ADDR_T_DEBUG_PRINT_BUF_END                                                           )  // 0x3900 - 0x3980  (0x0100 bytes )
#define  NVRAM_ADDR_T_TFT_LIB_END          (NVRAM_ADDR_T_TFT_LIB +  0x080                                                              )  // This 256 bytes buffer is used for Timers System

// TFT UART parameters 
#define  NVRAM_ADDR_T_TFT_UART_LIB         (NVRAM_ADDR_T_TFT_LIB_END                                                                   )  // 0x3980 - 0x3A00  (0x0100 bytes )
#define  NVRAM_ADDR_T_TFT_UART_LIB_END     (NVRAM_ADDR_T_TFT_UART_LIB +  0x080                                                         )  // This 256 bytes buffer is used for Timers System
                    

                    
//-----
#define  NVRAM_ADDR_END                   (NVRAM_ADDR_T_TIMERS_SYSTEM_END                                                              )  // 0x3800

                                          /* Pointers */
// temporary buffers
#define  p_NVRAM_PRN_TX_BUF               ((byte                  *) NVRAM_ADDR_PRN_TX_BUF            )
#define  p_NVRAM_APP_COMMANDS             ((tCommand              *) NVRAM_ADDR_APP_COMMANDS          )
#define  p_NVRAM_SERVER_TX_PACKET         ((char                  *) NVRAM_ADDR_SERVER_TX_PACKET      )
#define  p_NVRAM_SERVER_Q_CMD             ((Cmd_Q_Data            *) NVRAM_ADDR_SERVER_Q_CMD          )
#define  p_NVRAM_TRACK_2                  ((char                  *) NVRAM_ADDR_TRACK_2               )
#define  p_NVRAM_APP_TRANS_INFO           ((tApp_TransInfo        *) NVRAM_ADDR_APP_TRANS_INFO        )
#define  p_NVRAM_SERVER_D_CMD             ((Cmd_D_Data            *) NVRAM_ADDR_SERVER_D_CMD          )
#define  p_NVRAM_APP_E_CMD                ((Cmd_E_Data            *) NVRAM_ADDR_APP_E_CMD             )
#define  p_NVRAM_TFT_DISPLAY              ((char                  *) NVRAM_ADDR_TFT_DISPLAY           )
#define  p_NVRAM_APP_LOG_TRANS_ITEMS      ((tRcptItem             *) NVRAM_ADDR_APP_LOG_TRANS_ITEMS   )
#define  p_NVRAM_APP_B2B_TX_DATA          ((tB2B_FromMaster       *) NVRAM_ADDR_APP_B2B_TX_DATA       )
#define  p_NVRAM_APP_TXT_BUF              ((char                  *) NVRAM_ADDR_APP_TXT_BUF           )
#define  p_NVRAM_APP_PRODUCTS             ((tProduct_Transaction  *) NVRAM_ADDR_APP_PRODUCTS          )
#define  p_NVRAM_APP_SALES_INF0           ((tApp_SalesInfo        *) NVRAM_ADDR_APP_SALES_INF0        )
#define  p_NVRAM_APP_COINS_INF0           ((tApp_CoinsInfo        *) NVRAM_ADDR_APP_COINS_INF0        )
#define  p_NVRAM_APP_BILLS_INF0           ((tApp_BillsInfo        *) NVRAM_ADDR_APP_BILLS_INF0        )
#define  p_NVRAM_LOGGER_BUF               ((byte                  *) NVRAM_ADDR_T_LOGGER_BUFFER       )
/*----------------------------------------------------------------------------*/
void                    Task_Application_DisplayCommLoss        (void);
void                    Task_Application_DisplayReDraw          (void);
// Testing
void                    Task_Application_Test_Leds              (byte Led, byte Mode   );
void                    Task_Application_SystemDuringSetup      (void);
void                    Task_Application_RecoverEquipmentID     (void);
void                            Task_Application_SetEvent_CoinHopperRefill(byte HopperId, usint Quantity);
