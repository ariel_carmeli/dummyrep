/*------------------------------ Include Files -------------------------------*/
#include "T_HW.h"       // Task Hardware (LCD, LED, Relay, DIP Switch etc.)
#include "T_Init.h"     // Task Initialization
#include "T_Timer.h"    // Task Timer

#include "T_B2Bmst.h"   // Task B2B (Board-to-Board communication) - Master unit
#include "T_Server.h"   // Task Server
#include "T_Log.h"      // Task Log (Events)
#include "T_Print.h"    // Task Printer
#include "T_App.h"      // Task Application
#include "T_Params.h"   // Task Parameters
#include "T_Disp.h"     // Task TFT Display
//#include "T_TFT.h"     // Task TFT Display
#include "T_Hopper.h"   // Task Hopper
#include "T_Setup.h"    // Task Setup (VM Setup menus)
#include "T_TibaPay.h"  // Task TibaPay (protocol)
#include "T_Lib.h"      // Task Lib is responsible to monitor (and init?) the array of libraries.
#include "T_TimerSys.h" // pool of general purpose Timers system
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
/*----------------------------------------------------------------------------*/

