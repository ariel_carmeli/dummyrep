/*------------------------------ Include Files -------------------------------*/

/*------------------------ Precompilation Definitions ------------------------*/

/*---------------------------- Typedef Directives ----------------------------*/
typedef enum
{
   T_LIB_EPSON = 0,     // 0
   T_LIB_LED,           // 1
   T_LIB_BUZZ,          // 2
   T_LIB_CLOCK,         // 3
   T_LIB_IN_OUT,        // 4
   T_LIB_UTIL,          // 5
   T_LIB_LCD,           // 6
   T_LIB_KBD,           // 7
   T_LIB_HOPPER,        // 8
      
   // ===========
   T_LIB_MAX_LIB
}  tT_LIB_LIB_CODE;


typedef byte                    (*Lib_GetError)( void);                 // This type is a GetError library type
typedef struct 
{
   tT_LIB_LIB_CODE      LibraryCode;
   byte                 ErrorCode;
} tT_Lib_Error;


/*---------------------------- External Variables ----------------------------*/

/*------------------------ Global Function Prototypes ------------------------*/

/*----------------------------------------------------------------------------*/
void                    		Task_Lib_Init( void);
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
void                    		Task_Lib_Main( void );
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
void                                    Task_Lib_GetLastError(byte* pLibCode, byte* pErrorCode );
/*----------------------------------------------------------------------------*/

