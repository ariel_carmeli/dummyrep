/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
#define MAX_HOPPERS_DE_FACTO    4

typedef  enum
{
   HOPPER_STATUS_IDLE         =  0,
   HOPPER_STATUS_BUSY         =  1,
   HOPPER_STATUS_DONE_OK      =  2,
   HOPPER_STATUS_DONE_ERROR   =  3,
}  tHopperStatus;


typedef  struct
{
   ulong          MoneyRefilled;
   ulong          MoneyEmptied;
   ulong          MoneyInTubes;
   ulong          MoneyInTube[MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/];
   usint          CoinsInTube[MAX_HOPPERS_DE_FACTO /*MAX_HOPPERS*/];
   byte           Void[27];
   byte           Checksum;                  
}  tHoppers_Info;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                            Task_Hopper_Init( void);
void                    	Task_Hopper_Main( void);
tHopperStatus                   Task_Hopper_GetStatus( void);
tHoppers_Info                   *Task_Hopper_GetInfo( void);
ulong                           Task_Hopper_GetValueDispensed( void);
usint                           Task_Hopper_GetCoinsDispensed( tHopper Hopper);
bool                            Task_Hopper_IsLowLevel( tHopper Hopper);
usint                           Task_Hopper_GetCoinValue( tHopper Hopper);
void                            Task_Hopper_AddCoinsToHopper( tHopper Hopper, usint   Count);
void                    	Task_Hopper_Dispense_Value( ulong   Value, bool fInternalCall);
void                    	Task_Hopper_Dispense_Count( tHopper Hopper, usint  Count, bool fInternalCall);
void                    	Task_Hopper_ResetInfo( void);
void                            Task_Hopper_SetxRam_CoinVal_Capacity( ulong coin_value, ulong capacity, byte hopper_num);
/*----------------------------------------------------------------------------*/






