/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define  MAX_L_REC_RECEIPT_ITEMS                3
#define  SERVER_MAX_COINS                       8
#define  SERVER_MAX_SIZE_TRACK_2                32

#define  TRANS_REC_TYPE_CASH                    1
#define  TRANS_REC_TYPE_CASH_NEXT               0xF0
#define  TRANS_REC_TYPE_PRODUCTS_HEADER         0x82
#define  TRANS_REC_TYPE_PRODUCTS_PROD_1         0x83
#define  TRANS_REC_TYPE_PRODUCTS_PROD_2         0x84
#define  TRANS_REC_TYPE_PRODUCTS_PROD_3         0x85

#define  TRANS_REC_DATA_TYPE_CASH_VALUE         1     // Cash - Value
#define  TRANS_REC_DATA_TYPE_CREDIT_VALUE       200   // Credit Card    - Value
#define  TRANS_REC_DATA_TYPE_CREDIT_CONF_FP     201   // Credit Card    - Confirmation # (FastPark)
#define  TRANS_REC_DATA_TYPE_CREDIT_CONF_CS     207   // Credit Card    - Confirmation # (Credit-Server)
#define  TRANS_REC_DATA_TYPE_DIGITEL_CONF_FP    251   // Digitel Card   - Confirmation # (FastPark)
#define  TRANS_REC_DATA_TYPE_DIGITEL_DISCOUNT   252   // Digitel Card   - Total Discount

#define  TRANS_REC_FLAG_HAS_NEXT_REC            BIT_0             // BIT_0

//#define   ACTIVE_COIN_CHANGER                    BIT_0
//#define   ACTIVE_BILL_VALIDATOR                  BIT_1
//#define   ACTIVE_PRINTER                         BIT_2

#define  PRN_STATUS_EOP_SENSOR                  BIT_0
#define  PRN_STATUS_PAPER_OUT                   BIT_1
#define  PRN_STATUS_OFF                         0xFF

#define  D_CMD_VOID                             0
#define  D_CMD_EMERGENCY_CREDIT_CARD            0x10
#define  D_CMD_DB                               0x20
#define  D_CMD_DIGITEL_AUTHENTICATION           0x81
#define  D_CMD_PRODUCT_ENABLE_DISABLE           0x82
#define  D_CMD_TECHNICIAN                       0x99

// Sub-commands for D-32
#define  D_SUB_CMD_DB_RESET_V_POINTERS          0x01
#define  D_SUB_CMD_DB_RESET_L_POINTERS          0x02
#define  D_SUB_CMD_DB_RESET_COIN_ROUTING        0x03
#define  D_SUB_CMD_DB_RESET_Z_POINTERS          0x04
#define  D_SUB_CMD_DB_RESET_VARS                0x05
#define  D_SUB_CMD_DB_RESET_HOPPERS             0x06
#define  D_SUB_CMD_DB_RESET_ALL                 0x0F  // 15
#define  D_SUB_CMD_DB_DEL_CRNT_Z_RECORD         0x10  // 16

// Sub-commands for D-99
#define  D_SUB_CMD_TECH_VOID                    0x00
#define  D_SUB_CMD_TECH_RESET                   0x01
#define  D_SUB_CMD_TECH_REPLY_TO_ANY_MSG        0x02
#define  D_SUB_CMD_TECH_RESUME                  0x03
#define  D_SUB_CMD_TECH_PRINT_RECEIPT           0x04
#define  D_SUB_CMD_BILL_VALIDATOR               0x05
#define  D_SUB_CMD_TECH_DEBUG_PARAMS            0xEE
#define  D_SUB_CMD_TECH_FACTORY_DEFAULTS        0xFF

#define  EMERGENCY_TYPE_VOID                    0
#define  EMERGENCY_TYPE_CREDIT_CARD             0x10
#define  EMERGENCY_TYPE_DIGITEL_AUTHENTICATION  0x81
#define  EMERGENCY_TYPE_TIBAPAY_COMM_LOSS       0xEE            // 0xEE temporary TibaPay Comm Loss code
#define  EMERGENCY_TYPE_TIBAPAY_COMM_BACK       0xEF            // 0xEF temporary TibaPay Comm BACK code

#define  MAX_DEVICES_MGC                        2
#define  MAX_PRODUCTS                           3  // Variety of products, supported by the application (like umbrella, chair and bed)

// Buttons pressed events
#define  SET_EVENT_BUTTON_S_1                   1
#define  SET_EVENT_BUTTON_S_2                   2
#define  SET_EVENT_BUTTON_S_3                   3
#define  SET_EVENT_BUTTON_LANGUAGE              4
#define  SET_EVENT_BUTTON_RECEIPT               5
#define  SET_EVENT_BUTTON_CANCEL                6



/*---------------------------- Typedef Directives ----------------------------*/
typedef  struct
{
   byte           Code;
   byte           Amount;
}  tCmd_E_Product;


                  /**********************************************/
                  /******************* MASTER *******************/
                  /**********************************************/
typedef  union    /* Command D   (Do action) */
{
   struct   // 0x09
   {
      byte           Void;
      byte           EquipmentID;
      byte           ActionCode;
      ulong          Value;
   }  Tech;       // Technician actions

   struct   // 0x10
   {
      byte           Void_1;
      byte           EquipmentID;
      byte           ResultCode;
      ulong          Value;
      ulong          AuthorizationCode;   // of Clearance house
      ulong          ConfirmationCode;    // of PC
      usint          Last4Digits;
      byte           CardIssuer;
   }  CreditCard; // Credit Card clearance

   struct   // 0x20
   {
      byte           Void;
      byte           EquipmentID;
      byte           ActionCode;
   }  DB;         // Database actions

   struct   // 0x81
   {
      byte           Void_1;
      byte           EquipmentID;
      byte           ResultCode;
      ulong          Void_2;
      ulong          AuthorizationCode;
      ulong          ConfirmationCode;    // of PC
      usint          Last4Digits;
      byte           Void_3;
      tCmd_E_Product Products[MAX_PRODUCTS];
   }  Digitel; // Digitel Card clearance

   struct   // 0x82
   {
      bool           fMode;   // 0 = Disable; 1 = Enable
      byte           EquipmentID;
      byte           ProductCode;   // FF = All
   }  Product_EnaDis;         // Product Enable/Disable
}  Cmd_D_Data;


typedef  union    /* Master Structure */
{
   struct         // Command L   (Emergency / Transaction / Event / Status)
   {
      usint       Idx_R_Trans;   // Read pointer of Transactions
      byte        EquipmentID;   // Device ID (number)
   }  Cmd_L;

   struct         // Command D
   {
      byte        SubCmd;     // 00 (Keep gate closed)
                              // 01 (Keep gate opened)
                              // 02 (Hold relay ON/OFF)
                              // 16 (Credit Card clearance)
      Cmd_D_Data  Data;       // 16 bytes
   }  Cmd_D;

   struct         // Command T
   {
      byte        Day_BCD;
      byte        Month_BCD;
      byte        Year_BCD;
      byte        Hour_BCD;
      byte        Minutes_BCD;
      byte        Seconds_BCD;
   }  Cmd_T;

   struct         // Command W
   {
      byte        Page;
      usint       Offset;
   }  Cmd_R;

   struct         // Command W
   {
      byte        Page;
      usint       Offset;
      byte        Data[32];
   }  Cmd_W;

   struct         // Command L   (Emergency / Transaction / Event / Status)
   {
      usint       New_Z;
      byte        Void;
      byte        EquipmentID;
   }  Cmd_Y;

   struct         // Command L   (Emergency / Transaction / Event / Status)
   {
      byte        Delimiter1;
      byte        Delimiter2;
      usint       Version;
      byte        EquipmentID;
   }  Cmd_Z;
}  tServer_MasterRec;






                  /**********************************************/
                  /******************* SLAVE ********************/
                  /**********************************************/

typedef  struct    /* Command E   (Emergency) */
{
   ulong          ValueToCharge;
   byte           Void1;
   byte           EquipmentID;
   byte           Void2;
   ulong          TicketNum;
   ulong          DateAndTime;
   byte           Track2[SERVER_MAX_SIZE_TRACK_2]; // filled with ending FF's
}  Cmd_E_CreditCard; // Credit Card


typedef  struct    /* Command E   (Emergency) */
{
   ulong          Void1;
   byte           Void2;
   byte           EquipmentID;
   byte           Void3;
   ulong          Void4;
   ulong          DateAndTime;
   byte           Track2[SERVER_MAX_SIZE_TRACK_2]; // filled with ending FF's
}  Cmd_E_Digitel; // Digitel card


typedef  union    /* Command E   (Emergency) */
{
   Cmd_E_CreditCard  CreditCard; // Type 0x10
   Cmd_E_Digitel     Digitel;    // Type 0x81
}  Cmd_E_Data;


typedef  struct   /* Command E   (Emergency) - Digitel Products*/
{
   tCmd_E_Product Products[MAX_PRODUCTS];
}  Cmd_E_Products;


typedef  struct
{
   byte           Type;
   ulong          Value;
}  tRcptItem;


typedef  struct   /* Command L   (Product) */
{
   byte           Code;
   byte           Count_FullPrice;
   byte           Count_Discount;
   ulong          UnitPrice;
   ulong          Total_Discount;
   ulong          Void;
}  tCmd_L_Product;


typedef  union    /* Command L   (Transaction) */
{
   struct
   {
      byte        Type;       // 1  Cash transaction        (Coins, Bills, Credit Cards)
      ulong       DateAndTime;
      usint       RcptConfirm_Num;  // Receipt number / Confirmation number
      usint       Receipt_Z_Num;
      byte        EquipmentID;
      tRcptItem   Item;
      byte        Void;
   }  RcptHeader; // 16 bytes of Receipt Header

   struct
   {
      byte        ContCode;   // Continuation Code
                              // Continuation of Cash receipt: F1 = Has next record; F0 = Last record
      ulong       DateAndTime;
      usint       Void1;
      usint       Void2;
      byte        Void3;
      tRcptItem   Item;
      byte        DataTypes;  // 0xC? (0xC1 - 0xC5)
   }  RcptHeader_2;  // 16 bytes of 2nd part of Header

   struct
   {
      byte        ContCode;   // Continuation Code
                              // Continuation of Cash receipt: F1 = Has next record; F0 = Last record
      tRcptItem   Item[MAX_L_REC_RECEIPT_ITEMS];
                              // Type:
                              // 200	Credit Card - Value
                              // 201	Credit Card - Confirmation # (generated by PC)
                              // 207	Credit Card - Confirmation # (generated by Credit-Server)
                              // 251	Digitel Card #1 - Confirmation # (generated by PC)
                              // 252	Digitel Card #2 - Confirmation # (generated by PC)
   }  RcptData;   // 16 bytes of Receipt Data

   struct
   {
      byte        Type;       // 0x82
      ulong       DateAndTime;
      usint       RcptConfirm_Num;  // Receipt number / Confirmation number
      usint       Receipt_Z_Num;
      byte        EquipmentID;
      tRcptItem   Void1;
      byte        Void2;
   }  ProductsHeader;   // 16 bytes of Products Header

   struct
   {
      byte              Type;       // 0x83 - 0x85
      tCmd_L_Product    Product;
   }  ProductData;   // 16 bytes of of Product's data (x MAX_PRODUCTS)
}  Cmd_L_Data;


typedef  struct
{
   usint          Price;
   byte           Amount;
   byte           Code;
}  Cmd_L_Data_Product;


typedef  union
{
   struct
   {
      byte        Void[8];
   }  Type_0;

   struct
   {
      ulong       Field1;
      byte        Field2[4];
   }  Type_1;

   struct
   {
      ulong       Field1;
      ulong       Field2;
   }  Type_2;

   struct
   {
      usint       Field1;
      usint       Field2;
      usint       Field3;
      usint       Field4;
   }  Type_3;
   /*
   struct
   {
      byte        Field1[6];
      usint       Field2;
   }  Type_4;

   struct
   {
      byte        Field1[8];
   }  Type_5;
   */
}  Cmd_V_Data_Format;



typedef  struct   /* Command V   (Event) */
{
   byte              SubCmd;
   byte              EvCode;
   byte              EquipmentID;
   ulong             DateAndTime;
   Cmd_V_Data_Format Data;
   byte              Void2;
}  Cmd_V_Data;


typedef  struct   /* Command Q   (Status) */
{
   byte     Inactivity;       // 0 = Active; 1 = Inactive
                              // BIT_0 -  Machine's Application (I)
                              // BIT_1 -  Machine's Application (II)
                              // BIT_5 -  always ON (1)
   byte     Void_01[16];
   byte     Inputs_B2B_DIP_Switches;
   byte     SystemErrors_1;
   byte     SystemErrors_2;
   byte     Security;
   byte     Void_02;
   byte     Void_03;   // Bill Validator offline ???
   //byte     Inputs_Local_A;
   //byte     Inputs_Local_B;
   //byte     Inputs_GT_CTL[MAX_DEVICES_MGC / 2];  // MAX_DEVICES_MGC = 8
   //byte     Relays_GT_CTL[MAX_DEVICES_MGC / 2];  // MAX_DEVICES_MGC = 8
   //byte     Relays_Local;
   byte     Status_Printer;
   byte     Void_04[4];
   byte     Void_05;
   byte     Void_06_BV;
   byte     Void_07;
   byte     BillValidator;
   byte     Void_08;
   //byte     Status_Coin_Vldtr_1;
   //byte     Status_Coin_Vldtr_2;
   //byte     Status_Coin_Vldtr_3;
   //byte     Status_Bill_Vldtr_1;
   //byte     Status_Bill_Vldtr_2;
   //byte     Status_Bill_Vldtr_3;
   //byte     State;
   //byte     Void;
   //byte     IdxValue_Coin[SERVER_MAX_COINS];
/**** Note: from this point DONT CHANGE VAR'S POSITION ****/
   usint    Qtty_Coins_Hopper_A;
   usint    Qtty_Coins_Hopper_B;
   usint    Qtty_Coins_Hopper_C;
   usint    Qtty_Bills_20;
   usint    Qtty_Bills_50;
   usint    Qtty_Bills_100;
   usint    Qtty_Bills_200;
   usint    Qtty_Coins_Central_100;
   usint    Qtty_Coins_Central_500;
   usint    Qtty_Coins_Central_1000;
   usint    Qtty_Bills_1;
   usint    Qtty_Bills_5;
   usint    Qtty_Bills_10;
   usint    Qtty_Coins_Hopper_D;
   usint    Qtty_Coins_Central_200;
   usint    Void_09[10];
   ulong    CommQuality_Percent;
   //usint    Qtty_Coins_Central_5;
   //usint    Qtty_Coins_Central_10;
   //usint    Qtty_Coins_Central_25;
   //usint    Qtty_Coins_Central_50;
   //usint    Qtty_Bills_Void;
   //ulong    Total_Value_Coins;
   //ulong    Total_Value_Central;
   //ulong    Total_Value_Bills;   // 3 longs (12 bytes)
   /* end of Note section */  // see Task_Server_SaveStatusVars() + Task_Server_ReadStatusVars()
}  Cmd_Q_Data; // 87 bytes


typedef  union    /* Slave Structure */
{
   struct         // Command E   (Emergency)
   {
      usint          Idx_W;
      usint          Idx_R;
      byte           Type;
      Cmd_E_Data     Data; // 16/48 bytes
      Cmd_E_Products Products; // 16/48 bytes
      usint          Current_Z_Num;
   }  Cmd_E;

   struct         // Command L   (Transaction)
   {
      usint          Idx_W;
      usint          Idx_R;
      Cmd_L_Data     Data; // 16 bytes
      usint          Current_Z_Num;
   }  Cmd_L;

   struct         // Command V   (Event)
   {
      usint          Idx_W;
      usint          Idx_R;
      Cmd_V_Data     Data; // 16 bytes
      usint          Current_Z_Num;
   }  Cmd_V;
}  tServer_SlaveRec;
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		Task_Server_Init                 ( void);
void                    		Task_Server_Main                 ( void);
Cmd_Q_Data                   *Task_Server_GetStatusStructPtr   ( void);
void                          Task_Server_SaveStatusVars       ( void);
void                          Task_Server_SendStatus           ( void);
void                          Task_Server_SetEquipmentID       ( byte   EquipmentID);
byte                          Task_Server_GetEquipmentID       ( void);
bool                          Task_Server_IsServerOnline       ( void);
void                          Task_Server_SetMode_IamMaster    ( bool   fMode);
void                          Task_Server_SetMode_ReplyToAnyMsg( bool   fMode);
/*----------------------------------------------------------------------------*/
/*--------------- Parameters in External SRAM (direct access) ----------------*/
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void				Task_Server_DbgIncrTransac( void);
void				Task_Server_DbgClearTransac( void);
void				Task_Server_DbgIncrIdx_R_Trans( void);
void				Task_Server_View_Idx_R_Server( void);

