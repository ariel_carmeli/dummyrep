/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
/*---------------------------- Typedef Directives ----------------------------*/
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		Task_Timer_Init( void);
void                    		Task_Timer_Main( void);
tTimerMsec							Task_Timer_GetTimer( void);
tTimerMsec							Task_Timer_SetTimeMsec( usint	Value);
tTimerSec							Task_Timer_SetTimeSec( usint  Value);
tTimerMin							Task_Timer_SetTimeMin( byte Value);
tTimerHour							Task_Timer_SetTimeHour( byte Value);
/*----------------------------------------------------------------------------*/

