/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define  BAR_CODE_READER_1                0
#define  BAR_CODE_READER_2                1

#define  B2B_MAX_SIZE_READER_BUF          30
#define  B2B_MAX_SIZE_TRACK_2             40

#define	EXT_LCD_LINE_WIDTH               20 // maximum 20 characters per LCD line
#define	MAX_BYTES_PRINT_TEXT             CHARS_PER_LINE_FONT_B_NORMAL  // see PrintAPI.h

#if 0
                        "========================="
                        "    Sub-Commands List    "
                        "========================="
#endif

// Sub-Commands:  General
#define  SUB_CMD_VOID                           0

// Sub-Commands:  Bar-Code Reader
#define  SUB_CMD_BRC_GET_STATUS                 0
#define  SUB_CMD_BRC_SET_MODE                   1
#define  SUB_CMD_BRC_GET_BARCODE_LABEL          2
#define  SUB_CMD_BRC_HANDLE_TICKET              3

// Sub-Commands:  Bill Validator
#define  SUB_CMD_BLV_GET_STATUS                 0
#define  SUB_CMD_BLV_SET_MODE                   1
#define  SUB_CMD_BLV_GET_BILL_VALUE             2
#define  SUB_CMD_BLV_HANDLE_BILL                3
#define  SUB_CMD_BLV_GET_NUM_BILLS_IN_STACKER   4
#define  SUB_CMD_BLV_SET_ENABLED_BILLS          5

// Sub-Commands:  Buzzer
#define  SUB_CMD_BUZ_SET_MODE                   0

// Sub-Commands:  Clock
#define  SUB_CMD_CLK_SET_CLOCK                  0
#define  SUB_CMD_CLK_GET_CLOCK                  1

// Sub-Commands:  Coin Validator
#define  SUB_CMD_CNV_GET_STATUS                 0
#define  SUB_CMD_CNV_SET_MODE                   1
#define  SUB_CMD_CNV_GET_COIN_VALUE             2
#define  SUB_CMD_CNV_SET_ENABLED_COINS          3
#define  SUB_CMD_CNV_SET_COIN                   4

// Sub-Commands:  Default Parameters
#define  SUB_CMD_PRM_SET_PARAMETER              0

// Sub-Commands:  LCD
#define  SUB_CMD_LCD_WRITE_STRING               0
#define  SUB_CMD_LCD_SET_TEXT_BLINKING          1
#define  SUB_CMD_LCD_SET_CURSOR_POSITION        2
#define  SUB_CMD_LCD_SET_CURSOR_MODE            3
#define  SUB_CMD_LCD_SET_BACKLIGHT              4
#define  SUB_CMD_LCD_CLEAR_LINE                 5
#define  SUB_CMD_LCD_CLEAR_SCREEN               6
#define  SUB_CMD_LCD_WRITE_PRICE                7
#define  SUB_CMD_LCD_WRITE_NUMBER               8
#define  SUB_CMD_LCD_WRITE_NUMBER_HEX           9
#define  SUB_CMD_LCD_WRITE_CHAR                 10

// Sub-Commands:  LED
#define  SUB_CMD_LED_SET_MODE                   0

// Sub-Commands:  MGC (Magnetic Card Reader)
#define  SUB_CMD_MGC_GET_STATUS                 0
#define  SUB_CMD_MGC_SET_MODE                   1
#define  SUB_CMD_MGC_GET_TRACK2                 2

// Sub-Commands:  Printer
#define  SUB_CMD_PRN_GET_STATUS                 0
#define  SUB_CMD_PRN_SET_FONT                   1
#define  SUB_CMD_PRN_PRINT_TEXT                 2
#define  SUB_CMD_PRN_CUTTER                     3
#define  SUB_CMD_PRN_PRINT_BARCODE_LABLE        4

// Sub-Commands:  Relay
#define  SUB_CMD_RLY_SET_MODE                   0
/*---------------------------- Typedef Directives ----------------------------*/
typedef  struct
{
   byte        UnitType;
   byte        Address;
   byte        Command;
   byte        Sub_Command;
   byte        Index;
}  tB2B_Command;



#if 0
                        "========================="
                        "Messages from Master unit"
                        "========================="
#endif

typedef  union
{
/*********** Bar-Code Reader ***********/
   bool        BC_Reader_SetOperationMode;   // Enabled  Disabled
   bool        BC_Reader_HandleTicket;       // Return / Accept (Swallow)

/*********** Bill Validator ***********/
   bool        Bill_Valid_SetOperationMode;  // Enabled / Disabled
   bool        Bill_Valid_HandleBill;        // Return / Accept
   usint       Bill_Valid_EnabledBills;      // Array of 16 bits. Each bit represents an individual bill

/*********** Buzzer ***********/
   struct
   {
      byte     Mode;                         // Pattern
      byte     Time;                         // Time of Activation
   }  Buzzer_SetOperation;

/*********** Clock ***********/
   ulong       Clock_SetDateAndTime;         // Seconds elapsed since Jan-1-2000

/*********** Coin Validator */
   bool        Coin_Valid_SetOperationMode;  // Enabled / Disabled
   usint       Coin_Valid_EnabledCoins;      // Array of 16 bits. Each bit represents an individual coin

   struct
   {
      usint    Value;                        // Coin value in Agorot (Cents)
      byte     Routing;                      // Routing (Hopper)
   }  Coin_Valid_SetCoin;

/*********** LCD ***********/
#if 0 // [
   struct
   {
      byte     Line;                         // Line
      byte     Offset;                       // Offset (position)
      char     Text[EXT_LCD_LINE_WIDTH + 1]; // Text to show
   }  LCD_WriteString;

   bool        LCD_SetTextBlinkingMode;      // ON / OFF

   struct
   {
      byte     Line;                         // Line
      byte     Offset;                       // Offset (position)
   }  LCD_SetCursorPosition;

   bool        LCD_SetCursorMode;            // Hidden / Blinking
   bool        LCD_SetBacklightMode;         // ON / OFF
   byte        LCD_ClearLine;                // Clear line

   struct
   {
      byte     Line;                         // Line
      byte     Offset;                       // Offset (position)
      ulong    Value;                        // Price to show
      byte     Digits;                       // Digits (length) of the number
      byte     Justify;                      // Justify text (void / left / center / right)
   }  LCD_WriteNumber;

   struct
   {
      byte     Line;                         // Line
      byte     Offset;                       // Offset (position)
      char     Char;                         // Character to show
   }  LCD_WriteChar;
#endif   // ]

/*********** LED ***********/
   struct
   {
      byte     Mode;                         // Pattern
      byte     Time;                         // Time of Activation
   }  LED_SetOperation;

/*********** MGC (Magnetic Card Reader) ***********/
   bool        MGC_SetOperationMode;         // Enabled  Disabled

/*********** Printer ***********/
   struct
   {
      byte     Type;                         // Font type (A / B)
      byte     Height;                       // Font height (Normal / Double)
      byte     Width;                        // Font width (Normal / Double)
   }  Printer_SetFont;

   struct
   {
      byte     Alignment;                    // Text alignment (Left / Center / Right)
      usint    Count;                        // Number of bytes to print
      byte     Text[MAX_BYTES_PRINT_TEXT + 1];// Text to print
   }  Printer_PrintText;

   byte        Printer_SetCutterMode;        // Full / Half

   struct
   {
      byte     Type;                         // Bar-Code type (like Code128)
      ulong    Number;
   }  Printer_BarCode;

/*********** Relay ***********/
   struct
   {
      byte     Mode;                         // Pattern
      byte     Time;                         // Time of Activation
   }  Relay_SetOperation;
}  tB2B_FromMaster;





#if 0
                        "========================"
                        "Messages from Slave unit"
                        "========================"
#endif


// Slave's unit status
#define  B2B_STATUS_DEVICE_AFTER_RESET       BIT_0

// Bar-Code Reader
#define  B2B_STATUS_READER_LABEL_DETECTED    BIT_0
#define  B2B_STATUS_READER_SENSOR_1_ACTIVE   BIT_1 // Close to bezel (front)
#define  B2B_STATUS_READER_SENSOR_2_ACTIVE   BIT_2 // Close to Scanner (back)
#define  B2B_STATUS_READER_DISABLED          BIT_7
#define  B2B_STATUS_READER_DISCONNECTED      0xFF

// Bill Validator
#define  B2B_STATUS_BILL_VAL_BILL_DETECTED   BIT_0
#define  B2B_STATUS_BILL_VAL_RETURNED        BIT_1
#define  B2B_STATUS_BILL_VAL_STACKED         BIT_2
#define  B2B_STATUS_BILL_VAL_BILL_JAM        BIT_3
#define  B2B_STATUS_BILL_VAL_STACKER_JAM     BIT_4
#define  B2B_STATUS_BILL_VAL_STACKER_OUT     BIT_5
#define  B2B_STATUS_BILL_VAL_STACKER_FULL    BIT_6
#define  B2B_STATUS_BILL_VAL_DISABLED        BIT_7
#define  B2B_STATUS_BILL_VAL_DISCONNECTED    0xFF

// Printer
#define  B2B_STATUS_PRN_BUSY                 BIT_0
#define  B2B_STATUS_PRN_NEARLY_OUT_OF_PAPER  BIT_1
#define  B2B_STATUS_PRN_ERROR                BIT_2
#define  B2B_STATUS_PRN_DISCONNECTED         0xFF

// Coin Validator
#define  B2B_STATUS_COIN_VAL_COIN_DETECTED   BIT_0
#define  B2B_STATUS_COIN_VAL_DISABLED        BIT_7

// MGC Reader
#define  B2B_STATUS_MGC_CARD_DETECTED        BIT_0
#define  B2B_STATUS_MGC_SENSOR_ACTIVE        BIT_1
#define  B2B_STATUS_MGC_DISABLED             BIT_7

// Buttons (IO_Buttons)
#define  B2B_STATUS_BUTTON_1                 BIT_0 // Selection 3
#define  B2B_STATUS_BUTTON_2                 BIT_1 // Language
#define  B2B_STATUS_BUTTON_3                 BIT_2 // Receipt
#define  B2B_STATUS_BUTTON_4                 BIT_3 // Cancel
#define  B2B_STATUS_BUTTON_5                 BIT_4 // Selection 1
#define  B2B_STATUS_BUTTON_6                 BIT_5 // Selection 2


// Bill Validator Status bytes
/*

            STATUS 1
+---+---+---+---+---+---+---+---+
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
+---+---+---+---+---+---+---+---+
  |   |   |   |               |
  |   |   |   +---------------+---- Bill Type / Error Code
  |   |   +------------------------ Bill in Escrow
  |   +---------------------------- Bill Returned
  +-------------------------------- Bill Stacked

            STATUS 2
+---+---+---+---+---+---+---+---+
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
+---+---+---+---+---+---+---+---+
  |   |   |   |   |   |   |   |
  |   |   |   |   |   |   |   +---- Idle state
  |   |   |   |   |   |   +-------- Accepting state
  |   |   |   |   |   +------------ Returning state
  |   |   |   |   +---------------- Stacking state
  |   |   |   +-------------------- Rejecting state
  |   |   +------------------------ Cheated
  |   +---------------------------- Invalid Escrow request
  +-------------------------------- Power-ON reset

            STATUS 3
+---+---+---+---+---+---+---+---+
| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
+---+---+---+---+---+---+---+---+
  |   |   |   |   |   |   |   |
  |   |   |   |   |   |   |   +---- Bill jam
  |   |   |   |   |   |   +-------- Stacker jam
  |   |   |   |   |   +------------ Stacker OFF
  |   |   |   |   +---------------- Stacker is full
  |   |   |   +-------------------- Power-up with bill in escrow
  |   |   +------------------------ Timeout
  |   +---------------------------- Disabled
  +-------------------------------- Inhibited
*/

#define  BILL_STATUS1_BILL_TYPE              (BIT_0 | BIT_1 | BIT_2 | BIT_3 | BIT_4)
#define  BILL_STATUS1_ERROR_CODE             (BIT_0 | BIT_1 | BIT_2 | BIT_3 | BIT_4)
#define  BILL_STATUS1_BILL_IN_ESCROW         BIT_5
#define  BILL_STATUS1_BILL_RETURNED          BIT_6
#define  BILL_STATUS1_BILL_STACKED           BIT_7

#define  BILL_STATUS2_IDLE                   BIT_0
#define  BILL_STATUS2_ACCEPTING              BIT_1
#define  BILL_STATUS2_RETURNING              BIT_2
#define  BILL_STATUS2_STACKING               BIT_3
#define  BILL_STATUS2_REJECTING              BIT_4 // if this bit is HIGH, BILL_STATUS1_ERROR_CODE is relevane
#define  BILL_STATUS2_CHEATED                BIT_5
#define  BILL_STATUS2_INVALID_ESCROW_REQ     BIT_6
#define  BILL_STATUS2_PWR_ON_RESET           BIT_7

#define  BILL_STATUS3_BILL_JAM               BIT_0
#define  BILL_STATUS3_STACKER_JAM            BIT_1
#define  BILL_STATUS3_STACKER_OUT            BIT_2
#define  BILL_STATUS3_STACKER_IS_FULL        BIT_3
#define  BILL_STATUS3_PWR_UP_WITH_BILL_IN    BIT_4
#define  BILL_STATUS3_TIMEOUT                BIT_5
#define  BILL_STATUS3_DISABLED               BIT_6
#define  BILL_STATUS3_INHIBITED              BIT_7

#define  BILL_ERROR_OPTICAL_TEST_FAIL        0x07
#define  BILL_ERROR_DENOMINATION_DISABLED    0x08
#define  BILL_ERROR_MAGNETIC_TEST_FAIL       0x0C
#define  BILL_ERROR_MAX_CREDIT_STRORED       0x0E
#define  BILL_ERROR_BLUE_RATIO_TEST_FAIL     0x0F
#define  BILL_ERROR_ALL_BILLS_INHIBITED      0x10
#define  BILL_ERROR_BILL_CANNOT_BE_STACKED   0x11
#define  BILL_ERROR_HIGH_SECURITY_TEST_FAIL  0x12


typedef  struct   // DO NOT CHANGE THIS TO 'union' !!!
{
   /* Device */
   struct
   {
      byte           Device;           // Slave device's status
      byte           BC_Reader_1;      // Bar-Code Reader No. 1 (TIBA)
      byte           BC_Reader_2;      // Bar-Code Reader No. 2 (TIBA)
      byte           Bill_Validator;   // Bill Validator (GPT)
      byte           Printer;          // Printer (T-102)
      byte           Coin_Validator;   // Coin Validator (T-12 / T-15)
      byte           MGC_Reader;       // Magnetic Card Reader
      byte           Relays;           // Relays (8 relays - ON/OFF)
      byte           IO_Exar_A;        // Inputs: Exar port A
      byte           IO_Exar_B;        // Inputs: Exar port B
      byte           IO_Exar_C;        // Inputs: Exar port C
      byte           IO_Exar_D;        // Inputs: Exar port D
      byte           IO_DIP_Switches;  // Inputs: DIP Switches (8 knobs - ON/OFF)
      byte           IO_Buttons;       // Inputs: Selection Buttons (8 buttons - ON/OFF)
      byte           BV_Status_1;      // Bill Validator: Status byte #1
      byte           BV_Status_2;      // Bill Validator: Status byte #2
      byte           BV_Status_3;      // Bill Validator: Status byte #3
      byte           PRN_Status;       // Printer: Status byte
   }  GetStatus;                                                  // 18 bytes

   /* Bar-Code Reader I */
   struct
   {
      byte           Sensor_1;
      byte           Sensor_2;
   }  BC_Reader_1_Status;                                         // 4 bytes

   char              BC_Reader_1_Label[B2B_MAX_SIZE_READER_BUF];  // 30 bytes

   /* Bill Validator */
   struct
   {
      byte           VendorCode;
   }  Bill_Valid_Status;                                          // 1 bytes

   usint             Bill_Valid_Value;                            // 2 bytes
   usint             Bill_Valid_BillsInStacker;                   // 2 bytes

   /* Clock */
   ulong             Clock_DateAndTime;                           // 4 bytes

   /* Coin Validator */
   byte              Coin_Valid_Status;                           // 1 byte

   struct
   {
      usint          Value;
      byte           RoutedTo;
   }  Coin_Valid_Value;                                           // 3 bytes

   /* MGC (Magnetic Card Reader) */
   byte              MGC_Status;                                  // 1 byte
   char              MGC_Track2[B2B_MAX_SIZE_TRACK_2];            // 40 bytes

   /* Printer */
   struct
   {
      byte           VendorCode;
      byte           Status_1;
      byte           Status_2;
      byte           Status_3;
   }  Printer_Status;                                             // 4 bytes
}  tB2B_FromSlave;                                                // Total: 108 bytes
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    	Task_B2B_Master_Init( void);
void                    	Task_B2B_Master_Main( void);
bool                    	Task_B2B_Master_IsClearToSend( void);
tB2B_FromMaster                 *Task_B2B_GetTxDataPtr( void);
tB2B_FromSlave                  *Task_B2B_GetRxDataPtr( void);
void                            Task_B2B_SendCommand( tB2B_Command  *pCommand, tB2B_FromMaster  *pData);
bool                    	Task_B2B_IsSlaveActive( byte Type, byte  Address);
void                            Task_B2B_SetSlaveAddress( byte   Address);
void                    	Task_B2B_SetSlaveAddress( byte Address);
bool                    	Task_B2B_IsSlaveCoinActive(void);
bool                    	Task_B2B_IsSlaveBillActive(void);
/*----------------------------------------------------------------------------*/

