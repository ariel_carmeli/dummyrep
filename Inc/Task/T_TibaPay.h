/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
#define     TP_MAX_RECEIPT_REC_ITEMS                  3
#define     TP_SERVER_MAX_COINS                       8
#define     TP_SERVER_MAX_SIZE_TRACK_2                32

#define     TP_TRANS_REC_TYPE_CASH                    1
#define     TP_TRANS_REC_TYPE_CASH_NEXT               0xC1
#define     TP_TRANS_REC_TYPE_CASH_LAST               0xC0
#define     TP_TRANS_REC_TYPE_CONFIRMATION            2
#define     TP_TRANS_REC_TYPE_CONFIRMATION_NEXT       0xD1
#define     TP_TRANS_REC_TYPE_CONFIRMATION_LAST       0xD0
#define     TP_TRANS_REC_TYPE_MANUY_ENTRANCE_MIKVE    51
#define     TP_TRANS_REC_TYPE_BALAN_ENTRANCE_MIKVE_1  61 // Main door
#define     TP_TRANS_REC_TYPE_BALAN_ENTRANCE_MIKVE_2  62 // Emergency door
#define     TP_TRANS_REC_TYPE_TIME_AND_ATTENDANCE     71 // Time and Attendance
#define     TP_TRANS_REC_TYPE_SERVER_GATE_OPEN        81
#define     TP_TRANS_REC_TYPE_TERMINATE_Z             90
#define     TP_TRANS_REC_TYPE_STATUS_DOOR_OPENED      100   // 100-109
#define     TP_TRANS_REC_TYPE_STATUS_DOOR_CLOSED      110   // 110-119
#define     TP_TRANS_REC_TYPE_STATUS_Z_TERMINATED     120   // 120-129

#define     TP_TRANS_REC_DATA_TYPE_CREDIT_VALUE       200   // Credit Card - Value
#define     TP_TRANS_REC_DATA_TYPE_CREDIT_CONFIRM     201   // Credit Card - Confirmation #
#define     TP_TRANS_REC_DATA_TYPE_CREDIT_LAST_4      202   // Credit Card - Number's Last 4 digits
#define     TP_TRANS_REC_DATA_TYPE_STICKER_NUM        203   // Sticker - Number
#define     TP_TRANS_REC_DATA_TYPE_STICKER_VALUE      204   // Sticker - Value
#define     TP_TRANS_REC_DATA_TYPE_PRODUCT            210   // Product - Byte 0 (MSB) - Product Code; Byte 1 = Amount (Qtty); Byte 2+3 = Product Price
#define     TP_TRANS_REC_DATA_TYPE_MANUY_CARD         221   // Member Card - Card Number
#define     TP_TRANS_REC_DATA_TYPE_MANUY_VALUE        222   // Member Card - Value
#define     TP_TRANS_REC_DATA_TYPE_MANUY_DATE         223   // Member Card - New Expiration Date
#define     TP_TRANS_REC_DATA_TYPE_MANUY_BALANCE      224   // Member Card - Balance
#define     TP_TRANS_REC_DATA_TYPE_DEBT_NUMBER        230   // Debt - Confirmation Number
#define     TP_TRANS_REC_DATA_TYPE_DEBT_VALUE         231   // Debt - Value paid

#define     TP_TRANS_REC_FLAG_HAS_NEXT_REC            BIT_0             // BIT_0
#define     TP_TRANS_REC_FLAG_VALUE_10_EXP_0          0                 // BIT_1 = 0; BIT_2 = 0
#define     TP_TRANS_REC_FLAG_VALUE_10_EXP_1          BIT_1             // BIT_1 = 1; BIT_2 = 0
#define     TP_TRANS_REC_FLAG_VALUE_10_EXP_2          BIT_2             // BIT_1 = 0; BIT_2 = 1
#define     TP_TRANS_REC_FLAG_VALUE_10_EXP_3          (BIT_1 | BIT_2)   // BIT_1 = 1; BIT_2 = 1

#define     TP_TRANS_REC_ACTION_ENTRANCE_MIKVE        0
#define     TP_TRANS_REC_ACTION_RECHARGE_PREPAID      1
#define     TP_TRANS_REC_ACTION_RENEW_MEMBERSHIP      2
#define     TP_TRANS_REC_ACTION_DEBT_PAYMENT          3
#define     TP_TRANS_REC_ACTION_SERVICE               51 // 51 - 100

#define     TP_ACTIVE_COIN_CHANGER                    BIT_0
#define     TP_ACTIVE_BILL_VALIDATOR                  BIT_1
#define     TP_ACTIVE_PRINTER                         BIT_2
#define     TP_ACTIVE_RFID_CARD_RW                    BIT_3
#define     TP_ACTIVE_GT_CTL_1                        BIT_4
#define     TP_ACTIVE_GT_CTL_2                        BIT_5
#define     TP_ACTIVE_GT_CTL_3                        BIT_6
#define     TP_ACTIVE_GT_CTL_4                        BIT_7

#define     TP_PRN_STATUS_EOP_SENSOR                  BIT_0
#define     TP_PRN_STATUS_PAPER_OUT                   BIT_1
#define     TP_PRN_STATUS_OFF                         0xFF

#define     TP_D_CMD_VOID                             0
#define     TP_D_CMD_TECHNICIAN                       9  // 0x09
#define     TP_D_CMD_EMERGENCY_CREDIT_CARD            16 // 0x10
#define     TP_D_CMD_DB                               32 // 0x20

// Sub-commands for D-09
#define     TP_D_SUB_CMD_TECH_RESET                   0x22
#define     TP_D_SUB_CMD_TECH_OPEN_CLOSE_MACHINE      0x01
#define     TP_D_SUB_CMD_TECH_VOID                    0x02
#define     TP_D_SUB_CMD_TECH_RESUME                  0x03
#define     TP_D_SUB_CMD_TECH_OPEN_GATE               0x04
#define     TP_D_SUB_CMD_TECH_ACTIVATE_RELAY          0x05
#define     TP_D_SUB_CMD_TECH_ENABLE_TECH_MODE        0x06
#define     TP_D_SUB_CMD_TECH_SET_Z_NUMBER            0x07

// Sub-commands for D-32
#define     TP_D_SUB_CMD_DB_RESET_L_POINTERS          0x01  // 1
#define     TP_D_SUB_CMD_DB_RESET_V_POINTERS          0x02  // 2
#define     TP_D_SUB_CMD_DB_RESET_Z_POINTERS          0x03  // 3
#define     TP_D_SUB_CMD_DB_RESET_DEBT_POINTERS       0x04  // 4
#define     TP_D_SUB_CMD_DB_RESET_ALL_POINTERS        0x0F  // 15
#define     TP_D_SUB_CMD_DB_DEL_CRNT_Z_RECORD         0x10  // 16
#define     TP_D_SUB_CMD_DB_FORMAT_PARAMS             0x20  // 32
#define     TP_D_SUB_CMD_DB_FORMAT_STRINGS            0x30  // 48
#define     TP_D_SUB_CMD_DB_FORMAT_VARS               0x40  // 64
#define     TP_D_SUB_CMD_DB_FORMAT_L_RECORDS          0x50  // 80
#define     TP_D_SUB_CMD_DB_FORMAT_V_RECORDS          0x51  // 81
#define     TP_D_SUB_CMD_DB_FORMAT_E_RECORDS          0x52  // 82
#define     TP_D_SUB_CMD_DB_FORMAT_Z_RECORDS          0x53  // 83

#define     TP_EMERGENCY_TYPE_VOID                    0
#define     TP_EMERGENCY_TYPE_CREDIT_CARD             16
/*---------------------------- Typedef Directives ----------------------------*/
                  /**********************************************/
                  /******************* MASTER *******************/
                  /**********************************************/
#if 1
typedef  union    /* Command D   (Do action) */
{
   struct
   {
      byte        ReaderID;
      byte        MessageNum;
   }  CloseGate;  // Keep gate closed

   struct
   {
      byte        ReaderID;
      byte        MessageNum;
   }  OpenGate;   // Keep gate opened

   struct
   {
      byte        ReaderID;
      byte        fRelay;  // bit 0    Relay 1
                           // bit 1    Relay 2
                           // bit 2    Relay 3
                           // bit 3    Relay 4
      byte        fMode;   // bit 0    Relay 1 (0 = OFF, 1 = ON)
                           // bit 1    Relay 2 (0 = OFF, 1 = ON)
                           // bit 2    Relay 3 (0 = OFF, 1 = ON)
                           // bit 3    Relay 4 (0 = OFF, 1 = ON)
   }  HoldRelay;  // Hold Relay ON/OFF

   struct
   {
      byte        Void;
      byte        ReaderID;
      byte        ActionCode;
      ulong       Value[4];
   }  Tech;       // Technician actions

   struct
   {
      byte        Void;
      byte        ReaderID;
      byte        ActionCode;
   }  DB;         // Database actions

   struct
   {
      byte        Void_1;
      byte        ReaderID;
      byte        ResultCode;
      ulong       Value;
      ulong       AuthorizationCode;   // of Clearance house
      ulong       ConfirmationCode;    // of PC
      usint       Last4Digits;
      byte        CardIssuer;
   }  CreditCard; // Credit Card clearance
}  TP_Cmd_D_Data;
#endif

#if 1
typedef  union    /* Master Structure */
{
   struct         // Command L   (Emergency / Transaction / Event / Status)
   {
      usint       Idx_R_Trans;   // Read pointer of Transactions
      byte        DeviceID;      // Device ID (number)
   }  Cmd_L;

   struct         // Command D
   {
      byte        SubCmd;     // 00 (Keep gate closed)
                              // 01 (Keep gate opened)
                              // 02 (Hold relay ON/OFF)
                              // 16 (Credit Card clearance)
      TP_Cmd_D_Data  Data;       // 16 bytes
   }  Cmd_D;
}  tTibaPay_MasterRec;
#endif





                  /**********************************************/
                  /******************* SLAVE ********************/
                  /**********************************************/

typedef  struct    /* Command E   (Emergency) */
{
   byte        TagType;
   ulong       Tag_Code_Num;
   byte        Reader_Num;
   byte        IO_Status;
   ulong       Void;
   ulong       TimeStamp;
}  Cmd_E_RdrMpSw; // MP/SW

#if 0
typedef  struct    /* Command E   (Emergency) */
{
   ulong       ValueToCharge;
   byte        Void1;
   byte        Reader_Num;
   byte        CipherKeyIdx;
   ulong       TicketNum;
   ulong       TimeStamp;
   byte        Track2[SERVER_MAX_SIZE_TRACK_2]; // filled with ending FF's
}  Cmd_E_CreditCard; // Credit Card
#endif
#if 0
typedef  union    /* Command E   (Emergency) */
{
   Cmd_E_RdrMpSw     RdrMpSw;    // Type 01
   Cmd_E_CreditCard  CreditCard; // Type 16
}  Cmd_E_Data;
#endif
#if 0
typedef  struct
{
   byte           Type;
   ulong          Value;
}  tRcptItem;
#endif
#if 0
typedef  union    /* Command L   (Transaction) */
{
   struct
   {
      byte        Type;       // 1  Cash transaction        (Coins, Bills, Credit Cards)
                              // 2  Confirmation (non-paid) (Prepaid cards, Coupons, Stickers)
      ulong       DateAndTime;
      usint       RcptConfirm_Num;  // Receipt number / Confirmation number
      usint       Receipt_Z_Num;
      byte        Reader_Num;
      byte        Flags;      // BIT 0    -  Has a Next record
                              // BIT1+2   -  Exp. 10 of money values
                              //    00    -  Agorot
                              //    01    -  1/10 NIS
                              //    10    -  1 NIS
                              //    11    -  10 NIS
      ulong       CashValue;
      byte        ActionCode; // 0     Entrance to Mikve (of any kind)
                              // 1     Recharging Prepaid Card
                              // 2     Renewing Membership Card
                              // 3     Debt payment ('Tashlum Hov')
                              // 51    Service  -     Dipping ('Tvillah') only
                              // 52    Service  -     Preparation & Dipping ('Hachana & Tvillah')
                              // 53    Service  -     Private Room
                              // 54    Service  -     Kallah (Bride)
   }  RcptHeader; // Receipt Header

   struct
   {
      byte        ContCode;   // Continuation Code
                              // Continuation of Cash receipt: C1 = Has next record; C0 = Last record
                              // Continuation of Confirmation: D1 = Has next record; D0 = Last record
      tRcptItem   Items[MAX_RECEIPT_REC_ITEMS];
                              // Type:
                              // 200	Credit Card - Value
                              // 201	Credit Card - Confirmation # (generated by PC)
                              // 202	Credit Card - Last 4 digits (sent by PC)
                              // 203	Sticker #
                              // 204	Sticker Value
                              // 210	Product
                              // 221	Member Card    - Number
                              // 222	Member Card    - Value
                              // 223   Member Card    - Exp. Date
                              // 224   Member Card    - New Balance
                              // 230	Debt payment   - Confirmation #
                              // 231	Debt payment   - Value

                              // Value:
                              // case of Product (210)
                              // Byte 1 (MSB)   Product Code
                              //                0  Void
                              //                1	Extra Towel
                              //                2	Tootbrush
                              //                3	Shaving Blade
                              //                4	Bathing Kit
                              //                5	Companions of Kalla
                              //                   Amount	=	# of companions
                              // Byte 2		   Amount (Qty)
                              // Bytes 3..4	   Total Value (Money, up to 655.00 NIS)
   }  RcptData;   // Receipt Data

   struct
   {
      byte        Type;       // 51 Entrance to Mikve
      ulong       DateAndTime;
      byte        Void1;
      byte        Void2;
      byte        Reader_Num; // Gate
      ulong       CardNumber;
      usint       Balance;
      byte        Void3;
      byte        Void4;
   }  Manuy;   // Receipt Data

   struct
   {
      byte        Type;       // 71 Time and Attendance (In/Out transaction)
      ulong       DateAndTime;
      byte        Void1;
      byte        Void2;
      byte        Reader_Num;
      ulong       CardNumber;
      byte        Direction;  // 0 = IN; 1 = OUT
      byte        Void3;
      byte        Void4;
      byte        Void5;
   }  TimeAndAttendance;

   struct
   {
      byte        Type;       // 81 Remote Gate Opening
      ulong       DateAndTime;
      byte        Void1;
      byte        Void2;
      byte        Reader_Num; // Gate
      ulong       Void3;
      usint       Void4;
      byte        Void5;
      byte        Void6;
   }  RemoteGateOpen;   // Remote Gate Opening (by server)

   struct
   {
      byte        Type;       // 90 Terminate Z
      ulong       DateAndTime;
      usint       Terminated_Z_Num;
      byte        Reader_Num;
      usint       Void1;
      usint       Void2;
      usint       Void3;
      usint       New_Z_Num;
   }  Terminate_Z;   // Terminate Z (and start a new Z session)

   struct
   {
      byte        Type;       // 100 (Machine's Door Opened) / 110 (Machine's Door Closed) / 120 (Z Terminated / New Z)
      ulong       DateAndTime;
      usint       Z_Num;
      byte        Reader_Num;
      ulong       Total_Bills;
      ulong       Status_Num;
   }  Status_100_110_120;  // Machine Status 100/110/120 - Totals

   struct
   {
      byte        Type;    // 101 (Machine's Door Opened) / 111 (Machine's Door Closed) / 121 (Z Terminated / New Z)
      ulong       DateAndTime;
      usint       Z_Num;
      byte        Reader_Num;
      ulong       Total_Central;
      ulong       Total_Changer;
   }  Status_101_111_121;  // Machine Status 101/111/121

   struct
   {
      byte        Type;    // 102 (Machine's Door Opened) / 112 (Machine's Door Closed) / 122 (Z Terminated / New Z)
      ulong       DateAndTime;
      usint       Z_Num;
      byte        Reader_Num;
      usint       Qtty_Bills_20;
      usint       Qtty_Bills_50;
      usint       Qtty_Bills_100;
      usint       Qtty_Bills_200;
   }  Status_102_112_122;  // Machine Status 102/112/122

   struct
   {
      byte        Type;    // 103 (Machine's Door Opened) / 113 (Machine's Door Closed) / 123 (Z Terminated / New Z)
      ulong       DateAndTime;
      usint       Z_Num;
      byte        Reader_Num;
      usint       Qtty_Central_1;
      usint       Qtty_Central_2;
      usint       Qtty_Central_5;
      usint       Qtty_Central_10;
   }  Status_103_113_123;  // Machine Status 103/113/123

   struct
   {
      byte        Type;    // 104 (Machine's Door Opened) / 114 (Machine's Door Closed) / 124 (Z Terminated / New Z)
      ulong       DateAndTime;
      usint       Z_Num;
      byte        Reader_Num;
      usint       Qtty_Changer_1;
      usint       Qtty_Changer_2;
      usint       Qtty_Changer_5;
      usint       Qtty_Changer_10;
   }  Status_104_114_124;  // Machine Status 104/114/124
}  Cmd_L_Data;
#endif

#if 0
typedef  struct
{
   usint          Price;
   byte           Amount;
   byte           Code;
}  Cmd_L_Data_Product;
#endif

#if 0
typedef  union
{
   struct
   {
      byte        Void[8];
   }  Type_0;

   struct
   {
      ulong       Field1;
      byte        Field2[4];
   }  Type_1;

   struct
   {
      ulong       Field1;
      ulong       Field2;
   }  Type_2;

   struct
   {
      usint       Field1;
      usint       Field2;
      usint       Field3;
      usint       Field4;
   }  Type_3;

   struct
   {
      byte        Field1[6];
      usint       Field2;
   }  Type_4;

   struct
   {
      byte        Field1[8];
   }  Type_5;
}  Cmd_V_Data_Format;
#endif

#if 0
typedef  struct   /* Command V   (Event) */
{
   byte              SubCmd;
   byte              EvCode;
   byte              Reader_Num;
   ulong             DateAndTime;
   Cmd_V_Data_Format Data;
   byte              Void2;
}  Cmd_V_Data;
#endif
#if 1
typedef  struct   /* Command Q   (Status) */
{
   byte     Inputs_Local_A;
   byte     Inputs_Local_B;
   byte     Inputs_GT_CTL[MAX_DEVICES_MGC / 2];  // MAX_DEVICES_MGC = 8
   byte     Relays_GT_CTL[MAX_DEVICES_MGC / 2];  // MAX_DEVICES_MGC = 8
   byte     Relays_Local;
   byte     Status_Printer;
   byte     Status_Coin_Vldtr_1;
   byte     Status_Coin_Vldtr_2;
   byte     Status_Coin_Vldtr_3;
   byte     Status_Bill_Vldtr_1;
   byte     Status_Bill_Vldtr_2;
   byte     Status_Bill_Vldtr_3;
   byte     State;
   byte     Void;
   byte     IdxValue_Coin[SERVER_MAX_COINS];
   usint    Status_Activity;  // 0 = Inactive; 1 = Active
                              // BIT_0 -  Coin Changer
                              // BIT_1 -  Bill Validator
                              // BIT_2 -  Printer
                              // BIT_3 -  RFID Card R/W
                              // BIT_4 -  GT-CTL (1)
                              // BIT_5 -  GT-CTL (2)
                              // BIT_6 -  GT-CTL (3)
                              // BIT_7 -  GT-CTL (4)
   /* Note: from this point DONT CHANGE VAR'S POSITION */
   usint    Qtty_Coins_Hopper[SERVER_MAX_COINS];
   usint    Qtty_Coins_Central_5;
   usint    Qtty_Coins_Central_10;
   usint    Qtty_Coins_Central_25;
   usint    Qtty_Coins_Central_50;
   usint    Qtty_Coins_Central_100;
   usint    Qtty_Coins_Central_200;
   usint    Qtty_Coins_Central_500;
   usint    Qtty_Coins_Central_1000;
   usint    Qtty_Bills_1;
   usint    Qtty_Bills_5;
   usint    Qtty_Bills_10;
   usint    Qtty_Bills_20;
   usint    Qtty_Bills_50;
   usint    Qtty_Bills_100;
   usint    Qtty_Bills_200;
   usint    Qtty_Bills_Void;  // 25 integers (50 bytes)
   ulong    Total_Value_Coins;
   ulong    Total_Value_Central;
   ulong    Total_Value_Bills;   // 3 longs (12 bytes)
   /* end of Note section */  // see Task_Server_SaveStatusVars() + Task_Server_ReadStatusVars()
}  TP_Cmd_Q_Data; // 90 bytes
#endif
#if 0
typedef  union    /* Slave Structure */
{
   struct         // Command E   (Emergency)
   {
      usint       Idx_W;
      usint       Idx_R;
      byte        Type;
      Cmd_E_Data  Data; // 16/48 bytes
      usint       Current_Z_Num;
   }  Cmd_E;

   struct         // Command L   (Transaction)
   {
      usint       Idx_W;
      usint       Idx_R;
      Cmd_L_Data  Data; // 16 bytes
      usint       Current_Z_Num;
   }  Cmd_L;

   struct         // Command V   (Event)
   {
      usint       Idx_W;
      usint       Idx_R;
      Cmd_V_Data  Data; // 16 bytes
      usint       Current_Z_Num;
   }  Cmd_V;

   struct         // Command Q   (Status)
   {
      usint       Void1;
      usint       Void2;
      //Cmd_Q_Data  Data; // Up to 96 bytes (this structure is handled by task_StatusStruct in Task Server module) 
   }  Cmd_Q;
}  tServer_SlaveRec;
#endif

/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    	Task_TibaPay_Init( void);
void                    	Task_TibaPay_Main( void);
TP_Cmd_Q_Data                   *Task_TibaPay_GetStatusStructPtr( void);
void                            Task_TibaPay_SaveStatusVars( void);
void                            Task_TibaPay_SendStatus( void);
usint                           Task_TibaPay_GetMachineID_Server( void);

void                            Task_TibaPay_ShowTR2buffers(void);              // debug
void                            Task_TibaPay_ShowTxbuffers(void);               // debug
bool                            Task_TibaPay_IsCommLoss(void);
/*----------------------------------------------------------------------------*/
/*--------------- Parameters in External SRAM (direct access) ----------------*/
/*----------------------------------------------------------------------------*/




