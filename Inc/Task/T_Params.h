/*------------------------------ Include Files -------------------------------*/
/*------------------------ Precompilation Definitions ------------------------*/
typedef  ulong                   tParams_A_Rec;       // 4 bytes  (ulong)
typedef  char                    tParams_B_Rec[16];   // 16 bytes (string)
typedef  ulong                   tParams_C_Rec;       // 4 bytes  (ulong)  Server Parameter
typedef  char                    tParams_D_Rec[16];   // 16 bytes (string) Server String
typedef  char                    tParams_E_Rec[32];   // 64 bytes (string) Server String
typedef  ulong                   tParams_Var_Rec;       // 4 bytes  (ulong)  Server Parameter

#define  PARAM_TYPE_A            0
#define  PARAM_TYPE_B            1
#define  PARAM_TYPE_C            2
#define  PARAM_TYPE_D            3
#define  PARAM_TYPE_E            4
#define  PARAM_TYPE_VARS         5
#define  MAX_PARAM_TYPES         6

#if   defined  (__IOM128_H) || defined  (__IO2561_H) || defined  (__IO2560_H) // Atmel ATmega128/2561 (4K bytes in Internal EEPROM); ATmega2560 (8K bytes in Internal EEPROM)
#define  MAX_PARAMS_GROUP_A      1000  // 1000 X 4 =  4000
#define  MAX_PARAMS_GROUP_B      6     // 6 X 16   =  96
#endif

#if   defined  (__IOM16_H) || defined  (__IOM163_H)      // Atmel ATmega16 or ATmega163 (512 bytes in Internal EEPROM)
#define  MAX_PARAMS_GROUP_A      120   // 120 X 4  =  480
#define  MAX_PARAMS_GROUP_B      2     // 2 X 16   =  32
#endif

#ifdef   __IOM32_H   // Atmel    ATmega32  (1024 bytes in Internal EEPROM)
#define  MAX_PARAMS_GROUP_A      240   // 240 X 4  =  960
#define  MAX_PARAMS_GROUP_B      4     // 4 X 16   =  64
#endif
#define  MAX_PARAMS_GROUP_C      6144  //
#define  MAX_PARAMS_GROUP_D      0     //
#define  MAX_PARAMS_GROUP_E      256   //
#define  MAX_SIZE_VARS           8192  //

#if 0
// Internal EEPROM
#define  BASE_ADDRESS_PARAMS_GROUP_A   (ulong)(0)
#define  BASE_ADDRESS_PARAMS_GROUP_B   (ulong)((ulong)MAX_PARAMS_GROUP_A * sizeof( tParams_A_Rec)) // 0x0000   -  0x1FFF

// External SRAM
#define  BASE_ADDRESS_PARAMS_GROUP_C   (ulong)(0x8000L)                                            // 0x8000   -  0xDFFF  24K of Longs            (6144 elements)
#define  BASE_ADDRESS_PARAMS_GROUP_D   (ulong)(MAX_PARAMS_GROUP_C * sizeof( tParams_C_Rec))        // 0xE000   -  0xE000  0K of  16-byte Strings  (0  elements)
#define  BASE_ADDRESS_PARAMS_GROUP_E   (ulong)(MAX_PARAMS_GROUP_D * sizeof( tParams_D_Rec))        // 0xE000   -  0xFFFF  8K of  32-byte Strings  (256  elements)
#define  BASE_ADDRESS_VARS             (ulong)(MAX_PARAMS_GROUP_E * sizeof( tParams_E_Rec))        // 0x10000  -  0x11FFF 8K                      (var. # of elements)
#define  BASE_ADDRESS_PARAMS_END       (ulong)(BASE_ADDRESS_VARS + MAX_SIZE_VARS)                  // 0x12000
#endif
#if 1
// Internal EEPROM
#define  BASE_ADDRESS_PARAMS_GROUP_A   (ulong)(0)
#define  BASE_ADDRESS_PARAMS_GROUP_B   (BASE_ADDRESS_PARAMS_GROUP_A  +  (ulong)(MAX_PARAMS_GROUP_A * sizeof( tParams_A_Rec))  )  // 0x0000   -  0x1FFF

// External SRAM
#define  BASE_ADDRESS_PARAMS_GROUP_C   (ulong)(0x8000L)                                            // 0x8000   -  0xDFFF  24K of Longs            (6144 elements)
#define  BASE_ADDRESS_PARAMS_GROUP_D   (BASE_ADDRESS_PARAMS_GROUP_C  +  (ulong)(MAX_PARAMS_GROUP_C * sizeof( tParams_C_Rec))  )  // 0xE000   -  0xE000  0K of  16-byte Strings  (0  elements)
#define  BASE_ADDRESS_PARAMS_GROUP_E   (BASE_ADDRESS_PARAMS_GROUP_D  +  (ulong)(MAX_PARAMS_GROUP_D * sizeof( tParams_D_Rec))  )  // 0xE000   -  0xFFFF  8K of  32-byte Strings  (256  elements)
#define  BASE_ADDRESS_VARS             (BASE_ADDRESS_PARAMS_GROUP_E  +  (ulong)(MAX_PARAMS_GROUP_E * sizeof( tParams_E_Rec))  )  // 0x10000  -  0x11FFF 8K                      (var. # of elements)
#define  BASE_ADDRESS_PARAMS_END       (BASE_ADDRESS_VARS            +  (ulong)(MAX_SIZE_VARS)                                )  // 0x12000
#endif

#define  BASE_ADDRESS_STRINGS_16       BASE_ADDRESS_PARAMS_GROUP_D
#define  BASE_ADDRESS_STRINGS_32       BASE_ADDRESS_PARAMS_GROUP_E

#define  MAX_HEADERS                   5
#define  MAX_FOOTERS                   5

/* End of passage */
/*---------------------------- Typedef Directives ----------------------------*/
typedef  enum  // Parameters - Int. EEPROM
{
   PARAM_VOID  =  0                 ,  // DON'T REMOVE THIS LINE !!! (see T_Setup.c)

   PARAM_A_MACHINE_ADDRESS          ,
   PARAM_A_LANGUAGE                 ,
   PARAM_A_BLOCKED_COINS            ,
   PARAM_A_BLOCKED_BILLS            ,
   PARAM_A_LICENSED_NUMBER          ,
   PARAM_A_BRANCH_NUMBER            ,
   PARAM_A_PASSWORD_LEVEL_1         ,
   PARAM_A_PASSWORD_LEVEL_2         ,
   PARAM_A_PASSWORD_LEVEL_3         ,
   PARAM_A_CHANGE_LIMIT             ,
   PARAM_A_CHANGE_MIN_LEVEL         ,
   PARAM_A_HOPPER_TIMEOUT_DISPENSE  ,  // Extra time to rotate the Hopper's disk after dispensing the last coin
   PARAM_A_HOPPER_TIMEOUT_EMPTY     ,
   PARAM_A_HOPPER_A_COIN_VALUE      ,  // Value of coin in the hopper (like 1.00, 5.00 etc.)
   PARAM_A_HOPPER_B_COIN_VALUE      ,
   PARAM_A_HOPPER_C_COIN_VALUE      ,
   PARAM_A_HOPPER_D_COIN_VALUE      ,
   PARAM_A_HOPPER_A_CAPACITY        ,  // How many coins - maximum - a hopper can hold
   PARAM_A_HOPPER_B_CAPACITY        ,
   PARAM_A_HOPPER_C_CAPACITY        ,
   PARAM_A_HOPPER_D_CAPACITY        ,
   PARAM_A_HOPPER_A_QTTY_TO_EMPTY   ,  // How many coins TO take out from the hopper for an emptying session
   PARAM_A_HOPPER_B_QTTY_TO_EMPTY   ,
   PARAM_A_HOPPER_C_QTTY_TO_EMPTY   ,
   PARAM_A_HOPPER_D_QTTY_TO_EMPTY   ,
   PARAM_A_HOPPER_A_REFILLED        ,  // How many coins were placed into the hopper on last refill session
   PARAM_A_HOPPER_B_REFILLED        ,
   PARAM_A_HOPPER_C_REFILLED        ,
   PARAM_A_HOPPER_D_REFILLED        ,
   PARAM_A_HOPPER_A_EMPTYING        ,  // How many coins were taken out from the hopper on last emptying session
   PARAM_A_HOPPER_B_EMPTYING        ,
   PARAM_A_HOPPER_C_EMPTYING        ,
   PARAM_A_HOPPER_D_EMPTYING        ,
   //---
   PARAM_A_CREDIT_CARD_BLOCKED      ,  // ����� �����
   PARAM_A_SITE_NAME                ,  // Site name
   PARAM_A_BLOCK_OCCAS_CLIENT       ,  // ����� ���� �����
   /******************/
   PARAM_A_LAST                     ,  // LAST PARAMETER IN GROUP A (Don't delete this line !!!)
}  tParam_Code_A;


typedef  enum  // Strings - Int. EEPROM
{
   PARAM_A_END                      =  MAX_PARAMS_GROUP_A,
   /******************/
   PARAM_B_SERVICE_PHONE_NUMBER     ,
   PARAM_B_BRANCH_NAME              ,
   PARAM_B_BRANCH_PHONE_NUMBER      ,
   /******************/
   PARAM_B_LAST                     ,  // LAST PARAMETER IN GROUP B (Don't delete this line !!!)
}  tParam_Code_B;


typedef  enum  // Parameters - Ext. EEPROM
{
   PARAM_C_MACHINE_NUMBER                 ,  // 0x0000
   PARAM_C_ZONE_CODE                      ,  // 0x0001
   PARAM_C_SITE_CODE                      ,  // 0x0002
   PARAM_C_LICENSED_NUMBER                ,  // 0x0003   Licensed Number ('���� �����')
   PARAM_C_COMPANY_NUMBER                 ,  // 0x0004   Company Number ('���� �.�')
   PARAM_C_TIME_PWR_UP                    ,  // 0x0005   Wait-time after Powerup (sec)
   PARAM_C_TIME_SEND_STATUS               ,  // 0x0006   Time-value to update the Server with Status (Q)
   PARAM_C_f_BUZZER_OFF                   ,  // 0x0007   Buzzer mode (Y/N)
   PARAM_C_PASSWORD_1                     ,  // 0x0008   Password for System Operator (if 0 then actual password is 3333)
   PARAM_C_AUTO_Z_MODE                    ,  // 0x0009   Automatic Z creation (0 = disabled; 1 = every month; 2 = every week; 3 = every day)
   PARAM_C_PRODUCT_EXPONENT               ,  // 0x000A   Product Price Exponent (0 = default; 1 = 1/10 [0.1 NIS]; 2 = 1/100 [1 NIS]; 3 = 1/1000 [10 NIS])
   PARAM_C_TIME_AUTO_CANCEL               ,  // 0x000B   Time to activate Auto-Cancellation

   PARAM_C_HMI_LANGUAGE_01       =  0x20  ,  // 0x0020   Hebrew   (Y/N)
   PARAM_C_HMI_LANGUAGE_02                ,  // 0x0021   English  (Y/N)
   PARAM_C_HMI_LANGUAGE_03                ,  // 0x0022
   PARAM_C_HMI_LANGUAGE_04                ,  // 0x0023
   PARAM_C_HMI_LANGUAGE_05                ,  // 0x0024
   PARAM_C_HMI_LANGUAGE_06                ,  // 0x0025
   PARAM_C_COIN_MIN_VALUE                 ,  // 0x0026   Coin - value of minimum accepted coin
   PARAM_C_COIN_MAX_VALUE                 ,  // 0x0027   Coin - value of maximum accepted coin
   PARAM_C_BILL_MIN_VALUE                 ,  // 0x0028   Bill - value of minimum accepted bill
   PARAM_C_BILL_MAX_VALUE                 ,  // 0x0029   Bill - value of maximum accepted bill
   PARAM_C_HPR_AUTO_EMPTY                 ,  // 0x002A   Hopper - automatic empty-state setting
   PARAM_C_HPR_TIMEOUT_PAY                ,  // 0x002B   Hopper - payout timeout
   PARAM_C_HPR_XTRA_TIME_ON               ,  // 0x002C   Hopper - keep-ON extra time
   PARAM_C_HPR_01_VALUE                   ,  // 0x002D   Hopper - coin value
   PARAM_C_HPR_01_MAX_LEVEL               ,  // 0x002E   Hopper - maximum coins level
   PARAM_C_HPR_01_MIN_LEVEL               ,  // 0x002F   Hopper - minimum coins level
   PARAM_C_HPR_02_VALUE                   ,  // 0x0030
   PARAM_C_HPR_02_MAX_LEVEL               ,  // 0x0031
   PARAM_C_HPR_02_MIN_LEVEL               ,  // 0x0032
   PARAM_C_HPR_03_VALUE                   ,  // 0x0033
   PARAM_C_HPR_03_MAX_LEVEL               ,  // 0x0034
   PARAM_C_HPR_03_MIN_LEVEL               ,  // 0x0035
   PARAM_C_HPR_04_VALUE                   ,  // 0x0036
   PARAM_C_HPR_04_MAX_LEVEL               ,  // 0x0037
   PARAM_C_HPR_04_MIN_LEVEL               ,  // 0x0038
   PARAM_C_HPR_05_VALUE                   ,  // 0x0039
   PARAM_C_HPR_05_MAX_LEVEL               ,  // 0x003A
   PARAM_C_HPR_05_MIN_LEVEL               ,  // 0x003B
   PARAM_C_HPR_06_VALUE                   ,  // 0x003C
   PARAM_C_HPR_06_MAX_LEVEL               ,  // 0x003D
   PARAM_C_HPR_06_MIN_LEVEL               ,  // 0x003E
   PARAM_C_COIN_01_VALUE                  ,  // 0x003F   Coin Validator - coin value
   PARAM_C_COIN_02_VALUE                  ,  // 0x0040
   PARAM_C_COIN_03_VALUE                  ,  // 0x0041
   PARAM_C_COIN_04_VALUE                  ,  // 0x0042
   PARAM_C_COIN_05_VALUE                  ,  // 0x0043
   PARAM_C_COIN_06_VALUE                  ,  // 0x0044
   PARAM_C_COIN_07_VALUE                  ,  // 0x0045
   PARAM_C_COIN_08_VALUE                  ,  // 0x0046
   PARAM_C_COIN_09_VALUE                  ,  // 0x0047
   PARAM_C_COIN_10_VALUE                  ,  // 0x0048
   PARAM_C_COIN_11_VALUE                  ,  // 0x0049
   PARAM_C_COIN_12_VALUE                  ,  // 0x004A
   PARAM_C_COIN_13_VALUE                  ,  // 0x004B
   PARAM_C_COIN_14_VALUE                  ,  // 0x004C
   PARAM_C_COIN_15_VALUE                  ,  // 0x004D
   PARAM_C_COIN_16_VALUE                  ,  // 0x004E
   PARAM_C_COIN_TMR_1_VAL_1               ,  // 0x004F   Coin Validator - validator timer value
   PARAM_C_COIN_TMR_1_VAL_2               ,  // 0x0050
   PARAM_C_COIN_TMR_2_VAL_1               ,  // 0x0051
   PARAM_C_COIN_TMR_2_VAL_2               ,  // 0x0052
   PARAM_C_SPRT_TMR_1_VAL_1               ,  // 0x0053   Coin Validator - separator timer value
   PARAM_C_SPRT_TMR_1_VAL_2               ,  // 0x0054
   PARAM_C_SPRT_TMR_2_VAL_1               ,  // 0x0055
   PARAM_C_SPRT_TMR_2_VAL_2               ,  // 0x0056
   PARAM_C_BILL_f_STCKR                   ,  // 0x0057   Bill Validator - stacker exists (Y/N)
   PARAM_C_BILL_STCKR_SIZE                ,  // 0x0058   Bill Validator - stacker apacity (size)
   PARAM_C_CRDT_SRVR_TIMOUT               ,  // 0x0059   Credit card server - response timeout
   PARAM_C_CRDT_void                      ,  // 0x005A
   PARAM_C_LVL_EXACT_CHANGE               ,  // 0x005B   Exact change - level
   PARAM_C_CHANGE_void                    ,  // 0x005C
   PARAM_C_MAX_MONEY_CHANGE               ,  // 0x005D   Maximum money back - case of change
   PARAM_C_MAX_MONEY_CANCEL               ,  // 0x005E   Maximum money back - case of cancellation
   PARAM_C_CHANGE_MANAGE_EN               ,  // 0x005F   Change management (Y/N)
   PARAM_C_CHANGE_HPR_1_LVL               ,  // 0x0060   Change management - hopper level
   PARAM_C_CHANGE_HPR_1_QTY               ,  // 0x0061   Change management - quantity to dispense
   PARAM_C_CHANGE_HPR_2_LVL               ,  // 0x0062
   PARAM_C_CHANGE_HPR_2_QTY               ,  // 0x0063
   PARAM_C_CHANGE_HPR_3_LVL               ,  // 0x0064
   PARAM_C_CHANGE_HPR_3_QTY               ,  // 0x0065
   PARAM_C_CHANGE_HPR_4_LVL               ,  // 0x0066
   PARAM_C_CHANGE_HPR_4_QTY               ,  // 0x0067
   PARAM_C_CHANGE_HPR_5_LVL               ,  // 0x0068
   PARAM_C_CHANGE_HPR_5_QTY               ,  // 0x0069
   PARAM_C_CHANGE_HPR_6_LVL               ,  // 0x006A
   PARAM_C_CHANGE_HPR_6_QTY               ,  // 0x006B
   PARAM_C_PRN_EXTRA_LF                   ,  // 0x006C   Printer - Extra LF after printout
   PARAM_C_PRN_COPIES_RPRT1               ,  // 0x006D   Printer - Copies of Open/Close Door report
   PARAM_C_PRN_void_03                    ,  // 0x006E
   PARAM_C_PRN_void_04                    ,  // 0x006F
   PARAM_C_PRN_void_05                    ,  // 0x0070

   PARAM_C_ALERT_01_TIME         =  0xA0  ,  // 0x00A0   Time elapsed before reporting the alert
   PARAM_C_ALERT_02_TIME                  ,  // 0x00A1
   PARAM_C_ALERT_03_TIME                  ,  // 0x00A2
   PARAM_C_ALERT_04_TIME                  ,  // 0x00A3
   PARAM_C_ALERT_05_TIME                  ,  // 0x00A4
   PARAM_C_ALERT_06_TIME                  ,  // 0x00A5
   PARAM_C_ALERT_07_TIME                  ,  // 0x00A6
   PARAM_C_ALERT_08_TIME                  ,  // 0x00A7
   PARAM_C_ALERT_09_TIME                  ,  // 0x00A8
   PARAM_C_ALERT_10_TIME                  ,  // 0x00A9
   PARAM_C_ALERT_11_TIME                  ,  // 0x00AA
   PARAM_C_ALERT_12_TIME                  ,  // 0x00AB
   PARAM_C_ALERT_13_TIME                  ,  // 0x00AC
   PARAM_C_ALERT_14_TIME                  ,  // 0x00AD
   PARAM_C_ALERT_15_TIME                  ,  // 0x00AE
   PARAM_C_ALERT_16_TIME                  ,  // 0x00AF

   PARAM_C_PROD_01_CODE          =  0x2C0 ,  // 0x02C0   Product's code
   PARAM_C_PROD_01_PRICE                  ,  // 0x02C1   Product's Price
   PARAM_C_PROD_01_DISCOUNT_M             ,  // 0x02C2   Discount - Money
   PARAM_C_PROD_01_DISCOUNT_P             ,  // 0x02C3   Discount - Percentage
   PARAM_C_PROD_01_MAX_PROD               ,  // 0x02C4   Max. units allowed per transaction
   PARAM_C_PROD_01_SPARE                  ,  // 0x02C5
   PARAM_C_PROD_02_CODE                   ,  // 0x02C6
   PARAM_C_PROD_02_PRICE                  ,  // 0x02C7
   PARAM_C_PROD_02_DISCOUNT_M             ,  // 0x02C8
   PARAM_C_PROD_02_DISCOUNT_P             ,  // 0x02C9
   PARAM_C_PROD_02_MAX_PROD               ,  // 0x02CA
   PARAM_C_PROD_02_SPARE                  ,  // 0x02CB
   PARAM_C_PROD_03_CODE                   ,  // 0x02CC
   PARAM_C_PROD_03_PRICE                  ,  // 0x02CD
   PARAM_C_PROD_03_DISCOUNT_M             ,  // 0x02CE
   PARAM_C_PROD_03_DISCOUNT_P             ,  // 0x02CF
   PARAM_C_PROD_03_MAX_PROD               ,  // 0x02D0
   PARAM_C_PROD_03_SPARE                  ,  // 0x02D1
   PARAM_C_DIGITEL_TRACK_2_LENGTH         ,  // 0x02D2   17
   PARAM_C_DIGITEL_CARD_ID_START          ,  // 0x02D3   0
   PARAM_C_DIGITEL_CARD_ID_LEN            ,  // 0x02D4   4
   PARAM_C_DIGITEL_CARD_ID_VALUE          ,  // 0x02D5   2145
   /********************************/
   PARAM_C_LAST                           ,
}  tParam_Code_C;




// 0xE000 - 0xFFFF   -  32-bytes strings
#define  STR_E_HEADER                        0x0000                                                                        // Offset from base address (BASE_ADDRESS_STRINGS_32)
#define  STR_E_FOOTER                        ((sizeof( tParams_E_Rec) * MAX_HEADERS)      +  STR_E_HEADER               )  // 0x00A0
#define  STR_E_PROD_01_NAME_HE               ((sizeof( tParams_E_Rec) * MAX_FOOTERS)      +  STR_E_FOOTER               )  // 0x0140
#define  STR_E_PROD_02_NAME_HE               (sizeof( tParams_E_Rec)                      +  STR_E_PROD_01_NAME_HE      )  // 0x0160
#define  STR_E_PROD_03_NAME_HE               (sizeof( tParams_E_Rec)                      +  STR_E_PROD_02_NAME_HE      )  // 0x0180
#define  STR_E_PROD_01_NAME_EN               (sizeof( tParams_E_Rec)                      +  STR_E_PROD_03_NAME_HE      )  // 0x01A0
#define  STR_E_PROD_02_NAME_EN               (sizeof( tParams_E_Rec)                      +  STR_E_PROD_01_NAME_EN      )  // 0x01C0
#define  STR_E_PROD_03_NAME_EN               (sizeof( tParams_E_Rec)                      +  STR_E_PROD_02_NAME_EN      )  // 0x01E0
#define  STR_E_END                           (sizeof( tParams_E_Rec)                      +  STR_E_PROD_03_NAME_EN      )  // 0x0200

// 0x10000  + (0x0000 - 0x01FF) - General Variables                                                                        // Offset from base address (BASE_ADDRESS_VARS)
#define  VAR_LAST_EVENT_NUMERATOR            0x0000   // Base address of VARIABLES                                         // 0x0000
#define  VAR_CURRENT_Z_NUMBER                (sizeof( tParams_Var_Rec)  +  VAR_LAST_EVENT_NUMERATOR                     )  // 0x0004
#define  VAR_CURRENT_RECEIPT_NUMBER          (sizeof( tParams_Var_Rec)  +  VAR_CURRENT_Z_NUMBER                         )  // 0x0008
#define  VAR_CURRENT_CONFIRM_NUMBER          (sizeof( tParams_Var_Rec)  +  VAR_CURRENT_RECEIPT_NUMBER                   )  // 0x000C
#define  VAR_LAST_POWERUP_TIME_STAMP         (sizeof( tParams_Var_Rec)  +  VAR_CURRENT_CONFIRM_NUMBER                   )  // 0x0010
#define  VAR_REPORT_NUMERATOR_1              (sizeof( tParams_Var_Rec)  +  VAR_LAST_POWERUP_TIME_STAMP                  )  // 0x0014
#define  VAR_DAILY_COUNTER                   (sizeof( tParams_Var_Rec)  +  VAR_REPORT_NUMERATOR_1                       )  // 0x0018
#define  VAR_NEXT_Z                          (sizeof( tParams_Var_Rec)  +  VAR_DAILY_COUNTER                            )  // 0x001C
#define  VAR_T_APP_FLAGS                     (sizeof( tParams_Var_Rec)  +  VAR_NEXT_Z                                   )  // 0x0020   Bit 0 CreditCardNotSupported
                                                                                                                           //          Bit 1
                                                                                                                           //          Bit 2

#define  VAR_RCPT_START                      0x0080   // Base address of last receipt                                         0x0080
#define  VAR_RCPT_TRANS_TOTAL_VALUE          VAR_RCPT_START                                                                // 0x0080
#define  VAR_RCPT_TRANS_PRICE                (sizeof( tParams_Var_Rec)  +  VAR_RCPT_TRANS_TOTAL_VALUE                   )  // 0x0084
#define  VAR_RCPT_AMOUNT_RECEIVED            (sizeof( tParams_Var_Rec)  +  VAR_RCPT_TRANS_PRICE                         )  // 0x0088
#define  VAR_RCPT_CHANGE                     (sizeof( tParams_Var_Rec)  +  VAR_RCPT_AMOUNT_RECEIVED                     )  // 0x008C
#define  VAR_RCPT_ACTION                     (sizeof( tParams_Var_Rec)  +  VAR_RCPT_CHANGE                              )  // 0x0090
#define  VAR_RCPT_CASH_AMOUNT_RECEIVED       (sizeof( tParams_Var_Rec)  +  VAR_RCPT_ACTION                              )  // 0x0094
#define  VAR_RCPT_CC_AMOUNT_RECEIVED         (sizeof( tParams_Var_Rec)  +  VAR_RCPT_CASH_AMOUNT_RECEIVED                )  // 0x0098
#define  VAR_RCPT_CC_AUTHORIZATION_CODE      (sizeof( tParams_Var_Rec)  +  VAR_RCPT_CC_AMOUNT_RECEIVED                  )  // 0x009C
#define  VAR_RCPT_CC_CONFIRMATION_CODE       (sizeof( tParams_Var_Rec)  +  VAR_RCPT_CC_AUTHORIZATION_CODE               )  // 0x00A0
#define  VAR_RCPT_CC_LAST_4_DIGITS           (sizeof( tParams_Var_Rec)  +  VAR_RCPT_CC_CONFIRMATION_CODE                )  // 0x00A4
#define  VAR_RCPT_CC_CARD_ISSUER             (sizeof( tParams_Var_Rec)  +  VAR_RCPT_CC_LAST_4_DIGITS                    )  // 0x00A8
#define  VAR_RCPT_RESIDENT_DISCOUNT_TOTAL    (sizeof( tParams_Var_Rec)  +  VAR_RCPT_CC_CARD_ISSUER                      )  // 0x00AC
#define  VAR_RCPT_PRODUCTS                   (sizeof( tParams_Var_Rec)  +  VAR_RCPT_RESIDENT_DISCOUNT_TOTAL             )  // 0x00B0
#define  VAR_RCPT_END                        ((sizeof( tParams_Var_Rec) * MAX_L_REC_RECEIPT_ITEMS) +  VAR_RCPT_PRODUCTS )  // 0x00BC

// 0x10000  + (0x0180 - 0x021F) -  up to 10 records of 16 bytes each (see 'tLogTrans_Rec' in Task_Log.h)
#define  VAR_RECEIPT                         0x0180   // Base address of last receipt 0x0180                                  0x0180

// 0x10000  + (0x0220 - 0x023F) - Z-Record (see 'tLogZ_Rec' in T_Log.h)                                                    // Offset from base address (BASE_ADDRESS_VARS)
#define  VAR_Z_RECORD                        0x0220                                                                        // 0x0220

// 0x10000  + (0x0600 - 0x0613) - vacant (reserved for non-volatile info)

// 0x10000  + (0x0614 - 0x064F) - totals of the machine (non-volatile info)
#define  VAR_STATUS_START                    0x0614
#define  TYPE_VAR_STATUS_QTTY                usint // see T_Server.h::Cmd_Q_Data
#define  VAR_STATUS_QTTY_COINS_HOPPER_A      VAR_STATUS_START                                                              // 0x0614
#define  VAR_STATUS_QTTY_COINS_HOPPER_B      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_A      )  // 0x0616
#define  VAR_STATUS_QTTY_COINS_HOPPER_C      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_B      )  // 0x0618
#define  VAR_STATUS_QTTY_BILLS_20            (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_C      )  // 0x061A
#define  VAR_STATUS_QTTY_BILLS_50            (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_20            )  // 0x061C
#define  VAR_STATUS_QTTY_BILLS_100           (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_50            )  // 0x061E
#define  VAR_STATUS_QTTY_BILLS_200           (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_100           )  // 0x0620
#define  VAR_STATUS_QTTY_COINS_CENTRAL_100   (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_200           )  // 0x0622
#define  VAR_STATUS_QTTY_COINS_CENTRAL_500   (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_100   )  // 0x0624
#define  VAR_STATUS_QTTY_COINS_CENTRAL_1000  (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_500   )  // 0x0626
#define  VAR_STATUS_QTTY_COINS_HOPPER_D      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_1000  )  // 0x0628
#define  VAR_STATUS_QTTY_COINS_CENTRAL_200   (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_D      )  // 0x062A
#define  VAR_STATUS_QTTY_VOID_1              (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_200   )  // 0x062C
#define  VAR_STATUS_QTTY_VOID_2              (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_VOID_1              )  // 0x062E
#define  VAR_STATUS_QTTY_VOID_3              (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_VOID_2              )  // 0x0630
#define  VAR_STATUS_QTTY_VOID_4              (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_VOID_3              )  // 0x0632
#define  VAR_STATUS_QTTY_VOID_5              (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_VOID_4              )  // 0x0634
#define  VAR_STATUS_END                      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_VOID_5              )  // 0x0636
/*
#define  VAR_STATUS_QTTY_COINS_HOPPER_A      VAR_STATUS_START                                                              // 0x0614
#define  VAR_STATUS_QTTY_COINS_HOPPER_B      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_A      )  // 0x0616
#define  VAR_STATUS_QTTY_COINS_HOPPER_C      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_B      )  // 0x0618
#define  VAR_STATUS_QTTY_COINS_HOPPER_D      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_C      )  // 0x061A
#define  VAR_STATUS_QTTY_COINS_HOPPER_E      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_D      )  // 0x061C
#define  VAR_STATUS_QTTY_COINS_HOPPER_F      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_E      )  // 0x061E
#define  VAR_STATUS_QTTY_COINS_HOPPER_G      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_F      )  // 0x0620
#define  VAR_STATUS_QTTY_COINS_HOPPER_H      (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_G      )  // 0x0622
#define  VAR_STATUS_QTTY_COINS_CENTRAL_5     (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_HOPPER_H      )  // 0x0624
#define  VAR_STATUS_QTTY_COINS_CENTRAL_10    (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_5     )  // 0x0626
#define  VAR_STATUS_QTTY_COINS_CENTRAL_25    (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_10    )  // 0x0628
#define  VAR_STATUS_QTTY_COINS_CENTRAL_50    (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_25    )  // 0x062A
#define  VAR_STATUS_QTTY_COINS_CENTRAL_100   (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_50    )  // 0x062C
#define  VAR_STATUS_QTTY_COINS_CENTRAL_200   (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_100   )  // 0x062E
#define  VAR_STATUS_QTTY_COINS_CENTRAL_500   (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_200   )  // 0x0630
#define  VAR_STATUS_QTTY_COINS_CENTRAL_1000  (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_500   )  // 0x0632
#define  VAR_STATUS_QTTY_BILLS_1             (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_COINS_CENTRAL_1000  )  // 0x0634
#define  VAR_STATUS_QTTY_BILLS_5             (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_1             )  // 0x0636
#define  VAR_STATUS_QTTY_BILLS_10            (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_5             )  // 0x0638
#define  VAR_STATUS_QTTY_BILLS_20            (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_10            )  // 0x063A
#define  VAR_STATUS_QTTY_BILLS_50            (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_20            )  // 0x063C
#define  VAR_STATUS_QTTY_BILLS_100           (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_50            )  // 0x063E
#define  VAR_STATUS_QTTY_BILLS_200           (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_100           )  // 0x0640
#define  VAR_STATUS_QTTY_BILLS_VOID          (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_200           )  // 0x0642
#define  TYPE_VAR_STATUS_VALUES              ulong
#define  VAR_STATUS_TOTAL_VALUE_COINS        (sizeof( TYPE_VAR_STATUS_QTTY    )  +  VAR_STATUS_QTTY_BILLS_VOID          )  // 0x0644
#define  VAR_STATUS_TOTAL_VALUE_CENTRAL      (sizeof( TYPE_VAR_STATUS_VALUES  )  +  VAR_STATUS_TOTAL_VALUE_COINS        )  // 0x0648
#define  VAR_STATUS_TOTAL_VALUE_BILLS        (sizeof( TYPE_VAR_STATUS_VALUES  )  +  VAR_STATUS_TOTAL_VALUE_CENTRAL      )  // 0x064C
#define  VAR_STATUS_END                      (sizeof( TYPE_VAR_STATUS_VALUES  )  +  VAR_STATUS_TOTAL_VALUE_BILLS        )  // 0x0650
*/
#define  VAR_END                             VAR_STATUS_END
/*---------------------------- External Variables ----------------------------*/
/*------------------------ Global Function Prototypes ------------------------*/
void                    		Task_Params_Init              ( void);
void                    		Task_Params_Main              ( void);
bool                    		Task_Params_IsBusy            ( void);
void                    		Task_Params_Format            ( void);
bool                    		Task_Params_Read              ( void  *pRec, byte ParamType, usint  ParamCode);
bool                    		Task_Params_Write             ( void  *pRec, byte ParamType, usint  ParamCode);
void                    		Task_Params_Read_Var          ( void  *pRec, usint Size, usint  ParamCode);
void                    		Task_Params_Write_Var         ( void  *pRec, usint Size, usint  ParamCode);
void                    		Task_Params_Read_String       ( byte ParamType, void  *pRec, usint Size, usint  ParamCode);
void                    		Task_Params_Write_String      ( byte ParamType, void  *pRec, usint Size, usint  ParamCode);
void                    		Task_Params_RestoreSettings   ( void);
void                    		Task_Params_SaveSettings      ( void);
void                    		Task_Params_SetFactoryDefaults( void);
/*----------------------------------------------------------------------------*/






